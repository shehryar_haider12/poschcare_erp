<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stocks extends Model
{
    protected $table = 'stocks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        's_id',
        'quantity',
        'stock_date',
        'created_by',
        'updated_by',
        'purchase_d_id',
        's_type',
        'cost',
        'w_id',
        'unit_quantity',
        'grn_no',
        'type',
        'challan_no',
        'inv_no',
    ];

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function current()
    {
        return $this->hasMany('App\CurrentStock','p_id','p_id');
    }

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Vendors','id','s_id');
    }

    public function pdetails()
    {
        return $this->hasOne('App\PurchaseOrderDetails','id','purchase_d_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
