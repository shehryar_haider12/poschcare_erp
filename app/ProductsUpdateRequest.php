<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsUpdateRequest extends Model
{
    protected $table = 'products_update_request';
    protected $primaryKey = 'autoid';
    protected $fillable = [
        'id',
        'pro_name',
        'pro_code',
        'weight',
        'cost',
        'price',
        'alert_quantity',
        'unit_id',
        'brand_id',
        'cat_id',
        's_cat_id',
        'image',
        'description',
        'visibility',
        'p_type',
        'status',
        'vstatus',
        'mrp',
        'tp',
        'ct_id',
        'otherCost',
    ];

    public function unit()
    {
        return $this->hasOne('App\Unit','id','unit_id');
    }
    public function costType()
    {
        return $this->hasOne('App\CostType','id','ct_id');
    }
    public function brands()
    {
        return $this->hasOne('App\Brands','id','brand_id');
    }
    public function category()
    {
        return $this->hasOne('App\Category','id','cat_id');
    }
    public function subcategory()
    {
        return $this->hasOne('App\Subcategory','id','s_cat_id');
    }
    public function stocks()
    {
        return $this->belongsTo('App\Stocks');
    }
    public function currentstocks()
    {
        return $this->hasMany('App\CurrentStock','p_id','id');
    }

    public function current()
    {
        return $this->belongsTo('App\CurrentStock');
    }

    public function finish()
    {
        return $this->hasMany('App\FinishProducts','id','id');
    }

    public function variants()
    {
        return $this->hasMany('App\ProductVariantUpdate','p_id','id');
    }
}
