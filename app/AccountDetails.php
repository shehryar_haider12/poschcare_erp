<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountDetails extends Model
{
    protected $table = 'account_details';
    protected $fillable = [
        'Code',
        'name_of_account',
        'c_id',
        'created_by',
        'updated_by',
        'type',
        'account_type'
    ];

    public function headCategory()
    {
        return $this->hasOne('App\HeadCategory','id','c_id');
    }

    public function parent()
    {
        return $this->hasOne('App\AccountDetails','id','c_id');
    }

    public function generalLedger()
    {
        return $this->hasMany('App\GeneralLedger','account_code','Code');
    }
    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
    public function accounts()
    {
        return $this->hasMany('App\AccountDetails','c_id','id')->with('accounts');
    }

    public function children()
    {
        return $this->hasMany('App\AccountDetails','c_id','id')->with('accounts');
    }
}
