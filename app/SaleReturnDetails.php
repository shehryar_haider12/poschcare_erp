<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleReturnDetails extends Model
{
    protected $table = 'sale_return_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        'ret_id',
        'sd_id',
        'Dquantity',
        'Nquantity',
    ];

    public function salereturn()
    {
        return $this->hasOne('App\SaleReturn','id','ret_id');
    }

    public function saledetail()
    {
        return $this->hasOne('App\SaleDetails','id','sd_id');
    }

}
