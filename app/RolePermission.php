<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table = 'role_permission';
    protected $primaryKey = 'id';
    protected $fillable = [
        'r_id',
        'p_id',
        'created_by',
        'updated_by',
    ];

    public function permission()
    {
        return $this->hasOne('App\Permissions','id','p_id');
    }

    public function role()
    {
        return $this->hasOne('App\Roles','id','r_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
