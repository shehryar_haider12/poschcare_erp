<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralLedger extends Model
{
    protected $table = 'general_ledger';
    protected $primaryKey = 'id';
    protected $fillable = [
        'source',
        'description',
        'account_name',
        'link_id',
        'created_by',
        'accounting_date',
        'posted_date',
        'period',
        'account_code',
        'transaction_no',
        'currency_code',
        'debit',
        'credit',
        'net_value',
        'updated_by',
        'amount',
        'balance',
        'stock_in',
        'stock_out',
        'type',
        'w_id'
    ];

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    public function accounts()
    {
        return $this->hasOne('App\AccountDetails','Code','account_code');
    }

    /**
     * Get the gdn associated with the GeneralLedger
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gdn()
    {
        return $this->hasMany(StockOut::class, 'gdn_no', 'transaction_no');
    }

    /**
     * Get the productVariant associated with the GeneralLedger
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productVariant()
    {
        return $this->hasOne(ProductVariants::class, 'name', 'account_name');
    }
}
