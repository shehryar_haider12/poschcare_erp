<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorAttachment extends Model
{
    protected $table = 'vendor_attachments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'v_id',
        'document'
    ];
    public function vendor()
    {
        return $this->hasOne('App\Vendors','id','v_id');
    }
}
