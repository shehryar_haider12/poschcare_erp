<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostType extends Model
{
    protected $table = 'cost_type';
    protected $primaryKey = 'id';
    protected $fillable = [
        'cost_type',
    ];
}
