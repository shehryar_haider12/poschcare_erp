<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    protected $table = 'counter';
    protected $primaryKey = 'id';
    protected $fillable = [
        'counterNo',
        'open',
        'close',
    ];
    public function salecounter()
    {
        return $this->belongsTo('App\SaleCounter');
    }

}
