<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariants extends Model
{
    protected $table = 'product_variants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'name',
        'status',
        'cost',
        'price', //as mrp
        'tp',
    ];
    public function product()
    {
        return $this->hasOne('App\Products','id','p_id');
    }
    public function currentstocks()
    {
        return $this->hasMany('App\CurrentStock','p_id','id');
    }

}
