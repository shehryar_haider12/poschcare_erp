<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'sub_category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'cat_id',
        's_cat_name',
        'status',
        'created_by',
        'updated_by',
    ];

    public function products()
    {
        return $this->belongsTo('App\Products');
    }

    public function category()
    {
        return $this->hasOne('App\Category','id','cat_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
