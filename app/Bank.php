<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'bank';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'address',
        'branch',
        'c_id',
        'c_no',
        'status',
        'opening_balance',
    ];

    public function city()
    {
        return $this->hasOne('App\City','id','c_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\TransactionHistory');
    }
}
