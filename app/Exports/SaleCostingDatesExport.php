<?php

namespace App\Exports;

use DB;

use App\SaleDetails;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class SaleCostingDatesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithColumnFormatting, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from,$to;
    protected $count = 0 , $quantity = 0 , $price = 0 , $final = 0;

    function __construct($from,$to) {
        $this->from = $from;
        $this->to = $to;
    }



    public function collection()
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });

        $data=[];
        $index = 0;
        $count = 1;
        $s_price = 0;
        $c_price = 0;
        $cost = 0;
        $p_count = 0;
        foreach ($stock as $key => $value)
        {
            foreach ($value as $key2 => $v)
            {
                // if($v->purchase->isEmpty())
                // {
                    $s_price = $v->price * $v->quantity;
                    $this->price += $v->price * $v->quantity;
                    $c_price = trim($v->products->cost) * $v->quantity;
                    $data[$index]['s_no'] = $count;
                    $data[$index]['invoice'] = $v->s_id;
                    $data[$index]['date'] = $key;
                    $data[$index]['customer'] = $v->sale->customer->name;
                    if($v->products->type == 0)
                    {
                        $data[$index]['product'] = $v->products->pro_code.' - '.$v->products->pro_name;
                    }
                    else {
                        $data[$index]['product'] = $v->variant->name;
                    }

                    $data[$index]['quantity'] = $v->quantity;
                    $data[$index]['price'] = $v->price;
                    $data[$index]['s_price'] = $s_price;
                    if($v->products->type == 0)
                    {
                        $data[$index]['cost'] = trim($v->products->cost);
                    }
                    else {
                        $data[$index]['cost'] = $v->variant->cost;
                    }
                    $data[$index]['c_price'] = $c_price;
                    $data[$index]['pl'] = $s_price - $c_price;
                    $this->final += $s_price - $c_price;
                    $this->quantity+=$v->quantity;

                // }
                // else
                // {
                //     $p_count = count($v->purchase);
                //     foreach ($v->purchase as $key2 => $c) {
                //         $cost += $c->cost;
                //     }
                //     $s_price = $v->price * $v->quantity;
                //     $this->price += $v->price * $v->quantity;
                //     $c_price = ($cost / $p_count) * $v->quantity;
                //     $data[$index]['s_no'] = $count;
                //     $data[$index]['invoice'] = $v->s_id;
                //     $data[$index]['date'] = $key;
                //     $data[$index]['customer'] = $v->sale->customer->name;
                //     if($v->products->type == 0)
                //     {
                //         $data[$index]['product'] = $v->products->pro_code.' - '.$v->products->pro_name;
                //     }
                //     else {
                //         $data[$index]['product'] = $v->variant->name;
                //     }
                //     $data[$index]['quantity'] = $v->quantity;
                //     $data[$index]['price'] = $v->price;
                //     $data[$index]['s_price'] = $s_price;
                //     $data[$index]['cost'] = $cost / $p_count;
                //     $data[$index]['c_price'] = $c_price;
                //     $data[$index]['pl'] = $s_price - $c_price;
                //     $this->final += $s_price - $c_price;
                //     $this->quantity+=$v->quantity;

                // }

                $count++;
                $index++;
                $s_price = 0;
                $cost = 0;
                $p_count = 0;
                $c_price = 0;
            }
        }
        $this->count = $count;
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            [' PROFIT/LOSS STATEMENT'],
            ['(From '.$this->from.' To '.$this->to.')'],
            [],
            [
                'S.NO',
                'INVOICE#',
                'DATE',
                'CUSTOMER',
                'PRODUCT NAME',
                'QUANTITY',
                'SALE PRICE',
                'SALE AMOUNT',
                'COST PRICE',
                'COST AMOUNT',
                'PROFIT/LOSS'
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {

                $cellRange = 'A1:K1'; // All headers
                $event->sheet->mergeCells('A1:K1');
                $cellRange1 = 'A3:K3'; // All headers
                $last_row = $this->count + 3;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->setCellValue(sprintf('F%d',$last_row),$this->quantity);
                $event->sheet->setCellValue(sprintf('H%d',$last_row),$this->price);
                $event->sheet->setCellValue(sprintf('K%d',$last_row),$this->final);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);

                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':K'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }

            },
        ];

    }

    public function columnFormats(): array
    {
        return [

            'K' => NumberFormat::FORMAT_NUMBER_NEGATIVE,
            'G' => NumberFormat::FORMAT_NUMBER_NEGATIVE,
            'H' => NumberFormat::FORMAT_NUMBER_NEGATIVE,
            'I' => NumberFormat::FORMAT_NUMBER_NEGATIVE,
            'J' => NumberFormat::FORMAT_NUMBER_NEGATIVE,
            'F' => NumberFormat::FORMAT_NUMBER_NEGATIVE

        ];

    }
}
