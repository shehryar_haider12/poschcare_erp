<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class VoucherExportStructure implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data=[];
        $index = 0;
        $data[$index]['date'] = '31/12/2021';
        $data[$index]['hd'] = 'Marketing/ Admin/ Utility';
        $data[$index]['ad'] = "Conveyance Exp";
        $data[$index]['hc'] = 'Cash/ Bank';
        $data[$index]['ac'] = "Cash In Hand/ meezan bank - bahadrabad (Bank name - Branch)";
        $data[$index]['desc'] = '(Write Description)';
        $data[$index]['amount'] = '12000';
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['JOURNAL VOUCHER STRUCTURE'],
            ['Note: Do not use comma or space in Dr.'],
            ["Note: write same heads with correct spelling which are available in software."],
            ['Note: Write account names with correct spellings if they are available in software.'],
            ['Note: Else write other names with correct spelling'],
            ['Note: Remove all the note rows before uploading excel sheet'],
            [],
            ['Date',
            'head categoryD',
            'A/d',
            'head categoryC',
            'A/c',
            'Particular',
            'Dr']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange1 = 'A1:G1'; // All headers
                $cellRange2 = 'A2:G2'; // All headers
                $cellRange3 = 'A3:G3'; // All headers
                $cellRange4 = 'A4:G4'; // All headers
                $cellRange5 = 'A5:G5'; // All headers
                $cellRange6 = 'A6:G6'; // All headers
                $cellRange7 = 'A8:G8'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange2)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange3)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange4)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange5)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange6)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange7)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);

                $event->sheet->mergeCells($cellRange1);
                $event->sheet->mergeCells($cellRange2);
                $event->sheet->mergeCells($cellRange3);
                $event->sheet->mergeCells($cellRange4);
                $event->sheet->mergeCells($cellRange5);
                $event->sheet->mergeCells($cellRange6);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getFont()->setSize(14);



                $event->sheet->getDelegate()->getStyle($cellRange7)->getFont()->setSize(11);
                // for ($i=3; $i < $last_row ; $i++) {
                //     // $event->sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
                //     $event->sheet->getStyle('A'.$i.':W'.$i)->applyFromArray([
                //         'borders' => [
                //             'allBorders' => [
                //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                 'color' => ['argb' => '000000'],
                //             ],
                //         ],
                //     ]);
                // }
            },
        ];
    }
}
