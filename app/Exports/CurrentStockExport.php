<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\CurrentStock;
use Carbon\Carbon;

class CurrentStockExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        $stock=CurrentStock::with(['warehouse','stock','sales','products.brands','products.category','products.unit','products','variant.product.category','variant.product.unit','variant.product.brands'])
        ->get();
        // dd($stock);
        $data=[];
        $index = 0;
        $a = 1;
        $cost = 0 ;
        $count = 0 ;
        $price = 0 ;
        $price1 = 0 ;
        $count1 = 0 ;
        foreach ($stock as $key => $s) {
            $data[$index]['s_no'] = $a;
            $data[$index]['w_name'] = $s->warehouse->w_name;
            $data[$index]['w_type'] = $s->warehouse->w_type;
            $data[$index]['pro_name'] = $s->type == 0 ? $s->products->pro_code.' - '.$s->products->pro_name : $s->variant->name;
            $data[$index]['b_name'] = $s->type == 0 ? $s->products->brands->b_name : $s->variant->product->brands->b_name;
            $data[$index]['cat_name'] = $s->type == 0 ? $s->products->category->cat_name : $s->variant->product->category->cat_name;
            $data[$index]['quantity'] = $s->quantity;
            if ($s->stock->isEmpty()) {
                $data[$index]['cost'] = $s->type == 0 ? $s->products->cost : $s->variant->cost;
            }
            else {

                $count = count($s->stock);
                foreach ($s->stock as $key1 => $p) {
                    $cost+=$p->cost;
                }
                $data[$index]['cost'] = round($cost/$count);
            }

            if ($s->sales->isEmpty())
            {
                $price1 = $s->type == 0 ? $s->products->price : $s->variant->price;
            }
            else
            {
                $count1 = count($s->sales);
                foreach ($s->sales as $key2 => $t) {
                    $price+=$t->price;
                }
                $price1 = round($price/$count1);
            }
            $data[$index]['price'] = $price1;
            $a++;
            $index++;
            $count = 0 ;
            $count1 = 0 ;
            $price = 0 ;
            $price1 = 0 ;
            $cost = 0 ;
            $this->count++;
        }
        return collect($data);
    }

    public function headings(): array
    {
        $year = Carbon::now()->format('Y');
        return
        [
            ['CURRENT STOCK REPORT '],
            [],
            ['S.NO',
            'WAREHOUSE NAME',
            'WAREHOUSE TYPE',
            'PRODUCT CODE',
            'PRODUCT NAME',
            'BRAND',
            'CATEGORY',
            'QUANTITY',
            'COST',
            'PRICE']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange1 = 'A3:J3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
