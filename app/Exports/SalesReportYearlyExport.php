<?php

namespace App\Exports;

use App\SaleDetails;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;

class SalesReportYearlyExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $count = 0;
    function __construct($year,$c_id,$sp_id,$p,$types,$w,$check) {
        $this->sp_id = $sp_id;
        $this->c_id = $c_id;
        $this->p = $p;
        $this->w = $w;
        $this->check = $check;
        $this->year = $year;
        $this->types = $types;
    }

    public function collection()
    {
        if($this->check == 0)
        {
            $saledetail = SaleDetails::with(['sale' => function($query) {
                $avg = $query->select('id','sale_date','w_id','c_id','sp_id')->get();
            },'sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 1)
        {
            $saledetail = SaleDetails::with(['sale' => function($query) {
                $avg = $query->select('id','sale_date','w_id','c_id','sp_id')->get();
            },'sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 2)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 3)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 4)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('sp_id',$this->sp_id);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 5)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 6)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id)->where('sp_id',$this->sp_id);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 7)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id)->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 8)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('sp_id',$this->sp_id)->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 9)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('sp_id',$this->sp_id);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 10)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check ==11)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('sp_id',$this->sp_id)->where('c_id',$this->c_id);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 12)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id)->where('sp_id',$this->sp_id)->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 13)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id)->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }

        if($this->check == 14)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('sp_id',$this->sp_id)->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }


        if($this->check == 15)
        {
            $saledetail = SaleDetails::whereHas('sale', function ($query) {
                $query->where('c_id',$this->c_id)->where('sp_id',$this->sp_id)->where('w_id',$this->w);
            })->with(['sale.warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },
            'sale.customer' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'sale.saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            },
            'products' => function($query) {
                $avg = $query->select('id','pro_name')->get();
            },
            'variant' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$this->year)
            ->where('p_id',$this->p)
            ->where('type',$this->types)
            ->select('id','s_id','p_id','price','quantity','type')
            ->get();
        }
        $this->count = count($saledetail);
        // dd($saledetail);
        return $saledetail;
    }

    public function map($saledetail): array
    {
        // dd($saledetail);
        if ($saledetail->type == 0) {
            return [
                $saledetail->id,
                $saledetail->s_id,
                $saledetail->sale->sale_date,
                $saledetail->sale->customer->name,
                $saledetail->products->pro_name,
                $saledetail->quantity,
                $saledetail->price,
                $saledetail->price * $saledetail->quantity,
                $saledetail->sale->warehouse->w_name,
                $saledetail->sale->saleperson->name
            ];
        }
        else {
            return [
                $saledetail->id,
                $saledetail->s_id,
                $saledetail->sale->sale_date,
                $saledetail->sale->customer->name,
                $saledetail->variant->name,
                $saledetail->quantity,
                $saledetail->price,
                $saledetail->price * $saledetail->quantity,
                $saledetail->sale->warehouse->w_name,
                $saledetail->sale->saleperson->name
            ];
        }

    }

    public function headings(): array
    {
        return
        [
            ['SALES REPORT'],
            ['Year '.$this->year],
            [],
            ['S.NO',
            'INVOICE.NO',
            'SALE DATE',
            'CUSTOMER',
            'PRODUCT NAME',
            'QUANTITY',
            'PRICE',
            'AMOUNT',
            'WAREHOUSE',
            'SALE PERSON',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange2 = 'A2:J2'; // All headers
                $cellRange1 = 'A4:J4'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->mergeCells($cellRange2);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=4; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
