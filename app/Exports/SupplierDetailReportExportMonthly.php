<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use DB;
use App\PurchaseOrder;
use App\Vendors;

class SupplierDetailReportExportMonthly implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    function __construct($month,$id) {
        $this->id = $id;
        $this->month = $month;
    }

    public function collection()
    {
        $purchase = PurchaseOrder::where('s_id',$this->id)
        ->with(['warehouse','supplier','biller'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
        }])
        ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$this->month)
        ->get();
        $data=[];
        $count =1;
        $index = 0;
        foreach ($purchase as $key => $b) {
            $data[$index]['s_no'] = $count;
            $data[$index]['date'] = $b->order_date;
            $data[$index]['ono'] = $b->id;
            $data[$index]['biller'] = $b->biller->name;
            $data[$index]['total'] = $b->total;
            $data[$index]['paid'] = $b->total_amount == null ? '0' : $b->total_amount;
            $data[$index]['balance'] = $b->total - ($b->total_amount == null ? '0' : $b->total_amount);
            $data[$index]['ss'] = $b->status;
            $data[$index]['ps'] = $b->p_status;
            $count++;
            $index++;
        }
        $this->count = count($purchase);
        return collect($data);
    }

    public function headings(): array
    {
        $supplier = Vendors::find($this->id);
        return
        [
            ['Supplier('.$supplier->name.') Detailed Report'],
            [],
            ['S.NO',
            'DATE',
            'ORDER NO',
            'BILLER',
            'TOTAL',
            'PAID',
            'BALANCE',
            'ORDER STATUS',
            'PAYMENT STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:I1'; // All headers
                $cellRange1 = 'A3:I3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':I'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
