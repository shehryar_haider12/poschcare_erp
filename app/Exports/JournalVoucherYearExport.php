<?php

namespace App\Exports;

use App\JournalVoucher;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use DB;
use Carbon\Carbon;

class JournalVoucherYearExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    function __construct($year,$acc,$check) {
        $this->acc = $acc;
        $this->year = $year;
        $this->check = $check;
    }

    protected $count = 0;

    public function collection()
    {
        $data=[];
        $index = 0;
        $count = 1;

        if($this->check == 0)
        {
            $voucher     =   JournalVoucher::with(['acode1'])
            ->where(DB::raw("(DATE_FORMAT(date,'%Y'))"),$this->year)
            ->get();
        }
        else
        {
            $voucher     =   JournalVoucher::with(['acode1'])
            ->where(DB::raw("(DATE_FORMAT(date,'%Y'))"),$this->year)
            ->where('account1',$this->acc)->get();
        }


        foreach ($voucher as $key => $b) {
            $data[$index]['s_no'] = $count;
            $data[$index]['date'] = Carbon::parse($b->date)->format('d-m-Y');
            $data[$index]['debit'] = $b->acode1->name_of_account;
            $data[$index]['description'] = $b->description;
            $data[$index]['amount'] = $b->amount;
            $data[$index]['status'] = $b->status;
            $count++;
            $index++;
        }
        $this->count = count($voucher);
        return collect($data);
    }
    public function headings(): array
    {
        return
        [
            ['JOURNAL VOUCHERS '.$this->year],
            [],
            ['S.NO',
            'DATE',
            'A/C',
            'DESCRIPTION',
            'AMOUNT',
            'STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:F1'; // All headers
                $cellRange1 = 'A3:F3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    // $event->sheet->getStyle('F'.$i)->getAlignment()->setWrapText(true);
                    $event->sheet->getStyle('A'.$i.':F'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
