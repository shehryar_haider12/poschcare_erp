<?php

namespace App\Exports;

use App\Products;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class FinishProductExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        $data=[];
        $index = 0;
        $count = 1;
        $product =  DB::table('products')
        ->join('brands','brands.id','products.brand_id')
        ->join('category','category.id','products.cat_id')
        ->join('sub_category','sub_category.id','products.s_cat_id')
        ->select('products.pro_name','products.pro_code','products.weight','products.cost',
        'products.price','brands.b_name','category.cat_name','sub_category.s_cat_name',
        'products.status')
        ->where('p_type', 'Finished')
        ->get();
        foreach ($product as $key => $b) {
            $data[$index]['s_no'] = $count;
            $data[$index]['PRODUCT'] = $b->pro_name;
            $data[$index]['CODE'] = $b->pro_code;
            $data[$index]['WEIGHT'] = $b->weight;
            $data[$index]['COST'] = $b->cost;
            $data[$index]['PRICE'] = $b->price;
            $data[$index]['BRAND'] = $b->b_name;
            $data[$index]['CATEGORY'] = $b->cat_name;
            $data[$index]['SUB'] = $b->s_cat_name;
            $data[$index]['STATUS'] = $b->status;
            $count++;
            $index++;
        }
        $this->count = count($product);
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['FINISH PRODUCTS LIST'],
            [],
            ['S.NO',
            'PRODUCT NAME',
            'CODE',
            'WEIGHT',
            'COST',
            'PRICE',
            'BRAND NAME',
            'CATEGORY NAME',
            'SUB-CATEGORY NAME',
            'STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange1 = 'A3:J3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
