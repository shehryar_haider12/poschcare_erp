<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;

class incomeStatementExport implements FromArray, WithMapping,WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array():array
    {
        $data = array('sales' => '1');
        return ($data);
    }


    public function map($data): array
    {
        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales'");
        $opening_stock = '0';

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase'");

        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%'");

        $cost_of_goods = ($opening_stock + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;
        // dd($data);
        // This example will return 3 rows.
        // First row will have 2 column, the next 2 will have 1 column
        $map = [
            [
                'INCOME STATEMENT'
            ],
            [
                'Sales',
                '-',
                $sales[0]->credit
            ],
            [
                'Opening Stock',
                $opening_stock,
                '-',
            ],
            [
                'Purchases',
                $purchase[0]->debit,
                '-',
            ],
            [
                'Closing Stock',
                '('.$closing_stock[0]->closing.')',
                '-'
            ],
            [
                'Cost of Goods',
                '-',
                '('.$cost_of_goods.')'
            ],
            [
                'Gross Loss/Profit',
                '-',
                $pnl,
            ]
        ];
         return $map;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange3 = 'A1:C1'; // All headers
                $cellRange = 'A2:C2'; // All headers
                $cellRange1 = 'A6:C6'; // All headers
                $cellRange2 = 'A7:C7'; // All headers
                $cellRange4 = 'A3:C3'; // All headers
                $cellRange5 = 'A4:C4'; // All headers
                $cellRange6 = 'A5:C5'; // All headers
                $event->sheet->mergeCells('A1:C1');
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setBold(true);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            },
        ];
    }

}
