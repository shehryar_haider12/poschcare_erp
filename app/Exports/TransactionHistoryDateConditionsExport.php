<?php

namespace App\Exports;

use DB;

use App\TransactionHistory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class TransactionHistoryDateConditionsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0 ;

    function __construct($index,$tf,$pb) {
        $this->index = $index;
        $this->tf = $tf;
        $this->pb = $pb;
    }

    public function collection()
    {
        $data=[];
        $index1 = 0;
        $a = 1;
        if($this->index == 5)
        {
            if($this->tf == 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf == 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
        }
        if($this->index == 6)
        {
            $date = Carbon::today()->subDays(7);
            if($this->tf == 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where('created_at','>=',$date)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf == 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where('created_at','>=',$date)
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where('created_at','>=',$date)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where('created_at','>=',$date)
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
        }
        if($this->index == 7)
        {
            $date = Carbon::today()->subDays(15);
            if($this->tf == 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where('created_at','>=',$date)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf == 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where('created_at','>=',$date)
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where('created_at','>=',$date)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where('created_at','>=',$date)
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
        }
        if($this->index == 8)
        {
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            if($this->tf == 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf == 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb == 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
                ->orderBy('created_at','desc')
                ->get();
            }
            if($this->tf != 0 && $this->pb != 0)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$this->tf)
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
                ->where('paid_by',$this->pb)
                ->orderBy('created_at','desc')
                ->get();
            }
        }
        foreach ($trans as $key => $b) {
            $data[$index1]['s_no'] = $a;
            $data[$index1]['INVOICE'] = $b->id;
            if($b->p_type == 'Sales')
            {
                $data[$index1]['NAME'] = $b->sales->customer->name;
            }
            else
            {
                $data[$index1]['NAME'] = $b->purchase->supplier->name;
            }
            $data[$index1]['TRANSACTION FROM'] = $b->p_type;
            $data[$index1]['TRANSACTION TYPE'] = $b->t_type;
            $data[$index1]['DATE'] = Carbon::parse($b->created_at)->format('d-m-Y');
            $data[$index1]['PAID BY'] = $b->paid_by;
            $data[$index1]['AMOUNT'] = $b->total;
            $a++;
            $index1++;
        }

        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['TRANSACTION HISTORY'],
            [],
            ['S.NO',
            'INVOICE NO',
            'NAME',
            'TRANSACTION FROM',
            'TRANSACTION TYPE',
            'DATE',
            'PAID BY',
            'AMOUNT']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:H1'; // All headers
                $cellRange1 = 'A3:H3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':H'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
