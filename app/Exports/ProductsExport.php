<?php

namespace App\Exports;

use App\Products;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class ProductsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        // $product =  Products::with(['brands'=>function($brand){
        //     $brand->select(['id','b_name']);
        // },'category'=>function($category){
        //     $category->select(['id','cat_name']);
        // },'subcategory'=>function($subcategory){
        //     $subcategory->select(['id','s_cat_name']);
        // },'warehouse'=>function($warehouse){
        //     $warehouse->select(['id','w_name']);
        // }])->select(['brand_id','cat_id','s_cat_id','w_id','pro_name','pro_code','weight','cost', 'price','status'])->get();

        // return $product;
        // dd($product);
        $data=[];
        $index = 0;
        $count = 1;
        $varinats = '';
        $product=Products::with(['brands','unit','category','subcategory','variants'])
        // ->doesntHave('finish.rawproducts')
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'));
        }])
        ->withCount(['currentstocks as unit_total' => function($query) {
            $avg = $query->select(DB::raw('sum(unit_quantity)'));
        }])
        ->get();
        // dd($product);
        foreach ($product as $key => $b) {
            $data[$index]['s_no'] = $count;
            $data[$index]['CODE'] = $b->pro_code;
            $data[$index]['PRODUCT'] = $b->pro_name;
            $data[$index]['TYPE'] = $b->p_type;
            if(count($b->variants) == 0)
            {
                $data[$index]['VARIANTS'] = 'No Variants';
            }
            else {
                for ($i=0; $i <count($b->variants) ; $i++) {
                    $varinats .= $b->variants[$i]->name."\n";
                }
                $data[$index]['VARIANTS'] = $varinats;
            }
            $data[$index]['BRAND'] = $b->brands->b_name;
            $data[$index]['CATEGORY'] = $b->category->cat_name;
            $data[$index]['SUB'] = $b->subcategory->s_cat_name;
            $data[$index]['COST'] = $b->cost;
            $data[$index]['TP'] = $b->tp;
            $data[$index]['MRP'] = $b->mrp;
            $data[$index]['STATUS'] = $b->status;
            $count++;
            $index++;
            $varinats = '';

        }
        $this->count = count($product);
        return collect($data);

    }
    public function headings(): array
    {
        return
        [
            ['PRODUCTS LIST'],
            [],
            ['S.NO',
            'CODE',
            'PRODUCT NAME',
            'TYPE',
            'VARIANTS',
            'BRAND NAME',
            'CATEGORY NAME',
            'SUB-CATEGORY NAME',
            'COST',
            'TP',
            'MRP',
            'STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:L1'; // All headers
                $cellRange1 = 'A3:L3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
                    $event->sheet->getStyle('A'.$i.':L'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }


}
