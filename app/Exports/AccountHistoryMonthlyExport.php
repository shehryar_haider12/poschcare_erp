<?php

namespace App\Exports;

use DB;

use App\GeneralLedger;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class AccountHistoryMonthlyExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    function __construct($month,$id) {
        $this->id = $id;
        $this->month = $month;
    }

    public function collection()
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code',$this->code)
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$this->month)
        ->get();
        $data=[];
        $count =1;
        $index = 0;
        if($this->id != 'CA-02' && $this->id != 'EXP-05')
        {
            foreach ($ledger as $key => $a) {
                $gl = GeneralLedger::where('link_id',$a->link_id)
                ->where('account_code','!=',$a->account_code)
                ->first();
                $data[$index]['id'] = $count;
                $data[$index]['desc'] = $a->description;
                $data[$index]['adate'] = $a->accounting_date;
                $data[$index]['acc'] = $gl == null ? '-' : $gl->account_name;
                $data[$index]['pdate'] = $a->posted_date;
                $data[$index]['period'] = $a->period;
                $data[$index]['refno'] = $a->transaction_no;
                $data[$index]['debit'] = $a->debit;
                $data[$index]['credit'] = $a->credit;
                $data[$index]['net'] = $a->net_value;
                $data[$index]['blnc'] = $a->balance;
                $count++;
                $index++;
            }
            $this->count = $count;
        }
        if($this->id == 'CA-02')
        {
            foreach ($ledger as $key => $a) {
                $gl = GeneralLedger::where('link_id',$a->link_id)
                ->where('account_code','!=',$a->account_code)
                ->first();
                $data[$index]['id'] = $count;
                $data[$index]['desc'] = $a->description;
                $data[$index]['adate'] = $a->accounting_date;
                $data[$index]['acc'] = $gl == null ? '-' : $gl->account_name;
                $data[$index]['pdate'] = $a->posted_date;
                $data[$index]['period'] = $a->period;
                $data[$index]['refno'] = $a->transaction_no;
                $data[$index]['debit'] = $a->debit;
                $data[$index]['credit'] = $a->credit;
                $data[$index]['sin'] = $a->stock_in;
                $data[$index]['sout'] = $a->stock_out;
                $data[$index]['net'] = $a->net_value;
                $data[$index]['blnc'] = $a->balance;
                $data[$index]['amount'] = $a->amount;
                $count++;
                $index++;
            }
            $this->count = $count;
        }
        if($this->id == 'EXP-05')
        {
            foreach ($ledger as $key => $a) {
                $gl = GeneralLedger::where('link_id',$a->link_id)
                ->where('account_code','!=',$a->account_code)
                ->first();
                $data[$index]['id'] = $count;
                $data[$index]['desc'] = $a->description;
                $data[$index]['adate'] = $a->accounting_date;
                $data[$index]['acc'] = $gl == null ? '-' : $gl->account_name;
                $data[$index]['pdate'] = $a->posted_date;
                $data[$index]['period'] = $a->period;
                $data[$index]['refno'] = $a->transaction_no;
                $data[$index]['debit'] = $a->debit;
                $data[$index]['credit'] = $a->credit;
                $data[$index]['net'] = $a->net_value;
                $data[$index]['blnc'] = $a->balance;
                $data[$index]['amount'] = $a->amount;
                $count++;
                $index++;
            }

            $this->count = $count;
        }
        return collect($data);
    }

    public function headings(): array
    {
        if($this->id != 'CA-02' && $this->id != 'EXP-05')
        {
            return
            [
                ['LEDGER HISTORY'],
                [],
                [],
                [
                    'S.NO',
                    'DESCRIPTION',
                    'ACCOUNTING DATE',
                    'AGAINST ACCOUNT',
                    'POSTED DATE',
                    'PERIOD',
                    'REFERENCE NO',
                    'DEBIT',
                    'CREDIT',
                    'NET VALUE',
                    'BALANCE'
                ]
            ];
        }
        if($this->id == 'CA-02')
        {
            return
            [
                ['LEDGER HISTORY'],
                [],
                [],
                [
                    'S.NO',
                    'DESCRIPTION',
                    'ACCOUNTING DATE',
                    'AGAINST ACCOUNT',
                    'POSTED DATE',
                    'PERIOD',
                    'REFERENCE NO',
                    'DEBIT',
                    'CREDIT',
                    'STOCK IN',
                    'STOCK OUT',
                    'NET VALUE',
                    'BALANCE',
                    'AMOUNT'
                ]
            ];
        }
        if($this->id == 'EXP-05')
        {
            return
            [
                ['LEDGER HISTORY'],
                [],
                [],
                [
                    'S.NO',
                    'DESCRIPTION',
                    'ACCOUNTING DATE',
                    'AGAINST ACCOUNT',
                    'POSTED DATE',
                    'PERIOD',
                    'REFERENCE NO',
                    'DEBIT',
                    'CREDIT',
                    'NET VALUE',
                    'BALANCE',
                    'AMOUNT'
                ]
            ];
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {

                $cellRange = 'A1:N1'; // All headers
                $cellRange2 = 'A2:N2'; // All headers
                $cellRange3 = 'A4:N4'; // All headers
                $event->sheet->mergeCells($cellRange);
                $event->sheet->mergeCells($cellRange2);
                // $last_row = $this->count + 3;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange2)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange3)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);

                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);


                for ($i=4; $i <= $this->count+3 ; $i++) {
                    $event->sheet->getStyle('A'.$i.':N'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }

            },
        ];

    }
}
