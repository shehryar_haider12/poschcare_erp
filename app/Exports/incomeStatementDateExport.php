<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Carbon\Carbon;

class incomeStatementDateExport implements FromArray, WithMapping,WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $from,$to;

    function __construct($from,$to) {
        $this->from = $from;
        $this->to = $to;
    }

    public function array():array
    {
        $data = array('sales' => '1');
        return ($data);
    }

    public function map($data): array
    {
        $from = $this->from;
        $to = $this->to;
        $Lastmonth = date("Y-m",strtotime($from));
        $monthNumber =  Carbon::parse($Lastmonth)->firstOfMonth();
        $SecondlastMonth =  $monthNumber->subMonth()->format('Y-m');
        $date1 = $SecondlastMonth.'-26';
        $date2 = $Lastmonth.'-25';
        // dd($date1,$date2);

        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales' and
        accounting_date between '".$from."' and '".$to."'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase' and
        accounting_date between '".$from."' and '".$to."'");

        $opening_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as opening
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        accounting_date between '".$date1."' and '".$date2."'");

        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%' and account_code not like '%NCA-02%' and
        accounting_date between '".$from."' and '".$to."'");

        $cost_of_goods = ($opening_stock[0]->opening + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;

        $map = [
            [
                'INCOME STATEMENT from '.$from.' to '.$to
            ],
            [
                'Sales',
                '-',
                $sales[0]->credit
            ],
            [
                'Opening Stock',
                $opening_stock[0]->opening,
                '-',
            ],
            [
                'Purchases',
                $purchase[0]->debit,
                '-',
            ],
            [
                'Closing Stock',
                '('.$closing_stock[0]->closing.')',
                '-'
            ],
            [
                'Cost of Goods',
                '-',
                '('.$cost_of_goods.')'
            ],
            [
                'Gross Loss/Profit',
                '-',
                $pnl,
            ]
        ];
         return $map;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange3 = 'A1:C1'; // All headers
                $cellRange = 'A2:C2'; // All headers
                $cellRange1 = 'A6:C6'; // All headers
                $cellRange2 = 'A7:C7'; // All headers
                $cellRange4 = 'A3:C3'; // All headers
                $cellRange5 = 'A4:C4'; // All headers
                $cellRange6 = 'A5:C5'; // All headers
                $event->sheet->mergeCells('A1:C1');
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setBold(true);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            },
        ];
    }
}
