<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class ReceivablesExportStructure implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data=[];
        $index = 0;
        $data[$index]['cname'] = 'Waqas';
        $data[$index]['company'] = "Vicky's cosmetics";
        $data[$index]['date'] = '25/12/2021';
        $data[$index]['amount'] = '12000';
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['RECEIVABLES OPENING ACCOUNT STRUCTURE'],
            ['Note: Remove all the note rows before uploading excel sheet'],
            ["Note: Do not use comma or space in amount. Do not use `-` in customer and company name"],
            ['Note: Do not repeat the customer and company name which are already in software'],
            [],
            ['Customer Name',
            'company',
            'opening date',
            'Amount']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange1 = 'A1:H1'; // All headers
                $cellRange2= 'A2:H2'; // All headers
                $cellRange3 = 'A3:H3'; // All headers
                $cellRange4 = 'A4:H4'; // All headers
                $cellRange5 = 'A6:D6'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange2)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange3)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange4)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange5)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);

                $event->sheet->mergeCells($cellRange1);
                $event->sheet->mergeCells($cellRange2);
                $event->sheet->mergeCells($cellRange3);
                $event->sheet->mergeCells($cellRange4);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getFont()->setSize(12);



                $event->sheet->getDelegate()->getStyle($cellRange5)->getFont()->setSize(11);
                // for ($i=3; $i < $last_row ; $i++) {
                //     // $event->sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
                //     $event->sheet->getStyle('A'.$i.':W'.$i)->applyFromArray([
                //         'borders' => [
                //             'allBorders' => [
                //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                 'color' => ['argb' => '000000'],
                //             ],
                //         ],
                //     ]);
                // }
            },
        ];
    }
}
