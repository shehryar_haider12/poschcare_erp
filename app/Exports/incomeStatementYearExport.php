<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Carbon\Carbon;

class incomeStatementYearExport implements FromArray, WithMapping,WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $year;

    function __construct($year) {
        $this->year = $year;
    }
    public function array():array
    {
        $data = array('sales' => '1');
        return ($data);
    }

    public function map($data): array
    {
        $year = $this->year;
        $a = Carbon::parse('Mar-'.$year);
        $year_p = $a->copy()->subYears(1)->format('Y');

        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales' and
        DATE_FORMAT(created_at,'%Y') = '".$this->year."'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase' and
        DATE_FORMAT(created_at,'%Y') = '".$this->year."'");

        $opening_s = DB::select("SELECT (sum(debit) - sum(credit))
        as opening
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        DATE_FORMAT(created_at,'%Y') = '".$year_p."'");

        if($opening_s[0]->opening == null)
        {
            $opening_stock = 0;
        }
        else
        {
            $opening_stock = $opening_s[0]->opening;
        }


        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        DATE_FORMAT(created_at,'%Y') = '".$this->year."'");

        $cost_of_goods = ($opening_stock + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;

        $map = [
            [
                'INCOME STATEMENT '.$year
            ],
            [
                'Sales',
                '-',
                $sales[0]->credit
            ],
            [
                'Opening Stock',
                $opening_stock,
                '-',
            ],
            [
                'Purchases',
                $purchase[0]->debit,
                '-',
            ],
            [
                'Closing Stock',
                '('.$closing_stock[0]->closing.')',
                '-'
            ],
            [
                'Cost of Goods',
                '-',
                '('.$cost_of_goods.')'
            ],
            [
                'Gross Loss/Profit',
                '-',
                $pnl,
            ]
        ];
         return $map;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange3 = 'A1:C1'; // All headers
                $cellRange = 'A2:C2'; // All headers
                $cellRange1 = 'A6:C6'; // All headers
                $cellRange2 = 'A7:C7'; // All headers
                $cellRange4 = 'A3:C3'; // All headers
                $cellRange5 = 'A4:C4'; // All headers
                $cellRange6 = 'A5:C5'; // All headers
                $event->sheet->mergeCells('A1:C1');
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setBold(true);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            },
        ];
    }
}
