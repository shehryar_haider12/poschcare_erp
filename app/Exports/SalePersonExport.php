<?php

namespace App\Exports;

use App\Vendors;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class SalePersonExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        $data=[];
        $index = 0;
        $count = 1;
        $person = Vendors::where('v_type','Saleperson')
        ->select('name','address','c_no','email')
        ->get();
        foreach ($person as $key => $b) {
            $data[$index]['s_no'] = $count;
            $data[$index]['NAME'] = $b->name;
            $data[$index]['ADDRESS'] = $b->address;
            $data[$index]['CONTACT NO'] = $b->c_no;
            $data[$index]['EMAIL'] = $b->email;
            $count++;
            $index++;
        }
        $this->count = count($person);
        return collect($data);
    }
    public function headings(): array
    {
        return
        [
            ['BILLERS INFORMATION'],
            [],
            [
                'S.NO',
                'NAME',
                'ADDRESS',
                'CONTACT NO',
                'EMAIL'
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:E1'; // All headers
                $cellRange1 = 'A3:E3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':E'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
