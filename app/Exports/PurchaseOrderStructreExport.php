<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class PurchaseOrderStructreExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data=[];
        $index = 0;
        $data[$index]['date'] = '20/12/2021';
        $data[$index]['supplier'] = 'Fusion Cosmeceuticals';
        $data[$index]['company'] = 'Fusion Cosmeceuticals';
        $data[$index]['pay date'] = '30/12/2021';
        $data[$index]['status'] = 'Pending/ Approved/ Partial/ Received';
        $data[$index]['remarks'] = 'anything';
        $data[$index]['payment status'] = 'Pending/ Partial/ Paid';
        $data[$index]['tax status'] = 'Yes/ No';
        $data[$index]['tax'] = '(In percentage)';
        $data[$index]['expected date'] = '30/12/2021';
        $data[$index]['payment mode'] = 'Credit/ Cash on Delivery';
        $data[$index]['brand'] = 'No Brand';
        $data[$index]['category'] = 'No Category';
        $data[$index]['unit'] = 'Pieces';
        $data[$index]['subcategory'] = 'No Subcategory';
        $data[$index]['ptype'] = 'Raw/ Material/ Packaging/ Finished';
        $data[$index]['product name'] = 'Empty HDPE Can';
        $data[$index]['variant'] = '';
        $data[$index]['size'] = '5000ml';
        $data[$index]['color'] = '';
        $data[$index]['product code'] = '2067263049';
        $data[$index]['cost'] = 5;
        $data[$index]['order qty'] = 10;
        $data[$index]['subtotal'] = 50;
        $data[$index]['rcv qty'] = 10;
        return collect($data);
    }
    public function headings(): array
    {
        return
        [
            ['PURCHASE ORDER STRUCTURE'],
            ['Note: Kindly use the supplier and its company name from software with correct spelling.'],
            ['Note: If it is not available in the software then write correct name and company of supplier'],
            ['Note: Use all statuses same as given in the sheet'],
            ['Note: Write dates in the format given in the sheet'],
            ['Note: write pay date if payment mode is Credit'],
            ['Note: Write tax in %, if tax status is Yes'],
            ['Note: write product details (brand, category, unit, subcategory, ptype, name, codes) same as available in the software. If the product is not in the software product list then write each details with correct information. '],
            ['Note: if the product unit is in liter, mililiter, grams or kilograms; write per unit price and quantity as how many liters do you want?'],
            ['Note: if a product has multiple sizes, variant and color. Check the product list for examples and then write new names if they are not available.'],
            ['Note: Remove all the note rows before uploading excel sheet'],
            [],
            ['Date',
            'supplier',
            'company',
            'pay date',
            'status',
            'remarks',
            'payment status',
            'tax status',
            'tax',
            'expected date',
            'payment mode',
            'brand',
            'category',
            'unit',
            'subcategory',
            'ptype',
            'product name',
            'variant',
            'size',
            'color',
            'product code',
            'subtotal',
            'cost',
            'order qty',
            'rcv qty']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange1 = 'A1:K1'; // All headers
                $cellRange2= 'A2:K2'; // All headers
                $cellRange3 = 'A3:K3'; // All headers
                $cellRange4 = 'A4:K4'; // All headers
                $cellRange5 = 'A5:K5'; // All headers
                $cellRange6 = 'A6:K6'; // All headers
                $cellRange7 = 'A7:K7'; // All headers
                $cellRange8 = 'A8:K8'; // All headers
                $cellRange9 = 'A9:K9'; // All headers
                $cellRange10 = 'A10:K10'; // All headers
                $cellRange11 = 'A11:K11'; // All headers
                $cellRange12 = 'A13:Y13'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange2)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange3)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange4)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange5)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange6)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange7)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange8)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange9)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange10)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange11)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange12)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange1);
                $event->sheet->mergeCells($cellRange2);
                $event->sheet->mergeCells($cellRange3);
                $event->sheet->mergeCells($cellRange4);
                $event->sheet->mergeCells($cellRange5);
                $event->sheet->mergeCells($cellRange6);
                $event->sheet->mergeCells($cellRange7);
                $event->sheet->mergeCells($cellRange8);
                $event->sheet->mergeCells($cellRange9);
                $event->sheet->mergeCells($cellRange10);
                $event->sheet->mergeCells($cellRange11);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange7)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange7)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange8)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange8)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange9)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange9)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange10)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange10)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange11)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange11)->getFont()->setSize(14);



                $event->sheet->getDelegate()->getStyle($cellRange12)->getFont()->setSize(11);
                // for ($i=3; $i < $last_row ; $i++) {
                //     // $event->sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
                //     $event->sheet->getStyle('A'.$i.':W'.$i)->applyFromArray([
                //         'borders' => [
                //             'allBorders' => [
                //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                 'color' => ['argb' => '000000'],
                //             ],
                //         ],
                //     ]);
                // }
            },
        ];
    }
}
