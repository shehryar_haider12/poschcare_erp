<?php

namespace App\Exports;

use App\PurchaseOrder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use DB;
use Carbon\Carbon;

class PurchaseExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        $data=[];
        $index = 0;
        $count = 1;

        $purchase=PurchaseOrder::with(['supplier','biller','orderdetails.products','orderdetails.variant'])
        ->orderBy('order_date','asc')
        ->get();
        foreach ($purchase as $key => $s) {
            foreach ($s->orderdetails as $key => $p) {
                $data[$index]['s_no'] = $count;
                $data[$index]['date'] = Carbon::parse($s->order_date)->format('d-M-Y');
                $data[$index]['supplier'] = $s->supplier->name;
                $data[$index]['biller'] = $s->biller->name;

                if($p->type == 0)
                {
                    $data[$index]['products'] = $p->products->pro_name;
                }
                else
                {
                    $v = explode('-',$p->variant->name);
                    $data[$index]['products'] = $v[1];
                }
                $data[$index]['cost'] = $p->cost;
                $data[$index]['qty'] = $p->quantity;
                $data[$index]['total'] = $p->sub_total;
                $data[$index]['s_status'] = $s->status;
                $data[$index]['p_status'] = $s->p_status;
                $index++;
                $count++;
            }
        }

        $this->count = count($purchase);
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['PURCHASE ORDERS'],
            [],
            ['S.NO',
            'DATE',
            'SUPPLIER',
            'BILLER',
            'PRODUCTS',
            'COST',
            'QUANTITY',
            'TOTAL',
            'ORDER STATUS',
            'PAYMENT STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange1 = 'A3:J3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    // $event->sheet->getStyle('F'.$i)->getAlignment()->setWrapText(true);
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
