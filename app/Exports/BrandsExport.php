<?php

namespace App\Exports;

use App\Brands;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class BrandsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        $BRAND =  Brands::select('id','b_name','c_p_name','c_p_contactNo','status')
        ->get();
        $this->count = count($BRAND);
        return collect($BRAND);
    }
    public function headings(): array
    {
        return
        [
            ['BRANDS LIST'],
            [],
            ['ID',
            'BRAND NAME',
            'CONTACT PERSON NAME',
            'CONTACT PERSON CONTACT NO',
            'STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:E1'; // All headers
                $cellRange1 = 'A3:E3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':E'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
