<?php

namespace App\Exports;

use App\ProductTransaferHistory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class ProductTransferHistoryDatesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    function __construct($from,$to,$p_name,$fw,$tw) {
        $this->from = $from;
        $this->to = $to;
        $this->p_name = $p_name;
        $this->fw = $fw;
        $this->tw = $tw;
    }
    public function collection()
    {
        $data=[];
        $index = 0;
        $a = 1;
        if($this->p_name == '0' && $this->tw == '0' && $this->fw == '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->orderBy('created_at','desc')
            ->get();
        }
        if($this->p_name != '0' && $this->tw != '0' && $this->fw != '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('from_w',$this->fw)
            ->where('to_w',$this->tw)
            ->where('p_id',$this->p_name)
            ->where('type',$this->types)
            ->orderBy('created_at','desc')
            ->get();
        }
        if($this->p_name == '0' && $this->fw != '0'  && $this->tw == '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('from_w',$this->fw)
            ->orderBy('created_at','desc')
            ->get();
        }
        if($this->tw != '0' && $this->fw == '0' && $this->p_name == '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('to_w',$this->tw)
            ->orderBy('created_at','desc')
            ->get();
        }
        if($this->tw == '0' && $this->fw == '0' && $this->p_name != '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('p_id',$this->p_name)
            ->where('type',$this->types)
            ->orderBy('created_at','desc')
            ->get();
        }

        if($this->p_name != '0' && $this->fw != '0'  && $this->tw == '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('from_w',$this->fw)
            ->where('p_id',$this->p_name)
            ->where('type',$this->types)
            ->orderBy('created_at','desc')
            ->get();
        }
        if($this->tw != '0' && $this->fw == '0' && $this->p_name != '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('to_w',$this->tw)
            ->where('p_id',$this->p_name)
            ->where('type',$this->types)
            ->orderBy('created_at','desc')
            ->get();
        }
        if($this->tw != '0' && $this->fw != '0' && $this->p_name == '0')
        {
            $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
            ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$this->from,$this->to])
            ->where('from_w',$this->fw)
            ->where('to_w',$this->tw)
            ->orderBy('created_at','desc')
            ->get();
        }
        foreach ($product as $key => $b) {
            $data[$index]['s_no'] = $a;
            $data[$index]['Transfer Date'] = $b->transfer_date;
            $data[$index]['From Warehouse'] = $b->fromWarehouse->w_name;
            $data[$index]['To Warehouse'] = $b->toWarehouse->w_name;
            if($b->type == 1)
            {
                $data[$index]['Code'] = $b->products->pro_code.' - '.$b->products->pro_name;
                $data[$index]['Brand'] = $b->products->brands->b_name;
                $data[$index]['Category'] = $b->products->category->cat_name;
                $data[$index]['Quantity'] = $b->quantity;
                $data[$index]['Cost'] = $b->products->cost;
                $data[$index]['Price'] = $b->products->price;
            }
            else {
                $data[$index]['Code'] = $b->variant->name;
                $data[$index]['Brand'] = $b->variant->product->brands->b_name;
                $data[$index]['Category'] = $b->variant->product->category->cat_name;
                $data[$index]['Quantity'] = $b->quantity;
                $data[$index]['Cost'] = $b->variant->product->cost;
                $data[$index]['Price'] = $b->variant->product->price;
            }

            $a++;
            $index++;
        }
        $this->count = count($product);
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['PRODUCT TRANSFER HISTORY'],
            [],
            ['S.NO',
            'TRANSFER DATE',
            'FROM WAREHOUSE',
            'TO WAREHOUSE',
            'PRODUCT CODE - NAME',
            'BRAND',
            'CATEGORY',
            'QUANTITY',
            'COST',
            'PRICE'
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange1 = 'A3:J3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
