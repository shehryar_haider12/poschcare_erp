<?php

namespace App\Exports;
use App\SaleDetails;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;

class SalesReportSalepersonExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;
    function __construct($sp) {
        $this->sp = $sp;
    }

    public function collection()
    {
        $saledetail = SaleDetails::whereHas('sale', function ($query) {
            $query->where('sp_id',$this->sp);
        })->with(['sale.warehouse' => function($query) {
            $avg = $query->select('id','w_name')->get();
        },
        'sale.customer' => function($query) {
            $avg = $query->select('id','name')->get();
        },
        'sale.saleperson' => function($query) {
            $avg = $query->select('id','name')->get();
        },
        'products' => function($query) {
            $avg = $query->select('id','pro_name')->get();
        },
        'variant' => function($query) {
            $avg = $query->select('id','name')->get();
        }
        ])
        ->select('id','s_id','p_id','price','quantity','type')
        ->get();
        $this->count = count($saledetail);
        return $saledetail;
    }

    public function map($saledetail): array
    {
        // dd($saledetail);
        if ($saledetail->type == 0) {
            return [
                $saledetail->id,
                $saledetail->s_id,
                $saledetail->sale->sale_date,
                $saledetail->sale->customer->name,
                $saledetail->products->pro_name,
                $saledetail->quantity,
                $saledetail->price,
                $saledetail->price * $saledetail->quantity,
                $saledetail->sale->warehouse->w_name,
                $saledetail->sale->saleperson->name
            ];
        }
        else {
            return [
                $saledetail->id,
                $saledetail->s_id,
                $saledetail->sale->sale_date,
                $saledetail->sale->customer->name,
                $saledetail->variant->name,
                $saledetail->quantity,
                $saledetail->price,
                $saledetail->price * $saledetail->quantity,
                $saledetail->sale->warehouse->w_name,
                $saledetail->sale->saleperson->name
            ];
        }

    }

    public function headings(): array
    {
        return
        [
            ['SALES REPORT'],
            [],
            ['S.NO',
            'INVOICE.NO',
            'SALE DATE',
            'CUSTOMER',
            'PRODUCT NAME',
            'QUANTITY',
            'PRICE',
            'AMOUNT',
            'WAREHOUSE',
            'SALE PERSON',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange1 = 'A3:J3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
