<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use DB;
use App\Sales;
use App\Vendors;

class CustomerDetailReportExportDateDifference implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    function __construct($from,$to,$id) {
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;
    }

    public function collection()
    {
        $sales = Sales::where('c_id',$this->id)
        ->with(['warehouse','customer','biller','saleperson'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
        }])
        ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$this->from,$this->to])
        ->get();
        $data=[];
        $count =1;
        $index = 0;
        foreach ($sales as $key => $b) {
            $data[$index]['s_no'] = $count;
            $data[$index]['date'] = $b->sale_date;
            $data[$index]['itype'] = $b->Iformat == 'Plain' ? 'Whithout GST' : $b->Iformat;
            $data[$index]['biller'] = $b->biller->name;
            $data[$index]['ino'] = $b->Ino;
            $data[$index]['total'] = $b->total;
            $data[$index]['paid'] = $b->total_amount == null ? '0' : $b->total_amount;
            $data[$index]['balance'] = $b->total - ($b->total_amount == null ? '0' : $b->total_amount);
            $data[$index]['ss'] = $b->s_status;
            $data[$index]['ps'] = $b->p_status;
            $count++;
            $index++;
        }
        $this->count = count($sales);
        return collect($data);
    }

    public function headings(): array
    {
        $customer = Vendors::find($this->id);
        return
        [
            ['Customer('.$customer->name.') Detailed Report'],
            [],
            ['S.NO',
            'DATE',
            'INVOICE TYPE',
            'BILLER',
            'INVOICE NO',
            'TOTAL',
            'PAID',
            'BALANCE',
            'SALE STATUS',
            'PAYMENT STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1'; // All headers
                $cellRange1 = 'A3:J3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':J'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
