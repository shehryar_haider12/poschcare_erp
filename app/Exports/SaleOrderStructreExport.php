<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class SaleOrderStructreExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data=[];
        $index = 0;
        $data[$index]['date'] = '19/12/2021';
        $data[$index]['company'] = "vicky's cosmetics";
        $data[$index]['name'] = "waqas";
        $data[$index]['sp'] = 'Haider Ali';
        $data[$index]['biller'] = "Posch Care";
        $data[$index]['stype'] = '(use sale types available in the list of software)';
        $data[$index]['itype'] = 'GST/ Plain';
        $data[$index]['ino'] = '(Write if invoice type is GST)';
        $data[$index]['pdate'] = '21/12/2021';
        $data[$index]['refno'] = '';
        $data[$index]['address'] = 'karachi';
        $data[$index]['ss'] = 'Pending/ Approve/ Partial/ Complete/ Delivered';
        $data[$index]['ps'] = 'Pending/ Partial/ Paid';
        $data[$index]['edd'] = '21/12/2021';
        $data[$index]['pm'] = 'Bill To Bill/ Days/ Cash on Delivery';
        $data[$index]['paytype'] = 'MRP/ TP';
        $data[$index]['remarks'] = 'anything';
        $data[$index]['brand'] = 'Swansi';
        $data[$index]['ptype'] = 'Raw/ Material/ Packaging/ Finished';
        $data[$index]['pname'] = 'Swansi aloe vera 140ml';
        $data[$index]['variant'] = '';
        $data[$index]['size'] = '';
        $data[$index]['color'] = '';
        $data[$index]['Qty'] = 10;
        $data[$index]['Price'] = 120;
        $data[$index]['deliver qty'] = 10;
        $data[$index]['discp'] = 5;
        $data[$index]['disca'] = 60;
        $data[$index]['vet'] = 1200;
        $data[$index]['ada'] = 1140;
        $data[$index]['tax'] = 19.38;
        $data[$index]['total'] = 1159.38;
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['SALE ORDER STRUCTURE'],
            ['Note: Kindly use the customer and its company name from software with correct spelling.'],
            ["Note: If it is not available in the software then write correct name and company of customer"],
            ['Note: Use all statuses same as given in the sheet'],
            ['Note: Write dates in the format given in the sheet'],
            ["Note: write pay date if payment mode is 'Days'"],
            ["Note: Write tax amount,value ex tax, after discount amount, if invoice type is 'GST'"],
            ["Note: write product details (brand,  ptype, name, codes) same as available in the software."],
            ["Note: if the product unit is in liter, mililiter, grams or kilograms; write per unit price and quantity as how many liters you want to sell?"],
            ["Note: if a product has multiple sizes, variant and color. Check the product list for examples"],
            ["Note: Remove all the note rows before uploading excel sheet"],
            ["Note: write invoice numbers of the sales whose invoice format is 'GST'"],
            [],
            ['Sale date',
            'company',
            'customer name',
            'Saleperson',
            'biller name',
            'sale type',
            'Invoice Type',
            'Invoice number',
            'pay date',
            'Refno',
            'address',
            'Sale status',
            'Payment status',
            'expected delivery date',
            'Payment mode',
            'Price type',
            'remarks',
            'brand',
            'product type',
            'product name',
            'variant',
            'size',
            'color',
            'Qty',
            'Price',
            'deliver qty',
            'Discount%',
            'Discount amount',
            'Value Ex tax',
            'After discount amount',
            'Tax amount',
            'Total',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange1 = 'A1:L1'; // All headers
                $cellRange2 = 'A2:L2'; // All headers
                $cellRange3 = 'A3:L3'; // All headers
                $cellRange4 = 'A4:L4'; // All headers
                $cellRange5 = 'A5:L5'; // All headers
                $cellRange6 = 'A6:L6'; // All headers
                $cellRange7 = 'A7:L7'; // All headers
                $cellRange8 = 'A8:L8'; // All headers
                $cellRange9 = 'A9:L9'; // All headers
                $cellRange10 = 'A10:L10'; // All headers
                $cellRange11 = 'A11:L11'; // All headers
                $cellRange12 = 'A12:L12'; // All headers
                $cellRange13 = 'A14:AF14'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange2)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange3)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange4)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange5)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange6)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange7)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);

                $event->sheet->getDelegate()->getStyle($cellRange8)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange9)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange10)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange11)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange12)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange13)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);

                $event->sheet->mergeCells($cellRange1);
                $event->sheet->mergeCells($cellRange2);
                $event->sheet->mergeCells($cellRange3);
                $event->sheet->mergeCells($cellRange4);
                $event->sheet->mergeCells($cellRange5);
                $event->sheet->mergeCells($cellRange6);
                $event->sheet->mergeCells($cellRange7);
                $event->sheet->mergeCells($cellRange8);
                $event->sheet->mergeCells($cellRange9);
                $event->sheet->mergeCells($cellRange10);
                $event->sheet->mergeCells($cellRange11);
                $event->sheet->mergeCells($cellRange12);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getFont()->setSize(14);

                $event->sheet->getDelegate()->getStyle($cellRange7)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange7)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange8)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange8)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange9)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange9)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange10)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange10)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange11)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange11)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange12)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange12)->getFont()->setSize(14);



                $event->sheet->getDelegate()->getStyle($cellRange13)->getFont()->setSize(11);
                // for ($i=3; $i < $last_row ; $i++) {
                //     // $event->sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
                //     $event->sheet->getStyle('A'.$i.':W'.$i)->applyFromArray([
                //         'borders' => [
                //             'allBorders' => [
                //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                 'color' => ['argb' => '000000'],
                //             ],
                //         ],
                //     ]);
                // }
            },
        ];
    }
}
