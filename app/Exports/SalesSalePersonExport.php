<?php

namespace App\Exports;

use App\Sales;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Carbon\Carbon;

class SalesSalePersonExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;
    function __construct($sp_id) {
        $this->sp_id = $sp_id;
    }

    public function collection()
    {
        $data=[];
        $index = 0;
        $count = 1;
        $sales=Sales::with(['warehouse','customer','biller','saleperson','sdetails.products.brands','sdetails.variant.product.brands'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
        }])
        ->where('sp_id',$this->sp_id)
        ->orderBy('sale_date','asc')
        ->get();
        foreach ($sales as $key => $s) {
            foreach ($s->sdetails as $key => $p) {
                $data[$index]['s_no'] = $count;
                $data[$index]['i_no'] = $s->Ino;
                $data[$index]['i_type'] = $s->Iformat == 'Plain' ? 'Without GST' : $s->Iformat;
                $data[$index]['date'] = Carbon::parse($s->sale_date)->format('d-M-Y');
                $data[$index]['customer'] = $s->customer->name;
                $data[$index]['biller'] = $s->biller->name;
                $data[$index]['saleperson'] = $s->saleperson->name;

                if($p->type == 0)
                {
                    $data[$index]['brand'] = $p->products->brands->b_name == null ? 'No Brand' : $p->products->brands->b_name ;
                    $data[$index]['products'] = $p->products->pro_name;
                }
                else
                {
                    $v = explode('-',$p->variant->name);
                    $data[$index]['brand'] = $p->variant->product->brands->b_name == null ? "No Brand" : $p->variant->product->brands->b_name;
                    $data[$index]['products'] = $v[1];
                }
                $data[$index]['price'] = $p->price;
                $data[$index]['qty'] = $p->quantity;
                $data[$index]['discount'] = $p->discounted_amount;
                $data[$index]['tax'] = $p->taxA;
                $data[$index]['total'] = $p->sub_total;
                $data[$index]['s_status'] = $s->s_status;
                $data[$index]['p_status'] = $s->p_status == 'Paid' ? 'Received' : $s->p_status;
                $index++;
                $count++;
            }
        }

        $this->count = count($sales);
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['SALES DATA '],
            [],
            ['S.NO',
            'INVOICE #',
            'INVOICE TYPE',
            'DATE',
            'CUSTOMER',
            'BILLER',
            'SALEPERSON',
            'BRAND',
            'PRODUCTS',
            'PRICE',
            'QUANTITY',
            'DISCOUNT',
            'TAX',
            'TOTAL',
            'SALE STATUS',
            'PAYMENT STATUS']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $cellRange1 = 'A3:P3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    // $event->sheet->getStyle('F'.$i)->getAlignment()->setWrapText(true);
                    $event->sheet->getStyle('A'.$i.':P'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
