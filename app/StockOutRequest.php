<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOutRequest extends Model
{
    protected $table = 'stock_out_request';
    protected $primaryKey = 'id';
    protected $fillable = [
        'req_date',
        'reason',
        'w_id',
        'status',
        'created_by',
        'updated_by',
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function rdetails()
    {
        return $this->belongsTo('App\StockOutRequestDetails');
    }

    public function reqdetails()
    {
        return $this->hasMany('App\StockOutRequestDetails','r_id','id');
    }
    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
