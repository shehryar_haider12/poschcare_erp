<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTransaferHistory extends Model
{
    protected $table = 'product_transfer_history';
    protected $primaryKey = 'id';
    protected $fillable = [
        'transfer_date',
        'from_w',
        'to_w',
        'p_id',
        'quantity',
        'created_by',
        'status',
        'type',
    ];
    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function fromWarehouse()
    {
        return $this->hasOne('App\Warehouse','id','from_w');
    }

    public function toWarehouse()
    {
        return $this->hasOne('App\Warehouse','id','to_w');
    }

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }
    public function currentStock()
    {
        // 'w_id','from_w',
        return $this->hasOne('App\CurrentStock','p_id','p_id');
    }
}
