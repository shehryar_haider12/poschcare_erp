<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationDetails extends Model
{
    protected $table = 'quotation_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        'q_id',
        'p_id',
        'cost',
        'quantity',
        'type'
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function quotation()
    {
        return $this->hasMany('App\Quotation','id','q_id');
    }

    public function request()
    {
        return $this->hasMany('App\PurchaseRequestDetails','p_id','p_id');
    }


}
