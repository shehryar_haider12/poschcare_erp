<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleType extends Model
{
    protected $table = 'sale_type';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
    ];

    public function sales()
    {
        return $this->hasMany('App\Sales','st_id','id');
    }
}
