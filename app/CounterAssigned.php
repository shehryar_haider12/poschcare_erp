<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CounterAssigned extends Model
{
    protected $table = 'counter_assigned';
    protected $primaryKey = 'id';
    protected $fillable = [
        'u_id',
        'c_id',
    ];

    public function counter()
    {
        return $this->hasOne('App\Counter','id','c_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','u_id');
    }
}
