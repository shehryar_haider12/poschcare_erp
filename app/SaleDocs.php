<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleDocs extends Model
{
    protected $table = 'sale_doc';
    protected $primaryKey = 'id';
    protected $fillable = [
        's_id',
        'document'
    ];
    public function vendor()
    {
        return $this->hasOne('App\Sales','id','s_id');
    }
}
