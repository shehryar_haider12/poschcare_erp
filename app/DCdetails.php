<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DCdetails extends Model
{
    protected $table = 'dc_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        's_id',
        'p_id',
        'quantity',
        'sub_total',
        'price',
        'delivered_quantity',
        'discount_percent',
        'discounted_amount',
        'type',
        'cost',
        'taxA',
        'vet',
        'afterDiscount',
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function dc()
    {
        return $this->hasOne('App\DC','id','s_id');
    }

    public function current()
    {
        return $this->hasMany('App\CurrentStock','p_id','p_id');
    }

    public function stockout()
    {
        return $this->belongsTo('App\StockOut');
    }

    public function stock()
    {
        return $this->hasMany('App\StockOut','sale_d_id','id')->where('s_type','DC');
    }
}
