<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadCategory extends Model
{
    protected $table = 'head_category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'a_id',
        'created_by',
        'updated_by',
        'code'
    ];

    public function hoa()
    {
        return $this->hasOne('App\HeadofAccounts','id','a_id');
    }

    public function accountDetails()
    {
        return $this->hasMany('App\AccountDetails','c_id','id');
    }
}
