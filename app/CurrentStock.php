<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentStock extends Model
{
    protected $table = 'current_stock';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'w_id',
        'quantity',
        'unit_quantity',
        'variant',
        'unit_id',
        'type',
    ];

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function stock()
    {
        return $this->hasMany('App\Stocks','p_id','p_id');
    }

    public function sales()
    {
        return $this->hasMany('App\SaleDetails','p_id','p_id');
    }

}
