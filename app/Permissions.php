<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    protected $table = 'permissions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'm_id',
        'route',
    ];

    public function menu()
    {
        return $this->hasOne('App\UserMenu','id','m_id');
    }

    public function roleP()
    {
        return $this->hasMany('App\RolePermission','p_id','id');
    }
}
