<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetails extends Model
{
    protected $table = 'purchase_order_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        'o_id',
        'p_id',
        'quantity',
        'sub_total',
        'received_quantity',
        'cost',
        'type'
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function sales()
    {
        return $this->hasMany('App\SaleDetails','p_id','p_id');
    }

    public function purchase()
    {
        return $this->hasOne('App\PurchaseOrder','id','o_id');
    }

    public function stocks()
    {
        return $this->belongsTo('App\Stocks');
    }

    public function stock()
    {
        return $this->hasMany('App\Stocks','purchase_d_id','id');
    }

    // public function purchasestocks()
    // {
    //     return $this->belongsTo('App\Stocks');
    // }

}
