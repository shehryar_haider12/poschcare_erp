<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalVoucherDetails extends Model
{
    protected $table = 'journal_voucher_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        'vid',
        'spid',
    ];
}
