<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'cat_name',
        'created_by',
        'updated_by',
        'status'
    ];

    public function products()
    {
        return $this->belongsTo('App\Products');
    }
    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }
    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
