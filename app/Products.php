<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'pro_name',
        'pro_code',
        'weight',
        'cost',
        'price',
        'alert_quantity',
        'unit_id',
        'brand_id',
        'cat_id',
        's_cat_id',
        'image',
        'description',
        'visibility',
        'created_by',
        'updated_by',
        'p_type',
        'status',
        'vstatus',
        'mrp',
        'wooId',
        'mstatus',
        'tp',
        'ct_id',
        'otherCost',
    ];
    public function unit()
    {
        return $this->hasOne('App\Unit','id','unit_id');
    }
    public function costType()
    {
        return $this->hasOne('App\CostType','id','ct_id');
    }
    public function brands()
    {
        return $this->hasOne('App\Brands','id','brand_id');
    }
    public function category()
    {
        return $this->hasOne('App\Category','id','cat_id');
    }
    public function subcategory()
    {
        return $this->hasOne('App\Subcategory','id','s_cat_id');
    }

    public function odetails()
    {
        return $this->hasMany('App\PurchaseOrderDetails','p_id','id');
    }

    public function rdetails()
    {
        return $this->hasMany('App\PurchaseOrderDetails','p_id','id');
    }


    public function sdetails()
    {
        return $this->hasMany('App\SaleDetails','p_id','id');
    }

    public function qdetails()
    {
        return $this->hasMany('App\QuotationDetails','p_id','id');
    }

    public function stocks()
    {
        return $this->belongsTo('App\Stocks');
    }

    public function stockout()
    {
        return $this->belongsTo('App\StockOut');
    }

    public function currentstocks()
    {
        return $this->hasMany('App\CurrentStock','p_id','id');
    }

    public function current()
    {
        return $this->belongsTo('App\CurrentStock');
    }

    public function finish()
    {
        return $this->hasMany('App\FinishProducts','id','id');
    }

    public function transfer()
    {
        return $this->belongsTo('App\ProductTransaferHistory');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    public function variants()
    {
        return $this->hasMany('App\ProductVariants','p_id','id');
    }

}
