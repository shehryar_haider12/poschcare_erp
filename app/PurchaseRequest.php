<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
    protected $table = 'purchase_request';
    protected $primaryKey = 'id';
    protected $fillable = [
        'req_date',
        'w_id',
        'status',
        'created_by',
        'updated_by',
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function rdetails()
    {
        return $this->belongsTo('App\PurchaseRequestDetails');
    }

    public function quotations()
    {
        return $this->belongsTo('App\Quotation');
    }

    public function quotation()
    {
        return $this->hasMany('App\Quotation','r_id','id');
    }


    public function reqdetails()
    {
        return $this->hasMany('App\PurchaseRequestDetails','o_id','id');
    }
    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
