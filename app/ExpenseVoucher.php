<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseVoucher extends Model
{
    protected $table = 'expense_voucher';
    protected $primaryKey = 'id';
    protected $fillable = [
        'v_date',
        'c_id',
        'sc_id',
        'description',
        'status',
        'amount',
        'prepared_by',
    ];

    public function preparedUser()
    {
        return $this->hasOne('App\User','id','prepared_by');
    }

    public function category()
    {
        return $this->hasOne('App\HeadCategory','id','c_id');
    }
    public function subcategory()
    {
        return $this->hasOne('App\AccountDetails','Code','sc_id');
    }
}
