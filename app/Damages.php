<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Damages extends Model
{
    protected $table = 'damages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'quantity',
        'stock_date',
        'price',
        'w_id',
        'sale_d_id',
        'sale_id',
        'created_by',
        'type',
    ];

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function sdetails()
    {
        return $this->hasOne('App\SaleDetails','id','sale_d_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }
}
