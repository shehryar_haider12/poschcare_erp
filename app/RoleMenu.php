<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = 'role_menu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'r_id',
        'm_id',
        'created_by',
        'updated_by',
    ];

    public function menu()
    {
        return $this->hasOne('App\UserMenu','id','m_id');
    }

    public function role()
    {
        return $this->hasOne('App\Roles','id','r_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
