<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvgCost extends Model
{
    protected $table = 'avg_cost';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'po_id',
        'cost',
        'type',
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function purchase()
    {
        return $this->hasOne('App\PurchaseOrder','id','o_id');
    }
}
