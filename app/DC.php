<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DC extends Model
{
    protected $table = 'dc';
    protected $primaryKey = 'id';
    protected $fillable = [
        'date',
        'ref_no',
        'b_id',
        'c_id',
        'w_id',
        's_address',
        'd_status',
        'p_status',
        'doc',
        'total',
        'note',
        'created_by',
        'updated_by',
        'sp_id',
        'expected_date',
        'advance',
        'pay_type',
        'Iformat',
        'Itype',
        'Ino',
        'pdate',
        'remarks',
        'st_id',
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function saletype()
    {
        return $this->hasOne('App\SaleType','id','st_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Vendors','id','c_id');
    }

    public function biller()
    {
        return $this->hasOne('App\Vendors','id','b_id');
    }

    public function saleperson()
    {
        return $this->hasOne('App\Vendors','id','sp_id');
    }

    public function sdetails()
    {
        return $this->hasMany('App\DCdetails','s_id','id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
