<?php
use App\RoleMenu;
use App\RolePermission;
use App\Permissions;
use App\UserMenu;
use Automattic\WooCommerce\Client;

function getMenuId($request){
    return UserMenu::where('route',$request->route()->getName())->first()->id;
}
function getRolePermission($menu_id){
    $permission_id      =   Permissions::where('m_id',$menu_id)->get()->pluck('id')->toArray();
    $role_permissions   =   RolePermission::with('permission')->where('r_id',Auth::user()->r_id)->whereIn('p_id',$permission_id)->get()->pluck('p_id')->toArray();
    return Permissions::whereIn('id',$role_permissions)->get()->pluck('name')->toArray();
}



function wooCommerce()
{

    $woocommerce = new Client(
        'https://lovinegallery.com/', // Your store URL
        'ck_8557d1867eceb6c58ec1be382fa0e195b60ba758', // Your consumer key
        'cs_026abbfba695f10c5019ee4df2dce105398499b8', // Your consumer secret
        [
            'wp_api' => true, // Enable the WP REST API integration
            'version' => 'wc/v3' // WooCommerce WP REST API version
        ]
    );
    return $woocommerce;
}

function readCsv($path){
    $filename         = $path;
    $delimiter        = ',';

    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $header = null;
    $data = [];
    if (($handle = fopen($filename, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                  $header = $row;
            else
                {
                  // if(count($row) == count($header))
                  // {

                    $data[] = array_combine($header, $row);
                  // }
                  // else {
                    // dd($row);
                  // }
                }

        }
        fclose($handle);
    }
    return $data;
}


?>
