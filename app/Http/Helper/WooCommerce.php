<?php

use App\Products;
use App\ProductVariants;
use App\Vendors;
use App\Variants;

function storeProduct(array $request, $variants)
{
    // dd($variants);
    $woocommerce        =   wooCommerce();
    if(isset($request['image']))
    {
        $wp_Data=([
            'name' => $request['pro_name'],
            'regular_price' => $request['price'],
            'description' => $request['description'],
            'short_description' => null,
            'manage_stock' => true,
            'categories' => [
                ['id' => $request['cat_id']]
            ],
            'images' => [
                'src' => env('AppUrl').$request['image']
            ]
        ]);
    }
    else {
        $wp_Data=([
            'name' => $request['pro_name'],
            'regular_price' => $request['price'],
            'description' => $request['description'],
            'short_description' => null,
            'manage_stock' => true,
            'categories' => [
                ['id' => $request['cat_id']]
            ],
            'images' => [

            ]
        ]);
    }
    $woo = $woocommerce->post('products', $wp_Data);
    // if(count($variants['variants']) > 0 )
    // {
    //     for ($i=1; $i <= count($variants['variants']) ; $i++)
    //     {
    //         for ($j=0; $j < count($variants['variants'][$i]) ; $j++) {
    //             $pv = Variants::where('name',$variants['variants'][$i][$j])->first();
    //             $data = [
    //                 'regular_price' => $variants['pricev'][$i] == null ? $request['price'] :  $variants['pricev'][$i],
    //                 'image' => [],
    //                 'attributes' => [
    //                     [
    //                         'id' => 5,
    //                         'variation' => true,
    //                         'visible' => true,
    //                         'option' => $variants['variants'][$i][$j]
    //                     ]
    //                 ]
    //             ];
    //             // dd($data);
    //             $woocommerce->post('products/'.$woo->id.'/variations', $data);
    //         }
    //     }
    // }


    return $woo;
}

function listProduct()
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('products')),true);
}

function addStock(array $request,$id)
{
    $woocommerce        =   wooCommerce();
    $products_w = json_decode(json_encode($woocommerce->get('products')),true);
    $product = Products::with('unit')
    ->where('id',$id)
    ->first();


    if(count($products_w) > 0)
    {
        for ($i=0; $i < count($products_w) ; $i++) {
            if($product->pro_name == $products_w[$i]['name']){
                if($products_w[$i]['stock_quantity'] == null)
                {
                    $stock_quantity = [
                        'stock_quantity' => $request['quantity']
                    ];
                }
                else {
                    $stock_quantity = [
                        'stock_quantity' => $request['quantity'] + $products_w[$i]['stock_quantity']
                    ];
                }

                $woocommerce->put('products/'.$products_w[$i]['id'], $stock_quantity);
            }
        }
    }

}

function listOrders()
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('orders')),true);
}


function addPurchaseQty(array $request)
{
    $woocommerce        =   wooCommerce();
    for ($i=0; $i < count($request['p_id']) ; $i++) {
        $product = Products::with('unit')
        ->where('id',$request['p_id'])
        ->first();
        if($product->wooId != null)
        {
            $wooproduct = $woocommerce->get('products/'.$product->wooId);
            if($wooproduct['stock_quantity'] == null)
            {
                $stock_quantity = [
                    'stock_quantity' => $request['quantity']
                ];
            }
            else {
                $stock_quantity = [
                    'stock_quantity' => $request['quantity'] + $wooproduct['stock_quantity']
                ];
            }
            $woocommerce->put('products/'.$product->wooId, $stock_quantity);
        }
    }
}

function subSaleQty(array $request)
{
    $woocommerce        =   wooCommerce();
    for ($i=0; $i < count($request['p_id']) ; $i++) {
        $product = Products::with('unit')
        ->where('id',$request['p_id'])
        ->first();
        if($product->wooId != null)
        {
            $wooproduct = $woocommerce->get('products/'.$product->wooId);

            $stock_quantity = [
                'stock_quantity' => $wooproduct['stock_quantity'] - $request['quantity']
            ];
            $woocommerce->put('products/'.$product->wooId, $stock_quantity);
        }
    }
}



//////////// trax API ////////////////
function addShipmentTrax(array $request)
{
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://sonic.pk/']);
    $biller = $client->request('GET', '/api/pickup_addresses', [
        'headers' => [
            'Authorization'     => 'U0ZHeTVSbm13dFl1cmtHR1RrSXViTnh5UGEwcTBZSkxSOHZHQWFCRnRXRXM5WG92d0gzeGhNVURGTllG618b71cf0a874'
            ]
        ]);
    $biller = json_decode($biller->getBody(),true);
    $city = $client->request('GET', '/api/cities', [
        'headers' => [
            'Authorization'     => 'U0ZHeTVSbm13dFl1cmtHR1RrSXViTnh5UGEwcTBZSkxSOHZHQWFCRnRXRXM5WG92d0gzeGhNVURGTllG618b71cf0a874'
            ]
        ]);
    $city = json_decode($city->getBody(),true);

    $customer = Vendors::find($request['c_id']);
    $city_id = 0;
    foreach ($city['cities'] as $key => $c) {
        if(str_contains($customer->address,$c['name']))
        {
            $city_id = $c['id'];
            break;
        }
    }
    $products = '';
    $totalQty = 0;
    for ($i=0; $i < count($request['p_id']) ; $i++) {
        if($request['type'][$i] == 0)
        {
            $prod = Products::find($request['p_id'][$i]);
            $products.= $prod->pro_name.'('.$request['quantity'][$i].')'."\n";
        }
        else
        {
            $prod = ProductVariants::find($request['p_id'][$i]);
            $name = explode('-',$prod->name);
            $products.= $name[1].'('.$request['quantity'][$i].')'."\n";
        }
        $totalQty+=$request['quantity'][$i];
    }

    // dd($products);
    $data=([
        'service_type_id' => 1,
        'pickup_address_id' => $biller['pickup_addresses'][0]['id'],
        'information_display' => 1,
        'consignee_city_id' => $city_id == 0 ? '202' : $city_id,
        'consignee_name' => $customer->name,
        'consignee_address' => $request['s_address'] ,
        'consignee_phone_number_1' => $customer->c_no,
        'consignee_email_address' => $customer->email == null ? 'abc@gmail.com' : $customer->email,
        'item_product_type_id' => 8,
        'item_description' => $products,
        'item_quantity' => $totalQty,
        'item_insurance' => 0,
        'pickup_date' => $request['sale_date'],
        'estimated_weight' => 0.1,
        'shipping_mode_id' => 1,
        'amount' => round($request['final']),
        'payment_mode_id' => $request['advance'] == 'Yes' ? 4 : 1,
        'charges_mode_id' => 4

    ]);
    $order = $client->request('POST', '/api/shipment/book', [
        'headers' => [
            'Authorization'     => 'U0ZHeTVSbm13dFl1cmtHR1RrSXViTnh5UGEwcTBZSkxSOHZHQWFCRnRXRXM5WG92d0gzeGhNVURGTllG618b71cf0a874'
        ],
        'form_params' => $data
    ]);
    // dd(json_decode($order->getBody(),true));
}

?>
