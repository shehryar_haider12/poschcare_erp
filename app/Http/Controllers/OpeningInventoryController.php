<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GeneralLedger;
use App\AvgCost;
use App\Damages;

class OpeningInventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewOpening(Request $request)
    {
        $ledger = GeneralLedger::where('description','September Opening')->where('account_code','Like','CA-02%')
        ->where('account_code','not like','NCA-02%')->get();
        return view('openingInventory.index',compact('ledger'));
    }

    public function edit($id)
    {
        $ledger = GeneralLedger::find($id);
        return view('openingInventory.edit',compact('ledger','id'));

    }

    public function update(Request $request)
    {
        if($request->type == 'Cost')
        {
            GeneralLedger::where('id',$request->id)->update([
                'amount' => $request->cost,
                'debit' => $request->cost * $request->oqty,
            ]);
        }
        if($request->type == 'Quantity')
        {
            GeneralLedger::where('id',$request->id)->update([
                'stock_in' => $request->qty,
                'net_value' => $request->qty,
                'balance' => $request->qty,
                'debit' => $request->ocost * $request->qty,
            ]);
            $debit = GeneralLedger::where('account_code',$request->code)->where('w_id',$request->w_id)
            ->where('id','>',$request->id)->get();
            if($debit->isNotEmpty())
            {
                foreach ($debit as $key => $d) {
                    $balance = 0 ;
                    $balance = $d->balance - $request->oqty + $request->qty;
                    GeneralLedger::where('id',$d->id)
                    ->update([
                        'balance' => $balance
                    ]);
                }

            }

        }
        if($request->type == 'Both')
        {
            GeneralLedger::where('id',$request->id)->update([
                'amount' => $request->cost1,
                'stock_in' => $request->qty1,
                'debit' => $request->cost1 * $request->qty1,
                'net_value' => $request->qty1,
                'balance' => $request->qty1,
            ]);

            $debit = GeneralLedger::where('account_code',$request->code)->where('w_id',$request->w_id)
            ->where('id','>',$request->id)->get();
            if($debit->isNotEmpty())
            {
                foreach ($debit as $key => $d) {
                    $balance = 0 ;
                    $balance = $d->balance - $request->oqty + $request->qty1;
                    GeneralLedger::where('id',$d->id)
                    ->update([
                        'balance' => $balance
                    ]);
                }

            }
        }

        toastr()->success('Ledger updated successfully!');
        return redirect(url('').'/openingInventory');
    }
    public function avgHistory()
    {
        $avg = AvgCost::with(['products','variant','purchase'])->get();
        return view('avg.history',compact('avg'));
    }

    public function damagesHistory()
    {
        $damages = Damages::with(['products','variant','warehouse'])->orderBy('id','desc')->get();
        // return $damages;
        return view('damages.history',compact('damages'));
    }

}
