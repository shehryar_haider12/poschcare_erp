<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Products;
use App\ProductsUpdateRequest;
use App\Unit;
use App\Brands;
use App\Category;
use App\Vendors;
use App\Variants;
use App\ProductVariants;
use App\ProductVariantUpdate;
use App\Stocks;
use App\StockReceiving;
use App\Subcategory;
use App\CostType;
use App\Warehouse;
use App\CurrentStock;
use App\FinishProducts;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\GST;
use App\User;
use DataTables;
use Storage;
use DB;
use App\Exports\ProductsExport;
use App\Exports\FinishProductExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Carbon\Carbon;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //pricechecker
    public function productsearch($id)
    {
        $product = Products::find($id);
        return $product;
    }

    public function index(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.index',compact('cat','brands','permissions'));
    }

    public function datatable()
    {
        $product=Products::with(['brands','unit','category','subcategory','variants'])
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'));
        }])
        ->withCount(['currentstocks as unit_total' => function($query) {
            $avg = $query->select(DB::raw('sum(unit_quantity)'));
        }])
        ->get();
        return DataTables::of($product)->make();
    }

    public function finishProducts(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.Finishindex',compact('cat','brands','permissions'));
    }

    public function finishProductsDatatable()
    {
        $product=Products::with(['brands','unit','category','subcategory'])
        ->has('finish.rawproducts')
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'));
        }])
        ->where('p_type','Finished')
        ->get();
        // return $product;
        return DataTables::of($product)->make();
    }

    public function modal($id)//sales create using warehouse
    {
        $product=CurrentStock::with(['products','products.brands','products.unit','products.category','products.subcategory','variant.product.brands','variant.product.category','variant.product.unit'])
        ->where('w_id',$id)
        ->get();
        return DataTables::of($product)->make();
    }

    public function detail($id)//each finish product detail
    {
        $fp = FinishProducts::with(['rawproducts','rawproducts2'])
        ->where('id',$id)
        ->get();
        return $fp;
    }

    public function finishProductEdit($id)
    {
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $code ='P-'.rand();
        $fp = FinishProducts::with(['rawproducts.brands','rawproducts.unit'])
        ->where('id',$id)
        ->get();
        $product = Products::find($id);
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $data= [
            'isEdit' => true,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'product' => $product,
            'pro' => $pro,
            'fp' => $fp,
            'code' => $code,
        ];
        return view('product.finish',$data);
    }

    public function finishProductUpdate($id,Request $request)
    {
        $product = Products::find($id);
        DB::table('finish_products')->where('id', $id)->delete();
        $u_id = Auth::user()->id;
        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        $final = 0;
        for ($i=0; $i <count($request->p_id) ; $i++)
        {
            if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i] * 1000;
            }
            else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i] * 1000;
            }
            else
            {
                $final =$request->quantity[$i] * 1;
            }
            FinishProducts::create([
                'id' => $id,
                'p_id' => $request->p_id[$i],
                'quantity' => $final
            ]);
        }
        if(isset($request->image))
        {
            $data=([
                'image' => $request->image,
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'updated_by' => $u_id,
                'mrp' => $request->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$request->image);
            Products::where('id',$id)
            ->update($data);
            toastr()->success('Product updated successfully!');
            return redirect(url('').'/product/finishProducts');
        }
        else
        {
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'updated_by' => $u_id,
                'mrp' => $request->mrp
            ]);
            Products::where('id',$id)
            ->update($data);
        }
        $hcat = HeadCategory::where('name','Inventory')->first();

        $account = AccountDetails::where('c_id',$hcat->id)
        ->latest('created_at')->first();

        if($account == null)
        {
            $id = 001;
        }
        else
        {
            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
        }

        $str_length = strlen((string)$id)+2;
        $id = substr("0000{$id}", -$str_length);
        $code = $hcat->code.'-'.$id;
        $data1 = [
            'Code' => $code,
            'name_of_account' => $request->pro_name.' - '.$request->pro_code,
            'c_id' => $hcat->id,
            'created_by' => $u_id
        ];
        AccountDetails::create($data1);

        toastr()->success('Product updated successfully!');
        return redirect(url('').'/product/finishProducts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $variant=Variants::all();
        $pro=Products::with(['brands','unit','category','subcategory'])
        // ->doesntHave('finish.rawproducts')
        ->get();
        $code =rand();
        $data= [
            'isEdit' => false,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'code' => $code,
            'variant' => $variant,
            'pro' => $pro,
            'ct' => CostType::all(),
            'permissions' => getRolePermission($menu_id)
        ];
        return view('product.create',$data);
    }


    public function addDT()
    {
        $product=DB::select("SELECT * FROM brands,units, products where (products.p_type='Raw' or products.p_type='Packaging' or products.p_type='Material' or products.p_type='Finished')
        and brands.id = products.brand_id and units.id = products.unit_id");
        return DataTables::of($product)->make();
    }

    public function add(Request $request)
    {
        $menu_id =   getMenuId($request);
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $code = rand();
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $data= [
            'isEdit' => false,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'pro' => $pro,
            'code' => $code,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('product.finish',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addFinishProduct(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pro_code'      =>  'required|string|max:255|unique:products'
        ]);

        $role_id = Auth::user()->r_id;
        $env_a_id = env('ADMIN_ID');
        $env_m_id = env('MANAGER_ID');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }

        $vstatusp=0;
        // dd($status);

        $u_id = Auth::user()->id;
        $unit = Unit::find($request->unit_id);
        $quantity = 0;
        $date=Carbon::now()->format('Y-m-d');
        if(isset($request->image))
        {
            $data=([
                'image' => $request->image,
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $request->unit_id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => 'Finished',
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$request->image);
        }
        else
        {
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $request->unit_id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => 'Finished',
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp
            ]);
        }

        $p = Products::create($data);
        $final = 0;
        for ($i=1; $i <= count($request->p_id) ; $i++) {
            if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i]  ;
            }
            else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i] ;
            }
            else
            {
                $final =$request->quantity[$i] * 1;
            }
            FinishProducts::create([
                'id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $final,
                'type' => $request->type[$i]
            ]);
        }
           $hcat = HeadCategory::where('name','Inventory')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;
            $data1 = [
                'Code' => $code,
                'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                'c_id' => $hcat->id,
                'created_by' => $u_id,
                'type' => 0
            ];
            AccountDetails::create($data1);

        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data2 = [
            'notification' => 'New Finished product has been added by '.$u_name,
            'link' => url('').'/product/finishProducts',
            'name' => 'View Finish Products',
        ];
        Notification::send($user, new AddNotification($data2));
        toastr()->success('Final Product added successfully!');
        return redirect()->back();
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pro_code'      =>  'required|string|max:255|unique:products'
        ]);

        if( !isset($request->cat_id))
        {
            $catid = Category::where('cat_name','No Category')->pluck('id')->first();
        }
        else {
            $catid = $request->cat_id;
        }
        if( !isset($request->s_cat_id))
        {
            $scatid = SubCategory::where('s_cat_name','No Subcategory')->pluck('id')->first();
        }
        else {
            $scatid = $request->s_cat_id;
        }

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
            $vstatus = 1;
        }
        else
        {
            $status = 0;
            $vstatus = 0;
        }
        if($request->variants[1][0] != null)
        {
            $vstatusp=1;
        }
        else
        {
            $vstatusp=0;
        }
        $u_id = Auth::user()->id;

        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        $pro=Products::where('pro_name',$request->pro_name)
        ->where('brand_id',$request->brand_id)
        ->where('s_cat_id',$scatid)
        ->where('cat_id',$catid)
        ->first();
        if($pro == null)
        {
            if(isset($request->image))
            {
                $data=([
                    'image' => $request->image,
                    's_cat_id' => $scatid,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'unit_id' => $unit->id,
                    'cost' =>  (isset($request->ct_id) && $request->ct_id != 'No Cost') ? ($request->cost  + $request->otherCost) : $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $catid,
                    'visibility' => $request->visibility,
                    'description' => $request->description,
                    'p_type' => $request->p_type,
                    'created_by' => $u_id,
                    'status' => $status,
                    'vstatus' => $vstatusp,
                    'mrp' => $request->mrp,
                    'mstatus' => $request->mstatus,
                    'tp' => $request->tp,
                    'ct_id' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->ct_id : null,
                    'otherCost' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->otherCost : null
                ]);
                $data['image']= Storage::disk('uploads')->putFile('',$request->image);
            }
            else
            {
                $data=([
                    's_cat_id' => $scatid,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'unit_id' => $unit->id,
                    'cost' =>  (isset($request->ct_id) && $request->ct_id != 'No Cost') ? ($request->cost  + $request->otherCost) : $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $catid,
                    'visibility' => $request->visibility,
                    'description' => $request->description,
                    'p_type' => $request->p_type,
                    'created_by' => $u_id,
                    'status' => $status,
                    'vstatus' => $vstatusp,
                    'mrp' => $request->mrp,
                    'mstatus' => $request->mstatus,
                    'tp' => $request->tp,
                    'ct_id' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->ct_id : null,
                    'otherCost' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->otherCost : null
                ]);


            }
            // dd($data);
            $p=Products::create($data);

            $u_name = Auth::user()->name;
            $user = User::where('r_id', config('app.adminId'))->get();
            $data2 = [
                'notification' => 'New '.$request->p_type.' product has been added by '.$u_name,
                'link' => url('').'/product',
                'name' => 'View Products',
            ];
            $final = 0;
            if(isset($request->p_id))
            {
                if(count($request->p_id) > 0)
                {
                    for ($i=1; $i <= count($request->p_id) ; $i++) {

                        $final =$request->quantity[$i] * 1;
                        FinishProducts::create([
                            'id' => $p->id,
                            'p_id' => $request->p_id[$i],
                            'quantity' => $final,
                            'type' => $request->type[$i],
                            'cost' => $request->costf[$i],
                            'costperunit' => $request->costperunit[$i],
                        ]);
                    }
                }
            }

            if($request->variants[1][0] != null)
            {
                for ($i=1; $i <= count($request->variants) ; $i++) {
                    $name = $request->pro_code.' - '.$request->pro_name.'-';

                    for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                        if($j == count($request->variants[$i] ) - 1){
                            $name .=$request->variants[$i][$j];
                        }else {
                            $name .=$request->variants[$i][$j].'-';
                        }
                    }

                    $ProductVariants = ProductVariants::where('p_id',$p->id)
                    ->where('name',$name)->first();
                    if($ProductVariants == null)
                    {

                        ProductVariants::create([
                            'p_id' => $p->id,
                            'name' => $name,
                            'status' => $vstatus,
                            'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
                            'price' => $request->pricev[$i] == null ? $request->mrp : $request->pricev[$i],
                            'tp' => $request->tpv[$i] == null ? $request->tp : $request->tpv[$i],
                        ]);
                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->orderBy('id','desc')->first();

                        if($account == null)
                        {
                            $id = 001;
                        }
                        else
                        {
                            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }
                        // dd($id);
                        $str_length = strlen((string)$id)+2;
                        $id = substr("0000{$id}", -$str_length);
                        $code = $hcat->code.'-'.$id;

                        $data1 = [
                            'Code' => $code,
                            'name_of_account' => $name,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 1
                        ];
                        AccountDetails::create($data1);
                    }

                }
            }
            else
            {
                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;
                $data1 = [
                    'Code' => $code,
                    'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id,
                    'type' => 0
                ];
                AccountDetails::create($data1);
            }
            Notification::send($user, new AddNotification($data2));
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Product added successfully!';
                $response['p'] = $p;
                return response()->json($response, 200);
            }
            toastr()->success('Product added successfully!');
            return redirect()->back();
        }
        else
        {
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Product account already exist!';
                $p = null;
                return response()->json($response, 200);
            }
            toastr()->error('Product already exist!');
            return redirect()->back();
        }
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Products::find($id);
        if ($item->update(['status' => $status])) {
            Products::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' =>$u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


    public function Vstatus(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $item = ProductVariants::find($id);
        if ($item->update(['status' => $status])) {
            ProductVariants::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::with(['brands','unit','category','subcategory','finish'])
        ->where('id',$id)
        ->first();
        return $product;
    }
    public function stockShow($id)
    {
        if(request()->ajax())
        {
            $p_id = substr($id, 0, strpos($id, '-'));
            $type = substr($id, strpos($id, '-')+1);
            $idntype = [];
            $qty = [];
            if($type == 0)
            {
                $product = Products::with(['brands','unit','category','subcategory','finish'])
                ->where('id',$id)
                ->first();
                if($product->finish->isNotEmpty())
                {
                    foreach ($product->finish as $key => $f) {
                        if($f->type == 1)
                        {
                            $punit = ProductVariants::with('product.unit')
                            ->where('id',$f->p_id)
                            ->first();
                            if($punit->product->unit->u_name == 'Liter' || $punit->product->unit->u_name == 'Mililiter' || $punit->product->unit->u_name == 'Grams' || $punit->product->unit->u_name == 'Kilograms')
                            {
                                array_push($idntype, array($f->p_id, $f->type));
                            }
                        }
                        else
                        {
                            $punit = Products::with('unit')->where('id',$f->p_id)->first();
                            if($punit->unit->u_name == 'Liter' || $punit->unit->u_name == 'Mililiter' || $punit->unit->u_name == 'Grams' || $punit->unit->u_name == 'Kilograms')
                            {
                                array_push($idntype, array($f->p_id, $f->type));
                            }
                        }
                    }
                    if(count($idntype) <= 0)
                    {
                        $qty = [];
                    }
                    else
                    {
                        foreach ($idntype as $key => $value) {
                            if($value[1] == 1)
                            {
                                array_push($qty,StockReceiving::with(['warehouse','variant','unit'])->where('p_id',$value[0])
                                ->where('type',$value[1])->get());
                            }
                            else {
                                array_push($qty,StockReceiving::with(['warehouse','products','unit'])->where('p_id',$value[0])
                                ->where('type',$value[1])->get());
                            }
                        }
                    }

                }
                else {
                    array_push($qty,StockReceiving::with(['warehouse','variant','unit','products'])->where('p_id',$p_id)
                    ->where('type',$type)->get());
                }
                return [$product,$type,$qty];
            }
            else
            {
                $product = ProductVariants::with(['product.brands','product.unit','product.category','product.subcategory'])
                ->where('id',$id)
                ->first();
                return [$product,$type];
            }

        }

    }

    public function sales($id)
    {
        $p_id = substr($id, 0, strpos($id, '.'));
        $w_id = substr($id, strpos($id, '.')+1);
        $prod = Products::with(['unit'])->where('id',$p_id)->first();
        $type = $prod->vstatus;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {

            if($prod->vstatus == 0)
            {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(unit_quantity)'));
                }])
                ->where('id',$p_id)
                ->first();
            }
            else {
                $product = Products::with(['brands','unit','category','subcategory','variants'])
                // ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id) {
                //     $avg = $query->where('w_id',$w_id)->select(DB::raw('sum(unit_quantity)'));
                // }])
                ->where('id',$p_id)
                ->first();
            }
        }
        else
        {
            if ($prod->vstatus == 0) {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$type);
                }])
                ->where('id',$p_id)
                ->first();
            }
            else {
                $product = Products::with(['brands','unit','category','subcategory','variants'])
                // ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id) {
                //     $avg = $query->select(DB::raw('quantity'))->where('w_id',$w_id);
                // }])
                ->where('id',$p_id)
                ->first();
            }
        }
        // dd($product);
        return $product;
    }

    public function salesProduct($id) //use to fetch product data in sales create
    {
        // dd($id);
        $p_id = substr($id, 0, strpos($id, '.'));
        $type = substr($id, strpos($id, '.')+1);
        $type = substr($type,0, strpos($type, '.'));
        $gst = GST::latest('created_at')->orderBy('id','desc')->first();


        $w_id = substr($id,  strpos($id, '.', strpos($id, '.')+1)+1);
        // dd($type,$w_id,$id,$p_id);
        if($type == 1)
        {
            // dd('1');
            $prod = ProductVariants::find($p_id);
            $product = ProductVariants::with(['product.brands','product.unit','product.category','product.subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(quantity)'));
            }])
            ->where('id',$p_id)
            ->first();
        }
        else
        {
            $prod = Products::with(['unit'])->where('id',$p_id)->first();
            if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
            {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(quantity)'));
                }])
                ->where('id',$p_id)
                ->first();
            }
            else
            {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$type);
                }])
                ->where('id',$p_id)
                ->first();

            }
        }

        return [$product,$type,$gst];
    }

    public function addSingleProduct($id)//creating finish product it shows data of single product
    {
        if(request()->ajax())
        {
            $product = Products::with(['brands','unit','variants'])
            ->where('id',$id)->first();
            return $product;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $variant=Variants::all();
        $p_variant=ProductVariants::where('p_id',$id)->get();
        $product = Products::find($id);
        $fp = FinishProducts::with(['rawproducts.brands','rawproducts.unit','rawproducts2.product.brands','rawproducts2.product.unit'])
        ->where('id',$id)
        ->get();
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $data=[
            'isEdit' => true,
            'product' => $product,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'ct' => CostType::all(),
            'brands' => $brands,
            'variant' => $variant,
            'p_variant' => $p_variant,
            'fp' => $fp,
            'pro' => $pro,

        ];
        return view('product.create',$data);
    }

    public function variants($id)
    {
        $variants = ProductVariants::where('p_id',$id)->get();
        return $variants;
    }

    public function Pvariants($id)
    {
        $v_id = substr($id, 0, strpos($id, '.'));
        $w_id = substr($id, strpos($id, '.')+1);
        $type = 1;
        $variants = ProductVariants::where('id',$v_id)
        ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
            $avg = $query->select(DB::raw('quantity'))->where('w_id',$w_id)->where('type',$type);
        }])
        ->with('product')
        ->where('status',1)
        ->first();
        return $variants;
    }

    public function Pvariantsforfinishproduct($id)
    {
        $variants = ProductVariants::where('id',$id)
        ->with('product.unit')
        ->first();
        return $variants;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $role_id = Auth::user()->r_id;
        $productid = $id;
        $product = Products::find($id);
        $u_id = Auth::user()->id;
        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        $final = 0;

        if(isset($request->p_id))
        {
            DB::table('finish_products')->where('id', $id)->delete();
            for ($i=1; $i <=count($request->p_id) ; $i++)
            {
                $final =$request->quantity[$i] * 1;
                FinishProducts::create([
                    'id' => $id,
                    'p_id' => $request->p_id[$i],
                    'quantity' => $final,
                    'type' => $request->type[$i],
                    'cost' => $request->costf[$i],
                    'costperunit' => $request->costperunit[$i],
                ]);
            }
        }
        if($request->variants[1][0] != null)
        {
            $vstatusp=1;
        }
        else
        {
            $vstatusp=0;
        }
        if($product->vstatus == 0)
        {
            $account = $product->pro_code.' - '.$product->pro_name;
            $ledger = GeneralLedger::where('account_name',$account)
            ->get();
            $account_product = AccountDetails::where('name_of_account',$account)->first();
            AccountDetails::where('id',$account_product->id)
            ->update([
                'name_of_account' => $request->pro_code.' - '.$request->pro_name
            ]);
            if($ledger->isNotEmpty())
            {
                foreach ($ledger as $key => $l) {
                    GeneralLedger::where('id',$l->id)
                    ->update([
                        'account_name' => $request->pro_code.' - '.$request->pro_name
                    ]);
                }
            }
        }
        else
        {
            for ($i=1; $i <= count($request->variants) ; $i++)
            {
                if(isset($request->v_id[$i]))
                {
                    $name = $request->pro_code.' - '.$request->pro_name.'-';
                    for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                        if($j == count($request->variants[$i]) - 1){
                            $name .=$request->variants[$i][$j];
                        }else {
                            $name .=$request->variants[$i][$j].'-';
                        }

                    }

                    $pv = ProductVariants::where('id',$request->v_id[$i])->first();
                    $account_product = AccountDetails::where('name_of_account',$pv->name)->first();
                    $ledger = GeneralLedger::where('account_name',$pv->name)
                    ->get();
                    ProductVariants::where('id',$pv->id)->update([
                       'name' =>  $name
                    ]);
                    AccountDetails::where('id',$account_product->id)
                    ->update([
                        'name_of_account' => $name
                    ]);
                    if($ledger->isNotEmpty())
                    {
                        foreach ($ledger as $key => $l) {
                            GeneralLedger::where('id',$l->id)
                            ->update([
                                'account_name' => $name
                            ]);
                        }
                    }
                }
            }
        }

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {

            if(isset($request->image))
            {
                $data=([
                    'image' => $request->image,
                    's_cat_id' => $request->s_cat_id,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'unit_id' => $unit->id,
                    'cost' => $request->cost,
                    'p_type' => $request->p_type,
                    'alert_quantity' => $request->alert_quantity,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    'visibility' => $request->visibility,
                    'description' => $request->description,
                    'updated_by' => $u_id,
                    'mrp' => $request->mrp,
                    'mstatus' => $request->mstatus,
                    'tp' => $request->tp,
                    'vstatus' => $vstatusp,
                    'ct_id' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->ct_id : null,
                    'otherCost' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->otherCost : null

                ]);
                $data['image']= Storage::disk('uploads')->putFile('',$request->image);
                Products::where('id',$id)
                ->update($data);
            }
            else
            {
                $data=([
                    'pro_code' => $request->pro_code,
                    'pro_name' => $request->pro_name,
                    'p_type' => $request->p_type,
                    'weight' => $request->weight,
                    'description' => $request->description,
                    'cost' => $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'unit_id' => $unit->id,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    's_cat_id' => $request->s_cat_id,
                    'visibility' => $request->visibility,
                    'updated_by' => $u_id,
                    'mrp' => $request->mrp,
                    'mstatus' => $request->mstatus,
                    'tp' => $request->tp,
                    'vstatus' => $vstatusp,
                    'ct_id' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->ct_id : null,
                    'otherCost' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->otherCost : null
                ]);
                Products::where('id',$id)
                ->update($data);
            }
            if($request->variants[1][0] != null)
            {

                for ($i=1; $i <= count($request->variants) ; $i++)
                {
                    if(isset($request->v_id[$i]))
                    {
                        $pv = ProductVariants::where('id',$request->v_id[$i])->first();
                        $pv->update([
                            'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
                            'price' => $request->pricev[$i] == null ? $request->mrp : $request->pricev[$i],
                            'tp' => $request->tpv[$i] == null ? $request->tp : $request->tpv[$i],
                        ]);
                    }
                    else {
                        $name = $request->pro_code.' - '.$request->pro_name.'-';
                        for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                            if($j == count($request->variants[$i]) - 1){
                                $name .=$request->variants[$i][$j];
                            }else {
                                $name .=$request->variants[$i][$j].'-';
                            }

                        }
                        $ProductVariants = ProductVariants::where('p_id',$productid)
                        ->where('name',$name)->first();
                        if($ProductVariants == null)
                        {
                            $pvar = new ProductVariants;
                            $pvar->p_id = $productid;
                            $pvar->name = $name;
                            $pvar->status = 1;
                            $pvar->cost = $request->costv[$i] == null ? $request->cost : $request->costv[$i] ;
                            $pvar->price = $request->pricev[$i] == null ? $request->mrp : $request->pricev[$i] ;
                            $pvar->tp = $request->tpv[$i] == null ? $request->tp : $request->tpv[$i] ;
                            $pvar->save();
                            $hcat = HeadCategory::where('name','Inventory')->first();

                            $account = AccountDetails::where('c_id',$hcat->id)
                            ->latest('created_at')->orderBy('id','desc')->first();

                            if($account == null)
                            {
                                $id = 001;
                            }
                            else
                            {
                                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                            }

                            $str_length = strlen((string)$id)+2;
                            $id = substr("0000{$id}", -$str_length);
                            $code = $hcat->code.'-'.$id;
                            $data1 = [
                                'Code' => $code,
                                'name_of_account' => $name,
                                'c_id' => $hcat->id,
                                'created_by' => $u_id,
                                'type' => 1
                            ];
                            AccountDetails::create($data1);
                        }
                    }
                }
            }

            toastr()->success('Product updated successfully!');
            return redirect(url('').'/product');
        }
        else {
            if(isset($request->image))
            {
                $data=([
                    'id' => $productid,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'cost' => $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'unit_id' => $unit->id,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    's_cat_id' => $request->s_cat_id,
                    'image' => $request->image,
                    'description' => $request->description,
                    'visibility' => $request->visibility,
                    'p_type' => $request->p_type,
                    'mrp' => $request->mrp,
                    'mstatus' => $request->mstatus,
                    'tp' => $request->tp,
                    'vstatus' => $vstatusp,
                ]);
                $data['image']= Storage::disk('uploads')->putFile('',$request->image);
            }
            else
            {
                $data=([
                    'id' => $productid,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'cost' => $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'unit_id' => $unit->id,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    's_cat_id' => $request->s_cat_id,
                    'image' => $request->image,
                    'description' => $request->description,
                    'visibility' => $request->visibility,
                    'p_type' => $request->p_type,
                    'mrp' => $request->mrp,
                    'msatus' => $request->msatus,
                    'tp' => $request->tp,
                    'vstatus' => $vstatusp,
                    'ct_id' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->ct_id : null,
                    'otherCost' => (isset($request->ct_id) && $request->ct_id != 'No Cost') ? $request->otherCost : null
                ]);
            }
            ProductsUpdateRequest::create($data);
            if($request->variants[1][0] != null)
            {
                for ($i=1; $i <= count($request->variants) ; $i++) {
                    if(isset($request->v_id[$i]))
                    {
                        $pv = ProductVariants::where('id',$request->v_id[$i])->first();
                        $pv->update([
                            'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
                            'price' => $request->pricev[$i] == null ? $request->mrp : $request->pricev[$i],
                            'tp' => $request->tpv[$i] == null ? $request->tp : $request->tpv[$i],
                        ]);
                    }
                    else {
                        $name = $request->pro_code.' - '.$request->pro_name.'-';
                        for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                            if($j == count($request->variants[$i]) - 1){
                                $name .=$request->variants[$i][$j];
                            }else {
                                $name .=$request->variants[$i][$j].'-';
                            }

                        }
                        ProductVariantUpdate::create([
                            'p_id' => $productid,
                            'name' => $name,
                            'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null ,
                            'price' => $request->pricev[$i] == null ? $request->mrp : $request->pricev[$i] == null,
                            'tp' => $request->tpv[$i] == null ? $request->tp : $request->tpv[$i] == null,
                        ]);

                    }
                }
            }
            toastr()->success('Product update request sent successfully!');
            return redirect(url('').'/product');
        }
        // dd($request->all());
        // $productid = $id;
        // $product = Products::find($id);
        // $var = ProductVariants::where('p_id',$id)->pluck('id')->toArray();
        // $vid= [];
        // if(isset($request->v_id))
        // {
        //     for ($i=1; $i <= count($request->v_id) ; $i++)
        //     {
        //         array_push($vid,$request->v_id[$i]);
        //     }
        // }

        // $unit = Unit::where('u_name' , $request->unit_id)
        // ->first();

        // if($role_id == $env_a_id || $role_id == $env_m_id)
        // {
        //     $u_id = Auth::user()->id;
        //     if($request->variants[1][0] != null)
        //     {
        //         if(isset($request->image))
        //         {
        //             $data=([
        //                 'image' => $request->image,
        //                 's_cat_id' => $request->s_cat_id,
        //                 'pro_name' => $request->pro_name,
        //                 'pro_code' => $request->pro_code,
        //                 'weight' => $request->weight,
        //                 'unit_id' => $unit->id,
        //                 'cost' => $request->cost,
        //                 'tp' => $request->price,
        //                 'alert_quantity' => $request->alert_quantity,
        //                 'brand_id' => $request->brand_id,
        //                 'cat_id' => $request->cat_id,
        //                 'visibility' => $request->visibility,
        //                 'description' => $request->description,
        //                 'updated_by' => $u_id,
        //                 'mrp' => $request->mrp,
        //                 'mstatus' => $request->mstatus,
        //             ]);
        //             $data['image']= Storage::disk('uploads')->putFile('',$request->image);
        //             Products::where('id',$productid)
        //             ->update($data);
        //         }
        //         else
        //         {
        //             $data=([
        //                 's_cat_id' => $request->s_cat_id,
        //                 'pro_name' => $request->pro_name,
        //                 'pro_code' => $request->pro_code,
        //                 'weight' => $request->weight,
        //                 'unit_id' => $unit->id,
        //                 'cost' => $request->cost,
        //                 'price' => $request->price,
        //                 // 'quantity' => $request->quantity,
        //                 'alert_quantity' => $request->alert_quantity,
        //                 'brand_id' => $request->brand_id,
        //                 'cat_id' => $request->cat_id,
        //                 // 'w_id' => $request->w_id,
        //                 'visibility' => $request->visibility,
        //                 'description' => $request->description,
        //                 'updated_by' => $u_id,
        //                 'mrp' => $request->mrp,
        //                 'mstatus' => $request->mstatus,
        //             ]);
        //             Products::where('id',$productid)
        //             ->update($data);

        //         }
        //         for ($i=1; $i <= count($request->variants) ; $i++) {
        //             if(isset($request->v_id[$i]))
        //             {
        //                 for ($j=0; $j < count($var) ; $j++)/// if any variant is deleted by user then check in ledger if ledger is null delete it
        //                 {
        //                     if(in_array($var[$j],$vid))
        //                     {
        //                         $pv = ProductVariants::where('id',$var[$j])->first();
        //                         $ledger = GeneralLedger::where('account_name',$pv->name)
        //                         ->first();
        //                         if(empty($ledger) && is_null($ledger))
        //                         {
        //                             AccountDetails::where('name_of_account', $pv->name)->delete();
        //                             ProductVariants::where('id',$var[$j])->delete();
        //                             $name = $request->pro_code.' - '.$request->pro_name.'-';
        //                             for ($k=0; $k < count($request->variants[$i]) ; $k++) {
        //                                 if($k == count($request->variants[$i]) - 1){
        //                                     $name .=$request->variants[$i][$k];
        //                                 }else {
        //                                     $name .=$request->variants[$i][$k].'-';
        //                                 }

        //                             }
        //                             $ProductVariants = ProductVariants::where('p_id',$productid)
        //                             ->where('name',$name)->first();
        //                             if($ProductVariants == null)
        //                             {
        //                                 ProductVariants::create([
        //                                     'p_id' => $productid,
        //                                     'name' => $name,
        //                                     'status' => 1,
        //                                     'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
        //                                     'price' => $request->pricev[$i] == null ? $request->price : $request->pricev[$i],
        //                                 ]);
        //                                 $hcat = HeadCategory::where('name','Inventory')->first();

        //                                 $account = AccountDetails::where('c_id',$hcat->id)
        //                                 ->latest('created_at')->first();

        //                                 if($account == null)
        //                                 {
        //                                     $id = 001;
        //                                 }
        //                                 else
        //                                 {
        //                                     $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
        //                                 }

        //                                 $str_length = strlen((string)$id)+2;
        //                                 $id = substr("0000{$id}", -$str_length);
        //                                 $code = $hcat->code.'-'.$id;
        //                                 $data1 = [
        //                                     'Code' => $code,
        //                                     'name_of_account' => $name,
        //                                     'c_id' => $hcat->id,
        //                                     'created_by' => $u_id,
        //                                     'type' => 1
        //                                 ];
        //                                 AccountDetails::create($data1);
        //                             }
        //                             else {
        //                                 $pv->update([
        //                                     'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
        //                                     'price' => $request->pricev[$i] == null ? $request->price : $request->pricev[$i]
        //                                 ]);
        //                             }
        //                             continue;

        //                         }
        //                         else {
        //                             $pv->update([
        //                                 'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
        //                                 'price' => $request->pricev[$i] == null ? $request->price : $request->pricev[$i]
        //                             ]);
        //                         }
        //                         continue;
        //                     }
        //                     else {
        //                             $pv = ProductVariants::where('id',$var[$j])->first();
        //                             $ledger = GeneralLedger::where('account_name',$pv->name)
        //                             ->first();
        //                             if($ledger == null)
        //                             {
        //                                 DB::table('account_details')->where('name_of_account', $pv->name)->delete();
        //                                 DB::table('product_variants')->where('id', $var[$j])->delete();
        //                             }
        //                             else {
        //                                 $pv->update([
        //                                     'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
        //                                     'price' => $request->pricev[$i] == null ? $request->price : $request->pricev[$i]
        //                                 ]);
        //                             }

        //                     }
        //                 }
        //             }
        //             else {
        //                 $name = $request->pro_code.' - '.$request->pro_name.'-';
        //                 for ($j=0; $j < count($request->variants[$i]) ; $j++) {
        //                     if($j == count($request->variants[$i]) - 1){
        //                         $name .=$request->variants[$i][$j];
        //                     }else {
        //                         $name .=$request->variants[$i][$j].'-';
        //                     }

        //                 }
        //                 $ProductVariants = ProductVariants::where('p_id',$productid)
        //                 ->where('name',$name)->first();
        //                 if($ProductVariants == null)
        //                 {
        //                     $pvar = new ProductVariants;
        //                     $pvar->p_id = $productid;
        //                     $pvar->name = $name;
        //                     $pvar->status = 1;
        //                     $pvar->cost = $request->costv[$i] == null ? $request->cost : $request->costv[$i] ;
        //                     $pvar->price = $request->pricev[$i] == null ? $request->price : $request->pricev[$i] ;
        //                     $pvar->save();
        //                     $hcat = HeadCategory::where('name','Inventory')->first();

        //                     $account = AccountDetails::where('c_id',$hcat->id)
        //                     ->latest('created_at')->first();

        //                     if($account == null)
        //                     {
        //                         $id = 001;
        //                     }
        //                     else
        //                     {
        //                         $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
        //                     }

        //                     $str_length = strlen((string)$id)+2;
        //                     $id = substr("0000{$id}", -$str_length);
        //                     $code = $hcat->code.'-'.$id;
        //                     $data1 = [
        //                         'Code' => $code,
        //                         'name_of_account' => $name,
        //                         'c_id' => $hcat->id,
        //                         'created_by' => $u_id,
        //                         'type' => 1
        //                     ];
        //                     AccountDetails::create($data1);
        //                 }
        //                 else {
        //                     continue;
        //                 }

        //             }
        //         }
        //     }
        //     else
        //     {
        //         if(isset($request->image))
        //         {
        //             $data=([
        //                 'image' => $request->image,
        //                 's_cat_id' => $request->s_cat_id,
        //                 'pro_name' => $request->pro_name,
        //                 'pro_code' => $request->pro_code,
        //                 'weight' => $request->weight,
        //                 'unit_id' => $unit->id,
        //                 'cost' => $request->cost,
        //                 'p_type' => $request->p_type,
        //                 'price' => $request->price,
        //                 'alert_quantity' => $request->alert_quantity,
        //                 'brand_id' => $request->brand_id,
        //                 'cat_id' => $request->cat_id,
        //                 'visibility' => $request->visibility,
        //                 'description' => $request->description,
        //                 'updated_by' => $u_id,
        //                 'mrp' => $request->mrp,
        //                 'mstatus' => $request->mstatus,
        //             ]);
        //             $data['image']= Storage::disk('uploads')->putFile('',$request->image);
        //             Products::where('id',$productid)
        //             ->update($data);
        //         }
        //         else
        //         {
        //             $data=([
        //                 's_cat_id' => $request->s_cat_id,
        //                 'pro_name' => $request->pro_name,
        //                 'pro_code' => $request->pro_code,
        //                 'weight' => $request->weight,
        //                 'unit_id' => $unit->id,
        //                 'cost' => $request->cost,
        //                 'price' => $request->price,
        //                 'p_type' => $request->p_type,
        //                 // 'quantity' => $request->quantity,
        //                 'alert_quantity' => $request->alert_quantity,
        //                 'brand_id' => $request->brand_id,
        //                 'cat_id' => $request->cat_id,
        //                 // 'w_id' => $request->w_id,
        //                 'visibility' => $request->visibility,
        //                 'description' => $request->description,
        //                 'updated_by' => $u_id,
        //                 'mrp' => $request->mrp,
        //                 'mstatus' => $request->mstatus,

        //             ]);
        //             Products::where('id',$productid)
        //             ->update($data);
        //         }
        //         if(count($var)<=0)
        //         {
        //         }
        //         else
        //         {
        //             foreach ($var as $key => $va) {
        //                 $ledger = GeneralLedger::where('account_name',$va->name)
        //                 ->first();
        //                 if($ledger == null)
        //                 {
        //                     DB::table('product_variants')->where('id', $va->id)->delete();
        //                 }
        //                 else
        //                 {

        //                 }
        //             }
        //         }

        //         $final = 0;
        //         if(isset($request->p_id))
        //         {

        //             if(count($request->p_id) > 0)
        //             {
        //                 $product = Products::find($productid);
        //                 DB::table('finish_products')->where('id', $productid)->delete();
        //                 for ($i=1; $i <= count($request->p_id) ; $i++) {
        //                     if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
        //                     {
        //                         $final = 1 * $request->quantity[$i];
        //                     }
        //                     else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
        //                     {
        //                         $final = 1 * $request->quantity[$i] ;
        //                     }
        //                     else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
        //                     {
        //                         $final = 1 * $request->quantity[$i];
        //                     }
        //                     else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
        //                     {
        //                         $final = 1 * $request->quantity[$i] ;
        //                     }
        //                     else
        //                     {
        //                         $final =$request->quantity[$i] * 1;
        //                     }
        //                     FinishProducts::create([
        //                         'id' => $productid,
        //                         'p_id' => $request->p_id[$i],
        //                         'quantity' => $final,
        //                         'type' => $request->type[$i]
        //                     ]);
        //                 }
        //             }
        //         }

        //     }

        //     toastr()->success('Product updated successfully!');
        //     return redirect(url('').'/product');

        // }
        // else
        // {
        //     if(isset($request->image))
        //     {
        //         $data=([
        //             'id' => $productid,
        //             'pro_name' => $request->pro_name,
        //             'pro_code' => $request->pro_code,
        //             'weight' => $request->weight,
        //             'cost' => $request->cost,
        //             'price' => $request->price,
        //             'alert_quantity' => $request->alert_quantity,
        //             'unit_id' => $unit->id,
        //             'brand_id' => $request->brand_id,
        //             'cat_id' => $request->cat_id,
        //             's_cat_id' => $request->s_cat_id,
        //             'image' => $request->image,
        //             'description' => $request->description,
        //             'visibility' => $request->visibility,
        //             'p_type' => $request->p_type,
        //             'mrp' => $request->mrp,
        //             'mstatus' => $request->mstatus,
        //         ]);
        //         $data['image']= Storage::disk('uploads')->putFile('',$request->image);
        //     }
        //     else
        //     {
        //         $data=([
        //             'id' => $productid,
        //             'pro_name' => $request->pro_name,
        //             'pro_code' => $request->pro_code,
        //             'weight' => $request->weight,
        //             'cost' => $request->cost,
        //             'price' => $request->price,
        //             'alert_quantity' => $request->alert_quantity,
        //             'unit_id' => $unit->id,
        //             'brand_id' => $request->brand_id,
        //             'cat_id' => $request->cat_id,
        //             's_cat_id' => $request->s_cat_id,
        //             'image' => $request->image,
        //             'description' => $request->description,
        //             'visibility' => $request->visibility,
        //             'p_type' => $request->p_type,
        //             'mrp' => $request->mrp,
        //             'msatus' => $request->msatus,
        //         ]);
        //     }
        //     ProductsUpdateRequest::create($data);
        //     if($request->variants[1][0] != null)
        //     {
        //         for ($i=1; $i <= count($request->variants) ; $i++) {
        //             if(isset($request->v_id[$i]))
        //             {
        //                 for ($j=0; $j < count($var) ; $j++)/// if any variant is deleted by user then check in ledger if ledger is null delete it
        //                 {
        //                     if(in_array($var[$j],$vid))
        //                     {
        //                         $pv = ProductVariants::find($var[$j]);
        //                         $ledger = GeneralLedger::where('account_name',$pv->name)
        //                         ->first();
        //                         if($ledger == null)
        //                         {
        //                             DB::table('product_variants')->where('id', $var[$j])->delete();
        //                             DB::table('account_details')->where('name_of_account', $pv->name)->delete();
        //                             $name = $request->pro_code.' - '.$request->pro_name.'-';
        //                             for ($k=0; $k < count($request->variants[$i]) ; $k++) {
        //                                 if($k == count($request->variants[$i]) - 1){
        //                                     $name .=$request->variants[$i][$k];
        //                                 }else {
        //                                     $name .=$request->variants[$i][$k].'-';
        //                                 }

        //                             }
        //                             ProductVariantUpdate::create([
        //                                 'p_id' => $productid,
        //                                 'name' => $name,
        //                                 'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null ,
        //                                 'price' => $request->pricev[$i] == null ? $request->price : $request->pricv[$i] == null,
        //                             ]);
        //                             continue;
        //                         }
        //                         else {
        //                             # code...
        //                         }
        //                     }
        //                     else {
        //                         $pv = ProductVariants::find($var[$j]);
        //                         $ledger = GeneralLedger::where('account_name',$pv->name)
        //                         ->first();
        //                         if($ledger == null)
        //                         {
        //                             DB::table('product_variants')->where('id', $var[$j])->delete();
        //                             DB::table('account_details')->where('name_of_account', $pv->name)->delete();
        //                         }
        //                         else {
        //                             # code...
        //                         }

        //                     }
        //                 }
        //             }
        //             else {
        //                 $name = $request->pro_code.' - '.$request->pro_name.'-';
        //                 for ($j=0; $j < count($request->variants[$i]) ; $j++) {
        //                     if($j == count($request->variants[$i]) - 1){
        //                         $name .=$request->variants[$i][$j];
        //                     }else {
        //                         $name .=$request->variants[$i][$j].'-';
        //                     }

        //                 }
        //                 ProductVariantUpdate::create([
        //                     'p_id' => $productid,
        //                     'name' => $name,
        //                     'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null ,
        //                     'price' => $request->pricev[$i] == null ? $request->price : $request->pricv[$i] == null,
        //                 ]);

        //             }
        //         }
        //     }
        //     else
        //     {
        //         if(count($var)<=0)
        //         {
        //         }
        //         else
        //         {
        //             foreach ($var as $key => $va) {
        //                 $ledger = GeneralLedger::where('account_name',$va->name)
        //                 ->first();
        //                 if($ledger == null)
        //                 {
        //                     DB::table('product_variants')->where('id', $va->id)->delete();
        //                 }
        //                 else
        //                 {

        //                 }
        //             }
        //         }

        //         $final = 0;
        //         if(count($request->p_id) > 0)
        //         {
        //             for ($i=1; $i <= count($request->p_id) ; $i++) {
        //                 if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
        //                 {
        //                     $final = 1 * $request->quantity[$i];
        //                 }
        //                 else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
        //                 {
        //                     $final = 1 * $request->quantity[$i] ;
        //                 }
        //                 else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
        //                 {
        //                     $final = 1 * $request->quantity[$i];
        //                 }
        //                 else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
        //                 {
        //                     $final = 1 * $request->quantity[$i] ;
        //                 }
        //                 else
        //                 {
        //                     $final =$request->quantity[$i] * 1;
        //                 }
        //                 FinishProducts::create([
        //                     'id' => $p->id,
        //                     'p_id' => $request->p_id[$i],
        //                     'quantity' => $final,
        //                     'type' => $request->type[$i]
        //                 ]);
        //             }
        //         }

        //     }


        //     toastr()->success('Product update request sent successfully successfully!');
        //     return redirect(url('').'/product');

        // }
    }

    public function excel()
    {
        return Excel::download(new ProductsExport, 'Products.xlsx');
    }

    public function Finishexcel()
    {
        return Excel::download(new FinishProductExport, 'FinishProducts.xlsx');
    }
}
