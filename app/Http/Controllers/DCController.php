<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DC;
use App\DCdetails;
use App\Warehouse;
use App\Vendors;
use App\Products;
use App\ProductVariants;
use App\Unit;
use App\User;
use App\Stocks;
use App\StockOut;
use App\City;
use App\Bank;
use App\Brands;
use App\Category;
use App\Subcategory;
use App\Groups;
use App\HeadCategory;
use App\CurrentStock;
use App\AccountDetails;
use App\GeneralLedger;
use App\UserMenu;
use App\GST;
use App\SaleType;
use App\Roles;
use DataTables;
use Storage;
use DB;
use PDF;
use Auth;
use Response;
use Session;
use Carbon\Carbon;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class DCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        $role_id = Auth::user()->r_id;
        $adminName = Auth::user()->name;

        $roleName = Roles::find($role_id);
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $counter = null;
        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $counter = null;
        }
        else
        {
            $counter = Session::get('assign');
        }

        if($roleName->name == 'Admin')
        {
            $sales=DC::with(['warehouse','customer','biller','saleperson','saletype'])
            ->get();
        }
        if($roleName->name == 'Sale Person')
        {
            $email = Auth::user()->email;
            $sp = Vendors::where('email',$email)->where('v_type','Saleperson')->first();
            $sales=DC::with(['warehouse','customer','biller','saleperson','saletype'])
            ->where(function ($query) {
                $query->where('d_status','Pending')
                ->orWhere('d_status','Approved');
            })
            ->where('sp_id',$sp->id)
            ->get();
        }

        if($roleName->name == 'warehouse person')
        {
            $sales=DC::with(['warehouse','customer','biller','saleperson','saletype'])
            ->where('d_status','Partial')
            ->orWhere('d_status','Approved')
            ->get();
        }

        if($roleName->name == 'Sale Manager')
        {
            $spname = config('app.salepersons');
            $spid = [];
            foreach ($spname as $key => $value) {
                $sp = Vendors::where('v_type', 'Saleperson')->where('name',$value)
                ->first();
                array_push($spid,$sp->id);
            }
            $sales=DC::with(['warehouse','customer','biller','saleperson','saletype'])
            ->whereIn('sp_id',$spid)
            ->get();
        }

        if($roleName->name == 'DeliveryBoy')
        {
            $sales=DC::with(['warehouse','customer','biller','saleperson','saletype'])
            ->where('d_status','Partial')
            ->orWhere('d_status','Complete')
            ->get();
        }

        $customer=Vendors::where('v_type','Customer')->where('status',1)->get();
        $saleperson=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        $bank=Bank::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        // dd($sales);

        $index = 0;
        return view('dc.index',compact('adminName','roleName','customer','bank','permissions','menu_id','sales','saleperson','index','counter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $dc=DC::max('id');
        if($dc == null)
        {
            $id=1;
        }
        else
        {
            $id=$dc+1;
        }
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $person=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        $customer=Vendors::where('v_type','Customer')->get();
        $ware=Warehouse::all();
        $bank=Bank::where('status',1)->get();
        $saletype=SaleType::all();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city = City::where('status',1)->get();
        $cgroup=Groups::where('g_type','Customer')->get();
        $pgroup=Groups::where('g_type','Price')->get();
        $date=Carbon::now()->format('Y-m-d');
        $data=[
            'isEdit' => false,
            'customer' => $customer,
            'biller' => $biller,
            'person' => $person,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'bank' => $bank,
            'sub' => $sub,
            'brands' => $brands,
            'city' => $city,
            'cgroup' => $cgroup,
            'pgroup' => $pgroup,
            'date' => $date,
            'id' => $id,
            'stype' => SaleType::all(),
            'permissions' => getRolePermission($menu_id)
        ];
        return view('dc.create',$data);
    }

    /// function to get invoice number according to invoice format

    public function saleFormat($format,$biller)
    {
        $id = DC::where('Iformat',$format)->where('b_id',$biller)->max('id');
        if($id == null)
        {
            return 1;
        }
        else {

            $Ino = DC::where('id',$id)->pluck('Ino');
            if($Ino[0] == '')
            {
                return 1;
            }
            else {
                return $Ino[0]+1;
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->final == 'NaN')
        {
            toastr()->error('Network error!');
            return redirect()->back();
        }
        if(!(isset($request->p_id)))
        {
            toastr()->error('Select Products to create DC !');
            return redirect()->back();
        }
        else
        {

            $d_status='';
            $u_id = Auth::user()->id;

            $role_id = Auth::user()->r_id;
            $env_a_id = config('app.adminId');
            $env_m_id = config('app.managerId');
            if($role_id == $env_a_id || $role_id == $env_m_id)
            {
                $d_status = 'Approved';
            }
            else
            {
                $d_status = 'Pending';
            }

            if(isset($request->doc))
            {
                $data=([
                    'date' => $request->date,
                    'ref_no' => $request->ref_no,
                    'b_id' => $request->b_id,
                    'c_id' => $request->c_id,
                    'w_id' => $request->w_id,
                    's_address' => $request->s_address,
                    'd_status' => $d_status,
                    'doc' => $request->doc,
                    'total' => $request->final,
                    'note' => $request->editor1,
                    'created_by' => $u_id,
                    'sp_id' => $request->sp_id,
                    'expected_date' => $request->expected_date,
                    'pay_type' => $request->pay_type,
                    'Iformat' => $request->Iformat,
                    'Itype' => $request->Itype,
                    'Ino' => $request->Ino,
                    'pdate' => $request->pdate,
                    'remarks' => $request->remarks,
                    'st_id' => $request->st_id,
                ]);
                $data['doc']= Storage::disk('uploads')->putFile('',$request->doc);
            }
            else
            {
                $data=([
                    'ref_no' => $request->ref_no,
                    'date' => $request->date,
                    'pdate' => $request->pdate,
                    'b_id' => $request->b_id,
                    'c_id' => $request->c_id,
                    'w_id' => $request->w_id,
                    's_address' => $request->s_address,
                    'd_status' => $d_status,
                    'total' => $request->final,
                    'note' => $request->editor1,
                    'created_by' => $u_id,
                    'sp_id' => $request->sp_id,
                    'expected_date' => $request->expected_date,
                    'st_id' => $request->st_id,
                    'pay_type' => $request->pay_type,
                    'Iformat' => $request->Iformat,
                    'Itype' => $request->Itype,
                    'Ino' => $request->Ino,
                    'remarks' => $request->remarks,
                ]);
            }

            $p=DC::create($data);

            if(isset($request->vet))
            {
                for ($i=0; $i <count($request->p_id) ; $i++) {
                    DCdetails::create([
                        's_id' => $p->id,
                        'p_id' => $request->p_id[$i],
                        'quantity' => $request->quantity[$i],
                        'sub_total' => $request->sub_total[$i],
                        'price' => $request->price[$i],
                        'cost' => $request->cost[$i],
                        'discount_percent' => $request->discount_percent[$i] == 'NaN' ? '0' : $request->discount_percent[$i] ,
                        'discounted_amount' => $request->discounted_amount[$i],
                        'type' => $request->type[$i],
                        'vet' => $request->vet[$i],
                        'taxA' => $request->taxA[$i],
                        'afterDiscount' => $request->ad[$i],
                    ]);
                }
            }
            else {
                for ($i=0; $i <count($request->p_id) ; $i++) {
                    DCdetails::create([
                        's_id' => $p->id,
                        'p_id' => $request->p_id[$i],
                        'quantity' => $request->quantity[$i],
                        'sub_total' => $request->sub_total[$i],
                        'price' => $request->price[$i],
                        'cost' => $request->cost[$i],
                        'discount_percent' => $request->discount_percent[$i] == 'NaN' ? '0' : $request->discount_percent[$i] ,
                        'discounted_amount' => $request->discounted_amount[$i],
                        'type' => $request->type[$i],
                    ]);
                }
            }



            $u_name = Auth::user()->name;
            $user = User::whereIn('r_id',[config('app.managerId'),config('app.adminId')])
            ->get();
            $notify = [
                'notification' => 'New dc generated by '.$u_name,
                'link' => url('')."/dc",
                'name' => 'View DC;s',
            ];
            Notification::send($user, new AddNotification($notify));

            toastr()->success('DC Added successfully!');
            // Session::flash('download', '/sales/pdf');
            return redirect(url('').'/dc');
            // dd('done');
        }
    }


    public function status(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $u_id = Auth::user()->id;
        // $gdn_no = rand();

        if($status == 'Pending' || $status == 'Approved')
        {
            $item = DC::find($id);
            if ($item->update(['d_status' => $status])) {
                DC::where('id',$id)
                ->update([
                    'd_status' => $status,
                    'updated_by' => $u_id
                ]);
                $u_name = Auth::user()->name;
                $user = User::whereIn('r_id',[config('app.warehouse'),config('app.adminId')])
                ->get();
                $notify = [
                    'notification' => 'DC number '.$id.' is approved by '.$u_name,
                    'link' => url('')."/dc",
                    'name' => 'View DCs',
                ];
                Notification::send($user, new AddNotification($notify));
                $response['status'] = $status;
                $response['message'] = 'status updated successfully.';
                return response()->json($response, 200);
            }
        }
        else if($status == 'Delivered')
        {
            $item = DC::find($id);
            if ($item->update(['d_status' => $status])) {
                DC::where('id',$id)
                ->update([
                    'd_status' => $status,
                    'updated_by' => $u_id
                ]);
                $u_name = Auth::user()->name;
                $user = User::whereIn('r_id',[config('app.managerId'),config('app.adminId')])
                ->get();
                $notify = [
                    'notification' => 'DC number '.$id.' is delivered by '.$u_name,
                    'link' => url('')."/dc",
                    'name' => 'View DCs',
                ];
                Notification::send($user, new AddNotification($notify));
                $response['status'] = $status;
                $response['message'] = 'status updated successfully.';
                return response()->json($response, 200);
            }
        }
        else
        {
            return response()->json($response, 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p=DC::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$id)
        ->first();
        $d=DCdetails::with(['products','products.brands','products.unit','dc','variant','variant.product.brands','variant.product.unit'])
        ->where('s_id',$id)
        ->get();
        return [$p,$d];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = UserMenu::where('route','dc.create')->first();
        $permissions     =   getRolePermission($menu->id);
        // dd($permissions);
        $sale=DC::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$id)
        ->first();
        $saledetail=DCdetails::with(['products.currentstocks','products','products.brands','products.unit','dc',
        'variant.currentstocks','variant.product.brands','variant.product.unit'])
        ->where('s_id',$id)
        ->get();
        // dd($sale);
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $person=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        $customer=Vendors::where('v_type','Customer')->get();
        $ware=Warehouse::all();
        $bank=Bank::all();
        $product=CurrentStock::with(['products','products.brands','products.unit','products.category','products.subcategory','variant.product.brands'])
        ->where('w_id',$sale->w_id)
        ->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city = City::where('status',1)->get();
        $cgroup=Groups::where('g_type','Customer')->get();
        $pgroup=Groups::where('g_type','Price')->get();
        $date=Carbon::now()->format('Y-m-d');
        $gst = GST::latest('created_at')->orderBy('id','desc')->first();
        $customers = Vendors::find($sale->c_id);
        $balance = 0;
        $cus_balance = 0;
        $credit = 0;
        $debit = 0;
        if($customers->balance != null)
        {
            $account =AccountDetails::with('generalLedger')->where('name_of_account',$customers->company.' - '.$customers->name)
            ->where('Code','like','CA-01%')
            ->where('Code','Not like','NCA-01%')
            ->first();
            if($account!=null)
            {
                for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                {
                    $credit += $account->generalLedger[$k]->credit;

                    $debit += $account->generalLedger[$k]->debit;
                }
                $balance =  $debit - $credit;
                $cus_balance = $customer->balance;
            }
            else {
                $balance = 0;
                $cus_balance = 0;
            }
        }
        else {
            $account =AccountDetails::with('generalLedger')->where('name_of_account',$customers->company.' - '.$customers->name)
            ->where('Code','like','CA-01%')
            ->where('Code','Not like','NCA-01%')
            ->first();
            if($account!=null)
            {
                for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                {
                    $credit += $account->generalLedger[$k]->credit;

                    $debit += $account->generalLedger[$k]->debit;
                }
                $balance =  $debit - $credit;
                $cus_balance = 0;
            }
            else {
                $balance = 0;
                $cus_balance = 0;
            }
        }
        $data=[
            'isEdit' => true,
            'customer' => $customer,
            'biller' => $biller,
            'person' => $person,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'bank' => $bank,
            'sub' => $sub,
            'brands' => $brands,
            'product' => $product,
            'city' => $city,
            'cgroup' => $cgroup,
            'pgroup' => $pgroup,
            'date' => $date,
            'id' => $id,
            'sale' => $sale,
            'saledetail' => $saledetail,
            'gst'        => $gst,
            'balance'    => $balance,
            'cus_balance' => $cus_balance,
            'stype' => SaleType::all(),
            'permissions' => $permissions
        ];
        return view('dc.create',$data);
    }

    public function pdf1()
    {
        $id=DC::max('id');
        return $this->pdf($id);
    }

    public function pdf($id)
    {
        $credit = 0;
        $debit = 0;
        $sales=DC::with(['warehouse','customer','biller.attachment','saleperson'])
        ->where('id',$id)
        ->first();
        $saledetail=DCdetails::with(['products','products.brands','products.unit','dc','variant','variant.product.brands','variant.product.unit'])
        ->where('s_id',$id)
        ->get();
        $account=AccountDetails::with('generalLedger')->where('name_of_account','Like','%'.$sales->customer->name)
        ->where('Code','like','CA-01%')
        ->first();
        if($account!=null)
        {
            for($k = 0 ; $k < count($account->generalLedger) ; $k++)
            {
                $credit += $account->generalLedger[$k]->credit;

                $debit += $account->generalLedger[$k]->debit;
            }
            $balance =  $debit - $credit;
        }
        else {
            $balance = 0;
        }
        // dd($thd);
        $pdf = PDF::loadView('dc.pdf', compact('sales','saledetail','balance'));
        // return view('sales.pdf', compact('sales','saledetail','th','balance','thd'));
        return $pdf->download('DC'.$sales->Ino.'.pdf');
        // return redirect()->back();
    }

    public function document($id)
    {
        $sales = DC::find($id);
        $file= public_path(). "/uploads/". $sales->doc;
        // return  Storage::download($file);
        $headers = array(
                'Content-Type: application/pdf',
                );

        return Response::download($file, $sales->doc, $headers);
    }

    public function GDN($id)
    {
        $p = DC::find($id);
        $purchase = DCdetails::with('stock','products','products.brands','variant')
        ->where('s_id',$id)
        ->get();
        return $purchase;
    }

    public function GDR($gdn,$sid)
    {
        $sales=DC::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$sid)
        ->first();
        $sdetail=StockOut::with(['products','products.unit','products.brands','variant.product.unit'])
        ->where('gdn_no',$gdn)
        ->get();
        $pdf = PDF::loadView('dc.gdr', compact('sales','sdetail','gdn'));
        // return view('porder.grr', compact('purchase','pdetail','grn'));

        return $pdf->download('GDN.pdf');
    }

    public function GDR1($id)
    {
        $sid = substr($id, strrpos($id, '-') + 1);
        $gdn = substr($id, 0, strpos($id, "-"));
        $sales=DC::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$sid)
        ->first();
        $sdetail=StockOut::with(['products','products.unit','products.brands','variant.product.unit'])
        ->where('gdn_no',$gdn)
        ->get();
        $pdf = PDF::loadView('dc.gdr', compact('sales','sdetail','gdn'));
        // return view('porder.grr', compact('purchase','pdetail','grn'));

        return $pdf->download('GDN.pdf');
    }

    public function delivery(Request $request)
    {
        // dd($request->all());
        // dd($request->p_total1);
        $amount = 0;
        $u_id = Auth::user()->id;
        $date=Carbon::now()->format('Y-m-d');
        $gdn_no = rand();
        $item = DC::find($request->s_id);
        $details=DCdetails::where('s_id',$request->s_id)
        ->get();
        $customer = Vendors::find($item->c_id);
        for ($j=0; $j < count($request->p_id)  ; $j++) {
            if($request->type[$j] == 0)
            {

                $product = Products::with('unit')
                ->where('id',$request->p_id[$j])
                ->first();
                // ->where('type',$request->type[$j])

                if($request->delivered_quantity[$j] == 0)
                {

                }
                else
                {
                    $prod=CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->first();

                    if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                    {
                        $unit_quantity = $request->delivered_quantity[$j];
                    }
                    else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                    {
                        $unit_quantity =  $request->delivered_quantity[$j];

                    }
                    else
                    {
                        $unit_quantity = null;
                    }

                    if($prod->unit_quantity == null)//changes
                    {
                        $u_quan=null;
                    }
                    else
                    {
                        $u_quan = $prod->unit_quantity - $unit_quantity;
                    }
                    if($prod->quantity == null)
                    {
                        $quan = null;
                    }
                    else {
                        $quan=$prod->quantity - $request->delivered_quantity[$j];

                    }

                    //for current stock

                    $amount += $details[$j]->sub_total;
                    CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->update([
                        'quantity' => $quan,
                        'unit_quantity' => $u_quan
                    ]);
                    StockOut::create([
                        'p_id' => $request->p_id[$j],
                        'quantity' => $request->delivered_quantity[$j],
                        'stock_date' => $date,
                        'sale_d_id' => $request->id[$j],
                        'price' => $request->subtotal[$j],
                        'created_by' => $u_id,
                        'w_id' => $request->w_id,
                        'gdn_no' => $gdn_no,
                        's_type' => 'DC',
                        'type' => $request->type[$j]
                    ]);
                }
            }
            else
            {
                $product = ProductVariants::with('product.unit')->where('id',$request->p_id[$j])
                ->first();

                if($request->delivered_quantity[$j] == 0)
                {

                }
                else
                {
                    $prod=CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->first();

                    if($product->product->unit->u_name == 'Liter' || $product->product->unit->u_name == 'Kilograms')
                    {
                        $unit_quantity = ( 1000) * $request->delivered_quantity[$j];
                    }
                    else if($product->product->unit->u_name == 'Mililiter' || $product->product->unit->u_name == 'Grams')
                    {
                        $unit_quantity =  $request->delivered_quantity[$j];
                    }
                    else
                    {
                        $unit_quantity = null;
                    }

                    if($prod->unit_quantity == null)//changes
                    {
                        $u_quan=null;
                    }
                    else
                    {
                        $u_quan = $prod->unit_quantity - $unit_quantity;
                    }
                    if ($prod->quantity == null) {
                        $quan = null;
                    } else {
                        $quan=$prod->quantity - $request->delivered_quantity[$j];
                    }



                    $amount += $details[$j]->sub_total;
                    CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->update([
                        'quantity' => $quan,
                        'unit_quantity' => $u_quan
                    ]);
                    StockOut::create([
                        'p_id' => $request->p_id[$j],
                        'quantity' => $request->delivered_quantity[$j],
                        'stock_date' => $date,
                        'sale_d_id' => $request->id[$j],
                        'price' => $request->subtotal[$j],
                        'created_by' => $u_id,
                        'w_id' => $request->w_id,
                        'gdn_no' => $gdn_no,
                        's_type' => 'DC',
                        'type' => $request->type[$j]
                    ]);
                }
            }

        }

        DC::where('id',$request->s_id)
        ->update([
            'd_status' => $request->d_status,
            'updated_by' => $u_id
        ]);

        for ($j=0; $j < count($request->p_id)  ; $j++) {
            $delivered_quantity = 0;
            $pd=DCdetails::find($request->id[$j]);
            if($pd->delivered_quantity == 0)
            {
                DCdetails::where('id',$request->id[$j])
                ->update([
                    'delivered_quantity' => $request->delivered_quantity[$j]
                ]);
            }
            else
            {
                $delivered_quantity = $pd->delivered_quantity + $request->delivered_quantity[$j];
                DCdetails::where('id',$request->id[$j])
                ->update([
                    'delivered_quantity' => $delivered_quantity
                ]);
            }
        }
        $product = [];
        $account_i = [];

        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');
        $gl = GeneralLedger::max('id');
        if($gl == null)
        {
            $link_id = 1;
        }
        else
        {
            $ledger = GeneralLedger::where('id',$gl)->first();
            $link_id = $ledger->link_id + 1;
        }

        for ($j=0; $j < count($request->p_id) ; $j++) {
            if($request->type[$j] == 0)
            {
                $product[] = Products::find($request->p_id[$j]);
            }
            else
            {
                $product[] = ProductVariants::find($request->p_id[$j]);
            }
        }

        for ($i=0; $i < count($request->p_id) ; $i++){
            if($request->type[$i] == 0)
            {
                $account_i[] = AccountDetails::where('name_of_account',$product[$i]->pro_code.' - '.$product[$i]->pro_name)
                ->first();
            }
            else
            {
                $account_i[] = AccountDetails::where('name_of_account',$product[$i]->name)
                ->first();
            }
        }

        $final_amount = 0;
        $taxAmount = 0;
        $subTotals = 0;

        $final_amount = $request->p_total1;

        $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
        ->first();
        // dd($account_c);
        $customer_l = GeneralLedger::where('account_code',$account_c->Code)
        ->get();
        $account_s = AccountDetails::where('name_of_account','Sales')->first();
        $sales = GeneralLedger::where('account_code',$account_s->Code)
        ->get();
        $sales_n = 0;
        $sales_d = 0;
        $sales_c = 0;
        if($sales->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to '.$customer->name,
                'account_name' => $account_s->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_s->Code,
                'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => '0',
                'credit' => $final_amount,
                'net_value' =>  0 - $final_amount,
                'balance' =>  0 - $final_amount
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($sales as $key => $d) {
                $sales_d+=$d->debit;
                $sales_c+=$d->credit;
                $balance+=$d->net_value;
            }
            $sales_c += $final_amount;
            // $sales_n = $sales_d - $sales_c;
            $sales_n = $balance + (0 - $final_amount);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to '.$customer->name,
                'account_name' => $account_s->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_s->Code,
                'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => '0',
                'credit' => $final_amount,
                'net_value' => 0 - $final_amount,
                'balance' => $sales_n
            ]);
        }

        $customer_d = 0;
        $customer_c = 0;
        $customer_n = 0;
        if($customer_l->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to'.$customer->name,
                'account_name' => $account_c->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_c->Code,
                'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => $final_amount,
                'credit' => '0',
                'net_value' => $final_amount,
                'balance' => $final_amount
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($customer_l as $key => $c) {
                $customer_c+=$c->credit;
                $customer_d+=$c->debit;
                $balance+=$c->net_value;
            }
            $customer_d += $final_amount;
            // $customer_n = $customer_d - $customer_c;
            $customer_n = $balance + ($final_amount - 0);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to '.$customer->name,
                'account_name' => $account_c->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_c->Code,
                'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => $final_amount,
                'credit' => '0',
                'net_value' => $final_amount,
                'balance' => $customer_n
            ]);
        }
        for ($k=0; $k < count($account_i) ; $k++) {
            if($request->delivered_quantity[$k] == 0)
            {

            }
            else
            {
                $balance = 0;
                $net_d_p = 0;
                $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                ->where('w_id',$item->w_id)
                ->get();
                // dd($debit_p);
                if($debit_p->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sale product for sale number: '.$item->id,
                        'account_name' => $account_i[$k]->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_i[$k]->Code,
                        'transaction_no' => $gdn_no,
                        'currency_code' => 'PKR',
                        'stock_out' => $request->delivered_quantity[$k],
                        'stock_in' => '0',
                        'net_value' => 0 - $request->delivered_quantity[$k],
                        'balance' => 0 - $request->delivered_quantity[$k],
                        'credit' =>  $request->cost[$k] * $request->delivered_quantity[$k],
                        'debit' => 0,
                        'w_id' => $item->w_id,
                        'type' => $request->type[$k],
                        'amount' => 0 - $request->cost[$k]
                    ]);
                }
                else
                {
                    foreach ($debit_p as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $net_d_p = $balance + ( 0 - $request->delivered_quantity[$k] );
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sale product for sale number: '.$item->id,
                        'account_name' => $account_i[$k]->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_i[$k]->Code,
                        'transaction_no' => $gdn_no,
                        'currency_code' => 'PKR',
                        'stock_out' => $request->delivered_quantity[$k],
                        'stock_in' => '0',
                        'net_value' => 0 - $request->delivered_quantity[$k],
                        'balance' => $net_d_p,
                        'credit' => $request->cost[$k] * $request->delivered_quantity[$k],
                        'debit' => 0,
                        'w_id' => $item->w_id ,
                        'type' => $request->type[$k],
                        'amount' => 0 - $request->cost[$k]
                    ]);
                }

            }
        }

        $u_name = Auth::user()->name;
        $user = User::whereIn('r_id',[config('app.managerId'),config('app.adminId')])
        ->get();
        $notify = [
            'notification' => 'DC number'.$request->s_id.' delivery package is ready '.$u_name,
            'link' => url('')."/dc",
            'name' => 'View DCs',
        ];
        Notification::send($user, new AddNotification($notify));


        return $this->GDR($gdn_no,$request->s_id);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->final == 'NaN')
        {
            toastr()->error('Network error!');
            return redirect()->back();
        }
        if(!(isset($request->p_id)))
        {
            toastr()->error('Select Products to create DC !');
            return redirect()->back();
        }
        else
        {
            $s_status='';
            $u_id = Auth::user()->id;
            if($request->doc == null)
            {
                $doc = $request->doc1;
            }
            else
            {
                $doc= Storage::disk('uploads')->putFile('',$request->doc);
            }
            $s_status = 'Pending';

            DC::where('id',$id)
            ->update([
                'ref_no' => $request->ref_no,
                'date' => $request->date,
                'pdate' => $request->pdate,
                'b_id' => $request->b_id,
                'c_id' => $request->c_id,
                'w_id' => $request->w_id,
                's_address' => $request->s_address,
                'd_status' => $s_status,
                'doc' => $doc,
                'total' => $request->final,
                'note' => $request->editor1,
                'updated_by' => $u_id,
                'sp_id' => $request->sp_id,
                'expected_date' => $request->expected_date,
                'st_id' => $request->st_id,
                'pay_type' => $request->pay_type,
                'remarks' => $request->remarks,
            ]);
            DB::table('dc_details')->where('s_id', $id)->delete();
            if(isset($request->vet))
            {
                for ($i=0; $i <count($request->p_id) ; $i++) {
                    DCdetails::create([
                        's_id' => $id,
                        'p_id' => $request->p_id[$i],
                        'quantity' => $request->quantity[$i],
                        'sub_total' => $request->sub_total[$i],
                        'price' => $request->price[$i],
                        'cost' => $request->cost[$i],
                        'discount_percent' => $request->discount_percent[$i] == 'NaN' ? '0' : $request->discount_percent[$i] ,
                        'discounted_amount' => $request->discounted_amount[$i],
                        'type' => $request->type[$i],
                        'vet' => $request->vet[$i],
                        'taxA' => $request->taxA[$i],
                        'afterDiscount' => $request->ad[$i],
                    ]);
                }
            }
            else {
                for ($i=0; $i <count($request->p_id) ; $i++) {
                    DCdetails::create([
                        's_id' => $id,
                        'p_id' => $request->p_id[$i],
                        'quantity' => $request->quantity[$i],
                        'sub_total' => $request->sub_total[$i],
                        'price' => $request->price[$i],
                        'cost' => $request->cost[$i],
                        'discount_percent' => $request->discount_percent[$i] == 'NaN' ? '0' : $request->discount_percent[$i] ,
                        'discounted_amount' => $request->discounted_amount[$i],
                        'type' => $request->type[$i],
                    ]);
                }
            }

            toastr()->success('DC Updated successfully!');
            return redirect(url('').'/dc');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
