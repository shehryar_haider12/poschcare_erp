<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeadCategory;
use App\HeadofAccounts;
use Auth;

class HeadofAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $hoa=HeadofAccounts::all();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('HOA.index',compact('hoa','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isEdit = false;
        return view('HOA.create',compact('isEdit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      =>  'required|unique:head_of_accounts'
        ]);
        $u_id = Auth::user()->id;
        $data = [
            'name' => $request->name,
            'created_by' => $u_id
        ];
        HeadofAccounts::create($data);
        toastr()->success('Head of Accounts added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $a = HeadofAccounts::find($id);
        $hcat = HeadCategory::where('a_id',$id)
        ->latest('created_at')->first();
        if($hcat == null)
        {
            $id = 01;
        }
        else
        {
            $id = substr($hcat->code, strpos($hcat->code, '-')+1)+1;
        }

        if($a->name == 'Non-current Asset')
        {
            $name = 'NCA';
        }

        if($a->name == 'Expense')
        {
            $name = 'EXP';
        }

        if($a->name == 'Current Liability')
        {
            $name = 'CL';
        }

        if($a->name == 'Equity')
        {
            $name = 'EQ';
        }

        if($a->name == 'Revenue')
        {
            $name = 'RV';
        }

        if($a->name == 'Purchases')
        {
            $name = 'PUR';
        }

        if($a->name == 'Current Asset')
        {
            $name = 'CA';
        }

        if($a->name == 'Non-current Liability')
        {
            $name = 'NCL';
        }
        $str_length = strlen((string)$id)+1;
        $id = substr("0000{$id}", -$str_length);
        return $code = $name.'-'.$id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hoa = HeadofAccounts::find($id);
        $data=[
            'isEdit' => true,
            'hoa' => $hoa
        ];
        return view('HOA.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      =>  'required|string|max:50'
        ]);
        $u_id = Auth::user()->id;
        HeadofAccounts::where('id',$id)
        ->update([
            'name' => $request->name,
            'updated_by' => $u_id
        ]);
        toastr()->success('Head of Accounts updated successfully!');
        return redirect(url('').'/headofaccounts');
    }

    public function find($id)
    {
        $hcat = HeadCategory::with('accountDetails.children')->whereNotIn('name',['Receivables','Payables','Inventory'])
        ->where('a_id',$id)->get();
        return $hcat;
    }


}
