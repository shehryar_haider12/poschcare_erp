<?php

namespace App\Http\Controllers;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use App\Exports\CategoryExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('category.index',compact('permissions'));
    }

    public function datatable()
    {
        $cat=Category::all();
        return DataTables::of($cat)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';
        $u_id = Auth::user()->id;
        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Category::find($id);
        if ($item->update(['status' => $status])) {
            Category::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('category.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cat_name'      =>  'required|string|max:255|unique:category'
        ]);
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        $data = [
            'cat_name' => $request->cat_name,
            'created_by' => $u_id,
            'status' => $status
        ];
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New category has been added by '.$u_name,
            'link' => url('').'/category',
            'name' => 'View Categories',
        ];
        Notification::send($user, new AddNotification($data1));
        $cat = Category::create($data);
        if(isset($request->request_type) && $request->request_type == "ajax")
        {
            $response['message'] = 'Category added successfully!';
            $response['cat'] = $cat;
            return response()->json($response, 200);
        }
        toastr()->success('Category added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        if(request()->ajax())
        {
            return $category;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data=[
            'isEdit' => true,
            'cat' =>$category
        ];
        return view('category.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Category $category)
    {
        $data = $request->validate([
            'cat_name'      =>  'required|string|max:255|unique:category,cat_name,'.$category->id
        ]);
        $data['updated_by'] = Auth::user()->id;
        $category ->update($data);
        toastr()->success('Category Updated successfully!');
        return redirect(url('').'/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new CategoryExport, 'Category.xlsx');
    }
}
