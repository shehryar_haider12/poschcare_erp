<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\VendorAttachment;
use App\VendorsUpdateRequest;
use App\City;
use App\Products;
use App\User;
use DataTables;
use App\Exports\BillerExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Auth;
use Storage;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class BillerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('biller.index',compact('permissions'));
    }

    public function datatable()
    {
        $vendor=Vendors::where('v_type','Biller')->get();
        return DataTables::of($vendor)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $city=City::where('status',1)->get();
        $data= [
            'isEdit' => false,
            'city' => $city,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('biller.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }

        $u_id = Auth::user()->id;
        $biller = Vendors::create([
            'company' => $request->company,
            'name' => $request->name,
            'address' => $request->address,
            'c_no' => $request->c_no,
            'country' => $request->country,
            'VAT' => $request->VAT,
            'GST' => $request->GST,
            'state' => $request->state,
            'email' => $request->email,
            'postalCode' => $request->postalCode,
            'c_id' => $request->c_id,
            'v_type' => $request->v_type,
            'website' => $request->website,
            'abr' => $request->abr,
            'created_by' => $u_id,
            'status' => $status
        ]);

        if(isset($request->document))
        {
            $document= Storage::disk('documents')->putFile('',$request->document);
            VendorAttachment::create([
                'v_id' => $biller->id,
                'document' => $document
            ]);
        }

        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data = [
            'notification' => 'New Biller has been added by '.$u_name,
            'link' => url('').'/biller',
            'name' => 'View Billers',
        ];

        if(isset($request->request_type) && $request->request_type == "ajax")
        {
            $response['message'] = 'Biller added successfully!';
            $response['biller'] = $biller;
            return response()->json($response, 200);
        }


        Notification::send($user, new AddNotification($data));
        toastr()->success('Biller added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=Vendors::with(['city','attachment'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor=Vendors::with(['city','attachment'])
        ->where('id',$id)
        ->first();
        // return $vendor;
        $city=City::where('status',1)->get();
        $data= [
            'isEdit' => true,
            'city' => $city,
            'vendor' => $vendor
        ];
        return view('biller.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {

            $u_id = Auth::user()->id;
            $vendor=Vendors::where('id',$id)
            ->update([
                'company' => $request->company,
                'name' => $request->name,
                'address' => $request->address,
                'c_no' => $request->c_no,
                'country' => $request->country,
                'VAT' => $request->VAT,
                'GST' => $request->GST,
                'state' => $request->state,
                'email' => $request->email,
                'postalCode' => $request->postalCode,
                'c_id' => $request->c_id,
                'website' => $request->website,
                'abr' => $request->abr,
                'updated_by' => $u_id
            ]);
            if(isset($request->document))
            {
                $va = VendorAttachment::where('v_id',$id)->first();
                $document= Storage::disk('documents')->putFile('',$request->document);
                if($va != null)
                {
                    Storage::disk('documents')->delete($va->document);
                    VendorAttachment::where('v_id',$id)->update([
                        'document' => $document
                    ]);
                }
                else
                {
                    VendorAttachment::create([
                        'v_id' => $id,
                        'document' => $document
                    ]);
                }
            }
            toastr()->success('Biller updated successfully!');
            return redirect(url('')."/biller");
        }
        else
        {
            VendorsUpdateRequest::create([
                'company' => $request->company,
                'name' => $request->name,
                'address' => $request->address,
                'c_no' => $request->c_no,
                'country' => $request->country,
                'VAT' => $request->VAT,
                'GST' => $request->GST,
                'state' => $request->state,
                'email' => $request->email,
                'postalCode' => $request->postalCode,
                'c_id' => $request->c_id,
                'v_type' => $request->v_type
            ]);
            toastr()->success('Biller update request sent successfully!');
            return redirect(url('')."/biller");
        }
    }

    public function excel()
    {
        return Excel::download(new BillerExport, 'billers.xlsx');
    }

    public function pdf()
    {
        $vendor =  Vendors::where('v_type','Biller')
        ->select('name','company','address','c_no')
        ->get();
        // dd($vendor);
        $pdf = PDF::loadView('biller.pdf', compact('vendor'));

        return $pdf->download('billers.pdf');
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Vendors::find($id);
        if ($item->update(['status' => $status])) {
            Vendors::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
