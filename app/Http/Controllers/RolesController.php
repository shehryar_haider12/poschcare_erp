<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use App\RoleMenu;
use App\RolePermission;
use App\Permissions;
use App\UserMenu;
use Auth;
use DataTables;
use DB;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        return  view('roles.index',compact('permissions'));
    }

    public function datatable()
    {
        $roles=Roles::all();
        return DataTables::of($roles)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';
        $u_id = Auth::user()->id;
        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Roles::find($id);
        if ($item->update(['status' => $status])) {
            Roles::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false,
            'menus' =>  UserMenu::with(['children','permission'])->get(),
        ];
        return view('roles.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      =>  'required|string|max:255|unique:roles',
            'description' =>  'required',
            'm_id' => "required|array",
        ]);
        $u_id = Auth::user()->id;
        $r= Roles::create([
            'name' => $request->name,
            'description' => $request->description,
            'created_by' => $u_id
        ]);

        if($request->m_id[0] != null)
        {
            for ($i=0; $i <count($request->m_id) ; $i++)
            {
                RoleMenu::create([
                    'r_id' => $r->id,
                    'm_id' => $request->m_id[$i],
                    'created_by' => $u_id
                ]);
            }
        }

        if($request->p_id[0] != null)
        {
            for ($i=0; $i <count($request->p_id) ; $i++)
            {
                RolePermission::create([
                    'r_id' => $r->id,
                    'p_id' => $request->p_id[$i],
                    'created_by' => $u_id
                ]);
            }
        }


        toastr()->success('Role added successfully!');
        return redirect(url('').'/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Roles $role)
    {
        $roles = $role;
        $rm = RoleMenu::with('menu')->where('r_id',$roles->id)->get();
        $rp = RolePermission::with('permission')->where('r_id',$roles->id)->get();
        $menu = UserMenu::with(['children','permission'])->get();
        // return ($menu);
        $data=[
            'isEdit' => true,
            'roles' =>$roles,
            'rmenu' =>$rm,
            'rper' =>$rp,
            'menus' => $menu
        ];
        return view('roles.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roles $role)
    {
        $request->validate([
            'name'      =>  'required|string|max:255|unique:roles,name,'.$role->id,
            'description' =>  'required',
            'm_id' => "required|array",
        ]);
        $u_id = Auth::user()->id;
        $role->update([
            'name' =>$request->name,
            'description' =>$request->description,
            'updated_by' => $u_id
        ]);

        DB::table('role_menu')->where('r_id', $role->id)->delete();
        DB::table('role_permission')->where('r_id', $role->id)->delete();

        if($request->m_id[0] != null)
        {
            for ($i=0; $i <count($request->m_id) ; $i++)
            {
                RoleMenu::create([
                    'r_id' => $role->id,
                    'm_id' => $request->m_id[$i],
                    'created_by' => $u_id
                ]);
            }
        }

        if($request->p_id[0] != null)
        {
            for ($i=0; $i <count($request->p_id) ; $i++)
            {
                RolePermission::create([
                    'r_id' => $role->id,
                    'p_id' => $request->p_id[$i],
                    'created_by' => $u_id
                ]);
            }
        }
        toastr()->success('Role updated successfully!');
        return redirect(url('').'/roles');
    }

    public function menu($id)
    {
        $menu = RoleMenu::with('menu')->where('r_id',$id)->get();
        return $menu;
        $rp = RolePermission::with('permission')->where('r_id',$id)->get();
    }

    public function permission($id)
    {
        $permission = RolePermission::with('permission')->where('r_id',$id)->get();
        return $permission;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
