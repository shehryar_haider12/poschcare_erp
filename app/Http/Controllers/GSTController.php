<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\GST;
use App\User;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class GSTController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return  view('gst.index');
    }

    public function datatable()
    {
        $gst=GST::all();
        return DataTables::of($gst)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gst.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'tax' =>  'required'
        ]);
        $gst = GST::create($data);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Tax has been added by '.$u_name,
            'link' => url('').'/gst',
            'name' => 'View GST List',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('GST added successfully!');
        return redirect()->back();
    }
}
