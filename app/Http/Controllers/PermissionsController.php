<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserMenu;
use App\Permissions;
use App\User;
use DataTables;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('permission.index',compact('permissions'));
    }

    public function datatable()
    {
        $permission=Permissions::with('menu')->get();
        return DataTables::of($permission)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false,
            'menus' => UserMenu::all(),
        ];
        return view('permission.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Permissions::create([
            'm_id' => $request->m_id,
            'name' => $request->name,
            'route' => $request->route
        ]);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data = [
            'notification' => 'New permission has been added by '.$u_name,
            'link' => url('').'/permission',
            'name' => 'View Permissions',
        ];
        Notification::send($user, new AddNotification($data));
        toastr()->success('Permission added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission=Permissions::find($id);
        $data=[
            'isEdit' => true,
            'permission' =>$permission,
            'menus' => UserMenu::all()
        ];
        return view('permission.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Permissions=Permissions::where('id',$id)
        ->update([
            'm_id' => $request->m_id,
            'name' => $request->name,
            'route' => $request->route
        ]);
        toastr()->success('Permission updated successfully!');
        return redirect(url('').'/permission');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
