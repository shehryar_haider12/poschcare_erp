<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserMenu;
use Auth;
use DataTables;

class UserMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('menu.index',compact('permissions'));
    }

    public function datatable()
    {
        $menu=UserMenu::with('parent')->get();
        return DataTables::of($menu)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false,
            'menus' => UserMenu::all(),
        ];
        return view('menu.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'p_id'      =>  'required',
            'name'      =>  'required|string|max:255|unique:user_menu',
            'sort'      =>  'required',
            'icon'      => 'nullable',
            'route'     => 'nullable',
        ]);
        $data['created_by'] = Auth::user()->id;
        UserMenu::create($data);
        toastr()->success('Menu added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserMenu $menu)
    {
        $data=[
            'isEdit' => true,
            'menus' => UserMenu::all(),
            'menu' =>$menu
        ];
        return view('menu.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserMenu $menu)
    {
        $data = $request->validate([
            'name'      =>  'required|string|max:255|unique:user_menu,name,'.$menu->id,
            'p_id'      =>  'required',
            'sort'      =>  'required',
            'icon'      => 'nullable',
            'route'     => 'nullable',
        ]);
        $menu->update($data);
        toastr()->success('Menu updated successfully!');
        return redirect(url('').'/menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
