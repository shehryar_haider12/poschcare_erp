<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\SaleDocs;

class SaleDocumentController extends Controller
{
    public function addDoc(Request $request)
    {
        if($request->document == null)
        {
            toastr()->error('Attach Document!');
            return redirect()->back();
        }
        else
        {
            $document = '';
            $document= Storage::disk('documents')->putFile('',$request->document);
            SaleDocs::create([
                's_id' => $request->s_id,
                'document' => $document
            ]);
            toastr()->success('Document Added Successfully!');
            return redirect()->back();
        }
    }

    public function viewDoc($id)
    {
        $doc = SaleDocs::where('s_id',$id)->get();
        return $doc;
    }
}
