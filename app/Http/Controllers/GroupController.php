<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Groups;
use DataTables;
use Auth;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('group.index',compact('permissions'));
    }

    public function datatable()
    {
        $group=Groups::all();
        return DataTables::of($group)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('group.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      =>  'required|string|max:50'
        ]);
        $u_id = Auth::user()->id;
        $data = [
            'name' => $request->name,
            'g_type' => $request->g_type,
            'created_by' => $u_id
        ];

        Groups::create($request->all());
        toastr()->success('Group added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $group=Groups::find($id);
            return $group;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Groups::find($id);
        $data=[
            'isEdit' => true,
            'group' => $group
        ];
        return view('group.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      =>  'required|string|max:50'
        ]);
        $u_id = Auth::user()->id;
        $group=Groups::where('id',$id)
        ->update([
            'name' => $request->name,
            'g_type' => $request->g_type,
            'updated_by' => $u_id
        ]);
        toastr()->success('Group updated successfully!');
        return redirect(url('').'/group');
    }


}
