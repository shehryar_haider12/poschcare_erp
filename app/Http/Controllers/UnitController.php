<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use DataTables;
use Auth;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('unit.index',compact('permissions'));
    }

    public function unitDatatable()
    {
        $unit=Unit::all();
        return DataTables::of($unit)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';
        $u_id = Auth::user()->id;
        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Unit::find($id);
        if ($item->update(['status' => $status])) {
            Unit::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' =>false
        ];
        return view('unit.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'u_name'      =>  'required|string|max:255|unique:units'
        ]);
        $data['created_by'] = Auth::user()->id;
        $unit = Unit::create($data);
        if(isset($request->request_type) && $request->request_type == "ajax")
        {
            $response['message'] = 'Unit added successfully!';
            $response['unit'] = $unit;
            return response()->json($response, 200);
        }
        toastr()->success('Unit added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        // dd($id)
        if(request()->ajax())
        {
            return $unit;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        $data=[
            'isEdit' => true,
            'unit' =>$unit
        ];
        return view('unit.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        $data = $request->validate([
            'u_name'      =>  'required|string|max:255|unique:units,u_name,'.$unit->id
        ]);
        $data['updated_by'] = Auth::user()->id;
        $unit->update($data);
        toastr()->success('Unit updated successfully!');
        return redirect(url('').'/unit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
