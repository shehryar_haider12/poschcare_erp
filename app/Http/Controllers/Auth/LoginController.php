<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function loginUser(Request $request)
    {
        $user = User::with('role')->where('email',$request->email)->first();
        if($user->role->status == 1)
        {
            $userdata = array
            (
                'email' => $request->email ,
                'password' => $request->password
            );
            if (Auth::attempt($userdata))
            {

                return redirect('/dashboard');
            }
            else
            {
                toastr()->error('Incorrect username or password!');
                return redirect()->back();
            }
        }
        else
        {
            toastr()->error('Your Role is blocked!');
            return redirect()->back();
        }
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
