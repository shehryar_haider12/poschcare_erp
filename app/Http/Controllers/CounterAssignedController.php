<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Counter;
use App\CounterAssigned;
use App\User;
use DataTables;
use Auth;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CounterAssignedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('assign.index',compact('permissions'));
    }

    public function datatable()
    {
        $counter = CounterAssigned::with(['user','counter'])->get();
        return DataTables::of($counter)->make();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isEdit = false;
        $user = User::with('role')->get();
        $counter = Counter::all();
        return view('assign.create',compact('isEdit','user','counter'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->u_id as $key => $u) {
            CounterAssigned::create([
                'u_id' => $u,
                'c_id' => $request->c_id,
            ]);
        }
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Counter Assigned by '.$u_name,
            'link' => url('').'/assigned',
            'name' => 'View Assigned Counter Users',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Counter Assigned successfully!');
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $isEdit = true;
        $user = User::with('role')->get();
        $counter = Counter::all();
        $assign = CounterAssigned::find($id);
        return view('assign.create',compact('isEdit','user','counter','assign'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        DB::table('counter_assigned')->where('c_id', $request->c_id)->delete();
        foreach ($request->u_id as $key => $u) {
            CounterAssigned::create([
                'u_id' => $u,
                'c_id' => $request->c_id,
            ]);
        }
        toastr()->success('Assigned Counter updated successfully!');
        return redirect(url('').'/assigned');
    }


}
