<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\User;
use DataTables;
use App\Exports\CompanyExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('company.index',compact('permissions'));
    }

    public function datatable()
    {
        $company=Company::all();
        return DataTables::of($company)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('company.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      =>  'required|unique:company',
            'address'      =>  'required|unique:company'
        ]);
        $u_id = Auth::user()->id;
        $data = [
            'name' => $request->name,
            'address' => $request->address,
            'c_p_name' => $request->c_p_name,
            'c_p_contactNo' => $request->c_p_contactNo,
            'created_by' => $u_id
        ];
        Company::create($data);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New company has been added by '.$u_name,
            'link' => url('').'/company',
            'name' => 'View Companies',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Company added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $company=Company::find($id);
            return $company;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $data=[
            'isEdit' => true,
            'company' => $company
        ];
        return view('company.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      =>  'required|unique:company,name,'.$id
        ]);
        $u_id = Auth::user()->id;
        $company=Company::where('id',$id)
        ->update([
            'name' => $request->name,
            'address' => $request->address,
            'c_p_name' => $request->c_p_name,
            'c_p_contactNo' => $request->c_p_contactNo,
            'updated_by' => $u_id
        ]);
        toastr()->success('Company updated successfully!');
        return redirect(url('').'/company');
    }


    public function excel()
    {
        return Excel::download(new CompanyExport, 'Company.xlsx');
    }

    public function pdf()
    {
        $company = Company::select('id','name','address','c_p_name','c_p_contactNo')
        ->get();
        $pdf = PDF::loadView('company.pdf', compact('company'));

        return $pdf->download('companies.pdf');
    }
}
