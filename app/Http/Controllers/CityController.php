<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\City;
use App\User;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('city.index',compact('permissions'));
    }

    public function datatable()
    {
        $city=City::all();
        return DataTables::of($city)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('city.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        $data = $request->validate([
            'c_name' =>  'required|string|max:255|unique:city'
        ]);
        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $data['status'] = 1;
        }
        else
        {
            $data['status'] = 0;
        }
        $data['created_by'] = Auth::user()->id;
        $city = City::create($data);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New city has been added by '.$u_name,
            'link' => url('').'/city',
            'name' => 'View Cities',
        ];
        Notification::send($user, new AddNotification($data1));
        if(isset($request->request_type) && $request->request_type == "ajax")
        {
            $response['message'] = 'City added successfully!';
            $response['city'] = $city;
            return response()->json($response, 200);
        }
        toastr()->success('City added successfully!');
        return redirect()->back();
    }



    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = City::find($id);
        if ($item->update(['status' => $status])) {
            City::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        if(request()->ajax())
        {
            return $city;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $data=[
            'isEdit' => true,
            'city' => $city
        ];
        return view('city.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,City $city)
    {
        $data = $request->validate([
            'c_name' =>  'required|string|max:255|unique:city,c_name,'.$city->id
        ]);
        $data['updated_by'] = Auth::user()->id;
        $city->update($data);
        toastr()->success('City updated successfully!');
        return redirect(url('').'/city');
    }
}
