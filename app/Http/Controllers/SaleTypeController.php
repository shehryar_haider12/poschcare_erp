<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\SaleType;
use App\User;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class SaleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return  view('saletype.index');
    }

    public function datatable()
    {
        $ct=SaleType::all();
        return DataTables::of($ct)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('saletype.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' =>  'required'
        ]);
        $gst = SaleType::create($data);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Sale type has been added by '.$u_name,
            'link' => url('').'/saletype',
            'name' => 'View Sale Types List',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Sale type added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SaleType $saletype)
    {
        $data=[
            'isEdit' => true,
            'ct' => $saletype
        ];
        return view('saletype.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SaleType $saletype)
    {
        $data = $request->validate([
            'name' =>  'required|unique:name,sale_type,'.$saletype->id
        ]);
        $data['updated_by'] = Auth::user()->id;
        $costtype->update($data);
        toastr()->success('Sale type updated successfully!');
        return redirect(url('').'/saletype');
    }

}
