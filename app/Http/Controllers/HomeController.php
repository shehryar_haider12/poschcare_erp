<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\Bank;
use App\Brands;
use App\CounterAssigned;
use App\Category;
use App\Products;
use App\PurchaseOrder;
use App\Sales;
use App\SaleCounter;
use App\PurchaseOrderDetails;
use App\SaleDetails;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use DB;
use Session;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\incomeStatementExport;
use App\Exports\incomeStatementYearExport;
use App\Exports\incomeStatementMonthExport;
use App\Exports\incomeStatementDateExport;
use App\Exports\SaleCostingExport;
use App\Exports\SaleCostingYearExport;
use App\Exports\SaleCostingMonthExport;
use App\Exports\SaleCostingDateExport;
use App\Exports\SaleCostingDatesExport;
use App\Exports\TrialBalanceExport;
use App\Exports\TrialBalanceSearchExport;

use App\Exports\CustomerDetailReportExport;
use App\Exports\CustomerDetailReportExportYearly;
use App\Exports\CustomerDetailReportExportMonthly;
use App\Exports\CustomerDetailReportExportDate;
use App\Exports\CustomerDetailReportExportDateDifference;
use App\Exports\CustomerDetailReportExportDateConditions;

use App\Exports\SupplierDetailReportExport;
use App\Exports\SupplierDetailReportExportYearly;
use App\Exports\SupplierDetailReportExportMonthly;
use App\Exports\SupplierDetailReportExportDate;
use App\Exports\SupplierDetailReportExportDateDifference;
use App\Exports\SupplierDetailReportExportDateConditions;

use PDF;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //pos opening and closing
    public function POS_OC()
    {
        $today = Carbon::now()->format('Y-m-d');
        $counter = Session::get('assign');
        if ($counter == null)
        {
            $sales = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sales s INNER JOIN sale_counter c ON s.id=c.s_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' and c.ret_id is null GROUP BY c.date,c.co_id,c.ret_id");
            $return = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sale_return s INNER JOIN sale_counter c ON s.id=c.ret_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' GROUP BY c.date,c.co_id,c.ret_id");
        }
        else
        {
            $sales = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sales s INNER JOIN sale_counter c ON s.id=c.s_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' and c.co_id = '".$counter."' and c.ret_id is null GROUP BY c.date,c.co_id,c.ret_id");
            $return = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sale_return s INNER JOIN sale_counter c ON s.id=c.ret_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' and c.co_id = '".$counter."' GROUP BY c.date,c.co_id,c.ret_id");
        }



        return view('pos_oc',compact('sales','return'));
    }

    //pos view
    public function pos()
    {
        $counter = Session::get('assign');

        // $curl_handle_cat = curl_init();

        // $caturl = "http://165.227.69.207/poschcare_erp/public/api/category";

        // // // Set the curl URL option
        // curl_setopt($curl_handle_cat, CURLOPT_URL, $caturl);

        // // // This option will return data as a string instead of direct output
        // curl_setopt($curl_handle_cat, CURLOPT_RETURNTRANSFER, true);

        // // // Execute curl & store data in a variable
        // $cat_data = curl_exec($curl_handle_cat);

        // curl_close($curl_handle_cat);

        // // // Decode JSON into PHP array
        // $cat = json_decode($cat_data);

        // $curl_handle_brand = curl_init();

        // $brandurl = "http://165.227.69.207/poschcare_erp/public/api/brands";

        // // // Set the curl URL option
        // curl_setopt($curl_handle_brand, CURLOPT_URL, $brandurl);

        // // // This option will return data as a string instead of direct output
        // curl_setopt($curl_handle_brand, CURLOPT_RETURNTRANSFER, true);

        // // // Execute curl & store data in a variable
        // $brand_data = curl_exec($curl_handle_brand);

        // curl_close($curl_handle_brand);

        // // // Decode JSON into PHP array
        // $brand = json_decode($brand_data);
        // dd($brand);
        $cat = Category::where('status',1)->get();
        $bank = Bank::where('status',1)->get();
        $brand = Brands::where('status',1)->get();
        $biller = Vendors::where('v_type','Biller')->where('status',1)->get();
        $customer = Vendors::where('v_type','Customer')->where('status',1)->get();
        $product = Products::where('vstatus',0)->where('status',1)->where('mstatus','Yes')->get();
        return view('pos',compact('cat','brand','biller','customer','product','bank'));
    }

    //pos find product using id
    public function posProduct($id)
    {
        $prod = Products::with(['unit'])->where('id',$id)->first();
        $w_id = Auth::user()->w_id;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {
            // dd('y');
            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->where('w_id',$w_id)->where('type',$prod->vstatus)->select(DB::raw('sum(unit_quantity)'));
            }])
            ->where('id',$id)
            ->first();
        }
        else
        {
            // dd('n');

            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$prod->vstatus);
            }])
            ->where('id',$id)
            ->first();
            // dd($product);

        }
        return $product;
    }

    //pos find product using code
    public function posProductCode($code)
    {
        $prod = Products::with(['unit'])->where('pro_code',$code)->first();
        $w_id = Auth::user()->w_id;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {
            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->where('w_id',$w_id)->where('type',$prod->vstatus)->select(DB::raw('sum(unit_quantity)'));
            }])
            ->where('id',$prod->id)
            ->first();
        }
        else
        {

            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$prod->vstatus);
            }])
            ->where('id',$prod->id)
            ->first();
        }
        return $product;
    }

    //price checker
    public function priceChecker()
    {
        $product = Products::where('vstatus',0)->where('status',1)->get();
        return view('priceChecker',compact('product'));
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permission         =   getRolePermission($menu_id);



        $assign = CounterAssigned::where('u_id',Auth::user()->id)->first();
        if($assign != null)
        {
            Session::put('assign',$assign->c_id);
            $counter = Session::get('assign');
        }
        $biller=Vendors::where('v_type','Biller')->count('id');
        $customer=Vendors::where('v_type','Customer')->count('id');
        $supplier=Vendors::where('v_type','Supplier')->count('id');
        $sp=Vendors::where('v_type','Saleperson')->count('id');
        $bank=Bank::count('id');
        $brand=Brands::count('id');
        $cat=Category::count('id');
        $po=PurchaseOrder::count('id');
        $sale=Sales::count('id');
        $raw=DB::select("SELECT sum(current_stock.quantity) as quantity from current_stock , products
        where products.p_type = 'Raw'
        and products.id = current_stock.p_id
        and current_stock.type=0
        GROUP BY (products.p_type)");
        $pakg=DB::select("SELECT sum(current_stock.quantity) as quantity from current_stock , products
        where products.p_type = 'Packaging'
        and products.id = current_stock.p_id
        and current_stock.type=0
        GROUP BY (products.p_type)");
        $finish=DB::select("SELECT sum(current_stock.quantity) as quantity from current_stock , products
        where products.p_type = 'Finished'
        and products.id = current_stock.p_id
        and current_stock.type=0
        GROUP BY (products.p_type)");
        return view('Dashboard',compact('biller','customer','supplier','sp','bank','brand','cat'
        ,'raw','pakg','finish','po','sale','permission'));
    }

    public function salesCosting(Request $request)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $index=0;
        // return $stock;
        return view('reports.saleCosting',compact('stock','permissions','menu_id','index'));
    }

    public function saleCostingPDF()
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $pdf = PDF::loadView('reports.saleCostingPDF', compact('stock'));

        return $pdf->download('saleCosting.pdf');
    }

    public function saleCostingExcel()
    {
        return Excel::download(new SaleCostingExport, 'saleCostingExport.xlsx');
    }

    public function saleCostingSearch(Request $request)
    {
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        if($request->optradio == 'Year')
        {
            $stock = SaleDetails::with('purchase','products','sale.customer','variant')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
            ->get()->groupBy(function($item){
                return $item->created_at->format('Y-m-d');
            });
            $index = 1;
            $year = $request->year;
            return view('reports.saleCosting',compact('stock','index','year','permissions','menu_id'));
        }
        else if($request->optradio == 'Month')
        {
            $stock = SaleDetails::with('purchase','products','sale.customer','variant')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
            ->get()->groupBy(function($item){
                return $item->created_at->format('Y-m-d');
            });
            $index = 2;
            // $monthName = Carbon::parse($request->month)->format('M-Y');
            $month = $request->month;
            // dd($stock);
            return view('reports.saleCosting',compact('stock','index','month','permissions','menu_id'));
        }

        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                $index = 3;
                $stock = SaleDetails::with('purchase','products','sale.customer','variant')
                ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                ->get()->groupBy(function($item){
                    return $item->created_at->format('Y-m-d');
                });
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                $stock = SaleDetails::with('purchase','products','sale.customer','variant')
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                ->get()->groupBy(function($item){
                    return $item->created_at->format('Y-m-d');
                });
            }
            if($index == 3)
            {
                return view('reports.saleCosting',compact('stock','index','from','to','permissions','menu_id'));
            }
            if($index == 4)
            {
                return view('reports.saleCosting',compact('stock','index','date','permissions','menu_id'));
            }
        }
    }

    public function saleCostingSearchYExcel($year)
    {
        return Excel::download(new SaleCostingYearExport($year), 'SaleCostingExportYearly.xlsx');
    }

    public function saleCostingSearchYPDF($year)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$year)
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $pdf = PDF::loadView('reports.saleCostingYearlyPDF', compact('stock','year'));

        return $pdf->download('saleCostingYearly.pdf');
    }

    public function saleCostingSearchMExcel($month)
    {
        return Excel::download(new SaleCostingMonthExport($month), 'SaleCostingExportMonthly.xlsx');
    }

    public function saleCostingSearchMPDF($month)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $monthName = Carbon::parse($month)->format('M Y');
        $pdf = PDF::loadView('reports.saleCostingMonthlyPDF', compact('stock','monthName'));

        return $pdf->download('saleCostingMonthly.pdf');
    }

    public function saleCostingSearchDExcel($date)
    {
        return Excel::download(new SaleCostingDateExport($date), 'SaleCostingExportDate.xlsx');
    }

    public function saleCostingSearchDPDF($date)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $pdf = PDF::loadView('reports.saleCostingDatePDF', compact('stock','date'));

        return $pdf->download('saleCostingDate.pdf');
    }

    public function saleCostingSearchDatesExcel($from,$to)
    {
        return Excel::download(new SaleCostingDatesExport($from,$to), 'SaleCostingExportDateDifference.xlsx');
    }


    public function incomeStatement(Request $request)
    {
        $index=0;
        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales'");

        $opening_stock = DB::select("SELECT sum(debit) as opening
        FROM `general_ledger` where
        account_code like '%CA-02%'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase'");
        // dd($purchase);

        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%'");

        $cost_of_goods = ($opening_stock[0]->opening + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;

        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $year = '';
        $month = '';

        return view('reports.incomeStatement',compact('sales','opening_stock','purchase'
        ,'closing_stock','cost_of_goods','index','pnl','permissions','menu_id','year','month'));

    }

    public function incomeStatementExcel()
    {
        return Excel::download(new incomeStatementExport, 'incomeStatementExport.xlsx');
    }

    public function incomeStatementPDF()
    {
        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales'");

        $opening_stock = DB::select("SELECT sum(debit) as opening
        FROM `general_ledger` where
        account_code like '%CA-02%'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase'");

        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%'");

        $cost_of_goods = ($opening_stock[0]->opening+ $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;

        $pdf = PDF::loadView('reports.incomeStatementPDF', compact('sales','opening_stock','purchase'
        ,'closing_stock','cost_of_goods','pnl'));

        return $pdf->download('incomeStatement.pdf');
    }

    public function incomeStatementSearch(Request $request)
    {
        // dd($request->all());
        if($request->optradio == 'Year')
        {
            // $stock = SaleDetails::with('purchase','products','sale.customer')
            // ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
            // ->get()->groupBy(function($item){
            //     return $item->created_at->format('Y-m-d');
            // });
            $index = 1;
            $month = '';
            $from = '';
            $to = '';
            $year = $request->year;
            $a = Carbon::parse('Mar-'.$year);
            $year_p = $a->copy()->subYears(1)->format('Y');

            $sales = DB::select("SELECT sum(credit) as credit
            FROM `general_ledger` where account_name = 'Sales' and
            DATE_FORMAT(created_at,'%Y') = '".$request->year."'");

            $purchase = DB::select("SELECT sum(debit) as debit
            FROM `general_ledger` where account_name = 'Purchase' and
            DATE_FORMAT(created_at,'%Y') = '".$request->year."'");

            $opening_stock = DB::select("SELECT (sum(debit) - sum(credit))
            as opening
            FROM `general_ledger` where
            account_code like '%CA-02%' and account_code not like '%NCA-02%' and
            DATE_FORMAT(created_at,'%Y') = '".$year_p."'");



            $exp = DB::select("select sum(debit) as expense from general_ledger where account_code like 'EXP%'
            and  DATE_FORMAT(created_at,'%Y') = '".$year."'");


            $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
            as closing
            FROM `general_ledger` where
            account_code like '%CA-02%' and account_code not like '%NCA-02%' and
            DATE_FORMAT(created_at,'%Y') = '".$request->year."'");

            $cost_of_goods = ($opening_stock[0]->opening + $purchase[0]->debit) - $closing_stock[0]->closing;
            $pnl = $sales[0]->credit - $cost_of_goods ;

            $menu_id            =   $request->menuid;
            $permissions        =   getRolePermission($menu_id);

            return view('reports.incomeStatement',compact('sales','opening_stock','purchase','exp'
            ,'closing_stock','cost_of_goods','index','pnl','permissions','year','menu_id','month','from','to'));

        }
        else if($request->optradio == 'Month')
        {
            $year = '';
            $from = '';
            $to = '';
            $index = 2;
            $month = Carbon::parse($request->month)->format('M-y');
            $monthNumber =  Carbon::parse($request->month)->firstOfMonth();
            $lastMonth =  $monthNumber->subMonth()->format('M-y');

            $sales = DB::select("SELECT sum(credit) as credit
            FROM `general_ledger` where account_name = 'Sales' and
            period = '".$month."'");

            $purchase = DB::select("SELECT sum(debit) as debit
            FROM `general_ledger` where account_name = 'Purchase' and
            period = '".$month."'");
            $opening_stock = DB::select("SELECT (sum(debit) - sum(credit))
            as opening
            FROM `general_ledger` where
            account_code like '%CA-02%' and account_code not like '%NCA-02%' and
            period = '".$lastMonth."'");


            $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
            as closing
            FROM `general_ledger` where
            account_code like '%CA-02%' and account_code not like '%NCA-02%' and
            period = '".$month."'");

            $exp = DB::select("select sum(debit) as expense from general_ledger where account_code like 'EXP%'
            and  period = '".$month."'");

            $cost_of_goods = ($opening_stock[0]->opening + $purchase[0]->debit) - $closing_stock[0]->closing;
            $pnl = $sales[0]->credit - $cost_of_goods ;


            $menu_id            =   $request->menuid;
            $permissions        =   getRolePermission($menu_id);

            return view('reports.incomeStatement',compact('sales','opening_stock','purchase','exp'
            ,'closing_stock','cost_of_goods','index','pnl','permissions','month','menu_id','year','from','to'));

        }
        else if($request->optradio == 'Date')
        {
            $year = '';
            $month = '';
            $index = 3;
            $from = $request->from;
            $to = $request->to;
            $Lastmonth = date("Y-m",strtotime($from));
            $monthNumber =  Carbon::parse($Lastmonth)->firstOfMonth();
            $SecondlastMonth =  $monthNumber->subMonth()->format('Y-m');
            $date1 = $SecondlastMonth.'-26';
            $date2 = $Lastmonth.'-25';
            // dd($date1,$date2);

            $sales = DB::select("SELECT sum(credit) as credit
            FROM `general_ledger` where account_name = 'Sales' and
            accounting_date between '".$from."' and '".$to."'");

            $purchase = DB::select("SELECT sum(debit) as debit
            FROM `general_ledger` where account_name = 'Purchase' and
            accounting_date between '".$from."' and '".$to."'");

            $opening_stock = DB::select("SELECT (sum(debit) - sum(credit))
            as opening
            FROM `general_ledger` where
            account_code like '%CA-02%' and
            accounting_date between '".$date1."' and '".$date2."'");

            $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
            as closing
            FROM `general_ledger` where
            account_code like '%CA-02%' and account_code not like '%NCA-02%' and
            accounting_date between '".$from."' and '".$to."'");

            $exp = DB::select("select sum(debit) as expense from general_ledger where account_code like 'EXP%'
            and  accounting_date between '".$from."' and '".$to."'");


            $cost_of_goods = ($opening_stock[0]->opening + $purchase[0]->debit) - $closing_stock[0]->closing;
            $pnl = $sales[0]->credit - ($cost_of_goods ) ;


            $menu_id            =   $request->menuid;
            $permissions        =   getRolePermission($menu_id);

            return view('reports.incomeStatement',compact('sales','opening_stock','purchase','exp'
            ,'closing_stock','cost_of_goods','index','pnl','permissions','month','menu_id','year','from','to'));
        }
    }

    public function incomeStatementSearchYExcel($year)
    {
        return Excel::download(new incomeStatementYearExport($year), 'incomeStatementExportYearly.xlsx');
    }

    public function incomeStatementSearchYPDF($year)
    {
        // $stock = SaleDetails::with('purchase','products','sale.customer')
        // ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$year)
        // ->get()->groupBy(function($item){
        //     return $item->created_at->format('Y-m-d');
        // });
        $a = Carbon::parse('Mar-'.$year);
        $year_p = $a->copy()->subYears(1)->format('Y');

        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales' and
        DATE_FORMAT(created_at,'%Y') = '".$year."'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase' and
        DATE_FORMAT(created_at,'%Y') = '".$year."'");

        $opening_s = DB::select("SELECT (sum(debit) - sum(credit))
        as opening
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        DATE_FORMAT(created_at,'%Y') = '".$year_p."'");

        if($opening_s[0]->opening == null)
        {
            $opening_stock = 0;
        }
        else
        {
            $opening_stock = $opening_s[0]->opening;
        }


        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        DATE_FORMAT(created_at,'%Y') = '".$year."'");

        $cost_of_goods = ($opening_stock + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;

        $pdf = PDF::loadView('reports.incomeStatementYearlyPDF', compact('sales','pnl','cost_of_goods','closing_stock','opening_stock','purchase','year'));

        return $pdf->download('incomeStatementYearly.pdf');
    }

    public function incomeStatementSearchMExcel($month)
    {
        return Excel::download(new incomeStatementMonthExport($month), 'incomeStatementExportMonthly.xlsx');
    }

    public function incomeStatementSearchMPDF($month)
    {
        // $stock = SaleDetails::with('purchase','products','sale.customer')
        // ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
        // ->get()->groupBy(function($item){
        //     return $item->created_at->format('Y-m-d');
        // });
        $monthName = Carbon::parse($month)->format('M Y');
        $monthNumber =  Carbon::parse($month)->firstOfMonth();
        $lastMonth =  $monthNumber->subMonth()->format('M-y');

        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales' and
        DATE_FORMAT(created_at,'%Y-%m') = '".$month."'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase' and
        DATE_FORMAT(created_at,'%Y-%m') = '".$month."'");

        $opening_s = DB::select("SELECT (sum(debit) - sum(credit))
        as opening
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        DATE_FORMAT(created_at,'%Y-%m') = '".$lastMonth."'");

        if($opening_s[0]->opening == null)
        {
            $opening_stock = 0;
        }
        else
        {
            $opening_stock = $opening_s[0]->opening;
        }


        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        DATE_FORMAT(created_at,'%Y-%m') = '".$month."'");

        $cost_of_goods = ($opening_stock + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;
        $pdf = PDF::loadView('reports.incomeStatementMonthlyPDF', compact('sales','pnl','cost_of_goods','closing_stock','opening_stock','purchase','monthName'));

        return $pdf->download('incomeStatementMonthly.pdf');
    }


    public function incomeStatementSearchDExcel($from,$to)
    {
        return Excel::download(new incomeStatementDateExport($from,$to), 'incomeStatementExportDateWise.xlsx');
    }

    public function incomeStatementSearchDPDF($from,$to)
    {
        $Lastmonth = date("Y-m",strtotime($from));
        $monthNumber =  Carbon::parse($Lastmonth)->firstOfMonth();
        $SecondlastMonth =  $monthNumber->subMonth()->format('Y-m');
        $date1 = $SecondlastMonth.'-26';
        $date2 = $Lastmonth.'-25';
        // dd($date1,$date2);

        $sales = DB::select("SELECT sum(credit) as credit
        FROM `general_ledger` where account_name = 'Sales' and
        accounting_date between '".$from."' and '".$to."'");

        $purchase = DB::select("SELECT sum(debit) as debit
        FROM `general_ledger` where account_name = 'Purchase' and
        accounting_date between '".$from."' and '".$to."'");

        $opening_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as opening
        FROM `general_ledger` where
        account_code like '%CA-02%' and
        accounting_date between '".$date1."' and '".$date2."'");

        $closing_stock = DB::select("SELECT (sum(debit) - sum(credit))
        as closing
        FROM `general_ledger` where
        account_code like '%CA-02%' and account_code not like '%NCA-02%' and
        accounting_date between '".$from."' and '".$to."'");

        $cost_of_goods = ($opening_stock[0]->opening + $purchase[0]->debit) - $closing_stock[0]->closing;
        $pnl = $sales[0]->credit - $cost_of_goods;
        $pdf = PDF::loadView('reports.incomeStatementDatePDF', compact('sales','pnl','cost_of_goods','closing_stock','opening_stock','purchase','from','to'));

        return $pdf->download('incomeStatementDate.pdf');
    }


    public function trialBalance(Request $request)
    {
        $hcat=HeadCategory::all();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $trial = 1;
        $index = 0;
        return view('trial.trialBalance',compact('hcat','permissions','trial','menu_id','index'));
    }

    public function trialBalanceExcel()
    {
        return Excel::download(new TrialBalanceExport, 'TrialBalanceExport.xlsx');
    }

    public function trialBalanceSearchExcel($lastMonth,$trial,$period)
    {
        return Excel::download(new TrialBalanceSearchExport($lastMonth,$trial,$period), 'TrialBalanceSearchExport.xlsx');
    }

    public function trialBalanceSearch(Request $request)
    {
        // $lastMonth =  Carbon::parse($request->period)->format('M-y');
        // dd($lastMonth);
        $request->validate([
            'p_type' => 'required',
            'b_type' => 'required',
            'period' => 'required',
        ]);
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        $p_type = $request->p_type;
        $b_type = $request->b_type;
        $index = 1;
        $hcat=HeadCategory::all();
        if($request->p_type == 'PTD' && $request->b_type == 'Opening')
        {
            $monthNumber =  Carbon::parse($request->period)->firstOfMonth();
            $lastMonth =  $monthNumber->subMonth()->format('M-y');
            $trial = 2;
            $period = '';
            return view('trial.trialBalance',compact('p_type','b_type','period','index','trial','hcat','lastMonth','menu_id','permissions'));
        }

        if($request->p_type == 'PTD' && $request->b_type == 'Closing')
        {
            $lastMonth =  Carbon::parse($request->period)->format('M-y');
            $trial = 2;
            $period = '';
            return view('trial.trialBalance',compact('p_type','b_type','period','index','trial','hcat','lastMonth','menu_id','permissions'));
        }

        if($request->p_type == 'YTD' && $request->b_type == 'Opening')
        {
            $period = GeneralLedger::first()->period;
            $monthNumber =  Carbon::parse($request->period)->firstOfMonth();
            $lastMonth =  $monthNumber->subMonth()->format('M-y');
            $trial = 3;
            return view('trial.trialBalance',compact('p_type','b_type','index','trial','hcat','lastMonth','period','menu_id','permissions'));
        }

        if($request->p_type == 'YTD' && $request->b_type == 'Closing')
        {
            $period = GeneralLedger::first()->period;
            $lastMonth =  Carbon::parse($request->period)->format('M-y');
            $trial = 3;
            return view('trial.trialBalance',compact('p_type','b_type','index','trial','hcat','lastMonth','period','menu_id','permissions'));
        }


    }


    public function customerReport(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        $customer = Vendors::with(['sales' => function($query1) {
            $query1->withCount(['transaction as paid' => function($query2){
                $avg = $query2->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }]);
        }])
        ->where('v_type','Customer')
        ->withCount(['sales as total' => function($query) {
            $avg = $query->select(DB::raw('count(id)'));
        }])
        ->withCount(['sales as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'));
        }])

        ->get();
        return view('reports.customer',compact('customer','permissions'));
    }

    public function customerReportDetail($id)
    {
        $sales = Sales::where('c_id',$id)
        ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
        }])
        ->get();
        $index =0;
        return view('reports.customerDetail',compact('sales','id','index'));
    }

    public function customerReportexcel($id)
    {
        return Excel::download(new CustomerDetailReportExport($id), 'CustomerDetailReport.xlsx');
    }

    public function customerReportsearchYExcel($year,$id)
    {
        return Excel::download(new CustomerDetailReportExportYearly($year,$id), 'CustomerDetailReport.xlsx');
    }

    public function customerReportsearchMExcel($month,$id)
    {
        return Excel::download(new CustomerDetailReportExportMonthly($month,$id), 'CustomerDetailReport.xlsx');
    }

    public function customerReportsearchDExcel($date,$id)
    {
        return Excel::download(new CustomerDetailReportExportDate($date,$id), 'CustomerDetailReport.xlsx');
    }

    public function customerReportsearchDDExcel($from,$to,$id)
    {
        return Excel::download(new CustomerDetailReportExportDateDifference($from,$to,$id), 'CustomerDetailReport.xlsx');
    }

    public function customerReportsearchExcel($index,$id)
    {
        return Excel::download(new CustomerDetailReportExportDateConditions($index,$id), 'CustomerDetailReport.xlsx');
    }

    public function customerReportsearch(Request $request)
    {
        $id            =   $request->id;
        if($request->optradio == 'Year')
        {
            $index = 1;
            $year = $request->year;

            $sales = Sales::where('c_id',$id)
            ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
            ->get();

            return view('reports.customerDetail',compact('sales','id','index','year'));
        }
        if($request->optradio == 'Month')
        {
           $sales = Sales::where('c_id',$id)
            ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
            ->get();
            $index = 2;
            $month = $request->month;
            return view('reports.customerDetail',compact('sales','id','index','month'));
        }
        if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $index = 3;
                $from = $request->from;
                $to = $request->to;

                $sales = Sales::where('c_id',$id)
                ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                ->get();

                return view('reports.customerDetail',compact('sales','id','index','from','to'));
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }

                $sales = Sales::where('c_id',$id)
                ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                ->get();
                return view('reports.customerDetail',compact('sales','id','index','date'));
            }
        }
        if($request->optradio == 'lastweek')
        {
            $index = 5;
            $date = Carbon::today()->subDays(7);
            $sales = Sales::where('c_id',$id)
            ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where('sale_date','>=',$date)
            ->get();

            return view('reports.customerDetail',compact('sales','id','index','date'));
        }
        if($request->optradio == 'last15Days')
        {
            $index = 6;
            $date = Carbon::today()->subDays(15);
            $sales = Sales::where('c_id',$id)
            ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where('sale_date','>=',$date)
            ->get();
            return view('reports.customerDetail',compact('sales','id','index','date'));
        }
        if($request->optradio == 'lastMonth')
        {
            $index = 7;
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');

            $sales = Sales::where('c_id',$id)
            ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$month)
            ->get();

            return view('reports.customerDetail',compact('sales','id','index','month'));
        }
    }

    public function supplierReport(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        $supplier = Vendors::with(['purchases' => function($query1) {
            $query1->withCount(['transaction as paid' => function($query2){
                $avg = $query2->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }]);
        }])
        ->where('v_type','Supplier')
        ->withCount(['purchases as total' => function($query) {
            $avg = $query->select(DB::raw('count(id)'));
        }])
        ->withCount(['purchases as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'));
        }])

        ->get();
        return view('reports.supplier',compact('supplier','permissions'));
    }


    public function supplierReportDetail($id)
    {
        $purchase = PurchaseOrder::where('s_id',$id)
        ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
        }])
        ->get();
        $index = 0;
        return view('reports.supplierDetail',compact('purchase','id','index'));
    }

    public function supplierReportsearch(Request $request)
    {
        $id            =   $request->id;
        if($request->optradio == 'Year')
        {
            $index = 1;
            $year = $request->year;

            $purchase = PurchaseOrder::where('s_id',$id)
            ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }])
            ->where(DB::raw("(DATE_FORMAT(order_date,'%Y'))"),$request->year)
            ->get();

            return view('reports.supplierDetail',compact('purchase','id','index','year'));
        }
        if($request->optradio == 'Month')
        {
            $purchase = PurchaseOrder::where('s_id',$id)
            ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }])
            ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$request->month)
            ->get();
            $index = 2;
            $month = $request->month;

            return view('reports.supplierDetail',compact('purchase','id','index','month'));
        }
        if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $index = 3;
                $from = $request->from;
                $to = $request->to;

                $purchase = PurchaseOrder::where('s_id',$id)
                ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->whereBetween(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                ->get();

                return view('reports.supplierDetail',compact('purchase','id','index','from','to'));
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }

                $purchase = PurchaseOrder::where('s_id',$id)
                ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),$date)
                ->get();

                return view('reports.supplierDetail',compact('purchase','id','index','date'));
            }
        }
        if($request->optradio == 'lastweek')
        {
            $index = 5;
            $date = Carbon::today()->subDays(7);
            $purchase = PurchaseOrder::where('s_id',$id)
            ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }])
            ->where('order_date','>=',$date)
            ->get();

            return view('reports.supplierDetail',compact('purchase','id','index','date'));
        }
        if($request->optradio == 'last15Days')
        {
            $index = 6;
            $date = Carbon::today()->subDays(15);
            $purchase = PurchaseOrder::where('s_id',$id)
            ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }])
            ->where('order_date','>=',$date)
            ->get();

            return view('reports.supplierDetail',compact('purchase','id','index','date'));
        }
        if($request->optradio == 'lastMonth')
        {
            $index = 7;
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');

            $purchase = PurchaseOrder::where('s_id',$id)
            ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }])
            ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$month)
            ->get();

            return view('reports.supplierDetail',compact('purchase','id','index','month'));
        }
    }

    public function supplierReportexcel($id)
    {
        return Excel::download(new SupplierDetailReportExport($id), 'SupplierDetailReport.xlsx');
    }

    public function supplierReportsearchYExcel($year,$id)
    {
        return Excel::download(new SupplierDetailReportExportYearly($year,$id), 'SupplierDetailReport.xlsx');
    }

    public function supplierReportsearchMExcel($month,$id)
    {
        return Excel::download(new SupplierDetailReportExportMonthly($month,$id), 'SupplierDetailReport.xlsx');
    }

    public function supplierReportsearchDExcel($date,$id)
    {
        return Excel::download(new SupplierDetailReportExportDate($date,$id), 'SupplierDetailReport.xlsx');
    }

    public function supplierReportsearchDDExcel($from,$to,$id)
    {
        return Excel::download(new SupplierDetailReportExportDateDifference($from,$to,$id), 'SupplierDetailReport.xlsx');
    }

    public function supplierReportsearchExcel($index,$id)
    {
        return Excel::download(new SupplierDetailReportExportDateConditions($index,$id), 'SupplierDetailReport.xlsx');
    }


}

