<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\VendorsUpdateRequest;
use DataTables;
use Auth;

class SupplierUpdateRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('supplier.request',compact('permissions'));
    }
    public function datatable()
    {
        $vendor=VendorsUpdateRequest::where('v_type','Supplier')->get();
        // dd($vendor);
        return DataTables::of($vendor)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=Vendors::with(['city'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }

    public function status($id)
    {
        $v = VendorsUpdateRequest::find($id);
        $u_id = Auth::user()->id;

        Vendors::where('id',$id)
        ->update([
            'company' => $v->company,
            'name' => $v->name,
            'address' => $v->address,
            'c_no' => $v->c_no,
            'country' => $v->country,
            'VAT' => $v->NTN,
            'GST' => $v->GST,
            'state' => $v->state,
            'email' => $v->email,
            'postalCode' => $v->postalCode,
            'c_id' => $v->c_id,
            'updated_by' => $u_id
        ]);
        VendorsUpdateRequest::where('id',$id)
        ->update([
            'status' => 'Approved'
        ]);

        toastr()->success('Supplier updated successfully!');
        return redirect(url('').'/supplierUpdateRequest');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
