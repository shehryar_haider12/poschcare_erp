<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\ProductVariantUpdate;
use App\ProductVariants;
use App\HeadCategory;
use App\AccountDetails;
use App\ProductsUpdateRequest;
use DataTables;
use Auth;
use App\Brands;
use App\Category;
use DB;

class ProductUpdateRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.request',compact('cat','brands','permissions'));
    }

    public function datatable()
    {
        $product=ProductsUpdateRequest::with(['brands','unit','category','subcategory'])
        ->get();
        return DataTables::of($product)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $product = ProductsUpdateRequest::with(['brands','unit','category','subcategory'])
            ->where('id',$id)
            ->first();
            return $product;
        }
    }

    public function status($id)
    {
        $v = ProductsUpdateRequest::where('id',$id)->where('status','Pending')->first();
        $u_id = Auth::user()->id;
        $pvariant = ProductVariantUpdate::where('p_id',$id)->where('status','Pending')->get();
        if(isset($v->image))
        {
            $data=([
                'image' => $v->image,
                's_cat_id' => $v->s_cat_id,
                'pro_name' => $v->pro_name,
                'pro_code' => $v->pro_code,
                'weight' => $v->weight,
                'unit_id' => $v->unit_id,
                'cost' => $v->cost,
                'tp' => $v->tp,
                'alert_quantity' => $v->alert_quantity,
                'brand_id' => $v->brand_id,
                'cat_id' => $v->cat_id,
                'visibility' => $v->visibility,
                'description' => $v->description,
                'p_type' => $v->p_type,
                'vstatus' => $v->vstatus,
                'updated_by' => $u_id,
                'mrp' => $v->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$v->image);
            Products::where('id',$id)
            ->update($data);

        }
        else
        {
            $data=([
                's_cat_id' => $v->s_cat_id,
                'pro_name' => $v->pro_name,
                'pro_code' => $v->pro_code,
                'weight' => $v->weight,
                'unit_id' => $v->unit_id,
                'cost' => $v->cost,
                'tp' => $v->tp,
                'alert_quantity' => $v->alert_quantity,
                'brand_id' => $v->brand_id,
                'cat_id' => $v->cat_id,
                'visibility' => $v->visibility,
                'p_type' => $v->p_type,
                'description' => $v->description,
                'updated_by' => $u_id,
                'vstatus' => $v->vstatus,
                'mrp' => $v->mrp
            ]);
            Products::where('id',$id)
            ->update($data);

        }

        ProductsUpdateRequest::where('id',$id)->where('status','Pending')
        ->update([
            'status' => 'Approved'
        ]);
        if($pvariant->isEmpty())
        {
        }
        else {
            ProductVariantUpdate::where('p_id',$id)->where('status','Pending')
            ->update([
                'status' => 'Approved'
            ]);
            foreach ($pvariant as $key => $v) {
                ProductVariants::create([
                    'p_id' => $id,
                    'name' => $v->name,
                    'status' => 1,
                    'cost' => $v->cost,
                    'price' => $v->price,
                    'tp' => $v->tp,
                ]);
                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id1 = 001;
                }
                else
                {
                    $id1 = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }
                // dd($id1);
                $str_length = strlen((string)$id1)+2;
                $id1 = substr("0000{$id1}", -$str_length);
                $code = $hcat->code.'-'.$id1;

                $data1 = [
                    'Code' => $code,
                    'name_of_account' => $v->name,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id,
                    'type' => 1
                ];
                AccountDetails::create($data1);


            }
        }
        toastr()->success('Product information updated successfully!');
        return redirect(url('').'/productUpdateRequest');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
