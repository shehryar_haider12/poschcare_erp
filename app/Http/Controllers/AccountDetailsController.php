<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeadofAccounts;
use App\HeadCategory;
use App\AccountDetails;
use App\Bank;
use App\Vendors;
use App\Products;
use App\ProductVariants;
use App\GeneralLedger;
use Auth;
use App\Exports\AccountHistoryExport;
use App\Exports\AccountHistoryYearlyExport;
use App\Exports\AccountHistoryMonthlyExport;
use App\Exports\AccountHistoryDateExport;
use App\Exports\AccountHistoryDateDifferenceExport;
use App\Exports\AccountHistoryDateConditionsExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Carbon\Carbon;
use DB;
// use Schema;

class AccountDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $hcat=HeadCategory::whereNotIn('name',['Purchases','Revenue'])->get();
        $account=AccountDetails::with(['headCategory','generalLedger','parent'])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        return view('accounts.index',compact('account','hcat','permissions','menu_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hcat=HeadCategory::whereNotIn('name',['Purchases','Revenue'])->get();
        $hoa=HeadofAccounts::all();
        $data=[
            'hcat' => $hcat,
            'hoa' => $hoa,
        ];
        return view('accounts.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $c_id = substr($request->c_id,0,strpos($request->c_id,"-"));
        $account_type = substr($request->c_id,strpos($request->c_id,"-")+1);
        $request->validate([
            'Code'      =>  'required|unique:account_details',
            'name_of_account' => 'required',
            'c_id' => 'required',
        ]);
        if(str_contains($request->name_of_account,'_'))
        {
            $type =  substr($request->name_of_account,  strpos($request->name_of_account, '_')+1);
        }
        else {
            $type = null;
        }
        if($type == null)
        {
            $account=AccountDetails::where('name_of_account',$request->name_of_account)
            ->where('c_id',$c_id)
            ->where('account_type',$account_type)
            ->first();
            $u_id = Auth::user()->id;
            $data = [
                'Code' => $request->Code,
                'name_of_account' => $request->name_of_account,
                'created_by' => $u_id,
                'type' => $type,
                'c_id' => $c_id,
                'account_type' => $account_type,
            ];
            if($account==null)
            {
                $account_c = AccountDetails::create($data);
                if($request->opening != null)
                {
                    $posted_date = Carbon::now()->format('Y-m-d');
                    $period = Carbon::now()->format('M-y');
                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id + 1;
                    }
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Opening Account',
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' =>  $posted_date,
                        'posted_date' =>  $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'currency_code' => 'PKR',
                        'debit' => $request->opening,
                        'credit' => '0',
                        'net_value' => $request->opening,
                        'balance' => $request->opening
                    ]);
                }
                toastr()->success('Account created successfully!');
                return redirect()->back();
            }
            else
            {
                toastr()->error('Account already exist of this Category!');
                return redirect()->back();
            }
        }
        else
        {
            $name = substr($request->name_of_account,0,strpos($request->name_of_account,'_'));
            $account=AccountDetails::where('name_of_account',$name)
            ->where('c_id',$c_id)
            ->where('account_type',$account_type)
            ->first();
            $u_id = Auth::user()->id;
            $data = [
                'Code' => $request->Code,
                'name_of_account' => $name,
                'created_by' => $u_id,
                'type' => $type,
                'c_id' => $c_id,
                'account_type' => $account_type,
            ];
            if($account==null)
            {
                $account_c = AccountDetails::create($data);
                toastr()->success('Account created successfully!');
                return redirect()->back();
            }
            else
            {
                toastr()->error('Account already exist of this Category!');
                return redirect()->back();
            }
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = AccountDetails::where('Code',$id)
        ->first();
        return $account;
    }

    public function prodName($id)
    {
        // dd($id);
        $account = AccountDetails::where('name_of_account',$id)
        ->first();
        if($account->type == 0)
        {
            $id =  substr($id,  strpos($id, '-')+2);
            $product = Products::where('pro_name',$id)->first();
            $code = $product->pro_code;
            return [$account,$product,$code];
        }
        else {

            $product = ProductVariants::where('name',$id)->first();
            $code = substr($product->name,0,strpos($product->name,"-"));
            return [$account,$product,$code];
        }

    }

    public function view($id,$type)
    {
        if($type == 'p')
        {
            $account = AccountDetails::where('c_id',$id)->latest('created_at')->orderBy('id','desc')->first();

            $hcat = HeadCategory::find($id);

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1;
            }
            // dd($id);
            $str_length = strlen((string)$id)+2;
            // dd($str_length);
            $id = substr("0000{$id}", -$str_length);
            // dd($id);

            if($hcat->name == 'Receivables')
            {
                $customer=Vendors::where('v_type','Customer')->get();
                $code = $hcat->code.'-'.$id;
                return [$code,$customer];
            }

            if($hcat->name == 'Loan')
            {
                $customer=Vendors::where('v_type','Customer')->get();
                $code = $hcat->code.'-'.$id;
                return [$code,$customer];
            }

            if($hcat->name == 'Short Loan')
            {
                $customer=Vendors::where('v_type','Customer')->get();
                $code = $hcat->code.'-'.$id;
                return [$code,$customer];
            }

            if($hcat->name == 'Inventory')
            {
                $product=Products::with('variants')->get();
                $code = $hcat->code.'-'.$id;
                return [$code,$product];
            }
            if($hcat->name == 'Bank')
            {
                $bank=Bank::with('city')->get();
                $code = $hcat->code.'-'.$id;
                return [$code,$bank];
            }
            if($hcat->name == 'Payables')
            {
                $supplier=Vendors::where('v_type','Supplier')->get();
                $code = $hcat->code.'-'.$id;
                return [$code,$supplier];
            }
            else
            {
                $code = $hcat->code.'-'.$id;
                return [$code];
            }
        }
        else
        {
            $accountId = AccountDetails::where('id',$id)->first();
            $account = AccountDetails::where('c_id',$accountId->id)->where('account_type',$type)->latest('created_at')->orderBy('id','desc')->first();
            // $hcat = HeadCategory::find($accountId->c_id);

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $explode = explode('-',$account->Code);
                $countString = count($explode);
                $id = $explode[$countString - 1] + 1;
            }
            // dd($id);
            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);

            // if($hcat->name == 'Receivables')
            // {
            //     $customer=Vendors::where('v_type','Customer')->get();
            //     $code = $accountId->Code.'-'.$id;
            //     return [$code,$customer];
            // }

            // if($hcat->name == 'Loan')
            // {
            //     $customer=Vendors::where('v_type','Customer')->get();
            //     $code = $accountId->Code.'-'.$id;
            //     return [$code,$customer];
            // }

            // if($hcat->name == 'Short Loan')
            // {
            //     $customer=Vendors::where('v_type','Customer')->get();
            //     $code = $accountId->Code.'-'.$id;
            //     return [$code,$customer];
            // }

            // if($hcat->name == 'Inventory')
            // {
            //     $product=Products::with('variants')->get();
            //     $code = $accountId->Code.'-'.$id;
            //     return [$code,$product];
            // }
            // if($hcat->name == 'Bank')
            // {
            //     $bank=Bank::with('city')->get();
            //     $code = $accountId->Code.'-'.$id;
            //     return [$code,$bank];
            // }
            // if($hcat->name == 'Payables')
            // {
            //     $supplier=Vendors::where('v_type','Supplier')->get();
            //     $code = $accountId->Code.'-'.$id;
            //     return [$code,$supplier];
            // }
            // else
            // {
                $code = $accountId->Code.'-'.$id;
                return [$code];
            // }
            // dd($code);
        }

    }

    public function chkHistory($id)
    {
        $ledger = GeneralLedger::where('account_code',$id)
        ->get();
        if($ledger->isEmpty())
        {
            return $ledger=0;
        }
        else
        {
            return $ledger;
        }

    }


    public function excel($id,$code,$name)
    {
        return Excel::download(new AccountHistoryExport($id,$code,$name), 'Ledger.xlsx');
    }

    public function searchYExcel($year,$id)
    {
        return Excel::download(new AccountHistoryYearlyExport($year,$id), 'LedgerYearly.xlsx');
    }

    public function searchMExcel($month,$id)
    {
        return Excel::download(new AccountHistoryMonthlyExport($month,$id), 'LedgerMonthly.xlsx');
    }

    public function searchDExcel($date,$id)
    {
        return Excel::download(new AccountHistoryDateExport($date,$id), 'LedgerDateWise.xlsx');
    }

    public function searchDDExcel($from,$to,$id)
    {
        return Excel::download(new AccountHistoryDateDifferenceExport($from,$to,$id), 'LedgerDateRange.xlsx');
    }

    public function searchExcel($index,$id)
    {
        return Excel::download(new AccountHistoryDateConditionsExport($index,$id), 'Ledger.xlsx');
    }

    public function pdf($id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code',$id)
        ->get();
        $id = substr($id, 0, strpos($id, '-', strpos($id, '-')+1));
        $pdf = PDF::loadView('accounts.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }

    public function searchYPDF($year,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y'))"),$year)
        ->get();
        $id = $id;
        $pdf = PDF::loadView('accounts.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchMPDF($month,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$month)
        ->get();
        $id = $id;
        $pdf = PDF::loadView('accounts.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchDPDF($date,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),$date)
        ->get();
        $id = $id;
        $pdf = PDF::loadView('accounts.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchDDPDF($from,$to,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->whereBetween(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),[$from,$to])
        ->get();
        $id = $id;
        $pdf = PDF::loadView('accounts.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchPDF($index,$id)
    {
        if($index == 5)
        {
            $date = Carbon::today()->subDays(7);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
        }
        if($index == 6)
        {
            $date = Carbon::today()->subDays(15);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
        }
        if($index == 7)
        {
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$month)
            ->get();
        }
        $id = $id;
        $pdf = PDF::loadView('accounts.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }


    public function history($id,$mid)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code',$id)
        ->get();
        $permissions        =   getRolePermission($mid);
        // $id = substr($id, 0, strpos($id, '-', strpos($id, '-')+1));
        $index = 0;
        $mid = $mid;
        return view('accounts.history',compact('ledger','id','permissions','index','mid'));
    }


    public function search(Request $request)
    {
        // dd($request->all());
        $mid            =   $request->menuid;
        $id            =   $request->id;
        $permissions        =   getRolePermission($mid);

        if($request->optradio == 'Year')
        {
            $index = 1;
            $year = $request->year;
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y'))"),$request->year)
            ->get();
            return view('accounts.history',compact('ledger','id','permissions','index','mid','year'));
        }
        if($request->optradio == 'Month')
        {
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$request->month)
            ->get();
            $index = 2;
            $month = $request->month;
            return view('accounts.history',compact('ledger','id','permissions','index','mid','month'));
        }
        if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $index = 3;
                $from = $request->from;
                $to = $request->to;
                $ledger = GeneralLedger::with('createUser')
                ->where('account_code','like',$id.'%')
                ->whereBetween(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                ->get();
                return view('accounts.history',compact('ledger','id','permissions','index','mid','from','to'));
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                $ledger = GeneralLedger::with('createUser')
                ->where('account_code','like',$id.'%')
                ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),$date)
                ->get();
                return view('accounts.history',compact('ledger','id','permissions','index','mid','date'));
            }
        }
        if($request->optradio == 'lastweek')
        {
            $index = 5;
            $date = Carbon::today()->subDays(7);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
            return view('accounts.history',compact('ledger','id','permissions','index','mid','date'));
        }
        if($request->optradio == 'last15Days')
        {
            $index = 6;
            $date = Carbon::today()->subDays(15);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
            return view('accounts.history',compact('ledger','id','permissions','index','mid','date'));
        }
        if($request->optradio == 'lastMonth')
        {
            $index = 7;
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$month)
            ->get();
            return view('accounts.history',compact('ledger','id','permissions','index','mid','month'));
        }
    }
}
