<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\VendorsUpdateRequest;
use App\City;
use App\User;
use App\Roles;
use DataTables;
use App\Exports\SalePersonExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Hash;
use Auth;

class SalePersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('saleperson.index',compact('permissions'));
    }

    public function datatable()
    {
        $vendor=Vendors::where('v_type','Saleperson')->get();
        return DataTables::of($vendor)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $city=City::where('status',1)->get();
        $user=User::max('id');
        // dd($user);
        if($user == null)
        {
            $u_id=1;
        }
        else
        {
            $u_id=$user+1;
        }
        $data= [
            'isEdit' => false,
            'city' => $city,
            'u_id' => $u_id,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('saleperson.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }

        $u_id = Auth::user()->id;
        $password = Hash::make($request->password);
        $role = Roles::where('name','Sale Person')->first();
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'r_id' => $role->id,
        ]);
        $saleperson = Vendors::create([
            'name' => $request->name,
            'c_id' => $request->c_id,
            'address' => $request->address,
            'c_no' => $request->c_no,
            'email' => $request->email,
            'v_type' => $request->v_type,
            'u_id' => $request->u_id,
            'created_by' => $u_id,
            'status' => $status
        ]);
        if(isset($request->request_type) && $request->request_type == "ajax")
        {
            $response['message'] = 'Sale person added successfully!';
            $response['saleperson'] = $saleperson;
            return response()->json($response, 200);
        }
        toastr()->success('Sale person added successfully!');
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=Vendors::with(['city'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor=Vendors::with(['city','user'])
        ->where('id',$id)
        ->first();
        $city=City::where('status',1)->get();
        $data= [
            'isEdit' => true,
            'city' => $city,
            'vendor' => $vendor
        ];
        return view('saleperson.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        $user = User::find($request->u_id);
        // dd($user);

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            if($user == null)
            {
                $password = Hash::make($request->password);
                $role = Roles::where('name','Sale Person')->first();
                $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => $password,
                    'r_id' => $role->id,
                    'w_id' => 1
                ]);
            }
            else {

                if($request->password == null)
                {
                    User::where('id',$request->u_id)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email
                    ]);
                    $password = null;
                }
                else
                {
                    $password = Hash::make($request->password);
                    User::where('id',$request->u_id)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => $password
                    ]);
                }
            }
            $vendor=Vendors::where('id',$id)
            ->update([
                'name' => $request->name,
                'address' => $request->address,
                'c_no' => $request->c_no,
                'email' => $request->email,
                'c_id' => $request->c_id,
                'updated_by' => $u_id,
                'u_id' => $user->id
            ]);
            toastr()->success('Sale person updated successfully!');
            return redirect(url('').'/saleperson');
        }
        else
        {
            VendorsUpdateRequest::create([
                'name' => $request->name,
                'c_id' => $request->c_id,
                'address' => $request->address,
                'c_no' => $request->c_no,
                'email' => $request->email,
                'v_type' => $request->v_type,
                'u_id' => $request->u_id,
                'password' => $password
            ]);
            toastr()->success('Sale person update request has been sent successfully!');
            return redirect(url('').'/saleperson');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new SalePersonExport, 'Salepersons.xlsx');
    }

    public function pdf()
    {
        $vendor =  Vendors::where('v_type','Saleperson')
        ->select('name','address','c_no','email')
        ->get();
        // dd($vendor);
        $pdf = PDF::loadView('saleperson.pdf', compact('vendor'));

        return $pdf->download('salepersons.pdf');
    }


    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Vendors::find($id);
        if ($item->update(['status' => $status])) {
            Vendors::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
