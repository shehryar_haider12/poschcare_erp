<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\VendorsUpdateRequest;
use DataTables;
use Auth;

class CustomerUpdateRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('customer.request',compact('permissions'));
    }

    public function datatable()
    {
        $vendor=VendorsUpdateRequest::where('v_type','Customer')->get();
        // dd($vendor);
        return DataTables::of($vendor)->make();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=VendorsUpdateRequest::with(['city','cgroup','pgroup'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }
    public function status($id)
    {
        $v = VendorsUpdateRequest::find($id);
        $u_id = Auth::user()->id;
        Vendors::where('id',$id)
        ->update([
            'c_group' => $v->c_group,
            'p_group' => $v->p_group,
            'company' => $v->company,
            'name' => $v->name,
            'c_no' => $v->c_no,
            'name2' => $v->name2,
            'c_no2' => $v->c_no2,
            'address' => $v->address,
            'country' => $v->country,
            'VAT' => $v->NTN,
            'GST' => $v->GST,
            'state' => $v->state,
            'email' => $v->email,
            'postalCode' => $v->postalCode,
            'c_id' => $v->c_id,
            'updated_by' => $u_id
        ]);
        VendorsUpdateRequest::where('id',$id)
        ->update([
            'status' => 'Approved'
        ]);

        toastr()->success('Customer updated successfully!');
        return redirect(url('').'/customerUpdateRequest');
    }


}
