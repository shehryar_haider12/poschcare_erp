<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\CostType;
use App\User;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CostTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return  view('costType.index');
    }

    public function datatable()
    {
        $ct=CostType::all();
        return DataTables::of($ct)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('costType.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'cost_type' =>  'required'
        ]);
        $gst = CostType::create($data);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Cost type has been added by '.$u_name,
            'link' => url('').'/costtype',
            'name' => 'View Cost Types List',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Cost type added successfully!');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CostType $costtype)
    {
        $data=[
            'isEdit' => true,
            'ct' => $costtype
        ];
        return view('costType.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostType $costtype)
    {
        $data = $request->validate([
            'cost_type' =>  'required|unique:cost_type,cost_type,'.$costtype->id
        ]);
        $data['updated_by'] = Auth::user()->id;
        $costtype->update($data);
        toastr()->success('Cost type updated successfully!');
        return redirect(url('').'/costtype');
    }
}
