<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseOrder;
use App\PurchaseOrderDetails;
use App\TransactionHistory;
use App\Warehouse;
use App\Vendors;
use App\City;
use App\Bank;
use App\AvgCost;
use App\ProductVariants;
use App\Products;
use App\Unit;
use App\Brands;
use App\Category;
use App\Subcategory;
use App\Stocks;
use App\StockReceiving;
use App\GeneralLedger;
use App\HeadCategory;
use App\AccountDetails;
use App\CurrentStock;
use App\User;
use App\CostType;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use DataTables;
use Storage;
use DB;
use PDF;
use Session;
use Response;
use Auth;
use Carbon\Carbon;
use App\Exports\PurchaseExport;
use App\Exports\PurchaseStatusExport;
use App\Exports\PurchaseYearlyExport;
use App\Exports\PurchaseMonthlyExport;
use App\Exports\PurchaseDateExport;
use App\Exports\PurchaseDateConditionsExport;
use App\Exports\PurchaseDateDifferenceExport;
use App\Exports\PurchaseOrderStructreExport;
use Maatwebsite\Excel\Facades\Excel;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function importStructure()
    {
        return Excel::download(new PurchaseOrderStructreExport, 'PurchaseOrderStructureExport.xlsx');
    }

    public function index(Request $request)
    {
        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        $bank=Bank::where('status',1)->get();
        $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
        }])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $index = 0;
        $adminName = Auth::user()->name;
        // return $porder;
        return view('porder.index',compact('adminName','bank','supplier','menu_id','permissions','porder','index'));
    }


    public function product($id)
    {
        $purchase = PurchaseOrderDetails::with('products')
        ->with(['purchase' => function($query) {
            $avg = $query->select(DB::raw('*'))->get();
        },'purchase.warehouse' => function($query) {
            $avg = $query->select(DB::raw('*'));
        }
        ])
        ->where('p_id',$id)
        ->get();
        return $purchase;
    }

    public function GRN($id)
    {
        // dd($id);
        $p = PurchaseOrder::find($id);
        $purchase = PurchaseOrderDetails::with('stock','products','variant')
        ->where('o_id',$id)
        ->get();
        return $purchase;
    }

    public function report()
    {
        $supplier=Vendors::where('v_type','Supplier')->get();
        $ware=Warehouse::all();
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        $purchasedetail=PurchaseOrderDetails::with(['purchase','purchase.warehouse','purchase.supplier','products','variant'])
        ->get();
        return view('porder.report',compact('supplier','ware','product','purchasedetail'));
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $id     = $request->id;
        $status = $request->status;
        $u_id = Auth::user()->id;
        $item = PurchaseOrder::find($id);
        if ($item->update(['status' => $status])) {
            PurchaseOrder::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function prodDatatable()
    {
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        return DataTables::of($product)->make();
    }
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $order=PurchaseOrder::max('id');
        // dd($sales);
        if($order == null)
        {
            $id=1;
        }
        else
        {
            $id=$order+1;
        }
        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $ware=Warehouse::all();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city = City::where('status',1)->get();
        $date=Carbon::now()->format('Y-m-d');
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $data=[
            'isEdit' => false,
            'supplier' => $supplier,
            'biller' => $biller,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'city' => $city,
            'product' => $product,
            'date' => $date,
            'id' => $id,
            'ct' => CostType::all(),
            'permissions' => getRolePermission($menu_id)
        ];
        return view('porder.create',$data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'ref_no'      =>  'max:11|unique:purchase_order'
        // ]);
        if($request->final == 'NaN')
        {
            toastr()->error('Network Error!');
            return redirect()->bak();
        }
        else
        {
            $role_id = Auth::user()->r_id;
            $env_a_id = config('app.adminId');
            $env_m_id = config('app.managerId');
            if($role_id == $env_a_id || $role_id == $env_m_id)
            {
                $status = 'Approved';
            }
            else
            {
                $status = 'Pending';
            }
            $u_id = Auth::user()->id;
            if(isset($request->doc))
            {
                $data=([
                    'order_date' => $request->order_date,
                    'ref_no' => $request->ref_no,
                    'w_id' => $request->w_id,
                    // 'status' => $request->status,
                    'doc' => $request->doc,
                    's_id' => $request->s_id,
                    'b_id' => $request->b_id,
                    'note' => $request->editor1,
                    'total' => $request->final,
                    'created_by' => $u_id,
                    'status' => $status,
                    'tax_status' => $request->tax_status,
                    'tax' => $request->tax,
                    'exp_rcv_date' => $request->exp_rcv_date,
                    'payment_mode' => $request->payment_mode,
                    'days' => $request->days,
                    'pdate' => $request->pdate,
                ]);
                $data['doc']= Storage::disk('uploads')->putFile('',$request->doc);
            }
            else
            {
                $data=([
                    'order_date' => $request->order_date,
                    'ref_no' => $request->ref_no,
                    'w_id' => $request->w_id,
                    // 'status' => $request->status,
                    's_id' => $request->s_id,
                    'b_id' => $request->b_id,
                    'note' => $request->editor1,
                    'total' => $request->final,
                    'created_by' => $u_id,
                    'status' => $status,
                    'tax_status' => $request->tax_status,
                    'tax' => $request->tax,
                    'exp_rcv_date' => $request->exp_rcv_date,
                    'payment_mode' => $request->payment_mode,
                    'days' => $request->days,
                    'pdate' => $request->pdate,
                ]);
            }

            $p=PurchaseOrder::create($data);


            for ($i=1; $i <= count($request->p_id) ; $i++) {
                PurchaseOrderDetails::create([
                    'o_id' => $p->id,
                    'p_id' => $request->p_id[$i],
                    'quantity' => $request->quantity[$i],
                    'cost' => $request->cost[$i],
                    'sub_total' => $request->sub_total[$i],
                    'type' => $request->type[$i],
                ]);
            }

            $u_name = Auth::user()->name;
            $user = User::where('r_id',config('app.adminId'))->get();
            $data1 = [
                'notification' => 'New Purchase order request has been added by '.$u_name,
                'link' => url('')."/purchase",
                'name' => 'View Purchase Orders',
            ];
            Notification::send($user, new AddNotification($data1));

            toastr()->success('Purchase Order created successfully! Wait For The Invoice Download');
            // return $this->pdf($p->id);
            Session::flash('download', url('')."/purchase/pdf");
            return redirect(url('')."/purchase");
        }
    }

    public function pdf1()
    {
        $id=PurchaseOrder::max('id');
        return $this->pdf($id);
    }


    public function purchaseDetail($id)
    {
        $details = PurchaseOrderDetails::with(['products.unit','variant.product.unit'])->where('id',$id)->first();
        return $details;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $p=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$id)
        ->first();
        $d=PurchaseOrderDetails::with(['products','products.brands','products.unit','purchase','variant','variant.product.unit','variant.product.brands'])
        ->where('o_id',$id)
        ->get();
        return [$p,$d];
    }

    public function pdf($id)
    {
        $purchase=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$id)
        ->first();
        $pdetail=PurchaseOrderDetails::with(['products','products.brands','products.unit','purchase','variant.product.unit','variant.product.brands'])
        ->where('o_id',$id)
        ->get();
        $th=TransactionHistory::where('p_s_id',$id)
        ->where('p_type','Purchase')
        ->sum('total');
        // dd($saledetail);
        $pdf = PDF::loadView('porder.pdf', compact('purchase','pdetail','th'));
        // return view('porder.pdf', compact('purchase','pdetail','th'));

        return $pdf->download('purchaseOrder.pdf');
    }
    public function purchaseInvoice($id)
    {
        $purchase=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$id)
        ->first();
        $pdetail=PurchaseOrderDetails::with(['products','products.brands','products.unit','purchase','variant.product.brands'])
        ->where('o_id',$id)
        ->get();
        $th=TransactionHistory::where('p_s_id',$id)
        ->where('p_type','Purchase')
        ->sum('total');
        $pdf = PDF::loadView('porder.pinvoice', compact('purchase','pdetail','th'));

        return $pdf->download('purchaseInvoice.pdf');
    }

    public function document($id)
    {
        $purchase = PurchaseOrder::find($id);
        $file= public_path(). "/uploads/". $purchase->doc;
        // return  Storage::download($file);
        $headers = array(
                'Content-Type: application/pdf',
                );

        return Response::download($file, $purchase->doc, $headers);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $purchase=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$id)
        ->first();
        $pdetail=PurchaseOrderDetails::with(['products','products.brands','products.category','products.unit','purchase','variant.product.brands','variant.product.unit'])
        ->with(['products.currentstocks' => function($query) use ($purchase) {
            $avg = $query->select(DB::raw('*'))->where('w_id',$purchase->w_id);
        }])
        ->with(['variant.currentstocks' => function($query) use ($purchase) {
            $avg = $query->select(DB::raw('*'))->where('w_id',$purchase->w_id);
        }])
        ->where('o_id',$id)
        ->get();
        // dd($pdetail);
        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $ware=Warehouse::all();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city=City::where('status',1);
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $data=[
            'isEdit' => true,
            'invoice' => false,
            'supplier' => $supplier,
            'biller' => $biller,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'city' => $city,
            'product' => $product,
            'id' => $id,
            'purchase' => $purchase,
            'ct' => CostType::all(),
            'pdetail' => $pdetail
        ];
        return view('porder.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function GRR($grn,$pid)
    {
        $purchase=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$pid)
        ->first();
        $pdetail=Stocks::with(['products','products.unit','products.brands','variant.product.unit','variant.product.brands'])
        ->where('grn_no',$grn)
        ->get();
        $pdf = PDF::loadView('porder.grr', compact('purchase','pdetail','grn'));
        // return view('porder.grr', compact('purchase','pdetail','grn'));

        return $pdf->download('GRR.pdf');
    }

    public function GRR1($id)
    {
        $pid = substr($id, strrpos($id, '-') + 1);
        $grn = substr($id, 0, strpos($id, "-"));
        $purchase=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$pid)
        ->first();
        $pdetail=Stocks::with(['products','products.unit','products.brands','variant.product.unit','variant.product.brands'])
        ->where('grn_no',$grn)
        ->get();
        $pdf = PDF::loadView('porder.grr', compact('purchase','pdetail','grn'));
        // return view('porder.grr', compact('purchase','pdetail','grn'));

        return $pdf->download('GRR.pdf');
    }


    public function updateQuantity(Request $request)//receiving order
    {
        // dd($request->all());

        $status = false;
        if(isset($request->hasUnit))
        {
            for ($j=0; $j < count($request->p_id) ; $j++)
            {
                if(isset($request->hasUnit))
                {
                    if($request->hasUnit[$j] == null)
                    {
                        if(isset($request->pid))
                        {
                            for ($i=0; $i <count($request->pid) ; $i++) {
                                $units = Unit::where('id',$request->uid[$i])->first();
                                if($units->u_name == 'Liter' || $units->u_name=='Kilograms' || $units->u_name == 'Mililiter' || $units->u_name=='Grams')
                                {
                                    $status = true;
                                    break;
                                }
                                else {
                                    $status = false;
                                }
                            }
                        }
                        else {
                            $status = true;
                        }
                        if($status == true && (!isset($request->size) || !isset($request->quantity2)))
                        {
                            toastr()->error('Add size and quantity of receiving');
                            return redirect()->back();
                        }
                    }
                }
            }
        }

        $u_id = Auth::user()->id;
        $date=Carbon::now()->format('Y-m-d');
        // addPurchaseQty($request->all());
        //for Inventory accounts
        $product = [];
        $account_i = [];
        $supplier_d = 0 ;
        $supplier_c = 0 ;
        $supplier_n = 0 ;
        $cash_d = 0 ;
        $cash_c = 0 ;
        $cash_n = 0 ;
        $bank_c = 0;
        $bank_d = 0;
        $bank_n = 0;
        $purchase_d = 0 ;
        $purchase_c = 0 ;
        $purchase_n = 0 ;
        $Grn_no = rand();
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');

        $po = PurchaseOrder::where('id',$request->pu_id)
        ->first();

        $gl = GeneralLedger::max('id');
        if($gl == null)
        {
            $link_id = 1;
        }
        else
        {
            $ledger1 = GeneralLedger::where('id',$gl)->first();
            $link_id = $ledger1->link_id + 1;
        }

        $supplier=Vendors::find($po->s_id);
        $account_s = AccountDetails::where('name_of_account',$supplier->name.' - '.$supplier->company)
        ->first();
        $payable = GeneralLedger::where('account_code',$account_s->Code)
        ->get();

        $account_c = AccountDetails::where('name_of_account','Cash In Hand')->first();
        $cash = GeneralLedger::where('account_code',$account_c->Code)
        ->get();

         //for purchase account
         $account_p = AccountDetails::where('name_of_account','Purchase')->first();
         $purchase = GeneralLedger::where('account_code',$account_p->Code)
         ->get();

        if($request->b_id != null)
        {
            $bank=Bank::find($request->b_id);
            $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
            $bank_l = GeneralLedger::where('account_code',$account_b->Code)
            ->get();
        }

        for ($j=0; $j < count($request->p_id) ; $j++) {
            if($request->type[$j] == 0)
            {
                $product[] = Products::find($request->p_id[$j]);
            }
            else
            {
                $product[] = ProductVariants::find($request->p_id[$j]);
            }
        }
        for ($i=0; $i < count($request->p_id) ; $i++){
            if($request->type[$i] == 0)
            {
                $account_i[] = AccountDetails::where('name_of_account',$product[$i]->pro_code.' - '.$product[$i]->pro_name)
                ->first();
            }
            else
            {

                $account_i[] = AccountDetails::where('name_of_account',$product[$i]->name)
                ->first();
            }
        }
        //entry in purchase ledger
        if($purchase->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Purchase from '.$supplier->name,
                'account_name' => $account_p->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_p->Code,
                'transaction_no' => $Grn_no,
                'currency_code' => 'PKR',
                'debit' => $request->purchase,
                'credit' => '0',
                'net_value' => $request->purchase,
                'balance' => $request->purchase
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($purchase as $key => $d) {
                $purchase_d+=$d->debit;
                $purchase_c+=$d->credit;
                $balance+=$d->net_value;
            }
            $purchase_d += $request->purchase;
            // $purchase_n = $purchase_d - $purchase_c;
            $purchase_n = $balance + ($request->purchase - 0);
            $dfdfd = GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Purchase from '.$supplier->name,
                'account_name' => $account_p->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_p->Code,
                'transaction_no' => $Grn_no,
                'currency_code' => 'PKR',
                'debit' => $request->purchase,
                'credit' => '0',
                'net_value' => $request->purchase,
                'balance' => $purchase_n
            ]);
        }

        for ($i=0; $i <count($request->p_id) ; $i++) {
            if($request->received_quantity[$i] == 0)
            {

            }
            else
            {

                Stocks::create([
                    'p_id' => $request->p_id[$i],
                    'quantity' => $request->received_quantity[$i],
                    'stock_date' => $date,
                    's_id' => $request->s_id,
                    'purchase_d_id' => $request->id[$i],
                    'cost' => $request->cost[$i],
                    'created_by' => $u_id,
                    'w_id' => $request->w_id,
                    'grn_no' => $Grn_no,
                    'type' =>  $request->type[$i],
                ]);
                if ($request->type[$i] == 0)
                {
                    $product = Products::with('unit')
                    ->where('id',$request->p_id[$i])
                    ->first();
                    $cs=CurrentStock::where('p_id',$request->p_id[$i])
                    ->where('w_id',$request->w_id)
                    ->where('type',$request->type[$i])
                    ->first();
                    if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                    {
                        $unit_quantity = 1000 * $request->received_quantity[$i];
                    }
                    else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                    {
                        $unit_quantity = $request->received_quantity[$i];
                    }
                    else
                    {
                        $unit_quantity = null;
                    }
                    // dd($unit_quantity);
                    if($cs == null)
                    {
                        CurrentStock::create([
                            'p_id' => $request->p_id[$i],
                            'w_id' => $request->w_id,
                            'quantity' => $request->received_quantity[$i],
                            'unit_quantity' => $unit_quantity,
                            'type' => $request->type[$i],
                        ]);
                    }

                    else
                    {
                        $quan = $cs->quantity + $request->received_quantity[$i];
                        if($cs->unit_quantity == null)//changes
                        {
                            $u_quan=null;
                        }
                        else if($cs->unit_quantity == 0)
                        {
                            $u_quan = $cs->unit_quantity + $unit_quantity;
                        }
                        else
                        {
                            $u_quan = $cs->unit_quantity + $unit_quantity;
                        }
                        CurrentStock::where('p_id',$request->p_id[$i])
                        ->where('w_id',$request->w_id)
                        ->where('type',$request->type[$i])
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                    }

                }
                else
                {
                    $cs=CurrentStock::where('p_id',$request->p_id[$i])
                    ->where('w_id',$request->w_id)
                    ->where('type',$request->type[$i])
                    ->first();


                    $products = ProductVariants::with('product.unit')
                    ->where('id',$request->p_id[$i])
                    ->first();
                    if($products->product->unit->u_name == 'Liter' || $products->product->unit->u_name == 'Kilograms')
                    {
                        $unit_quantity =  1000 * $request->received_quantity[$i];
                    }
                    else if($products->product->unit->u_name == 'Mililiter'  || $products->product->unit->u_name == 'Grams')
                    {
                        $unit_quantity = $request->received_quantity[$i];
                    }
                    else
                    {
                        $unit_quantity = null;
                    }
                    if($cs == null)
                    {
                        CurrentStock::create([
                            'p_id' => $request->p_id[$i],
                            'w_id' => $request->w_id,
                            'quantity' => $request->received_quantity[$i],
                            'unit_quantity' => $unit_quantity,
                            'type' => $request->type[$i],
                        ]);
                    }

                    else
                    {
                        $quan = $cs->quantity + $request->received_quantity[$i];
                        if($cs->unit_quantity == null)//changes
                        {
                            $u_quan=null;
                        }
                        else if($cs->unit_quantity == 0)
                        {
                            $u_quan = $cs->unit_quantity + $unit_quantity;
                        }
                        else
                        {
                            $u_quan = $cs->unit_quantity + $unit_quantity;
                        }
                        CurrentStock::where('p_id',$request->p_id[$i])
                        ->where('w_id',$request->w_id)
                        ->where('type',$request->type[$i])
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                    }
                }

            }
        }
        // dd($unit_quantity);

        $status='';
        for ($j=0; $j < count($request->p_id) ; $j++) {

            $pd=PurchaseOrderDetails::find($request->id[$j]);
            if($pd->received_quantity == 0)
            {
                PurchaseOrderDetails::where('id',$request->id[$j])
                ->update([
                    'received_quantity' => $request->received_quantity[$j]
                ]);
                if($request->quantity[$j] == $request->received_quantity[$j])
                {
                    $status='Received';
                }
                else
                {
                    $status='Partial';
                }
            }
            else
            {
                $received_quantity = $pd->received_quantity + $request->received_quantity[$j];
                PurchaseOrderDetails::where('id',$request->id[$j])
                ->update([
                    'received_quantity' => $received_quantity,
                ]);
                if($request->quantity[$j] == $received_quantity)
                {
                    $status='Received';
                }
                else
                {
                    $status='Partial';
                }
            }
        }

        if(isset($request->pid))
        {

            for ($i=0; $i <count($request->pid) ; $i++) {
                $units = Unit::where('id',$request->uid[$i])->first();
                $check = StockReceiving::where('p_id',$request->pid[$i])
                ->where('size',$request->size[$i])
                ->where('u_id',$request->uid[$i])
                ->where('type',$request->type1[$i])
                ->where('w_id',$request->w_id)->first();
                if($check == null)
                {
                    if($units->u_name == 'Liter' || $units->u_name=='Kilograms')
                    {
                        $totalqty = $request->quantity2[$i] * 1000;
                    }
                    if($units->u_name == 'Mililiter' || $units->u_name=='Grams')
                    {
                        $totalqty  = $request->quantity2[$i];
                    }
                    StockReceiving::create([
                        'p_id' => $request->pid[$i],
                        'quantity' => $request->quantity2[$i],
                        'size' => $request->size[$i],
                        'u_id' => $request->uid[$i],
                        'type' =>  $request->type1[$i],
                        'w_id' => $request->w_id,
                        'total' => $totalqty
                    ]);
                }
                else {
                    if($units->u_name == 'Liter' || $units->u_name=='Kilograms')
                    {
                        $totalqty = $request->quantity2[$i] * 1000;
                    }
                    if($units->u_name == 'Mililiter' || $units->u_name=='Grams')
                    {
                        $totalqty  = $request->quantity2[$i];
                    }
                    $c_qty = $check->quantity + $request->quantity2[$i];
                    $qty = $check->total + $totalqty;
                    StockReceiving::where('p_id',$request->pid[$i])
                    ->where('size',$request->size[$i])
                    ->where('u_id',$request->uid[$i])
                    ->where('type',$request->type1[$i])
                    ->where('w_id',$request->w_id)
                    ->update([
                        'quantity' => $c_qty,
                        'total' => $qty,
                    ]);
                }
            }
        }

        $p_status='';

        if($request->paid_by == null)
        {
            $pos = PurchaseOrder::where('id',$request->pu_id)
            ->first();
            $p_status = $pos->p_status;
            if($payable->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Purchase from '.$supplier->name,
                    'account_name' => $account_s->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_s->Code,
                    'transaction_no' => $Grn_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $request->purchase,
                    'net_value' => 0 - $request->purchase,
                    'balance' => 0 - $request->purchase
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($payable as $key => $c) {
                    $supplier_c+=$c->credit;
                    $supplier_d+=$c->debit;
                    $balance+=$c->net_value;
                }
                $supplier_c += $request->purchase;
                // $supplier_n = $supplier_d - $supplier_c;
                $supplier_n = $balance + (0 - $request->purchase);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Purchase from '.$supplier->name,
                    'account_name' => $account_s->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_s->Code,
                    'transaction_no' => $Grn_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $request->purchase,
                    'net_value' => 0 - $request->purchase,
                    'balance' => $supplier_n
                ]);
            }
        }
        else
        {
            TransactionHistory::create([
                'p_s_id' => $request->p_s_id,
                'p_type' => $request->p_type,
                't_type' => $request->t_type,
                'paid_by' => $request->paid_by,
                // 'ref_no' => $request->p_ref_no,
                'total' => $request->p_total,
                'cheque_no' => $request->cheque_no,
                'cc_no' => $request->cc_no,
                'gift_no' => $request->gift_no,
                'cc_holder' => $request->cc_holder,
                'note' => $request->editor2,
                'b_id' => $request->b_id,
                'created_by' => $u_id
            ]);
            $ref_no = TransactionHistory::max('id');
            if($request->p_total < $request->purchase)
            {
                // dd('less');
                $amount = $request->purchase - $request->p_total;

                if($payable->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Purchase from '.$supplier->name,
                        'account_name' => $account_s->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_s->Code,
                        'transaction_no' => $Grn_no,
                        'currency_code' => 'PKR',
                        'credit' => '0',
                        'debit' => $amount,
                        'net_value' => $amount,
                        'balance' => $amount
                    ]);
                }
                else
                {
                    $balance = 0 ;
                    foreach ($payable as $key => $c) {
                        $supplier_c+=$c->credit;
                        $supplier_d+=$c->debit;
                        $balance+=$c->net_value;
                    }


                    $supplier_c += $amount;
                    // $supplier_n = $supplier_d - $supplier_c;
                    $supplier_n = $balance + ( $amount - 0);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Purchase from '.$supplier->name,
                        'account_name' => $account_s->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_s->Code,
                        'transaction_no' => $Grn_no,
                        'currency_code' => 'PKR',
                        'debit' => $amount,
                        'credit' => '0',
                        'net_value' => $amount,
                        'balance' => $supplier_n
                    ]);
                }

            }
            if($request->p_total == $request->purchase)
            {
                // dd('equal');
                $amount = $request->p_total;

            }


            if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
            {
                if($cash->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$supplier->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($cash as $key => $c) {
                        $cash_c+=$c->credit;
                        $cash_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $cash_c += $request->p_total;
                    // $cash_n = $cash_d - $cash_c;
                    $cash_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$supplier->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' => $cash_n
                    ]);
                }
            }
            else
            {
                if($bank_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$supplier->name,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_b->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($bank_l as $key => $c) {
                        $bank_c+=$c->credit;
                        $bank_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $bank_c += $request->p_total;
                    // $bank_n = $bank_d - $bank_c;
                    $bank_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$supplier->name,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_b->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance'  => $bank_n
                    ]);
                }
            }


            if($request->p_total == $request->actual)
            {
                $p_status='Paid';
            }
            if($request->p_total < $request->actual)
            {
                $p_status='Partial';
            }
        }


        //entry in Inventory ledger
        for ($k=0; $k < count($account_i) ; $k++) {
            if($request->received_quantity[$k] == 0)
            {

            }
            else
            {
                $sumcost=0;
                $sumqty = 0;
                $avg = 0;
                $debit_pro = GeneralLedger::where('account_code',$account_i[$k]->Code)
                ->get();
                if($debit_pro->isEmpty())
                {
                    $sumcost = $request->cost[$k];
                    $sumqty = $request->received_quantity[$k];
                    $avg = $request->cost[$k];
                    AvgCost::create([
                        'p_id' => $request->p_id[$k],
                        'po_id' => $po->id,
                        'cost' => $request->cost[$k],
                        'type' => $request->type[$k],
                    ]);
                    if($request->type[$k] == 0)
                    {
                        $pr = Products::find($request->p_id[$k]);
                        $pr->update([
                            'cost' => $avg
                        ]);
                    }
                    else
                    {
                        $pr = ProductVariants::find($request->p_id[$k]);
                        $pr->update([
                            'cost' => $avg
                        ]);
                    }
                }
                else {
                    $sumcost = $request->cost[$k] * $request->received_quantity[$k];
                    foreach ($debit_pro as $key => $c) {
                        $sumcost+=($c->amount * $c->stock_in);
                        $sumqty+= $c->stock_in;
                    }

                    $avg = $sumcost / $sumqty;
                    AvgCost::create([
                        'p_id' => $request->p_id[$k],
                        'po_id' => $po->id,
                        'cost' => $avg,
                        'type' => $request->type[$k],
                    ]);
                    if($request->type[$k] == 0)
                    {
                        $pr = Products::find($request->p_id[$k]);
                        $pr->update([
                            'cost' => $avg
                        ]);
                    }
                    else
                    {
                        $pr = ProductVariants::find($request->p_id[$k]);
                        $pr->update([
                            'cost' => $avg
                        ]);
                    }
                }

                $balance = 0;
                $net_d_p = 0;
                $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                ->where('w_id',$po->w_id)
                ->get();
                if($debit_p->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Purchase product for purchase order number: '.$po->ref_no,
                        'account_name' => $account_i[$k]->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_i[$k]->Code,
                        'transaction_no' => $Grn_no,
                        'currency_code' => 'PKR',
                        'stock_in' => $request->received_quantity[$k],
                        'stock_out' => '0',
                        'net_value' => $request->received_quantity[$k],
                        'balance' => $request->received_quantity[$k],
                        'debit' => ($avg * $request->received_quantity[$k]),
                        'credit' => 0,
                        'w_id' => $po->w_id,
                        'type' => $request->type[$k],
                        'amount' => ($avg)
                    ]);
                }
                else
                {
                    foreach ($debit_p as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $net_d_p = $balance + ( $request->received_quantity[$k] - 0);
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Purchase product for purchase order number: '.$po->ref_no,
                        'account_name' => $account_i[$k]->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_i[$k]->Code,
                        'transaction_no' => $Grn_no,
                        'currency_code' => 'PKR',
                        'stock_in' => $request->received_quantity[$k],
                        'stock_out' => '0',
                        'net_value' => $request->received_quantity[$k],
                        'balance' => $net_d_p,
                        'debit' => ($avg * $request->received_quantity[$k]),
                        'credit' => 0,
                        'w_id' => $po->w_id,
                        'type' => $request->type[$k],
                        'amount' => ($avg)
                    ]);
                }
            }
        }
        PurchaseOrder::where('id',$request->pu_id)
        ->update([
            'status' => $status,
            'p_status' => $p_status,
            'updated_by' => $u_id
        ]);
        // return $this->invoice($request->pu_id);
        return $this->GRR($Grn_no,$request->pu_id);
    }



    public function invoice($id)
    {
        $th=TransactionHistory::with('purchase.warehouse','purchase.supplier','purchase.biller')->find($id);
        $pdf = PDF::loadView('porder.invoice', compact('th'));
        // return view('porder.invoice', compact('purchase','pdetail','stock','th'));

        return $pdf->download('paymentInvoice.pdf');
    }

    public function update(Request $request,$id)
    {
        // dd($request->all());
        if($request->final == 'NaN')
        {
            toastr()->error('Network Error!');
            return redirect()->bak();
        }
        else
        {
            $u_id = Auth::user()->id;
            PurchaseOrder::where('id',$id)
            ->update([
                'order_date' => $request->order_date,
                'ref_no' => $request->ref_no,
                'w_id' => $request->w_id,
                's_id' => $request->s_id,
                'b_id' => $request->b_id,
                'note' => $request->editor1,
                'total' => $request->final,
                'updated_by' => $u_id,
                'tax_status' => $request->tax_status,
                'tax' => $request->tax,
                'exp_rcv_date' => $request->exp_rcv_date,
                'payment_mode' => $request->payment_mode,
                'days' => $request->days,
                'pdate' => $request->pdate,
            ]);
            DB::table('purchase_order_details')->where('o_id', $id)->delete();
            for ($i=1; $i <= count($request->p_id) ; $i++) {
                PurchaseOrderDetails::create([
                    'o_id' => $id,
                    'p_id' => $request->p_id[$i],
                    'quantity' => $request->quantity[$i],
                    'cost' => $request->cost[$i],
                    'sub_total' => $request->sub_total[$i],
                    'type' => $request->type[$i],
                ]);
            }

            toastr()->success('Purchase Order updated successfully!');
            // Session::flash('download', '/purchase/pdf');
            return redirect(url('').'/purchase');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new PurchaseExport, 'PurchaseOrders.xlsx');
    }

    public function purchaseOSExcel($s_id,$status,$check)
    {
        return Excel::download(new PurchaseStatusExport($s_id,$status,$check), 'PurchaseOrdersStatus.xlsx');
    }
    public function purchaseMonthExcel($month,$s_id,$status,$check)
    {
        return Excel::download(new PurchaseMonthlyExport($month,$s_id,$status,$check), 'PurchaseOrdersMonthly.xlsx');
    }
    public function purchaseYearExcel($year,$s_id,$status,$check)
    {
        return Excel::download(new PurchaseYearlyExport($year,$s_id,$status,$check), 'PurchaseOrdersYearly.xlsx');
    }
    public function purchaseDateExcel($date,$s_id,$status,$check)
    {
        return Excel::download(new PurchaseDateExport($date,$s_id,$status,$check), 'PurchaseOrdersDate.xlsx');
    }

    public function purchaseDatesExcel($from,$to,$s_id,$status,$check)
    {
        return Excel::download(new PurchaseDateDifferenceExport($from,$to,$s_id,$status,$check), 'PurchaseOrdersDateDifference.xlsx');
    }

    public function purchaseExcel($index,$s_id,$status,$check)
    {
        return Excel::download(new PurchaseDateConditionsExport($index,$s_id,$status,$check), 'PurchaseOrders.xlsx');
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        $bank=Bank::where('status',1)->get();
        $adminName = Auth::user()->name;
        if($request->optradio == 'Year')
        {
            $index = 1;
            $year = $request->year;
            if($request->status == null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y'))"),$request->year)
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $status = 0;
                $s_id = 0;
            }
            if($request->status != null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y'))"),$request->year)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $status = $request->status;
                $s_id = 0;
            }
            if($request->status == null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y'))"),$request->year)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 2;
                $status = 0;
                $s_id = $request->s_id;
            }
            if($request->status != null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y'))"),$request->year)
                ->where('s_id',$request->s_id)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 3;
                $status = $request->status;
                $s_id = 0;
            }
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','year','s_id','status','check'));
        }
        else if($request->optradio == 'Month')
        {
            $index = 2;
            $month = $request->month;
            if($request->status == null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$request->month)
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $status = 0;
                $s_id = 0;
            }
            if($request->status != null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$request->month)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $status = $request->status;
                $s_id = 0;
            }
            if($request->status == null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$request->month)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 2;
                $status = 0;
                $s_id = $request->s_id;
            }
            if($request->status != null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$request->month)
                ->where('status',$request->status)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 3;
                $status = $request->status;
                $s_id = $request->s_id;
            }
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','month','check','status','s_id'));
        }
        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $index = 3;
                $from = $request->from;
                $to = $request->to;
                if($request->status == null && $request->s_id == null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 0;
                    $status = 0;
                    $s_id = 0;

                }
                if($request->status != null && $request->s_id == null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('status',$request->status)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 1;
                    $status = $request->status;
                    $s_id = 0;
                }
                if($request->status == null && $request->s_id != null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_id',$request->s_id)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 2;
                    $status = 0;
                    $s_id = $request->s_id;
                }
                if($request->status != null && $request->s_id != null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('status',$request->status)
                    ->where('s_id',$request->s_id)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 3;
                    $status = $request->status;
                    $s_id = $request->s_id;
                }
                return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','from','to','check','status','s_id'));
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }

                if($request->status == null && $request->s_id == null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),$date)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 0;
                    $status = 0;
                    $s_id = 0;
                }
                if($request->status != null && $request->s_id == null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),$date)
                    ->where('status',$request->status)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 1;
                    $status = $request->status;
                    $s_id = 0;
                }
                if($request->status == null && $request->s_id != null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),$date)
                    ->where('s_id',$request->s_id)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 2;
                    $status = 0;
                    $s_id = $request->s_id;
                }
                if($request->status != null && $request->s_id != null)
                {
                    $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                    }])
                    ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m-%d'))"),$date)
                    ->where('status',$request->status)
                    ->where('s_id',$request->s_id)
                    ->orderBy('order_date','desc')
                    ->get();
                    $check = 3;
                    $status = $request->status;
                    $s_id = $request->s_id;
                }
                return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','date','check','s_id','status'));
            }

        }
        else if($request->optradio == 'Status')
        {
            $index = 5;
            if($request->s_id == null)
            {
                $status = $request->status;
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $s_id = 0;
            }
            if($request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('status',$request->status)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $s_id = $request->s_id;
            }
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','check','s_id','status'));
        }
        else if($request->optradio == 'last24Hours')
        {
            if($request->status == null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $status = 0;
                $s_id = 0;
            }
            if($request->status != null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $status = $request->status;
                $s_id = 0;
            }
            if($request->status == null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 2;
                $status = 0;
                $s_id = $request->s_id;
            }
            if($request->status != null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->where('s_id',$request->s_id)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 3;
                $status = $request->status;
                $s_id = 0;
            }
            $index = 6;
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','s_id','status','check'));
        }
        else if($request->optradio == 'lastweek')
        {
            $date = Carbon::today()->subDays(7);
            if($request->status == null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $status = 0;
                $s_id = 0;
            }
            if($request->status != null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $status = $request->status;
                $s_id = 0;
            }
            if($request->status == null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 2;
                $status = 0;
                $s_id = $request->s_id;
            }
            if($request->status != null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->where('s_id',$request->s_id)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 3;
                $status = $request->status;
                $s_id = 0;
            }
            $index = 7;
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','s_id','status','check'));
        }
        else if($request->optradio == 'last15Days')
        {
            $date = Carbon::today()->subDays(15);
            if($request->status == null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $status = 0;
                $s_id = 0;
            }
            if($request->status != null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $status = $request->status;
                $s_id = 0;
            }
            if($request->status == null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 2;
                $status = 0;
                $s_id = $request->s_id;
            }
            if($request->status != null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where('order_date','>=',$date)
                ->where('s_id',$request->s_id)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 3;
                $status = $request->status;
                $s_id = 0;
            }
            $index = 8;
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','s_id','status','check'));
        }
        else if($request->optradio == 'lastMonth')
        {
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            if($request->status == null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$month)
                ->orderBy('order_date','desc')
                ->get();
                $check = 0;
                $status = 0;
                $s_id = 0;
            }
            if($request->status != null && $request->s_id == null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$month)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 1;
                $status = $request->status;
                $s_id = 0;
            }
            if($request->status == null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$month)
                ->where('s_id',$request->s_id)
                ->orderBy('order_date','desc')
                ->get();
                $check = 2;
                $status = 0;
                $s_id = $request->s_id;
            }
            if($request->status != null && $request->s_id != null)
            {
                $porder=PurchaseOrder::with(['warehouse','supplier','orderdetails'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
                }])
                ->where(DB::raw("(DATE_FORMAT(order_date,'%Y-%m'))"),$month)
                ->where('s_id',$request->s_id)
                ->where('status',$request->status)
                ->orderBy('order_date','desc')
                ->get();
                $check = 3;
                $status = $request->status;
                $s_id = 0;
            }
            $index = 9;
            return view('porder.index',compact('adminName','supplier','permissions','bank','menu_id','porder','index','s_id','status','check'));
        }

    }

    public function editInvoice($id)
    {
        $purchase=PurchaseOrder::with(['warehouse','supplier','biller'])
        ->where('id',$id)
        ->first();
        $pdetail=PurchaseOrderDetails::with(['products','products.brands','products.category','products.unit','purchase','variant.product.brands','variant.product.unit'])
        ->with(['products.currentstocks' => function($query) use ($purchase) {
            $avg = $query->select(DB::raw('*'))->where('w_id',$purchase->w_id);
        }])
        ->with(['variant.currentstocks' => function($query) use ($purchase) {
            $avg = $query->select(DB::raw('*'))->where('w_id',$purchase->w_id);
        }])
        ->where('o_id',$id)
        ->get();
        // dd($pdetail);
        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $ware=Warehouse::all();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city=City::where('status',1);
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $data=[
            'isEdit' => true,
            'invoice' => true,
            'supplier' => $supplier,
            'biller' => $biller,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'city' => $city,
            'product' => $product,
            'id' => $id,
            'purchase' => $purchase,
            'ct' => CostType::all(),
            'pdetail' => $pdetail
        ];
        return view('porder.create',$data);
    }

    public function updateInvoice(Request $request, $id)
    {
        // dd($request->all());
        if($request->final == 'NaN')
        {
            toastr()->error('Network error!');
            return redirect()->back();
        }
        if(!(isset($request->p_id)))
        {
            toastr()->error('Select Products to create purchase order !');
            return redirect()->back();
        }
        else
        {
            $u_id = Auth::user()->id;
            if($request->doc == null)
            {
                $doc = $request->doc1;
            }
            else
            {
                $doc= Storage::disk('uploads')->putFile('',$request->doc);
            }
            $po = PurchaseOrder::find($id);


            $p_status = $po->p_status;
            $status = '';


            PurchaseOrder::where('id',$id)
            ->update([
                'order_date' => $request->order_date,
                'ref_no' => $request->ref_no,
                'w_id' => $request->w_id,
                's_id' => $request->s_id,
                'b_id' => $request->b_id,
                'note' => $request->editor1,
                'total' => $request->final,
                'doc' => $doc,
                'updated_by' => $u_id,
                'tax_status' => $request->tax_status,
                'tax' => $request->tax,
                'exp_rcv_date' => $request->exp_rcv_date,
                'payment_mode' => $request->payment_mode,
                'days' => $request->days,
                'pdate' => $request->pdate,
                'p_status' => $p_status,
            ]);


            for ($i=1; $i <= count($request->p_id) ; $i++) {
                if(isset($request->pdid[$i]))
                {
                    $sd = PurchaseOrderDetails::find($request->pdid[$i]);
                    $orgQty = $sd->quantity;
                    $dlvQty = $sd->received_quantity;
                    $sout = Stocks::where('purchase_d_id',$request->pdid[$i])->first();
                    $rqtQty = $request->quantity[$i];
                    if($rqtQty < $dlvQty)
                    {
                        $totalQty = $dlvQty - $rqtQty;
                        Stocks::where('purchase_d_id',$request->pdid[$i])
                        ->update([
                            'quantity' => $rqtQty
                        ]);
                        $prod=CurrentStock::with(['products.unit','variant.product.unit'])->where('p_id',$request->p_id[$i])
                        ->where('w_id',$request->w_id)
                        ->where('type',$request->type[$i])
                        ->first();
                        $unit_quantity = null;
                        $u_quan = null;
                        $quan = null;
                        if($request->type[$i] == 0)
                        {
                            if($prod->products->unit->u_name == 'Liter' || $prod->products->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $totalQty;
                            }
                            else if($prod->products->unit->u_name == 'Mililiter' || $prod->products->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $totalQty;

                            }
                            else
                            {
                                $unit_quantity = null;
                            }
                        }
                        else
                        {
                            if($prod->variant->product->unit->u_name == 'Liter' || $prod->variant->product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $totalQty;
                            }
                            else if($prod->variant->product->unit->u_name == 'Mililiter' || $prod->variant->product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $totalQty;

                            }
                            else
                            {
                                $unit_quantity = null;
                            }
                        }



                        if($prod->unit_quantity == null)//changes
                        {
                            $u_quan=null;
                        }
                        else
                        {
                            $u_quan = $prod->unit_quantity - $unit_quantity;
                        }
                        if($prod->quantity == null)
                        {
                            $quan = null;
                        }
                        else {
                            $quan=$prod->quantity - $totalQty;

                        }

                        CurrentStock::where('p_id',$request->p_id[$i])
                        ->where('w_id',$request->w_id)
                        ->where('type',$request->type[$i])
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);

                        PurchaseOrderDetails::where('id',$request->pdid[$i])->update([
                            'o_id' => $id,
                            'p_id' => $request->p_id[$i],
                            'quantity' => $request->quantity[$i],
                            'received_quantity' => $request->quantity[$i],
                            'cost' => $request->cost[$i],
                            'sub_total' => $request->sub_total[$i],
                            'type' => $request->type[$i],
                        ]);

                    }
                    else
                    {

                        PurchaseOrderDetails::where('id',$request->pdid[$i])->update([
                            'o_id' => $id,
                            'p_id' => $request->p_id[$i],
                            'quantity' => $request->quantity[$i],
                            'cost' => $request->cost[$i],
                            'sub_total' => $request->sub_total[$i],
                            'type' => $request->type[$i],
                        ]);
                    }

                }
                else
                {
                    $sd = PurchaseOrderDetails::create([
                        'o_id' => $id,
                        'p_id' => $request->p_id[$i],
                        'quantity' => $request->quantity[$i],
                        'cost' => $request->cost[$i],
                        'sub_total' => $request->sub_total[$i],
                        'type' => $request->type[$i],
                    ]);
                }
            }



            $dlvOQty = PurchaseOrderDetails::where('o_id',$id)->sum('received_quantity');
            $ordOQty = PurchaseOrderDetails::where('o_id',$id)->sum('quantity');
            if($po->status == 'Pending')
            {
                $status = 'Pending';
            }
            else
            {
                if($dlvOQty == $ordOQty)
                {
                    $status = 'Received';
                }
                else
                {
                    $status = 'Partial';
                }
            }
            PurchaseOrder::where('id',$id)
            ->update([
                'status' => $status
            ]);

            toastr()->success('PO Updated successfully!');
            // Session::flash('download', '/sales/pdf');
            return redirect(url('').'/purchase');
        }
    }

}
