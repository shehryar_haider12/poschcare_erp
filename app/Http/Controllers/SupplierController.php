<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\VendorsUpdateRequest;
use App\City;
use DataTables;
use App\Exports\SupplierExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Auth;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('supplier.index',compact('permissions'));
    }
    public function datatable()
    {
        $vendor=Vendors::where('v_type','Supplier')->get();
        // dd($vendor);
        return DataTables::of($vendor)->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $city=City::where('status',1)->get();
        $data= [
            'isEdit' => false,
            'city' => $city,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('supplier.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = Vendors::where('company',$request->company)
        ->where('name',$request->name)->where('v_type','Supplier')->first();
        if($supplier == null){

            $role_id = Auth::user()->r_id;
            $env_a_id = config('app.adminId');
            $env_m_id = config('app.managerId');

            if($role_id == $env_a_id || $role_id == $env_m_id)
            {
                $status = 1;
            }
            else
            {
                $status = 0;
            }

            $u_id = Auth::user()->id;

            $hcat = HeadCategory::where('name','Payables')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;

            $data = [
                'Code' => $code,
                'name_of_account' => $request->name.' - '.$request->company,
                'c_id' => $hcat->id,
                'created_by' => $u_id
            ];
            AccountDetails::create($data);

            $supplier = Vendors::create([
                'company' => $request->company,
                'name' => $request->name,
                'address' => $request->address,
                'c_no' => $request->c_no,
                'country' => $request->country,
                'VAT' => $request->VAT,
                'GST' => $request->GST,
                'state' => $request->state,
                'email' => $request->email,
                'postalCode' => $request->postalCode,
                'c_id' => $request->c_id,
                'v_type' => $request->v_type,
                'created_by' => $u_id,
                'status' => $status
            ]);
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Supplier added successfully!';
                $response['supplier'] = $supplier;
                return response()->json($response, 200);
            }
            toastr()->success('Supplier added successfully!');
            return redirect()->back();
        }
        else {
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Supplier account already exist!';
                $supplier = null;
                return response()->json($response, 200);
            }
            toastr()->error('Supplier account already exist!');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=Vendors::with(['city'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor=Vendors::with(['city'])
        ->where('id',$id)
        ->first();
        $city=City::where('status',1)->get();
        $data= [
            'isEdit' => true,
            'city' => $city,
            'vendor' => $vendor
        ];
        return view('supplier.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Vendors::find($id);
        $account = $supplier->name. ' - '. $supplier->company;
        $ledger = GeneralLedger::where('account_name',$account)
        ->first();


        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            // if($request->name != $supplier->name || $request->company != $supplier->company)
            // {
                if($ledger == null)
                {
                    AccountDetails::where('name_of_account',$account)
                    ->update([
                        'name_of_account' => $request->name.' - '.$request->company
                    ]);
                    $vendor=Vendors::where('id',$id)
                    ->update([
                        'company' => $request->company,
                        'name' => $request->name,
                        'address' => $request->address,
                        'c_no' => $request->c_no,
                        'country' => $request->country,
                        'VAT' => $request->VAT,
                        'GST' => $request->GST,
                        'state' => $request->state,
                        'email' => $request->email,
                        'postalCode' => $request->postalCode,
                        'c_id' => $request->c_id,
                        'updated_by' => $u_id
                    ]);
                    toastr()->success('Supplier updated successfully!');
                    return redirect(url('').'/supplier');
                }
                else
                {
                    $vendor=Vendors::where('id',$id)
                    ->update([
                        'address' => $request->address,
                        'c_no' => $request->c_no,
                        'country' => $request->country,
                        'VAT' => $request->VAT,
                        'GST' => $request->GST,
                        'state' => $request->state,
                        'email' => $request->email,
                        'postalCode' => $request->postalCode,
                        'c_id' => $request->c_id,
                        'updated_by' => $u_id
                    ]);
                    toastr()->success('Supplier updated successfully!');
                    return redirect(url('').'/supplier');
                }
            // }
        }
        else
        {
            // if($request->name != $supplier->name || $request->company != $supplier->company)
            // {
                if($ledger == null)
                {


                    $vendor=VendorsUpdateRequest::create([
                        'company' => $request->company,
                        'name' => $request->name,
                        'address' => $request->address,
                        'c_no' => $request->c_no,
                        'country' => $request->country,
                        'VAT' => $request->VAT,
                        'GST' => $request->GST,
                        'state' => $request->state,
                        'email' => $request->email,
                        'postalCode' => $request->postalCode,
                        'c_id' => $request->c_id,
                        'id' => $id
                    ]);
                    toastr()->success('Supplier update request sent successfully successfully!');
                    return redirect(url('').'/supplier');
                }
                else
                {
                    $vendor=VendorsUpdateRequest::create([
                        'address' => $request->address,
                        'c_no' => $request->c_no,
                        'country' => $request->country,
                        'VAT' => $request->VAT,
                        'GST' => $request->GST,
                        'state' => $request->state,
                        'email' => $request->email,
                        'postalCode' => $request->postalCode,
                        'c_id' => $request->c_id,
                        'id' => $id
                    ]);
                    toastr()->success('Supplier update request sent successfully successfully!');
                    return redirect(url('').'/supplier');
                }
            // }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new SupplierExport, 'suppliers.xlsx');
    }

    public function pdf()
    {
        $vendor =  Vendors::where('v_type','Supplier')
        ->select('name','company','address','c_no')
        ->get();
        // dd($vendor);
        $pdf = PDF::loadView('supplier.pdf', compact('vendor'));

        return $pdf->download('suppliers.pdf');
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Vendors::find($id);
        if ($item->update(['status' => $status])) {
            Vendors::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
