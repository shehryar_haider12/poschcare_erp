<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JournalVoucher;
use App\JournalVoucherDetails;
use App\AccountDetails;
use App\HeadCategory;
use App\GeneralLedger;
use App\User;
use App\PurchaseOrder;
use App\Sales;
use App\Bank;
use App\Vendors;
use App\TransactionHistory;
use DataTables;
use Auth;
use DB;
use PDF;
use Response;
use Storage;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;
use App\Exports\JournalVoucherAmountExport;
use App\Exports\JournalVoucherDescExport;
use App\Exports\JournalVoucherVnoExport;
use App\Exports\JournalVoucherExport;
use App\Exports\JournalVoucherYearExport;
use App\Exports\JournalVoucherMonthExport;
use App\Exports\JournalVoucherDateExport;
use App\Exports\JournalVoucherDateConditionsExport;
use App\Exports\JournalVoucherDateDifferenceExport;
use App\Exports\JournalVoucherAccountExport;
use App\Exports\VoucherExportStructure;
use Maatwebsite\Excel\Facades\Excel;


class JournalVoucherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menu_id     =   getMenuId($request);
        $permissions =   getRolePermission($menu_id);
        $account     =   AccountDetails::where('Code','NOT LIKE','CA-02%')->get();
        $voucher     =   JournalVoucher::with(['acode1','acode2'])->get();
        $index       =   0;
        // return $voucher;
        return view('vouchers.index',compact('voucher','permissions','account','index','menu_id'));
    }

    public function biller($id)
    {
        $bid = substr($id,'0',strpos($id,'-'));
        $vtype = substr($id,strpos($id,'-')+1);
        $biller = Vendors::find($bid);
        $name = $biller->abr.'-'.$vtype.'-00';

        $v_no = 0;
        $id = JournalVoucher::where('b_id',$bid)->max('id');
        if($id == null)
        {
            $v_no = 1;
            return [$v_no,$name];
        }
        else
        {
            $vno = JournalVoucher::where('id',$id)->pluck('vcno');
            if($vno[0] == '')
            {
                $v_no = 1;
                return [$v_no,$name];
            }
            else {
                $v_no = $vno[0]+1;
                return [$v_no,$name];
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $voucher=JournalVoucher::max('id');
        if($voucher == null)
        {
            $id=1;
        }
        else
        {
            $id=$voucher+1;
        }
        $data =[
            'account' => AccountDetails::all(),
            'head'    => HeadCategory::all(),
            'isEdit'  => false,
            'id'      => $id,
            'cby'     => Auth::user()->name,
            'date'    => Carbon::now()->format("Y-m-d"),
            'biller'  => Vendors::where('v_type','Biller')->where('status',1)->get(),
            'bank'    => Bank::where('status',1)->get(),
        ];

        return view('vouchers.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $u_id = Auth::user()->id;
        $month = Carbon::parse($request->date)->format('M-y');
        $image = null;
        if(isset($request->attachment))
        {
            $image = Storage::disk('uploads')->putFile('',$request->attachment);
        }
        if($request->v_type == 'CBD')
        {
            $v = JournalVoucher::create([
                'date' => $request->date,
                'account1' => $request->account1cbd,
                'account2' => $request->account2cbd,
                'amount' => $request->amountcbd,
                'description' => $request->descriptioncbd,
                'month' => $month,
                'head1' => $request->head1cbd,
                'head2' => $request->head2cbd,
                'b_id' => $request->b_id,
                'v_no' => $request->v_no,
                'created_by' => $u_id,
                'vcno' => $request->vcno,
                'p_id' => 0,
                'v_type' => $request->v_type,
                'remarks' => $request->remarks,
                'attachment' => $image
            ]);
            if(isset($request->cbdchqno))
            {
                for ($i=0; $i <count($request->cbdchqno) ; $i++) {
                    TransactionHistory::create([
                        'p_s_id' => $v->id,
                        'p_type' => 'Voucher',
                        'paid_by' => 'Cheque',
                        'total' => $request->cbdchqam[$i],
                        'cheque_no' => $request->cbdchqno[$i],
                        'merchant_type' => $request->cbdchqtype[$i],
                        'title' => $request->cbdpt[$i],
                        'chq_date' => $request->cbdchqdate[$i],
                        'b_id' => $request->cbdbank[$i],
                    ]);
                }
            }
            else
            {
                TransactionHistory::create([
                    'p_s_id' => $v->id,
                    'p_type' => 'Voucher',
                    'paid_by' => 'Cash',
                    'total' => $request->amountcbd,
                ]);
            }

        }
        if($request->v_type == 'CWD')
        {
            $v = JournalVoucher::create([
                'date' => $request->date,
                'account1' => $request->account1cwd,
                'account2' => $request->account2cwd,
                'amount' => $request->amountcwd,
                'description' => $request->descriptioncwd,
                'month' => $month,
                'head1' => $request->head1cwd,
                'head2' => $request->head2cwd,
                'b_id' => $request->b_id,
                'v_no' => $request->v_no,
                'vcno' => $request->vcno,
                'created_by' => $u_id,
                'p_id' => 0,
                'v_type' => $request->v_type,
                'remarks' => $request->remarks,
                'attachment' => $image
            ]);
            if(isset($request->cwdchqno))
            {
                for ($i=0; $i <count($request->cwdchqno) ; $i++) {
                    TransactionHistory::create([
                        'p_s_id' => $v->id,
                        'p_type' => 'Voucher',
                        'paid_by' => 'Cheque',
                        'total' => $request->cwdchqam[$i],
                        'cheque_no' => $request->cwdchqno[$i],
                        'merchant_type' => $request->cwdchqtype[$i],
                        'title' => $request->cwdpt[$i],
                        'chq_date' => $request->cwdchqdate[$i],
                        'b_id' => $request->cwdbank[$i],
                    ]);
                }
            }
            else
            {
                TransactionHistory::create([
                    'p_s_id' => $v->id,
                    'p_type' => 'Voucher',
                    'paid_by' => 'Cash',
                    'total' => $request->amountcwd,
                ]);
            }
        }

        if($request->v_type == 'DV')
        {
            $v = JournalVoucher::create([
                'date' => $request->date,
                'account2' => $request->account1dv,
                // 'account2' => $request->account2dv[0],
                'amount' => $request->Amountdv,
                'description' => $request->Descriptiondv,
                'month' => $month,
                'head2' => $request->head1dv,
                // 'head2' => $request->head2dv[0],
                'b_id' => $request->b_id,
                'v_no' => $request->v_no,
                'vcno' => $request->vcno,
                'created_by' => $u_id,
                'p_id' => 0,
                'v_type' => $request->v_type,
                'remarks' => $request->remarks,
                'attachment' => $image
            ]);
            for ($i=0; $i < count($request->account2dv) ; $i++) {
                JournalVoucher::create([
                    // 'date' => $request->date,
                    // 'account2' => $request->account1dv,
                    'account1' => $request->account2dv[$i],
                    'amount' => $request->amountdv[$i],
                    'description' => $request->descriptiondv[$i],
                    // 'month' => $month,
                    'head1' => $request->head2dv[$i],
                    // 'head2' => $request->head2dv[0],
                    // 'b_id' => $request->b_id,
                    // 'v_no' => $request->v_no,
                    // 'vcno' => $request->vcno,
                    // 'created_by' => $id,
                    'p_id' => $v->id,
                    // 'v_type' => $request->v_type,
                    // 'remarks' => $request->remarks,
                    // 'attachment' => $image
                ]);
            }

            if(isset($request->dvchqno))
            {
                for ($i=0; $i <count($request->dvchqno) ; $i++) {
                    TransactionHistory::create([
                        'p_s_id' => $v->id,
                        'p_type' => 'Voucher',
                        'paid_by' => 'Cheque',
                        'total' => $request->dvchqam[$i],
                        'cheque_no' => $request->dvchqno[$i],
                        'merchant_type' => $request->dvchqtype[$i],
                        'title' => $request->dvpt[$i],
                        'chq_date' => $request->dvchqdate[$i],
                        'b_id' => $request->dvbank[$i],
                    ]);
                }
            }
            else
            {
                TransactionHistory::create([
                    'p_s_id' => $v->id,
                    'p_type' => 'Voucher',
                    'paid_by' => 'Cash',
                    'total' => $request->Amountdv,
                ]);
            }
        }
        // if(isset($request->s_id))
        // {
        //     foreach ($request->s_id as $key => $value) {
        //         JournalVoucherDetails::create([
        //             'vid' => $v->id,
        //             'spid' => $value
        //         ]);
        //     }
        // }
        // if(isset($request->p_id))
        // {
        //     foreach ($request->p_id as $key => $value) {
        //         JournalVoucherDetails::create([
        //             'vid' => $v->id,
        //             'spid' => $value
        //         ]);
        //     }
        // }
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Voucher Created successfully by '.$u_name,
            'link' => url('')."/JournalVoucher",
            'name' => 'View Journal Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));

        toastr()->success('Voucher Created successfully');
        return redirect(url('')."/JournalVoucher");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voucher = JournalVoucher::with(['acode1','acode2','biller'])->where('id',$id)->first();
        $vdetails = JournalVoucherDetails::where('vid',$id)->get();
        $others = JournalVoucher::with(['acode1','acode2'])->where('p_id',$id)->get();
        return [$voucher,$vdetails,$others];
    }

    public function pdf($id)
    {
        $exp = JournalVoucher::with(['acode1','acode2','preparedUser'])->where('id',$id)->first();
        $others = JournalVoucher::with(['acode1','acode2'])->where('p_id',$id)->get();
        $pdf = PDF::loadView('vouchers.pdf', compact('exp','others'));
        return $pdf->download('JournalVoucher.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voucher = JournalVoucher::find($id);
        $others = JournalVoucher::where('p_id',$id)->get();
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        if($voucher->head2 == 'CA-01')
        {
            $vdetails =JournalVoucherDetails::where('vid',$id)->get();
            $spid = array();
            foreach ($vdetails as $key => $o) {
                array_push($spid,$o->spid);
            }
            $account =AccountDetails::where('Code',$voucher->account2)->first();
            $company = substr($account->name_of_account,0 , strpos($account->name_of_account,'-')-1);
            $name = substr($account->name_of_account, strpos($account->name_of_account,'-')+2);
            $c_id = Vendors::where('v_type','Customer')
            ->where('name',$name)->where('company',$company)->first();
            $sales = Sales::where('c_id',$c_id->id)->where(function ($query) {
                $query->where('p_status', 'Pending')
                    ->orWhere('p_status', 'Partial');
            })->where(function ($query) {
                $query->where('s_status','Partial')
                ->orWhere('s_status','Complete')
                ->orWhere('s_status','Delivered');
            })
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->get();
            $ledger = GeneralLedger::where('account_name', $company.' - '.$name)->where('description','Opening Account')->sum('debit');
            $data =[
                'account1' => AccountDetails::where('Code','like', $voucher->head1.'%')->get(),
                'account2' => AccountDetails::where('Code','like',$voucher->head2.'%')->get(),
                'account1name' => AccountDetails::where('Code',$voucher->account1)->first(),
                'account2name' => AccountDetails::where('Code',$voucher->account2)->first(),
                'head'    => HeadCategory::all(),
                'isEdit'  => true,
                'id'      => $id,
                'cby'     => Auth::user()->name,
                'date'    => Carbon::now()->format("Y-m-d"),
                'voucher' => $voucher,
                'sales' => $sales,
                'ledger' => $ledger,
                'vdetails' => $vdetails,
                'biller' => $biller,
                'spid' => $spid,
                'bank'    => Bank::where('status',1)->get(),
            ];
        }
        if($voucher->head1 == 'CL-01')
        {
            $vdetails =JournalVoucherDetails::where('vid',$id)->get();
            $spid = array();
            foreach ($vdetails as $key => $o) {
                array_push($spid,$o->spid);
            }
            $account =AccountDetails::where('Code',$voucher->account1)->first();
            $company = substr($account->name_of_account,0 , strpos($account->name_of_account,'-')-1);
            $name = substr($account->name_of_account, strpos($account->name_of_account,'-')+2);
            $c_id = Vendors::where('v_type','Supplier')
            ->where('name',$name)->where('company',$company)->first();
            $purchase = PurchaseOrder::where('s_id',$c_id->id)->where(function ($query) {
                $query->where('p_status', 'Pending')
                    ->orWhere('status', 'Partial');
            })->where(function ($query) {
                $query->where('status','Partial')
                ->orWhere('status','Received');
            })
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }])
            ->get();
            $ledger = GeneralLedger::where('account_name', $name.' - '.$company)->where('description','Opening Account')->sum('credit');
            $data =[
                'account1' => AccountDetails::where('Code','like', $voucher->head1.'%')->get(),
                'account2' => AccountDetails::where('Code','like',$voucher->head2.'%')->get(),
                'account1name' => AccountDetails::where('Code',$voucher->account1)->first(),
                'account2name' => AccountDetails::where('Code',$voucher->account2)->first(),
                'head'    => HeadCategory::all(),
                'isEdit'  => true,
                'id'      => $id,
                'cby'     => Auth::user()->name,
                'date'    => Carbon::now()->format("Y-m-d"),
                'voucher' => $voucher,
                'purchase' => $purchase,
                'ledger' => $ledger,
                'vdetails' => $vdetails,
                'biller' => $biller,
                'spid' => $spid,
                'bank'    => Bank::where('status',1)->get(),
            ];
        }
        if($voucher->head2 != 'CA-01' && $voucher->head1 != 'CL-01') {
            if($voucher->v_type == 'DV')
            {
                $data =[
                    'account1' => [],
                    'account2' => AccountDetails::where('Code','like',$voucher->head2.'%')->get(),
                    'account1name' => '',
                    'account2name' => AccountDetails::where('Code',$voucher->account2)->first(),
                    'head'    => HeadCategory::all(),
                    'isEdit'  => true,
                    'id'      => $id,
                    'biller'  => $biller,
                    'cby'     => Auth::user()->name,
                    'date'    => Carbon::now()->format("Y-m-d"),
                    'voucher' => $voucher,
                    'others' => $others,
                    'vdetails' => JournalVoucherDetails::where('vid',$id)->get(),
                    'bank'    => Bank::where('status',1)->get(),
                    'th'      => TransactionHistory::with(['bank'])->where('p_s_id',$id)->where('p_type','Voucher')->get(),
                ];
            }
            else
            {
                $ac1 = AccountDetails::where('Code',$voucher->account1)->first();
                $data =[

                    'account1' => AccountDetails::where('Code','like', $voucher->head1.'%')->get(),
                    'account2' => AccountDetails::where('Code','like',$voucher->head2.'%')->get(),
                    'account1name' => $ac1->name_of_account ,
                    'account2name' => AccountDetails::where('Code',$voucher->account2)->first(),
                    'head'    => HeadCategory::all(),
                    'isEdit'  => true,
                    'id'      => $id,
                    'biller'  => $biller,
                    'cby'     => Auth::user()->name,
                    'date'    => Carbon::now()->format("Y-m-d"),
                    'voucher' => $voucher,
                    'others' => $others,
                    'vdetails' => JournalVoucherDetails::where('vid',$id)->get(),
                    'bank'    => Bank::where('status',1)->get(),
                    'th'      => TransactionHistory::with(['bank'])->where('p_s_id',$id)->where('p_type','Voucher')->get(),
                ];
            }
        }
        // return ($data['vdetails']);
        return view('vouchers.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $month = Carbon::parse($request->date)->format('M-y');

        if(!isset($request->attachment))
        {
            $image = $request->att1;
        }
        else
        {
            $image= Storage::disk('uploads')->putFile('',$request->attachment);
        }

        if($request->v_type == 'CBD')
        {
            $v = JournalVoucher::where('id',$id)->update([
                'date' => $request->date,
                'account1' => $request->account1cbd,
                'account2' => $request->account2cbd,
                'amount' => $request->amountcbd,
                'description' => $request->descriptioncbd,
                'month' => $month,
                'head1' => $request->head1cbd,
                'head2' => $request->head2cbd,
                'b_id' => $request->b_id,
                'v_no' => $request->v_no,
                'vcno' => $request->vcno,
                'p_id' => 0,
                'v_type' => $request->v_type,
                'remarks' => $request->remarks,
                'attachment' => $image
            ]);
            DB::table('transaction_history')->where('p_s_id', $id)->delete();
            if(isset($request->cbdchqno))
            {
                for ($i=0; $i <count($request->cbdchqno) ; $i++) {
                    TransactionHistory::create([
                        'p_s_id' => $id,
                        'p_type' => 'Voucher',
                        'paid_by' => 'Cheque',
                        'total' => $request->cbdchqam[$i],
                        'cheque_no' => $request->cbdchqno[$i],
                        'merchant_type' => $request->cbdchqtype[$i],
                        'title' => $request->cbdpt[$i],
                        'chq_date' => $request->cbdchqdate[$i],
                        'b_id' => $request->cbdbank[$i],
                    ]);
                }
            }
            else
            {
                TransactionHistory::create([
                    'p_s_id' => $v->id,
                    'p_type' => 'Voucher',
                    'paid_by' => 'Cash',
                    'total' => $request->amountcbd,
                ]);
            }
        }
        if($request->v_type == 'CWD')
        {
            $v = JournalVoucher::where('id',$id)->update([
                'date' => $request->date,
                'account1' => $request->account1cwd,
                'account2' => $request->account2cwd,
                'amount' => $request->amountcwd,
                'description' => $request->descriptioncwd,
                'month' => $month,
                'head1' => $request->head1cwd,
                'head2' => $request->head2cwd,
                'b_id' => $request->b_id,
                'v_no' => $request->v_no,
                'vcno' => $request->vcno,
                'p_id' => 0,
                'v_type' => $request->v_type,
                'remarks' => $request->remarks,
                'attachment' => $image
            ]);
            DB::table('transaction_history')->where('p_s_id', $id)->delete();
            if(isset($request->cwdchqno))
            {
                for ($i=0; $i <count($request->cwdchqno) ; $i++) {
                    TransactionHistory::create([
                        'p_s_id' => $id,
                        'p_type' => 'Voucher',
                        'paid_by' => 'Cheque',
                        'total' => $request->cwdchqam[$i],
                        'cheque_no' => $request->cwdchqno[$i],
                        'merchant_type' => $request->cwdchqtype[$i],
                        'title' => $request->cwdpt[$i],
                        'chq_date' => $request->cwdchqdate[$i],
                        'b_id' => $request->cwdbank[$i],
                    ]);
                }
            }

            else
            {
                TransactionHistory::create([
                    'p_s_id' => $v->id,
                    'p_type' => 'Voucher',
                    'paid_by' => 'Cash',
                    'total' => $request->amountcwd,
                ]);
            }
        }

        if($request->v_type == 'DV')
        {
            $v = JournalVoucher::where('id',$id)->update([
                'date' => $request->date,
                'account2' => $request->account1dv,
                // 'account2' => $request->account2dv[0],
                'amount' => $request->Amountdv,
                'description' => $request->Descriptiondv,
                'month' => $month,
                'head2' => $request->head1dv,
                // 'head2' => $request->head2dv[0],
                'b_id' => $request->b_id,
                'v_no' => $request->v_no,
                'vcno' => $request->vcno,
                // 'created_by' => $u_id,
                'p_id' => 0,
                'v_type' => $request->v_type,
                'remarks' => $request->remarks,
                'attachment' => $image
            ]);
            DB::table('journal_voucher')->where('p_id', $id)->delete();


            for ($i=0; $i < count($request->account2dv) ; $i++) {

                JournalVoucher::create([
                    // 'date' => $request->date,
                    // 'account2' => $request->account1dv,
                    'account1' => $request->account2dv[$i],
                    'amount' => $request->amountdv[$i],
                    'description' => $request->descriptiondv[$i],
                    // 'month' => $month,
                    'head1' => $request->head2dv[$i],
                    // 'head2' => $request->head2dv[0],
                    // 'b_id' => $request->b_id,
                    // 'v_no' => $request->v_no,
                    // 'vcno' => $request->vcno,
                    // 'created_by' => $id,
                    'p_id' => $id,
                    // 'v_type' => $request->v_type,
                    // 'remarks' => $request->remarks,
                    // 'attachment' => $image
                ]);
            }

            DB::table('transaction_history')->where('p_s_id', $id)->delete();
            if(isset($request->dvchqno))
            {
                for ($i=0; $i <count($request->dvchqno) ; $i++) {
                    TransactionHistory::create([
                        'p_s_id' => $id,
                        'p_type' => 'Voucher',
                        'paid_by' => 'Cheque',
                        'total' => $request->dvchqam[$i],
                        'cheque_no' => $request->dvchqno[$i],
                        'merchant_type' => $request->dvchqtype[$i],
                        'title' => $request->dvpt[$i],
                        'chq_date' => $request->dvchqdate[$i],
                        'b_id' => $request->dvbank[$i],
                    ]);
                }
            }
            else
            {
                TransactionHistory::create([
                    'p_s_id' => $id,
                    'p_type' => 'Voucher',
                    'paid_by' => 'Cash',
                    'total' => $request->Amountdv,
                ]);
            }
        }
        // if(isset($request->s_id))
        // {
        //     $vdetails =JournalVoucherDetails::where('vid',$id)->get();
        //     if($vdetails->isNotEmpty())
        //     {
        //         DB::table('journal_voucher_details')->where('vid', $id)->delete();
        //         foreach ($request->s_id as $key => $value) {
        //             JournalVoucherDetails::create([
        //                 'vid' => $id,
        //                 'spid' => $value
        //             ]);
        //         }
        //     }
        // }
        // else {
        //     $vdetails =JournalVoucherDetails::where('vid',$id)->get();
        //     if($vdetails->isNotEmpty())
        //     {
        //         DB::table('journal_voucher_details')->where('vid', $id)->delete();
        //     }
        // }
        // if(isset($request->p_id))
        // {
        //     $vdetails =JournalVoucherDetails::where('vid',$id)->get();
        //     if($vdetails->isNotEmpty())
        //     {
        //         DB::table('journal_voucher_details')->where('vid', $id)->delete();
        //         foreach ($request->p_id as $key => $value) {
        //             JournalVoucherDetails::create([
        //                 'vid' => $id,
        //                 'spid' => $value
        //             ]);
        //         }
        //     }
        // }
        // else {
        //     $vdetails =JournalVoucherDetails::where('vid',$id)->get();
        //     if($vdetails->isNotEmpty())
        //     {
        //         DB::table('journal_voucher_details')->where('vid', $id)->delete();
        //     }
        // }
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Voucher Updated by '.$u_name,
            'link' => url('')."/JournalVoucher",
            'name' => 'View Journal Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));

        toastr()->success('Voucher Updated successfully');
        return redirect(url('')."/JournalVoucher");
    }

    public function status(Request $request)
    {
        $voucher = JournalVoucher::find($request->id);
        if($request->status == 'Approved')
        {
            $gl = GeneralLedger::max('id');
            if($gl == null)
            {
                $link_id = 1;
            }
            else
            {
                $ledger = GeneralLedger::where('id',$gl)->first();
                $link_id = $ledger->link_id + 1;
            }
            $u_id = Auth::user()->id;

            $a1 =  AccountDetails::where('Code',$voucher->account1)->first();
            $a2 =  AccountDetails::where('Code',$voucher->account2)->first();
            $account1 = GeneralLedger::where('account_code',$voucher->account1)
            ->get();
            $account2 = GeneralLedger::where('account_code',$voucher->account2)
            ->get();
            if($voucher->head2 == 'CA-01')
            {
                $vdetails =JournalVoucherDetails::where('vid',$request->id)->get();
                $totalAmount = $voucher->amount;
                foreach ($vdetails as $key => $v) {
                    if($v->spid != 0)
                    {
                        $sale = Sales::where('id',$v->spid)->withCount(['transaction as total_amount' => function($query) use($v) {
                            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales')->where('p_s_id',$v->spid);
                        }])->first();
                        if($totalAmount > 0)
                        {
                            if($totalAmount >= ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount )))
                            {
                                TransactionHistory::create([
                                    'p_s_id' => $v->spid,
                                    'p_type' => 'Sales',
                                    't_type' => 'Received',
                                    'paid_by' => 'Cash',
                                    'total' => ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount )),
                                    'created_by' => $u_id
                                ]);
                                Sales::where('id',$v->spid)
                                ->update(['p_status' => 'Paid']);
                                $totalAmount -= ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount ));
                            }
                            else if($totalAmount < ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount )))
                            {
                                TransactionHistory::create([
                                    'p_s_id' => $v->spid,
                                    'p_type' => 'Sales',
                                    't_type' => 'Received',
                                    'paid_by' => 'Cash',
                                    'total' => $totalAmount,
                                    'created_by' => $u_id
                                ]);
                                Sales::where('id',$v->spid)
                                ->update(['p_status' => 'Partial']);
                                $totalAmount -= $totalAmount;
                            }

                        }
                    }
                }
            }
            if($voucher->head1 == 'CL-01'){
                $vdetails =JournalVoucherDetails::where('vid',$request->id)->get();
                $totalAmount = $voucher->amount;
                foreach ($vdetails as $key => $v) {
                    if($v->spid != 0)
                    {
                        $sale = PurchaseOrder::where('id',$v->spid)->withCount(['transaction as total_amount' => function($query) use($v) {
                            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase')->where('p_s_id',$v->spid);
                        }])->first();
                        if($totalAmount > 0)
                        {
                            if($totalAmount >= ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount )))
                            {
                                TransactionHistory::create([
                                    'p_s_id' => $v->spid,
                                    'p_type' => 'Purchase',
                                    't_type' => 'Sent',
                                    'paid_by' => 'Cash',
                                    'total' => ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount )),
                                    'created_by' => $u_id
                                ]);
                                PurchaseOrder::where('id',$v->spid)
                                ->update(['p_status' => 'Paid']);
                                $totalAmount -= ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount ));
                            }
                            else if($totalAmount < ($sale->total - ($sale->total_amount == null ? 0 : $sale->total_amount )))
                            {
                                TransactionHistory::create([
                                    'p_s_id' => $v->spid,
                                    'p_type' => 'Purchase',
                                    't_type' => 'Sent',
                                    'paid_by' => 'Cash',
                                    'total' => $totalAmount,
                                    'created_by' => $u_id
                                ]);
                                PurchaseOrder::where('id',$v->spid)
                                ->update(['p_status' => 'Partial']);
                                $totalAmount -= $totalAmount;
                            }

                        }
                    }
                }
            }


            if ($account1->isEmpty()) {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => $voucher->description,
                    'account_name' => $a1->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $voucher->date,
                    'posted_date' => $voucher->date,
                    'period' => $voucher->month,
                    'account_code' => $voucher->account1,
                    'currency_code' =>'PKR',
                    'debit' => $voucher->amount,
                    'credit' => '0',
                    'net_value' => $voucher->amount - 0,
                    'balance' => $voucher->amount - 0
                ]);
            }
            else {
                $balance = 0;
                $an=0;
                foreach ($account1 as $key => $c) {
                    $balance+=$c->net_value;
                }
                $an = $balance + ($voucher->amount - 0);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => $voucher->description,
                    'account_name' => $a1->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $voucher->date,
                    'posted_date' => $voucher->date,
                    'period' => $voucher->month,
                    'account_code' => $voucher->account1,
                    'currency_code' =>'PKR',
                    'debit' => $voucher->amount,
                    'credit' => '0',
                    'net_value' => $voucher->amount - 0,
                    'balance' => $an
                ]);
            }

            if ($account2->isEmpty()) {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => $voucher->description,
                    'account_name' => $a2->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $voucher->date,
                    'posted_date' => $voucher->date,
                    'period' => $voucher->month,
                    'account_code' => $voucher->account2,
                    'currency_code' =>'PKR',
                    'credit' => $voucher->amount,
                    'debit' => '0',
                    'net_value' => 0 - $voucher->amount,
                    'balance' => 0 - $voucher->amount
                ]);
            }
            else {
                $balance = 0;
                $an=0;
                foreach ($account2 as $key => $c) {
                    $balance+=$c->net_value;
                }
                $an = $balance + ( 0 - $voucher->amount);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => $voucher->description,
                    'account_name' => $a2->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $voucher->date,
                    'posted_date' => $voucher->date,
                    'period' => $voucher->month,
                    'account_code' => $voucher->account2,
                    'currency_code' =>'PKR',
                    'credit' => $voucher->amount,
                    'debit' => '0',
                    'net_value' => 0 - $voucher->amount,
                    'balance' => $an
                ]);
            }
        }
        $voucher->update([
            'status' => $request->status
        ]);
        $response['status'] = $request->status;
        $response['message'] = 'status updated successfully.';
        return response()->json($response, 200);
    }

    public function document($id)
    {
        $voucher = JournalVoucher::find($id);
        $file= public_path(). "/uploads/". $voucher->attachment;
        // return  Storage::download($file);
        $headers = array(
                'Content-Type: application/pdf',
                );

        return Response::download($file, $voucher->attachment, $headers);
    }

    public function excel()
    {
        return Excel::download(new JournalVoucherExport, 'JournalVoucher.xlsx');
    }

    public function JournalVoucherExcel($index)
    {
        return Excel::download(new JournalVoucherDateConditionsExport($index), 'JournalVoucher.xlsx');
    }

    public function JournalVoucherYearExcel($year,$acc,$check)
    {
        return Excel::download(new JournalVoucherYearExport($year,$acc,$check), 'JournalVoucherYearly.xlsx');
    }
    public function JournalVoucherMonthExcel($month,$acc,$check)
    {
        return Excel::download(new JournalVoucherMonthExport($month,$acc,$check), 'JournalVoucherMonthly.xlsx');
    }
    public function JournalVoucherDateExcel($date,$acc,$check)
    {
        return Excel::download(new JournalVoucherDateExport($date,$acc,$check), 'JournalVoucherDate.xlsx');
    }
    public function JournalVoucherDatesExcel($from,$to,$acc,$check)
    {
        return Excel::download(new JournalVoucherDateDifferenceExport($from,$to,$acc,$check), 'JournalVoucherDateDifference.xlsx');
    }
    public function JournalVoucherAccountExcel($acc)
    {
        return Excel::download(new JournalVoucherAccountExport($acc), 'JournalVoucherAccount.xlsx');
    }
    public function JournalVoucherAmountExcel($amount)
    {
        return Excel::download(new JournalVoucherAmountExport($amount), 'JournalVoucherAmount.xlsx');
    }
    public function JournalVoucherDescExcel($desc)
    {
        return Excel::download(new JournalVoucherDescExport($desc), 'JournalVoucherSecription.xlsx');
    }
    public function JournalVoucherVnoExcel($vno)
    {
        return Excel::download(new JournalVoucherVnoExport($vno), 'JournalVoucher.xlsx');
    }

    public function importStructure()
    {
        return Excel::download(new VoucherExportStructure, 'VoucherExportStructure.xlsx');
    }

    public function search(Request $request)
    {
        $menu_id     =   $request->menuid;
        $permissions =   getRolePermission($menu_id);
        $account     =   AccountDetails::where('Code','NOT LIKE','CA-02%')->get();
        if($request->optradio == 'Year')
        {
            $index = 1;
            $year = $request->year;
            if($request->account1 == null)
            {
                $voucher     =   JournalVoucher::with(['acode1','acode2'])
                ->where(DB::raw("(DATE_FORMAT(date,'%Y'))"),$request->year)
                ->orderBy('date','desc')
                ->get();
                $acc = 0;
                $check = 0;
            }
            else
            {
                $voucher     =   JournalVoucher::with(['acode1','acode2'])
                ->where(DB::raw("(DATE_FORMAT(date,'%Y'))"),$request->year)
                ->where('account1',$request->account1)
                ->orderBy('date','desc')
                ->get();
                $acc = $request->account1;
                $check = 1;
            }
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','year','check','acc'));
        }
        else if($request->optradio == 'Month')
        {
            $index = 2;
            $month = $request->month;
            if($request->account1 == null)
            {
                $voucher     =   JournalVoucher::with(['acode1','acode2'])
                ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m'))"),$request->month)
                ->orderBy('date','desc')
                ->get();
                $acc = 0;
                $check = 0;
            }
            else
            {
                $voucher     =   JournalVoucher::with(['acode1','acode2'])
                ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m'))"),$request->month)
                ->where('account1',$request->account1)
                ->orderBy('date','desc')
                ->get();
                $acc = $request->account1;
                $check = 1;
            }
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','month','check','acc'));
        }
        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $index = 3;
                $from = $request->from;
                $to = $request->to;
                if($request->account1 == null)
                {
                    $voucher     =   JournalVoucher::with(['acode1','acode2'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('date','desc')
                    ->get();
                    $acc = 0;
                    $check = 0;
                }
                else
                {
                    $voucher     =   JournalVoucher::with(['acode1','acode2'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('account1',$request->account1)
                    ->orderBy('date','desc')
                    ->get();
                    $acc = $request->account1;
                    $check = 1;
                }
                return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','acc','check','from','to'));
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }

                if($request->account1 == null)
                {
                    $voucher     =   JournalVoucher::with(['acode1','acode2'])
                    ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),$date)
                    ->orderBy('date','desc')
                    ->get();
                    $acc = 0;
                    $check = 0;
                }
                else
                {
                    $voucher     =   JournalVoucher::with(['acode1','acode2'])
                    ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m-%d'))"),$date)
                    ->where('account1',$request->account1)
                    ->orderBy('date','desc')
                    ->get();
                    $acc = $request->account1;
                    $check = 1;
                }
                return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','acc','check','date'));
            }
        }
        else if($request->optradio == 'Account')
        {
            $index = 5;
            $acc = $request->account1;
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('account1',$request->account1)
            ->orderBy('date','desc')
            ->get();
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','acc'));
        }
        else if($request->optradio == 'last24Hours')
        {
            $index = 6;
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('created_at', '>=', Carbon::now()->subDay())
            ->orderBy('date','desc')
            ->get();
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id'));
        }
        else if($request->optradio == 'lastweek')
        {
            $index = 7;
            $date = Carbon::today()->subDays(7);
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('date','>=',$date)
            ->orderBy('date','desc')
            ->get();
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id'));
        }
        else if($request->optradio == 'last15Days')
        {
            $index = 8;
            $date = Carbon::today()->subDays(15);
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('date','>=',$date)
            ->orderBy('date','desc')
            ->get();
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id'));
        }
        else if($request->optradio == 'lastMonth')
        {
            $index = 9;
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where(DB::raw("(DATE_FORMAT(date,'%Y-%m'))"),$month)
            ->orderBy('date','desc')
            ->get();
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id'));
        }
        else if($request->optradio == 'Amount')
        {
            $index = 10;
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('amount',$request->amount)
            ->orderBy('date','desc')
            ->get();
            $amount = $request->amount;
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','amount'));
        }
        else if($request->optradio == 'Desc')
        {
            $index = 11;
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('description',$request->desc)
            ->orderBy('date','desc')
            ->get();
            $desc = $request->desc;
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','desc'));
        }
        else if($request->optradio == 'Vno')
        {
            $index = 12;
            $voucher     =   JournalVoucher::with(['acode1','acode2'])
            ->where('v_no',$request->vno)
            ->orderBy('date','desc')
            ->get();
            $vno = $request->vno;
            return view('vouchers.index',compact('voucher','permissions','account','index','menu_id','vno'));
        }
    }
}
