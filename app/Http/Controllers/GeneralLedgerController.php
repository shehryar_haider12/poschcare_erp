<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use Auth;
use Carbon\Carbon;
use App\PurchaseOrder;
use App\Products;
use App\TransactionHistory;
use App\PurchaseOrderDetails;
use App\Sales;
use App\Stocks;
use App\StockOut;
use App\User;
use App\CurrentStock;
use App\SaleDetails;
use App\Vendors;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class GeneralLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $account = AccountDetails::all();
        $head = HeadCategory::all();
        $product = AccountDetails::where('Code','like','CA-02%')
        ->get();
        $date = Carbon::now()->format('Y-m-d');
        // $month = Carbon::now()->format('M-y');
        $isEdit = false;
        $u_id = Auth::user()->id;
        $u_name = Auth::user()->name;
        return view('ledger.create',compact('head','account','date','isEdit','u_id','u_name','product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $period = Carbon::parse($request->period)->format('M-Y');
        $period1 = Carbon::parse($request->period)->format('Y-m');
        $request->validate([
            'period'      =>  'required',
            'accounting_date_d' => 'starts_with:'.$period1,
            'accounting_date_c' => 'starts_with:'.$period1
        ]);

        $dis = AccountDetails::where('name_of_account','Discount')
        ->first();
        $taxAcc = AccountDetails::where('name_of_account','Tax')
        ->first();
        $gl = GeneralLedger::max('id');
        $tax = 0;
        $amount = 0;
        $discount = 0;


        //balance maintenance of receivable
        if(str_contains($request->account_code_c, 'CA-01') && !str_contains($request->account_code_c, 'NCA-01'))
        {
            for($i = 0 ; $i < count($request->s_id) ; $i++)
            {
                $sales = Sales::where('id',$request->s_id[$i])->pluck('total')->toArray();
                $totalAmount = array_sum($sales);
            }
            $credit = $request->credit_c;
            if($totalAmount == $credit)
            {
                for($i = 0 ; $i < count($request->s_id) ; $i++)
                {
                    $sale = Sales::where('id',$request->s_id[$i])->first();
                    TransactionHistory::create([
                        'p_s_id' => $request->s_id[$i],
                        'p_type' => 'Sales',
                        't_type' => 'Received',
                        'paid_by' => 'Cash',
                        'total' => $sale->total,
                        'note' => $request->description_c,
                        'created_by' => $request->created_by
                    ]);
                    Sales::where('id',$request->s_id[$i])
                    ->update(['p_status' => 'Paid']);
                }
            }
            if($credit < $totalAmount)
            {
                for($i = 0 ; $i < count($request->s_id) ; $i++)
                {
                    $sale = Sales::where('id',$request->s_id[$i])->first();
                    if($sale->total < $credit)
                    {
                        TransactionHistory::create([
                            'p_s_id' => $request->s_id[$i],
                            'p_type' => 'Sales',
                            't_type' => 'Received',
                            'paid_by' => 'Cash',
                            'total' => $sale->total,
                            'note' => $request->description_c,
                            'created_by' => $request->created_by
                        ]);
                        Sales::where('id',$request->s_id[$i])
                        ->update(['p_status' => 'Paid']);
                    }
                    if($sale->total > $credit)
                    {
                        TransactionHistory::create([
                            'p_s_id' => $request->s_id[$i],
                            'p_type' => 'Sales',
                            't_type' => 'Received',
                            'paid_by' => 'Cash',
                            'total' => $sale->total,
                            'note' => $request->description_c,
                            'created_by' => $request->created_by
                        ]);
                        Sales::where('id',$request->s_id[$i])
                        ->update(['p_status' => 'Partial']);
                    }
                    $credit-=$sale->total;

                }
            }
        }


        if($gl == null)
        {
            $link_id = 1;

            $a = GeneralLedger::create([
                'source' => $request->source,
                'description' => $request->description_d,
                'account_name' => $request->account_name_d,
                'link_id' => $link_id,
                'created_by' => $request->created_by,
                'accounting_date' => $request->accounting_date_d,
                'posted_date' => $request->posted_date,
                'period' => $period,
                'account_code' => $request->account_code_d,
                'transaction_no' => $request->transaction_no_d,
                'currency_code' => $request->currency_code,
                'debit' => $request->debit_d,
                'credit' => $request->credit_d,
                'net_value' => $request->debit_d - $request->credit_d,
                'balance' => $request->debit_d - $request->credit_d,
            ]);

            if($request->tax != null && $request->discount != null )
            {
                $tax = $request->credit_c / $request->tax;
                $discount = $request->discount;
                // $amount = $tax + $request->credit_c;
                // $amount = $amount - $discount;
            }
            if($request->tax == null && $request->discount != null )
            {
                $discount = $request->discount;
                // $amount = $request->credit_c - $discount;
            }

            if($request->tax != null && $request->discount == null )
            {
                $tax = $request->credit_c / $request->tax;
                // $amount = $tax + $request->credit_c;
            }

            GeneralLedger::create([
                'source' => $request->source,
                'description' => $request->description_c,
                'account_name' => $request->account_name_c,
                'link_id' => $link_id,
                'created_by' => $request->created_by,
                'accounting_date' => $request->accounting_date_c,
                'posted_date' => $request->posted_date,
                'period' => $period,
                'account_code' => $request->account_code_c,
                'transaction_no' => $request->transaction_no_c,
                'currency_code' => $request->currency_code,
                'debit' => $request->debit_c,
                'credit' => $request->credit_c,
                'net_value' => $request->debit_c - $request->credit_c,
                'balance' => $request->debit_c - $request->credit_c,
            ]);

            if($request->tax != null)
            {
                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => 'Tax for sale number: '.$request->transaction_no_c,
                    'account_name' => $taxAcc->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_c,
                    'posted_date' => $request->posted_date,
                    'period' => $period,
                    'account_code' => $taxAcc->Code,
                    'transaction_no' => $request->transaction_no_c,
                    'currency_code' => $request->currency_code,
                    'debit' => '0',
                    'credit' => $tax,
                    'net_value' => 0 - $tax,
                    'balance' => 0 - $tax,
                    'amount' => $request->tax
                ]);
            }

            if($request->discount != null)
            {
                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => 'Discount for sale number: '.$request->transaction_no_c,
                    'account_name' => $dis->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_c,
                    'posted_date' => $request->posted_date,
                    'period' => $period,
                    'account_code' => $dis->Code,
                    'transaction_no' => $request->transaction_no_c,
                    'currency_code' => $request->currency_code,
                    'debit' => $discount,
                    'credit' => '0',
                    'net_value' => $discount,
                    'balance' => $discount
                ]);
            }

            if($request->prod_name_d[0] == null && $request->prod_name_c[0] != null)
            {
                for($i = 0 ; $i < count($request->prod_name_c) ; $i++)
                {
                    GeneralLedger::create([
                        'source' => $request->source,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'posted_date' => $request->posted_date,
                        'period' => $period,
                        'currency_code' => $request->currency_code,
                        'account_name' => $request->prod_name_c[$i],
                        'accounting_date' => $request->accounting_date_c,
                        'account_code' => $request->prod_code_c[$i],
                        'transaction_no' => $request->prod_transaction_no_c[$i],
                        'stock_in' => '0',
                        'stock_out' => $request->prod_credit[$i],
                        'net_value' => 0 - $request->prod_credit[$i],
                        'balance' => 0 - $request->prod_credit[$i],
                        'debit' =>  0,
                        'credit' =>  ($request->prod_amount_c[$i] * $request->prod_credit[$i]),
                        'amount' => ($request->prod_amount_c[$i] ),
                        'description' => 'Sale product for sale number: '.$request->prod_transaction_no_c[$i]
                    ]);
                }
            }

            if($request->prod_name_d[0] != null && $request->prod_name_c[0] == null)
            {
                // $purchase = PurchaseOrder::where
                for($i = 0 ; $i < count($request->prod_name_d) ; $i++)
                {
                    GeneralLedger::create([
                        'source' => $request->source,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'posted_date' => $request->posted_date,
                        'period' => $period,
                        'currency_code' => $request->currency_code,
                        'account_name' => $request->prod_name_d[$i],
                        'accounting_date' => $request->accounting_date_c,
                        'account_code' => $request->prod_code_d[$i],
                        'transaction_no' => $request->prod_transaction_no_d[$i],
                        'stock_in' => $request->prod_debit[$i],
                        'stock_out' => '0',
                        'net_value' => $request->prod_debit[$i] - 0,
                        'balance' => $request->prod_debit[$i] - 0,
                        'debit' => ($request->prod_amount_d[$i]) * $request->prod_debit[$i],
                        'credit' => 0,
                        'amount' => ($request->prod_amount_d[$i] ),
                        'description' => 'Purchase product for purchase order number: '.$request->prod_transaction_no_d[$i]
                    ]);
                }
            }

        }
        else
        {
            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            $link_id = $ledger->link_id + 1;
            $debit_d = 0 ;
            $credit_d = 0 ;
            $debit_c = 0 ;
            $credit_c = 0 ;
            $net_d = 0 ;
            $net_c = 0 ;
            $credit_c_p = 0;
            $debit_c_p = 0;
            $net_c_p = 0;
            $credit_d_p = 0;
            $debit_d_p = 0;
            $net_d_p = 0;
            if($request->tax != null && $request->discount != null )
            {
                $tax = $request->credit_c / $request->tax;
                $discount = $request->discount;
                // $amount = $tax + $request->credit_c;
                // $amount = $amount - $discount;
            }
            if($request->tax == null && $request->discount != null )
            {
                $discount = $request->discount;
                // $amount = $request->credit_c - $discount;
            }

            if($request->tax != null && $request->discount == null )
            {
                $tax = $request->credit_c / $request->tax;
                // $amount = $tax + $request->credit_c;
            }

            // if($request->tax == null && $request->discount == null )
            // {
            //     $amount = $request->credit_c;
            // }

            $credit = GeneralLedger::where('account_code',$request->account_code_c)
            ->get();
            // dd($credit);

            $debit = GeneralLedger::where('account_code',$request->account_code_d)
            ->get();
            if($debit->isEmpty())
            {
                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => $request->description_d,
                    'account_name' => $request->account_name_d,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_d,
                    'posted_date' => $request->posted_date,
                    'period' => $period,
                    'account_code' => $request->account_code_d,
                    'transaction_no' => $request->transaction_no_d,
                    'currency_code' => $request->currency_code,
                    'debit' => $request->debit_d,
                    'credit' => $request->credit_d,
                    'net_value' => $request->debit_d - $request->credit_d,
                    'balance' => $request->debit_d - $request->credit_d
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($debit as $key => $d) {
                    $debit_d+=$d->debit;
                    $credit_d+=$d->credit;
                    $balance += $d->net_value;
                }
                $debit_d += ($request->debit_d - $request->credit_d);
                $net_d = $balance + ($request->debit_d - $request->credit_d);

                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => $request->description_d,
                    'account_name' => $request->account_name_d,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_d,
                    'posted_date' => $request->posted_date,
                    'period' => $period,
                    'account_code' => $request->account_code_d,
                    'transaction_no' => $request->transaction_no_d,
                    'currency_code' => $request->currency_code,
                    'debit' => $request->debit_d,
                    'credit' => $request->credit_d,
                    'net_value' => $request->debit_d - $request->credit_d,
                    'balance' => $net_d
                ]);
            }


            if($credit->isEmpty())
            {
                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => $request->description_c,
                    'account_name' => $request->account_name_c,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_c,
                    'posted_date' => $request->posted_date,
                    'period' => $period,
                    'account_code' => $request->account_code_c,
                    'transaction_no' => $request->transaction_no_c,
                    'currency_code' => $request->currency_code,
                    'debit' => $request->debit_c,
                    'credit' => $request->credit_c,
                    'net_value' => $request->debit_c - $request->credit_c,
                    'balance' => $request->debit_c - $request->credit_c
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($credit as $key => $c) {
                    $credit_c+=$c->credit;
                    $debit_c+=$c->debit;
                    $balance += $c->net_value;
                }
                $credit_c += $request->credit_c;
                $net_c = $balance + ($request->debit_c - $request->credit_c);

                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => $request->description_c,
                    'account_name' => $request->account_name_c,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_c,
                    'posted_date' => $request->posted_date,
                    'period' => $period,
                    'account_code' => $request->account_code_c,
                    'transaction_no' => $request->transaction_no_c,
                    'currency_code' => $request->currency_code,
                    'debit' => $request->debit_c,
                    'credit' => $request->credit_c,
                    'net_value' => $request->debit_c - $request->credit_c,
                    'balance' => $net_c
                ]);
            }
            if($request->tax != null)
            {
                $TAX = GeneralLedger::where('account_code',$taxAcc->Code)
                ->get();
                $tax_c = 0;
                $tax_d = 0;
                $tax_n = 0;
                $balance = 0;
                if($TAX->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => $request->source,
                        'description' => 'Tax for sale number: '.$request->transaction_no_c,
                        'account_name' => $taxAcc->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->accounting_date_c,
                        'posted_date' => $request->posted_date,
                        'period' => $period,
                        'account_code' => $taxAcc->Code,
                        'transaction_no' => $request->transaction_no_c,
                        'currency_code' => $request->currency_code,
                        'debit' => '0',
                        'credit' => $tax,
                        'net_value' => 0 - $tax,
                        'balance' => 0 - $tax,
                        'amount' => $request->tax
                    ]);
                }
                else
                {
                    foreach ($TAX as $key => $c) {
                        $tax_c+=$c->credit;
                        $tax_d+=$c->debit;
                        $balance += $c->net_value;
                    }
                    $tax_c += $tax;
                    $tax_n = $balance + (0 - $tax);

                    GeneralLedger::create([
                        'source' => $request->source,
                        'description' => 'Tax for sale number: '.$request->transaction_no_c,
                        'account_name' => $taxAcc->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->accounting_date_c,
                        'posted_date' => $request->posted_date,
                        'period' => $period,
                        'account_code' => $taxAcc->Code,
                        'transaction_no' => $request->transaction_no_c,
                        'currency_code' => $request->currency_code,
                        'debit' => '0',
                        'credit' => $tax,
                        'net_value' => 0 - $tax,
                        'balance' => $tax_n,
                        'amount' => $request->tax
                    ]);
                }
            }

            if($request->discount != null)
            {
                $discount_l = GeneralLedger::where('account_code',$dis->Code)
                ->get();
                $dis_c = 0;
                $dis_d = 0;
                $dis_n = 0;
                $balance = 0;
                if($discount_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => $request->source,
                        'description' => 'Discount for sale number: '.$request->transaction_no_c,
                        'account_name' => $dis->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->accounting_date_c,
                        'posted_date' => $request->posted_date,
                        'period' => $period,
                        'account_code' => $dis->Code,
                        'transaction_no' => $request->transaction_no_c,
                        'currency_code' => $request->currency_code,
                        'debit' => $discount,
                        'credit' => '0',
                        'net_value' => $discount,
                        'balance' => $discount
                    ]);
                }
                else
                {
                    foreach ($discount_l as $key => $c) {
                        $dis_c+=$c->credit;
                        $dis_d+=$c->debit;
                        $balance += $c->net_value;
                    }
                    // $dis_c += $discount;
                    $dis_n = $balance + ($discount - 0);
                    GeneralLedger::create([
                        'source' => $request->source,
                        'description' => 'Discount for sale number: '.$request->transaction_no_c,
                        'account_name' => $dis->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->accounting_date_c,
                        'posted_date' => $request->posted_date,
                        'period' => $period,
                        'account_code' => $dis->Code,
                        'transaction_no' => $request->transaction_no_c,
                        'currency_code' => $request->currency_code,
                        'debit' => $discount,
                        'credit' => '0',
                        'net_value' => $discount,
                        'balance' => $dis_n
                    ]);
                }

            }

            if($request->prod_name_d[0] == null && $request->prod_name_c[0] != null)
            {
                for($i = 0 ; $i < count($request->prod_name_c) ; $i++)
                {
                    $credit_p = GeneralLedger::where('account_code',$request->prod_code_c[$i])
                    ->get();
                    if($credit_p->isEmpty())
                    {

                        GeneralLedger::create([
                            'source' => $request->source,
                            'link_id' => $link_id,
                            'created_by' => $request->created_by,
                            'posted_date' => $request->posted_date,
                            'period' => $period,
                            'currency_code' => $request->currency_code,
                            'account_name' => $request->prod_name_c[$i],
                            'accounting_date' => $request->accounting_date_c,
                            'account_code' => $request->prod_code_c[$i],
                            'transaction_no' => $request->prod_transaction_no_c[$i],
                            'stock_in' => '0',
                            'stock_out' => $request->prod_credit[$i],
                            'net_value' => 0 - $request->prod_credit[$i],
                            'balance' => 0 - $request->prod_credit[$i],
                            'debit' => 0,
                            'credit' =>  ($request->prod_amount_c[$i] * $request->prod_credit[$i]),
                            'amount' => ($request->prod_amount_c[$i] ),
                            'description' => 'Sale product for sale number: '.$request->prod_transaction_no_c[$i]
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($credit_p as $key => $c) {
                            $credit_c_p+=$c->credit;
                            $debit_c_p+=$c->debit;
                            $balance += $c->net_value;
                        }
                        $credit_c_p += $request->prod_credit[$i];
                        $net_c_p = $balance + (0 - $request->prod_credit[$i]);
                        GeneralLedger::create([
                            'source' => $request->source,
                            'link_id' => $link_id,
                            'created_by' => $request->created_by,
                            'posted_date' => $request->posted_date,
                            'period' => $request->period,
                            'currency_code' => $request->currency_code,
                            'account_name' => $request->prod_name_c[$i],
                            'accounting_date' => $request->accounting_date_c,
                            'account_code' => $request->prod_code_c[$i],
                            'transaction_no' => $request->prod_transaction_no_c[$i],
                            'stock_in' => '0',
                            'stock_out' => $request->prod_credit[$i],
                            'net_value' => 0 - $request->prod_credit[$i],
                            'balance' => $net_c_p,
                            'debit' => 0,
                            'credit' =>  ($request->prod_amount_c[$i] * $request->prod_credit[$i]),
                            'amount' => $request->prod_amount_c[$i],
                            'description' => 'Sale product for sale number: '.$request->prod_transaction_no_c[$i]
                        ]);
                    }
                    // $credit_p = '';
                    $credit_c_p = 0;
                    $debit_c_p = 0;
                    $net_c_p = 0;
                }
            }

            if($request->prod_name_d[0] != null && $request->prod_name_c[0] == null)
            {
                for($i = 0 ; $i < count($request->prod_name_d) ; $i++)
                {
                    $debit_p = GeneralLedger::where('account_code',$request->prod_code_d[$i])
                    ->get();
                    if($debit_p->isEmpty())
                    {

                        GeneralLedger::create([
                            'source' => $request->source,
                            'link_id' => $link_id,
                            'created_by' => $request->created_by,
                            'posted_date' => $request->posted_date,
                            'period' => $period,
                            'currency_code' => $request->currency_code,
                            'account_name' => $request->prod_name_d[$i],
                            'accounting_date' => $request->accounting_date_d,
                            'account_code' => $request->prod_code_d[$i],
                            'transaction_no' => $request->prod_transaction_no_d[$i],
                            'stock_in' => $request->prod_debit[$i],
                            'stock_out' => '0',
                            'net_value' => $request->prod_debit[$i],
                            'balance' => $request->prod_debit[$i],
                            'debit' => ($request->prod_amount_d[$i] * $request->prod_debit[$i]),
                            'credit' => 0,
                            'amount' => ($request->prod_amount_d[$i] ),
                            'description' => 'Purchase product for purchase order number: '.$request->prod_transaction_no_d[$i]
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($debit_p as $key => $c) {
                            $credit_d_p+=$c->credit;
                            $debit_d_p+=$c->debit;
                            $balance += $c->net_value;
                        }
                        // $debit_d_p += $request->prod_debit[$i];
                        $net_d_p =  $balance + ($request->prod_debit[$i] - 0);
                        GeneralLedger::create([
                            'source' => $request->source,
                            'link_id' => $link_id,
                            'created_by' => $request->created_by,
                            'posted_date' => $request->posted_date,
                            'period' => $request->period,
                            'currency_code' => $request->currency_code,
                            'account_name' => $request->prod_name_d[$i],
                            'accounting_date' => $request->accounting_date_d,
                            'account_code' => $request->prod_code_d[$i],
                            'transaction_no' => $request->prod_transaction_no_d[$i],
                            'stock_in' => $request->prod_debit[$i],
                            'stock_out' => '0',
                            'balance' => $net_d_p,
                            'net_value' => $request->prod_debit[$i] - 0,
                            'debit' => ($request->prod_amount_d[$i] * $request->prod_debit[$i]),
                            'credit' => 0,
                            'amount' => ($request->prod_amount_d[$i]),
                            'description' => 'Purchase product for purchase order number: '.$request->prod_transaction_no_d[$i]
                        ]);
                    }
                    // $debit_p = '';
                    $credit_d_p = 0;
                    $debit_d_p = 0;
                    $net_d_p = 0;
                }
            }

        }

        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Ledger Entry has been added by '.$u_name,
            'link' => url('').'/accountdetails',
            'name' => 'View Ledger Accounts',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Ledger Entry created successfully!');
        return redirect()->back();
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('link_id',$id)
        ->get();
        $accounts = array();
        $accountCode = array();
        foreach ($ledger as $key => $l) {
            array_push($accounts, $l->account_name);
            array_push($accountCode, substr($l->account_code, 0, strpos($l->account_code, '-', strpos($l->account_code, '-')+1)));
        }
        // dd($accounts,$accountCode);
        $account = AccountDetails::all();
        $product = AccountDetails::where('Code','like','CA-02%')
        ->get();
        $count = count($ledger);
        $u_id = Auth::user()->id;
        $u_name = Auth::user()->name;

        // $userRoles = array('Tax','Discount','Cash In Hand');
        // $result=array_intersect($userRoles,$accounts);

        $code = array('CA-01','RV-01','CL-02');
        $result1=array_intersect($code,$accountCode);
        // dd(count($result1));
        $date = Carbon::now()->format('Y-m-d');
        $month = Carbon::now()->format('M-y');
        // dd($result1);
        return view('ledger.edit',compact('result1','ledger','date','month','account','product','count','u_id','u_name'));

    }

    public function updateEntry(Request $request)
    {
        // dd($request->all());
        $old = GeneralLedger::where('link_id',$request->linkidold)
        ->get();
        $refNO = GeneralLedger::where('link_id',$request->linkidold)
        ->first();
        $oldaccountcode = array();
        $l_quantity = 0;

        foreach ($old as $key => $o) {
            $id = substr($o->account_code, 0, strpos($o->account_code, '-', strpos($o->account_code, '-')+1));
            array_push($oldaccountcode,$id);
            if($o->amount != null)
            {
                $l_quantity+=$o->debit;
            }
        }
        //sales transaction for liability or receivable

        if((in_array('CA-01',$oldaccountcode) || in_array('CL-02',$oldaccountcode) || in_array('CL-01',$oldaccountcode))&& (in_array('CA-03',$oldaccountcode) || in_array('CA-04',$oldaccountcode)))
        {
            // dd('as');
            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            $link_id = $ledger->link_id + 1;
            $cash_d = 0 ;
            $cash_c = 0 ;
            $cash_n = 0 ;
            $bank_d = 0 ;
            $bank_c = 0 ;
            $bank_n = 0 ;
            $customer_d = 0 ;
            $customer_c = 0 ;
            $customer_n = 0 ;
            $liability_n = 0 ;
            $c_credit = 0;
            $c_debit = 0;
            $l_credit = 0;
            $l_debit = 0;

            TransactionHistory::where('id',$refNO->transaction_no)
            ->update([
                'total' => $request->credit_c[0]
            ]);

            foreach ($old as $key => $a) {
                if(str_contains($a->account_code, 'CA-01'))//customer
                {
                    $customer = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;
                    foreach ($customer as $key => $d) {
                        $customer_d+=$d->debit;
                        $customer_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    if($a->credit == 0)
                    {
                        $customer_c += $a->debit;
                        // $customer_n = $customer_d - $customer_c;
                        $c_credit = $a->debit;
                        $c_debit = 0;
                    }
                    if($a->debit == 0)
                    {
                        $customer_d += $a->credit;
                        // $customer_n = $customer_d - $customer_c;
                        $c_debit = $a->credit;
                        $c_credit = 0;
                    }
                    $customer_n = $balance + ($c_debit - $c_credit);

                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $c_credit,
                        'debit' => $c_debit,
                        'net_value' => $c_debit - $c_credit,
                        'balance' => $customer_n
                    ]);
                }
                if(str_contains($a->account_code, 'CL-02'))//liability
                {
                    $customer = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;
                    foreach ($customer as $key => $d) {
                        $balance += $d->net_value;
                    }
                    if($a->credit == 0)
                    {
                        // $customer_c += $a->debit;
                        // $customer_n = $customer_d - $customer_c;
                        $l_credit = $a->debit;
                        $l_debit = 0;
                    }
                    if($a->debit == 0)
                    {
                        // $customer_d += $a->credit;
                        // $customer_n = $customer_d - $customer_c;
                        $l_debit = $a->credit;
                        $l_credit = 0;
                    }
                    $liability_n = $balance + ($l_debit - $l_credit);

                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $l_credit,
                        'debit' => $l_debit,
                        'net_value' => $l_debit - $l_credit,
                        'balance' => $liability_n
                    ]);
                }
                if(str_contains($a->account_code, 'CA-04'))//bank
                {
                    $bank = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($bank as $key => $d) {
                        $bank_d+=$d->debit;
                        $bank_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $bank_d += $a->credit;
                    $bank_n = $balance + ($a->credit - 0);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit,
                        'balance' => $bank_n
                    ]);
                }
                if(str_contains($a->account_code, 'CA-03'))//cash
                {
                    $cash = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($cash as $key => $d) {
                        $cash_d+=$d->debit;
                        $cash_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $cash_d += $a->credit;
                    $cash_n = $balance + ($a->credit - 0);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit,
                        'balance' => $cash_n
                    ]);
                }
            }



        }

        //purchase transactions only
        if(in_array('CL-01',$oldaccountcode) && (in_array('CA-03',$oldaccountcode) || in_array('CA-04',$oldaccountcode)))
        {
            // dd('dds');
            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            $link_id = $ledger->link_id + 1;
            $cash_d = 0 ;
            $cash_c = 0 ;
            $cash_n = 0 ;
            $bank_d = 0 ;
            $bank_c = 0 ;
            $bank_n = 0 ;
            $supplier_d = 0 ;
            $supplier_c = 0 ;
            $supplier_n = 0 ;
            $s_credit = 0;
            $s_debit = 0;

            TransactionHistory::where('id',$refNO->transaction_no)
            ->update([
                'total' => $request->credit_c[0]
            ]);

            foreach ($old as $key => $a) {
                if(str_contains($a->account_code, 'CL-01'))//supplier
                {
                    $supplier = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($supplier as $key => $d) {
                        $supplier_d+=$d->debit;
                        $supplier_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    if($a->credit == 0)
                    {
                        $supplier_c += $a->debit;
                        // $supplier_n = $supplier_d - $supplier_c;
                        $s_credit = $a->debit;
                        $s_debit = 0;
                    }
                    if($a->debit == 0)
                    {
                        $supplier_d += $a->credit;
                        // $supplier_n = $supplier_d - $supplier_c;
                        $s_debit = $a->credit;
                        $s_credit = 0;
                    }
                    $supplier_n = $balance + ($s_debit - $s_credit);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $s_credit,
                        'debit' => $s_debit,
                        'net_value' => $s_debit - $s_credit,
                        'balance' => $supplier_n
                    ]);
                }

                if(str_contains($a->account_code, 'CA-04'))//bank
                {
                    $bank = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($bank as $key => $d) {
                        $bank_d+=$d->debit;
                        $bank_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $bank_d += $a->credit;
                    $bank_n = $balance + ($a->credit - 0);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit,
                        'balance' => $bank_n
                    ]);
                }

                if(str_contains($a->account_code, 'CA-03'))//cash
                {
                    $cash = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($cash as $key => $d) {
                        $cash_d+=$d->debit;
                        $cash_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $cash_d += $a->credit;
                    $cash_n = $balance + ($a->credit - 0);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit - 0,
                        'balance' => $cash_n
                    ]);
                }
            }

        }

        if(in_array("PUR-01", $oldaccountcode))
        {
            // dd('sdsd');
            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            $link_id = $ledger->link_id + 1;
            $purchase_d = 0 ;
            $purchase_c = 0 ;
            $purchase_n = 0 ;
            $supplier_d = 0 ;
            $supplier_c = 0 ;
            $supplier_n = 0 ;
            $s_credit = 0;
            $s_debit = 0;
            $cash_d = 0 ;
            $cash_c = 0 ;
            $cash_n = 0 ;
            $bank_d = 0 ;
            $bank_c = 0 ;
            $bank_n = 0 ;
            $inv_d = 0 ;
            $inv_c = 0 ;
            $inv_n = 0 ;
            $product = [] ;
            $pod = [] ;

            $stock = Stocks::where('grn_no',$refNO->transaction_no)
            ->first();
            $purchaseOrderDetails = PurchaseOrderDetails::where('id',$stock->purchase_d_id)
            ->first();
            $purchaseOrder = PurchaseOrder::find($purchaseOrderDetails->o_id);
            $r_quantity = PurchaseOrderDetails::where('o_id',$purchaseOrderDetails->o_id)
            ->sum('received_quantity');
            $amount = $request->debit_d[0];
            $quantity = 0;
            $p_quantity = PurchaseOrderDetails::where('o_id',$purchaseOrderDetails->o_id)
            ->sum('quantity');

            for($i = 0 ; $i < count($request->prod_name_d) ; $i++)
            {
                $quantity+=$request->prod_credit_d[$i];

                $pr = substr($request->prod_name_d[$i], 0, strpos($request->prod_name_d[$i], '-'));
                $product[] = Products::with('unit')
                ->where('pro_name',$pr)
                ->first();
                $pod[] = PurchaseOrderDetails::where('p_id',$product[$i]->id)
                ->where('o_id',$purchaseOrderDetails->o_id)
                ->first();
            }
            $r_quantity -= $l_quantity;
            $r_quantity += $quantity;

            if($amount == $purchaseOrder->total && in_array('CA-03',$oldaccountcode))
            {
                $p_status = 'Paid';
            }
            else
            {
                $p_status = $purchaseOrder->status;
            }

            if($r_quantity == $p_quantity)
            {
                $status = 'Received';
            }
            else
            {
                $status = 'Partial';
            }

            $final = 0;
            $final4 = 0;
            $total = 0;
            for ($i=0; $i < count($request->prod_name_d) ; $i++) {
                Stocks::where('grn_no',$refNO->transaction_no)
                ->where('p_id',$product[$i]->id)
                ->update([
                    'quantity' => $request->prod_credit_d[$i]
                ]);
                $final = Stocks::where('purchase_d_id',$pod[$i]->id)
                ->sum('quantity');
                $final2 = PurchaseOrderDetails::where('p_id',$product[$i]->id)
                ->where('o_id',$purchaseOrder->id)
                ->update([
                    'received_quantity' => $final
                ]);

                $final3 = Stocks::where('p_id',$pod[$i]->p_id)
                ->where('w_id',$purchaseOrder->w_id)
                ->sum('quantity');
                $final4 = StockOut::where('p_id',$pod[$i]->p_id)
                ->where('w_id',$purchaseOrder->w_id)
                ->sum('quantity');
                $total = $final3 - $final4;
                if($product[$i]->unit->u_name == 'Liter')
                {
                    $unit_quantity = ($product[$i]->weight * 1000) * $total;
                }
                else if($product[$i]->unit->u_name == 'Mililiter')
                {
                    $unit_quantity = $product[$i]->weight * $total;
                }
                else
                {
                    $unit_quantity = null;
                }
                CurrentStock::where('p_id',$pod[$i]->p_id)
                ->where('w_id',$purchaseOrder->w_id)
                ->update([
                    'quantity' => $final3 - $final4,
                    'unit_quantity' => $unit_quantity

                ]);
            }

            PurchaseOrder::where('id',$purchaseOrder->o_id)
            ->update([
                'p_status' => $p_status,
                'status' => $status,
            ]);


            foreach ($old as $key => $a) {
                if(str_contains($a->account_code, 'PUR-01-001'))//purchase
                {
                    $purchase = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($purchase as $key => $d) {
                        $purchase_d+=$d->debit;
                        $purchase_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $purchase_c += $a->debit;
                    $purchase_n = $balance + (0 - $a->debit);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $a->debit,
                        'debit' => '0',
                        'net_value' => 0 - $a->debit,
                        'balance' => $purchase_n
                    ]);
                }
                if(str_contains($a->account_code, 'CL-01'))//supplier
                {
                    $supplier = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($supplier as $key => $d) {
                        $supplier_d+=$d->debit;
                        $supplier_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    if($a->credit == 0)
                    {
                        $supplier_c += $a->debit;
                        // $supplier_n = $supplier_d - $supplier_c;
                        $s_credit = $a->debit;
                        $s_debit = 0;
                    }
                    if($a->debit == 0)
                    {
                        $supplier_d += $a->credit;
                        // $supplier_n = $supplier_d - $supplier_c;
                        $s_debit = $a->credit;
                        $s_credit = 0;
                    }
                    $supplier_n = $balance + ($s_debit - $s_credit);

                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $s_credit,
                        'debit' => $s_debit,
                        'net_value' => $s_debit - $s_credit,
                        'balance' => $supplier_n
                    ]);
                }

                if(str_contains($a->account_code, 'CA-04'))//bank
                {
                    $bank = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;
                    TransactionHistory::where('id',$refNO->transaction_no)
                    ->update([
                        'total' => $request->credit_c[0]
                    ]);
                    foreach ($bank as $key => $d) {
                        $bank_d+=$d->debit;
                        $bank_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $bank_d += $a->credit;
                    $bank_n = $balance + ($a->credit - 0);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit,
                        'balance' => $bank_n
                    ]);
                }

                if(str_contains($a->account_code, 'CA-03'))//cash
                {
                    $cash = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;
                    TransactionHistory::where('id',$refNO->transaction_no)
                    ->update([
                        'total' => $request->credit_c[0]
                    ]);

                    foreach ($cash as $key => $d) {
                        $cash_d+=$d->debit;
                        $cash_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $cash_d += $a->credit;
                    $cash_n = $balance + ($a->credit);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit,
                        'balance' => $cash_n
                    ]);
                }

                if(str_contains($a->account_code, 'CA-02'))//inventory
                {
                    $inv = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;
                    foreach ($cash as $key => $d) {
                        $inv_d+=$d->debit;
                        $inv_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    // $cash_d += $a->credit;
                    $inv_n = $balance + (0 - $a->debit);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'currency_code' => $request->currency_code,
                        'account_name' => $a->account_name,
                        'accounting_date' => $request->posted_date,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'stock_out' => $a->stock_out,
                        'stock_in' => '0',
                        'net_value' => 0 - $a->stock_out,
                        'balance' => 0 - $inv_n,
                        'credit' =>  0 - ($a->debit),
                        'debit' =>  0,
                        'amount' =>  ($a->amount),
                        'description' => 'Changes in purchased product for purchase order number: '.$a->transaction_no
                    ]);
                }
            }
        }

        if(in_array("RV-01", $oldaccountcode))
        {
            $ledgers = GeneralLedger::with('gdn.sdetails.products')->where('link_id',$request->linkidold)
            ->where('account_code','like','CA-02%')
            ->get();
            // return $gdn;
            $product_amount = 0;
            $product_amount1 = 0;
            foreach ($ledgers as $key => $gdn) {
                foreach ($gdn->gdn as $key => $stock_out) {
                    foreach ($request->prod_name_c as $input_key => $prod_name_c) {
                        $code = explode(' - ',$prod_name_c);
                        if($code[1] == $stock_out->sdetails->products->pro_code){

                            $discount_per_product = ($stock_out->sdetails->price * $stock_out->sdetails->quantity * $stock_out->sdetails->discount_percent) /100;


                            $product_amount += (($stock_out->sdetails->price * $stock_out->sdetails->quantity ) - $discount_per_product);


                            $discount_per_product = ($stock_out->sdetails->price * $request->prod_credit[$input_key] * $stock_out->sdetails->discount_percent) /100;


                            $product_amount1 += (($stock_out->sdetails->price * $request->prod_credit[$input_key]  ) - $discount_per_product);

                        }

                    }
                }
            }
            $final1 = $product_amount;
            $taxS=0;
            $sub = 0;
            $quantity = 0;
            $stock = StockOut::where('gdn_no',$refNO->transaction_no)
            ->first();
            // dd($stock);
            $saleDetails = SaleDetails::where('id',$stock->sale_d_id)
            ->first();
            $saleOrder = Sales::find($saleDetails->s_id);
            $transaction = TransactionHistory::where('p_s_id',$saleDetails->s_id)
            ->where('p_type','Sales')
            ->sum('total');


            $r_quantity = SaleDetails::where('s_id',$saleDetails->s_id)
            ->sum('delivered_quantity');
            $p_quantity = SaleDetails::where('s_id',$saleDetails->s_id)
            ->sum('quantity');


            // dd($r_quantity,$p_quantity);
            if($request->tax!=0)
            {
                $amount = $request->credit_c[0];
                $taxS = ($amount / 100) * $request->tax;
                $final1 = $final1 + $amount + $tax;
            }

            if($request->tax==0 )
            {
                $amount = $request->credit_c[0];
                $final1 = $amount;
                $taxS = null ;
            }

            $final = 0;
            $product = [];
            $sod = [];
            $final4 = 0;
            $total = 0 ;
            for ($i=0; $i < count($request->prod_name_c) ; $i++) {
                $quantity+=$request->prod_credit[$i];
                $pr = substr($request->prod_name_c[$i], 0, strpos($request->prod_name_c[$i], '-'));
                $product[] = Products::with('unit')
                ->where('pro_name',$pr)
                ->first();
                $sod[] = SaleDetails::where('p_id',$product[$i]->id)
                ->where('s_id',$saleOrder->id)
                ->first();

                StockOut::where('gdn_no',$refNO->transaction_no)
                ->where('p_id',$product[$i]->id)
                ->update([
                    'quantity' => $request->prod_credit[$i]
                ]);
                $final = StockOut::where('sale_d_id',$sod[$i]->id)
                ->where('p_id',$product[$i]->id)
                ->first();
                $final2 = SaleDetails::where('p_id',$product[$i]->id)
                ->where('s_id',$saleOrder->id)
                ->update([
                    'delivered_quantity' => $final->quantity
                ]);

                $final3 = Stocks::where('p_id',$sod[$i]->p_id)
                ->where('w_id',$saleOrder->w_id)
                ->sum('quantity');
                $final4 = StockOut::where('p_id',$sod[$i]->p_id)
                ->where('w_id',$saleOrder->w_id)
                ->sum('quantity');
                $total = $final3 - $final4;
                if($product[$i]->unit->u_name == 'Liter')
                {
                    $unit_quantity = ($product[$i]->weight * 1000) * $total;
                }
                else if($product[$i]->unit->u_name == 'Mililiter')
                {
                    $unit_quantity = $product[$i]->weight * $total;
                }
                else
                {
                    $unit_quantity = null;
                }

                CurrentStock::where('p_id',$sod[$i]->p_id)
                ->where('w_id',$saleOrder->w_id)
                ->update([
                    'quantity' => $final3 - $final4,
                    'unit_quantity' => $unit_quantity

                ]);
            }

            $r_quantity -= $l_quantity;
            $r_quantity += $quantity;

            if($r_quantity == $p_quantity)
            {
                $status = 'Complete';
            }
            else
            {
                $status = 'Partial';
            }
            Sales::where('id',$saleOrder->id)
            ->update([
                'tax' => $taxS,
                'total' => $final1,
                's_status' => $status
            ]);

            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            $link_id = $ledger->link_id + 1;
            // $dis = AccountDetails::where('name_of_account','Discount')
            // ->first();
            // $taxAcc = AccountDetails::where('name_of_account','Tax')
            // ->first();   $sales_d = 0 ;
            $inv_d = 0 ;
            $inv_c = 0 ;
            $inv_n = 0 ;

            $sales_d = 0 ;
            $sales_c = 0 ;
            $sales_n = 0 ;
            $customer_d = 0 ;
            $customer_c = 0 ;
            $customer_n = 0 ;
            $c_credit = 0;
            $c_debit = 0;

            $liability_d = 0 ;
            $liability_c = 0 ;
            $liability_n = 0 ;
            $l_credit = 0;
            $l_debit = 0;


            foreach ($old as $key => $a) {
                if(str_contains($a->account_code, 'RV-01-001'))//sales
                {
                    $sales = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($sales as $key => $d) {
                        $sales_d+=$d->debit;
                        $sales_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    $sales_d += $a->credit;
                    $sales_n = $balance + ($a->credit);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' => $a->credit,
                        'balance' => $sales_n
                    ]);
                }
                if(str_contains($a->account_code, 'CA-01'))//customer
                {
                    $customer = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($customer as $key => $d) {
                        $customer_d+=$d->debit;
                        $customer_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    if($a->credit == 0)
                    {
                        $customer_c += $a->debit;
                        // $customer_n = $customer_d - $customer_c;
                        $c_credit = $a->debit;
                        $c_debit = 0;
                    }
                    if($a->debit == 0)
                    {
                        $customer_d += $a->credit;
                        // $customer_n = $customer_d - $customer_c;
                        $c_debit = $a->credit;
                        $c_credit = 0;
                    }
                    $customer_n = $balance + ($c_debit - $c_credit);

                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $c_credit,
                        'debit' => $c_debit,
                        'net_value' => $c_debit - $c_credit,
                        'balance' => $customer_n
                    ]);
                }

                if(str_contains($a->account_code, 'CL-02'))//liability
                {
                    $liability = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;

                    foreach ($liability as $key => $d) {
                        $liability_d+=$d->debit;
                        $liability_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    if($a->credit == 0)
                    {
                        $liability_c += $a->debit;
                        // $liability_n = $liability_d - $liability_c;
                        $l_credit = $a->debit;
                        $l_debit = 0;
                    }
                    if($a->debit == 0)
                    {
                        $liability_d += $a->credit;
                        // $liability_n = $liability_d - $liability_c;
                        $l_debit = $a->credit;
                        $l_credit = 0;
                    }
                    $liability_n = $balance + ($l_debit - $l_credit);

                    GeneralLedger::create([
                        'source' => 'Manual',
                        'description' => $a->description,
                        'account_name' => $a->account_name,
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'accounting_date' => $request->posted_date,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'currency_code' => $request->currency_code,
                        'credit' => $l_credit,
                        'debit' => $l_debit,
                        'net_value' => $l_debit - $l_credit,
                        'balance' => $liability_n
                    ]);
                }

                // if(str_contains($a->account_code, 'EXP-05'))//tax
                // {
                //     GeneralLedger::create([
                //         'source' => $request->source,
                //         'description' => 'Tax for sale number: '.$a->transaction_no_c,
                //         'account_name' => $taxAcc->name_of_account,
                //         'link_id' => $link_id,
                //         'created_by' => $request->created_by,
                //         'accounting_date' => $request->posted_date,
                //         'posted_date' => $request->posted_date,
                //         'period' => $request->period,
                //         'account_code' => $taxAcc->Code,
                //         'transaction_no' => $a->transaction_no_c,
                //         'currency_code' => $request->currency_code,
                //         'credit' => $a->debit,
                //         'debit' => $a->credit,
                //         'net_value' => $a->debit,
                //         'amount' => $a->amount
                //     ]);
                // }

                // if(str_contains($a->account_code, 'EXP-05'))//discount
                // {
                //     GeneralLedger::create([
                //         'source' => $request->source,
                //         'description' => 'Discount for sale number: '.$a->transaction_no_c,
                //         'account_name' => $dis->name_of_account,
                //         'link_id' => $link_id,
                //         'created_by' => $request->created_by,
                //         'accounting_date' => $$request->posted_date,
                //         'posted_date' => $request->posted_date,
                //         'period' => $request->period,
                //         'account_code' => $dis->Code,
                //         'transaction_no' => $a->transaction_no_c,
                //         'currency_code' => $request->currency_code,
                //         'credit' => $a->debit,
                //         'debit' => '0',
                //         'net_value' => $a->debit
                //     ]);
                // }

                if(str_contains($a->account_code, 'CA-02'))//inventory
                {
                    $inv = GeneralLedger::where('account_code',$a->account_code)
                    ->get();
                    $balance = 0;
                    foreach ($inv as $key => $d) {
                        $inv_d+=$d->debit;
                        $inv_c+=$d->credit;
                        $balance += $d->net_value;
                    }
                    // $cash_d += $a->credit;
                    $inv_n = $balance + ($a->credit - 0);
                    GeneralLedger::create([
                        'source' => 'Manual',
                        'link_id' => $link_id,
                        'created_by' => $request->created_by,
                        'posted_date' => $request->posted_date,
                        'period' => $request->period,
                        'currency_code' => $request->currency_code,
                        'account_name' => $a->account_name,
                        'accounting_date' => $request->posted_date,
                        'account_code' => $a->account_code,
                        'transaction_no' => $a->transaction_no,
                        'debit' => $a->credit,
                        'credit' => '0',
                        'net_value' =>  $a->credit,
                        'balance' =>  $inv_n,
                        'stock_in' =>  ($a->stock_out),
                        'stock_out' =>  0 ,
                        'amount' =>  ($a->amount),
                        'description' => 'Changes in purchased product for purchase order number: '.$a->transaction_no
                    ]);
                }
            }
        }
        else
        {
            // dd('da');
            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            $link_id = $ledger->link_id + 1;
            $debit_d = 0 ;
            $credit_d = 0 ;
            $net_d = 0 ;
            $debit_c = 0 ;
            $credit_c = 0 ;
            $net_c = 0 ;

            for($i = 0 ; $i < count($request->account_code_d) ; $i++)
            {
                $debit = GeneralLedger::where('account_code',$request->account_code_d[$i])
                ->get();
                $balance = 0;
                foreach ($debit as $key => $d) {
                    $debit_d+=$d->debit;
                    $credit_d+=$d->credit;
                    $balance += $d->net_value;
                }
                $net_d = $balance + ($request->credit_d[$i] - $request->debit_d[$i]);
                GeneralLedger::create([
                    'source' => $request->source,
                    'description' => $request->description_d[$i],
                    'account_name' => $request->account_name_d[$i],
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'accounting_date' => $request->accounting_date_d[$i],
                    'posted_date' => $request->posted_date,
                    'period' => $request->period,
                    'account_code' => $request->account_code_d[$i],
                    'transaction_no' => $request->transaction_no_d[$i],
                    'currency_code' => $request->currency_code,
                    'debit' => $request->credit_d[$i],
                    'credit' => $request->debit_d[$i],
                    'net_value' => $request->credit_d[$i] - $request->debit_d[$i],
                    'balance' => $net_d
                ]);
                $debit_d=0;
                $credit_d=0;
                $net_d=0;
            }

            for($i = 0 ; $i < count($request->account_code_c) ; $i++)
            {
                $credit = GeneralLedger::where('account_code',$request->account_code_c[$i])
                ->get();
                $balance = 0 ;
                foreach ($credit as $key => $c) {
                    $credit_c+=$c->credit;
                    $debit_c+=$c->debit;
                    $balance += $d->net_value;
                }
                $credit_c += $request->credit_c[$i];
                $net_c = $balance + ($request->credit_c[$i] - $request->debit_c[$i]);
                GeneralLedger::create([
                    'source' => $request->source,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'description' => $request->description_c[$i],
                    'posted_date' => $request->posted_date,
                    'period' => $request->period,
                    'currency_code' => $request->currency_code,
                    'account_name' => $request->account_name_c[$i],
                    'accounting_date' => $request->accounting_date_c[$i],
                    'account_code' => $request->account_code_c[$i],
                    'transaction_no' => $request->transaction_no_c[$i],
                    'credit' => $request->debit_c[$i],
                    'debit' => $request->credit_c[$i],
                    'net_value' => $request->credit_c[$i] - $request->debit_c[$i],
                    'balance' => $net_c
                ]);
                $debit_c=0;
                $credit_c=0;
                $net_c=0;
            }

        }

        $dis = AccountDetails::where('name_of_account','Discount')
        ->first();
        $taxAcc = AccountDetails::where('name_of_account','Tax')
        ->first();
        $id = GeneralLedger::max('id');
        $ledger = GeneralLedger::where('id',$id)->first();
        $link_id = $ledger->link_id + 1;
        $debit_d = 0 ;
        $credit_d = 0 ;
        $debit_c = 0 ;
        $credit_c = 0 ;
        $net_d = 0 ;
        $net_c = 0 ;
        $credit_c_p = 0;
        $debit_c_p = 0;
        $net_c_p = 0;
        $credit_d_p = 0;
        $debit_d_p = 0;
        $net_d_p = 0;
        $discount = 0;

        if($request->tax != 0)
        {
            $tax = $request->credit_c[0] / $request->tax;
        }


        for($i = 0 ; $i < count($request->account_code_d) ; $i++)
        {
            $debit = GeneralLedger::where('account_code',$request->account_code_d[$i])
            ->get();
            $balance = 0;
            foreach ($debit as $key => $d) {
                $debit_d+=$d->debit;
                $credit_d+=$d->credit;
                $balance += $d->net_value;
            }
            $debit_d += $request->debit_d[$i];
            $net_d = $balance + ((in_array("RV-01", $oldaccountcode) ? $product_amount1 : $request->debit_d[$i]) - $request->credit_d[$i]);
            GeneralLedger::create([
                'source' => $request->source,
                'description' => $request->description_d[$i],
                'account_name' => $request->account_name_d[$i],
                'link_id' => $link_id,
                'created_by' => $request->created_by,
                'accounting_date' => $request->accounting_date_d[$i],
                'posted_date' => $request->posted_date,
                'period' => $request->period,
                'account_code' => $request->account_code_d[$i],
                'transaction_no' => $request->transaction_no_d[$i],
                'currency_code' => $request->currency_code,
                'debit' => (in_array("RV-01", $oldaccountcode) ? $product_amount1 : $request->debit_d[$i]),
                'credit' => $request->credit_d[$i],
                'net_value' =>(in_array("RV-01", $oldaccountcode) ? $product_amount1 : $request->debit_d[$i]) - $request->credit_d[$i],
                'balance' => $net_d
            ]);
            $debit_d=0;
            $credit_d=0;
            $net_d=0;
        }

        for($i = 0 ; $i < count($request->account_code_c) ; $i++)
        {
            $credit = GeneralLedger::where('account_code',$request->account_code_c[$i])
            ->get();
            $balance = 0 ;
            foreach ($credit as $key => $c) {
                $credit_c+=$c->credit;
                $debit_c+=$c->debit;
                $balance += $d->net_value;
            }
            $credit_c += $request->credit_c[$i];
            $net_c = $balance + ($request->debit_c[$i] - (in_array("RV-01", $oldaccountcode) ? $product_amount1 : $request->credit_c[$i]));
            GeneralLedger::create([
                'source' => $request->source,
                'link_id' => $link_id,
                'created_by' => $request->created_by,
                'description' => $request->description_c[$i],
                'posted_date' => $request->posted_date,
                'period' => $request->period,
                'currency_code' => $request->currency_code,
                'account_name' => $request->account_name_c[$i],
                'accounting_date' => $request->accounting_date_c[$i],
                'account_code' => $request->account_code_c[$i],
                'transaction_no' => $request->transaction_no_c[$i],
                'debit' => $request->debit_c[$i],
                'credit' =>  (in_array("RV-01", $oldaccountcode) ? $product_amount1 : $request->credit_c[$i]),
                'net_value' => $request->debit_c[$i] - (in_array("RV-01", $oldaccountcode) ? $product_amount1 : $request->credit_c[$i]),
                'balance' => $net_c
            ]);
            $debit_c=0;
            $credit_c=0;
            $net_c=0;
        }


        if($request->tax != 0)
        {
            $tax_c = 0;
            $tax_d = 0;
            $tax_n = 0;
            $TAX = GeneralLedger::where('account_code', $taxAcc->Code)
            ->get();
            $balance = 0 ;
            foreach ($TAX as $key => $c) {
                $tax_c+=$c->credit;
                $tax_n+=$c->debit;
                $balance += $d->net_value;
            }
            // $tax_c += $request->credit_c[$i];
            $tax_n = $balance + (0 - $tax);
            GeneralLedger::create([
                'source' => $request->source,
                'description' => 'Tax for sale number: '.$request->transaction_no_c[0],
                'account_name' => $taxAcc->name_of_account,
                'link_id' => $link_id,
                'created_by' => $request->created_by,
                'accounting_date' => $request->accounting_date_c[0],
                'posted_date' => $request->posted_date,
                'period' => $request->period,
                'account_code' => $taxAcc->Code,
                'transaction_no' => $request->transaction_no_c[0],
                'currency_code' => $request->currency_code,
                'debit' => '0',
                'credit' => $tax,
                'net_value' => 0 - $tax,
                'balance' => $net_c,
                'amount' => $request->tax
            ]);
        }

        if($request->discount != 0)
        {
            $dis_c = 0;
            $dis_d = 0;
            $dis_n = 0;
            $Discount = GeneralLedger::where('account_code', $dis->Code)
            ->get();
            $balance = 0 ;
            foreach ($Discount as $key => $c) {
                $dis_c+=$c->credit;
                $dis_n+=$c->debit;
                $balance += $d->net_value;
            }
            // $tax_c += $request->credit_c[$i];
            $dis_n = $balance + ($discount - 0);
            GeneralLedger::create([
                'source' => $request->source,
                'description' => 'Discount for sale number: '.$request->transaction_no_c[0],
                'account_name' => $dis->name_of_account,
                'link_id' => $link_id,
                'created_by' => $request->created_by,
                'accounting_date' => $request->accounting_date_c[0],
                'posted_date' => $request->posted_date,
                'period' => $request->period,
                'account_code' => $dis->Code,
                'transaction_no' => $request->transaction_no_c[0],
                'currency_code' => $request->currency_code,
                'debit' => $discount,
                'credit' => '0',
                'net_value' => $discount,
                'balance' => $dis_n
            ]);
        }

        if($request->prod_name_c[0] != null)
        {
            for($i = 0 ; $i < count($request->prod_name_c) ; $i++)
            {
                $balance = 0;
                $credit_p = GeneralLedger::where('account_code',$request->prod_code_c[$i])
                ->get();

                foreach ($credit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_c_p = $balance + (0 - $request->prod_credit[$i]);
                GeneralLedger::create([
                    'source' => $request->source,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'posted_date' => $request->posted_date,
                    'period' => $request->period,
                    'currency_code' => $request->currency_code,
                    'account_name' => $request->prod_name_c[$i],
                    'accounting_date' => $request->posted_date,
                    'account_code' => $request->prod_code_c[$i],
                    'transaction_no' => $request->prod_transaction_no_c[$i],
                    'stock_in' => '0',
                    'stock_out' =>  $request->prod_credit[$i],
                    'net_value' => 0 - $request->prod_credit[$i],
                    'balance' => 0 - $request->prod_credit[$i],
                    'credit' =>  ($request->prod_amount_c[$i] * $request->prod_credit[$i] * (-1)),
                    'debit' => 0,
                    'amount' => ($request->prod_amount_c[$i] ),
                    'description' => 'Changes in sale product for sale number: '.$request->prod_transaction_no_c[$i]
                ]);

                $net_c_p = 0;
            }
        }

        if($request->prod_name_d[0] != null)
        {
            // dd('ds');
            for($i = 0 ; $i < count($request->prod_name_d) ; $i++)
            {
                $balance = 0;
                $debit_p = GeneralLedger::where('account_code',$request->prod_code_d[$i])
                ->get();
                foreach ($debit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_d_p = $balance + ( $request->prod_credit_d[$i] - 0);
                GeneralLedger::create([
                    'source' => $request->source,
                    'link_id' => $link_id,
                    'created_by' => $request->created_by,
                    'posted_date' => $request->posted_date,
                    'period' => $request->period,
                    'currency_code' => $request->currency_code,
                    'account_name' => $request->prod_name_d[$i],
                    'accounting_date' => $request->posted_date,
                    'account_code' => $request->prod_code_d[$i],
                    'transaction_no' => $request->prod_transaction_no_d[$i],
                    'stock_in' => $request->prod_credit_d[$i],
                    'stock_out' => '0',
                    'net_value' =>  $request->prod_credit_d[$i],
                    'debit' =>  ($request->prod_amount_d[$i] * $request->prod_credit_d[$i]),
                    'credit' =>  0,
                    'amount' =>  ($request->prod_amount_d[$i]),
                    'balance' =>  $net_d_p,
                    'description' => 'Changes in purchased product for purchase order number: '.$request->prod_transaction_no_d[$i]
                ]);

                $net_d_p = 0;
            }
        }
        toastr()->success('Ledger Entry updated successfully!');
        return redirect(url('').'/accountdetails');
    }



    public function findCustomer($id)
    {
        $company = substr($id,0 , strpos($id,'-')-1);
        $name = substr($id, strpos($id,'-')+2);
        $c_id = Vendors::where('v_type','Customer')
        ->where('name',$name)->where('company',$company)->first();
        $sales = Sales::where('c_id',$c_id->id)->where(function ($query) {
            $query->where('p_status', 'Pending')
                  ->orWhere('p_status', 'Partial');
        })->where(function ($query) {
            $query->where('s_status','Partial')
            ->orWhere('s_status','Complete')
            ->orWhere('s_status','Delivered');
        })
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
        }])
        ->with(['return' => function($q1){
            $q1->where('status','Approved');
        }])
        ->get();
        $ledger = GeneralLedger::where('account_name', $company.' - '.$name)->where('description','Opening Account')->sum('debit');
        return [$sales,$ledger];
    }

    public function findSupplier($id)
    {
        $name = substr($id,0 , strpos($id,'-')-1);
        $company = substr($id, strpos($id,'-')+2);
        $c_id = Vendors::where('v_type','Supplier')
        ->where('name',$name)->where('company',$company)->first();
        $purchase = PurchaseOrder::where('s_id',$c_id->id)->where(function ($query) {
            $query->where('p_status', 'Pending')
            ->orWhere('p_status', 'Partial');
        })->where(function ($query) {
            $query->where('status','Partial')
            ->orWhere('status','Received');
        })
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
        }])
        ->get();
        $ledger = GeneralLedger::where('account_name', $name.' - '.$company)->where('description','Opening Account')->sum('credit');
        return [$purchase,$ledger];
    }


}
