<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\VendorsUpdateRequest;
use App\Groups;
use App\City;
use App\User;
use App\VendorAttachment;
use DataTables;
use App\Exports\ReceivablesExportStructure;
use App\Exports\CustomerExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;
use Auth;
use Storage;
use Carbon\Carbon;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function importStructure()
    {
        return Excel::download(new ReceivablesExportStructure, 'ReceivablesExportStructure.xlsx');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('customer.index',compact('permissions'));
    }

    public function document($id)
    {
        $p = VendorAttachment::where('v_id',$id)->get();
        return $p;
    }

    public function singleDocument($id)
    {
        $p = VendorAttachment::find($id);
        $file= public_path()."/documents/".$p->document;

        return response()->download($file);
    }

    public function balance($id)
    {
        $credit = 0;
        $debit = 0;
        $customer = Vendors::find($id);
        if($customer->balance != null)
        {
            $account =AccountDetails::with('generalLedger')->where('name_of_account',$customer->company.' - '.$customer->name)
            ->where('Code','like','CA-01%')
            ->where('Code','Not like','NCA-01%')
            ->first();
            if($account!=null)
            {
                for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                {
                    $credit += $account->generalLedger[$k]->credit;

                    $debit += $account->generalLedger[$k]->debit;
                }
                $balance =  $debit - $credit;
                $cus_balance = $customer->balance;
            }
            else {
                $balance = 0;
                $cus_balance = 0;
            }
        }
        else {
            $account =AccountDetails::with('generalLedger')->where('name_of_account',$customer->company.' - '.$customer->name)
            ->where('Code','like','CA-01%')
            ->where('Code','Not like','NCA-01%')
            ->first();
            if($account!=null)
            {
                for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                {
                    $credit += $account->generalLedger[$k]->credit;

                    $debit += $account->generalLedger[$k]->debit;
                }
                $balance =  $debit - $credit;
                $cus_balance = 0;
            }
            else {
                $balance = 0;
                $cus_balance = 0;
            }
        }
        return [$balance,$cus_balance,$customer];
    }

    public function datatable()
    {
        $vendor=Vendors::where('v_type','Customer')->get();
        return DataTables::of($vendor)->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $city=City::where('status',1)->get();
        $cgroup=Groups::where('g_type','Customer')->get();
        $pgroup=Groups::where('g_type','Price')->get();
        $data= [
            'isEdit' => false,
            'city' => $city,
            'cgroup' => $cgroup,
            'pgroup' => $pgroup,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('customer.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');


        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        $customer = Vendors::where('company',$request->company)
        ->where('name',$request->name)->where('v_type','Customer')->first();
        if($customer == null)
        {
            $posted_date = Carbon::now()->format('Y-m-d');
            $period = Carbon::now()->format('M-y');
            $hcat = HeadCategory::where('name','Receivables')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->orderBy('id','desc')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;

            $data = [
                'Code' => $code,
                'name_of_account' => $request->company.' - '.$request->name,
                'c_id' => $hcat->id,
                'created_by' => $u_id
            ];
            $account_c = AccountDetails::create($data);
            $ven = Vendors::create([
                'c_group' => $request->c_group,
                'p_group' => $request->p_group,
                'company' => $request->company,
                'name' => $request->name,
                'c_no' => $request->c_no,
                'name2' => $request->name2,
                'c_no2' => $request->c_no2,
                'address' => $request->address,
                'country' => $request->country,
                'VAT' => $request->VAT,
                'GST' => $request->GST,
                'state' => $request->state,
                'email' => $request->email,
                'postalCode' => $request->postalCode,
                'c_id' => $request->c_id,
                'v_type' => $request->v_type,
                'balance' => $request->balance,
                'opening_balance' => $request->opening_balance,
                'created_by' => $u_id,
                'status' => $status
            ]);

            if(isset($request->document))
            {
                foreach ($request->document as $key => $d) {
                    if($d != null)
                    {
                        $document = '';
                        $document= Storage::disk('documents')->putFile('',$d);
                        VendorAttachment::create([
                            'v_id' => $ven->id,
                            'document' => $document
                        ]);
                    }
                }
            }

            if($request->opening_balance != null)
            {
                $gl = GeneralLedger::max('id');
                if($gl == null)
                {
                    $link_id = 1;
                }
                else
                {
                    $ledger = GeneralLedger::where('id',$gl)->first();
                    $link_id = $ledger->link_id + 1;
                }
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Opening Account',
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' =>  $posted_date,
                    'posted_date' =>  $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'currency_code' => 'PKR',
                    'debit' => $request->opening_balance,
                    'credit' => '0',
                    'net_value' => $request->opening_balance,
                    'balance' => $request->opening_balance
                ]);
            }
            $u_name = Auth::user()->name;
            $user = User::where('r_id',config('app.adminId'))->get();
            $data = [
                'notification' => 'New customer has been added by '.$u_name,
                'link' => url('').'/customer',
                'name' => 'View Customers',
            ];

            Notification::send($user, new AddNotification($data));
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Customer added successfully!';
                $response['ven'] = $ven;
                return response()->json($response, 200);
            }
            toastr()->success('Customer added successfully!');
            return redirect()->back();
        }

        else
        {
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Customer account already exist!';
                $ven = null;
                return response()->json($response, 200);
            }
            toastr()->error('Customer account already exist!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=Vendors::with(['city','cgroup','pgroup'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor=Vendors::with(['city','cgroup','pgroup','attachment'])
        ->where('id',$id)
        ->first();
        $city=City::where('status',1)->get();
        $cgroup=Groups::where('g_type','Customer')->get();
        $pgroup=Groups::where('g_type','Price')->get();
        $data= [
            'isEdit' => true,
            'city' => $city,
            'cgroup' => $cgroup,
            'pgroup' => $pgroup,
            'vendor' => $vendor,
            'permissions' => getRolePermission(44)
        ];
        return view('customer.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $customer = Vendors::find($id);
        $account = $customer->company. ' - '. $customer->name;
        $ledger = GeneralLedger::where('account_name',$account)
        ->first();
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');


        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            if($ledger == null)
            {
                AccountDetails::where('name_of_account',$account)
                ->update([
                    'name_of_account' => $request->company.' - '.$request->name
                ]);

                $account_c =AccountDetails::where('name_of_account',$request->company.' - '.$request->name)->first();
                $vendor=Vendors::where('id',$id)
                ->update([
                    'c_group' => $request->c_group,
                    'p_group' => $request->p_group,
                    'company' => $request->company,
                    'name' => $request->name,
                    'c_no' => $request->c_no,
                    'name2' => $request->name2,
                    'c_no2' => $request->c_no2,
                    'address' => $request->address,
                    'country' => $request->country,
                    'VAT' => $request->VAT,
                    'GST' => $request->GST,
                    'state' => $request->state,
                    'email' => $request->email,
                    'balance' => $request->balance,
                    'postalCode' => $request->postalCode,
                    'c_id' => $request->c_id,
                    'opening_balance' => $request->opening_balance,
                    'updated_by' => $u_id
                ]);
                if($request->opening_balance != null)
                {
                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id + 1;
                    }
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Opening Account',
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' =>  $posted_date,
                        'posted_date' =>  $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'currency_code' => 'PKR',
                        'debit' => $request->opening_balance,
                        'credit' => '0',
                        'net_value' => $request->opening_balance,
                        'balance' => $request->opening_balance
                    ]);
                }
                $va = VendorAttachment::where('v_id',$id)->get();
                if($va->isNotEmpty())
                {
                    VendorAttachment::where('v_id',$id)->delete();
                }
                if(isset($request->documents))
                {
                    foreach ($request->documents as $key => $value) {
                        VendorAttachment::create([
                            'v_id' => $id,
                            'document' => $value
                        ]);
                    }
                }
                if(isset($request->document))
                {
                    foreach ($request->document as $key => $d) {
                        if($d != null)
                        {
                            $document = '';
                            $document= Storage::disk('documents')->putFile('',$d);
                            VendorAttachment::create([
                                'v_id' => $id,
                                'document' => $document
                            ]);
                        }
                    }
                }
                toastr()->success('Customer updated successfully!');
                return redirect(url('').'/customer');
            }
            else
            {
                $ledger2 = GeneralLedger::where('account_name', $customer->company.' - '.$customer->name)->where('description','Opening Account')->first();
                $account_c =AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)->first();
                if($ledger2 == null)
                {
                    if($request->opening_balance != null)
                    {
                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger->link_id + 1;
                        }
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Opening Account',
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>  $posted_date,
                            'posted_date' =>  $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'currency_code' => 'PKR',
                            'debit' => $request->opening_balance,
                            'credit' => '0',
                            'net_value' => $request->opening_balance,
                            'balance' => $request->opening_balance
                        ]);
                    }
                }
                else {
                    DB::table('general_ledger')->where('id', $ledger2->id)->delete();
                    if($request->opening_balance != null)
                    {
                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger->link_id + 1;
                        }
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Opening Account',
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>  $posted_date,
                            'posted_date' =>  $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'currency_code' => 'PKR',
                            'debit' => $request->opening_balance,
                            'credit' => '0',
                            'net_value' => $request->opening_balance,
                            'balance' => $request->opening_balance
                        ]);
                    }
                }
                $vendor=Vendors::where('id',$id)
                ->update([
                    'c_group' => $request->c_group,
                    'p_group' => $request->p_group,
                    'c_no' => $request->c_no,
                    'name2' => $request->name2,
                    'c_no2' => $request->c_no2,
                    'address' => $request->address,
                    'country' => $request->country,
                    'VAT' => $request->VAT,
                    'GST' => $request->GST,
                    'state' => $request->state,
                    'email' => $request->email,
                    'balance' => $request->balance,
                    'postalCode' => $request->postalCode,
                    'c_id' => $request->c_id,
                    'opening_balance' => $request->opening_balance,
                    'updated_by' => $u_id
                ]);
                $va = VendorAttachment::where('v_id',$id)->get();
                if($va->isNotEmpty())
                {
                    VendorAttachment::where('v_id',$id)->delete();
                }
                if(isset($request->documents))
                {
                    foreach ($request->documents as $key => $value) {
                        VendorAttachment::create([
                            'v_id' => $id,
                            'document' => $value
                        ]);
                    }
                }
                if(isset($request->document))
                {
                    foreach ($request->document as $key => $d) {
                        if($d != null)
                        {
                            $document = '';
                            $document= Storage::disk('documents')->putFile('',$d);
                            VendorAttachment::create([
                                'v_id' => $id,
                                'document' => $document
                            ]);
                        }
                    }
                }
                toastr()->error('Customer updated successfully!');
                return redirect(url('').'/customer');
            }
        }
        else
        {
            // if($request->name != $customer->name || $request->company != $customer->company)
            // {
                if($ledger == null)
                {
                    VendorsUpdateRequest::create([
                        'id' => $id,
                        'c_group' => $request->c_group,
                        'p_group' => $request->p_group,
                        'company' => $request->company,
                        'name' => $request->name,
                        'c_no' => $request->c_no,
                        'name2' => $request->name2,
                        'c_no2' => $request->c_no2,
                        'address' => $request->address,
                        'country' => $request->country,
                        'VAT' => $request->VAT,
                        'GST' => $request->GST,
                        'state' => $request->state,
                        'email' => $request->email,
                        'balance' => $request->balance,
                        'postalCode' => $request->postalCode,
                        'c_id' => $request->c_id,
                        'v_type' => $request->v_type,
                    ]);
                    $va = VendorAttachment::where('v_id',$id)->get();
                    if($va->isNotEmpty())
                    {
                        VendorAttachment::where('v_id',$id)->delete();
                    }
                    if(isset($request->documents))
                    {
                        foreach ($request->documents as $key => $value) {
                            VendorAttachment::create([
                                'v_id' => $id,
                                'document' => $value
                            ]);
                        }
                    }
                    if(isset($request->document))
                    {
                        foreach ($request->document as $key => $d) {
                            if($d != null)
                            {
                                $document = '';
                                $document= Storage::disk('documents')->putFile('',$d);
                                VendorAttachment::create([
                                    'v_id' =>$id,
                                    'document' => $document
                                ]);
                            }
                        }
                    }
                    toastr()->success('Customer update request sent successfully successfully!');
                    return redirect(url('').'/customer');
                }
                else
                {
                    VendorsUpdateRequest::create([
                        'id' => $id,
                        'c_group' => $request->c_group,
                        'p_group' => $request->p_group,
                        'c_no' => $request->c_no,
                        'name2' => $request->name2,
                        'c_no2' => $request->c_no2,
                        'address' => $request->address,
                        'country' => $request->country,
                        'VAT' => $request->VAT,
                        'GST' => $request->GST,
                        'state' => $request->state,
                        'email' => $request->email,
                        'balance' => $request->balance,
                        'postalCode' => $request->postalCode,
                        'c_id' => $request->c_id,
                        'v_type' => $request->v_type,
                    ]);
                    $va = VendorAttachment::where('v_id',$id)->get();
                    if($va->isNotEmpty())
                    {
                        VendorAttachment::where('v_id',$id)->delete();
                    }
                    if(isset($request->documents))
                    {
                        foreach ($request->documents as $key => $value) {
                            VendorAttachment::create([
                                'v_id' => $id,
                                'document' => $value
                            ]);
                        }
                    }
                    if(isset($request->document))
                    {
                        foreach ($request->document as $key => $d) {
                            if($d != null)
                            {
                                $document = '';
                                $document= Storage::disk('documents')->putFile('',$d);
                                VendorAttachment::create([
                                    'v_id' =>$id,
                                    'document' => $document
                                ]);
                            }
                        }
                    }
                    toastr()->success('Customer Name and company is not allowed to update because its ledger is created!');
                    return redirect(url('').'/customer');
                }
            // }

        }


    }


    public function excel()
    {
        return Excel::download(new CustomerExport, 'customers.xlsx');
    }

    public function pdf()
    {
        $vendor =  Vendors::where('v_type','Customer')
        ->select('name','company','address','c_no')
        ->get();
        // dd($vendor);
        $pdf = PDF::loadView('customer.pdf', compact('vendor'));

        return $pdf->download('customers.pdf');
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Vendors::find($id);
        if ($item->update(['status' => $status])) {
            Vendors::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
