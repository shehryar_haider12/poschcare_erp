<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockOut;
use App\Products;
use App\Category;
use App\Brands;
use App\Vendors;
use App\Sales;
use App\SaleReturn;
use App\SaleCounter;
use App\Counter;
use App\SaleDetails;
use App\GeneralLedger;
use App\Bank;
use App\CurrentStock;
use App\AccountDetails;
use App\TransactionHistory;
use DB;
use Auth;
use Session;
use Carbon\Carbon;

class StockOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $stock=StockOut::with(['products.brands','warehouse','sdetails','products.category','products.unit','products','variant.product.category','variant.product.unit','variant.product.brands'])
        ->get();
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        return view('stocks.out',compact('product','stock'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function stockOutSearch(Request $request)
    {
        // dd($request->all());
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        if($request->optradio == 'Year')
        {
            if($request->p_id == null)
            {
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y'))"),$request->year)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null )
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'Month')
        {
            if($request->p_id == null)
            {
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$request->month)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                if($request->p_id == null)
                {
                    $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
            }
            else
            {
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }

                if($request->p_id == null)
                {
                    $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                    ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),$date)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                    ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
            }

        }
        else if($request->optradio == 'last24Hours')
        {
            if($request->p_id == null)
            {
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null )
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'lastweek')
        {
            $date = Carbon::today()->subDays(7);
            if($request->p_id == null)
            {
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where('stock_date','>=',$date)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null )
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where('stock_date','>=',$date)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'last15Days')
        {
            $date = Carbon::today()->subDays(15);
            if($request->p_id == null)
            {
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where('stock_date','>=',$date)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null )
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where('stock_date','>=',$date)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'lastMonth')
        {
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            if($request->p_id == null)
            {
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$month)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null )
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=StockOut::with(['variant.product.brands','products.brands','warehouse','sdetails','products.category','variant.product.category','products.unit','variant.product.unit','products'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }

        return view('stocks.out',compact('product','stock'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function PosInvoice($id)
    {
        $counters = Counter::find($id);
        $counter = $id;
        $userid = Auth::user()->id;
        $date = Carbon::now()->format('Y-m-d');
        $yesterday = Carbon::yesterday()->format('Y-m-d');
        $day = Carbon::now()->format('D');
        $totalSale = 0;
        $totalReturn = 0;
        $SaleAmount = 0;
        $ReturnAmount = 0;
        $discount = 0;
        $totalCashClosing = [];
        $totalCashOpening = 0;
        //yesterday sales
        $saleyes = SaleCounter::with('user')->where('u_id',$userid)->where('co_id',$counter)->where('date',$yesterday)->get();
        if(count($saleyes) > 0)
        {
            foreach ($saleyes as $key => $s) {
                if($s->s_id != null)
                {
                    $totalCashOpening = TransactionHistory::where('p_s_id',$s->s_id)->where('paid_by','Cash')->sum('total');
                }
            }
        }
        else {
            $totalCashOpening = 0;
        }
        //todays sales
        // $th = [];
        $sales = SaleCounter::with('user')->where('u_id',$userid)->where('co_id',$counter)->where('date',$date)->get();
        foreach ($sales as $key => $s) {
            if($s->s_id != null)
            {
                $sale = Sales::find($s->s_id);
                $totalSale +=1;
                $SaleAmount += $sale->total;
                if($sale->discount != null)
                {
                    $discount += $sale->discount;
                }
                $total_cash_price = TransactionHistory::where('p_s_id',$s->s_id)->where('paid_by','Cash')->pluck('total');
                // return $total_cash_price;
                if(count($total_cash_price) > 0 ){
                    $totalCashClosing[] = $total_cash_price[0];
                }
                $th= DB::select("SELECT merchant_type,sum(total) as total ,count(id) as id FROM `transaction_history` WHERE `created_at` LIKE '".$date."%' and paid_by='Credit Card' group by merchant_type");
            }
            if($s->ret != null)
            {
                $return = SaleReturn::find($s->ret_id);
                $totalReturn += 1;
                $ReturnAmount += $return->total;
            }

        }
        // dd($th);

        $saleCount = count($sales);
        return view('counter.oc_invoice',compact('th','totalCashOpening','totalCashClosing','counter','sales','date','day','counters','saleCount','totalSale','discount','totalReturn','SaleAmount','ReturnAmount'));
    }


    public function store(Request $request)
    {
        // dd($request->all());
        $amount = 0;
        $sid=0;
        $counter = Session::get('assign');
        $w_id = Auth::user()->w_id;
        $u_id = Auth::user()->id;
        $s_status = 'Delivered';
        $sale_date = Carbon::now()->format('Y-m-d');
        $p_status = 'Paid';
        if (isset($request->bank_id)) {
            $bank_id = $request->bank_id;
        } else {
            $bank_id =null;
        }

        if (isset($request->tax)) {
            $tax = $request->tax;
            $tax_status = 'Yes';
        } else {
            $tax =null;
            $tax_status = 'No';
        }
        if (isset($request->discount)) {
            $discount = $request->discount;
        } else {
            $discount =null;
        }
        // subSaleQty($request->all());

        $data=([
            'sale_date' => $sale_date,
            'b_id' => $request->b_id,
            'w_id' => $w_id,
            's_status' => $s_status,
            'p_status' => $p_status,
            'tax' => $tax,
            'total' => $request->actual,
            'created_by' => $u_id,
            'c_id' => $request->c_id,
            'advance' => 'No',
            'tax_status' => $tax_status,
            'pay_type' => 'Cash On Hand',
            'return_status' => 'No Return',
            's_type' => $request->s_type,
            // 'c_name' => $request->c_name,
            'return_amount' => $request->return_amount,
        ]);
        $p=Sales::create($data);
        // SaleCounter::create([
        //     'u_id' => $u_id,
        //     'co_id' => 1,
        //     's_id' => $p->id,
        //     'date' => $sale_date,
        // ]);
        for ($i=0; $i < count($request->p_id)  ; $i++)
        {
            $sid = SaleDetails::create([
                's_id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'sub_total' => $request->sub_total[$i],
                'price' => $request->price[$i],
                'discount_percent' => $request->discount_percent[$i],
                'discounted_amount' => $request->discounted_amount[$i],
                'type' => 0,
                'delivered_quantity' => $request->quantity[$i],
            ]);

            // dd($sid);
            $product = Products::with('unit')
            ->where('id',$request->p_id[$i])
            ->first();

            $prod=CurrentStock::where('p_id',$request->p_id[$i])
            ->where('w_id',$w_id)
            ->where('type',0)
            ->where('unit_quantity','>=',0)
            // ->orderBy('exp_date','asc')
            ->get();
            $finalqty = 0;
            foreach ($prod as $key => $minus) {

                if($minus->unit_quantity >= $request->quantity[$i] )
                {
                    if($finalqty > 0)
                    {
                        $quan=$minus->quantity;
                        $u_quan=$minus->unit_quantity - $finalqty;
                        $amount += $request->sub_total[$i];
                        CurrentStock::where('p_id',$request->p_id[$i])
                        ->where('w_id',$w_id)
                        ->where('type',0)
                        ->where('id',$minus->id)
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                        StockOut::create([
                            'p_id' => $request->p_id[$i],
                            'quantity' => $finalqty,
                            'sale_d_id' => $sid->id,
                            'stock_date' => $sale_date,
                            's_type' => 'Sales',
                            'price' => $request->price[$i],
                            'w_id' => $w_id,
                            'created_by' => $u_id,
                            // 'exp_date' => $minus->exp_date,
                            'type' => 0
                        ]);
                        $finalqty = 0;
                        break;
                    }
                    else{

                        $quan=$minus->quantity;
                        $u_quan=$minus->unit_quantity - $request->quantity[$i];
                        $amount += $request->sub_total[$i];
                        CurrentStock::where('p_id',$request->p_id[$i])
                        ->where('w_id',$w_id)
                        ->where('type',0)
                        ->where('id',$minus->id)
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                        StockOut::create([
                            'p_id' => $request->p_id[$i],
                            'quantity' => $request->quantity[$i],
                            'sale_d_id' => $sid->id,
                            'stock_date' => $sale_date,
                            's_type' => 'Sales',
                            'price' => $request->price[$i],
                            'w_id' => $w_id,
                            'created_by' => $u_id,
                            // 'exp_date' => $minus->exp_date,
                            'type' => 0
                        ]);
                        $finalqty = 0;
                        break;
                    }
                }
                else
                {
                    $finalqty = $request->quantity[$i];
                    if($finalqty > 0)
                    {
                        $quan=$minus->quantity;
                        $u_quan=$minus->unit_quantity - $finalqty;
                        $amount += $request->sub_total[$i];
                        CurrentStock::where('p_id',$request->p_id[$i])
                        ->where('w_id',$w_id)
                        ->where('type',0)
                        ->where('id',$minus->id)
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                        StockOut::create([
                            'p_id' => $request->p_id[$i],
                            'quantity' => $minus->quantity,
                            'sale_d_id' => $sid->id,
                            'stock_date' => $sale_date,
                            's_type' => 'Sales',
                            'price' => $request->price[$i],
                            'w_id' => $w_id,
                            'created_by' => $u_id,
                            // 'exp_date' => $minus->exp_date,
                            'type' => 0
                        ]);
                        $finalqty = $request->quantity[$i] - $minus->unit_quantity;
                    }
                }

            }

        }

        // $gl = GeneralLedger::max('id');
        // $posted_date = Carbon::now()->format('Y-m-d');
        // $period = Carbon::now()->format('M-y');
        // if($gl == null)
        // {
        //     $link_id = 1;
        // }
        // else
        // {
        //     $ledger = GeneralLedger::where('id',$gl)->first();
        //     $link_id = $ledger->link_id + 1;
        // }
        // $customer=Vendors::find($request->c_id);
        // $cash_c = 0;
        // $cash_d = 0;
        // $cash_n = 0;
        // $bank_c = 0;
        // $bank_d = 0;
        // $bank_n = 0;
        // $customer_c = 0;
        // $customer_d = 0;
        // $customer_n = 0;

        // $cashc_c = 0;
        // $cashc_d = 0;
        // $cashc_n = 0;
        // $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
        // ->first();
        // $receivable = GeneralLedger::where('account_code',$account_c->Code)
        // ->get();
        // $account_s = AccountDetails::where('name_of_account','Sales')->first();
        // $sales = GeneralLedger::where('account_code',$account_s->Code)
        // ->get();

        // $sales_n = 0;
        // $sales_d = 0;
        // $sales_c = 0;
        // if($sales->isEmpty())
        // {
        //     GeneralLedger::create([
        //         'source' => 'Automated',
        //         'description' => 'Sales to '.$customer->name,
        //         'account_name' => $account_s->name_of_account,
        //         'link_id' => $link_id,
        //         'created_by' => $u_id,
        //         'accounting_date' => $posted_date,
        //         'posted_date' => $posted_date,
        //         'period' => $period,
        //         'account_code' => $account_s->Code,
        //         // 'transaction_no' => $gdn_no,
        //         'currency_code' => 'PKR',
        //         'debit' => '0',
        //         'credit' => $request->actual,
        //         'net_value' =>  0 - $request->actual,
        //         'balance' =>  0 - $request->actual
        //     ]);
        // }
        // else
        // {
        //     $balance = 0;
        //     foreach ($sales as $key => $d) {
        //         $sales_d+=$d->debit;
        //         $sales_c+=$d->credit;
        //         $balance+=$d->net_value;
        //     }
        //     $sales_c += $request->actual;
        //     // $sales_n = $sales_d - $sales_c;
        //     $sales_n = $balance + (0 - $request->actual);

        //     GeneralLedger::create([
        //         'source' => 'Automated',
        //         'description' => 'Sales to '.$customer->name,
        //         'account_name' => $account_s->name_of_account,
        //         'link_id' => $link_id,
        //         'created_by' => $u_id,
        //         'accounting_date' => $posted_date,
        //         'posted_date' => $posted_date,
        //         'period' => $period,
        //         'account_code' => $account_s->Code,
        //         // 'transaction_no' => $gdn_no,
        //         'currency_code' => 'PKR',
        //         'debit' => '0',
        //         'credit' => $request->actual,
        //         'net_value' => 0 - $request->actual,
        //         'balance' => $sales_n
        //     ]);
        // }

        // if($receivable->isEmpty())
        // {
        //     GeneralLedger::create([
        //         'source' => 'Automated',
        //         'description' => 'Sales to'.$customer->name,
        //         'account_name' => $account_c->name_of_account,
        //         'link_id' => $link_id,
        //         'created_by' => $u_id,
        //         'accounting_date' => $posted_date,
        //         'posted_date' => $posted_date,
        //         'period' => $period,
        //         'account_code' => $account_c->Code,
        //         // 'transaction_no' => $gdn_no,
        //         'currency_code' => 'PKR',
        //         'debit' => $request->actual,
        //         'credit' => '0',
        //         'net_value' => $request->actual,
        //         'balance' => $request->actual
        //     ]);
        // }
        // else
        // {
        //     $balance = 0;
        //     foreach ($receivable as $key => $c) {
        //         $customer_c+=$c->credit;
        //         $customer_d+=$c->debit;
        //         $balance+=$c->net_value;
        //     }
        //     $customer_d += $request->actual;
        //     // $customer_n = $customer_d - $customer_c;
        //     $customer_n = $balance + ($request->actual - 0);

        //     GeneralLedger::create([
        //         'source' => 'Automated',
        //         'description' => 'Sales to '.$customer->name,
        //         'account_name' => $account_c->name_of_account,
        //         'link_id' => $link_id,
        //         'created_by' => $u_id,
        //         'accounting_date' => $posted_date,
        //         'posted_date' => $posted_date,
        //         'period' => $period,
        //         'account_code' => $account_c->Code,
        //         // 'transaction_no' => $gdn_no,
        //         'currency_code' => 'PKR',
        //         'debit' => $request->actual,
        //         'credit' => '0',
        //         'net_value' => $request->actual,
        //         'balance' => $customer_n
        //     ]);
        // }

        for ($i=0; $i <count($request->paid_by) ; $i++) {
            $ref_no = TransactionHistory::create([
                'p_s_id' =>  $p->id,
                'p_type' => 'Sales',
                't_type' => 'Received',
                'paid_by' => $request->paid_by[$i],
                'total' => $request->total[$i],
                'cc_no' => $request->cc_no,
                'cc_holder' => $request->cc_holder,
                'merchant_type' => $request->merchant_type,
                'b_id' => $bank_id,
                'created_by' => $u_id
            ]);


            // if($request->paid_by[$i] == 'Cash')
            // {
            //     if($counter != null)
            //     {
            //         $account_cash_c = AccountDetails::where('name_of_account','Cash Counter '.$counter)->first();
            //         $cashc = GeneralLedger::where('account_code',$account_cash_c->Code)
            //         ->get();
            //         if($cashc->isEmpty())
            //         {
            //             GeneralLedger::create([
            //                 'source' => 'Automated',
            //                 'description' => 'Amount Paid by: '.$customer->name,
            //                 'account_name' => $account_cash_c->name_of_account,
            //                 'link_id' => $link_id,
            //                 'created_by' => $u_id,
            //                 'accounting_date' => $posted_date,
            //                 'posted_date' => $posted_date,
            //                 'period' => $period,
            //                 'account_code' => $account_cash_c->Code,
            //                 'transaction_no' => $ref_no->id,
            //                 'currency_code' =>'PKR',
            //                 'debit' => $request->total[$i],
            //                 'credit' => '0',
            //                 'net_value' => $request->total[$i] - 0,
            //                 'balance' => $request->total[$i] - 0,
            //             ]);
            //         }
            //         else
            //         {
            //             $balance = 0;
            //             foreach ($cashc as $key => $c) {
            //                 $cashc_c+=$c->credit;
            //                 $cashc_d+=$c->debit;
            //                 $balance+=$c->net_value;
            //             }
            //             $cashc_d += $request->total[$i];
            //             $cashc_n = $balance + ($request->total[$i] - 0);

            //             GeneralLedger::create([
            //                 'source' => 'Automated',
            //                 'description' => 'Amount Paid by: '.$customer->name,
            //                 'account_name' => $account_cash_c->name_of_account,
            //                 'link_id' => $link_id,
            //                 'created_by' => $u_id,
            //                 'accounting_date' => $posted_date,
            //                 'posted_date' => $posted_date,
            //                 'period' => $period,
            //                 'account_code' => $account_cash_c->Code,
            //                 'transaction_no' => $ref_no->id,
            //                 'currency_code' =>'PKR',
            //                 'debit' => $request->total[$i],
            //                 'credit' => '0',
            //                 'net_value' => $request->total[$i] - 0,
            //                 'balance' => $cashc_n
            //             ]);
            //         }
            //     }

            //     $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
            //     $cash = GeneralLedger::where('account_code',$account_cash->Code)
            //     ->get();
            //     if($cash->isEmpty())
            //     {
            //         GeneralLedger::create([
            //             'source' => 'Automated',
            //             'description' => 'Amount Paid by: '.$customer->name,
            //             'account_name' => $account_cash->name_of_account,
            //             'link_id' => $link_id,
            //             'created_by' => $u_id,
            //             'accounting_date' => $posted_date,
            //             'posted_date' => $posted_date,
            //             'period' => $period,
            //             'account_code' => $account_cash->Code,
            //             'transaction_no' => $ref_no->id,
            //             'currency_code' =>'PKR',
            //             'debit' => $request->total[$i],
            //             'credit' => '0',
            //             'net_value' => $request->total[$i] - 0,
            //             'balance' => $request->total[$i] - 0,
            //         ]);
            //     }
            //     else
            //     {
            //         $balance = 0;
            //         foreach ($cash as $key => $c) {
            //             $cash_c+=$c->credit;
            //             $cash_d+=$c->debit;
            //             $balance+=$c->net_value;
            //         }
            //         $cash_d += $request->total[$i];
            //         $cash_n = $balance + ($request->total[$i] - 0);

            //         GeneralLedger::create([
            //             'source' => 'Automated',
            //             'description' => 'Amount Paid by: '.$customer->name,
            //             'account_name' => $account_cash->name_of_account,
            //             'link_id' => $link_id,
            //             'created_by' => $u_id,
            //             'accounting_date' => $posted_date,
            //             'posted_date' => $posted_date,
            //             'period' => $period,
            //             'account_code' => $account_cash->Code,
            //             'transaction_no' => $ref_no->id,
            //             'currency_code' =>'PKR',
            //             'debit' => $request->total[$i],
            //             'credit' => '0',
            //             'net_value' => $request->total[$i] - 0,
            //             'balance' => $cash_n
            //         ]);
            //     }
            // }
            // else
            // {
            //     $bank=Bank::find($bank_id);
            //     $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
            //     $bank_l = GeneralLedger::where('account_code',$account_b->Code)
            //     ->get();
            //     if($bank_l->isEmpty())
            //     {
            //         GeneralLedger::create([
            //             'source' => 'Automated',
            //             'description' => 'Amount Paid By: '.$customer->name,
            //             'account_name' => $account_b->name_of_account,
            //             'link_id' => $link_id,
            //             'created_by' => $u_id,
            //             'accounting_date' => $posted_date,
            //             'posted_date' => $posted_date,
            //             'period' => $period,
            //             'account_code' => $account_b->Code,
            //             'transaction_no' => $ref_no->id,
            //             'currency_code' =>'PKR',
            //             'debit' => $request->total[$i],
            //             'credit' => '0',
            //             'net_value' => $request->total[$i] - 0,
            //             'balance' => $request->total[$i] - 0
            //         ]);
            //     }
            //     else
            //     {
            //         $balance = 0;
            //         foreach ($bank_l as $key => $c) {
            //             $bank_c+=$c->credit;
            //             $bank_d+=$c->debit;
            //             $balance+=$c->net_value;
            //         }
            //         $bank_d += $request->total[$i];
            //         $bank_n = $balance + ($request->total[$i]);

            //         GeneralLedger::create([
            //             'source' => 'Automated',
            //             'description' => 'Amount Paid By: '.$customer->name,
            //             'account_name' => $account_b->name_of_account,
            //             'link_id' => $link_id,
            //             'created_by' => $u_id,
            //             'accounting_date' => $posted_date,
            //             'posted_date' => $posted_date,
            //             'period' => $period,
            //             'account_code' => $account_b->Code,
            //             'transaction_no' => $ref_no->id,
            //             'currency_code' =>'PKR',
            //             'debit' => $request->total[$i],
            //             'credit' => '0',
            //             'net_value' => $request->total[$i] - 0,
            //             'balance' => $bank_n
            //         ]);
            //     }
            // }

        }


        // for ($k=0; $k < count($request->p_id) ; $k++)
        // {
        //     $product = Products::find($request->p_id[$k]);
        //     $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
        //     ->first();
        //     // dd($account_i);
        //     $balance = 0;
        //     $net_d_p = 0;
        //     $debit_p = GeneralLedger::where('account_code',$account_i->Code)
        //     ->where('w_id',$w_id)
        //     ->get();
        //     // dd($debit_p);
        //     if($debit_p->isEmpty())
        //     {
        //         GeneralLedger::create([
        //             'source' => 'Automated',
        //             'description' => 'Sale product for sale number: '.$p->id,
        //             'account_name' => $account_i->name_of_account,
        //             'link_id' => $link_id,
        //             'created_by' => $u_id,
        //             'accounting_date' => $posted_date,
        //             'posted_date' => $posted_date,
        //             'period' => $period,
        //             'account_code' => $account_i->Code,
        //             // 'transaction_no' => $gdn_no,
        //             'currency_code' => 'PKR',
        //             'stock_out' => $request->quantity[$k],
        //             'stock_in' => '0',
        //             'net_value' => 0 - $request->quantity[$k],
        //             'balance' => 0 - $request->quantity[$k],
        //             'credit' =>  $product->cost * $request->quantity[$k],
        //             'debit' => 0,
        //             'w_id' => $w_id,
        //             'type' => 0,
        //             'amount' => 0 - $product->cost
        //         ]);
        //     }
        //     else
        //     {
        //         foreach ($debit_p as $key => $c) {
        //             $balance+=$c->net_value;
        //         }
        //         $net_d_p = $balance + ( 0 - $request->quantity[$k] );
        //         GeneralLedger::create([
        //             'source' => 'Automated',
        //             'description' => 'Sale product for sale number: '.$p->id,
        //             'account_name' => $account_i->name_of_account,
        //             'link_id' => $link_id,
        //             'created_by' => $u_id,
        //             'accounting_date' => $posted_date,
        //             'posted_date' => $posted_date,
        //             'period' => $period,
        //             'account_code' => $account_i->Code,
        //             // 'transaction_no' => $gdn_no,
        //             'currency_code' => 'PKR',
        //             'stock_out' => $request->quantity[$k],
        //             'stock_in' => '0',
        //             'net_value' => 0 - $request->quantity[$k],
        //             'balance' => $net_d_p,
        //             'credit' => $product->cost * $request->quantity[$k],
        //             'debit' => 0,
        //             'w_id' => $w_id ,
        //             'type' => 0,
        //             'amount' => 0 - $product->cost
        //         ]);
        //     }

        // }


        return redirect(url('').'/stockout/pdf/'.$p->id);
    }

    public function pdf($id)
    {
        $credit = 0;
        $debit = 0;
        $sales=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$id)
        ->first();
        $saledetail=SaleDetails::with(['products','products.brands','products.unit','sale','variant','variant.product.brands','variant.product.unit'])
        ->where('s_id',$id)
        ->get();
        $th=TransactionHistory::where('p_s_id',$id)
        ->where('p_type','Sales')
        ->get();
        $account=AccountDetails::with('generalLedger')->where('name_of_account','Like','%'.$sales->customer->name)
        ->where('Code','like','CA-01%')
        ->first();
        if($account!=null)
        {
            for($k = 0 ; $k < count($account->generalLedger) ; $k++)
            {
                $credit += $account->generalLedger[$k]->credit;

                $debit += $account->generalLedger[$k]->debit;
            }
            $balance =  $debit - $credit;
        }
        else {
            $balance = 0;
        }
        // dd($th);
        // $pdf = PDF::loadView('sales.pdf', compact('sales','saledetail','th','balance'));
        return view('sales.posInvoice', compact('sales','saledetail','th','balance'));

        // return $pdf->download('sales.pdf');
        // return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
