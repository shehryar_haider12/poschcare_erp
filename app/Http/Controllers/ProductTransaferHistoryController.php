<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Warehouse;
use App\Products;
use App\Brands;
use App\Category;
use App\CurrentStock;
use App\ProductTransaferHistory;
use App\User;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use Auth;
use Carbon\Carbon;
use App\Exports\ProductTransferHistoryExport;
use App\Exports\ProductTransferHistoryYearExport;
use App\Exports\ProductTransferHistoryMonthExport;
use App\Exports\ProductTransferHistoryDateExport;
use App\Exports\ProductTransferHistoryDatesExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class ProductTransaferHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $warehouse=Warehouse::all();
        $products=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        $brand=Brands::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands'])
        ->get();
        // return $product;
        $index = 0;
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('transfer.index',compact('products','menu_id','permissions','permissions','warehouse','brand','cat','product','index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date=Carbon::now()->format('Y-m-d');
        $warehouse=Warehouse::all();
        $data=[
            'warehouse' => $warehouse,
            'date' => $date
        ];
        return view('transfer.create',$data);
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $status = 1;

        $item = ProductTransaferHistory::find($id);
        $product = Products::with('unit')
        ->where('id',$item->p_id)
        ->first();

        $cs=CurrentStock::where('p_id',$item->p_id)
        ->where('w_id',$item->from_w)
        ->first();

        $cs1=CurrentStock::where('p_id',$item->p_id)
        ->where('w_id',$item->to_w)
        ->first();

        if($request->inhouse < $request->quantity)
        {
            return response()->json($response, 409);
        }
        else
        {
            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
            {
                $unit_quantity = ($product->weight * 1000) * $request->quantity;
                $unit_quantity1 = $cs->unit_quantity - $unit_quantity;
            }
            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
            {
                $unit_quantity = $product->weight * $request->quantity;
                $unit_quantity1 = $cs->unit_quantity - $unit_quantity;
            }
            else
            {
                $unit_quantity = null;
                $unit_quantity1 = null;
            }
            $quan = $cs->quantity - $request->quantity;
            CurrentStock::where('p_id',$request->p_id)
            ->where('w_id',$request->from_w)
            ->update([
                'quantity' => $quan,
                'unit_quantity' => $unit_quantity1
            ]);
            if($cs1 == null)
            {
                CurrentStock::create([
                    'p_id' => $request->p_id,
                    'w_id' => $request->to_w,
                    'quantity' => $request->quantity,
                    'unit_quantity' => $unit_quantity
                ]);
            }
            else
            {
                $quan1 = $cs1->quantity + $request->quantity;
                // dd($quan1);
                if($unit_quantity == null)
                {
                    $u_quan=null;
                }
                else
                {
                    $u_quan = $cs1->unit_quantity + $unit_quantity;
                }
                CurrentStock::where('p_id',$request->p_id)
                ->where('w_id',$request->to_w)
                ->update([
                    'quantity' => $quan1,
                    'unit_quantity' => $u_quan
                ]);
            }

            ProductTransaferHistory::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $u_id = Auth::user()->id;
        // dd($request->all());

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }

        if($status == 1)
        {
            if($request->inhouse < $request->quantity)
            {
                toastr()->danger('Product cannot be transfered as in requested warehouse quantity of a product is not enough!');
                return redirect()->back();
            }
            else
            {
                $id = substr($request->p_id,0,strpos($request->p_id,'.'));
                // dd($id);

                if($type==1)
                {
                    $product = ProductVariants::with(['product.unit'])->where('id',$id)->first();
                    $unit = $product->product->unit->u_name;
                }
                else {
                    $product = Products::with('unit')
                    ->where('id',$id)
                    ->first();
                    $unit = $product->unit->u_name;
                }
                $cs=CurrentStock::where('p_id',$id)
                ->where('w_id',$request->w_id)
                ->where('type',$type)
                ->first();

                if($unit== 'Liter' || $unit== 'Kilograms')
                {
                    $unit_quantity = (1000) * $request->quantity;
                    $unit_quantity1 = $cs->unit_quantity - $unit_quantity;
                }
                else if($unit== 'Mililiter' || $unit == 'Grams')
                {
                    $unit_quantity = $request->quantity;
                    $unit_quantity1 = $cs->unit_quantity - $unit_quantity;
                }
                else
                {
                    $unit_quantity = null;
                    $unit_quantity1 = null;
                }
                $quan = $cs->quantity - $request->quantity;
                CurrentStock::where('p_id',$id)
                ->where('w_id',$request->from_w)
                ->where('type',$request->type)
                ->update([
                    'quantity' => $quan,
                    'unit_quantity' => $unit_quantity1
                ]);
                if($cs1 == null)
                {
                    CurrentStock::create([
                        'p_id' => $id,
                        'w_id' => $request->to_w,
                        'quantity' => $request->quantity,
                        'unit_quantity' => $unit_quantity,
                        'type' => $request->type,
                    ]);
                }
                else
                {
                    $quan1 = $cs1->quantity + $request->quantity;
                    // dd($quan1);
                    if($unit_quantity == null)
                    {
                        $u_quan=null;
                    }
                    else
                    {
                        $u_quan = $cs1->unit_quantity + $unit_quantity;
                    }
                    CurrentStock::where('p_id',$id)
                    ->where('w_id',$request->to_w)
                    ->where('type',$request->type)
                    ->update([
                        'quantity' => $quan1,
                        'unit_quantity' => $u_quan
                    ]);
                }

                ProductTransaferHistory::create([
                    'transfer_date' => $request->transfer_date,
                    'from_w' => $request->from_w,
                    'to_w' => $request->to_w,
                    'p_id' => $id,
                    'quantity' => $request->quantity,
                    'created_by' => $u_id,
                    'status' => $status,
                    'type' => $request->type,
                ]);
                $u_name = Auth::user()->name;
                $from = Warehouse::find($request->from_w);
                $to = Warehouse::find($request->to_w);
                $user = User::where('r_id',config('app.adminId'))->get();
                $data1 = [
                    'notification' => 'Product transfer from '.$from.' to'.$to.' by '.$u_name,
                    'link' => url('').'/productTransfer',
                    'name' => 'View Product Transfer History',
                ];
                Notification::send($user, new AddNotification($data1));
                toastr()->success('Product transfered successfully!');
                return redirect()->back();
            }
        }
        else
        {
            ProductTransaferHistory::create([
                'transfer_date' => $request->transfer_date,
                'from_w' => $request->from_w,
                'to_w' => $request->to_w,
                'p_id' => $request->p_id,
                'quantity' => $request->quantity,
                'created_by' => $u_id,
                'status' => $status,
                'type' => $request->type,
            ]);
            $u_name = Auth::user()->name;
            $from = Warehouse::find($request->from_w);
            $to = Warehouse::find($request->to_w);
            $user = User::where('r_id',config('app.adminId'))->get();
            $data1 = [
                'notification' => 'Product transfer request is added from by '.$u_name,
                'link' => url('').'/productTransfer',
                'name' => 'View Product Transfer History',
            ];
            Notification::send($user, new AddNotification($data1));
            toastr()->success('Product transfered successfully!');
            return redirect()->back();
        }


    }

    public function product(Request $request)
    {
        // dd($request->all());
        $cs=CurrentStock::where('p_id',$request->p_id)
        ->where('w_id',$request->w_id)
        ->where('type',$request->type)
        ->first();
        return $cs;
    }

    public function pdf()
    {
        $ptransfer=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
        ->get();
        $pdf = PDF::loadView('transfer.pdf', compact('ptransfer'));

        return $pdf->download('ProductTransferHistoryReport.pdf');
    }

    public function excel()
    {
        return Excel::download(new ProductTransferHistoryExport, 'ProductTransferHistoryReport.xlsx');
    }

    public function SearchYExcel($year,$p_name,$fw,$tw,$types)
    {
        return Excel::download(new ProductTransferHistoryYearExport($year,$p_name,$fw,$tw,$types), 'ProductTransferHistoryYearly.xlsx');
    }

    public function SearchMExcel($month,$p_name,$fw,$tw,$types)
    {
        return Excel::download(new ProductTransferHistoryMonthExport($month,$p_name,$fw,$tw,$types), 'ProductTransferHistoryMonthly.xlsx');
    }

    public function SearchDExcel($date,$p_name,$fw,$tw,$types)
    {
        return Excel::download(new ProductTransferHistoryDateExport($date,$p_name,$fw,$tw,$types), 'ProductTransferHistoryDate.xlsx');
    }

    public function SearchDatesExcel($from,$to,$p_name,$fw,$tw,$types)
    {
        return Excel::download(new ProductTransferHistoryDatesExport($from,$to,$p_name,$fw,$tw,$types), 'ProductTransferHistoryDateDifference.xlsx');
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        $warehouse=Warehouse::all();
        $products=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        $brand=Brands::all();
        $cat=Category::all();
        if($request->optradio == 'Year')
        {
            if($request->p_name == null && $request->tw == null && $request->fw == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = '0';
                $tw = '0';
            }
            if($request->p_name != null && $request->tw != null && $request->fw != null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('from_w',$request->fw)
                ->where('to_w',$request->tw)
                ->where('p_id',$request->p_name)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = $request->fw;
                $tw = $request->tw;
            }
            if($request->p_name == null && $request->fw != null  && $request->tw == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('from_w',$request->fw)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = $request->fw;
                $tw = '0';
            }
            if($request->tw != null && $request->fw == null && $request->p_name == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('to_w',$request->tw)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = '0';
                $tw = $request->tw;
            }
            if($request->tw == null && $request->fw == null && $request->p_name != null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = '0';
                $tw = '0';
            }

            if($request->p_name != null && $request->fw != null  && $request->tw == null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('from_w',$request->fw)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = $request->fw;
                $tw = '0';
            }
            if($request->tw != null && $request->fw == null && $request->p_name != null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('to_w',$request->tw)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = '0';
                $tw = $request->tw;
            }
            if($request->tw != null && $request->fw != null && $request->p_name == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y'))"),$request->year)
                ->where('from_w',$request->fw)
                ->where('to_w',$request->tw)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = $request->fw;
                $tw = $request->tw;
            }

            $index = 1;
            $year = $request->year;
            return view('transfer.index',compact('types','products','warehouse','brand','cat','product','index','year','p_name','fw','tw','permissions','menu_id'));
        }
        else if($request->optradio == 'Month')
        {
            if($request->p_name == null && $request->tw == null && $request->fw == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name ='0';
                $types = '0';
                $fw = '0';
                $tw = '0';
            }
            if($request->p_name != null && $request->tw != null && $request->fw != null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('from_w',$request->fw)
                ->where('to_w',$request->tw)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = $request->fw;
                $tw = $request->tw;
            }
            if($request->p_name == null && $request->fw != null  && $request->tw == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('from_w',$request->fw)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = $request->fw;
                $tw = '0';
            }
            if($request->tw != null && $request->fw == null && $request->p_name == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('to_w',$request->tw)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = '0';
                $tw = $request->tw;
            }
            if($request->tw == null && $request->fw == null && $request->p_name != null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types=$type;
                $fw = '0';
                $tw = '0';
            }

            if($request->p_name != null && $request->fw != null  && $request->tw == null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('from_w',$request->fw)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = $request->fw;
                $tw = '0';
            }
            if($request->tw != null && $request->fw == null && $request->p_name != null)
            {
                $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('to_w',$request->tw)
                ->where('p_id',$pid)
                ->where('type',$type)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = $pid;
                $types = $type;
                $fw = '0';
                $tw = $request->tw;
            }
            if($request->tw != null && $request->fw != null && $request->p_name == null)
            {
                $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m'))"),$request->month)
                ->where('from_w',$request->fw)
                ->where('to_w',$request->tw)
                ->orderBy('transfer_date','desc')
                ->get();
                $p_name = '0';
                $types = '0';
                $fw = $request->fw;
                $tw = $request->tw;
            }
            $index = 2;
            $month = $request->month;
            return view('transfer.index',compact('types','products','warehouse','brand','cat','product','index','month','p_name','fw','tw','permissions','menu_id'));

        }

        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                $index = 3;
                // ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                if($request->p_name == null && $request->tw == null && $request->fw == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $fw = '0';
                    $types = '0';
                    $tw = '0';
                }
                if($request->p_name != null && $request->tw != null && $request->fw != null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('from_w',$request->fw)
                    ->where('to_w',$request->tw)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = $request->fw;
                    $tw = $request->tw;
                }
                if($request->p_name == null && $request->fw != null  && $request->tw == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('from_w',$request->fw)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $types = '0';
                    $fw = $request->fw;
                    $tw = '0';
                }
                if($request->tw != null && $request->fw == null && $request->p_name == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('to_w',$request->tw)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $types = '0';
                    $fw = '0';
                    $tw = $request->tw;
                }
                if($request->tw == null && $request->fw == null && $request->p_name != null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = '0';
                    $tw = '0';
                }

                if($request->p_name != null && $request->fw != null  && $request->tw == null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('from_w',$request->fw)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = $request->fw;
                    $tw = '0';
                }
                if($request->tw != null && $request->fw == null && $request->p_name != null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('to_w',$request->tw)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = '0';
                    $tw = $request->tw;
                }
                if($request->tw != null && $request->fw != null && $request->p_name == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('from_w',$request->fw)
                    ->where('to_w',$request->tw)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $types = '0';
                    $fw = $request->fw;
                    $tw = $request->tw;
                }
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                if($request->p_name == null && $request->tw == null && $request->fw == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $fw = '0';
                    $types = '0';
                    $tw = '0';
                }
                if($request->p_name != null && $request->tw != null && $request->fw != null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('from_w',$request->fw)
                    ->where('to_w',$request->tw)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = $request->fw;
                    $tw = $request->tw;
                }
                if($request->p_name == null && $request->fw != null  && $request->tw == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('from_w',$request->fw)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $types = '0';
                    $fw = $request->fw;
                    $tw = '0';
                }
                if($request->tw != null && $request->fw == null && $request->p_name == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('to_w',$request->tw)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $types = '0';
                    $fw = '0';
                    $tw = $request->tw;
                }
                if($request->tw == null && $request->fw == null && $request->p_name != null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = '0';
                    $tw = '0';
                }

                if($request->p_name != null && $request->fw != null  && $request->tw == null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('from_w',$request->fw)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = $request->fw;
                    $tw = '0';
                }
                if($request->tw != null && $request->fw == null && $request->p_name != null)
                {
                    $pid = substr($request->p_name,0,strpos($request->p_name,'-'));
                    $type = substr($request->p_name,strpos($request->p_name,'-')+1);
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('to_w',$request->tw)
                    ->where('p_id',$pid)
                    ->where('type',$type)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = $pid;
                    $types = $type;
                    $fw = '0';
                    $tw = $request->tw;
                }
                if($request->tw != null && $request->fw != null && $request->p_name == null)
                {
                    $product=ProductTransaferHistory::with(['products.brands','fromWarehouse','toWarehouse','currentStock','variant.product.brands','products.category','products.unit','variant.product.category','variant.product.unit'])
                    ->where(DB::raw("(DATE_FORMAT(transfer_date,'%Y-%m-%d'))"),$date)
                    ->where('from_w',$request->fw)
                    ->where('to_w',$request->tw)
                    ->orderBy('transfer_date','desc')
                    ->get();
                    $p_name = '0';
                    $types = '0';
                    $fw = $request->fw;
                    $tw = $request->tw;
                }
            }
            if($index == 3)
            {
                return view('transfer.index',compact('types','products','warehouse','brand','cat','product','index','from','to','p_name','fw','tw','permissions','menu_id'));
            }
            if($index == 4)
            {
                return view('transfer.index',compact('types','products','warehouse','brand','cat','product','date','p_name','fw','tw','permissions','menu_id'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
