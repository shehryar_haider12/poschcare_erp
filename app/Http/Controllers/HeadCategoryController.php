<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeadCategory;
use App\HeadofAccounts;
use App\GeneralLedger;
use App\User;
use App\AccountDetails;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HeadHistoryExport;
use App\Exports\HeadHistoryYearlyExport;
use App\Exports\HeadHistoryMonthlyExport;
use App\Exports\HeadHistoryDateExport;
use App\Exports\HeadHistoryDateDifferenceExport;
use App\Exports\HeadHistoryDateConditionsExport;
use PDF;
use Carbon\Carbon;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class HeadCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $hoa=HeadofAccounts::all();
        $hcat=HeadCategory::with(['hoa'])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('headCategory.index',compact('hoa','hcat','permissions','menu_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hoa=HeadofAccounts::all();
        $data=[
            'hoa' => $hoa,
            'isEdit' => false
        ];
        return view('headCategory.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hcat=HeadCategory::where('name',$request->name)
        ->where('a_id',$request->a_id)
        ->first();
        $u_id = Auth::user()->id;
        $data = [
            'name' => $request->name,
            'a_id' => $request->a_id,
            'code' => $request->code,
            'created_by' => $u_id
        ];
        if($hcat==null)
        {
            HeadCategory::create($data);
            $u_name = Auth::user()->name;
            $user = User::where('r_id',config('app.adminId'))->get();
            $data1 = [
                'notification' => 'New head category has been added by '.$u_name,
                'link' => url('').'/headcategory',
                'name' => 'View Head Categories',
            ];
            Notification::send($user, new AddNotification($data1));
            toastr()->success('Head Category added successfully!');
            return redirect()->back();
        }
        else
        {
            toastr()->error('Head Category already exist of this HOA!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ledger = GeneralLedger::where('account_code','like',$id.'%')
        ->get();
        if($ledger->isEmpty())
        {
            return $ledger=0;
        }
        else
        {
            return $ledger;
        }
    }
    public function history($id,$mid)
    {
        $permissions        =   getRolePermission($mid);
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->get();
        $index = 0;
        return view('headCategory.history',compact('ledger','id','permissions','index','mid'));
    }

    public function excel($id,$code,$name)
    {
        return Excel::download(new HeadHistoryExport($id,$code,$name), 'Ledger.xlsx');
    }

    public function searchYExcel($year,$id)
    {
        return Excel::download(new HeadHistoryYearlyExport($year,$id), 'LedgerYearly.xlsx');
    }

    public function searchMExcel($month,$id)
    {
        return Excel::download(new HeadHistoryMonthlyExport($month,$id), 'LedgerMonthly.xlsx');
    }

    public function searchDExcel($date,$id)
    {
        return Excel::download(new HeadHistoryDateExport($date,$id), 'LedgerDateWise.xlsx');
    }

    public function searchDDExcel($from,$to,$id)
    {
        return Excel::download(new HeadHistoryDateDifferenceExport($from,$to,$id), 'LedgerDateRange.xlsx');
    }

    public function searchExcel($index,$id)
    {
        return Excel::download(new HeadHistoryDateConditionsExport($index,$id), 'Ledger.xlsx');
    }

    public function pdf($id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->get();
        $id = $id;
        $pdf = PDF::loadView('headCategory.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchYPDF($year,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y'))"),$year)
        ->get();
        $id = $id;
        $pdf = PDF::loadView('headCategory.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchMPDF($month,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$month)
        ->get();
        $id = $id;
        $pdf = PDF::loadView('headCategory.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchDPDF($date,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),$date)
        ->get();
        $id = $id;
        $pdf = PDF::loadView('headCategory.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchDDPDF($from,$to,$id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->whereBetween(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),[$from,$to])
        ->get();
        $id = $id;
        $pdf = PDF::loadView('headCategory.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }
    public function searchPDF($index,$id)
    {
        if($index == 5)
        {
            $date = Carbon::today()->subDays(7);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
        }
        if($index == 6)
        {
            $date = Carbon::today()->subDays(15);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
        }
        if($index == 7)
        {
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$month)
            ->get();
        }
        $id = $id;
        $pdf = PDF::loadView('headCategory.pdf', compact('ledger','id'));

        return $pdf->download('Ledger.pdf');
    }

    public function accountsName($id)
    {
        if($id == 'CA-01')
        {
            $account = AccountDetails::where('Code','like',$id.'%')
            ->where('Code','Not like','NCA-01%')
            ->where('account_type','p')
            ->get();
        }
        else if($id == 'CL-01')
        {
            $account = AccountDetails::where('Code','like',$id.'%')
            ->where('Code','Not like','NCL-01%')
            ->where('account_type','p')
            ->get();
        }
        else
        {
            $account = AccountDetails::with('accounts')->where('Code','like',$id.'%')
            ->where('account_type','p')
            ->get();
        }
        return $account;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hcat = HeadCategory::find($id);
        $hoa=HeadofAccounts::all();
        $data=[
            'isEdit' => true,
            'hcat' => $hcat,
            'hoa' => $hoa
        ];
        return view('headCategory.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      =>  'required|string|max:50'
        ]);
        $u_id = Auth::user()->id;
        HeadCategory::where('id',$id)
        ->update([
            'name' => $request->name,
            'updated_by' => $u_id
        ]);
        toastr()->success('Head Category updated successfully!');
        return redirect(url('').'/headcategory');
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $mid            =   $request->menuid;
        $permissions        =   getRolePermission($mid);
        $id            =   $request->id;

        if($request->optradio == 'Year')
        {
            $index = 1;
            $year = $request->year;
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y'))"),$request->year)
            ->get();
            return view('headCategory.history',compact('ledger','id','permissions','index','mid','year'));
        }
        if($request->optradio == 'Month')
        {
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$request->month)
            ->get();
            $index = 2;
            $month = $request->month;
            return view('headCategory.history',compact('ledger','id','permissions','index','mid','month'));
        }
        if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $index = 3;
                $from = $request->from;
                $to = $request->to;
                $ledger = GeneralLedger::with('createUser')
                ->where('account_code','like',$id.'%')
                ->whereBetween(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                ->get();
                return view('headCategory.history',compact('ledger','id','permissions','index','mid','from','to'));
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                $ledger = GeneralLedger::with('createUser')
                ->where('account_code','like',$id.'%')
                ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m-%d'))"),$date)
                ->get();
                return view('headCategory.history',compact('ledger','id','permissions','index','mid','date'));
            }
        }
        if($request->optradio == 'lastweek')
        {
            $index = 5;
            $date = Carbon::today()->subDays(7);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
            return view('headCategory.history',compact('ledger','id','permissions','index','mid','date'));
        }
        if($request->optradio == 'last15Days')
        {
            $index = 6;
            $date = Carbon::today()->subDays(15);
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where('accounting_date','>=',$date)
            ->get();
            return view('headCategory.history',compact('ledger','id','permissions','index','mid','date'));
        }
        if($request->optradio == 'lastMonth')
        {
            $index = 7;
            $month = Carbon::now()->subMonth()->startOfMonth()->format('Y-m');
            $ledger = GeneralLedger::with('createUser')
            ->where('account_code','like',$id.'%')
            ->where(DB::raw("(DATE_FORMAT(accounting_date,'%Y-%m'))"),$month)
            ->get();
            return view('headCategory.history',compact('ledger','id','permissions','index','mid','month'));
        }
    }
}
