<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest;
use App\PurchaseRequestDetails;
use App\PurchaseOrder;
use App\PurchaseOrderDetails;
use App\Quotation;
use App\QuotationDetails;
use App\ProductVariants;
use App\Vendors;
use App\Products;
use Auth;
use Carbon\Carbon;
use Storage;
use App\User;
use App\City;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $quote = Quotation::with(['supplier','qdetails'])
        ->get();
        // dd($quote);
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $supplier = Vendors::where('v_type','Supplier')->where('status',1)->get();
        return view('quotation.index',compact('quote','supplier','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $id = [];
        $u_id = Auth::user()->id;
        for ($i=0; $i < count($request->s_id) ; $i++) {
            $id[] = Quotation::create([
                'date' => $request->date,
                'r_id' => $request->r_id,
                's_id' => $request->s_id[$i],
                'created_by' => $u_id
            ]);
        }
        for ($i=0; $i < count($id) ; $i++) {
            for ($j=0; $j < count($request->p_id) ; $j++) {
                QuotationDetails::create([
                    'q_id' => $id[$i]->id,
                    'p_id' => $request->p_id[$j],
                    'quantity' => $request->quantity[$j],
                    'type' => $request->type[$j]
                ]);
            }
        }
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Quotations for purchase request number: '.$request->r_id.'  is been added by '.$u_name,
            'link' => url('').'/quotation',
            'name' => 'View Purchase Request Quotations',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Quotation Added successfully!');
        return redirect(url('').'/quotation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p=Quotation::with(['supplier'])
        ->where('id',$id)
        ->first();
        $d=QuotationDetails::with(['products','variant.product.brands','variant.product.category','products.brands','products.category'])
        ->where('q_id',$id)
        ->get();
        return [$p,$d];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quote=Quotation::with(['supplier'])
        ->where('id',$id)
        ->first();
        $prequest = PurchaseRequestDetails::with('products','products.brands','products.category')
        ->where('o_id',$quote->r_id)
        ->get();
        $city=City::all();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $data=[
            'isEdit' => true,
            'supplier' => $supplier,
            'quote' => $quote,
            'prequest' => $prequest,
            'city' => $city,
        ];
        return view('quotation.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $data = [
            'attachment' => $request->attachment
        ];
        $data['attachment']= Storage::disk('uploads')->putFile('',$request->attachment);
        Quotation::where('id',$id)
        ->update($data);

        for ($i=0; $i < count($request->p_id) ; $i++) {

            QuotationDetails::where('q_id',$request->id)
            ->where('p_id',$request->p_id[$i])
            ->update([
                'cost' => $request->cost[$i]
            ]);
        }
        toastr()->success('Quotation updated successfully!');
        return redirect(url('').'/quotation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newQuotation($id)
    {
        // dd($id);
        $prequest = PurchaseRequestDetails::with(['products.currentstocks','products','products.brands','products.category','products.unit','request','variant.currentstocks'])
        ->where('o_id',$id)
        ->get();
        $city = City::all();
        // dd($prequest);
        $order=Quotation::max('id');
        if($order == null)
        {
            $ids=1;
        }
        else
        {
            $ids=$order+1;
        }
        // dd($prequest);
        $date=Carbon::now()->format('Y-m-d');
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $isEdit = false;
        $id = $id;
        return view('quotation.create',compact('city','date','supplier','isEdit','id','prequest','ids'));
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $id = $request->id;
        $biller = Vendors::where('v_type','Biller')->where('name','Posch Care')->first();
        $status = $request->status;
        if($status == 'Approved')
        {
            $date=Carbon::now()->format('Y-m-d');
            $u_id = Auth::user()->id;
            $quote = Quotation::where('id',$id)->first();
            $quotedet = QuotationDetails::where('q_id',$id)->get();
            $req = PurchaseRequest::where('id',$quote->r_id)->first();
            $reqdetails = PurchaseRequestDetails::where('o_id',$quote->r_id)->get();
            $total = 0;
            $sub_total = [];
            $n = 0;
            $d = 0;
            for ($i=0; $i < count($quotedet) ; $i++) {
                $total += $quotedet[$i]->cost * $reqdetails[$i]->quantity;
                $sub_total[] =$quotedet[$i]->cost * $reqdetails[$i]->quantity;
            }


            for ($i=0; $i < count($quotedet) ; $i++)
            {
                $qdetail = QuotationDetails::withCount(['quotation as quote' => function($query) {
                    $avg = $query->where('status','Approved')->select(DB::raw('id'));
                }])
                ->where('p_id',$quotedet[$i]->p_id)
                ->get();
                if($qdetail->isEmpty())
                {
                    if($quotedet[$i]->type == 1)
                    {
                        ProductVariants::where('id',$quotedet[$i]->p_id)
                        ->update([
                            'cost' => $quotedet[$i]->cost
                        ]);
                    }
                    else
                    {
                        Products::where('id',$quotedet[$i]->p_id)
                        ->update([
                            'cost' => $quotedet[$i]->cost
                        ]);
                    }
                }
                else
                {
                    foreach ($qdetail as $key => $q) {
                        $n += $q->cost * $q->quantity;
                        $d += $q->quantity;
                    }
                    if($quotedet[$i]->type == 1)
                    {
                        ProductVariants::where('id',$quotedet[$i]->p_id)
                        ->update([
                            'cost' =>  $n / $d
                        ]);
                    }
                    else
                    {
                        Products::where('id',$quotedet[$i]->p_id)
                        ->update([
                            'cost' =>  $n / $d
                        ]);
                    }

                    $n = 0;
                    $d = 0;
                }
            }
            $item = Quotation::find($id);
            if ($item->update(['status' => $status])) {
                Quotation::where('id',$id)
                ->update([
                    'status' => $status,
                    'updated_by' => $u_id
                ]);

            }
            $p = PurchaseOrder::create([
                'order_date' => $date,
                'ref_no' => $quote->r_id,
                'w_id' => $req->w_id,
                's_id' => $quote->s_id,
                'b_id' => $biller->id,
                'total' => $total,
                'created_by' => $u_id
            ]);
            for ($i=0; $i < count($quotedet) ; $i++) {
                PurchaseOrderDetails::create([
                    'o_id' => $p->id,
                    'p_id' => $quotedet[$i]->p_id,
                    'quantity' => $reqdetails[$i]->quantity,
                    'sub_total' => $sub_total[$i],
                    'cost' => $quotedet[$i]->cost,
                    'type' => $quotedet[$i]->type,
                ]);
            }

            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        else
        {
            $u_id = Auth::user()->id;
            $item = Quotation::find($id);
            if ($item->update(['status' => $status])) {
                Quotation::where('id',$id)
                ->update([
                    'status' => $status,
                    'updated_by' => $u_id
                ]);

            }
            return response()->json($response, 200);
        }
    }
}
