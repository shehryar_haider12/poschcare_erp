<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\GeneralLedger;
use Carbon\Carbon;
use App\Products;
use App\ProductVariants;
use App\Brands;
use App\Category;
use App\Unit;
use App\Subcategory;
use App\Stocks;
use App\CurrentStock;
use App\StockReceiving;
use App\AccountDetails;
use App\HeadCategory;

use App\JournalVoucher;
use App\TransactionHistory;

use App\PurchaseOrder;
use App\PurchaseOrderDetails;
use App\Vendors;
use App\AvgCost;

use App\Sales;
use App\SaleDetails;
use App\StockOut;
use App\SaleType;

class ImportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //Inventory  Opening
    public function openingInventory(Request $request)
    {
        $request->validate([
            'csv_file' => 'required',
       ]);
       $filePath       =   pathinfo($request->csv_file)['dirname'].'/'.pathinfo($request->csv_file)['basename'];
       $data           =   readCsv($filePath);
       $u_id = Auth::user()->id;
       for ($i = 0; $i < count($data); $i ++)
        {
            $brand          =   Brands::where('b_name',$data[$i]['brand'])->first();
            $category       =   Category::where('cat_name',$data[$i]['category'])->first();
            $subcategory    =   Subcategory::where('s_cat_name',$data[$i]['sub-category'])->first();
            $unit           =   Unit::where('u_name',$data[$i]['unit'])->first();

            if(empty($brand)){
                $brand = Brands::create([
                    'b_name'  => $data[$i]['brand']
                ]);
            }
            if(empty($category)){
                $category = Category::create([
                    'cat_name'  => $data[$i]['category']
                ]);
            }
            if(empty($subcategory)){
                $subcategory = Subcategory::create([
                    'cat_id'        => $category->id,
                    's_cat_name'    => $data[$i]['sub-category']
                ]);
            }
            if(empty($unit)){
                $unit = Unit::create([
                    'u_name'  => $data[$i]['unit']
                ]);
            }

            $product = Products::where('pro_name',$data[$i]['product name'])
            ->where('p_type',$data[$i]['product type'])
            ->where('brand_id',$brand->id)
            ->where('cat_id',$category->id)
            ->where('s_cat_id',$subcategory->id)
            ->where('unit_id',$unit->id)
            ->first();

            if(empty($product))
            {

            }

            $product_data=[
                'pro_name'         =>   $data[$i]['product name'],
                'brand_id'         =>   $brand->id,
                'cat_id'           =>   $category->id,
                's_cat_id'         =>   $subcategory->id,
                'p_type'           =>   'Finished',
                'pro_code'         =>   $data[$i]['code'],
                'unit_id'          =>   $unit->id,
                'cost'             =>   $data[$i]['cost'],
                'price'            =>   $data[$i]['price'],
                'case'              =>   $data[$i]['Case'],
                'description'      =>   $data[$i]['description'] ?? '',
                'mrp'              =>   $data[$i]['mrp'],
                'company_name'      =>   $data[$i]['Company'],
                'weight'           =>   '',
                'alert_quantity'   =>   10,
                'created_by'       =>   Auth::user()->id,
                'status'           =>   1,
                'vstatus'          =>   0
            ];

            if(empty($product))
            {
                $product = Products::create($product_data);


                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id         = substr("0000{$id}", -$str_length);
                $code       = $hcat->code.'-'.$id;

                $data1 = [
                    'Code'              =>  $code,
                    'name_of_account'   =>  $product->pro_code.' - '.$product->pro_name,
                    'c_id'              =>  $hcat->id,
                    'created_by'        =>  Auth::user()->id,
                    'type'              =>  0
                ];
                AccountDetails::create($data1);
            }
        }
        return redirect()->back()->with('uploaded','Product Uploaded Successfully');
    }


    // journal voucher
    public function journalVoucher(Request $request)
    {
        $request->validate([
            'csv_file' => 'required',
       ]);
       $filePath       =   pathinfo($request->csv_file)['dirname'].'/'.pathinfo($request->csv_file)['basename'];
       $data           =   readCsv($filePath);
       $u_id = Auth::user()->id;

        for ($i=0; $i <count($data) ; $i++)
        {
            $date =Carbon::createFromFormat('d/m/Y', $data[$i]['Date'])->format('Y-m-d');
            $hcatDebit = HeadCategory::where('name',$data[$i]['head categoryD'])->first();
            $hcatCredit = HeadCategory::where('name',$data[$i]['head categoryC'])->first();
            $accDebit = AccountDetails::where('name_of_account',$data[$i]['A/d'])->first();
            $accCredit = AccountDetails::where('name_of_account',$data[$i]['A/c'])->first();
            if(empty($accDebit))
            {
                $account1 = AccountDetails::where('c_id',$hcatDebit->id)->latest('created_at')->orderBy('id','desc')->first();
                if($account1 == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account1->Code, strpos($account1->Code, '-',strpos($account1->Code, '-')+1)+1) +1;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcatDebit->code.'-'.$id;
                $accountData=[
                    'Code' => $code,
                    'name_of_account' => $data[$i]['A/d'],
                    'c_id' => $hcatDebit->id,
                    'created_by' => $u_id
                ];
                $accDebit = AccountDetails::create($accountData);
            }

            if(empty($accCredit))
            {
                $account1 = AccountDetails::where('c_id',$hcatCredit->id)->latest('created_at')->orderBy('id','desc')->first();
                if($account1 == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account1->Code, strpos($account1->Code, '-',strpos($account1->Code, '-')+1)+1) +1;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcatCredit->code.'-'.$id;
                $accountData=[
                    'Code' => $code,
                    'name_of_account' => $data[$i]['A/c'],
                    'c_id' => $hcatCredit->id,
                    'created_by' => $u_id
                ];
                $accCredit = AccountDetails::create($accountData);
            }
            $status = 'Approved';
            $created_by=$u_id;
            $period =Carbon::createFromFormat('d/m/Y', $data[$i]['Date'])->format('M-y');
            JournalVoucher::create([
                'date' => $date,
                'status' => $status,
                'account1' => $accDebit->Code,
                'account2' => $accCredit->Code,
                'amount' => (int)$data[$i]['Dr'],
                'description' => $data[$i]['Particular'],
                'month' => $period,
                'head1' => $hcatDebit->code,
                'head2' => $hcatCredit->code,
                'created_by' => $created_by,
            ]);



            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            if($ledger == null)
            {
                $link_id=1;
            }
            else
            {
                $link_id = $ledger->link_id + 1;
            }
            $posted_date = Carbon::createFromFormat('d/m/Y', $data[$i]['Date'])->format('Y-m-d');
            $period =Carbon::createFromFormat('d/m/Y', $data[$i]['Date'])->format('M-y');
            $cash_c = 0;
            $cash_d = 0;
            $cash_n = 0;
            $ref_no = 0;
            if($data[$i]['head categoryC'] == 'Cash' || $data[$i]['head categoryC'] == 'Bank')
            {

                TransactionHistory::create([
                    'p_s_id' => 0,
                    'p_type' => 'Expense',
                    't_type' => 'Sent',
                    'paid_by' =>$data[$i]['head categoryC'],
                    'total' => (int)$data[$i]['Dr'],
                    'created_by' => $u_id
                ]);
                $ref_no = TransactionHistory::max('id');
            }
            $account_c = AccountDetails::where('name_of_account',$data[$i]['A/c'])->first();
            $cash = GeneralLedger::where('account_code',$account_c->Code)
            ->get();
            if($cash->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' =>$data[$i]['Particular'],
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'transaction_no' => $ref_no == 0 ? '' : $ref_no,
                    'currency_code' =>'PKR',
                    'debit' => '0',
                    'credit' => (int)$data[$i]['Dr'],
                    'net_value' => 0 - (int)$data[$i]['Dr'],
                    'balance' => 0 - (int)$data[$i]['Dr']
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($cash as $key => $c) {
                    $cash_c+=$c->credit;
                    $cash_d+=$c->debit;
                    $balance+=$c->net_value;
                }
                $cash_c += (int)$data[$i]['Dr'];
                $cash_n = $balance + (0 - (int)$data[$i]['Dr']);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' =>$data[$i]['Particular'],
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'transaction_no' => $ref_no == 0 ? '' : $ref_no,
                    'currency_code' =>'PKR',
                    'debit' => '0',
                    'credit' => (int)$data[$i]['Dr'],
                    'net_value' => 0 - (int)$data[$i]['Dr'],
                    'balance' =>$cash_n
                ]);
            }


            $exp_d = 0;
            $exp_c = 0;
            $exp_n = 0;
            $account_s = AccountDetails::where('name_of_account',$data[$i]['A/d'])
            ->first();
            $expense = GeneralLedger::where('account_code',$account_s->Code)
            ->get();

            if($expense->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => $data[$i]['Particular'],
                    'account_name' => $account_s->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_s->Code,
                    'transaction_no' => $ref_no == 0 ? '' : $ref_no,
                    'currency_code' =>'PKR',
                    'credit' => '0',
                    'debit' => (int)$data[$i]['Dr'],
                    'net_value' => (int)$data[$i]['Dr'],
                    'balance' => (int)$data[$i]['Dr']
                ]);
            }
            else
            {
                $balance = 0;
                    foreach ($expense as $key => $c) {
                        $exp_c+=$c->credit;
                        $exp_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $exp_c += (int)$data[$i]['Dr'];
                    $exp_n = $balance + ((int)$data[$i]['Dr'] - 0);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => $data[$i]['Particular'],
                        'account_name' => $account_s->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => 2,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_s->Code,
                        'transaction_no' => $ref_no == 0 ? '' : $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => '0',
                        'debit' => (int)$data[$i]['Dr'],
                        'net_value' => (int)$data[$i]['Dr'],
                        'balance' =>$exp_n
                    ]);
            }
        }
        toastr()->success('Vouchers added successfully!');
        return redirect()->back();

    }

    //// purchase order
    public function purchaseOrder(Request $request)
    {
        $request->validate([
            'csv_file' => 'required',
       ]);
       $filePath       =   pathinfo($request->csv_file)['dirname'].'/'.pathinfo($request->csv_file)['basename'];
       $data           =   readCsv($filePath);
       $u_id = Auth::user()->id;
       $total = 0;
       for ($i=0; $i <=count($data) ; $i++)
       {
            if($i == count($data))
            {
                $poid = PurchaseOrder::max('id');
                PurchaseOrder::where('id',$poid)->update(['total' => $total]);
                $purchased = PurchaseOrder::find($poid);
                if($purchased->status != 'Pending')
                {
                    if($purchased->status == 'Received')
                    {
                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger->link_id + 1;
                        }
                        $account_s = AccountDetails::where('name_of_account',$v->name.' - '.$v->company)
                        ->first();
                        $payable = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $account_p = AccountDetails::where('name_of_account','Purchase')->first();
                        $purchase = GeneralLedger::where('account_code',$account_p->Code)
                        ->get();
                        $posted_dateL = Carbon::parse($purchased->order_date)->format('Y-m-d');
                        $periodL =Carbon::parse($purchased->order_date)->format('M-y');

                        $purchase_d = 0 ;
                        $purchase_c = 0 ;
                        $purchase_n = 0 ;
                        $supplier_d = 0 ;
                        $supplier_c = 0 ;
                        $supplier_n = 0 ;
                        if($purchase->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_p->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_p->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => $total,
                                'credit' => '0',
                                'net_value' => $total,
                                'balance' => $total
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($purchase as $key => $d) {
                                $purchase_d+=$d->debit;
                                $purchase_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $purchase_d += $total;
                            // $purchase_n = $purchase_d - $purchase_c;
                            $purchase_n = $balance + ($total - 0);
                            $dfdfd = GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_p->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_p->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => $total,
                                'credit' => '0',
                                'net_value' => $total,
                                'balance' => $purchase_n
                            ]);
                        }

                        if($payable->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $total,
                                'net_value' => 0 - $total,
                                'balance' => 0 - $total
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($payable as $key => $c) {
                                $supplier_c+=$c->credit;
                                $supplier_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $supplier_c += $total;
                            // $supplier_n = $supplier_d - $supplier_c;
                            $supplier_n = $balance + (0 - $total);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $total,
                                'net_value' => 0 - $total,
                                'balance' => $supplier_n
                            ]);
                        }
                        $total =0;
                    }
                    if($purchased->status == 'Partial')
                    {
                        $podetails = PurchaseOrderDetails::where('o_id',$poid)->get();
                        $amount = 0;
                        foreach ($podetails as $key => $pd) {
                            if($pd->received_quantity != 0)
                            {
                                $amount += ($pd->sub_total / $pd->quantity) * $pd->received_quantity;
                            }
                        }
                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger->link_id + 1;
                        }
                        $account_s = AccountDetails::where('name_of_account',$v->name.' - '.$v->company)
                        ->first();
                        $payable = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $account_p = AccountDetails::where('name_of_account','Purchase')->first();
                        $purchase = GeneralLedger::where('account_code',$account_p->Code)
                        ->get();
                        $posted_dateL = Carbon::parse($purchased->order_date)->format('Y-m-d');
                        $periodL =Carbon::parse($purchased->order_date)->format('M-y');

                        $purchase_d = 0 ;
                        $purchase_c = 0 ;
                        $purchase_n = 0 ;
                        $supplier_d = 0 ;
                        $supplier_c = 0 ;
                        $supplier_n = 0 ;
                        if($purchase->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_p->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_p->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => $amount,
                                'credit' => '0',
                                'net_value' => $amount,
                                'balance' => $amount
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($purchase as $key => $d) {
                                $purchase_d+=$d->debit;
                                $purchase_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $purchase_d += $amount;
                            // $purchase_n = $purchase_d - $purchase_c;
                            $purchase_n = $balance + ($amount - 0);
                            $dfdfd = GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_p->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_p->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => $amount,
                                'credit' => '0',
                                'net_value' => $amount,
                                'balance' => $purchase_n
                            ]);
                        }

                        if($payable->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $periodL,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $amount,
                                'net_value' => 0 - $amount,
                                'balance' => 0 - $amount
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($payable as $key => $c) {
                                $supplier_c+=$c->credit;
                                $supplier_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $supplier_c += $amount;
                            // $supplier_n = $supplier_d - $supplier_c;
                            $supplier_n = $balance + (0 - $amount);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Purchase from '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_dateL,
                                'posted_date' => $posted_dateL,
                                'period' => $perioLd,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $Grn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $amount,
                                'net_value' => 0 - $amount,
                                'balance' => $supplier_n
                            ]);
                        }
                    }
                }
                break;
            }
            $posted_date = Carbon::createFromFormat('d/m/Y', $data[$i]['Date'])->format('Y-m-d');
            $period =Carbon::createFromFormat('d/m/Y', $data[$i]['Date'])->format('M-y');
            $v =Vendors::where('name',$data[$i]['supplier'])->where('company',$data[$i]['company'])->where('v_type','Supplier')->first();
            $biller =Vendors::where('name','Posch Care')->where('v_type','Biller')->first();
            if($v == null)
            {
                $hcat = HeadCategory::where('name','Payables')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;

                $data2 = [
                    'Code' => $code,
                    'name_of_account' => $data[$i]['supplier'].' - '.$data[$i]['company'],
                    'c_id' => $hcat->id,
                    'created_by' => 2
                ];
                AccountDetails::create($data2);

                $v = Vendors::create([
                    'company' => $data[$i]['company'],
                    'name' => $data[$i]['supplier'],
                    'v_type' => 'Supplier',
                    'created_by' => $u_id,
                    'status' => 1
                ]);
            }
            $p = PurchaseOrder::where('order_date',$posted_date)->where('s_id',$v->id)->first();
            $poid = PurchaseOrder::max('id');
            $Grn_no = rand();

            if($p == null)
            {
                if($i != 0)
                {
                    if($poid != null)
                    {
                        PurchaseOrder::where('id',$poid)->update(['total' => $total]);
                        $purchased = PurchaseOrder::find($poid);
                        if($purchased->status != 'Pending')
                        {
                            if($purchased->status == 'Received')
                            {
                                $gl = GeneralLedger::max('id');
                                if($gl == null)
                                {
                                    $link_id = 1;
                                }
                                else
                                {
                                    $ledger = GeneralLedger::where('id',$gl)->first();
                                    $link_id = $ledger->link_id + 1;
                                }
                                $account_s = AccountDetails::where('name_of_account',$v->name.' - '.$v->company)
                                ->first();
                                $payable = GeneralLedger::where('account_code',$account_s->Code)
                                ->get();
                                $account_p = AccountDetails::where('name_of_account','Purchase')->first();
                                $purchase = GeneralLedger::where('account_code',$account_p->Code)
                                ->get();
                                $posted_dateL = Carbon::parse($purchased->order_date)->format('Y-m-d');
                                $periodL =Carbon::parse($purchased->order_date)->format('M-y');

                                $purchase_d = 0 ;
                                $purchase_c = 0 ;
                                $purchase_n = 0 ;
                                $supplier_d = 0 ;
                                $supplier_c = 0 ;
                                $supplier_n = 0 ;
                                if($purchase->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_p->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_p->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => $total,
                                        'credit' => '0',
                                        'net_value' => $total,
                                        'balance' => $total
                                    ]);
                                }
                                else
                                {
                                    $balance = 0;
                                    foreach ($purchase as $key => $d) {
                                        $purchase_d+=$d->debit;
                                        $purchase_c+=$d->credit;
                                        $balance+=$d->net_value;
                                    }
                                    $purchase_d += $total;
                                    // $purchase_n = $purchase_d - $purchase_c;
                                    $purchase_n = $balance + ($total - 0);
                                    $dfdfd = GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_p->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_p->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => $total,
                                        'credit' => '0',
                                        'net_value' => $total,
                                        'balance' => $purchase_n
                                    ]);
                                }

                                if($payable->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_s->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_s->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => '0',
                                        'credit' => $total,
                                        'net_value' => 0 - $total,
                                        'balance' => 0 - $total
                                    ]);
                                }
                                else
                                {
                                    $balance = 0;
                                    foreach ($payable as $key => $c) {
                                        $supplier_c+=$c->credit;
                                        $supplier_d+=$c->debit;
                                        $balance+=$c->net_value;
                                    }
                                    $supplier_c += $total;
                                    // $supplier_n = $supplier_d - $supplier_c;
                                    $supplier_n = $balance + (0 - $total);

                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_s->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_s->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => '0',
                                        'credit' => $total,
                                        'net_value' => 0 - $total,
                                        'balance' => $supplier_n
                                    ]);
                                }
                                $total =0;
                            }
                            if($purchased->status == 'Partial')
                            {
                                $podetails = PurchaseOrderDetails::where('o_id',$poid)->get();
                                $amount = 0;
                                foreach ($podetails as $key => $pd) {
                                    if($pd->received_quantity != 0)
                                    {
                                        $amount += ($pd->sub_total / $pd->quantity) * $pd->received_quantity;
                                    }
                                }
                                $gl = GeneralLedger::max('id');
                                if($gl == null)
                                {
                                    $link_id = 1;
                                }
                                else
                                {
                                    $ledger = GeneralLedger::where('id',$gl)->first();
                                    $link_id = $ledger->link_id + 1;
                                }
                                $account_s = AccountDetails::where('name_of_account',$v->name.' - '.$v->company)
                                ->first();
                                $payable = GeneralLedger::where('account_code',$account_s->Code)
                                ->get();
                                $account_p = AccountDetails::where('name_of_account','Purchase')->first();
                                $purchase = GeneralLedger::where('account_code',$account_p->Code)
                                ->get();
                                $posted_dateL = Carbon::parse($purchased->order_date)->format('Y-m-d');
                                $periodL =Carbon::parse($purchased->order_date)->format('M-y');

                                $purchase_d = 0 ;
                                $purchase_c = 0 ;
                                $purchase_n = 0 ;
                                $supplier_d = 0 ;
                                $supplier_c = 0 ;
                                $supplier_n = 0 ;
                                if($purchase->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_p->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_p->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => $amount,
                                        'credit' => '0',
                                        'net_value' => $amount,
                                        'balance' => $amount
                                    ]);
                                }
                                else
                                {
                                    $balance = 0;
                                    foreach ($purchase as $key => $d) {
                                        $purchase_d+=$d->debit;
                                        $purchase_c+=$d->credit;
                                        $balance+=$d->net_value;
                                    }
                                    $purchase_d += $amount;
                                    // $purchase_n = $purchase_d - $purchase_c;
                                    $purchase_n = $balance + ($amount - 0);
                                    $dfdfd = GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_p->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_p->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => $amount,
                                        'credit' => '0',
                                        'net_value' => $amount,
                                        'balance' => $purchase_n
                                    ]);
                                }

                                if($payable->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_s->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $periodL,
                                        'account_code' => $account_s->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => '0',
                                        'credit' => $amount,
                                        'net_value' => 0 - $amount,
                                        'balance' => 0 - $amount
                                    ]);
                                }
                                else
                                {
                                    $balance = 0;
                                    foreach ($payable as $key => $c) {
                                        $supplier_c+=$c->credit;
                                        $supplier_d+=$c->debit;
                                        $balance+=$c->net_value;
                                    }
                                    $supplier_c += $amount;
                                    // $supplier_n = $supplier_d - $supplier_c;
                                    $supplier_n = $balance + (0 - $amount);

                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Purchase from '.$v->name,
                                        'account_name' => $account_s->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_dateL,
                                        'posted_date' => $posted_dateL,
                                        'period' => $perioLd,
                                        'account_code' => $account_s->Code,
                                        'transaction_no' => $Grn_no,
                                        'currency_code' => 'PKR',
                                        'debit' => '0',
                                        'credit' => $amount,
                                        'net_value' => 0 - $amount,
                                        'balance' => $supplier_n
                                    ]);
                                }
                            }
                        }
                        $total = 0;
                    }
                    else
                    {
                        $total =0;
                    }
                }

                $days = null;
                if($data[$i]['pay date'] != '')
                {
                    $paydate = Carbon::createFromFormat('d/m/Y', $data[$i]['pay date'])->format('Y-m-d');
                    $date1=date_create($posted_date);
                    $date2=date_create($paydate);
                    $diff = $diff=date_diff($date1,$date2);
                    $daye = $diff->format("%a Days");
                }
                else
                {
                    $days = null;
                }

                $podata=([
                    'order_date' => $posted_date,
                    'w_id' => 1,
                    'status' => $data[$i]['status'],
                    's_id' => $v->id,
                    'note' => $data[$i]['remarks'],
                    'p_status' => $data[$i]['payment status'],
                    'created_by' => $u_id,
                    'b_id' => $biller->id,
                    'tax_status' => $data[$i]['tax status'],
                    'tax' => $data[$i]['tax'],
                    'exp_rcv_date' => $data[$i]['expected date'] == ''  ? null : Carbon::createFromFormat('d/m/Y', $data[$i]['expected date'])->format('Y-m-d'),
                    'payment_mode' => $data[$i]['payment mode'],
                    'pdate' => $data[$i]['pay date'] == ''  ? null : $data[$i]['pay date'],
                    'days' => $days,
                    // 'total' => $total,
                ]);
                $p=PurchaseOrder::create($podata);
                $total+=$data[$i]['subtotal'];


                $brand = Brands::where('b_name',$data[$i]['brand'])->first();
                $cat = Category::where('cat_name',$data[$i]['category'])->first();
                $unit = Unit::where('u_name',$data[$i]['unit'])->first();
                $scat = Subcategory::where('s_cat_name',$data[$i]['subcategory'])->first();
                if($data[$i]['variant'] == '' && $data[$i]['size'] == ''  && $data[$i]['color'] == '' )
                {
                    $product = Products::where('brand_id',$brand->id)
                    ->where('cat_id',$cat->id)
                    ->where('unit_id',$unit->id)
                    ->where('s_cat_id',$scat->id)
                    ->where('p_type',$data[$i]['ptype'])
                    ->where('pro_name',$data[$i]['product name'])
                    ->first();
                    // $total+=$data[$i]['subtotal'];
                    if($product == null)
                    {
                        $pro_code = $data[$i]['product code'];
                        $product = Products::create([
                            'pro_code' => $pro_code,
                            'pro_name' => $data[$i]['product name'],
                            'p_type' => $data[$i]['ptype'],
                            'cost' => $data[$i]['cost'],
                            'unit_id' => $unit->id,
                            'brand_id' => $brand->id,
                            'cat_id' => $cat->id,
                            's_cat_id' => $scat->id,
                            'status' => 1,
                            'visibility' => 1,
                            'created_by' => $u_id,
                            'vstatus' => 0,
                            'mstatus' => 'No',
                        ]);
                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->orderBy('id','desc')->first();

                        if($account == null)
                        {
                            $id = 001;
                        }
                        else
                        {
                            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }

                        $str_length = strlen((string)$id)+2;
                        $id = substr("0000{$id}", -$str_length);
                        $code = $hcat->code.'-'.$id;
                        $data2 = [
                            'Code' => $code,
                            'name_of_account' => $pro_code.' - '.$data[$i]['product name'],
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 0
                        ];
                        AccountDetails::create($data2);
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 0,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  0,
                            ]);
                            $cs=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>0,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$product->id)
                                ->where('w_id',1)
                                ->where('type',0)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }
                    }
                    else
                    {
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 0,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  0,
                            ]);
                            $cs=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>0,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$product->id)
                                ->where('w_id',1)
                                ->where('type',0)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }
                    }

                }
                else {
                    $product = Products::where('brand_id',$brand->id)
                    ->where('cat_id',$cat->id)
                    ->where('unit_id',$unit->id)
                    ->where('s_cat_id',$scat->id)
                    ->where('p_type',$data[$i]['ptype'])
                    ->where('pro_name',$data[$i]['product name'])
                    ->first();
                    // $total+=$data[$i]['subtotal'];
                    if($product == null)
                    {
                        $pro_code = $data[$i]['product code'];
                        $product = Products::create([
                            'pro_code' => $pro_code,
                            'pro_name' => $data[$i]['product name'],
                            'p_type' => $data[$i]['ptype'],
                            'cost' => $data[$i]['cost'],
                            'unit_id' => $unit->id,
                            'brand_id' => $brand->id,
                            'cat_id' => $cat->id,
                            's_cat_id' => $scat->id,
                            'status' => 1,
                            'visibility' => 1,
                            'created_by' => $u_id,
                            'vstatus' => 0,
                            'mstatus' => 'No',
                        ]);

                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::create([
                            'p_id' => $product->id,
                            'name' => $name,
                            'status' => 1,
                            'cost' => $data[$i]['cost'],
                        ]);
                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->orderBy('id','desc')->first();

                        if($account == null)
                        {
                            $id = 001;
                        }
                        else
                        {
                            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }
                        $str_length = strlen((string)$id)+2;
                        $id = substr("0000{$id}", -$str_length);
                        $code = $hcat->code.'-'.$id;
                        $data2 = [
                            'Code' => $code,
                            'name_of_account' => $name,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 1
                        ];
                        AccountDetails::create($data2);
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 1,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  1,
                            ]);
                            $cs=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>1,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$var->id)
                                ->where('w_id',1)
                                ->where('type',1)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$var->name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }
                    }
                    else
                    {
                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::where('name',$name)->first();
                        if($var == null)
                        {
                            $var = ProductVariants::create([
                                'p_id' => $product->id,
                                'name' => $name,
                                'status' => 1,
                                'cost' => $data[$i]['cost'],
                            ]);
                            $hcat = HeadCategory::where('name','Inventory')->first();

                            $account = AccountDetails::where('c_id',$hcat->id)
                            ->latest('created_at')->orderBy('id','desc')->first();

                            if($account == null)
                            {
                                $id = 001;
                            }
                            else
                            {
                                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                            }
                            $str_length = strlen((string)$id)+2;
                            $id = substr("0000{$id}", -$str_length);
                            $code = $hcat->code.'-'.$id;
                            $data2 = [
                                'Code' => $code,
                                'name_of_account' => $name,
                                'c_id' => $hcat->id,
                                'created_by' => $u_id,
                                'type' => 1
                            ];
                            AccountDetails::create($data2);
                        }
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 1,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  1,
                            ]);
                            $cs=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>1,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$var->id)
                                ->where('w_id',1)
                                ->where('type',1)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$var->name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost =$data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['rcv qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }

                    }




                }

            }
            else
            {
                $sin = Stocks::max('id');
                $grn = Stocks::where('id',$sin)->first();
                $Grn_no = $grn->grn_no;
                $gl = GeneralLedger::max('id');
                if($gl == null)
                {
                    $link_id = 1;
                }
                else
                {
                    $ledger = GeneralLedger::where('id',$gl)->first();
                    $link_id = $ledger->link_id ;
                }


                $brand = Brands::where('b_name',$data[$i]['brand'])->first();
                $cat = Category::where('cat_name',$data[$i]['category'])->first();
                $unit = Unit::where('u_name',$data[$i]['unit'])->first();
                $scat = Subcategory::where('s_cat_name',$data[$i]['subcategory'])->first();
                if($data[$i]['variant'] == '' && $data[$i]['size'] == ''  && $data[$i]['color'] == '' )
                {
                    $product = Products::where('brand_id',$brand->id)
                    ->where('cat_id',$cat->id)
                    ->where('unit_id',$unit->id)
                    ->where('s_cat_id',$scat->id)
                    ->where('p_type',$data[$i]['ptype'])
                    ->where('pro_name',$data[$i]['product name'])
                    ->first();
                    $total+=$data[$i]['subtotal'];
                    if($product == null)
                    {
                        $pro_code = $data[$i]['product code'];
                        $product = Products::create([
                            'pro_code' => $pro_code,
                            'pro_name' => $data[$i]['product name'],
                            'p_type' => $data[$i]['ptype'],
                            'cost' => $data[$i]['cost'],
                            'unit_id' => $unit->id,
                            'brand_id' => $brand->id,
                            'cat_id' => $cat->id,
                            's_cat_id' => $scat->id,
                            'status' => 1,
                            'visibility' => 1,
                            'created_by' => $u_id,
                            'vstatus' => 0,
                            'mstatus' => 'No',
                        ]);
                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->orderBy('id','desc')->first();

                        if($account == null)
                        {
                            $id = 001;
                        }
                        else
                        {
                            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }

                        $str_length = strlen((string)$id)+2;
                        $id = substr("0000{$id}", -$str_length);
                        $code = $hcat->code.'-'.$id;
                        $data2 = [
                            'Code' => $code,
                            'name_of_account' => $pro_code.' - '.$data[$i]['product name'],
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 0
                        ];
                        AccountDetails::create($data2);
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 0,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  0,
                            ]);
                            $cs=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>0,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$product->id)
                                ->where('w_id',1)
                                ->where('type',0)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }
                    }
                    else
                    {
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 0,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  0,
                            ]);
                            $cs=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>0,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$product->id)
                                ->where('w_id',1)
                                ->where('type',0)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $product->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 0,
                                ]);
                                $pr = Products::find($product->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  0,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }
                    }

                }
                else {
                    $product = Products::where('brand_id',$brand->id)
                    ->where('cat_id',$cat->id)
                    ->where('unit_id',$unit->id)
                    ->where('s_cat_id',$scat->id)
                    ->where('p_type',$data[$i]['ptype'])
                    ->where('pro_name',$data[$i]['product name'])
                    ->first();
                    $total+=$data[$i]['subtotal'];
                    if($product == null)
                    {
                        $pro_code = $data[$i]['product code'];
                        $product = Products::create([
                            'pro_code' => $pro_code,
                            'pro_name' => $data[$i]['product name'],
                            'p_type' => $data[$i]['ptype'],
                            'cost' => $data[$i]['cost'],
                            'unit_id' => $unit->id,
                            'brand_id' => $brand->id,
                            'cat_id' => $cat->id,
                            's_cat_id' => $scat->id,
                            'status' => 1,
                            'visibility' => 1,
                            'created_by' => $u_id,
                            'vstatus' => 0,
                            'mstatus' => 'No',
                        ]);

                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::create([
                            'p_id' => $product->id,
                            'name' => $name,
                            'status' => 1,
                            'cost' => $data[$i]['cost'],
                        ]);
                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->orderBy('id','desc')->first();

                        if($account == null)
                        {
                            $id = 001;
                        }
                        else
                        {
                            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }
                        $str_length = strlen((string)$id)+2;
                        $id = substr("0000{$id}", -$str_length);
                        $code = $hcat->code.'-'.$id;
                        $data2 = [
                            'Code' => $code,
                            'name_of_account' => $name,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 1
                        ];
                        AccountDetails::create($data2);
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 1,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  1,
                            ]);
                            $cs=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>1,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$var->id)
                                ->where('w_id',1)
                                ->where('type',1)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$var->name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }
                    }
                    else
                    {
                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::where('name',$name)->first();
                        if($var == null)
                        {
                            $var = ProductVariants::create([
                                'p_id' => $product->id,
                                'name' => $name,
                                'status' => 1,
                                'cost' => $data[$i]['cost'],
                            ]);
                            $hcat = HeadCategory::where('name','Inventory')->first();

                            $account = AccountDetails::where('c_id',$hcat->id)
                            ->latest('created_at')->orderBy('id','desc')->first();

                            if($account == null)
                            {
                                $id = 001;
                            }
                            else
                            {
                                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                            }
                            $str_length = strlen((string)$id)+2;
                            $id = substr("0000{$id}", -$str_length);
                            $code = $hcat->code.'-'.$id;
                            $data2 = [
                                'Code' => $code,
                                'name_of_account' => $name,
                                'c_id' => $hcat->id,
                                'created_by' => $u_id,
                                'type' => 1
                            ];
                            AccountDetails::create($data2);
                        }
                        $pdid = PurchaseOrderDetails::create([
                            'o_id' => $p->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['order qty'],
                            'cost' => $data[$i]['cost'],
                            'sub_total' => $data[$i]['subtotal'],
                            'type' => 1,
                            'received_quantity' => $data[$i]['rcv qty'] == '' ? 0 : $data[$i]['rcv qty'] ,
                        ]);
                        if($data[$i]['rcv qty'] != '')
                        {
                            Stocks::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['rcv qty'],
                                'stock_date' => $p->order_date,
                                's_id' => $p->s_id,
                                'purchase_d_id' => $pdid->id,
                                'cost' => $data[$i]['cost'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'grn_no' => $Grn_no,
                                'type' =>  1,
                            ]);
                            $cs=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($cs == null)
                            {
                                CurrentStock::create([
                                    'p_id' => $product->id,
                                    'w_id' => 1,
                                    'quantity' => $data[$i]['rcv qty'],
                                    'unit_quantity' => null,
                                    'type' =>1,
                                ]);
                            }
                            else {
                                $quan = $cs->quantity + $data[$i]['rcv qty'];

                                CurrentStock::where('p_id',$var->id)
                                ->where('w_id',1)
                                ->where('type',1)
                                ->update([
                                    'quantity' => $quan,
                                    'unit_quantity' => null
                                ]);
                            }
                            $sumcost=0;
                            $sumqty = 0;
                            $avg = 0;
                            $account_i = AccountDetails::where('name_of_account',$var->name)
                            ->first();
                            $debit_pro = GeneralLedger::where('account_code',$account_i->Code)
                            ->get();
                            if($debit_pro->isEmpty())
                            {
                                $sumcost = $data[$i]['cost'];
                                $sumqty = $data[$i]['rcv qty'];
                                $avg = $data[$i]['cost'];
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $data[$i]['cost'],
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);
                            }
                            else {
                                $sumcost = $ $data[$i]['cost'] * $data[$i]['rcv qty'];
                                foreach ($debit_pro as $key => $c) {
                                    $sumcost+=($c->amount * $c->stock_in);
                                    $sumqty+= $c->stock_in;
                                }

                                $avg = $sumcost / $sumqty;
                                AvgCost::create([
                                    'p_id' => $var->id,
                                    'po_id' => $p->id,
                                    'cost' => $avg,
                                    'type' => 1,
                                ]);
                                $pr = ProductVariants::find($var->id);
                                $pr->update([
                                    'cost' => $avg
                                ]);

                            }
                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger1 = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger1->link_id + 1;
                            }

                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' =>  $data[$i]['rcv qty'],
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( $data[$i]['qty'] - 0);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Purchase product for purchase order number: '.$p->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $p->order_date,
                                    'posted_date' => $p->order_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' =>  $Grn_no,
                                    'currency_code' => 'PKR',
                                    'stock_in' =>  $data[$i]['rcv qty'],
                                    'stock_out' => '0',
                                    'net_value' =>  $data[$i]['rcv qty'],
                                    'balance' => $net_d_p,
                                    'debit' => ( $avg *  $data[$i]['rcv qty']),
                                    'credit' => 0,
                                    'w_id' => 1,
                                    'type' =>  1,
                                    'amount' => ( $avg)
                                ]);
                            }
                        }

                    }




                }
            }
       }
        toastr()->success('Purchase orders added successfully!');

        return redirect()->back();
    }

    ////// sale order
    public function saleOrder(Request $request)
    {
        $request->validate([
            'csv_file' => 'required',
       ]);
       $filePath       =   pathinfo($request->csv_file)['dirname'].'/'.pathinfo($request->csv_file)['basename'];
       $data           =   readCsv($filePath);
       $u_id = Auth::user()->id;
       $total = 0;
        for ($i = 0; $i <= count($data); $i ++)
        {
            if($i == count($data))
            {
                $saleid = Sales::max('id');
                Sales::where('id',$saleid)->update(['total' => $total]);
                $saledata = Sales::find($saleid);
                if($saledata->s_status != 'Pending')
                {
                    if($saledata->s_status == 'Delivered')
                    {
                        $account_s = AccountDetails::where('name_of_account','Sales')->first();
                        $sales = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $account_c = AccountDetails::where('name_of_account',$v->company.' - '.$v->name)
                        ->first();
                        // dd($account_c);
                        $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                        ->get();
                        $sales_n = 0;
                        $sales_d = 0;
                        $sales_c = 0;
                        $posted_date = Carbon::parse($saledata->sale_date)->format('Y-m-d');
                        $period = Carbon::parse($saledata->sale_date)->format('M-y');

                        if($sales->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $total,
                                'net_value' =>  0 - $total,
                                'balance' =>  0 - $total
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($sales as $key => $d) {
                                $sales_d+=$d->debit;
                                $sales_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $sales_c += $total;
                            // $sales_n = $sales_d - $sales_c;
                            $sales_n = $balance + (0 - $total);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => 2,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $total,
                                'net_value' => 0 - $total,
                                'balance' => $sales_n
                            ]);
                        }

                        $customer_d = 0;
                        $customer_c = 0;
                        $customer_n = 0;
                        if($customer_l->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to'.$v->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => 2,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $total,
                                'credit' => '0',
                                'net_value' => $total,
                                'balance' => $total
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($customer_l as $key => $c) {
                                $customer_c+=$c->credit;
                                $customer_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $customer_d += $total;
                            // $customer_n = $customer_d - $customer_c;
                            $customer_n = $balance + ($total - 0);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$v->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => 2,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $total,
                                'credit' => '0',
                                'net_value' => $total,
                                'balance' => $customer_n
                            ]);
                        }
                        $total =0;
                    }
                    if($saledata->s_status == 'Partial')
                    {
                        $sdetails = SaleDetails::where('s_id',$saleid)->get();
                        $amount = 0;
                        foreach ($sdetails as $key => $sd) {
                            if($sd->delivered_quantity != 0)
                            {
                                $amount += ($sd->sub_total / $sd->quantity) * $sd->delivered_quantity;
                            }
                        }
                        $account_s = AccountDetails::where('name_of_account','Sales')->first();
                        $sales = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $account_c = AccountDetails::where('name_of_account',$v->company.' - '.$v->name)
                        ->first();
                        // dd($account_c);
                        $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                        ->get();
                        $sales_n = 0;
                        $sales_d = 0;
                        $sales_c = 0;
                        $posted_date = Carbon::parse($saledata->sale_date)->format('Y-m-d');
                        $period = Carbon::parse($saledata->sale_date)->format('M-y');

                        if($sales->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $amount,
                                'net_value' =>  0 - $amount,
                                'balance' =>  0 - $amount
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($sales as $key => $d) {
                                $sales_d+=$d->debit;
                                $sales_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $sales_c += $amount;
                            // $sales_n = $sales_d - $sales_c;
                            $sales_n = $balance + (0 - $amount);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$v->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => 2,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $amount,
                                'net_value' => 0 - $amount,
                                'balance' => $sales_n
                            ]);
                        }

                        $customer_d = 0;
                        $customer_c = 0;
                        $customer_n = 0;
                        if($customer_l->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to'.$v->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => 2,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $amount,
                                'credit' => '0',
                                'net_value' => $amount,
                                'balance' => $amount
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($customer_l as $key => $c) {
                                $customer_c+=$c->credit;
                                $customer_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $customer_d += $amount;
                            // $customer_n = $customer_d - $customer_c;
                            $customer_n = $balance + ($amount - 0);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$v->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => 2,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $amount,
                                'credit' => '0',
                                'net_value' => $amount,
                                'balance' => $customer_n
                            ]);
                        }
                        $total =0;
                    }
                }
                break;
            }
            $posted_date = Carbon::createFromFormat('d/m/Y', $data[$i]['Sale date'])->format('Y-m-d');
            $period = Carbon::createFromFormat('d/m/Y', $data[$i]['Sale date'])->format('M-y');
            $v =Vendors::where('company',$data[$i]['company'])->where('name',$data[$i]['customer name'])->where('v_type','Customer')->first();
            if($v == null)
            {
                $hcat = HeadCategory::where('name','Receivables')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;

                $data2 = [
                    'Code' => $code,
                    'name_of_account' => $data[$i]['company'].' - '.$data[$i]['customer name'],
                    'c_id' => $hcat->id,
                    'created_by' => $u_id
                ];
                AccountDetails::create($data2);

                $v = Vendors::create([
                    'company' => $data[$i]['company'],
                    'name' => $data[$i]['customer name'],
                    'v_type' => 'Customer',
                    'created_by' => $u_id,
                    'status' => 1
                ]);
            }
            $sp =Vendors::where('name',$data[$i]['Saleperson'])->where('v_type','Saleperson')->first();
            if($sp == null)
            {
                $sp = Vendors::create([
                    'name' => $data[$i]['Saleperson'],
                    'v_type' => 'Saleperson',
                    'created_by' => $u_id,
                    'status' => 1
                ]);
            }

            $biller =Vendors::where('name',$data[$i]['biller name'])->where('v_type','Biller')->first();
            if($biller == null)
            {
                $biller = Vendors::create([
                    'name' => $data[$i]['biller name'],
                    'company' => $data[$i]['biller name'],
                    'v_type' => 'Biller',
                    'created_by' => $u_id,
                    'status' => 1
                ]);
            }

            $saletype = SaleType::where('name',$data[$i]['sale type'])->first();
            if($saletype == null)
            {
                $saletype = SaleType::create([
                        'name' => $data[$i]['sale type']
                ]);
            }


            $invno = 0;

            $maxid = Sales::where('Iformat',$data[$i]['Invoice Type'])->where('b_id',$biller->id)->max('id');

            if($data[$i]['Invoice Type'] == 'GST')
            {
                $invno = $data[$i]['Invoice number'];
            }
            else
            {

                if($maxid == null)
                {
                    $invno = 1;
                }
                else {

                    $Ino = Sales::where('id',$maxid)->pluck('Ino');
                    if($Ino[0] == '')
                    {
                        $invno = 1;
                    }
                    else {
                        $invno = $Ino[0]+1;
                    }
                }
            }
            if($data[$i]['Invoice Type'] == 'GST')
            {
                $sales = Sales::where('sale_date',$posted_date)
                ->where('Iformat',$data[$i]['Invoice Type'])->where('Ino',$invno)->first();
                $gdn_no = rand();

                if($sales == null)
                {
                    $saleid = Sales::max('id');

                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id + 1;
                    }
                    if($i != 0)
                    {
                        if($saleid != null)
                        {
                            Sales::where('id',$saleid)->update(['total' => $total]);
                            $saledata = Sales::find($saleid);
                            if($saledata->s_status != 'Pending')
                            {
                                if($saledata->s_status == 'Delivered')
                                {
                                    $account_s = AccountDetails::where('name_of_account','Sales')->first();
                                    $sales = GeneralLedger::where('account_code',$account_s->Code)
                                    ->get();
                                    $account_c = AccountDetails::where('name_of_account',$v->company.' - '.$v->name)
                                    ->first();
                                    // dd($account_c);
                                    $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                                    ->get();
                                    $sales_n = 0;
                                    $sales_d = 0;
                                    $sales_c = 0;
                                    $posted_date = Carbon::parse($saledata->sale_date)->format('Y-m-d');
                                    $period = Carbon::parse($saledata->sale_date)->format('M-y');

                                    if($sales->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => $u_id,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $total,
                                            'net_value' =>  0 - $total,
                                            'balance' =>  0 - $total
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($sales as $key => $d) {
                                            $sales_d+=$d->debit;
                                            $sales_c+=$d->credit;
                                            $balance+=$d->net_value;
                                        }
                                        $sales_c += $total;
                                        // $sales_n = $sales_d - $sales_c;
                                        $sales_n = $balance + (0 - $total);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $total,
                                            'net_value' => 0 - $total,
                                            'balance' => $sales_n
                                        ]);
                                    }

                                    $customer_d = 0;
                                    $customer_c = 0;
                                    $customer_n = 0;
                                    if($customer_l->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to'.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $total,
                                            'credit' => '0',
                                            'net_value' => $total,
                                            'balance' => $total
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($customer_l as $key => $c) {
                                            $customer_c+=$c->credit;
                                            $customer_d+=$c->debit;
                                            $balance+=$c->net_value;
                                        }
                                        $customer_d += $total;
                                        // $customer_n = $customer_d - $customer_c;
                                        $customer_n = $balance + ($total - 0);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $total,
                                            'credit' => '0',
                                            'net_value' => $total,
                                            'balance' => $customer_n
                                        ]);
                                    }
                                    $total =0;
                                }
                                if($saledata->s_status == 'Partial')
                                {
                                    $sdetails = SaleDetails::where('s_id',$saleid)->get();
                                    $amount = 0;
                                    foreach ($sdetails as $key => $sd) {
                                        if($sd->delivered_quantity != 0)
                                        {
                                            $amount += ($sd->sub_total / $sd->quantity) * $sd->delivered_quantity;
                                        }
                                    }
                                    $account_s = AccountDetails::where('name_of_account','Sales')->first();
                                    $sales = GeneralLedger::where('account_code',$account_s->Code)
                                    ->get();
                                    $account_c = AccountDetails::where('name_of_account',$v->company.' - '.$v->name)
                                    ->first();
                                    // dd($account_c);
                                    $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                                    ->get();
                                    $sales_n = 0;
                                    $sales_d = 0;
                                    $sales_c = 0;
                                    $posted_date = Carbon::parse($saledata->sale_date)->format('Y-m-d');
                                    $period = Carbon::parse($saledata->sale_date)->format('M-y');

                                    if($sales->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => $u_id,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $amount,
                                            'net_value' =>  0 - $amount,
                                            'balance' =>  0 - $amount
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($sales as $key => $d) {
                                            $sales_d+=$d->debit;
                                            $sales_c+=$d->credit;
                                            $balance+=$d->net_value;
                                        }
                                        $sales_c += $amount;
                                        // $sales_n = $sales_d - $sales_c;
                                        $sales_n = $balance + (0 - $amount);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $amount,
                                            'net_value' => 0 - $amount,
                                            'balance' => $sales_n
                                        ]);
                                    }

                                    $customer_d = 0;
                                    $customer_c = 0;
                                    $customer_n = 0;
                                    if($customer_l->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to'.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $amount,
                                            'credit' => '0',
                                            'net_value' => $amount,
                                            'balance' => $amount
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($customer_l as $key => $c) {
                                            $customer_c+=$c->credit;
                                            $customer_d+=$c->debit;
                                            $balance+=$c->net_value;
                                        }
                                        $customer_d += $amount;
                                        // $customer_n = $customer_d - $customer_c;
                                        $customer_n = $balance + ($amount - 0);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $amount,
                                            'credit' => '0',
                                            'net_value' => $amount,
                                            'balance' => $customer_n
                                        ]);
                                    }
                                    $total =0;
                                }
                            }
                            $total = 0;

                        }
                        else
                        {
                            $total =0;
                        }

                    }
                    $date = Carbon::createFromFormat('d/m/Y', $data[$i]['Sale date'])->format('Y-m-d');
                    $days = null;
                    if($data[$i]['pay date'] != '')
                    {
                        $paydate = Carbon::createFromFormat('d/m/Y', $data[$i]['pay date'])->format('Y-m-d');
                        $date1=date_create($posted_date);
                        $date2=date_create($paydate);
                        $diff = $diff=date_diff($date1,$date2);
                        $daye = $diff->format("%a Days");
                    }
                    else
                    {
                        $days = null;
                    }
                    $datasale=([
                        'sale_date' => $date,
                        'ref_no' => $data[$i]['Refno'],
                        'b_id' => $biller->id,
                        'c_id' => $v->id,
                        'w_id' => 1,
                        's_address' => $data[$i]['address'],
                        's_status' => $data[$i]['Sale status'],
                        'p_status' => $data[$i]['Payment status'],
                        'created_by' => $u_id,
                        'sp_id' => $sp->id,
                        'expected_date' => $data[$i]['expected delivery date'] == '' ? null : $data[$i]['expected delivery date'],
                        'advance' => 'No',
                        'pay_type' => $data[$i]['Payment mode'],
                        'return_status' => 'No Return',
                        'Iformat' => $data[$i]['Invoice Type'],
                        'Itype' => $data[$i]['Price type'],
                        'Ino' => $invno,
                        'pdate' => $data[$i]['pay date'] == '' ? null :  $data[$i]['pay date'],
                        'remarks' => $data[$i]['remarks'],
                        'st_id' => $saletype->id,
                    ]);
                    $total+=$data[$i]['Total'];
                    $sales=Sales::create($datasale);


                    $brand = Brands::where('b_name',$data[$i]['brand'])->first();
                    if($data[$i]['variant'] == '' && $data[$i]['size'] == ''  && $data[$i]['color'] == '' )
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => trim($product->cost),
                            'Itype' => $data[$i]['Price type'],
                            'type' => 0,
                            'vet' => $data[$i]['Value Ex tax'],
                            'afterDiscount' => $data[$i]['After discount amount'],
                            'taxA' => $data[$i]['Tax amount'],
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 0
                            ]);
                            $prod=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }
                    }
                    else
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::where('name',$name)->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => $product->cost,
                            'Itype' => $data[$i]['Price type'],
                            'type' => 1,
                            'vet' => $data[$i]['Value Ex tax'],
                            'afterDiscount' => $data[$i]['After discount amount'],
                            'taxA' => $data[$i]['Tax amount'],
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' => 1,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 1
                            ]);
                            $prod=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }

                    }
                }
                else {
                    $date = Carbon::createFromFormat('d/m/Y', $data[$i]['Sale date'])->format('Y-m-d');
                    $sout = StockOut::max('id');
                    $gdn = StockOut::where('id',$sout)->first();
                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id ;
                    }
                    $gdn_no = $gdn->gdn_no;
                    $total+=$data[$i]['Total'];

                    $brand = Brands::where('b_name',$data[$i]['brand'])->first();
                    if($data[$i]['variant'] == '' && $data[$i]['size'] == ''  && $data[$i]['color'] == '' )
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => trim($product->cost),
                            'Itype' => $data[$i]['Price type'],
                            'type' => 0,
                            'vet' => $data[$i]['Value Ex tax'],
                            'afterDiscount' => $data[$i]['After discount amount'],
                            'taxA' => $data[$i]['Tax amount'],
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 0
                            ]);
                            $prod=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }
                    }
                    else
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::where('name',$name)->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => $product->cost,
                            'Itype' => $data[$i]['Price type'],
                            'type' => 1,
                            'vet' => $data[$i]['Value Ex tax'],
                            'afterDiscount' => $data[$i]['After discount amount'],
                            'taxA' => $data[$i]['Tax amount'],
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' => 1,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 1
                            ]);
                            $prod=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }

                    }


                }
            }
            else
            {

               $saleid = Sales::max('id');

               $sales = Sales::where('sale_date',$posted_date)
               ->where('Iformat',$data[$i]['Invoice Type'])->where('b_id',$biller->id)
               ->where('c_id',$v->id)->where('sp_id',$sp->id)->first();
               $gdn_no = rand();

                if($sales == null)
                {

                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id + 1;
                    }
                    if($i != 0)
                    {
                        if($saleid != null)
                        {
                            Sales::where('id',$saleid)->update(['total' => $total]);
                            $saledata = Sales::find($saleid);
                            if($saledata->s_status != 'Pending')
                            {
                                if($saledata->s_status == 'Delivered')
                                {
                                    $account_s = AccountDetails::where('name_of_account','Sales')->first();
                                    $sales = GeneralLedger::where('account_code',$account_s->Code)
                                    ->get();
                                    $account_c = AccountDetails::where('name_of_account',$v->company.' - '.$v->name)
                                    ->first();
                                    // dd($account_c);
                                    $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                                    ->get();
                                    $sales_n = 0;
                                    $sales_d = 0;
                                    $sales_c = 0;
                                    $posted_date = Carbon::parse($saledata->sale_date)->format('Y-m-d');
                                    $period = Carbon::parse($saledata->sale_date)->format('M-y');

                                    if($sales->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => $u_id,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $total,
                                            'net_value' =>  0 - $total,
                                            'balance' =>  0 - $total
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($sales as $key => $d) {
                                            $sales_d+=$d->debit;
                                            $sales_c+=$d->credit;
                                            $balance+=$d->net_value;
                                        }
                                        $sales_c += $total;
                                        // $sales_n = $sales_d - $sales_c;
                                        $sales_n = $balance + (0 - $total);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $total,
                                            'net_value' => 0 - $total,
                                            'balance' => $sales_n
                                        ]);
                                    }

                                    $customer_d = 0;
                                    $customer_c = 0;
                                    $customer_n = 0;
                                    if($customer_l->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to'.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $total,
                                            'credit' => '0',
                                            'net_value' => $total,
                                            'balance' => $total
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($customer_l as $key => $c) {
                                            $customer_c+=$c->credit;
                                            $customer_d+=$c->debit;
                                            $balance+=$c->net_value;
                                        }
                                        $customer_d += $total;
                                        // $customer_n = $customer_d - $customer_c;
                                        $customer_n = $balance + ($total - 0);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $total,
                                            'credit' => '0',
                                            'net_value' => $total,
                                            'balance' => $customer_n
                                        ]);
                                    }
                                    $total =0;
                                }
                                if($saledata->s_status == 'Partial')
                                {
                                    $sdetails = SaleDetails::where('s_id',$saleid)->get();
                                    $amount = 0;
                                    foreach ($sdetails as $key => $sd) {
                                        if($sd->delivered_quantity != 0)
                                        {
                                            $amount += ($sd->sub_total / $sd->quantity) * $sd->delivered_quantity;
                                        }
                                    }
                                    $account_s = AccountDetails::where('name_of_account','Sales')->first();
                                    $sales = GeneralLedger::where('account_code',$account_s->Code)
                                    ->get();
                                    $account_c = AccountDetails::where('name_of_account',$v->company.' - '.$v->name)
                                    ->first();
                                    // dd($account_c);
                                    $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                                    ->get();
                                    $sales_n = 0;
                                    $sales_d = 0;
                                    $sales_c = 0;
                                    $posted_date = Carbon::parse($saledata->sale_date)->format('Y-m-d');
                                    $period = Carbon::parse($saledata->sale_date)->format('M-y');

                                    if($sales->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => $u_id,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $amount,
                                            'net_value' =>  0 - $amount,
                                            'balance' =>  0 - $amount
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($sales as $key => $d) {
                                            $sales_d+=$d->debit;
                                            $sales_c+=$d->credit;
                                            $balance+=$d->net_value;
                                        }
                                        $sales_c += $amount;
                                        // $sales_n = $sales_d - $sales_c;
                                        $sales_n = $balance + (0 - $amount);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_s->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_s->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => '0',
                                            'credit' => $amount,
                                            'net_value' => 0 - $amount,
                                            'balance' => $sales_n
                                        ]);
                                    }

                                    $customer_d = 0;
                                    $customer_c = 0;
                                    $customer_n = 0;
                                    if($customer_l->isEmpty())
                                    {
                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to'.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $amount,
                                            'credit' => '0',
                                            'net_value' => $amount,
                                            'balance' => $amount
                                        ]);
                                    }
                                    else
                                    {
                                        $balance = 0;
                                        foreach ($customer_l as $key => $c) {
                                            $customer_c+=$c->credit;
                                            $customer_d+=$c->debit;
                                            $balance+=$c->net_value;
                                        }
                                        $customer_d += $amount;
                                        // $customer_n = $customer_d - $customer_c;
                                        $customer_n = $balance + ($amount - 0);

                                        GeneralLedger::create([
                                            'source' => 'Automated',
                                            'description' => 'Sales to '.$v->name,
                                            'account_name' => $account_c->name_of_account,
                                            'link_id' => $link_id,
                                            'created_by' => 2,
                                            'accounting_date' => $posted_date,
                                            'posted_date' => $posted_date,
                                            'period' => $period,
                                            'account_code' => $account_c->Code,
                                            'transaction_no' => $gdn_no,
                                            'currency_code' => 'PKR',
                                            'debit' => $amount,
                                            'credit' => '0',
                                            'net_value' => $amount,
                                            'balance' => $customer_n
                                        ]);
                                    }
                                    $total =0;
                                }
                            }
                            $total = 0;
                        }
                        else
                        {
                            $total =0;
                        }

                    }
                    $date = Carbon::createFromFormat('d/m/Y', $data[$i]['Sale date'])->format('Y-m-d');
                    $days = null;
                    if($data[$i]['pay date'] != '')
                    {
                        $paydate = Carbon::createFromFormat('d/m/Y', $data[$i]['pay date'])->format('Y-m-d');
                        $date1=date_create($posted_date);
                        $date2=date_create($paydate);
                        $diff = $diff=date_diff($date1,$date2);
                        $daye = $diff->format("%a Days");
                    }
                    else
                    {
                        $days = null;
                    }
                    $datasale=([
                        'sale_date' => $date,
                        'ref_no' => $data[$i]['Refno'],
                        'b_id' => $biller->id,
                        'c_id' => $v->id,
                        'w_id' => 1,
                        's_address' => $data[$i]['address'],
                        's_status' => $data[$i]['Sale status'],
                        'p_status' => $data[$i]['Payment status'],
                        'created_by' => $u_id,
                        'sp_id' => $sp->id,
                        'expected_date' => $data[$i]['expected delivery date'] == '' ? null : $data[$i]['expected delivery date'],
                        'advance' => 'No',
                        'pay_type' => $data[$i]['Payment mode'],
                        'return_status' => 'No Return',
                        'Iformat' => $data[$i]['Invoice Type'],
                        'Itype' => $data[$i]['Price type'],
                        'Ino' => $invno,
                        'pdate' => $data[$i]['pay date'] == '' ? null : $data[$i]['pay date'],
                        'remarks' => $data[$i]['remarks'],
                        'st_id' => $saletype->id,
                    ]);
                    $total+=$data[$i]['Total'];
                    $sales=Sales::create($datasale);

                    $brand = Brands::where('b_name',$data[$i]['brand'])->first();
                    if($data[$i]['variant'] == '' && $data[$i]['size'] == ''  && $data[$i]['color'] == '' )
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => trim($product->cost),
                            'Itype' => $data[$i]['Price type'],
                            'type' => 0,
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 0
                            ]);
                            $prod=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }
                    }
                    else
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::where('name',$name)->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => $product->cost,
                            'Itype' => $data[$i]['Price type'],
                            'type' => 1,
                            'taxA' => $data[$i]['Tax amount'],
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' => 1,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 1
                            ]);
                            $prod=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }

                    }
                }
                else
                {
                    $date = Carbon::createFromFormat('d/m/Y', $data[$i]['Sale date'])->format('Y-m-d');
                    $sout = StockOut::max('id');
                    $gdn = StockOut::where('id',$sout)->first();
                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id ;
                    }
                    $gdn_no = $gdn->gdn_no;
                    $total+=$data[$i]['Total'];

                    $brand = Brands::where('b_name',$data[$i]['brand'])->first();
                    if($data[$i]['variant'] == '' && $data[$i]['size'] == ''  && $data[$i]['color'] == '' )
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $product->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => trim($product->cost),
                            'Itype' => $data[$i]['Price type'],
                            'type' => 0,
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 0,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $product->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 0
                            ]);
                            $prod=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$product->id)
                            ->where('w_id',1)
                            ->where('type',0)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }
                    }
                    else
                    {
                        $product = Products::where('brand_id',$brand->id)
                        ->where('p_type',$data[$i]['product type'])
                        ->where('pro_name',$data[$i]['product name'])
                        ->first();
                        $name = $product->pro_code.' - '.$data[$i]['product name'].'-';
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['color'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
                        }
                        if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['variant'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
                        {
                            $name.=$data[$i]['size'];
                        }
                        if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
                        {
                            $name.=$data[$i]['color'];
                        }
                        $var = ProductVariants::where('name',$name)->first();
                        $sd = SaleDetails::create([
                            's_id' => $sales->id,
                            'p_id' => $var->id,
                            'quantity' => $data[$i]['Qty'],
                            'sub_total' => $data[$i]['Total'],
                            'price' => $data[$i]['Price'],
                            'delivered_quantity' => $data[$i]['deliver qty'] == '' ? 0 : $data[$i]['deliver qty'],
                            'discount_percent' => $data[$i]['Discount%'],
                            'discounted_amount' => $data[$i]['Discount amount'],
                            'cost' => $product->cost,
                            'Itype' => $data[$i]['Price type'],
                            'type' => 1,
                        ]);
                        if($data[$i]['deliver qty'] != '')
                        {
                            $account_i = AccountDetails::where('name_of_account',$name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',1)
                            ->get();

                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => 0 - $data[$i]['deliver qty'],
                                    'credit' =>  trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' => 1,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $data[$i]['deliver qty'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $data[$i]['deliver qty'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $data[$i]['deliver qty'],
                                    'balance' => $net_d_p,
                                    'credit' => trim($product->cost) * $data[$i]['deliver qty'],
                                    'debit' => 0,
                                    'w_id' =>1 ,
                                    'type' => 1,
                                    'amount' => 0 - trim($product->cost)
                                ]);
                            }

                            StockOut::create([
                                'p_id' => $var->id,
                                'quantity' => $data[$i]['deliver qty'],
                                'stock_date' => $date,
                                'sale_d_id' => $sd->id,
                                'price' => $data[$i]['Price'],
                                'created_by' => $u_id,
                                'w_id' => 1,
                                'gdn_no' => $gdn_no,
                                's_type' => 'Sales',
                                'type' => 1
                            ]);
                            $prod=CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->first();
                            if($product->unit->u_name == 'Liter' || $product->unit->u_name == 'Kilograms')
                            {
                                $unit_quantity = $data[$i]['deliver qty'];
                            }
                            else if($product->unit->u_name == 'Mililiter' || $product->unit->u_name == 'Grams')
                            {
                                $unit_quantity =  $data[$i]['deliver qty'];

                            }
                            else
                            {
                                $unit_quantity = null;
                            }

                            if($prod->unit_quantity == null)//changes
                            {
                                $u_quan=null;
                            }
                            else
                            {
                                $u_quan = $prod->unit_quantity - $unit_quantity;
                            }
                            if($prod->quantity == null)
                            {
                                $quan = null;
                            }
                            else {
                                $quan=$prod->quantity - $data[$i]['deliver qty'];

                            }

                            // //for current stock

                            CurrentStock::where('p_id',$var->id)
                            ->where('w_id',1)
                            ->where('type',1)
                            ->update([
                                'quantity' => $quan,
                                'unit_quantity' => $u_quan
                            ]);
                        }

                    }


                }
            }

        }
        toastr()->success('Sale orders added successfully!');
        return redirect()->back();
    }

    /////// receivables opening account
    public function receivables(Request $request)
    {
        $request->validate([
            'csv_file' => 'required',
        ]);
        $filePath       =   pathinfo($request->csv_file)['dirname'].'/'.pathinfo($request->csv_file)['basename'];
        $data           =   readCsv($filePath);
        $u_id = Auth::user()->id;
        for ($i=0; $i <count($data) ; $i++)
        {
            $vendor = Vendors::where('name',$data[$i]['Customer Name'])->where('company',$data[$i]['company'])->where('v_type','Customer')->first();
            if($vendor == null)
            {
                $hcat = HeadCategory::where('name','Receivables')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;

                $data2 = [
                    'Code' => $code,
                    'name_of_account' => $data[$i]['company'].' - '.$data[$i]['Customer Name'],
                    'c_id' => $hcat->id,
                    'created_by' => $u_id
                ];
                $account_c = AccountDetails::create($data2);

                $v = Vendors::create([
                    'company' => $data[$i]['company'],
                    'name' => $data[$i]['Customer Name'],
                    'opening_balance' => $data[$i]['Amount'],
                    'v_type' => 'Customer',
                    'created_by' => $u_id,
                    'status' => 1
                ]);

                $id = GeneralLedger::max('id');
                $ledger = GeneralLedger::where('id',$id)->first();
                if($ledger == null)
                {
                    $link_id=1;
                }
                else
                {
                    $link_id = $ledger->link_id + 1;
                }

                $posted_date = Carbon::createFromFormat('d/m/Y', $data[$i]['opening date'])->format('Y-m-d');
                $period = Carbon::createFromFormat('d/m/Y', $data[$i]['opening date'])->format('M-y');
                $customer_d = 0;
                $customer_c = 0;
                $customer_n = 0;
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Opening account',
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' =>  $posted_date,
                    'posted_date' =>  $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'currency_code' => 'PKR',
                    'debit' => $data[$i]['Amount'],
                    'credit' => '0',
                    'net_value' => $data[$i]['Amount'],
                    'balance' => $data[$i]['Amount']
                ]);
            }
            else {
                $account_c = AccountDetails::where('name_of_account',$vendor->company.' - '.$vendor->name)->first() ;

                $id = GeneralLedger::max('id');
                $ledger = GeneralLedger::where('id',$id)->first();
                if($ledger == null)
                {
                    $link_id=1;
                }
                else
                {
                    $link_id = $ledger->link_id + 1;
                }
                $posted_date = Carbon::createFromFormat('d/m/Y', $data[$i]['opening date'])->format('Y-m-d');
                $period = Carbon::createFromFormat('d/m/Y', $data[$i]['opening date'])->format('M-y');
                $customer_d = 0;
                $customer_c = 0;
                $customer_n = 0;
                $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                ->where('description','Opening Account')
                ->where('posted_date',$posted_date)
                ->where('period',$period)
                ->first();
                if($customer_l != null)
                {
                    $balance = 0;
                    $balance = $customer_l->debit +  $data[$i]['Amount'];
                    // // dd($balance,$vendor->name);
                    GeneralLedger::where('id',$customer_l->id)
                    ->update([
                        'debit' => $balance,
                        'credit' => '0',
                        'net_value' => $balance,
                        'balance' => $balance
                    ]);
                }

            }
        }
        toastr()->success('Customers opening account added successfully!');
        return redirect()->back();
    }
}
