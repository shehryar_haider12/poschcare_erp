<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\City;
use App\Warehouse;
use Auth;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $city=City::all();
        return view('warehouse.index',compact('city','permissions'));
    }

    public function datatable()
    {
        $ware=Warehouse::with(['city'])
        ->get();
        return DataTables::of($ware)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $city=City::where('status',1)->get();
        $data=[
            'isEdit' => false,
            'city' => $city,
            'permissions' => getRolePermission($menu_id)
        ];
        // dd($cat);
        return view('warehouse.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'w_name'      =>  'required|string|max:255|unique:warehouse',
            'w_address'      =>  'required',
        ]);
        $u_id = Auth::user()->id;
        Warehouse::create([
            'w_name' => $request->w_name,
            'w_address' => $request->w_address,
            'w_type' => $request->w_type,
            'w_contactNo' => $request->w_contactNo,
            'c_p_name' => $request->c_p_name,
            'c_p_contactNo' => $request->c_p_contactNo,
            'c_id' => $request->c_id,
            'created_by' => $u_id
        ]);
        toastr()->success('Warehouse added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $ware=Warehouse::with(['city'])
            ->where('id',$id)
            ->first();
            return $ware;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ware=Warehouse::with(['city'])
        ->where('id',$id)
        ->first();
        $city=City::all();
        $data=[
            'isEdit' => true,
            'ware' => $ware,
            'city' => $city
        ];
        return view('warehouse.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'w_name'      =>  'required|string|max:255|unique:warehouse,w_name,'.$id,
            'w_address'      =>  'required|string|max:255|unique:warehouse,w_address,'.$id,
            // 'w_contactNo'      =>  'required|string|max:255|unique:warehouse,w_contactNo,'.$id
        ]);
        $u_id = Auth::user()->id;
        Warehouse::where('id',$id)
        ->update([
            'w_name' => $request->w_name,
            'c_id' => $request->c_id,
            'w_type' => $request->w_type,
            'updated_by' => $u_id
        ]);
        toastr()->success('Warehouse updated successfully!');
        return redirect(url('').'/warehouse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
