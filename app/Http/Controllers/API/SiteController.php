<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Category;
use App\Subcategory;
use App\Brands;
use App\Vendors;
use App\Sales;
use App\SaleReturn;
use App\SaleCounter;
use App\Counter;
use App\SaleDetails;
use App\GeneralLedger;
use App\Bank;
use App\CurrentStock;
use App\AccountDetails;
use App\TransactionHistory;
use DB;
use Auth;
use Session;
use Carbon\Carbon;
use App\StockOut;

class SiteController extends Controller
{
    public function getProduct(Request $request, $brand = 0, $category = 0)
    {
        if($category && $brand){

            $products = Products::where([['cat_id',$category],['brand_id',$brand],['visibility',1],['status',1]])->get();
        }
        elseif ($category) {

            $products = Products::where([['cat_id',$category],['visibility',1],['status',1]])->get();
        }
        elseif ($brand) {

            $products = Products::where([['brand_id',$brand],['visibility',1],['status',1]])->get();
        }else{

            $products = Products::where([['visibility',1],['status',1]])->get();
        }
        return response()->json([
            'product'   =>  $products
        ],200);
    }

    public function getCategory()
    {
        $category = Category::where([['status',1]])->get();
        return response()->json([
            'category'   =>  $category
        ],200);
    }

    public function getSubCategory(Request $request, $category)
    {
        $category = Subcategory::where([['cat_id',$category],['status',1]])->get();
        return response()->json([
            'category'   =>  $category
        ], 200);
    }

    public function getBrand()
    {
        $brand = Brands::where([['status',1]])->get();
        return response()->json([
            'brand'   =>  $brand
        ], 200);
    }


    public function saleOrder(Request $request)
    {
        $data = [
            'order_no' => $request->order,
        ];
        Sale::create($data);
        return "ok";
        $amount = 0;
        $sid=0;
        $w_id = 1;
        // $u_id = Auth::user()->id;
        $s_status = 'Delivered';
        $sale_date = Carbon::now()->format('Y-m-d');
        $p_status = 'Paid';
        $total = 0;

        // if (isset($request->tax)) {
        //     $tax = $request->tax;
        //     $tax_status = 'Yes';
        // } else {
            $tax =null;
            $tax_status = 'No';
        // }

        $customerid= Vendors::where('v_type','Customer')->where('company','WooCommerce')->pluck('id');


        foreach ($request->line_items as $key => $i) {
            $p = Products::find($i['product_id']);
            $total+=$p->price * $i['quantity'];
        }
        $data=([
            'sale_date' => $sale_date,
            'b_id' => 770,
            'c_id' => $customerid[0],
            'w_id' => $w_id,
            's_address' => $request->shipping[0]['address_1'],
            's_status' => $s_status,
            'p_status' => $p_status,
            'tax' => $tax,
            'total' => $total,
            // 'note' => $request->sale_note,
            // 'created_by' => $u_id,
            // 'sp_id' => $request->sp_id,
            // 'discount' => $discount,
            // 'expected_date' => $request->expected_date,
            'advance' => 'No',
            'tax_status' => $tax_status,
            'pay_type' => 'Cash on Delivery',
            'return_status' => 'No Return',
            's_type' => 'Pos',
        ]);
        $p=Sales::create($data);
        // SaleCounter::create([
        //     'u_id' => $u_id,
        //     'co_id' => 1,
        //     's_id' => $p->id,
        //     'date' => $sale_date,
        // ]);
        for ($i=0; $i < count($request->line_items)  ; $i++)
        {
            // $product = Products::find($request->line_items[$i]['product_id']);
            $product = Products::with('unit')
            ->where('id',$request->line_items[$i]['product_id'])
            ->first();
            $sid = SaleDetails::create([
                's_id' => $p->id,
                'p_id' => $request->line_items[$i]['product_id'],
                'quantity' => $request->line_items[$i]['quantity'],
                'sub_total' => ($product->price * $request->line_items[$i]['quantity']),
                'price' => $product->price,
                // 'discount_percent' => $request->discount_percent[$i],
                // 'discounted_amount' => $request->discounted_amount[$i],
                'type' => 0,
                'delivered_quantity' => $request->line_items[$i]['quantity'],
            ]);

            // dd($sid);
            $product = Products::with('unit')
            ->where('id',$request->line_items[$i]['product_id'])
            ->first();

            $prod=CurrentStock::where('p_id',$request->line_items[$i]['product_id'])
            ->where('w_id',$w_id)
            ->where('type',0)
            ->first();

            if($product->unit->u_name == 'Liter')
            {
                $unit_quantity = ( 1000) * $request->line_items[$i]['quantity'];
            }
            else if($product->unit->u_name == 'Mililiter')
            {
                $unit_quantity =  $request->line_items[$i]['quantity'];
            }
            else
            {
                $unit_quantity = null;
            }

            if($prod->unit_quantity == null)//changes
            {
                $u_quan=null;
            }
            else
            {
                $u_quan = $prod->unit_quantity - $unit_quantity;
            }

            $quan=$prod->quantity - $request->line_items[$i]['quantity'];
            // $amount += $request->sub_total[$i];
            CurrentStock::where('p_id',$request->line_items[$i]['product_id'])
            ->where('w_id',$w_id)
            ->where('type',0)
            ->update([
                'quantity' => $quan,
                'unit_quantity' => $u_quan
            ]);
            // dd($sid);
            StockOut::create([
                'p_id' => $request->line_items[$i]['product_id'],
                'quantity' => $request->line_items[$i]['quantity'],
                'sale_d_id' => $sid->id,
                'stock_date' => $sale_date,
                's_type' => 'Sales',
                'price' => $product->price,
                'w_id' => $w_id,
                // 'created_by' => $u_id,
                // 'gdn_no' => $gdn_no,
                'type' => 0
            ]);


        }

        $ref_no = TransactionHistory::create([
            'p_s_id' =>  $p->id,
            'p_type' => 'Sales',
            't_type' => 'Received',
            'paid_by' => 'Cash',
            // 'ref_no' => $request->p_ref_no,
            'total' => $total,
            // 'cheque_no' => $request->cheque_no,
            // 'cc_no' => $request->cc_no,
            // 'gift_no' => $request->gift_no,
            // 'cc_holder' => $request->cc_holder,
            // 'note' => $request->note,
            // 'merchant_type' => $request->merchant_type,
            // 'b_id' => $bank_id,
            // 'created_by' => $u_id
        ]);



        $gl = GeneralLedger::max('id');
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');
        if($gl == null)
        {
            $link_id = 1;
        }
        else
        {
            $ledger = GeneralLedger::where('id',$gl)->first();
            $link_id = $ledger->link_id + 1;
        }
        $customer=Vendors::find($customerid[0]);
        $cash_c = 0;
        $cash_d = 0;
        $cash_n = 0;
        // $bank_c = 0;
        // $bank_d = 0;
        // $bank_n = 0;
        $customer_c = 0;
        $customer_d = 0;
        $customer_n = 0;

        // $cashc_c = 0;
        // $cashc_d = 0;
        // $cashc_n = 0;
        $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
        ->first();
        // return response()->json( $account_c ,200);
        $receivable = GeneralLedger::where('account_code',$account_c->Code)
        ->get();

        $account_s = AccountDetails::where('name_of_account','Sales')->first();
        $sales = GeneralLedger::where('account_code',$account_s->Code)
        ->get();

        $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
        $cash = GeneralLedger::where('account_code',$account_cash->Code)
        ->get();

        $sales_n = 0;
        $sales_d = 0;
        $sales_c = 0;
        if($sales->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to '.$customer->name,
                'account_name' => $account_s->name_of_account,
                'link_id' => $link_id,
                // 'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_s->Code,
                // 'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => '0',
                'credit' => $total,
                'net_value' =>  0 - $total,
                'balance' =>  0 - $total
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($sales as $key => $d) {
                $sales_d+=$d->debit;
                $sales_c+=$d->credit;
                $balance+=$d->net_value;
            }
            $sales_c += $total;
            // $sales_n = $sales_d - $sales_c;
            $sales_n = $balance + (0 - $total);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to '.$customer->name,
                'account_name' => $account_s->name_of_account,
                'link_id' => $link_id,
                // 'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_s->Code,
                // 'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => '0',
                'credit' => $total,
                'net_value' => 0 - $total,
                'balance' => $sales_n
            ]);
        }

        if($receivable->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to'.$customer->name,
                'account_name' => $account_c->name_of_account,
                'link_id' => $link_id,
                // 'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_c->Code,
                // 'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => $total,
                'credit' => '0',
                'net_value' => $total,
                'balance' => $total
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($receivable as $key => $c) {
                $customer_c+=$c->credit;
                $customer_d+=$c->debit;
                $balance+=$c->net_value;
            }
            $customer_d += $total;
            // $customer_n = $customer_d - $customer_c;
            $customer_n = $balance + ($total - 0);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Sales to '.$customer->name,
                'account_name' => $account_c->name_of_account,
                'link_id' => $link_id,
                // 'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_c->Code,
                // 'transaction_no' => $gdn_no,
                'currency_code' => 'PKR',
                'debit' => $total,
                'credit' => '0',
                'net_value' => $total,
                'balance' => $customer_n
            ]);
        }


        if($cash->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount Paid by: '.$customer->name,
                'account_name' => $account_cash->name_of_account,
                'link_id' => $link_id,
                // 'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_cash->Code,
                'transaction_no' => $ref_no->id,
                'currency_code' =>'PKR',
                'debit' => $total,
                'credit' => '0',
                'net_value' => $total - 0,
                'balance' => $total - 0,
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($cash as $key => $c) {
                $cash_c+=$c->credit;
                $cash_d+=$c->debit;
                $balance+=$c->net_value;
            }
            $cash_d += $total;
            $cash_n = $balance + ($total - 0);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount Paid by: '.$customer->name,
                'account_name' => $account_cash->name_of_account,
                'link_id' => $link_id,
                // 'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_cash->Code,
                'transaction_no' => $ref_no->id,
                'currency_code' =>'PKR',
                'debit' => $total,
                'credit' => '0',
                'net_value' => $total - 0,
                'balance' => $cash_n
            ]);
        }



        for ($k=0; $k < count($request->line_items) ; $k++)
        {
            $product = Products::find($request->line_items[$k]['product_id']);
            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
            ->first();
            // dd($account_i);
            $balance = 0;
            $net_d_p = 0;
            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
            ->where('w_id',$w_id)
            ->get();
            // dd($debit_p);
            if($debit_p->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sale product for sale number: '.$p->id,
                    'account_name' => $account_i->name_of_account,
                    'link_id' => $link_id,
                    // 'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_i->Code,
                    // 'transaction_no' => $gdn_no,
                    'currency_code' => 'PKR',
                    'stock_out' => $request->line_items[$k]['quantity'],
                    'stock_in' => '0',
                    'net_value' => 0 - $request->line_items[$k]['quantity'],
                    'balance' => 0 - $request->line_items[$k]['quantity'],
                    'credit' =>  $product->cost * $request->line_items[$k]['quantity'],
                    'debit' => 0,
                    'w_id' => $w_id,
                    'type' => 0,
                    'amount' => 0 - $product->cost
                ]);
            }
            else
            {
                foreach ($debit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_d_p = $balance + ( 0 - $request->line_items[$k]['quantity'] );
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sale product for sale number: '.$p->id,
                    'account_name' => $account_i->name_of_account,
                    'link_id' => $link_id,
                    // 'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_i->Code,
                    // 'transaction_no' => $gdn_no,
                    'currency_code' => 'PKR',
                    'stock_out' => $request->line_items[$k]['quantity'],
                    'stock_in' => '0',
                    'net_value' => 0 - $request->line_items[$k]['quantity'],
                    'balance' => $net_d_p,
                    'credit' => $product->cost * $request->line_items[$k]['quantity'],
                    'debit' => 0,
                    'w_id' => $w_id ,
                    'type' => 0,
                    'amount' => 0 - $product->cost
                ]);
            }

        }


        return response()->json('sale added', 200);
    }
}
