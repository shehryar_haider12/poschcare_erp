<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;
use App\User;
use DataTables;
use App\Exports\BrandsExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('brands.index',compact('permissions'));
    }

    public function datatable()
    {
        $brand=Brands::all();
        return DataTables::of($brand)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('brands.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $data = $request->validate([
            'b_name'      =>  'required|string|max:50|unique:brands',
            'c_p_name'      =>  'nullable',
            'c_p_contactNo'      =>  'nullable',
        ]);
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $data['status'] = 1;
        }
        else
        {
            $data['status'] = 0;
        }
        $data['created_by'] =$u_id;

        $brands = Brands::create($data);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New brand has been added by '.$u_name,
            'link' => url('').'/brands',
            'name' => 'View Brands',
        ];
        Notification::send($user, new AddNotification($data1));
        if(isset($request->request_type) && $request->request_type == "ajax")
        {
            $response['message'] = 'Brand added successfully!';
            $response['brands'] = $brands;
            return response()->json($response, 200);
        }
        toastr()->success('Brand added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brands $brand)
    {
        if(request()->ajax())
        {
            return $brand;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brands $brand)
    {
        $data=[
            'isEdit' => true,
            'brand' => $brand
        ];
        return view('brands.create',$data);
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Brands::find($id);
        if ($item->update(['status' => $status])) {
            Brands::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brands $brand)
    {
        $data = $request->validate([
            'b_name'      =>  'required|string|max:50|unique:brands,b_name,'.$brand->id,
            'c_p_name'      =>  'nullable',
            'c_p_contactNo'      =>  'nullable',
        ]);
        $u_id = Auth::user()->id;
        $data['updated_by'] = $u_id;
        $brand->update($data);
        toastr()->success('Brand updated successfully!');
        return redirect(url('').'/brands');
    }

    public function excel()
    {
        return Excel::download(new BrandsExport, 'brands.xlsx');
    }
}
