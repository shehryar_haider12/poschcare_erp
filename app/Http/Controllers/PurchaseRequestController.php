<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest;
use App\PurchaseRequestDetails;
use App\Warehouse;
use App\Products;
use App\Unit;
use App\Brands;
use App\Category;
use App\Subcategory;
use App\Variants;
use App\ProductVariants;
use App\CostType;
use Auth;
use DB;
use App\User;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class PurchaseRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $ware=Warehouse::all();
        $prequest=PurchaseRequest::with(['warehouse','reqdetails','quotation'])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        // dd($prequest);
        return view('prequest.index',compact('prequest','ware','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $order=PurchaseRequest::max('id');
        if($order == null)
        {
            $id=1;
        }
        else
        {
            $id=$order+1;
        }
        $ware=Warehouse::all();
        $date=Carbon::now()->format('Y-m-d');
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $data=[
            'isEdit' => false,
            'ware' => $ware,
            'product' => $product,
            'date' => $date,
            'id' => $id,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ct' => CostType::all(),
            'permissions' => getRolePermission($menu_id)
        ];
        return view('prequest.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 'Approved';
        }
        else
        {
            $status = 'Pending';
        }
        $u_id = Auth::user()->id;
        $data=([
            'req_date' => $request->req_date,
            'w_id' => $request->w_id,
            'created_by' => $u_id,
            'status' => $status,
        ]);

        $p=PurchaseRequest::create($data);
        for ($i=1; $i<=count($request->p_id) ; $i++) {
            PurchaseRequestDetails::create([
                'o_id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'type' => $request->type[$i]
            ]);
        }

        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Purchase request has been added by '.$u_name,
            'link' => url('').'/request',
            'name' => 'View Purchase Requests',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Purchase Request created successfully!');
        return redirect(url('').'/request');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p=PurchaseRequest::with(['warehouse'])
        ->where('id',$id)
        ->first();
        $d=PurchaseRequestDetails::with(['variant.product','products','request'])
        ->where('o_id',$id)
        ->get();
        return [$p,$d];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $req=PurchaseRequest::with(['warehouse'])
        ->where('id',$id)
        ->first();
        $rdetail=PurchaseRequestDetails::with(['products','products.brands','products.category','products.unit','request'])
        ->with(['products.currentstocks' => function($query) use ($req) {
            $avg = $query->select(DB::raw('*'))->where('w_id',$req->w_id);
        }])
        ->with(['variant.currentstocks' => function($query) use ($req) {
            $avg = $query->select(DB::raw('*'))->where('w_id',$req->w_id);
        }])
        ->where('o_id',$id)
        ->get();
        // dd($rdetail);
        $ware=Warehouse::all();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $unit=Unit::where('status',1)->get();
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $data=[
            'isEdit' => true,
            'ware' => $ware,
            'product' => $product,
            'id' => $id,
            'req' => $req,
            'unit' => $unit,
            'brands' => $brands,
            'cat' => $cat,
            'sub' => $sub,
            'ct' => CostType::all(),
            'rdetail' => $rdetail
        ];
        return view('prequest.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $u_id = Auth::user()->id;
        PurchaseRequest::where('id',$id)
        ->update([
            'w_id' => $request->w_id,
            'updated_by' => $u_id
        ]);
        DB::table('purchase_request_details')->where('o_id', $id)->delete();
        for ($i=1; $i<=count($request->p_id) ; $i++) {
            PurchaseRequestDetails::create([
                'o_id' => $id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'type' => $request->type[$i]
            ]);
        }

        toastr()->success('Purchase Request updated successfully!');
        return redirect(url('').'/request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $u_id = Auth::user()->id;

        if($status == 'Approved')
        {
            $item = PurchaseRequest::find($id);
            if ($item->update(['status' => $status])) {
                PurchaseRequest::where('id',$id)
                ->update([
                    'status' => $status,
                    'updated_by' => $u_id
                ]);
                $response['status'] = $status;
                $response['message'] = 'status updated successfully.';
                return response()->json($response, 200);
            }
        }
        else
        {
            return response()->json($response, 409);
        }
    }
}
