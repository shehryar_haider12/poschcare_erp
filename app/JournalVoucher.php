<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalVoucher extends Model
{
    protected $table = 'journal_voucher';
    protected $primaryKey = 'id';
    protected $fillable = [
        'date',
        'status',
        'account1',
        'account2',
        'amount',
        'description',
        'month',
        'created_by',
        'head1',
        'head2',
        'b_id',
        'v_no',
        'vcno',
        'p_id',
        'v_type',
        'remarks',
        'attachment',

    ];

    public function children()
    {
        return $this->hasMany('App\JournalVoucher','p_id','id');
    }
    public function parent()
    {
        return $this->hasOne('App\JournalVoucher','id','p_id');
    }

    public function preparedUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function acode1()
    {
        return $this->hasOne('App\AccountDetails','Code','account1');
    }

    public function acode2()
    {
        return $this->hasOne('App\AccountDetails','Code','account2');
    }

    public function biller()
    {
        return $this->hasOne('App\Vendors','id','b_id');
    }
}
