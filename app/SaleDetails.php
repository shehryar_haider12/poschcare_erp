<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleDetails extends Model
{
    protected $table = 'sale_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        's_id',
        'p_id',
        'quantity',
        'sub_total',
        'price',
        'delivered_quantity',
        'discount_percent',
        'discounted_amount',
        'type',
        'returnQty',
        'wooId',
        'cost',
        'taxA',
        'Itype',
        'vet',
        'afterDiscount',
        'price2',
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function sale()
    {
        return $this->hasOne('App\Sales','id','s_id');
    }

    public function purchase()
    {
        return $this->hasMany('App\PurchaseOrderDetails','p_id','p_id');
    }

    public function current()
    {
        return $this->hasMany('App\CurrentStock','p_id','p_id');
    }

    public function stockout()
    {
        return $this->belongsTo('App\StockOut');
    }

    public function stock()
    {
        return $this->hasMany('App\StockOut','sale_d_id','id')->where('s_type','Sales');
    }

}
