<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariantUpdate extends Model
{
    protected $table = 'product_variant_update';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'name',
        'status',
        'cost',
        'price', //as mrp
        'tp'
    ];
    public function product()
    {
        return $this->hasOne('App\Products','id','p_id');
    }
}
