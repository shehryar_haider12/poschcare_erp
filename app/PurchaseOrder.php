<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = 'purchase_order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'order_date',
        'ref_no',
        'w_id',
        'status',
        'doc',
        's_id',
        'note',
        'total',
        'created_by',
        'updated_by',
        'b_id',
        'p_status',
        'tax_status',
        'tax',
        'advance_status',
        'exp_rcv_date',
        'payment_mode',
        'days',
        'pdate',
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Vendors','id','s_id');
    }

    public function biller()
    {
        return $this->hasOne('App\Vendors','id','b_id');
    }

    public function odetails()
    {
        return $this->belongsTo('App\PurchaseOrderDetails');
    }

    public function orderdetails()
    {
        return $this->hasMany('App\PurchaseOrderDetails','o_id','id');
    }

    public function transaction()
    {
        return $this->hasMany('App\TransactionHistory', 'p_s_id', 'id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    // public function sp()
    // {
    //     return $this->morphMany('App\TransactionHistory', 'order');
    // }
}
