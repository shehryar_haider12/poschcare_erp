<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorsUpdateRequest extends Model
{
    protected $table = 'vendors_update_request';
    protected $fillable = [
        'id',
        'c_group',
        'p_group',
        'v_type',
        'company',
        'name',
        'address',
        'c_no',
        'country',
        'NTN',
        'GST',
        'state',
        'email',
        'postalCode',
        'c_id',
        'u_id',
        'status',
        'name2',
        'c_no2',
        'password',
        'balance'
    ];
    public function city()
    {
        return $this->hasOne('App\City','id','c_id');
    }
    public function cgroup()
    {
        return $this->hasOne('App\Groups','id','c_group');
    }

    public function pgroup()
    {
        return $this->hasOne('App\Groups','id','p_group');
    }
}
