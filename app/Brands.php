<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    protected $table = 'brands';
    protected $primaryKey = 'id';
    protected $fillable = [
        'b_name',
        'c_p_name',
        'c_p_contactNo',
        'created_by',
        'updated_by',
        'status'
    ];

    public function products()
    {
        return $this->belongsTo('App\Products');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
