@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/trialBalance">Trial Balance</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-balance-scale font-white"></i>TRIAL BALANCE SHEET
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if ($index == 0)
                                                    @if(in_array('excel',$permissions))
                                                        <a style="float: right"  href="{{route('trialBalance.excel')}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                {{-- @if ($index == 1)
                                                    @if(in_array('excel',$permissions))
                                                        <a style="float: right"  href="{{url('')}}/trialBalance/search/{{$lastMonth}}/{{$trial}}/{{$period == '' ? 'abc' : $period}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/trialBalance/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" name="menuid">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Period</label>
                                                        <input type="month"  name="period" id="period" value="{{old('period')}}" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        <span class="text-danger">{{$errors->first('period') ? "Enter period" : null}}</span>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Period Type</label>
                                                            <select  class="form-control selectpicker" data-live-search="true" name="p_type" id="p_type" >
                                                                <option disabled selected>Select...</option>
                                                                <option value="YTD" >YTD (Year To Date)</option>
                                                                <option value="PTD" >PTD (Period To Date)</option>
                                                            </select>
                                                            <span class="text-danger">{{$errors->first('p_type') ?"select anyone" : null}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Balance Type</label>
                                                            <select  class="form-control selectpicker" data-live-search="true" name="b_type" id="b_type" >
                                                                <option value=""  selected disabled>Select...</option>
                                                                <option>Opening</option>
                                                                <option>Closing</option>
                                                            </select>
                                                            <span class="text-danger">{{$errors->first('b_type') ?"select anyone" : null}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Code</th>
                                                            <th>Head Category</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $balance = 0;
                                                            $total_credit = 0;
                                                            $total_debit = 0;
                                                        @endphp
                                                        @foreach ($hcat as $h)
                                                            @php
                                                                if ($trial == 1) {
                                                                    $account = DB::select("SELECT sum(debit) as debit,sum(credit) as credit
                                                                    FROM `general_ledger` where account_code LIKE '%".$h->code."%'");
                                                                }
                                                                if ($trial == 2) {
                                                                    $account = DB::select("SELECT sum(debit) as debit,sum(credit) as credit
                                                                    FROM `general_ledger` where account_code LIKE '%".$h->code."%'
                                                                    and period LIKE '%".$lastMonth."%'");
                                                                }
                                                                if ($trial == 3) {
                                                                    $account = DB::select("SELECT sum(debit) as debit,sum(credit) as credit
                                                                    FROM `general_ledger` where account_code LIKE '%".$h->code."%'
                                                                    and period between '".$period."' and '".$lastMonth."' ");
                                                                }

                                                                $a++;
                                                                $balance =  $account[0]->debit - $account[0]->credit;
                                                            @endphp
                                                            <tr>
                                                                <td>{{$a}}</td>
                                                                <td>{{$h->code}}</td>
                                                                <td>{{$h->name}}</td>
                                                                @if ($balance > 0)
                                                                    @php
                                                                        $total_debit+=$balance;
                                                                    @endphp
                                                                    <td>
                                                                        {{$balance}}
                                                                    </td>
                                                                    <td>-</td>
                                                                @endif
                                                                @if ($balance < 0)
                                                                    @php
                                                                        $total_credit+=$balance;
                                                                    @endphp
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>{{$balance}}</td>
                                                                @endif
                                                                @if ($balance == 0)
                                                                    <td>-</td>
                                                                    <td>-</td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                                <span><b>Total Debit: {{$total_debit}}</b></span>
                                                <span><b>Total Credit: {{((-1) * $total_credit)}} </b></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render

        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable();
            });

        </script>
    @endsection
@endsection
