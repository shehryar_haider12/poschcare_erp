@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/openingInventory">Opening Inventory</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-houzz font-white"></i>Opening Inventory List
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Account Name</th>
                                                            <th>Posted date</th>
                                                            <th>Period</th>
                                                            <th>Amount/Unit</th>
                                                            <th>Quantity</th>
                                                            <th>Total</th>
                                                            <th>Balance(Qty)</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($ledger as $l)
                                                            <tr>
                                                                <td>
                                                                    {{$l->id}}
                                                                </td>
                                                                <td>
                                                                    {{$l->account_name}}
                                                                </td>
                                                                <td>
                                                                    {{$l->posted_date}}
                                                                </td>
                                                                <td>
                                                                    {{$l->period}}
                                                                </td>
                                                                <td>
                                                                    {{$l->amount}}
                                                                </td>
                                                                <td>
                                                                    {{$l->stock_in}}
                                                                </td>
                                                                <td>
                                                                    {{$l->debit}}
                                                                </td>
                                                                <td>
                                                                    {{$l->balance}}
                                                                </td>
                                                                <td>
                                                                    <a id="GFG" href="{{url('')}}/openingInventory/{{$l->id}}/edit" class="text-info p-1">
                                                                        <button type="button" class="btn blue edit" >
                                                                            <i class="fa fa-edit"></i>
                                                                        </button>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable();
    });
    </script>

@endsection
@endsection
