@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/openingInventory">Opening Inventory </a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span> Edit Opening Inventory</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-houzz font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Update Opening Inventory</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="openingForm" action="{{  route('openingInventory.update')}} " class="form-horizontal" method="PUT" >
                        @csrf
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Account Code</label>
                                        <input type="hidden" name="id" value="{{$id}}">
                                        <input type="hidden" name="w_id" value="{{$ledger->w_id}}">
                                        <input name="code" value="{{ $ledger->account_code}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Account Name</label>
                                        <input value="{{ $ledger->account_name}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Cost</label>
                                        <input name="ocost" value="{{ $ledger->amount}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Quantity</label>
                                        <input name="oqty" value="{{ $ledger->stock_in}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label>Update By</label>
                                        <select class="form-control selectpicker" style="overflow-y: auto" data-live-search="true"  name="type" id="type" >
                                            <option selected disabled>Select Anyone</option>
                                            <option>Cost</option>
                                            <option>Quantity</option>
                                            <option>Both</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" hidden id="cost">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Updated Cost</label>
                                        <input type="text" name="cost" id="cost0" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row" hidden id="quantity">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Updated Quantity</label>
                                        <input type="text" name="qty" id="qty0" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row" hidden id="both">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Updated Cost</label>
                                        <input type="text" name="cost1" id="cost1" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Updated Quantity</label>
                                        <input type="text" name="qty1" id="qty1" class="form-control">
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
    <script>
        $(document).on('change','#type',function(){
            var value = $(this).val();
            if(value == 'Cost')
            {
                $('#cost').show();
                $('#quantity').hide();
                $('#both').hide();
                $('#qty0').val('');
                $('#qty1').val('');
                $('#cost1').val('');
            }
            if(value == 'Quantity')
            {
                $('#quantity').show();
                $('#cost').hide();
                $('#both').hide();
                $('#cost0').val('');
                $('#qty1').val('');
                $('#cost1').val('');
            }
            if(value == 'Both')
            {
                $('#both').show();
                $('#quantity').hide();
                $('#cost').hide();
                $('#cost0').val('');
                $('#qty0').val('');
            }
        });
    </script>
@toastr_js
@toastr_render
@endsection
