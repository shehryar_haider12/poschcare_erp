@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/headofaccounts">Head Of Accounts</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Head Of Accounts</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-money font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Category</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->

                    <form id="hoaForm" action="{{$isEdit ? route('headofaccounts.update',$hoa->id) :  route('headofaccounts.store')}}" class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Head of Account Name*</label>
                                        <input value="{{$hoa->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter HOA Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0 ">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#hoaForm').validate({
        rules: {
            name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>

@endsection
