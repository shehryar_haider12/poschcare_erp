@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/brands">Brands</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Brand</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-bold font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Brand</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="brandForm" action="{{$isEdit ? route('brands.update',$brand->id) :  route('brands.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Brand Name*</label>
                                        <input value="{{$brand->b_name ?? old('b_name')}}" {{$isEdit ? 'readonly' : ''}} class="form-control" type="text" placeholder="Enter Brand Name" name="b_name" >
                                        <span class="text-danger">{{$errors->first('b_name') ? 'brand already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact Person Name<small>(optional)</small></label>
                                        <input value="{{$brand->c_p_name ?? old('c_p_name')}}" class="form-control" type="text" placeholder="Enter Contact Person Name" name="c_p_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact Person Number <small>(optional)</small></label>
                                        <input autocomplete="off" title="11 characters minimum" pattern="[0-9]{11}" value="{{$brand->c_p_contactNo ?? old('c_p_contactNo')}}" class="form-control" type="number" placeholder="Enter Contact Person Number" name="c_p_contactNo">
                                    </div>
                                    <span class="text-danger">{{$errors->first('c_p_contactNo') ? 'Contact number not be greater than 11 digits' : null}}</span>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
<script>
    $('#brandForm').validate({
        rules: {
            b_name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@toastr_js
@toastr_render
@endsection
