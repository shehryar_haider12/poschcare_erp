@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/notifications">All Notifications</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-bell font-white"></i>All Notifications

                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="10%">S.No</th>
                                                            <th>Notification</th>
                                                            <th>Link</th>
                                                            <th width="30%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                        @endphp
                                                        @foreach (auth()->user()->notifications()->get() as $n)
                                                            <tr>
                                                                <td>{{$a}}</td>
                                                                <td>{{$n->data['notification']}}</td>
                                                                <td>
                                                                    <a href="{{$n->data['link']}}">
                                                                        {{$n->data['name']}}
                                                                    </a>
                                                                </td>
                                                                @if ($n->read_at == null)
                                                                    <td>
                                                                        <a href="{{route('markReadSingle',["$n->id"])}}">
                                                                            <button class="btn btn-sm btn-danger">Mark as read</button>
                                                                        </a>
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        <button class="btn btn-sm btn-success" disabled>Read</button>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @php
                                                                $a++;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render

        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable();
            });

        </script>
    @endsection
    @endsection
