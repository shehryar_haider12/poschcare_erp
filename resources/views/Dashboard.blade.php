@extends('layouts.master')
@section('top-styles')


<style>
    stat,.stat .display{margin-bottom:20px}
    .stat{-webkit-border-radius:4px;-moz-border-radius:4px;-ms-border-radius:4px;-o-border-radius:4px;border-radius:2px;background:#FFf;padding:10px 10px 10px}
    .stat.bordered{border:1px solid #e7ecf1}
    .stat .display:after,.stat .display:before{content:" ";display:table}
    .stat .display:after{clear:both}
    .stat .display .number{float:left;display:inline-block}
    .stat .display .number h3{margin:0 0 2px;padding:0;font-size:30px;font-weight:400}
    .stat .display .number h3>small{font-size:23px}
    .stat .display .number small{font-size:14px;color:#AAB5BC;font-weight:600;text-transform:uppercase}
    .stat .display .icon{display:inline-block;float:right;padding:7px 0 0}
    .stat .display .icon>i{color:#cbd4e0;font-size:26px}
    .stat .progress-info{clear:both}
    .stat .progress-info .progress{margin:0;height:4px;clear:both;display:block}
    .stat .progress-info .status{margin-top:5px;font-size:11px;color:#AAB5BC;font-weight:600;text-transform:uppercase}
    .stat .progress-info .status .status-title{float:left;display:inline-block}
    .stat .progress-info .status .status-number{float:right;display:inline-block}
</style>
<style>
   .small-box-footer {
    background: rgb(54, 65, 80);
    color: rgba(255,255,255,.8);
    display: block;
    position: relative;
    text-align: center;
    }
    .hr {
    display: block;
    border-style: inset;
    border-width: 1px;
    margin-top: 10px;
    margin-bottom: 10px;
    }
    .links {
        background: rgb(54, 65, 80);
        color: rgba(255,255,255,.8);
        display: block;
        text-align: center;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #icon
    {
        margin-right: 20px;
    }
</style>
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/dashboard">Dashboard</a>
</li>
@endsection
@section('content')
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Admin Dashboard
        {{-- <small>statistics, charts, recent events and reports</small> --}}
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <section class="qucik_links" style="padding-top:10px">
        <h1 class="" style="display:none">Quick Links <a style="display:none" href="#" class="circle_icon" id="menu_btn" title="Click to customize"><i class="fa fa-pencil"></i></a></h1>
        @if (in_array('count biller',$permission))
        <div class="quick_link_box"><a href="{{in_array('View Billers',$permission) ? route('biller.index') : ''}}"><i class="fa fa-newspaper-o"></i><p>TOTAL BILLERS</p></a><span data-counter="counterup" data-value="{{$biller}}">0</span></div>
        @endif
        @if (in_array('count supplier',$permission))
        <div class="quick_link_box"> @if (in_array('View Suppliers',$permission))<a href="{{route('customer.index')}}" ><i class="fa fa-file-text"></i><p>TOTAL SUPPLIERS</p></a><span data-counter="counterup" data-value="{{$supplier}}">0</span>   @endif</div>
        @endif
        @if (in_array('count customer',$permission))
        <div class="quick_link_box">  @if (in_array('View Customers',$permission))<a href="{{route('customer.index')}}"><i class="fa fa-shopping-cart"></i><p>TOTAL CUSTOMERS</p></a> <span data-counter="counterup" data-value="{{$customer}}"></span>  @endif</div>
        @endif
        @if (in_array('count sale person',$permission))
        <div class="quick_link_box HideOnSmallDevice">  @if (in_array('View Sale Persons',$permission))<a href="{{route('saleperson.index')}}"><i class="fa fa-sun-o"></i><p>TOTAL SALE PERSONS</p></a>   <span data-counter="counterup" data-value="{{$sp}}"></span>  @endif</div>
        @endif
        @if (in_array('count brands',$permission))
        <div class="quick_link_box HideOnSmallDevice"> @if (in_array('View Brands',$permission))<a href="{{route('brands.index')}}"><i class="fa fa-area-chart"></i><p>TOTAL BRANDS</p></a> <span data-counter="counterup" data-value="{{$brand}}"></span> @endif</div>
        @endif
        @if (in_array('count category',$permission))
        <div class="quick_link_box HideOnSmallDevice"> @if (in_array('View Categories',$permission))<a  href="{{route('category.index')}}"><i class="fa fa-calculator"></i><p>TOTAL CATEGORIES</p></a><span data-counter="counterup" data-value="{{$cat}}"></span> @endif</div>
        @endif
        <br>
        @if (in_array('count purchase orders',$permission))
        <div class="quick_link_box">   @if (in_array('View Purchase Orders',$permission))<a href="{{route('purchase.index')}}"> <i class="fa fa-line-chart"></i><p>PURCHASE ORDERS</p></a><span data-counter="counterup" data-value="{{$po}}"></span>@endif</div>
        @endif
        @if (in_array('count sales',$permission))
        <div class="quick_link_box">@if (in_array('View Sales',$permission))<a href="{{route('sales.index')}}"><i class="fa fa-bar-chart-o"></i><p>TOTAL SALES</p></a>  <span data-counter="counterup" data-value="{{$sale}}"></span> @endif</div>
        @endif


    </section>
    <hr color="black" class="hr">
    <div class="row">
        @if (in_array('count raw materials',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$raw == null ? 0 : $raw[0]->quantity}}"></span>
                        </h3>
                        <small>TOTAL RAW MATERIALS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (in_array('count packaging',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$pakg == null ? 0 : $pakg[0]->quantity}}"></span>
                        </h3>
                        <small>PACKAGING PRODUCTS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (in_array('count finish',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$finish== null ? 0 : $finish[0]->quantity}}"></span>
                        </h3>
                        <small>FINISHED PRODUCTS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    @if (in_array('Add Bank',$permission) || in_array('Add Brand',$permission) || in_array('Add Product',$permission) || in_array('Add Finish Product',$permission) || in_array('Add Stock',$permission) || in_array('Add Purchase Order',$permission) || in_array('Add Sale',$permission))

    <hr color="black" class="hr">
    <h3 style="text-decoration: underline"><b>QUICK LINKS</b></h3>
    @endif
    <div class="row">
        @if (in_array('Add Bank',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/bank/create" class="links"><i id="icon" class="fa fa-bank"></i>Add Bank</a>
        </div>
        @endif
        @if (in_array('Add Brand',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/brands/create" class="links"><i id="icon" class="fa fa-bold"></i>Add Brand</a>
        </div>
        @endif
        @if (in_array('Add Product',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/product/create" class="links"><i id="icon" class="fa fa-list-alt"></i>Add Product</a>
        </div>
        @endif
        {{-- @if (in_array('Add Finish Product',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/product/add" class="links"><i id="icon" class="fa fa-list-alt"></i>Add Finish Product</a>
        </div>
        @endif
    </div>
    <br>
    <div class="row"> --}}
        @if (in_array('Add Stock',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/stocks/create" class="links"><i id="icon" class="fa fa-tasks"></i>Add Stock</a>
        </div>
        @endif
    </div>
    <br>
    <div class="row">
        @if (in_array('Add Purchase Order',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/purchase/create" class="links"><i id="icon" class="fa fa-shopping-cart"></i>New Purchase Order</a>
        </div>
        @endif
        @if (in_array('Add Sale',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/sales/create" class="links"><i id="icon" class="fa fa-credit-card"></i>New Sale Order</a>
        </div>
        @endif

    </div>
    <br>



<style>

.qucik_links h1 {
    font-weight: 300;
    color: #505d6e;
    text-align: center;
    text-transform: uppercase;
    font-size: 30px;
    margin: 30px 0px;
}
.quick_link_box {
    display: inline-block;
    border-radius: 5px;
    box-shadow: 0px 4px 3px rgb(0 0 0 / 10%);
    text-align: center;
    background: #fff;
    cursor: pointer;
    margin: 10px;
    width: 150px;
}
.quick_link_box a {
    display: block;
    padding: 30px 10px;
}
.quick_link_box i {
    display: block;
    font-size: 35px;
    color: #00aff0;
    margin-bottom: 15px;
}
.quick_link_box p {
    margin: 0px;
    color: #505d6e;
    font-size: 13px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.quick_link_box span{
    font-size: 19px;
    font-weight: 700;
    line-height: 2;
    color: #00aff0;
    }
    .chart{
    height:500px;
    width:500px;
}
.pie-legend {
	list-style: none;
	margin: 0;
	padding: 0;
}
.pie-legend span {
	display: inline-block;
	width: 14px;
	height: 14px;
	border-radius: 100%;
	margin-right: 16px;
	margin-bottom: -2px;
}
.pie-legend li {
	margin-bottom: 10px;
}
</style>


@endsection
@section('page-scripts')
<!-- ChartJS -->


@endsection
@section('custom-scripts')

<script>
    	// global options variable
	var options = {
		responsive: true,
		easing:'easeInExpo',
		scaleBeginAtZero: true,
        // you don't have to define this here, it exists inside the global defaults
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	}

		// PIE
		// PROPERTY TYPE DISTRIBUTION
		// context
		var ctxPTD = $("#property_types").get(0).getContext("2d");
		// data
		var dataPTD = [
			{
				label: "Single Family Residence",
				color: "#5093ce",
				highlight: "#78acd9",
				value: 52
			},
			{
				label: "Townhouse/Condo",
				color: "#c7ccd1",
				highlight: "#e3e6e8",
				value: 12
			},
			{
				label: "Land",
				color: "#7fc77f",
				highlight: "#a3d7a3",
				value: 6
			},
			{
				label: "Multifamily",
				color: "#fab657",
				highlight: "#fbcb88",
				value: 8
			},
			{
				label: "Farm/Ranch",
				color: "#eaaede",
				highlight: "#f5d6ef",
				value: 8
			},
			{
				label: "Commercial",
				color: "#dd6864",
				highlight: "#e6918e",
				value: 14
			},

		]

		// Property Type Distribution
		var propertyTypes = new Chart(ctxPTD).Pie(dataPTD, options);
			// pie chart legend
			$("#pie_legend").html(propertyTypes.generateLegend());




</script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->



<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script>

@endsection
