<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A Content Management System designed by abc.com">
  <meta name="author" content="abc.com">

  {{-- <link rel="shortcut icon" href="{{url('')}}/assets/images/favicon3.ico"> --}}

  <title>Register - POSCH CARE</title>
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/fontawesome-free/css/all.min.css">

  <link href="{{url('')}}/assets/custom/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/style.css" rel="stylesheet" type="text/css" />

  <link href="{{url('')}}/assets/custom/css/animate.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/custom.css" rel="stylesheet" type="text/css" />

  <script src="{{url('')}}/assets/custom/js/modernizr.min.js"></script>
</head>

<body>

  <div class="account-pages bg-dark-theme">
    <div id="canvas-wrapper">
      <canvas id="demo-canvas"></canvas>
    </div>
  </div>

  <div class="login_page_custom">
    <div class="wrapper-page login_custom_con">
      <div class="animated fadeInDown">
        <div class="text-center bg-white b_r_5 bb_l_r_0 p10">
          {{-- <img src="{{url('/uploads/images').'/'}}" alt="Site logo" width="50%" /> --}}
          <h2>POSCH CARE</h2>
        </div>
        <div class="card-box bg-black-transparent4 bt_l_r_0">
          <div class="panel-heading text-center">

            <h1 class="text-white m0">
              <strong>Admin Signup </strong>
            </h1>
          </div>

          <div class="p-20">
            <form class="form-horizontal" action="{{route('register')}}" method="POST" id="quickForm">
                <div>
                  @csrf
                  <input type="hidden" name="device_token" id="device_token">
                  @if ($errors->any())
                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                      ×
                    </button>
                    @foreach ($errors->all() as $item)
                    {{ $item }}
                    <br>
                    @endforeach
                  </div>
                  @endif
                  <div class="form-group row">
                    <div class="col-12">
                      <input class="form-control" type="name" name="name" required='' placeholder="Full Name">

                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-12">
                    <input class="form-control" type="email" name="email" required='' placeholder="Email">

                  </div>
                </div>

                <div class="form-group">
                  <div class="col-12">
                    <input class="form-control" type="password" required='' name="password" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-12">
                    <input class="form-control" type="password" required='' name="password_confirmation" placeholder="Confirm Password">
                  </div>
                </div>

                <div class="form-group text-center m-t-40">
                  <div class="col-12">
                    <button class="btn btn-light-theme btn-block text-uppercase waves-effect waves-light" type="submit">Sign up</button>
                  </div>
                </div>


                <div class="form-group m-t-30 m-b-0">
                  <div class="col-12">
                    <a href="/" class="text-light-theme-link" style="color: white">
                      <i class="fas fa-user m-r-5"></i> Click here to Login</a>
                  </div>
                </div>
              </form>
          </div>


        </div>
      </div>
    </div>
  </div>

  <script>
    var resizefunc = [];
  </script>

  <!-- jQuery  -->
  <script src="{{url('')}}/assets/custom/js/jquery.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/popper.min.js"></script>
  <!-- Popper for Bootstrap -->
  <script src="{{url('')}}/assets/custom/js/bootstrap.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/detect.js"></script>
  <script src="{{url('')}}/assets/custom/js/fastclick.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.slimscroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.blockUI.js"></script>
  <script src="{{url('')}}/assets/custom/js/waves.js"></script>
  <script src="{{url('')}}/assets/custom/js/wow.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.nicescroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.scrollTo.min.js"></script>

  <!-- lOGIN Page Plugins -->
  <script src="{{url('')}}/assets/custom/pages/login/EasePack.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/rAF.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/TweenLite.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/login.js"></script>

  <script src="{{url('')}}/assets/custom/js/jquery.core.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.app.js"></script>
    <!-- jquery-validation -->
    <script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>

  <script type="text/javascript">
    jQuery(document).ready(function () {
        $('#quickForm').validate({
        rules: {
          first_name: {
            required: true,
          },
          email: {
            required: true,
            email: true,
          },
          password: {
            required: true,
            minlength: 5
          },
          password_confirmation: {
            required: true,
            minlength: 5
          },
         },
        messages: {
          email: {
            required: "Please enter a email address",
            email: "Please enter a vaild email address"
          },
          password: {
            required: "Please enter password",
            email: "Please enter a vaild email address"
          },
          password_confirmation: {
            required: "Please enter confirm password",
            minlength: "Your password must be at least 5 characters long"
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
      // Init CanvasBG and pass target starting location
      CanvasBG.init({
        Loc: {
          x: window.innerWidth / 2,
          y: window.innerHeight / 3.3
        },
      });

      fetch('https://ipapi.co/json/')
      .then(response => response.json())
      .then((response) => {
        $('#loginForm').append(`<input type="hidden" name="last_login_details" value='`+JSON.stringify(response)+`'>`);
      })
    });
  </script>

</body>

</html>
