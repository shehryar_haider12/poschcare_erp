<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A3;
        }
  </style>
    <body>

        <div class="attendance-table" style="margin-top: 25px">
            <h3 style="text-align: center"> PROFIT/LOSS STATEMENT ({{$monthName}})  </h3>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                    <th class="attendance-cell">S.No</th>
                    <th class="attendance-cell">Invoice#</th>
                    <th class="attendance-cell">Date</th>
                    <th class="attendance-cell">Customer</th>
                    <th class="attendance-cell">Products</th>
                    <th class="attendance-cell">Qty</th>
                    <th class="attendance-cell">Sale Price</th>
                    <th class="attendance-cell">Sale Amount</th>
                    <th class="attendance-cell">Cost Price</th>
                    <th class="attendance-cell">Cost Amount</th>
                    <th class="attendance-cell">Profit/Loss</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $a=1;
                        $cost=0;
                        $price=0;
                        $avg = 0;
                        $avg1 = 0;
                    @endphp
                    @foreach ($stock as $s => $st)
                        @foreach ($st as $item)
                            <tr>
                                <td class="attendance-cell" >
                                    {{$a}}
                                </td>
                                <td class="attendance-cell" >
                                    {{$item->s_id}}
                                </td>
                                <td class="attendance-cell" >
                                    {{$s}}
                                </td>
                                <td class="attendance-cell" >
                                    {{$item->sale->customer->name}}
                                </td>
                                @if ($item->type == 1)
                                    <td class="attendance-cell" >
                                        {{$item->variant->name}}
                                    </td>
                                @else
                                    <td class="attendance-cell" >
                                        {{$item->products->pro_code.' - '.$item->products->pro_name}}
                                    </td>
                                @endif
                                <td class="attendance-cell" >
                                    {{$item->quantity}}
                                </td>
                                <td class="attendance-cell" >
                                    {{$item->price}}
                                </td>
                                <td class="attendance-cell" >
                                    {{$item->sub_total}}
                                    @php
                                        $price = $item->sub_total;
                                    @endphp
                                </td>
                                    <td class="attendance-cell" >
                                        {{$item->type == 1 ? $item->variant->cost : $item->products->costt}}
                                    </td>

                                    <td class="attendance-cell" >
                                        {{($item->type == 1 ? $item->variant->cost : trim($item->products->cost)) * $item->quantity}}
                                    </td>
                                    @php
                                        $cost = ($item->type == 1 ? $item->variant->cost : trim($item->products->cost)) * $item->quantity;
                                    @endphp

                                @if ($price > $cost)
                                    <td class="attendance-cell" >
                                        {{$price - $cost}}
                                    </td>
                                @else
                                    <td class="attendance-cell" >
                                        ({{$cost - $price}})
                                    </td>
                                @endif
                            </tr>
                            @php
                                $a++;
                                $avg=0;
                                $avg1=0;
                            @endphp
                        @endforeach
                    @endforeach

                </tbody>
            </table>

        </div>
    </body>
</html>
