@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css

<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="#">Reports</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-bar-chart font-white"></i>Supplier Report Detailed
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if ($index == 0)
                                                    <a style="margin-left:200px; "  href="{{url('')}}/supplierReport/{{$id}}/excel">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 1)
                                                    <a style="margin-left:200px; "  href="{{url('')}}/supplierReport/search/{{$year}}/{{$id}}/excel/year">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 2)
                                                    <a style="margin-left:200px; "  href="{{url('')}}/supplierReport/search/{{$month}}/{{$id}}/excel/month">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 3)
                                                    <a style="margin-left:200px; "  href="{{url('')}}/supplierReport/search/{{$from}}/{{$to}}/{{$id}}/excel/dates">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 4)
                                                    <a style="margin-left:200px; "  href="{{url('')}}/supplierReport/search/{{$date}}/{{$id}}/excel/date">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 5 || $index == 6 || $index == 7)
                                                    <a style="margin-left:200px; "  href="{{url('')}}/supplierReport/search/{{$index}}/{{$id}}/excel">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/supplierReport/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$id}}" name="id">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Month</label>
                                                            <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From</label>
                                                                <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To</label>
                                                                <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="" style="visibility: hidden">.</label>
                                                                <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                <i class="fa fa-search pr-1"></i> Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">

                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Date</th>
                                                            <th>Biller</th>
                                                            <th>Supplier</th>
                                                            <th>Product(Qty)</th>
                                                            <th>Total</th>
                                                            <th>Paid</th>
                                                            <th>Balance</th>
                                                            <th>Purchase Status</th>
                                                            <th>Payment Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($purchase as $c)
                                                            <tr>
                                                                <td>
                                                                    {{$c->id}}
                                                                </td>
                                                                <td>
                                                                    {{$c->order_date}}
                                                                </td>
                                                                <td>
                                                                    {{$c->biller == null ? 'No Biller' : $c->biller->nam}}
                                                                </td>
                                                                <td>
                                                                    {{$c->supplier->name}}
                                                                </td>
                                                                <td>
                                                                    @foreach ($c->orderdetails as $d)
                                                                        {{$d->type == 0 ? $d->products->pro_name.'('.$d->quantity.')' : $d->variant->name.'('.$d->quantity.')'}}
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    {{$c->total}}
                                                                </td>
                                                                <td>
                                                                    {{$c->total_amount == null ? 0 : $c->total_amount }}
                                                                </td>
                                                                <td>
                                                                    {{$c->total - $c->total_amount}}
                                                                </td>
                                                                @if($c->status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Received')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-success" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Paid')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  tn-success" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $total = 0;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>


    @section('custom-script')
        @toastr_js
        @toastr_render

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#example').DataTable({
            scrollX: true
        });
        $('input:radio[name="optradio"]').change(function(){
            if ($(this).is(':checked')) {
                $('#search').prop('disabled',false);
                var val = $(this).val();
                if(val == 'Year')
                {
                    $('#year').prop('disabled',false);
                    $('#year').attr('required',true);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                }
                if(val == 'Month')
                {
                    $('#month').prop('disabled',false);
                    $('#month').attr('required',true);
                    $('#year').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#year').attr('required',false);
                }
                if(val == 'Date')
                {
                    $('#from').prop('disabled',false);
                    $('#from').attr('required',false);
                    $('#to').prop('disabled',false);
                    $('#to').attr('required',false);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#month').prop('disabled',true);
                    $('#year').attr('required',false);
                }
                if(val == 'lastweek')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                }
                if(val == 'last15Days')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                }
                if(val == 'lastMonth')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                }
            }
        });
    });

</script>

    @endsection
@endsection
