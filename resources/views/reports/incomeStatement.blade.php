@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a>Reports</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-bar-chart font-white"></i>INCOME STATEMENT
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if ($index == 0)
                                                {{-- <a style="margin-left:200px"  href="{{route('reports.incomeStatement.excel')}}">
                                                    <i class="fa fa-file-excel-o  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px;"  href="{{route('reports.incomeStatement.pdf')}}">
                                                    <i class="fa fa-file-pdf-o  font-white"></i>
                                                </a> --}}
                                                @endif

                                                @if ($index == 1)
                                                <a style="margin-left:200px"  href="{{url('')}}/reports/incomeStatementsearch/{{$year}}/excel/year">
                                                    <i class="fa fa-file-excel-o  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px;"  href="{{url('')}}/reports/incomeStatementsearch/{{$year}}/pdf/year">
                                                    <i class="fa fa-file-pdf-o  font-white"></i>
                                                </a>
                                                @endif
                                                @if ($index == 2)
                                                <a style="margin-left:200px"  href="{{url('')}}/reports/incomeStatementsearch/{{$month}}/excel/month">
                                                    <i class="fa fa-file-excel-o  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px;"  href="{{url('')}}/reports/incomeStatementsearch/{{$month}}/pdf/month">
                                                    <i class="fa fa-file-pdf-o  font-white"></i>
                                                </a>
                                                @endif

                                                @if ($index == 3)
                                                <a style="margin-left:200px"  href="{{url('')}}/reports/incomeStatementsearch/{{$from}}/{{$to}}/excel/dates">
                                                    <i class="fa fa-file-excel-o  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px;"  href="{{url('')}}/reports/incomeStatementsearch/{{$from}}/{{$to}}/pdf/dates">
                                                    <i class="fa fa-file-pdf-o  font-white"></i>
                                                </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/reports/incomeStatementsearch" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                      <div class="form-check-inline">
                                                          <label class="form-check-label" for="radio2">
                                                              <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                            </label>
                                                        </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            @if ($index == 0)
                                            @else

                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="3" align="center" style="font-weight: bold">
                                                                    INCOME STATEMENT  ({{$index == 3 ? 'From '.$from.' To '.$to : ($index == 1 ? $year :$month)}})
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold">Sales</td>
                                                                <td></td>
                                                                <td style="font-weight: bold">{{$sales[0]->credit == null ? '0' : $sales[0]->credit}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Opening Stock</td>
                                                                <td>{{$opening_stock[0]->opening}}</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Purchases</td>
                                                                <td>{{$purchase[0]->debit == null ? '0' : $purchase[0]->debit}}</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Closing Stock</td>
                                                                <td>({{$closing_stock[0]->closing == null ? '0' : $closing_stock[0]->closing}})</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold">Cost of Goods</td>
                                                                <td></td>
                                                                <td style="font-weight: bold">({{$cost_of_goods == 0 ? '0' : $cost_of_goods }})</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold">Gross {{$pnl == 0 ? '' : $pnl > 0 ? 'Profit' : 'Loss'}}</td>
                                                                <td></td>
                                                                <td style="font-weight: bold">{{$pnl == 0 ? '0' : $pnl}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold"> Expenses</td>
                                                                <td style="font-weight: bold">({{$exp[0]->expense}})</td>
                                                                <td></td>
                                                            </tr>
                                                            {{-- @php
                                                                $netvalue =0;
                                                            @endphp
                                                            @if ($pnl > )

                                                            @else

                                                            @endif --}}
                                                            <tr>
                                                                <td style="font-weight: bold"> Net </td>
                                                                <td></td>
                                                                <td style="font-weight: bold"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                scrollX: true
            });
        });
        $('input:radio[name="optradio"]').change(
        function(){
        if ($(this).is(':checked')) {
            $('#search').prop('disabled',false);
           var val = $(this).val();
           if(val == 'Year')
           {
                $('#year').prop('disabled',false);
                $('#year').attr('required',true);
                $('#month').attr('required',false);
                $('#month').prop('disabled',true);
           }
           if(val == 'Month')
           {
                $('#month').prop('disabled',false);
                $('#month').attr('required',true);
                $('#year').prop('disabled',true);
                $('#year').attr('required',false);
           }
           if(val == 'Date')
           {
                $('#month').prop('disabled',true);
                $('#year').prop('disabled',true);
                $('#from').prop('disabled',false);
                $('#to').prop('disabled',false);
                $('#month').attr('required',false);
                $('#year').attr('required',false);
                $('#from').attr('required',true);
                $('#to').attr('required',true);
           }
        }
    });
    </script>
@endsection
@endsection
