<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A3;
        }
  </style>
  <body>

        <div class="attendance-table" style="margin-top: 25px">
            <h3 style="text-align: center"> INCOME STATEMENT ({{$monthName}})  </h3>
        <table class="table table-striped table-bordered">
            <tbody>
                <tr>
                    <td class="attendance-cell" style="font-weight: bold">
                        Sales
                    </td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell" style="font-weight: bold">{{$sales[0]->credit == null ? '0' : $sales[0]->credit}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell">
                        Opening Stock
                    </td>
                    <td class="attendance-cell">{{$opening_stock}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">
                        Purchases
                    </td>
                    <td class="attendance-cell">{{$purchase[0]->debit == null ? '0' : $purchase[0]->debit}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">
                        Closing Stock
                    </td>
                    <td class="attendance-cell">({{$closing_stock[0]->closing == null ? '0' : $closing_stock[0]->closing}})</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell" style="font-weight: bold">
                        Cost of Goods
                    </td>
                    <td class="attendance-cell" ></td>
                    <td class="attendance-cell"  style="font-weight: bold">({{$cost_of_goods == 0 ? '0' : $cost_of_goods }})</td>
                </tr>
                <tr>
                    <td class="attendance-cell" style="font-weight: bold">
                        Gross {{$pnl == 0 ? '' : $pnl > $cost_of_goods ? 'Profit' : 'Loss'}}
                    </td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell" style="font-weight: bold">{{$pnl == 0 ? '0' : $pnl}}</td>
                </tr>
            </tbody>

        </table>

    </div>
  </body>
</html>
