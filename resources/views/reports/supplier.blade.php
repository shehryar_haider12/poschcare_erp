@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css

<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="#">Reports</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-bar-chart font-white"></i>Supplier Report

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Customer Name</label>
                                                        <input type="text" id="v_name" id="autocomplete-ajax1" class="form-control" placeholder="Customer Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="">Company</label>
                                                        <input type="text" id="company" id="autocomplete-ajax1" class="form-control" placeholder="Company" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th width="7%">S.No</th>
                                                        <th>Name</th>
                                                        <th>Company</th>
                                                        <th>Phone</th>
                                                        <th>Email</th>
                                                        <th>Total Purchases</th>
                                                        <th>Total Amount</th>
                                                        <th>Paid</th>
                                                        <th>Balance</th>
                                                        <th width="15%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $a = 1;
                                                        $total = 0;
                                                        $tp = 0;
                                                        $ta = 0;
                                                        $p = 0;
                                                        $b = 0;
                                                    @endphp
                                                    @foreach ($supplier as $c)
                                                        <tr>
                                                            <td>
                                                                {{$a}}
                                                            </td>
                                                            <td>
                                                                {{$c->name}}
                                                            </td>
                                                            <td>
                                                                {{$c->company}}
                                                            </td>
                                                            <td>
                                                                {{$c->c_no}}
                                                            </td>
                                                            <td>
                                                                {{$c->email}}
                                                            </td>
                                                            <td>
                                                                {{$c->total}}
                                                            </td>
                                                            <td>
                                                                {{$c->total_amount == null ? 0 : $c->total_amount }}
                                                            </td>
                                                            @if (count($c->purchases)>0)
                                                                @foreach ($c->purchases as $s)
                                                                    @php
                                                                        $paid = $s->paid == null ? 0 : $s->paid;
                                                                        $total+=$paid;
                                                                        $p +=$total;
                                                                        $b += ($c->total_amount == null ? 0 : $c->total_amount)- $total;

                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                @php
                                                                    $total = 0;
                                                                    $b += ($c->total_amount == null ? 0 : $c->total_amount)- $total;
                                                                    $p +=$total;
                                                                @endphp
                                                            @endif
                                                            <td>
                                                                {{$total}}
                                                            </td>
                                                            <td>
                                                                {{$c->total_amount - $total}}
                                                            </td>
                                                            <td>
                                                                @if(in_array('detail',$permissions))
                                                                    <a id="GFG" href="{{route('report.supplierReportDetail',$c->id)}}" class="text-info p-1">
                                                                        <button type="button" class="btn btn-primary " >
                                                                            View Details
                                                                        </button>
                                                                    </a>
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $a++;
                                                            $total = 0;
                                                            $tp +=$c->total;
                                                            $ta +=$c->total_amount == null ? 0 : $c->total_amount;

                                                        @endphp
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>TOTAL</th>
                                                        <th></th>
                                                        <th>{{$tp}}</th>
                                                        <th>{{$ta}}</th>
                                                        <th>{{$p}}</th>
                                                        <th>{{$b}}</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>


    @section('custom-script')
        @toastr_js
        @toastr_render

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#example').DataTable({
            scrollX: true
        });
        $('#v_name').on('keyup',function(){
            value = $("#v_name").val();
            table.columns(1).search(value, true, false).draw();
        });
        $('#company').on('keyup',function(){
            value = $("#company").val();
            table.columns(2).search(value, true, false).draw();
        });
    });

</script>

    @endsection
@endsection
