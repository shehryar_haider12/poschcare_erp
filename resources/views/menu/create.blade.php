@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/menu">User Menu</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} User Menu</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} User Menu</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('menu.update',$menu->id) :  route('menu.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-outline">
                                        <label for="">Parent Menu*</label>
                                        <select class="form-control selectpicker" style="overflow-y: auto" data-live-search="true"  name="p_id" id="p_id" >
                                            <option value=""  selected disabled>Select...</option>
                                            @if ($isEdit)
                                                @if ($menu->p_id == 0)
                                                    <option value="0" selected>No Parent</option>
                                                    @foreach ($menus as $m)
                                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                                    @endforeach
                                                @else
                                                <option value="0">No Parent</option>
                                                @foreach ($menus as $m)
                                                    <option value="{{$m->id}}" {{$menu->p_id == $m->id ? 'selected' : ''}} >{{$m->name}}</option>
                                                @endforeach
                                                @endif
                                            @else
                                                <option value="0">No Parent</option>
                                                @foreach ($menus as $m)
                                                    @if ($m->p_id != 0)
                                                        <option value="{{$m->id}}" {{$m->id == old('p_id') ? 'selected' : null}}>&emsp; {{$m->name}}</option>
                                                    @else
                                                        <option value="{{$m->id}}" {{old('p_id') == '0' ? 'selected' : null}}>{{$m->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('p_id') ? 'Select parent Menu' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Name*</label>
                                        <input value="{{$menu->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Menu Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Menu already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Icon Class*</label>
                                        <input value="{{$menu->icon ?? old('icon')}}" class="form-control" type="text" placeholder="Enter Icon Class" name="icon">
                                        <span class="text-success">Leave an empty value if saving as child</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Route*</label>
                                        <input value="{{$menu->route ?? old('route')}}" class="form-control" type="text" placeholder="Enter Route Name" name="route" >
                                        <span class="text-success">Leave an empty value if this menu has children</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Sort*</label>
                                        <input value="{{$menu->sort ?? old('sort')}}" class="form-control" type="number" min="1" placeholder="" name="sort">
                                        <span class="text-danger">{{$errors->first('sort') ? 'Enter sort number' : null}}</span>
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
