@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    .hr {
    display: block;
    border-style: inset;
    border-width: 1px;
    margin-top: 10px;
    margin-bottom: 10px;
    }
</style>
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/roles">Roles</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Role</span>
</li>
@endsection
@section('content')
<section id="role_create">
<div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Role</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="roleForm" action="{{$isEdit ? route('roles.update',$roles->id) :  route('roles.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-outline">
                                        <label >Role Name*</label>
                                        <input value="{{$roles->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Role Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Role already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea  name="description" rows="2" style="width: 100%"  >{{$roles->description ?? old('description')}}</textarea>
                                        <span class="text-danger">{{$errors->first('description') ? 'Enter description' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <span class="text-danger">{{$errors->first('m_id') ? 'Check any menu' : null}}</span>
                            <hr class="hr">
                            <div class="row">
                                @if ($isEdit)
                                    @php
                                        $status= false;
                                        $status1= false;
                                        $status2= false;
                                    @endphp
                                        @foreach ($menus as $m)

                                            @if ($m->p_id != 0)
                                            @else
                                                <div class="col-md-4">
                                                @foreach ($rmenu as $rm)
                                                    @if ($rm->m_id == $m->id)
                                                        @php
                                                            $status1 = true;
                                                        @endphp
                                                        @break
                                                    @else
                                                        @php
                                                            $status1 = false;
                                                        @endphp
                                                    @endif
                                                @endforeach
                                                    @if ($status1 == true)
                                                        <label ><b> {{$m->name}} </b></label>
                                                        <input checked name="m_id[]" value=" {{$m->id}}"  type="checkbox" class="abc parent-{{$m->id}}"  id="{{$m->id}}">
                                                        <br>
                                                    @else
                                                        <label  ><b> {{$m->name}} </b></label>
                                                        <input  name="m_id[]" value=" {{$m->id}}"  type="checkbox" class="abc parent-{{$m->id}}"  id="{{$m->id}}">
                                                        <br>
                                                    @endif
                                                    @if ($m->children->isEmpty())
                                                        {{-- {{dd('ffd')}} --}}
                                                        @foreach ($m->permission as $key => $per)
                                                            @foreach ($rper as $key1 => $rp)
                                                                @if ($rp->p_id == $per->id)
                                                                    @php
                                                                        $status = true;
                                                                    @endphp
                                                                    <label class="label_style"> &emsp; &emsp; {{$per->name}} </label>
                                                                    <input checked name="p_id[]" value=" {{$per->id}}"  type="checkbox" class=" {{'p'.$per->m_id}} parent{{$m->p_id}} permission" data-sub= "{{$per->m_id}}" data-parents="{{$m->p_id}}">
                                                                    <br>
                                                                    @break
                                                                @else
                                                                    @php
                                                                        $status = false;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if ($status == true)
                                                            @else
                                                                <label class="label_style"> &emsp; &emsp; {{$per->name}} </label>
                                                                <input name="p_id[]" value=" {{$per->id}}"  type="checkbox" class=" {{'p'.$per->m_id}} parent{{$m->p_id}} permission" data-sub= "{{$per->m_id}}" data-parents="{{$m->p_id}}">
                                                                <br>

                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @foreach ($m->children as $child)
                                                            @foreach ($rmenu as $rm)
                                                                @if ($rm->m_id == $child->id)
                                                                    @php
                                                                        $status2 = true;
                                                                    @endphp
                                                                    @break
                                                                @else
                                                                    @php
                                                                        $status2 = false;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if ($status2 == true)
                                                                <label> &emsp; {{$child->name}} </label>
                                                                <input checked  name="m_id[]" value=" {{$child->id}}"  type="checkbox" class=" {{'a'.$child->p_id}} sub sub-{{$child->id}}" data-parent="{{$child->p_id}}" id="{{$child->id}}">
                                                                <br>
                                                            @else
                                                                <label> &emsp; {{$child->name}} </label>
                                                                <input   name="m_id[]" value=" {{$child->id}}"  type="checkbox" class=" {{'a'.$child->p_id}} sub sub-{{$child->id}}" data-parent="{{$child->p_id}}" id="{{$child->id}}">
                                                                <br>
                                                            @endif
                                                            @foreach ($child->permission as $key => $per)
                                                                @foreach ($rper as $key1 => $rp)
                                                                    @if ($rp->p_id == $per->id)
                                                                        @php
                                                                            $status = true;
                                                                        @endphp
                                                                        <label class="label_style"> &emsp; &emsp; {{$per->name}} </label>
                                                                        <input checked name="p_id[]" value=" {{$per->id}}"  type="checkbox" class=" {{'p'.$per->m_id}} parent{{$child->p_id}} permission" data-sub= "{{$per->m_id}}" data-parents="{{$child->p_id}}">
                                                                        <br>
                                                                        @break
                                                                    @else
                                                                        @php
                                                                            $status = false;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if ($status == true)
                                                                @else
                                                                    <label class="label_style"> &emsp; &emsp; {{$per->name}} </label>
                                                                    <input name="p_id[]" value=" {{$per->id}}"  type="checkbox" class=" {{'p'.$per->m_id}} parent{{$child->p_id}} permission" data-sub= "{{$per->m_id}}" data-parents="{{$child->p_id}}">
                                                                    <br>

                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endif


                                                </div>
                                            @endif
                                        @endforeach
                                @else

                                    @foreach ($menus as $m)

                                        @if ($m->p_id != 0)
                                        @else
                                            <div class="col-md-4">

                                                    <label><b> {{$m->name}} </b></label>
                                                    <input  name="m_id[]" value=" {{$m->id}}"  type="checkbox" class="abc parent-{{$m->id}}"  id="{{$m->id}}">
                                                    <br>
                                                @if ($m->children->isEmpty())
                                                    @foreach ($m->permission as $key => $per)

                                                            <label class="label_style"> &emsp; &emsp; {{$per->name}} </label>
                                                            <input name="p_id[]" value=" {{$per->id}}"  type="checkbox" class=" {{'p'.$per->m_id}} parent{{$m->p_id}} permission" data-sub= "{{$per->m_id}}" data-parents="{{$m->p_id}}">
                                                            <br>

                                                    @endforeach
                                                @else
                                                    @foreach ($m->children as $child)

                                                            <label class="label_style"> &emsp; {{$child->name}} </label>
                                                            <input   name="m_id[]" value=" {{$child->id}}"  type="checkbox" class=" {{'a'.$child->p_id}} sub sub-{{$child->id}}" data-parent="{{$child->p_id}}" id="{{$child->id}}">
                                                            <br>
                                                        @foreach ($child->permission as $key => $per)

                                                                <label class="label_style"> &emsp; &emsp; {{$per->name}} </label>
                                                                <input name="p_id[]" value=" {{$per->id}}"  type="checkbox" class=" {{'p'.$per->m_id}} parent{{$child->p_id}} permission" data-sub= "{{$per->m_id}}" data-parents="{{$child->p_id}}">
                                                                <br>

                                                        @endforeach
                                                    @endforeach
                                                @endif


                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                {{-- @if(in_array('add',$permissions)) --}}
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                                {{-- @endif --}}
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
<section>
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#roleForm').validate({
        rules: {
            name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    $(".abc").click(function() {
        if(this.checked)
        {
            var id = this.id;
            $('.a'+id).prop('checked',true);
            $('.parent'+id).prop('checked',true);
            $('.p'+id).prop('checked',true);
        }
        if(!this.checked)
        {
            var id = this.id;
            $('.a'+id).prop('checked',false);
            $('.parent'+id).prop('checked',false);
            $('.p'+id).prop('checked',false);
        }
    });


    $('.sub').click(function () {
        if(this.checked)
        {
            var id = this.id;
            var parent = $(this).data('parent');
            $('.p'+id).prop('checked',true);
            $('.parent-'+parent).prop('checked',true);
        }
        if(!this.checked)
        {
            var id = this.id;
            var parent = $(this).data('parent');
            $('.p'+id).prop('checked',false);
        }
    });
</script>
@endsection
