@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/roles">Roles</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-user font-white"></i>View Roles
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('roles.create')}}" class="col-md-2" style="float: right">
                                                    <button  style="background: #00CCFF" type="button"  class="btn btn-block btn-info btn-md ">Add Role</button>
                                                </a>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1100px">
                                                    <thead>
                                                        <tr>
                                                            <th width="10%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Description</th>
                                                            <th style="width:400px">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('modal')
        <div id="ViewMenu" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Role Menus</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Menu Name</th>
                                            <th>Route</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="ViewPermissions" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Role Permissions</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Permissions</th>
                                            <th>Route</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
            <!--End Modal-->
    @endsection
    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    processing: true,
                    serverSide: true,
                    order: [[ 0, "desc" ]],
                    scrollX: true,
                    ajax: '{{route("roles.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "name",
                            "defaultContent": ""
                        },
                        {
                            "data": "description",
                            "defaultContent": ""
                        },
                        {
                            "data": "status",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                return meta.row + 1 ;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                var edit = '{{route("roles.edit",[":id"])}}';
                                edit = edit.replace(':id', row.id);
                                var checked = row.status == 1 ? 'checked' : null;
                                return `
                                @if(in_array('edit',$permissions))
                               <div class="row">
                                <a id="GFG" href="` + edit + `" class="text-info p-1">
                                    <button type="button" class="btn blue edit" >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                @endif
                                @if(in_array('View Menu',$permissions))
                                <button type="button" data-target="#ViewMenu" data-toggle="modal"  class="btn green viewM" id="`+row.id +`">
                                    View Menus
                                </button>
                                @endif
                                @if(in_array('View Permissions',$permissions))
                                <button type="button" data-target="#ViewPermissions" data-toggle="modal"  class="btn green viewP" id="`+row.id +`">
                                    View Permisions
                                </button>
                                @endif
                                @if(in_array('status',$permissions))
                                <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                                checked + ` value="` + row.id + `">
                                                </div>
                                @endif
                                `;
                            },
                        },
                    ],
                    "drawCallback": function (settings) {
                        var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                        if (elems) {
                        elems.forEach(function (html) {
                            var switchery = new Switchery(html, {
                                color: '#007bff'
                                , secondaryColor: '#dfdfdf'
                                , jackColor: '#fff'
                                , jackSecondaryColor: null
                                , className: 'switchery'
                                , disabled: false
                                , disabledOpacity: 0.5
                                , speed: '0.1s'
                                , size: 'small'
                                });

                            });
                        }
                        $('.status').change(function () {
                            var $this = $(this);
                            var id = $this.val();
                            var status = this.checked;

                            if (status) {
                                status = 1;
                            } else {
                                status = 0;
                            }
                    // console.log(status);
                        axios
                            .post('{{route("roles.status")}}', {
                            _token: '{{csrf_token()}}',
                            _method: 'post',
                            id: id,
                            status: status,
                            })
                            .then(function (responsive) {
                            console.log('responsive');
                            // location.reload();
                            })
                            .catch(function (error) {
                            console.log(error);
                            });
                        });

                    },
                });
            });

            $(document).on('click','.viewM',function(){
                var id=$(this).attr("id");
                $("#example1 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/roles/menu/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        var a = 1;
                        for (let i = 0; i < data.length; i++) {
                            if(data[i].menu.route == null)
                            {
                                route = 'No route';
                            }
                            else
                            {
                                route = data[i].menu.route
                            }
                            $("#example1").append("<tr><td>"+a+"</td><td>"+data[i].menu.name+"</td><td>"+route+"</td></tr>");
                            a+=1;
                        }
                        $('#ViewMenu').modal("show");
                        $('#example1').DataTable();
                    }
                });
            });

            $(document).on('click','.viewP',function(){
                var id=$(this).attr("id");
                $("#example2 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/roles/permission/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        var a = 1;
                        for (let i = 0; i < data.length; i++) {
                            if(data[i].permission.route == null)
                            {
                                route = 'No route';
                            }
                            else
                            {
                                route = data[i].permission.route
                            }
                            $("#example2").append("<tr><td>"+a+"</td><td>"+data[i].permission.name+"</td><td>"+route+"</td></tr>");
                            a+=1;
                        }
                        $('#ViewPermissions').modal("show");
                        $('#example2').DataTable();
                    }
                });
            });

        </script>
    @endsection
@endsection

