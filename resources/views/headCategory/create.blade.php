@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/headcategory">Head Category</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Head Category</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-money font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Head Category</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->

                    <form id="hcForm" action="{{$isEdit ? route('headcategory.update',$hcat->id) :  route('headcategory.store')}}" class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >HOA Name*</label>
                                        @if ($isEdit)
                                        <select style="overflow-y: scroll;" id="a_id" size="1" class="form-control selectpicker" data-live-search="true" name="a_id" disabled>
                                            <option value="" disabled selected>Select...</option>
                                                @foreach ($hoa as $s)
                                                <option {{$s->id == $hcat->a_id ? 'selected' : null}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                        </select>
                                            @else
                                            <select style="overflow-y: scroll;" id="a_id" size="1" class="form-control selectpicker" data-live-search="true" name="a_id" >
                                                <option value="" disabled selected>Select...</option>
                                                    @foreach ($hoa as $s)
                                                    <option value="{{$s->id}}" {{$s->id == old('a_id') ? 'selected' : null}}>{{$s->name}}</option>
                                                    @endforeach
                                            </select>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Head Category Name*</label>
                                        <input value="{{$hcat->name ?? old('s_cat_name')}}" class="form-control" type="text" placeholder="Enter Head Category Name" name="name" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Code*</label>
                                        <input value="{{$hcat->code ?? old('code')}}" class="form-control" type="text" name="code" id="code" readonly >
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $(document).on('change','#a_id',function(){
        var id=$(this).val();
        $.ajax({
            url:"{{url('')}}/headofaccounts/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $('#code').val(data);
            }
        });
    });
</script>

<script>
    $('#hcForm').validate({
        rules: {
            a_id: {
                required: true,
            },
            s_cat_name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>

@endsection
