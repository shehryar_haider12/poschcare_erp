<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;
        font-size: 12px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
            /* padding-right: 120px; */
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A3;
        }
  </style>
  <body>
      <header>
          <img src="{{url('')}}/uploads/posch.jpg" style="margin-top: 10px; float: left;" width="110px" height="40px" alt="logo" class="logo-default"  />
        LEDGER HISTORY
    </header>
    <div class="label col-sm-12">
        <br>
        <label><b>{{$ledger[0]->account_name}} Ledger</b></label>
        <br>
        <label><b>{{$ledger[0]->account_code}} Ledger</b></label>
    </div>
        <div class="attendance-table" style="margin-top: 25px">
            <table class="table table-striped table-bordered">

                <thead>
                    <tr>
                        <th class="attendance-cell">S.No</th>
                        <th class="attendance-cell"> Description</th>
                        <th class="attendance-cell">Accounting Date</th>
                        <th class="attendance-cell">Against Account</th>
                        <th class="attendance-cell">Posted Date</th>
                        <th class="attendance-cell">Period</th>
                        <th class="attendance-cell">Reference No</th>
                        @if ($id == 'CA-02')
                            <th class="attendance-cell">Debit</th>
                            <th class="attendance-cell">Credit</th>
                            <th class="attendance-cell">In Stock</th>
                            <th class="attendance-cell">Out Stock</th>
                            <th class="attendance-cell">Net Quantity</th>
                            <th class="attendance-cell">Balance</th>
                            <th class="attendance-cell">Amount</th>
                        @else
                        @if ($id == 'EXP-05')
                            <th class="attendance-cell">Debit</th>
                            <th class="attendance-cell">Credit</th>
                            <th class="attendance-cell">Net Value</th>
                            <th class="attendance-cell">Balance</th>
                            <th class="attendance-cell">Percentage</th>
                        @else
                            <th class="attendance-cell">Debit</th>
                            <th class="attendance-cell">Credit</th>
                            <th class="attendance-cell">Net Value</th>
                            <th class="attendance-cell">Balance</th>
                        @endif
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php
                        $count =1 ;
                    @endphp
                    @foreach ($ledger as $a)
                    @if ($a->account_code == 'CL-02-001')
                        @php
                            $gl = App\GeneralLedger::where('link_id',$a->link_id)
                            ->where('account_code','!=',$a->account_code)
                            ->where('account_code','not like','CA-01%')
                            ->first();
                        @endphp
                    @else
                        @php
                            $gl = App\GeneralLedger::where('link_id',$a->link_id)
                            ->where('account_code','!=',$a->account_code)
                            ->first();
                        @endphp
                    @endif
                        <tr>
                            <td class="attendance-cell" >
                                {{$count}}
                            </td>
                            <td class="attendance-cell" >
                                {{$a->description}}
                            </td>
                            @php
                                $period = Carbon\Carbon::parse($a->period)
                                ->format('Y-m');
                            @endphp
                            <td class="attendance-cell" >
                                {{$a->accounting_date}}
                            </td>
                            <td class="attendance-cell" >
                                {{$gl == null ? '-' : $gl->account_name}}
                            </td>
                            <td class="attendance-cell" >
                                {{$a->posted_date}}
                            </td>
                            <td class="attendance-cell" >
                                {{$period}}
                            </td>
                            <td class="attendance-cell" >
                                {{$a->transaction_no}}
                            </td>
                            <td class="attendance-cell" >
                                {{$a->debit}}
                            </td>
                            <td class="attendance-cell" >
                                {{$a->credit}}
                            </td>
                            @if ($id == 'CA-02')
                                <td class="attendance-cell" >
                                    {{$a->stock_in}}
                                </td>
                                <td class="attendance-cell" >
                                    {{$a->stock_out}}
                                </td>
                            @endif
                            <td class="attendance-cell" >
                                {{$a->net_value}}
                            </td>
                            <td class="attendance-cell" >
                                {{$a->balance}}
                            </td>

                            @if ($id == 'CA-02')

                                <td class="attendance-cell" >{{$a->amount}}</td>
                            @else
                                @if ($id == 'EXP-05')
                                <td class="attendance-cell" >{{$a->amount}}</td>
                                @endif
                            @endif`
                        </tr>
                        @php

                            $count++;
                        @endphp
                    @endforeach
                </tbody>

            </table>

    </div>
  </body>
</html>
