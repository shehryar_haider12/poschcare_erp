@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/counter">Counter</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-gg font-white"></i>View Counters
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('counter.create')}}" class="col-md-2" style="float: right">
                                                    <button  style="background: #00CCFF" type="button"  class="btn btn-block btn-info btn-md ">Add Counter</button>
                                                </a>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1000px">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:100px">Counter No</th>
                                                            <th >Shift Opening</th>
                                                            <th >Shift Closing</th>
                                                            <th >Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
            <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route("counter.datatable")}}',
                "columns": [{
                        "data": "counterNo",
                        "defaultContent": ""
                    },
                    {
                        "data": "open",
                        "defaultContent": ""
                    },
                    {
                        "data": "close",
                        "defaultContent": ""
                    },
                    {
                        "data": "id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            var edit = '{{route("counter.edit",[":id"])}}';
                            edit = edit.replace(':id', row.id);
                            return `
                            @if(in_array('edit',$permissions))
                                <a id="GFG" href="` + edit + `" class="text-info p-1">
                                    <button type="button" class="btn blue edit" >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            @endif
                            `;
                        },
                    },
                ],

            });
        });

    </script>

@endsection
@endsection
