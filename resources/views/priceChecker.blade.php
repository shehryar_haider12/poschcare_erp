@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/price-checker">Price Checker</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-check font-white"></i>Price Checker
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <select name="pid" id="pid" data-live-search="true" class="form-control selectpicker">
                                                        <option selected="" disabled value="">Select</option>
                                                        @foreach ($product as $p)
                                                            <option value={{$p->id}}>
                                                                {{$p->pro_code}} - {{$p->pro_name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <br>




                                            <div class="row">
                                            <div class="col-sm-4"><img id="pimage" hidden src="" width="100%" height="300px"></div>

                                                <div class="row">
                                                <div class="col-sm-2" id="mrp" hidden>
                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <span class="center_co">MRP</span>
                                                    <span class="center_cs" id="mrpLbl"></span>
                                                </div>
                                                <div class="col-sm-2" id="op" hidden>

                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <span class="center_co" >OUR PRICE</span>
                                                    <span class="center_cs" id="opLbl"></span>
                                                </div>
                                                <div class="col-sm-2" id="ys" hidden>

                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <span class="center_co" >YOU SAVE</span>
                                                    <span class="center_cs" id="ysLbl"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <span hidden id="nameLbl" ><B>Name</B></span>
                                                    <br>
                                                    <span hidden id="codeLbl"><B>Name</B></span>

                                            <div class="col-sm-12">
                                                <input type="text" id="p_id" placeholder="Scan Code" class="form-control" autofocus>
                                            </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label hidden id="nameLbl" style="font-size: 20px"><B>Name</B></label>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="row">
                                                <div class="col-sm-2" id="mrp" hidden>
                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <span >MRP</span>
                                                    <span  id="mrpLbl"></label>
                                                </div>
                                                <div class="col-sm-2" id="op" hidden>

                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <span >OUR PRICE</span>
                                                    <span  id="opLbl"></label>
                                                </div>
                                                <div class="col-sm-2" id="ys" hidden>

                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <span>YOU SAVE</span>
                                                    <span  id="ysLbl"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
    <script type="text/javascript" src="{{url('')}}/style-lik/jQuery-Scanner-Detection-master/jquery.scannerdetection.js"></script>
    <script>



    $(document).on('change','#pid',function(){
        var p_id = $(this).val();
        $.ajax({
            url:"{{url('')}}/product/productsearch/"+p_id,
            method:"GET",
            data:
            {
                p_id:p_id,
            },
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#nameLbl').show();
                $('#nameLbl').text('Product Name: '+data.pro_name);
                $('#codeLbl').show();
                $('#codeLbl').text('BarCode: '+data.pro_code);
                if (data.image == null) {
                    $('#pimage').attr('src','{{url('')}}/uploads/noimg.png');
                    $('#pimage').show();
                } else {

                    $('#pimage').attr('src','{{url('')}}/uploads/'+data.image);
                    $('#pimage').show();
                }
                if (data.mrp == null) {
                    $('#op').show();
                    $('#ys').hide();
                    $('#mrp').hide();
                    $('#opLbl').text(data.tp);
                } else {
                    $('#op').show();
                    $('#opLbl').text(data.tp);
                    $('#mrp').show();
                    $('#mrpLbl').text(data.mrp);
                    $('#ys').show();
                    $('#ysLbl').text(data.mrp - data.tp);
                }
            }
        });
    });
    function code(c)
    {
        var rowCount = $('#example tr').length ;
        var length = c.length;
        if(length > 6)
        {
            $.ajax({
                url:"{{url('')}}/pos-detail/product/code/"+c,
                method:"GET",
                error: function (request, error) {
                            console.log(" Can't do because: " + error +request);
                        },
                success:function(data){
                    $('#nameLbl').show();
                    $('#nameLbl').text('Product Name: '+data.pro_name);
                    $('#codeLbl').show();
                    $('#codeLbl').text('BarCode: '+data.pro_code);
                    if (data.image == null) {
                        $('#pimage').attr('src','{{url('')}}/uploads/noimg.png');
                        $('#pimage').show();
                    } else {

                        $('#pimage').attr('src','{{url('')}}/uploads/'+data.image);
                        $('#pimage').show();
                    }
                    if (data.mrp == null) {
                        $('#op').show();
                        $('#ys').hide();
                        $('#mrp').hide();
                        $('#opLbl').text(data.price);
                    } else {
                        $('#op').show();
                        $('#opLbl').text(data.price);
                        $('#mrp').show();
                        $('#mrpLbl').text(data.mrp);
                        $('#ys').show();
                        $('#ysLbl').text(data.mrp - data.price);
                    }
                }
            });
        }
    }

    $(window).scannerDetection();
    $(window).bind('scannerDetectionComplete',function(e,data){
        console.log('complete '+data.string);
        $("#pid").val(data.string);
        code(data.string);
        $('#pid').val('');
    })
    .bind('scannerDetectionError',function(e,data){
        console.log('detection error '+data.string);
    })
    .bind('scannerDetectionReceive',function(e,data){
        console.log('Recieve');
        console.log(data.evt.which);
    });
    </script>


    @endsection
@endsection
