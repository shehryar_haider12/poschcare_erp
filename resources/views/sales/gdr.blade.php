<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .hr {
        display: block;
        border-style: inset;
        border-width: 1px;
        margin-top: 30px;
        margin-right: 485px;
        width: 50%;
    }
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            font-size: 15px;
            text-align: left;
            /* color: #000; */
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            /* height: 60px; */
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            /* background: #F0FFFF; */
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A4;
        }
  </style>
    <body>
        <img src="{{url('')}}/uploads/posch.jpg" style="margin-top: 10px;  float: left;" width="110px" height="40px" alt="logo" class="logo-default"  />
        <header>
            GDN (GOOD DELIVERY NOTE)
        </header>
        <div class="label col-sm-12">
            {{-- <label><b>Reference#: </b>{{$purchase->ref_no}}</label>
            <br> --}}
            @php
                $date=Carbon\Carbon::parse($sdetail[0]->stock_date)->format('d-m-y');
            @endphp
            <label><b>GDN#: </b>{{$gdn}}</label>
            <br>
            <label><b>Invoice#: </b>{{$sales->Ino}}</label>
            <br>
            <label><b>Date: </b>{{$date}}</label>
            <br>
            <label><b>Sale Person Name: </b>{{$sales->saleperson->name}}</label>
        </div>
        <div class="form-body">

            <div class="row col">
                <div>
                    <div class="form-outline">
                        <label for=""><b>Customer:</b></label>
                    </div>
                </div>
                {{-- <div style="margin-top: -18px; margin-left: 480px">
                    <div class="form-outline">
                        <label for=""><b>Customer:</b></label>
                    </div>
                </div> --}}
            </div>
            <div class="row">
                <div>
                    <div class="form-outline">
                        <p><b>Name: {{ucwords($sales->customer->name)}}</b></p>
                        <p><b>Company: {{ucwords($sales->customer->company)}}</b></p>
                        <p><b>Contact No: {{$sales->customer->c_no}}</b></p>
                        <p><b>Address: {{ucwords($sales->customer->address)}}</b></p>
                    </div>
                </div>
                {{-- <div style="margin-top: -150px; margin-left: 480px">
                    <div class="form-outline">
                        <p><b>Name: {{ $purchase->biller == null ? 'Posch Care' :  ucwords($purchase->biller->name)}}</b></p>
                        <p><b>Warehouse Name: {{ucwords($purchase->warehouse->w_name)}}</b></p>
                        <p><b>Address: {{ucwords($purchase->warehouse->w_address)}}</b></p>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="attendance-table" style="margin-top: 25px">
            <table class="table table-striped table-bordered">

                <thead>
                    <tr>
                        <th class="attendance-cell">S.No</th>
                        <th class="attendance-cell">Product Code</th>
                        <th class="attendance-cell">Product Name</th>
                        <th class="attendance-cell">Brand Name</th>
                        <th class="attendance-cell">UOM</th>
                        {{-- <th class="attendance-cell">Ordered Quantity</th> --}}
                        <th class="attendance-cell">Delivered Quantity</th>
                        {{-- <th class="attendance-cell">Weight</th> --}}
                        {{-- <th class="attendance-cell">Brand</th> --}}
                        {{-- <th class="attendance-cell" >Price</th> --}}
                        {{-- <th class="attendance-cell">Sub total</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @php
                        $s_no=1;
                    @endphp
                    @foreach($sdetail as $d)
                        <tr>
                            <td class="attendance-cell" >{{$s_no}}</td>
                            @if ($d->type == 1)
                                @php
                                    $name = explode('-',$p->variant->name);
                                @endphp
                                <td class="attendance-cell" >{{$name[0]}}</td>
                                <td class="attendance-cell" >{{$name[1]}}</td>
                                <td class="attendance-cell" >{{$d->variant->product->brands->b_name}}</td>
                                <td class="attendance-cell" >{{$d->variant->product->unit->u_name}}</td>
                            @else
                                <td class="attendance-cell" >{{$d->products->pro_code}}</td>
                                <td class="attendance-cell" >{{$d->products->pro_name}}</td>
                                <td class="attendance-cell" >{{$d->products->brands->b_name}}</td>
                                <td class="attendance-cell" >{{$d->products->unit->u_name}}</td>
                            @endif
                            <td class="attendance-cell" >{{$d->quantity}}</td>
                            {{-- <td class="attendance-cell" >{{$d->price}}</td> --}}
                            {{-- <td class="attendance-cell" >{{$d->quantity * $d->price}}</td> --}}
                        </tr>
                        @php
                        $s_no++;
                        @endphp
                    @endforeach
                </tbody>

            </table>


        </div>
        <br>
        <br>
        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Recieved By:</b></label>
                    <hr color="black" class="hr">
                </div>
            </div>
        </div>
    </body>
</html>
