<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;
        /* border: 1px solid #000; */

        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        .row
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        header
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 150px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        @page
        {
            margin: 0;
            size: A4;
        }
        #overlay
        {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-image: url('{{url('')}}/uploads/CN7R2pF2hxI5A9KsFYNhsFSJ2HYFi0bsNDjt016D.jpg');
            background-position: center top;
            background-repeat: no-repeat;
            z-index: -1;
        }
  </style>

  <body>
    <header>
        SALES PAYMENT INVOICE
    </header>
    <div class="label col-sm-12">
        <label><b>Reference#: </b>{{$sales->ref_no}}</label>
        <br>
        @php
            $date=Carbon\Carbon::parse($sales->sale_date)->format('d-m-y');
            $date1=Carbon\Carbon::parse($th->created_at)->format('d-m-y');
        @endphp
        <label><b>Sale Date: </b>{{$date}}</label>
        <br>
        <label><b>Sale#: </b>Sale#: </b>{{$sales->Ino}} ({{$sales->Iformat}})</label>
        <br>
        <label><b>Customer ID: </b>{{$sales->customer->id}}</label>
        <br>
        <label><b>Terms: </b>Due upon receipt</label>
        <br>
        @if ($sales->saleperson == null)

        @else

            <label><b>Sale Person Name: </b>{{$sales->saleperson->name}}</label>
        @endif
        <br>
        <label><b>Remaining Balance: </b>{{$balance}}</label>
    </div>
    <div class="form-body">
        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Bill From:</b></label>
                </div>
            </div>
            <div style="margin-top: -18px; margin-left: 480px">
                <div class="form-outline">
                    <label for=""><b>Bill To:</b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline">
                    <p><b>Biller Name: {{ucwords($sales->biller->name)}}</b></p>
                    <p><b>Address: {{ucwords($sales->biller->address)}}</b></p>
                    <p><b>Contact No: {{ucwords($sales->biller->c_no)}}</b></p>
                    {{-- <p><b>Email: {{$sales->biller->email}}</b></p> --}}
                    <p><b>Warehouse Name: {{ucwords($sales->warehouse->w_name)}}</b></p>
                </div>
            </div>
            <div style="margin-top: -170px; margin-left: 480px">
                <div class="form-outline">
                    <p><b>Customer Name: {{ucwords($sales->customer->name)}}</b></p>
                    <p><b>Company Name: {{ucwords($sales->customer->company)}}</b></p>
                    {{-- <p><b>Customer Address: {{ucwords($sales->customer->address)}}</b></p> --}}
                    <p><b>Contact No: {{ucwords($sales->customer->c_no)}}</b></p>
                    {{-- <p><b>Email: {{ucwords($sales->customer->email)}}</b></p> --}}
                    <p><b>Shipment Address: {{ucwords($sales->customer->address)}}</b></p>
                </div>
            </div>
        </div>
    </div>
        <div class="attendance-table" >
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th class="attendance-cell"  width="5%">Invoice No</th>
                    <th class="attendance-cell"  width="30%">Transaction Date</th>
                    <th class="attendance-cell"  width="10%">Paid by</th>
                    <th class="attendance-cell"  width="15%">Amount</th>
                </tr>
            </thead>
            <tbody>
                {{-- @php
                    $amount=0;
                @endphp
                @foreach($saledetail as $d) --}}
                <tr>
                    <td class="attendance-cell" >{{$th->id}}</td>
                    <td class="attendance-cell" >{{$date1}}</td>
                    <td class="attendance-cell" >{{$th->paid_by}}</td>
                    <td class="attendance-cell" >{{$th->total}}</td>
                </tr>
                    {{-- @php
                        $amount+=$d->sub_total;
                    @endphp
                @endforeach --}}
                {{-- <tr >
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>SUB TOTAL</b></td>
                    <td class="attendance-cell" >{{$amount}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>PAID AMOUNT</b></td>
                    <td class="attendance-cell" >{{$th->total}}</td>
                </tr> --}}
            </tbody>

        </table>

    </div>
  </body>
</html>
