<html>

  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;
        font-size: 12px;
        /* border: 1px solid #000; */

        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        .row
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 25px;
        }
        header
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 80px;
            padding-left: 30px;
            /* font-weight: bold; */
        }

        .label1
        {
            font-size: 20px;
            text-align: left;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 150px;
            padding-left: 30px;
        }

        .label2
        {
            font-size: 18px;
            text-align: left;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 150px;
            padding-left: 30px;
        }
        @page
        {
            margin: 0;
            size: A3;
        }
        #overlay
        {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-image: url('{{url('')}}/uploads/CN7R2pF2hxI5A9KsFYNhsFSJ2HYFi0bsNDjt016D.jpg');
            background-position: center top;
            background-repeat: no-repeat;
            z-index: -1;
        }
  </style>
  <body>
    <img src="{{$sales->biller->attachment->isEmpty() ? url('').'/uploads/posch.jpg' : url('').'/documents/'.$sales->biller->attachment[0]->document}}" style="margin-top: 10px;  float: left;" width="110px" height="40px" alt="logo" class="logo-default"  />
    <header>
        {{$sales->Iformat == 'GST' ? 'SALES TAX INVOICE' : 'SALES INVOICE'}}
    </header>
    <div class="label col-sm-12">
        @php
            $date=Carbon\Carbon::parse($sales->sale_date)->format('d-m-y');
        @endphp
        <label><b>Date: </b>{{$date}}</label>
        <br>
        <label><b>Invoice#: </b>{{$sales->Ino}}</label>

        <br>
        <label><b>Sale Person Name: </b>{{$sales->saleperson->name}}</label>
    </div>
    <div class="form-body">
        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Bill From:</b></label>
                </div>
            </div>
            <div style="margin-top: -18px; margin-left: 700px">
                <div class="form-outline">
                    <label for=""><b>Bill To:</b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline">
                    <p><b>Biller Name: {{ucwords($sales->biller->name)}}</b></p>
                    <p><b>Address: {{ucwords($sales->biller->address)}}</b></p>
                    <p><b>Contact No: {{ucwords($sales->biller->c_no)}}</b></p>
                    <p><b>NTN: {{ucwords($sales->biller->VAT)}}</b></p>
                </div>
            </div>
            <div style="margin-top: -170px; margin-left: 700px">
                <div class="form-outline">
                    <p><b>Customer Name: {{ucwords($sales->customer->name)}}</b></p>
                    <p><b>Company Name: {{ucwords($sales->customer->company)}}</b></p>
                    <p><b>Contact No: {{ucwords($sales->customer->c_no)}}</b></p>
                    <p><b>Address: {{ucwords($sales->s_address)}}</b></p>
                    <p><b>NTN: {{ucwords($sales->customer->VAT)}}</b></p>
                    <p><b>STRN: {{ucwords($sales->customer->GST)}}</b></p>
                </div>
            </div>
        </div>
    </div>
        <div class="attendance-table" >
        <table class="table table-striped table-bordered">
            @if ($sales->Iformat == 'Plain')
                <thead>
                    <tr>
                        <th class="attendance-cell"  width="5%">S.No</th>
                        <th class="attendance-cell"  width="30%">Product Code - Name</th>
                        <th class="attendance-cell"  width="7%">Quantity</th>
                        <th class="attendance-cell"   width="7%">Rate</th>
                        <th class="attendance-cell"  width="7%">Sub total</th>
                        <th class="attendance-cell"  width="7%">Discount%</th>
                        <th class="attendance-cell"  width="7%">Return Quantity</th>
                        <th class="attendance-cell"  width="10%">Discounted Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $amount=0;
                        $total=0;
                        $tax=0;
                        $a=1;
                        $thd=0;
                        $ret=0;
                    @endphp
                    @foreach($saledetail as $d)
                        <tr>
                            <td class="attendance-cell" >{{$a}}</td>
                            @if ($d->type == 1)
                                <td class="attendance-cell" >{{$d->variant->name}}</td>
                            @else
                            <td class="attendance-cell" >{{$d->products->pro_code}} - {{$d->products->pro_name}}</td>
                            @endif
                            <td class="attendance-cell" >{{$d->quantity}}</td>
                            <td class="attendance-cell" >{{$d->price}}</td>
                            <td class="attendance-cell" >{{$d->quantity * $d->price}}</td>
                            <td class="attendance-cell" >{{$d->discount_percent == null ? '-' : $d->discount_percent.'%'}}</td>
                            <td class="attendance-cell" >{{$d->returnQty}}</td>
                            <td class="attendance-cell" >{{$d->sub_total}}</td>
                        </tr>
                        @php
                            $amount+=$d->sub_total;
                            $total+=$d->sub_total;
                            $ret= $d->sub_total / $d->quantity;
                            $thd+= $ret * ($d->returnQty == null ? 0 : $d->returnQty);
                            $a++;
                        @endphp
                    @endforeach
                    <tr>
                        <td class="attendance-cell"  colspan="6"></td>
                        <td class="attendance-cell" ><b>TOTAL</b></td>
                        <td class="attendance-cell" >{{$total}}</td>
                    </tr>

                    <tr>
                        <td class="attendance-cell"  colspan="6"></td>
                        <td class="attendance-cell" ><b>PAID AMOUNT</b></td>
                        <td class="attendance-cell" >{{$th}}</td>
                    </tr>
                    {{-- @if ($thd > 0) --}}
                    <tr>
                        <td class="attendance-cell"  colspan="6"></td>
                        <td class="attendance-cell" ><b>Sale Return Amount</b></td>
                        <td class="attendance-cell" >{{$thd}}</td>
                    </tr>
                    {{-- @endif --}}
                    <tr>
                        <td class="attendance-cell"  colspan="6"></td>
                        <td class="attendance-cell" ><b>BALANCE</b></td>
                        <td class="attendance-cell" >{{$total - $th - $thd}}</td>
                    </tr>
                </tbody>

            @else
                <thead>
                    <tr>
                        <th class="attendance-cell"  width="5%">S.No</th>
                        <th class="attendance-cell"  width="30%">Product Code - Name</th>
                        <th class="attendance-cell"  width="7%">Quantity</th>
                        <th class="attendance-cell"   width="7%">TP Rate</th>
                        <th class="attendance-cell"   width="7%">Value Excluding Tax</th>
                        <th class="attendance-cell"  width="7%">Discount%</th>
                        <th class="attendance-cell"  width="7%">Discount Amount</th>
                        <th class="attendance-cell"  width="10%">After Discount</th>
                        <th class="attendance-cell"  width="10%">Tax</th>
                        <th class="attendance-cell"  width="7%">Return Quantity</th>
                        <th class="attendance-cell"  width="7%">Net Amount</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                        $amount=0;
                        $total=0;
                        $tax=0;
                        $a=1;
                        $thd=0;
                        $ret=0;
                    @endphp
                    @foreach($saledetail as $d)
                        <tr>
                            <td class="attendance-cell" >{{$a}}</td>
                            @if ($d->type == 1)
                                <td class="attendance-cell" >{{$d->variant->name}}</td>
                            @else
                            <td class="attendance-cell" >{{$d->products->pro_code}} - {{$d->products->pro_name}}</td>
                            @endif
                            <td class="attendance-cell" >{{$d->quantity}}</td>
                            <td class="attendance-cell" >{{$d->price}}</td>
                            <td class="attendance-cell" >{{$d->vet}}</td>
                            <td class="attendance-cell" >{{($d->discount_percent == null || $d->discount_percent == 0) ? '-' :  $d->discount_percent.'%'}}</td>
                            <td class="attendance-cell" >{{($d->discounted_amount == null || $d->discounted_amount == 0) ? '-' :  $d->discounted_amount}}</td>
                            <td class="attendance-cell" >{{($d->afterDiscount == null || $d->afterDiscount == 0) ? '-' :  $d->afterDiscount}}</td>
                            <td class="attendance-cell" >{{$d->taxA}}</td>
                            <td class="attendance-cell" >{{$d->returnQty}}</td>
                            <td class="attendance-cell" >{{$d->sub_total}}</td>
                        </tr>
                        @php
                            $amount+=$d->sub_total;
                            $total+=$d->sub_total;
                            $ret= $d->sub_total / $d->quantity;
                            $thd+= $ret * ($d->returnQty == null ? 0 : $d->returnQty);
                            $a++;
                        @endphp
                    @endforeach
                    <tr>
                        <td class="attendance-cell"  colspan="9"></td>
                        <td class="attendance-cell" ><b>TOTAL</b></td>
                        <td class="attendance-cell" >{{$total}}</td>
                    </tr>

                    <tr>
                        <td class="attendance-cell"  colspan="9"></td>
                        <td class="attendance-cell" ><b>PAID AMOUNT</b></td>
                        <td class="attendance-cell" >{{$th}}</td>
                    </tr>
                    {{-- @if ($thd > 0) --}}
                    <tr>
                        <td class="attendance-cell"  colspan="9"></td>
                        <td class="attendance-cell" ><b>Sale Return Amount</b></td>
                        <td class="attendance-cell" >{{$thd}}</td>
                    </tr>
                    {{-- @endif --}}
                    <tr>
                        <td class="attendance-cell"  colspan="9"></td>
                        <td class="attendance-cell" ><b>BALANCE</b></td>
                        <td class="attendance-cell" >{{$total - $th - $thd}}</td>
                    </tr>
                </tbody>
            @endif

        </table>

    </div>
    <br>
    <div class="form-body">
        <label class="label2" style="margin-left: 10px">
            <b>Terms & Conditions: </b>{{$sales->pay_type}} {{$sales->note == null  ? '' : $sales->note}}
        </label>
        <div class="label2">
            @if ($sales->remarks != null)
                <br>

                <label>Note:</label>
                {!!$sales->remarks!!}
            @endif
        </div>
        <br>
        <br>
        <div class="label1">
            <label style="margin-left: 100px"  >
            {{$sales->biller->website ?? ''}}
            </label>
            <label style="margin-left: 150px">
                {{$sales->biller->c_no}}
            </label>
            <label style="margin-left: 150px">
                {{$sales->biller->email}}
            </label>
        </div>
    </div>
  </body>
</html>
