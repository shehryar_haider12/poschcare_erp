@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
@section('sidebar-name1')
{{-- @if(Session::has('download'))
         <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif --}}
<li>
    <a href="{{url('')}}/sales">Sales</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Sales</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-credit-card font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Sales</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? ($invoice ? route('sales.updateInvoice',$sale->id) : route('sales.update',$sale->id))  : route('sales.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Invoice Type*</label>
                                        <select {{$isEdit ? 'disabled' : ''}} name="Iformat" id="Iformat" class="form-control selectpicker"  data-live-search="true" required >
                                            <option selected disabled >Select Anyone</option>
                                            @if ($isEdit)
                                                <option {{$sale->Iformat == 'GST' ? 'selected' : null}}>GST</option>
                                                <option {{$sale->Iformat == 'Plain' ? 'selected' : null}}>Plain</option>
                                            @else
                                                <option value="GST" {{old('Iformat') == 'GST' ? 'selected' : null}}>GST</option>
                                                <option value="Plain" {{old('Iformat') == 'Plain' ? 'selected' : null}}>Without GST</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="{{in_array('Add Biller',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Biller*</label>
                                        <select class="form-control selectpicker bid" id="b_id" data-live-search="true" name="b_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if (in_array('Add Biller',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                            @foreach ($biller as $s)
                                            <option {{$s->id == $sale->b_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($biller as $s)
                                            <option value="{{$s->id}}" {{$s->id == old('b_id') ? 'selected' : null}}>{{$s->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if (in_array('Add Biller',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#billerModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Price Type*</label>
                                        @if ($isEdit)
                                            <input type="hidden" name="Itype" value="{{$sale->Itype}}">
                                        @endif
                                        <select {{$isEdit ? 'disabled' : ''}} name="Itype" id="Itype" class="form-control selectpicker"  data-live-search="true" required >
                                            <option selected disabled >Select Anyone</option>
                                            @if ($isEdit)
                                                <option {{$sale->Itype == 'MRP' ? 'selected' : null}}>MRP</option>
                                                <option {{$sale->Itype == 'TP' ? 'selected' : null}}>TP</option>
                                            @else
                                                <option {{old('Itype') == 'MRP' ? 'selected' : null}}>MRP</option>
                                                <option {{old('Itype') == 'TP' ? 'selected' : null}}>TP</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sale Date*</label>
                                        <input id="sale_date" value="{{ $sale->sale_date ?? $date ??  old('sale_date') }}" class="form-control" type="date" placeholder="Enter Sale Date" name="sale_date" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sale Number</label>
                                        <input id="Ino" required value="{{$isEdit ? $sale->Ino : old('Ino')}}" name="Ino" class="form-control" type="text" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Reference Number <small>(optional)</small></label>
                                        <input value="{{ $sale->ref_no ??  old('ref_no')}}" class="form-control" type="text" placeholder="Enter Reference Number" name="ref_no" >
                                        {{-- <span class="text-danger">{{$errors->first('ref_no') ? 'Reference Number already exist' : null}}</span> --}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="w_id" id="w_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            @foreach ($ware as $s)
                                            <option {{$s->id == $sale->w_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->w_name}} - {{$s->w_type}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($ware as $s)
                                            <option value="{{$s->id}}" {{$s->id == old('w_id') ? 'selected' : null}}>{{$s->w_name}} - {{$s->w_type}}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="{{in_array('Add Customer',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Company*</label>
                                        <select class="form-control selectpicker cmid" data-live-search="true" id="cm_id" name="c_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if (in_array('Add Customer',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                            @foreach ($customer as $s)
                                            <option {{$s->id == $sale->c_id ? 'selected' : $s->id}} value="{{$s->id}}">Company Name: {{$s->company}} ; Contact Person: {{$s->name}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($customer as $s)
                                            <option value="{{$s->id}}" {{$s->id == old('c_id') ? 'selected' : null}}> Company Name: {{$s->company}} ; Contact Person: {{$s->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ( in_array('Add Customer',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#customerModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Attach Document <small>(optional)</small></label>
                                        <input class="form-control" type="file" placeholder="Enter Product Name" name="doc" accept=".pdf, .docx, .txt">
                                    </div>
                                </div>
                                @if ($isEdit)
                                    @if ($sale->doc != null)
                                        <div class="row">
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-5">
                                                <input type="hidden" name="doc1" value="{{$sale->doc}}">
                                                <label style="margin-top: 10px" ><b> Document1 </b></label>
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="{{route('sales.document',$sale->id)}}" class='btn green invoice' style="margin-top: 10px" ><i class='fa fa-download'></i></a>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Company Remaining Balance</label>
                                        <input id="crb" value="{{$isEdit ? $balance : ''}}" readonly class="form-control" type="text"  >
                                    </div>
                                </div>

                                <div class="{{in_array('Add Sale Person',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Sale Person*</label>
                                        <select class="form-control selectpicker" id="sp_id" data-live-search="true" name="sp_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if (in_array('Add Sale Person',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                            @foreach ($person as $s)
                                            <option {{$s->id == $sale->sp_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($person as $s)
                                            <option value="{{$s->id}}" {{$s->id == old('sp_id') ? 'selected' : null}}>{{$s->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if (in_array('Add Sale Person',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#personModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Expected Delivery Date <small>(optional)</small> </label>
                                        <input class="form-control" value="{{$sale->expected_date ??  old('expected_date') }}"  type="date"  name="expected_date">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Shipment Address*</label>
                                        <input class="form-control" type="text" name="s_address" id="s_address" value="{{$isEdit ?  $sale->s_address : old('s_address')}}">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sale Type*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="s_type" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($stype as $s)
                                                    <option {{$s->id == $sale->st_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($stype as $s)
                                                    <option value="{{$s->id}}" {{$s->id == old('st_id') ? 'selected' : null}}>{{$s->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="{{ in_array('Add Product',$permissions) ? 'col-sm-10' : 'col-sm-11'}}">
                                    <div class="form-outline">
                                        <label >Products*</label>
                                        <select class="form-control {{$isEdit ? 'selectpicker' : ''}} " data-live-search="true" id="p_id" {{$isEdit ? '' : 'required'}}>
                                            @if ($isEdit)
                                                <option disabled selected>Select..</option> --}}
                                                 @foreach ($product as $s)
                                                    @if ($s->type == 0)
                                                        <option value="{{$s->p_id.'.'.$s->type}}">{{$s->products->pro_code.' - '.$s->products->pro_name.' - '.$s->products->unit->u_name.' - '.$s->products->brands->b_name.' - '.$s->products->p_type}}</option>
                                                    @else
                                                        <option value={{$s->variant->id.'.'.$s->type}}>{{$s->variant->name.' - '.$s->variant->product->p_type}}</option>
                                                    @endif
                                                @endforeach

                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ( in_array('Add Product',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-1">
                                    <div class="form-control" style="margin-top: 23px;  width:50px">
                                        <a id="all">
                                            <i class="fa fa-2x fa-list addIcon font-green" style="margin-top: 3px"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="table-responsive">
                                        <table id="exampleG" {{$isEdit ? ($sale->Iformat == 'GST' ? '' : 'hidden')  : (old('Iformat') == 'GST' ? '' : 'hidden')}} class="table table-striped table-bordered GST" style="overflow-x:auto;">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Product Name</th>
                                                    <th>Brand Name</th>
                                                    <th>Available Quantity</th>
                                                    <th>Quantity</th>
                                                    <th>MRP</th>
                                                    <th>TP</th>
                                                    <th>Value Excluding Tax</th>
                                                    <th>Discount%</th>
                                                    <th>Discounted Amount</th>
                                                    <th>After Discount</th>
                                                    <th>Tax</th>
                                                    <th>Sub Total</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $a=1;
                                                    $subtotal=0;
                                                    $quantity=0;
                                                    $tax = 0;
                                                    $sub = 0;
                                                @endphp
                                                @if ($isEdit && $sale->Iformat == 'GST')
                                                    @foreach ($saledetail as $p)
                                                        @if ($p->type == 1)
                                                            @php
                                                                $currentstock = \App\ProductVariants::with(['currentstocks' => function($query) use ($sale,$p) {
                                                                    $avg = $query->select(DB::raw('*'))->where('w_id',$sale->w_id)->where('type',$p->type);
                                                                }])
                                                                ->where('id',$p->p_id)->first();
                                                                $sub = $p->price * $p->quantity;
                                                                $tax = round($sub - ($sub /  $gst->tax));
                                                                $name = explode(' - ',$ps->variant->name);
                                                            @endphp
                                                             {{-- mrpTax = Math.round(data[0].price - (data[0].price / data[2].tax)); --}}
                                                            <tr>
                                                                <td><input type='hidden' id='otax{{$a}}' value='{{$sale->Itype == 'TP' ? $gst->tax : $tax}}'>
                                                                <input type='hidden' id='mrp{{$a}}' value='{{$p->variant->price}}'>
                                                                <input type='hidden' name='cost[]' value='{{$p->cost}}'>
                                                                <input type='hidden' name='sdid[]' value='{{$p->id}}'>
                                                                <input name="p_id[]" style='width:75px' type="text" class="form-control" readonly value="{{$p->p_id}}"></td>
                                                                <td>{{$name[1]}} </td>
                                                                <td>{{$p->variant->product->brands->b_name}}</td>
                                                                <td><input type='text' style='width:75px' class='form-control' readonly  value='{{$currentstock->currentstocks[0]->quantity ?? 0}}'></td>
                                                                <td><input type='hidden' name='type[]' value='{{$p->type}}'>
                                                                <input type='text' style='width:75px' class='form-control quantity' required min='1' id='{{$a}}' max='{{$currentstock->currentstocks[0]->quantity}}' name='quantity[]' value='{{$p->quantity}}'></td>
                                                                <td><input required style='width:75px' type='text' class='form-control price price{{$a}}' min='1' id='price{{$a}}' data-id='{{$a}}' name='price[]' value='{{$p->price}}'></td>
                                                                <td><input style='width:85px' readonly required type='text' class='form-control mrp mrp{{$a}}' min='1' id='mrp{{$a}}' data-id='mrp{{$a}}' name='price2[]' value='{{$p->price2}}'></td>
                                                                <td><input type='text' style='width:75px'  name='vet[]' class='form-control nv net{{$a}}' readonly id='nv{{$a}}' value='{{$p->vet}}'></td>
                                                                <td><input type='text' style='width:75px' data-id='{{$a}}' class='form-control discountp percent{{$a}}' name='discount_percent[]' id='percent{{$a}}' value='{{$p->discount_percent}}'></td>
                                                                <td><input  type='text' style='width:75px' data-id='{{$a}}' class='form-control discountr rate{{$a}}' name='discounted_amount[]' id='rate{{$a}}' value='{{$p->discounted_amount}}'></td>

                                                                <td><input style='width:75px' type='text' class='form-control ads ad{{$a}}' value='{{$p->afterDiscount}}' id='ad{{$a}}' name='ad[]'></td>
                                                                <td><input name='taxA[]' type='text' style='width:85px' id='taxA{{$a}}' readonly class='form-control ttax taxA{{$a}}' value='{{$p->taxA}}'></td>
                                                                <td><input type='text' style='width:75px' readonly class='form-control abc sub{{$a}}' name='sub_total[]' id='sub{{$a}}' value='{{$p->sub_total}}'></td>
                                                                <td><button type='button' id='{{$a}}' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>
                                                        @else
                                                            @php
                                                                $currentstock = \App\Products::with(['currentstocks' => function($query) use ($sale,$p) {
                                                                    $avg = $query->select(DB::raw('*'))->where('w_id',$sale->w_id)->where('type',$p->type);
                                                                }])
                                                                ->where('id',$p->p_id)->first();
                                                                $sub = $p->price * $p->quantity;
                                                                $tax = round($sub - ($sub /  $gst->tax));
                                                            @endphp
                                                            <tr>
                                                                <td><input type='hidden' id='otax{{$a}}' value='{{$sale->Itype == 'TP' ? $gst->tax : $tax}}'>
                                                                <input type='hidden' id='mrp{{$a}}' value='{{$p->products->mrp}}'>
                                                                <input type='hidden' name='cost[]' value='{{$p->cost}}'>
                                                                <input type='hidden' name='sdid[]' value='{{$p->id}}'>
                                                                <input name="p_id[]" style='width:75px' type="text" class="form-control" readonly value="{{$p->p_id}}"></td>
                                                                <td>{{$p->products->pro_name}}</td>
                                                                <td>{{$p->products->brands->b_name}}</td>
                                                                <td><input type='text' style='width:75px' class='form-control' readonly  value='{{$currentstock->currentstocks[0]->quantity ?? 0}}'></td>
                                                                <td><input type='hidden' name='type[]' value='{{$p->type}}'>
                                                                <input type='text' style='width:75px' class='form-control quantity' required min='1' id='{{$a}}' max='{{$currentstock->currentstocks[0]->quantity}}' name='quantity[]' value='{{$p->quantity}}'></td>
                                                                <td><input required style='width:75px' type='text' class='form-control price price{{$a}}' min='1' id='price{{$a}}' data-id='{{$a}}' name='price[]' value='{{$p->price}}'></td>
                                                                <td><input style='width:85px' readonly required type='text' class='form-control mrp mrp{{$a}}' min='1' id='mrp{{$a}}' data-id='mrp{{$a}}' name='price2[]' value='{{$p->price2}}'></td>
                                                                <td><input type='text' style='width:75px' class='form-control net nv{{$a}}' readonly id='nv{{$a}}' name='vet[]' value='{{$p->vet}}'></td>
                                                                <td><input type='text' style='width:75px' data-id='{{$a}}' class='form-control discountp percent{{$a}}' name='discount_percent[]' id='percent{{$a}}' value='{{$p->discount_percent}}'></td>
                                                                <td><input  style='width:75px' type='text' data-id='{{$a}}' class='form-control discountr rate{{$a}}' name='discounted_amount[]' id='rate{{$a}}' value='{{$p->discounted_amount}}'></td>

                                                                <td><input style='width:75px' type='text' class='form-control ads ad{{$a}}' value='{{$p->afterDiscount}}' id='ad{{$a}}' name='ad[]'></td>
                                                                <td><input name='taxA[]' style='width:75px' type='text' style='width:85px' id='taxA{{$a}}' readonly class='form-control ttax taxA{{$a}}' value='{{$p->taxA}}'></td>
                                                                <td><input type='text' style='width:75px' readonly class='form-control abc sub{{$a}}' name='sub_total[]' id='sub{{$a}}' value='{{$p->sub_total}}'></td>
                                                                <td><button type='button' id='{{$a}}' class='btn red delete' ><i class='fa fa-trash'></i></button></td>
                                                            </tr>
                                                        @endif
                                                        @php
                                                            $a++;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="exampleP" {{$isEdit ? ($sale->Iformat == 'Plain' ? '' : 'hidden') : (old('Iformat') == 'Plain' ? '' : 'hidden')}}  class="table table-striped table-responsive table-bordered Plain" style="overflow-x:auto;" >
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Product Name</th>
                                                    <th>Brand Name</th>
                                                    <th>Available Quantity</th>
                                                    <th>Quantity</th>
                                                    <th>Rate</th>
                                                    <th>Sub Total</th>
                                                    <th>Discount%</th>
                                                    <th>Discounted Amount</th>
                                                    <th>Net Value</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $a=1;
                                                    $subtotal=0;
                                                    $quantity=0;
                                                @endphp
                                                @if ($isEdit && $sale->Iformat == 'Plain')
                                                    @foreach ($saledetail as $p)
                                                        @if ($p->type == 1)
                                                            @php
                                                                $currentstock = \App\ProductVariants::with(['currentstocks' => function($query) use ($sale,$p) {
                                                                    $avg = $query->select(DB::raw('*'))->where('w_id',$sale->w_id)->where('type',$p->type);
                                                                }])
                                                                ->where('id',$p->p_id)->first();
                                                                $name =  explode(' - ',$p->variant->name);
                                                            @endphp
                                                            <tr>
                                                                <td><input type='hidden' name='cost[]' value='{{$p->cost}}'>
                                                                <input type='hidden' name='sdid[]' value='{{$p->id}}'>
                                                                <input name="p_id[]" style='width:75px' type="text" class="form-control" readonly value="{{$p->p_id}}"></td>
                                                                <td>{{$name[1]}} </td>
                                                                <td>{{$p->variant->product->brands->b_name}}</td>
                                                                <td><input type='text' style='width:75px' class='form-control' readonly  value='{{$currentstock->currentstocks[0]->quantity}}'></td>
                                                                <td><input type='hidden' name='type[]' value='{{$p->type}}'>
                                                                    <input type='text' style='width:75px' class='form-control quantity' required min='1' id='{{$a}}' max='{{$currentstock->currentstocks[0]->quantity}}' name='quantity[]' value='{{$p->quantity}}'></td>
                                                                <td><input required type='text' style='width:75px' class='form-control price price{{$a}}' min='1' id='price{{$a}}' data-id='{{$a}}' name='price[]' value='{{$p->price}}'></td>
                                                                <td><input type='text' style='width:75px' class='form-control net nv{{$a}}' readonly id='nv{{$a}}' value='{{$p->quantity * $p->price}}'></td>
                                                                <td><input type='text' style='width:75px' data-id='{{$a}}' class='form-control discountp percent{{$a}}' name='discount_percent[]' id='percent{{$a}}' value='{{$p->discount_percent}}'></td>


                                                                <td><input  style='width:75px' type='text' data-id='{{$a}}' class='form-control discountr rate{{$a}}' name='discounted_amount[]' id='rate{{$a}}' value='{{$p->discounted_amount}}'></td>
                                                                <td><input type='text' style='width:75px' readonly class='form-control abc sub{{$a}}' name='sub_total[]' id='sub{{$a}}' value='{{$p->sub_total}}'></td>
                                                                <td><button type='button' id='{{$a}}' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>
                                                        @else
                                                            @php
                                                                $currentstock = \App\Products::with(['currentstocks' => function($query) use ($sale,$p) {
                                                                    $avg = $query->select(DB::raw('*'))->where('w_id',$sale->w_id)->where('type',$p->type);
                                                                }])
                                                                ->where('id',$p->p_id)->first();
                                                            @endphp
                                                            <tr>
                                                                <td><input type='hidden' name='cost[]' value='{{$p->cost}}'>
                                                                <input type='hidden' name='sdid[]' value='{{$p->id}}'>
                                                                <input name="p_id[]" style='width:75px' type="text" class="form-control" readonly value="{{$p->p_id}}"></td>
                                                                <td>{{$p->products->pro_name}}</td>
                                                                <td>{{$p->products->brands->b_name}}</td>
                                                                <td><input type='text' style='width:75px' class='form-control' readonly  value='{{$currentstock->currentstocks[0]->quantity}}'></td>
                                                                <td><input type='hidden' name='type[]' value='{{$p->type}}'>
                                                                    <input type='text' style='width:75px' class='form-control quantity' required min='1' id='{{$a}}' max='{{$currentstock->currentstocks[0]->quantity}}' name='quantity[]' value='{{$p->quantity}}'></td>
                                                                <td><input required style='width:75px' type='text' class='form-control price price{{$a}}' min='1' id='price{{$a}}' data-id='{{$a}}' name='price[]' value='{{$p->price}}'></td>
                                                                <td><input type='text' style='width:75px' class='form-control net nv{{$a}}' readonly id='nv{{$a}}' value='{{$p->quantity * $p->price}}'></td>
                                                                <td><input type='text' style='width:75px' data-id='{{$a}}' class='form-control discountp percent{{$a}}' name='discount_percent[]' id='percent{{$a}}' value='{{$p->discount_percent}}'></td>


                                                                <td><input  style='width:75px' type='text' data-id='{{$a}}' class='form-control discountr rate{{$a}}' name='discounted_amount[]' id='rate{{$a}}' value='{{$p->discounted_amount}}'></td>
                                                                <td><input type='text' style='width:75px' readonly class='form-control abc sub{{$a}}' name='sub_total[]' id='sub{{$a}}' value='{{$p->sub_total}}'></td>
                                                                <td><button type='button' id='{{$a}}' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>
                                                        @endif
                                                        @php
                                                            $a++;
                                                        @endphp
                                                    @endforeach
                                                @endif

                                            </tbody>


                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <div class="col-sm-6">
                                    <label for=""><b>TOTAL PRICE:</b> </label>
                                    <label id="total">0</label>
                                </div> --}}
                                <div class="col-sm-6">
                                    <label for=""><b>TOTAL AMOUNT:</b> </label>
                                    <label hidden id="amount">{{$isEdit ? $subtotal : '0'}}</label>
                                    <input readonly type="text" style="border: 0px" name="final" id="final" value="{{ $isEdit ? $sale->total : 0}}">
                                </div>
                            </div>
                            <input type="hidden" name="l_amount" id="l_amount">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Advance Payment*</label>
                                            <select class="form-control selectpicker" name="advance" id="adv" required>
                                                <option value="" disabled selected>Select...</option>
                                                @if ($isEdit)
                                                <option {{$sale->advance == 'Yes' ? 'selected' : null}} value="Yes">Yes</option>
                                                <option {{$sale->advance == 'No' ? 'selected' : null}} value="No">No</option>
                                                @else
                                                <option value="Yes" {{old('advance') == 'Yes' ? 'selected' : null}}>Yes</option>
                                                <option value="No" {{old('advance') == 'No' ? 'selected' : null}}>No</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Payment Term Type*</label>
                                            <select class="form-control" name="pay_type" id="pay_type" required>
                                                <option value=""  selected>Select...</option>
                                                @if ($isEdit)
                                                <option {{$sale->pay_type == 'Bill To Bill' ? 'selected' : null}} value="Bill To Bill">Bill To Bill</option>
                                                <option {{$sale->pay_type == 'Days' ? 'selected' : null}} value="Days">Days</option>
                                                <option {{$sale->pay_type == 'Cash on Delivery' ? 'selected' : null}} value="Cash on Delivery">Cash on Delivery</option>
                                                @else
                                                <option value="Bill To Bill" {{old('pay_type') == 'Bill To Bill' ? 'selected' : null}}>Bill To Bill</option>
                                                <option value="Days" {{old('pay_type') == 'Days' ? 'selected' : null}}>Days</option>
                                                <option value="Cash on Delivery" {{old('pay_type') == 'Cash on Delivery' ? 'selected' : null}} >Cash on Delivery</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Term</label>
                                        <input class="form-control" readonly type="text" id="note" name="editor1" value="{{$isEdit ?  $sale->note : old('note')}}">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Date</label>
                                        <input class="form-control" readonly type="date" id="pdate" name="pdate" value="{{$isEdit ?  $sale->pdate : old('pdate')}}">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Status</label>
                                        <select class="form-control" {{$isEdit ? 'disabled' : 'disabled'}} name="p_status" id="p_status" required>
                                            <option value=""  selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$sale->p_status == 'Pending' ? 'selected' : null}} value="Pending">Pending</option>
                                            <option {{$sale->p_status == 'Partial' ? 'selected' : null}} value="Partial">Partial</option>
                                            <option {{$sale->p_status == 'Paid' ? 'selected' : null}} value="Paid">Paid</option>
                                            @else
                                            {{-- <option value="Pending">Pending</option> --}}
                                            <option value="Partial">Partial</option>
                                            <option value="Paid">Paid</option>
                                            @endif
                                        </select>
                                        <input type="hidden" name="p_status1" value="Pending">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Note <small>(optional)</small></label>
                                        <input type="hidden" id="remark" value="{{$isEdit ?  $sale->remarks : old('remarks')}}">
                                        {{-- @if ($isEdit)
                                            <textarea style="display:none;" id="note1" value="{{$purchase->note}}"></textarea>
                                        @else --}}
                                        <textarea name="remarks" id="remarks" rows="10" cols="80">
                                        </textarea>
                                        {{-- @endif --}}
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="pending" class="tableview" {{$isEdit ? '' : 'hidden'}}>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline">
                                            <label >Amount</label>
                                            <input readonly value="{{$isEdit ? $sale->total : ''}}" class="form-control" type="text" min="0"  name="p_total" id="p_total">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="payment" class="tableview" hidden>
                                <div class="row">
                                    {{-- <div class="col-sm-4">
                                        <div class="form-outline">
                                            <label >Payment Reference Number</label>
                                            <input class="form-control" type="number" min="0" placeholder="Enter Payment Reference Number" name="p_ref_no" id="p_ref_no">
                                        </div>
                                    </div> --}}
                                    <div class="col-md-4">
                                        <div class="form-outline">
                                            <label >Amount*</label>
                                            <input  value="" class="form-control" type="text"  name="p_total1" id="p_total1">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-outline">
                                            <label >Paying By*</label>
                                            <select class="form-control selectpicker" name="paid_by" id="paid_by" >
                                                <option value="" disabled selected>Select...</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Gift">Gift Card</option>
                                                <option value="Credit">Credit Card</option>
                                                <option value="Cheque">Cheque</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="cheque" hidden>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Cheque Number*</label>
                                                <input class="form-control" type="text" placeholder="Enter Cheque Number"  name="cheque_no">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Bank*</label>
                                                <select class="form-control selectpicker" id="bank_id" data-live-search="true" name="bank_id" >
                                                    <option value="" disabled selected>Select...</option>
                                                        @foreach ($bank as $s)
                                                        <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                        @endforeach
                                                        <option>other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="gift" hidden>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-outline">
                                                <label >Gift Card Number*</label>
                                                <input class="form-control" type="text" placeholder="Enter Gift Card Number"  name="gift_no">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="credit" hidden>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Credit Card Number*</label>
                                                <input class="form-control" type="text" placeholder="Enter Credit Card Number"  name="cc_no">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Holder Name*</label>
                                                <input class="form-control" type="text" placeholder="Enter Holder Name"  name="cc_holder">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Bank*</label>
                                                <select class="form-control selectpicker" id="bank_id" data-live-search="true" name="bank_id" >
                                                    <option value="" disabled selected>Select...</option>
                                                        @foreach ($bank as $s)
                                                        <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                        @endforeach
                                                        <option>other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-outline">
                                            <label >Payment Note <small>(optional)</small></label>
                                            <textarea name="editor2" id="editor2" rows="10" cols="80">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@section('modal')
    @include('modals.biller')
    @include('modals.saleperson')
    @include('modals.customer')
    @include('modals.allproducts')
@endsection

@endsection
@section('custom-script')
@toastr_js
@toastr_render

<script>

    var note=$('#remark').val();
    if(note == '')
    {
        CKEDITOR.replace( 'remarks' );
    }
    else
    {
        CKEDITOR.replace( 'remarks' );
        CKEDITOR.instances['remarks'].setData(note);
        // CKEDITOR.Text= note;
    }
    $(document).on('change','#w_id',function(){
        var id = $(this).val();
        $("#p_id").empty();
        $('#p_id').selectpicker('destroy');

        $.ajax({
            url:"{{url('')}}/stocks/search/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#p_id').append('<option disabled selected>Select..</option>');
                for(var i = 0 ; i < data.length ; i++)
                {
                    if(data[i].type == 0)
                    {
                        if(data[i].products.weight == null)
                        {
                            weight='';
                        }
                        else
                        {
                            weight = data[i].products.weight ;
                        }
                        $("#p_id").append('<option value='+data[i].p_id+'.'+data[i].type+'>'+data[i].products.pro_code+' - '+data[i].products.pro_name+' - '+weight+data[i].products.unit.u_name+'- '+data[i].products.brands.b_name+' - '+data[i].products.category.cat_name+' - '+data[i].products.p_type+'</option>');
                    }
                    else
                    {
                        $("#p_id").append('<option value='+data[i].variant.id+'.'+data[i].type+'>'+data[i].variant.name+' - '+data[i].variant.product.p_type+'</option>');
                    }
                }

                $('#p_id').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        });
    });

    $(document).on('change','#adv',function(){
        var val = $(this).val();
        console.log(val);
        if(val == 'Yes')
        {
            $('#p_status').prop('disabled',false);
        }
        else
        {
            $('#p_status').prop('disabled',true);
        }
    });


    $(document).on('click','#all',function(){
        var table = $('#example5').DataTable();
        table.destroy();
        var w_id = $('#w_id').val();
        var Itype = $('#Itype').val();
        var Iformat = $('#Iformat').val();
        if(w_id == null)
        {
            alert('Select warehouse first');
        }
        if(Itype == null)
        {
            alert('Select Invoice Type first');
        }
        else
        {
            $('#allproduct').modal("show");
            var table = $('#example5').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{url('')}}/product/modal/'+w_id,
                "columns": [{
                        "data": "",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.products.vstatus == null)
                            {
                                return  row.p_id;
                            }
                            else
                            {
                                return row.variant.id
                            }
                        },
                    },
                    {
                        "data": "pro_name",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.products.vstatus == null)
                            {
                                return  row.products.pro_code+ ` - ` +row.products.pro_name;
                            }
                            else
                            {
                                return row.variant.name;
                            }
                        },
                    },
                    {
                        "data": "",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.products.vstatus == null)
                            {
                                return row.products.unit.u_name;
                            }
                            else
                            {
                                return row.variant.product.unit.u_name;
                            }

                        },
                    },
                    {
                        "data": "products.brands.b_name",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.products.vstatus == null)
                            {
                                return  row.products.brands.b_name;
                            }
                            else
                            {
                                return row.variant.product.brands.b_name
                            }
                        },
                    },
                    {
                        "data": "products.category.cat_name",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.products.vstatus == null)
                            {
                                return  row.products.category.cat_name;
                            }
                            else
                            {
                                return row.variant.product.category.cat_name
                            }
                        },
                    },
                    {
                        "data": "p_id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            return ` <button type="button"  class="btn green add" id="add`+row.p_id +`.`+row.type+`">
                                <i class="fa fa-plus"></i>
                            </button>`;
                        },
                    },
                ],
            });
        }
    });
</script>
<script>
    $(document).ready(function() {
        $('#b_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#billerModal').modal("show"); //Open Modal
            }
            else
            {
                var Iformat = $('#Iformat').val();
                if(Iformat == null)
                {
                    $(".bid option:selected").removeAttr("selected");
                    $('.bid').selectpicker('refresh');
                    alert('select invoice type first');
                }
                else
                {
                    if(Iformat == 'GST')
                    {
                        $('.GST').show();
                        $('.Plain').hide();
                    }
                    else
                    {
                        $('.Plain').show();
                        $('.GST').hide();
                    }
                    $.ajax({
                        url:"{{url('')}}/sales/format/"+Iformat+"/"+opval,
                        method:"GET",

                        error: function (request, error) {
                                    alert(" Can't do because: " + error +request);
                                },
                        success:function(response){
                            console .log(response);
                            $('#Ino').val(response);
                        }
                    });
                }
            }
        });
        $('#bank_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#bankModal').modal("show"); //Open Modal
            }
        });
        $('#sp_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#personModal').modal("show"); //Open Modal
            }
        });
        $('.cmid').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#customerModal').modal("show"); //Open Modal
            }
            else
            {
                $.ajax({
                    url:"{{url('')}}/customer/balance/"+opval,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        if(data[2].status == 0)
                        {
                            $(".cmid option:selected").removeAttr("selected");
                            $('.cmid').selectpicker('refresh');
                            alert('Selected customer is inactive, so sale order can not be created.');
                        }
                        else
                        {
                            if(data[0] == 0 && data[1] == 0)
                            {
                                $('#s_address').val(data[2].address);
                                $('#crb').val(data[0]);
                            }
                            else if(data[0] != 0 && data[1] == 0)
                            {
                                $('#s_address').val(data[2].address);
                                $('#crb').val(data[0]);
                            }
                            else
                            {

                                if(data[0] < data[1])
                                {
                                    $('#crb').val(data[0]);
                                }
                                else
                                {
                                    alert('Sale Cannot be created of this customer, as its balance exceeds its limit.');
                                    $(".cmid option:selected").removeAttr("selected");
                                    $('.cmid').selectpicker('refresh');
                                }
                            }
                        }
                    }
                });
            }
        });


        $('#example5').on('click','.add',function(){
            var Iformat = $('#Iformat').val();
            if(Iformat == 'GST')
            {
                var rowCount = $('#exampleG tr').length;
            }
            else
            {
                var rowCount = $('#exampleP tr').length;
            }
            var id=$(this).attr('id');
            id = id.substring(3);
            var w_id = $('#w_id').val();
            $.ajax({
                url:"{{url('')}}/product/salesProduct/"+id+'.'+w_id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){


                    var Itype = $('#Itype').val();
                    var Iformat = $('#Iformat').val();
                    if(Itype == null)
                    {
                        alert('Select Invoice Type First');
                    }
                    else
                    {
                        // if($('#'+data[0].id).length){
                        //     console.log('dsd');
                        // }
                        // else
                        // {

                            var price = 0;
                            var mrpTax =0;
                            var taxAmount = 0;
                            var oTax = 0;
                            if(Iformat == 'GST')
                            {
                                if(data[0].product == null) //checks either the data is from variants or product
                                {

                                    if(data[0].total_quantity == null)
                                    {
                                        total_quantity=0;
                                    }
                                    else
                                    {
                                        total_quantity = data[0].total_quantity;
                                    }

                                    if(Itype == 'MRP')
                                    {
                                        price =data[0].mrp;
                                        mrpTax = Math.round(data[0].mrp - (data[0].mrp / data[2].tax));
                                        taxAmount = +mrpTax + +price;
                                        oTax = mrpTax;
                                    }
                                    else
                                    {
                                        price =data[0].tp;
                                        mrpTax = Math.round( price * (data[2].tax - 1));
                                        taxAmount = +mrpTax + +price;
                                        oTax = data[2].tax;
                                    }
                                    if($('#'+data[0].id).length){
                                        console.log('dsd');
                                    }
                                    else
                                    {
                                        $("#exampleG").append("<tr id='"+data[0].id+"'><td><input type='hidden' id='otax"+rowCount+"' value='"+oTax+"'><input type='hidden' id='mrp"+rowCount+"' value='"+data[0].mrp+"'><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input style='width:75px' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_name+"</td><td>"+data[0].brands.b_name+"</td><td><input style='width:75px' type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input  type='hidden' name='type[]' value='"+data[1]+"'><input style='width:75px' type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:85px' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input type='text' style='width:75px' class='form-control net nv"+rowCount+"' id='nv"+rowCount+"' readonly name='vet[]' value='"+price+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' readonly name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:75px' type='text' class='form-control ads ad"+rowCount+"' id='ad"+rowCount+"' name='ad[]'></td><td><input type='text' style='width:85px' readonly class='form-control ttax taxA"+rowCount+"' name='taxA[]' id='taxA"+rowCount+"' value='"+mrpTax+"'></td><td><input style='width:110px' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"'  value='"+taxAmount+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                    }
                                }
                                else
                                {
                                    price =data[0].tp;
                                    if(data[0].total_quantity == null)
                                    {
                                        total_quantity=0;
                                    }
                                    else
                                    {
                                        total_quantity = data[0].total_quantity;
                                    }
                                    if(Itype == 'MRP')
                                    {
                                        mrpTax = Math.round(data[0].price - (data[0].price / data[2].tax));
                                        taxAmount = +mrpTax + +price;
                                        oTax = mrpTax;
                                    }
                                    else
                                    {
                                        mrpTax = Math.round( price * (data[2].tax - 1));
                                        taxAmount = +mrpTax + +price;
                                        oTax = data[2].tax;
                                    }
                                    if($('#'+data[0].id).length){
                                        console.log('dsd');
                                    }
                                    else
                                    {
                                        var name=data[0].name.split(' - ');
                                        $("#exampleG").append("<tr id='"+data[0].id+"'><td><input type='hidden' id='otax"+rowCount+"' value='"+oTax+"'><input type='hidden' id='mrp"+rowCount+"' value='"+data[0].mrp+"'><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input style='width:75px' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+name[1]+"</td><td>"+data[0].product.brands.b_name+"</td><td><input style='width:75px' type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input  type='hidden' name='type[]' value='"+data[1]+"'><input style='width:75px' type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:85px' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input type='text' style='width:75px' class='form-control net nv"+rowCount+"' id='nv"+rowCount+"' readonly name='vet[]' value='"+price+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' readonly name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:75px' type='text' class='form-control ads ad"+rowCount+"' id='ad"+rowCount+"' name='ad[]'></td><td><input type='text' style='width:85px' readonly id='taxA"+rowCount+"' name='taxA[]' class='form-control ttax taxA"+rowCount+"' value='"+mrpTax+"'></td><td><input style='width:110px' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"'  value='"+taxAmount+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                    }
                                }
                                var amount=0;
                                var total=0;
                                var rowCount1 = $('#exampleG tr').length;
                                for (let i = 1; i < rowCount1; i++) {
                                    amount = +amount + +$('.sub'+i).val();
                                    total = +total + +$('.price'+i).val();
                                }
                                $('#amount').html(amount);
                                $('#l_amount').val(amount);
                                $('#final').val(amount);
                                $('#p_total1').val(amount);
                            }
                            else
                            {
                                if(data[0].product == null) //checks either the data is from variants or product
                                {
                                    if(Itype == 'MRP')
                                    {
                                        price =data[0].mrp;
                                    }
                                    else
                                    {
                                        price =data[0].tp;
                                    }
                                    if(data[0].total_quantity == null)
                                    {
                                        total_quantity=0;
                                    }
                                    else
                                    {
                                        total_quantity = data[0].total_quantity;
                                    }

                                    if(data[0].weight == null)
                                    {
                                        weight='';
                                    }
                                    else
                                    {
                                        weight = data[0].weight;
                                    }
                                    if($('#'+data[0].id).length){
                                        console.log('dsd');
                                    }
                                    else
                                    {
                                        $("#exampleP").append("<tr id='"+data[0].id+"'><td><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_name+"</td><td>"+data[0].brands.b_name+"</td><td><input type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input type='text' readonly class='form-control net nv"+rowCount+"' id='nv"+rowCount+"' value='"+price+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input readonly type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                    }
                                }
                                else
                                {
                                    if(Itype == 'MRP')
                                    {
                                        price =data[0].price; //in variants price attribute used as mrp
                                    }
                                    else
                                    {
                                        price =data[0].tp;
                                    }
                                    if(data[0].total_quantity == null)
                                    {
                                        total_quantity=0;
                                    }
                                    else
                                    {
                                        total_quantity = data[0].total_quantity;
                                    }
                                    if($('#'+data[0].id).length){
                                        console.log('dsd');
                                    }
                                    else
                                    {
                                        var name=data[0].name.split(' - ');
                                        $("#exampleP").append("<tr id='"+data[0].id+"'><td><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+name[1]+"</td><td>"+data[0].product.brands.b_name+"</td><td><input type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input type='text' readonly class='form-control net nv"+rowCount+"' id='nv"+rowCount+"' value='"+price+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' readonly name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+price+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                    }
                                }
                                var amount=0;
                                var total=0;
                                var rowCount1 = $('#exampleP tr').length;
                                for (let i = 1; i < rowCount1; i++) {
                                    amount = +amount + +$('.sub'+i).val();
                                    total = +total + +$('.price'+i).val();
                                }
                                $('#amount').html(amount);
                                $('#l_amount').val(amount);
                                $('#final').val(amount);
                                $('#p_total1').val(amount);
                            }

                        // }
                    }
                }
            });
        });
    });
    $(document).on('change','#p_status',function(){
        var p=$(this).val();
        if(p == 'Partial')
        {
            $('#payment').show();
            $('#pending').hide();
            var amount = $('#final').val();
            $('#p_total1').val(amount);
            $('#p_total1').attr('max',amount);
            $('#p_total1').attr('min','1');
            $('#p_total1').attr('readonly',false);
        }
        if(p == 'Paid')
        {
            $('#payment').show();
            $('#pending').hide();
            var amount = $('#final').val();
            $('#p_total1').val(amount);
            $('#p_total1').attr('max',amount);
            $('#p_total1').attr('min',amount);
            $('#p_total1').attr('readonly',true);
        }

        if(p == 'Pending')
        {
            $('#payment').hide();
            $('#pending').show();
            var amount = $('#final').val();
            $('#p_total').val(amount);
            $('#p_total').attr('max',amount);
            $('#p_total').attr('min',amount);
            $('#p_total').attr('readonly',true);
        }
    });
    $(document).on('change','#paid_by',function(){
        var p=$(this).val();
        if(p == 'Cheque')
        {
            $('#cheque').show();
            $("#cheque_no").prop('required',true);
            $('#gift').hide();
            $("#gift_no").prop('required',false);
            $('#credit').hide();
            $("#cc_no").prop('required',false);
            $("#cc_holder").prop('required',false);
        }
        if(p == 'Gift')
        {
            $('#gift').show();
            $("#gift_no").prop('required',true);
            $('#cheque').hide();
            $("#cheque_no").prop('required',false);
            $('#credit').hide();
            $("#cc_no").prop('required',false);
            $("#cc_holder").prop('required',false);
        }
        if(p == 'Cash')
        {
            $('#gift').hide();
            $("#gift_no").prop('required',false);
            $('#cheque').hide();
            $("#cheque_no").prop('required',false);
            $('#credit').hide();
            $("#cc_no").prop('required',false);
            $("#cc_holder").prop('required',false);
        }
        if(p == 'Credit')
        {
            $('#gift').hide();
            $("#gift_no").prop('required',false);
            $('#cheque').hide();
            $("#cheque_no").prop('required',false);
            $('#credit').show();
            $("#cc_no").prop('required',true);
            $("#cc_holder").prop('required',true);
        }
    });

    $(document).on('change','#pay_type',function(){
        var p=$(this).val();
        console.log(p);
        if(p == 'Days')
        {
            $('#pdate').prop('readonly',false);
            $('#pdate').prop('required',true);
        }
        else
        {
            $('#pdate').prop('readonly',true);
            $('#pdate').prop('required',false);
        }
    });

    $(document).on('change','#pdate',function(){
        var odate = $('#sale_date').val();
        var today = new Date(odate);
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var pdate = $(this).val();
        var date1 = new Date(date);
        var date2 = new Date(pdate);
        var diff =  date2.getTime() - date1.getTime();
        var Difference_In_Days = diff / (1000 * 3600 * 24);
        $('#note').val(Math.round(Difference_In_Days));
    });

    $(document).on('change','#p_id',function(){
        var Iformat = $('#Iformat').val();
        if(Iformat == 'GST')
        {
            var rowCount = $('#exampleG tr').length;
        }
        else
        {
            var rowCount = $('#exampleP tr').length;
        }
        var id=$(this).val();
        var w_id = $('#w_id').val();

        $.ajax({
            url:"{{url('')}}/product/salesProduct/"+id+'.'+w_id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                var Itype = $('#Itype').val();
                var Iformat = $('#Iformat').val();
                if(Itype == null)
                {
                    alert('Select Invoice Type First');
                }
                else
                {
                    var price = 0;
                    var price2 = 0;
                    var mrpTax =0;
                    var taxAmount = 0;
                    var oTax = 0;
                    if(Iformat == 'GST')
                    {
                        if(data[0].product == null) //checks either the data is from variants or product
                        {
                            if(data[0].total_quantity == null)
                            {
                                total_quantity=0;
                            }
                            else
                            {
                                total_quantity = data[0].total_quantity;
                            }
                            if(Itype == 'MRP')
                            {
                                price =data[0].mrp == null ? 0 : data[0].mrp;
                                price2 =data[0].tp == null ? 0 : data[0].tp;

                                mrpTax = Math.round(data[0].mrp - (data[0].mrp / data[2].tax));
                                console.log(mrpTax,data[0].mrp,data[2].tax);
                                taxAmount = +mrpTax + +price;
                                oTax = mrpTax;
                                if($('#'+data[0].id).length){
                                    console.log('dsd');
                                }
                                else
                                {

                                    $("#exampleG").append("<tr id='"+data[0].id+"'><td><input type='hidden' id='otax"+rowCount+"' value='"+oTax+"'><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input style='width:75px' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_name+" </td><td>"+data[0].brands.b_name+"</td><td><input style='width:75px' type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input  type='hidden' name='type[]' value='"+data[1]+"'><input style='width:75px' type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:85px' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input style='width:85px' readonly required type='text' class='form-control mrp mrp"+rowCount+"' min='1' id='mrp"+rowCount+"' data-id='mrp"+rowCount+"' name='price2[]' value='"+price2+"'></td><td><input type='text' style='width:75px' class='form-control net nv"+rowCount+"' readonly id='nv"+rowCount+"' name='vet[]' value='"+price+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"'  name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:75px' type='text' readonly class='form-control ads ad"+rowCount+"' id='ad"+rowCount+"' name='ad[]'></td><td><input name='taxA[]' type='text' style='width:85px' id='taxA"+rowCount+"' readonly class='form-control ttax taxA"+rowCount+"' value='"+mrpTax+"'></td><td><input style='width:110px' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"'  value='"+taxAmount+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                }
                            }
                            else
                            {
                                price =data[0].mrp == null ? 0 : data[0].mrp;
                                price2 =data[0].tp == null ? 0 : data[0].tp;

                                mrpTax = Math.round( price * (data[2].tax - 1));
                                taxAmount = +mrpTax + +price;
                                oTax = data[2].tax;
                                if($('#'+data[0].id).length){
                                    console.log('dsd');
                                }
                                else
                                {

                                    $("#exampleG").append("<tr id='"+data[0].id+"'><td><input type='hidden' id='otax"+rowCount+"' value='"+oTax+"'><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input style='width:75px' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_name+" </td><td>"+data[0].brands.b_name+"</td><td><input style='width:75px' type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input  type='hidden' name='type[]' value='"+data[1]+"'><input style='width:75px' type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:85px' required type='text' readonly class='form-control mrp mrp"+rowCount+"' min='1' id='mrp"+rowCount+"' data-id='mrp"+rowCount+"' name='price[]' value='"+price+"'></td><td><input style='width:85px' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price2[]' value='"+price2+"'></td><td><input type='text' style='width:75px' class='form-control net nv"+rowCount+"' readonly id='nv"+rowCount+"' name='vet[]' value='"+price+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"'  name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:75px' type='text' readonly class='form-control ads ad"+rowCount+"' id='ad"+rowCount+"' name='ad[]'></td><td><input name='taxA[]' type='text' style='width:85px' id='taxA"+rowCount+"' readonly class='form-control ttax taxA"+rowCount+"' value='"+mrpTax+"'></td><td><input style='width:110px' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"'  value='"+taxAmount+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                }
                            }

                        }
                        else
                        {

                            if(data[0].total_quantity == null)
                            {
                                total_quantity=0;
                            }
                            else
                            {
                                total_quantity = data[0].total_quantity;
                            }
                            if(Itype == 'MRP')
                            {
                                price =data[0].mrp == null ? 0 : data[0].mrp;
                                price2 =data[0].tp == null ? 0 : data[0].tp;
                                mrpTax = Math.round(data[0].price - (data[0].price / data[2].tax));
                                taxAmount = +mrpTax + +price;
                                oTax = mrpTax;
                                if($('#'+data[0].id).length){
                                    console.log('dsd');
                                }
                                else
                                {
                                    var name=data[0].name.split(' - ');
                                    $("#exampleG").append("<tr id='"+data[0].id+"'><td><input type='hidden' id='otax"+rowCount+"' value='"+oTax+"'><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input style='width:75px' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+name[1]+"</td><td>"+data[0].product.brands.b_name+"</td><td><input style='width:75px' type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input  type='hidden' name='type[]' value='"+data[1]+"'><input style='width:75px' type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:85px' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input style='width:85px' readonly required type='text' class='form-control mrp mrp"+rowCount+"' min='1' id='mrp"+rowCount+"' data-id='mrp"+rowCount+"' name='price2[]' value='"+price2+"'></td><td><input type='text' style='width:75px' class='form-control net nv"+rowCount+"' id='nv"+rowCount+"' name='vet[]' readonly value='"+price+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"'  name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:75px' type='text' readonly class='form-control ads ad"+rowCount+"' id='ad"+rowCount+"' name='ad[]'></td><td><input type='text' style='width:85px' readonly name='taxA[]' class='form-control ttax taxA"+rowCount+"' id='taxA"+rowCount+"' value='"+mrpTax+"'></td><td><input style='width:110px' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"'  value='"+taxAmount+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                }
                            }
                            else
                            {
                                price =data[0].mrp == null ? 0 : data[0].mrp;
                                price2 =data[0].tp == null ? 0 : data[0].tp;
                                mrpTax = Math.round( price * (data[2].tax - 1));
                                taxAmount = +mrpTax + +price;
                                oTax = data[2].tax;
                                if($('#'+data[0].id).length){
                                    console.log('dsd');
                                }
                                else
                                {
                                    var name=data[0].name.split(' - ');
                                    $("#exampleG").append("<tr id='"+data[0].id+"'><td><input type='hidden' id='otax"+rowCount+"' value='"+oTax+"'><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input style='width:75px' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+name[1]+"</td><td>"+data[0].product.brands.b_name+"</td><td><input style='width:75px' type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input  type='hidden' name='type[]' value='"+data[1]+"'><input style='width:75px' type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:85px' required type='text' readonly class='form-control mrp mrp"+rowCount+"' min='1' id='mrp"+rowCount+"' data-id='mrp"+rowCount+"' name='price[]' value='"+price+"'></td><td><input style='width:85px' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price2[]' value='"+price2+"'></td><td><input type='text' style='width:75px' class='form-control net nv"+rowCount+"' id='nv"+rowCount+"' name='vet[]' readonly value='"+price+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:75px' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"'  name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:75px' type='text' readonly class='form-control ads ad"+rowCount+"' id='ad"+rowCount+"' name='ad[]'></td><td><input type='text' style='width:85px' readonly name='taxA[]' class='form-control ttax taxA"+rowCount+"' id='taxA"+rowCount+"' value='"+mrpTax+"'></td><td><input style='width:110px' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"'  value='"+taxAmount+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                                }
                            }

                        }
                        var amount=0;
                        var total=0;
                        var rowCount1 = $('#exampleG tr').length;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            total = +total + +$('.price'+i).val();
                        }
                        $('#amount').html(amount);
                        $('#l_amount').val(amount);
                        $('#final').val(amount);
                        $('#p_total1').val(amount);
                    }
                    else
                    {
                        if(data[0].product == null) //checks either the data is from variants or product
                        {
                            if(Itype == 'MRP')
                            {
                                price =data[0].mrp;
                            }
                            else
                            {
                                price =data[0].tp;
                            }
                            if(data[0].total_quantity == null)
                            {
                                total_quantity=0;
                            }
                            else
                            {
                                total_quantity = data[0].total_quantity;
                            }

                            if(data[0].weight == null)
                            {
                                weight='';
                            }
                            else
                            {
                                weight = data[0].weight;
                            }
                            if($('#'+data[0].id).length){
                                console.log('dsd');
                            }
                            else
                            {
                                $("#exampleP").append("<tr id='"+data[0].id+"'><td><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_name+"</td><td>"+data[0].brands.b_name+" </td><td><input type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input type='text' class='form-control net nv"+rowCount+"' readonly id='nv"+rowCount+"' value='"+price+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                            }
                        }
                        else
                        {
                            if(Itype == 'MRP')
                            {
                                price =data[0].price; //in variants price attribute used as mrp
                            }
                            else
                            {
                                price =data[0].tp;
                            }
                            if(data[0].total_quantity == null)
                            {
                                total_quantity=0;
                            }
                            else
                            {
                                total_quantity = data[0].total_quantity;
                            }
                            if($('#'+data[0].id).length){
                                console.log('dsd');
                            }
                            else
                            {
                                var name=data[0].name.split(' - ');
                                $("#exampleP").append("<tr id='"+data[0].id+"'><td><input type='hidden' name='cost[]' value='"+data[0].cost+"'><input name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+name[1]+"</td><td>"+data[0].product.brands.b_name+"</td><td><input type='text' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='text' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+price+"'></td><td><input type='text' class='form-control net nv"+rowCount+"' readonly id='nv"+rowCount+"' value='"+price+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+price+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                            }
                        }
                        var amount=0;
                        var total=0;
                        var rowCount1 = $('#exampleP tr').length;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            total = +total + +$('.price'+i).val();
                        }
                        $('#amount').html(amount);
                        $('#l_amount').val(amount);
                        $('#final').val(amount);
                        $('#p_total1').val(amount);
                    }
                }


            }
        });
    });



    $('table').on('click', '.delete', function(e){
        var id=$(this).attr('id');
        console.log(id);
        var className = $(this).closest('table').attr('class');

        $(this).closest('tr').remove();

        if(className.includes('GST')){
            $(".ttax").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'taxA'+(i+1));
                $(this).removeClass('taxA'+(i+2));
                $(this).addClass('taxA'+(i+1));
            });
            $(".ads").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'ad'+(i+1));
                $(this).removeClass('ad'+(i+2));
                $(this).addClass('ad'+(i+1));
            });
            $(".mrp").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'mrp'+(i+1));
                $(this).removeClass('mrp'+(i+2));
                $(this).addClass('mrp'+(i+1));
                $(this).removeAttr('data-id');
                $(this).attr('data-id', 'mrp'+(i+1));
            });
        }


        $(".abc").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'sub'+(i+1));
           $(this).removeClass('sub'+(i+2));
           $(this).addClass('sub'+(i+1));
        });
        $(".net").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'nv'+(i+1));
           $(this).removeClass('nv'+(i+2));
           $(this).addClass('nv'+(i+1));
        });
        $(".price").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'price'+(i+1));
            $(this).removeAttr('data-id');
            $(this).attr('data-id', (i+1));
           $(this).removeClass('price'+(i+2));
           $(this).addClass('price'+(i+1));
        });
        $(".discountp").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'percent'+(i+1));

            $(this).removeAttr('data-id');
            $(this).attr('data-id', (i+1));
           $(this).removeClass('percent'+(i+2));
           $(this).addClass('percent'+(i+1));
        });
        $(".discountr").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'rate'+(i+1));
            $(this).removeAttr('data-id');
            $(this).attr('data-id', (i+1));
           $(this).removeClass('rate'+(i+2));
           $(this).addClass('rate'+(i+1));
        });
        $(".delete").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });
        $(".quantity").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });

        var Iformat = $('#Iformat').val();
        if(Iformat =='GST')
        {
            var rowCount = $('#exampleG tr').length;
        }
        else
        {
            var rowCount = $('#exampleP tr').length;
        }
        var amount = 0;
        for (let i = 1; i < rowCount; i++) {
            amount = +amount + +$('.sub'+i).val();
        }
        $('#amount').html(amount);
        $('#l_amount').val(amount);
        $('#final').val(amount);
        if($('#p_total').val() == 0)
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }

        $('#amount').html(amount);
        $('#l_amount').val(amount);

        $('#final').val(amount);
        if($('#p_total').val() == '')
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }

    });

    $('table').on('keyup','.quantity',function(){
        var a=$(this).val();
        var id=$(this).attr('id');
        var price=$('#price'+id).val();
        var price2=$('.mrp'+id).val();
        var amount=0;
        var sub= (a*price).toFixed(2);
        var sub2= (a*price2).toFixed(2);

        var Iformat = $('#Iformat').val();
        $('#nv'+id).val(sub);
        var disc = $('#percent'+id).val();

        var tax = $('#otax'+id).val();
        if(Iformat =='GST')
        {
            var Itype = $('#Itype').val();
            if(Itype == 'MRP')
            {
                var rowCount = $('#exampleG tr').length;
                if(disc == '')
                {
                    $('#ad'+id).val('0');
                    if(tax == '' || tax == 0)
                    {
                        tax = price2 - (price2 / 1.17);
                        var taxx = tax * a;
                        // console.log(taxx,tax,a);
                        var vet = 0;
                        vet = +sub + +tax;
                        $('#taxA'+id).val(taxx.toFixed(2));
                        $('#sub'+id).val(vet.toFixed(2));
                    }
                    else
                    {
                        if(a !='')
                        {
                            var taxx = tax * a;
                            // console.log(taxx,tax,a);
                            var vet = 0;
                            vet = +sub + +tax;
                            $('#taxA'+id).val(taxx.toFixed(2));
                            $('#sub'+id).val(vet.toFixed(2));
                        }
                    }
                }
                else
                {
                    var discount = ((sub2 / 100) * disc);
                    $('#rate'+id).val(discount.toFixed(2));
                    var vad = 0;
                    vad = sub2 - discount;
                    $('#ad'+id).val(vad.toFixed(2));

                    if(tax == '' || tax == 0)
                    {
                        tax = sub - (sub / 1.17);
                        // var taxx = tax * a;
                        var vet = 0;
                        vet = +vad + +tax;
                        $('#taxA'+id).val(tax.toFixed(2));
                        $('#sub'+id).val(vet.toFixed(2));
                    }
                    else
                    {
                        if(a !='')
                        {
                            tax = sub - (sub / 1.17);
                            // var taxx = tax * a;
                            var vet = 0;
                            vet = +vad + +tax;
                            $('#taxA'+id).val(tax.toFixed(2));
                            $('#sub'+id).val(vet.toFixed(2));
                        }
                    }
                }
            }
            else
            {
                var OriginalTax = $('#otax'+id).val();
                var tax = $('#taxA'+id).val();

                var rowCount = $('#exampleG tr').length;
                if(disc == 0 || disc == '' )
                {
                    var OriginalTax = $('#otax'+id).val();
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub);
                    // }
                    // else
                    // {
                        var vet = 0;
                        var abc = sub * (OriginalTax - 1);
                        $('#taxA'+id).val(abc.toFixed(2));
                        vet = +abc + +sub;
                        $('#sub'+id).val(vet.toFixed(2));
                    // }
                }
                else
                {
                    var discount = ((sub / 100) * disc);
                    var OriginalTax = $('#otax'+id).val();
                    $('#rate'+id).val(discount.toFixed(2));
                    var vad = 0;
                    vad = sub - discount;
                    $('#ad'+id).val(vad.toFixed(2));

                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(vad.toFixed(2));
                    // }
                    // else
                    // {
                        var vet = 0;
                        var abc = vad * (OriginalTax - 1);
                        $('#taxA'+id).val(abc.toFixed(2));
                        vet = +vad + +abc;
                        $('#sub'+id).val(vet.toFixed(2));
                    // }
                }

            }
        }
        else
        {
            var rowCount = $('#exampleP tr').length;
            if(disc == '')
            {
                $('#sub'+id).val(sub);
            }
            else
            {
                var discount = ((sub / 100) * disc);
                $('#rate'+id).val(discount.toFixed(2));
                sub = sub - discount;
                $('#sub'+id).val(sub.toFixed(2));
            }
        }

        for (let i = 1; i < rowCount; i++) {
            amount = +amount + +$('.sub'+i).val();
        }
        $('#amount').html(amount);
        $('#l_amount').val(amount);
        $('#final').val(amount);
        if($('#p_total').val() == 0)
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }

    });

    $('table').on('keyup','.price',function(){
        var id=$(this).attr('data-id');
        var price=$(this).val();
        var quan=$('#'+id);
        quan=quan.val();
        var sub=(quan * price).toFixed(2);
        var Iformat = $('#Iformat').val();
        $('#nv'+id).val(sub);
        var disc = $('#percent'+id).val();
        var nv = $('#nv'+id).val();


        var amount=0;
        var total=0;

        if(Iformat =='GST')
        {
            var tax = $('#taxA'+id).val();
            var Itype = $('#Itype').val();
            var rowCount1 = $('#exampleG tr').length;
            if(disc == 0.00 || disc == '')
            {
                if(Itype == 'MRP')
                {
                    var tax = $('#otax'+id).val();
                    if(price !='')
                    {
                        var tp = (price /100)*5;
                        var tpf= price - tp;
                        $('#mrp'+id).val(tpf);
                        var sub=(quan * price).toFixed(2);
                        var taxx = nv - ( nv / 1.17) ;
                        var vet = 0;
                        vet = +sub + +taxx;
                        // $('#ad'+id).val(sub.toFixed(2));
                        $('#sub'+id).val(vet.toFixed(2));
                        $('#taxA'+id).val(taxx.toFixed(2));
                    }
                }
                else
                {
                    var OriginalTax = $('#otax'+id).val();
                    var vet = 0;
                    if(price !='')
                    {
                        var abc = sub * (OriginalTax - 1);
                        $('#taxA'+id).val(abc.toFixed(2));
                        vet = +abc + +price;
                        $('#sub'+id).val(vet.toFixed(2));
                    }
                }
            }
            else
            {
                if(Itype == 'MRP')
                {
                    var tp = (price /100)*5;
                    var tpf= price - tp;
                    $('#mrp'+id).val(tpf);
                    var sub2 = tpf * quan;
                    var discount = ((sub2 / 100) * disc);
                    $('#rate'+id).val(discount);
                    var vad = 0;
                    vad = sub2 - discount;
                    $('#ad'+id).val(vad.toFixed(2));

                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(vad.toFixed(2));
                    // }
                    // else
                    // {
                        var vet = 0;
                        var nv = $('#nv'+id).val();
                        var taxx = nv - ( nv / 1.17) ;
                        vet = +vad + +taxx;
                        $('#taxA'+id).val(taxx.toFixed(2));
                        $('#sub'+id).val(vet.toFixed(2));
                    // }
                }
                else
                {
                    var discount = ((sub / 100) * disc);

                    var OriginalTax = $('#otax'+id).val();
                    $('#rate'+id).val(discount.toFixed(2));
                    var vad = 0;
                    vad = sub - discount;
                    $('#ad'+id).val(vad.toFixed(2));

                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(vad.toFixed(2));
                    // }
                    // else
                    // {
                        var vet = 0;
                        var abc = sub * (OriginalTax - 1);
                        $('#taxA'+id).val(abc.toFixed(2));
                        vet = +vad + +abc;
                        $('#sub'+id).val(vet.toFixed(2));
                    // }
                }
            }
        }
        else
        {
            var rowCount1 = $('#exampleP tr').length;
            if(disc == '')
            {
                $('#sub'+id).val(sub);
            }
            else
            {
                var discount = ((sub / 100) * disc);
                $('#rate'+id).val(discount);
                sub = sub - discount;
                $('#sub'+id).val(sub.toFixed(2));
            }
        }
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            total = +total + +$('.price'+i).val();
        }
        $('#amount').html(amount);

        $('#final').val(amount);
        if($('#p_total').val() == '')
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }

    });


    $('table').on('keyup','.discountp',function(){
        var id=$(this).attr('data-id');
        var percent=$(this).val();
        var price=$('#mrp'+id).val();
        var quan=$('#'+id);
        quan=quan.val();
        var sub=(quan * price).toFixed(2);
        var amount=0;
        var total=0;
        var Iformat = $('#Iformat').val();
        if(Iformat =='GST')
        {
            var tax = $('#taxA'+id).val();
            var Itype = $('#Itype').val();
            var OriginalTax = $('#otax'+id).val();
            if(Itype == 'MRP')
            {
                if($(this).val() == '')
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        $('#ad'+id).val('');
                        $('#rate'+id).val('');
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }
                }
                else
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    //     $('#ad'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        var disc = (sub * percent)/100;
                        sub = sub - disc;

                        $('#rate'+id).val(disc.toFixed(2));
                        $('#ad'+id).val(sub.toFixed(2));
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }
                }
            }
            else
            {
                if($(this).val() == '')
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    //     $('#ad'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        $('#ad'+id).val('');
                        $('#rate'+id).val('');
                        // sub = sub * (OriginalTax - 1);
                        // $('#taxA'+id).val(sub.toFixed(2));
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }

                }
                else
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    //     $('#ad'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        var disc = (sub * percent)/100;
                        sub = sub - disc;

                        $('#rate'+id).val(disc.toFixed(2));
                        var vet = 0;
                        $('#ad'+id).val(sub.toFixed(2));
                        // vet = sub * (OriginalTax - 1);
                        // $('#taxA'+id).val(vet.toFixed(2));
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }
                }
            }
            var rowCount1 = $('#exampleG tr').length;
        }
        else
        {
            $('#sub'+id).val(sub.toFixed(2));
            var rowCount1 = $('#exampleP tr').length;
        }
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            total = +total + +$('.price'+i).val();
        }
        $('#amount').html(amount);

        $('#final').val(amount);
        if($('#p_total').val() == '')
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }

    });



    $('table').on('keyup','.discountr',function(){
        var id=$(this).attr('data-id');
        var percent=$(this).val();
        var price=$('#mrp'+id).val();
        var quan=$('#'+id);
        quan=quan.val();
        var sub=(quan * price).toFixed(2);
        var disc = (percent/sub) * 100;
        sub = sub - percent;

        $('#percent'+id).val(disc.toFixed(2));
        var amount=0;
        var total=0;
        var Iformat = $('#Iformat').val();
        if(Iformat =='GST')
        {
            var tax = $('#taxA'+id).val();
            var Itype = $('#Itype').val();
            var OriginalTax = $('#otax'+id).val();
            if(Itype == 'MRP')
            {
                if($(this).val() == '')
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        $('#ad'+id).val('');
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }
                }
                else
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    //     $('#ad'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        $('#ad'+id).val(sub.toFixed(2));
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }
                }
            }
            else
            {
                if($(this).val() == '')
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    //     $('#ad'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        $('#ad'+id).val('');
                        sub = sub * (OriginalTax - 1);
                        $('#taxA'+id).val(sub.toFixed(2));
                        var afterTax = 0;
                        afterTax = +sub + +tax;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }

                }
                else
                {
                    // if(tax == null)
                    // {
                    //     $('#sub'+id).val(sub.toFixed(2));
                    //     $('#ad'+id).val(sub.toFixed(2));
                    // }
                    // else
                    // {
                        var vet = 0;
                        $('#ad'+id).val(sub.toFixed(2));
                        vet = sub * (OriginalTax - 1);
                        $('#taxA'+id).val(vet.toFixed(2));
                        var afterTax = 0;
                        afterTax = +sub + +vet;
                        $('#sub'+id).val(afterTax.toFixed(2));
                    // }
                }
            }
            var rowCount1 = $('#exampleG tr').length;
        }
        else
        {
            $('#sub'+id).val(sub.toFixed(2));
            var rowCount1 = $('#exampleP tr').length;
        }
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            total = +total + +$('.price'+i).val();
        }
        $('#amount').html(amount);

        $('#final').val(amount);
        if($('#p_total').val() == '')
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }

    });

</script>
@endsection
