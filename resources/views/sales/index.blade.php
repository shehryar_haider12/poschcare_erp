@extends('layouts.master')
@section('top-styles')
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<style>
    .tableview
    {
        background-color:transparent;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="{{url('')}}/sales">Sales</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-credit-card font-white"></i>View Sales
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    @if ($index == 0)
                                                        <a style="margin-left:-20px; "  href="{{route('sales.excel')}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 1)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$year}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/year">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 2)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$month}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/month">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 3)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$date}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 4)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$from}}/{{$to}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 5)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$c}}/{{$sp}}/{{$ss}}/{{$check}}/excel/ss">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 6)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$c}}/{{$sp}}/{{$ps}}/{{$check}}/excel/ps">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 7)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$c}}/excel/c">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 8)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$sp}}/excel/sp">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 9 || $index == 10 || $index == 11 || $index == 12)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/sales/search/{{$index}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('sales.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Sales</button>
                                                    </a>
                                                @endif

                                                @if(in_array('Sync',$permissions))
                                                    <a id="GFG" href="{{route('sales.sync')}}" >
                                                        <button style="background: #00CCFF; margin-left:-80px; margin-top:-32px" type="button"  class="btn  btn-primary btn-sm ">Sync</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        @if (in_array('importExcel',$permissions))
                                            <div class="tableview">
                                                <a href="{{route('sales.importStructure')}}" download>
                                                    <button style="background: #000000; margin-left:20px; margin-top:-2px" type="button"  class="btn btn-primary btn-sm ">
                                                    <i class='fa fa-download'></i>
                                                    Download Structure</button>
                                                </a>
                                                <form id="Import_product" action="{{route('import.saleOrder')}}" method="post" enctype="multipart/form-data" id="advanceSearch">
                                                    <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="col-md-4">
                                                            <input type="file" name="csv_file" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <button style="background: #000000; margin-left:-90px; margin-top:-2px" type="submit"  class="btn btn-primary btn-sm ">Import Csv</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                        <div class="custom_datatable">

                                            <form action="{{url('')}}/sales/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" id="menuid" name="menuid">
                                                <div class="tableview">
                                                    @if ($roleName->name == 'Admin')
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="PStatus">By Payment Status
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                            </label>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Year</label>
                                                                    <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">From</label>
                                                                    <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">To</label>
                                                                    <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Customer Name</label>
                                                                    <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($customer as $u)
                                                                        <option  value="{{$u->id}}">Company: {{$u->company}} ; Contact Person: {{$u->name}} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sale Persons Name</label>
                                                                    <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($saleperson as $u)
                                                                        <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sales Status</label>
                                                                    <select name="status" id="status" class="form-control " disabled>
                                                                        <option disabled selected>No Filter</option>
                                                                        <option>Pending</option>
                                                                        <option>Approved</option>
                                                                        <option>Partial</option>
                                                                        <option>Complete</option>
                                                                        <option>Delivered</option>
                                                                        <option>Return</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Payment Status</label>
                                                                    <select name="p_status" id="p_status" class="form-control " disabled>
                                                                        <option disabled selected>No Filter</option>
                                                                        <option>Pending</option>
                                                                        <option>Partial</option>
                                                                        <option>Paid</option>
                                                                        <option>Return</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                                {{-- <div class="col-md-8"></div> --}}
                                                                <div class="col-md-4">
                                                                <div class="form-group">
                                                                    {{-- <label for="">Unit Name</label> --}}
                                                                    <label for="" style="visibility: hidden">.</label>
                                                                    <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    @endif
                                                    @if ($roleName->name == 'warehouse person')
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                            </label>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Year</label>
                                                                    <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">From</label>
                                                                    <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">To</label>
                                                                    <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Customer Name</label>
                                                                    <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($customer as $u)
                                                                        <option  value="{{$u->id}}">Company: {{$u->company}} ; Contact Person: {{$u->name}} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sale Persons Name</label>
                                                                    <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($saleperson as $u)
                                                                        <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sales Status</label>
                                                                    <select name="status" id="status" class="form-control " disabled>
                                                                        <option disabled selected>No Filter</option>
                                                                        <option>Approved</option>
                                                                        <option>Partial</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                                {{-- <div class="col-md-8"></div> --}}
                                                                <div class="col-md-4">
                                                                <div class="form-group">
                                                                    {{-- <label for="">Unit Name</label> --}}
                                                                    <label for="" style="visibility: hidden">.</label>
                                                                    <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    @endif
                                                    @if ($roleName->name == 'DeliveryBoy')
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                            </label>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Year</label>
                                                                    <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">From</label>
                                                                    <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">To</label>
                                                                    <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Customer Name</label>
                                                                    <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($customer as $u)
                                                                        <option  value="{{$u->id}}">Company: {{$u->company}} ; Contact Person: {{$u->name}} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sale Persons Name</label>
                                                                    <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($saleperson as $u)
                                                                        <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sales Status</label>
                                                                    <select name="status" id="status" class="form-control " disabled>
                                                                        <option disabled selected>No Filter</option>
                                                                        <option>Partial</option>
                                                                        <option>Complete</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                                {{-- <div class="col-md-8"></div> --}}
                                                                <div class="col-md-4">
                                                                <div class="form-group">
                                                                    {{-- <label for="">Unit Name</label> --}}
                                                                    <label for="" style="visibility: hidden">.</label>
                                                                    <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    @endif
                                                    @if ($roleName->name == 'Sale Person')
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                            </label>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Year</label>
                                                                    <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">From</label>
                                                                    <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">To</label>
                                                                    <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Customer Name</label>
                                                                    <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($customer as $u)
                                                                        <option  value="{{$u->id}}">Company: {{$u->company}} ; Contact Person: {{$u->name}} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                                {{-- <div class="col-md-8"></div> --}}
                                                                <div class="col-md-4">
                                                                <div class="form-group">
                                                                    {{-- <label for="">Unit Name</label> --}}
                                                                    <label for="" style="visibility: hidden">.</label>
                                                                    <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    @endif
                                                    @if ($roleName->name == 'Sale Manager')
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="PStatus">By Payment Status
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                            </label>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Year</label>
                                                                    <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">From</label>
                                                                    <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">To</label>
                                                                    <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Customer Name</label>
                                                                    <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($customer as $u)
                                                                        <option  value="{{$u->id}}">Company: {{$u->company}} ; Contact Person: {{$u->name}} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sale Persons Name</label>
                                                                    <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($saleperson as $u)
                                                                            @if ($u->name == 'Ahsan Iqbal' || $u->name == 'Haider Ali' || $u->name == 'Subhan')
                                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Sales Status</label>
                                                                    <select name="status" id="status" class="form-control " disabled>
                                                                        <option disabled selected>No Filter</option>
                                                                        <option>Pending</option>
                                                                        <option>Approved</option>
                                                                        <option>Partial</option>
                                                                        <option>Complete</option>
                                                                        <option>Delivered</option>
                                                                        <option>Return</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Payment Status</label>
                                                                    <select name="p_status" id="p_status" class="form-control " disabled>
                                                                        <option disabled selected>No Filter</option>
                                                                        <option>Pending</option>
                                                                        <option>Partial</option>
                                                                        <option>Paid</option>
                                                                        <option>Return</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                                {{-- <div class="col-md-8"></div> --}}
                                                                <div class="col-md-4">
                                                                <div class="form-group">
                                                                    {{-- <label for="">Unit Name</label> --}}
                                                                    <label for="" style="visibility: hidden">.</label>
                                                                    <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1400px">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:150px;">S.No</th>
                                                            <th style="width:150px;">Ref.No</th>
                                                            <th style="width:150px;">Invoice No</th>
                                                            <th style="width:150px;">Invoice Type</th>
                                                            <th style="width:150px;">Sales Date</th>
                                                            <th style="width:200px;">Action</th>
                                                            <th style="width:200px;">Company</th>
                                                            <th style="width:200px;">Contact Person</th>
                                                            <th style="width:100px;">Sale Status</th>
                                                            <th style="width:100px;">Payment Status</th>
                                                            <th style="width:100px;">Sale Person</th>
                                                            <th style="width:100px;">Total</th>
                                                            <th style="width:100px;">Paid</th>
                                                            <th style="width:100px;">Balance</th>
                                                            <th style="width:300px;">Expected Delivery Date</th>
                                                            <th style="width:200px;">Warehouse</th>
                                                            <th style="width:200px;">Biller</th>
                                                            <th style="width:200px;">Customer Remaining Balance</th>
                                                            <th style="width:100px;">Return Status</th>
                                                            <th style="width:100px;">Sale Type</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $totalAmount = 0;
                                                            $totalBalance = 0;
                                                            $totalPaid = 0;
                                                            $count = count($sales);
                                                        @endphp
                                                        @foreach ($sales as $s)
                                                            {{-- @if ($counter == null) --}}
                                                                <tr>
                                                                    <td style="width:150px;">
                                                                        {{$s->id}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->ref_no}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->Ino}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->Iformat == 'Plain' ? 'Without GST' : $s->Iformat}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->sale_date}}
                                                                    </td>
                                                                    <td>
                                                                        @if ($s->s_status == 'Complete' && $s->p_status == 'Paid')
                                                                            <select class="form-control action" id="{{$s->id}}" >
                                                                                <option >Actions</option>
                                                                                @if(in_array('view invoices',$permissions))
                                                                                    <option >View All Sale Reports</option>
                                                                                @endif
                                                                                @if(in_array('show',$permissions))
                                                                                    <option >Sale Details</option>
                                                                                @endif
                                                                                @if ($adminName != 'Danish Messani')
                                                                                    @if(in_array('Edit sale ledger',$permissions))
                                                                                        <option >Edit Invoice</option>
                                                                                    @endif
                                                                                @endif
                                                                                @if(in_array('report',$permissions))
                                                                                    <option >Sale Report</option>
                                                                                @endif
                                                                                @if ($s->return_status == 'No Return')
                                                                                    @if(in_array('Return',$permissions))
                                                                                        <option >Sale Return</option>
                                                                                    @endif
                                                                                @endif
                                                                                @if(in_array('View Payment',$permissions))
                                                                                    <option >View Payment</option>
                                                                                @endif
                                                                                @if(in_array('Add doc',$permissions))
                                                                                    <option >Attach Documents</option>
                                                                                @endif
                                                                                @if(in_array('View doc',$permissions))
                                                                                    <option >View Documents</option>
                                                                                @endif
                                                                                @if ($s->saletype->name !='Pos')
                                                                                    @if(in_array("View Gdn's",$permissions))
                                                                                        <option >View GDN History</option>
                                                                                    @endif
                                                                                @endif
                                                                            </select>
                                                                        @else
                                                                            @if ($s->s_status == 'Delivered' && $s->p_status == 'Paid')
                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Sale Details</option>
                                                                                    @endif
                                                                                    @if ($adminName != 'Danish Messani')
                                                                                        @if(in_array('Edit sale ledger',$permissions))
                                                                                            <option >Edit Invoice</option>
                                                                                        @endif
                                                                                    @endif
                                                                                    @if(in_array('view invoices',$permissions))
                                                                                        <option >View All Sale Reports</option>
                                                                                    @endif
                                                                                    @if(in_array('report',$permissions))
                                                                                        <option >Sale Report</option>
                                                                                    @endif
                                                                                    @if ($s->return_status == 'No Return')
                                                                                        @if(in_array('Return',$permissions))
                                                                                            <option >Sale Return</option>
                                                                                        @endif
                                                                                    @endif
                                                                                    @if(in_array('View Payment',$permissions))
                                                                                        <option >View Payment</option>
                                                                                    @endif
                                                                                    @if ($s->saletype->name !='Pos')
                                                                                        @if(in_array("View Gdn's",$permissions))
                                                                                            <option >View GDN History</option>
                                                                                        @endif
                                                                                    @endif
                                                                                    @if(in_array('Add doc',$permissions))
                                                                                        <option >Attach Documents</option>
                                                                                    @endif
                                                                                    @if(in_array('View doc',$permissions))
                                                                                        <option >View Documents</option>
                                                                                    @endif
                                                                                </select>
                                                                            @else
                                                                                @if ($s->s_status == 'Delivered' && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                        <option >Actions</option>
                                                                                        @if(in_array('Add Payment',$permissions))
                                                                                            <option>Add Payment</option>
                                                                                        @endif
                                                                                        @if ($adminName != 'Danish Messani')
                                                                                            @if(in_array('Edit sale ledger',$permissions))
                                                                                                <option >Edit Invoice</option>
                                                                                            @endif
                                                                                        @endif
                                                                                        @if(in_array('view invoices',$permissions))
                                                                                            <option >View All Sale Reports</option>
                                                                                        @endif
                                                                                        @if(in_array('show',$permissions))
                                                                                            <option >Sale Details</option>
                                                                                        @endif
                                                                                        @if(in_array('report',$permissions))
                                                                                            <option >Sale Report</option>
                                                                                        @endif
                                                                                        @if ($s->return_status == 'No Return')
                                                                                            @if(in_array('Return',$permissions))
                                                                                                <option >Sale Return</option>
                                                                                            @endif
                                                                                        @endif
                                                                                        @if(in_array('View Payment',$permissions))
                                                                                            <option >View Payment</option>
                                                                                        @endif
                                                                                        @if ($s->saletype->name !='Pos')
                                                                                            @if(in_array("View Gdn's",$permissions))
                                                                                                <option >View GDN History</option>
                                                                                            @endif
                                                                                        @endif
                                                                                        @if(in_array('Add doc',$permissions))
                                                                                            <option >Attach Documents</option>
                                                                                        @endif
                                                                                        @if(in_array('View doc',$permissions))
                                                                                            <option >View Documents</option>
                                                                                        @endif
                                                                                    </select>
                                                                                @else
                                                                                    @if($s->s_status == 'Complete' && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                                            <option >Actions</option>
                                                                                            @if(in_array('status Delivered',$permissions))
                                                                                                <option >Delivered</option>
                                                                                            @endif
                                                                                            @if ($adminName != 'Danish Messani')
                                                                                                @if(in_array('Edit sale ledger',$permissions))
                                                                                                    <option >Edit Invoice</option>
                                                                                                @endif
                                                                                            @endif
                                                                                            @if(in_array('view invoices',$permissions))
                                                                                                <option >View All Sale Reports</option>
                                                                                            @endif
                                                                                            @if(in_array('show',$permissions))
                                                                                                <option >Sale Details</option>
                                                                                            @endif
                                                                                            @if(in_array('report',$permissions))
                                                                                                <option >Sale Report</option>
                                                                                            @endif
                                                                                            @if(in_array('Add Payment',$permissions))
                                                                                                <option>Add Payment</option>
                                                                                            @endif
                                                                                            @if(in_array('View Payment',$permissions))
                                                                                                <option >View Payment</option>
                                                                                            @endif
                                                                                            @if ($s->saletype->name !='Pos')
                                                                                                @if(in_array("View Gdn's",$permissions))
                                                                                                    <option >View GDN History</option>
                                                                                                @endif
                                                                                            @endif
                                                                                            @if ($s->return_status == 'No Return')
                                                                                                @if(in_array('Return',$permissions))
                                                                                                    <option >Sale Return</option>
                                                                                                @endif
                                                                                            @endif
                                                                                            @if(in_array('Add doc',$permissions))
                                                                                                <option >Attach Documents</option>
                                                                                            @endif
                                                                                            @if(in_array('View doc',$permissions))
                                                                                                <option >View Documents</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    @else
                                                                                        @if($s->s_status == 'Complete' && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                            <select class="form-control action" id="{{$s->id}}" >
                                                                                                <option >Actions</option>
                                                                                                @if(in_array('status Delivered',$permissions))
                                                                                                    <option >Delivered</option>
                                                                                                @endif
                                                                                                @if ($adminName != 'Danish Messani')
                                                                                                    @if(in_array('Edit sale ledger',$permissions))
                                                                                                        <option >Edit Invoice</option>
                                                                                                    @endif
                                                                                                @endif
                                                                                                @if(in_array('view invoices',$permissions))
                                                                                                    <option >View All Sale Reports</option>
                                                                                                @endif
                                                                                                @if(in_array('show',$permissions))
                                                                                                    <option >Sale Details</option>
                                                                                                @endif
                                                                                                @if(in_array('report',$permissions))
                                                                                                    <option >Sale Report</option>
                                                                                                @endif
                                                                                                @if(in_array('Add Payment',$permissions))
                                                                                                    <option>Add Payment</option>
                                                                                                @endif
                                                                                                @if(in_array('View Payment',$permissions))
                                                                                                    <option >View Payment</option>
                                                                                                @endif
                                                                                                @if ($s->saletype->name !='Pos')
                                                                                                    @if(in_array("View Gdn's",$permissions))
                                                                                                        <option >View GDN History</option>
                                                                                                    @endif
                                                                                                @endif
                                                                                                @if ($s->return_status == 'No Return')
                                                                                                    @if(in_array('Return',$permissions))
                                                                                                        <option >Sale Return</option>
                                                                                                    @endif
                                                                                                @endif
                                                                                                @if(in_array('Add doc',$permissions))
                                                                                                    <option >Attach Documents</option>
                                                                                                @endif
                                                                                                @if(in_array('View doc',$permissions))
                                                                                                    <option >View Documents</option>
                                                                                                @endif
                                                                                            </select>
                                                                                        @else
                                                                                            @if(($s->s_status == 'Approved') && ($s->p_status == 'Partial'))
                                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                                    <option >Actions</option>
                                                                                                    @if(in_array('Ready To Deliver',$permissions))
                                                                                                        <option >Ready To Deliver</option>
                                                                                                    @endif
                                                                                                    @if(in_array('view invoices',$permissions))
                                                                                                        <option >View All Sale Reports</option>
                                                                                                    @endif

                                                                                                    @if(in_array('show',$permissions))
                                                                                                        <option >Sale Details</option>
                                                                                                    @endif
                                                                                                    {{-- @if(in_array('report',$permissions))
                                                                                                        <option >Sale Report</option>
                                                                                                    @endif --}}
                                                                                                    @if(in_array('View Payment',$permissions))
                                                                                                        <option >View Payment</option>
                                                                                                    @endif
                                                                                                    @if(in_array('Add doc',$permissions))
                                                                                                        <option >Attach Documents</option>
                                                                                                    @endif
                                                                                                    @if(in_array('View doc',$permissions))
                                                                                                        <option >View Documents</option>
                                                                                                    @endif
                                                                                                </select>
                                                                                            @else
                                                                                                @if(($s->s_status == 'Approved') && ($s->p_status == 'Pending'))
                                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                                        <option >Actions</option>
                                                                                                        @if(in_array('Ready To Deliver',$permissions))
                                                                                                            <option >Ready To Deliver</option>
                                                                                                        @endif
                                                                                                        @if(in_array('view invoices',$permissions))
                                                                                                            <option >View All Sale Reports</option>
                                                                                                        @endif
                                                                                                        @if(in_array('edit',$permissions))
                                                                                                            <option >Edit Sale</option>
                                                                                                        @endif

                                                                                                        @if(in_array('show',$permissions))
                                                                                                            <option >Sale Details</option>
                                                                                                        @endif
                                                                                                        {{-- @if(in_array('report',$permissions))
                                                                                                            <option >Sale Report</option>
                                                                                                        @endif --}}
                                                                                                        @if(in_array('View Payment',$permissions))
                                                                                                            <option >View Payment</option>
                                                                                                        @endif
                                                                                                        @if(in_array('Add doc',$permissions))
                                                                                                            <option >Attach Documents</option>
                                                                                                        @endif
                                                                                                        @if(in_array('View doc',$permissions))
                                                                                                            <option >View Documents</option>
                                                                                                        @endif
                                                                                                    </select>

                                                                                                @else

                                                                                                    @if(($s->s_status == 'Partial') && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                                                            <option >Actions</option>
                                                                                                            @if(in_array('Ready To Deliver',$permissions))
                                                                                                                <option >Ready To Deliver</option>
                                                                                                            @endif
                                                                                                            @if ($adminName != 'Danish Messani')
                                                                                                                @if(in_array('Edit sale ledger',$permissions))
                                                                                                                    <option >Edit Invoice</option>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                            @if(in_array('view invoices',$permissions))
                                                                                                                <option >View All Sale Reports</option>
                                                                                                            @endif
                                                                                                            @if(in_array('show',$permissions))
                                                                                                                <option >Sale Details</option>
                                                                                                            @endif
                                                                                                            @if ($s->return_status == 'No Return')
                                                                                                                @if(in_array('Return',$permissions))
                                                                                                                    <option >Sale Return</option>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                            @if(in_array('report',$permissions))
                                                                                                                <option >Sale Report</option>
                                                                                                            @endif
                                                                                                            @if(in_array('Add Payment',$permissions))
                                                                                                                <option>Add Payment</option>
                                                                                                            @endif
                                                                                                            @if(in_array('View Payment',$permissions))
                                                                                                                <option >View Payment</option>
                                                                                                            @endif
                                                                                                            @if ($s->saletype->name !='Pos')
                                                                                                                @if(in_array("View Gdn's",$permissions))
                                                                                                                    <option >View GDN History</option>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                            @if(in_array('Add doc',$permissions))
                                                                                                                <option >Attach Documents</option>
                                                                                                            @endif
                                                                                                            @if(in_array('View doc',$permissions))
                                                                                                                <option >View Documents</option>
                                                                                                            @endif
                                                                                                        </select>
                                                                                                    @else
                                                                                                        @if($s->s_status == 'Partial' && $s->p_status == 'Paid')
                                                                                                            <select class="form-control action" id="{{$s->id}}" >
                                                                                                                <option >Actions</option>
                                                                                                                @if(in_array('Ready To Deliver',$permissions))
                                                                                                                    <option >Ready To Deliver</option>
                                                                                                                @endif
                                                                                                                @if ($adminName != 'Danish Messani')
                                                                                                                    @if(in_array('Edit sale ledger',$permissions))
                                                                                                                        <option >Edit Invoice</option>
                                                                                                                    @endif
                                                                                                                @endif
                                                                                                                @if(in_array('view invoices',$permissions))
                                                                                                                    <option >View All Sale Reports</option>
                                                                                                                @endif

                                                                                                                @if(in_array('show',$permissions))
                                                                                                                    <option >Sale Details</option>
                                                                                                                @endif
                                                                                                                {{-- @if(in_array('report',$permissions))
                                                                                                                    <option >Sale Report</option>
                                                                                                                @endif --}}
                                                                                                                @if(in_array('View Payment',$permissions))
                                                                                                                    <option >View Payment</option>
                                                                                                                @endif
                                                                                                                @if ($s->saletype->name !='Pos')
                                                                                                                    @if(in_array("View Gdn's",$permissions))
                                                                                                                        <option >View GDN History</option>
                                                                                                                    @endif
                                                                                                                @endif

                                                                                                                @if(in_array('Add doc',$permissions))
                                                                                                                    <option >Attach Documents</option>
                                                                                                                @endif
                                                                                                                @if(in_array('View doc',$permissions))
                                                                                                                    <option >View Documents</option>
                                                                                                                @endif
                                                                                                            </select>
                                                                                                        @else
                                                                                                            @if($s->s_status == 'Approved' && $s->p_status == 'Paid')
                                                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                                                    <option >Actions</option>
                                                                                                                    @if(in_array('Ready To Deliver',$permissions))
                                                                                                                        <option >Ready To Deliver</option>
                                                                                                                    @endif
                                                                                                                    @if(in_array('view invoices',$permissions))
                                                                                                                        <option >View All Sale Reports</option>
                                                                                                                    @endif

                                                                                                                    @if(in_array('show',$permissions))
                                                                                                                        <option >Sale Details</option>
                                                                                                                    @endif
                                                                                                                    {{-- @if(in_array('report',$permissions))
                                                                                                                        <option >Sale Report</option>
                                                                                                                    @endif --}}
                                                                                                                    @if(in_array('View Payment',$permissions))
                                                                                                                        <option >View Payment</option>
                                                                                                                    @endif
                                                                                                                    @if ($s->saletype->name !='Pos')
                                                                                                                        @if(in_array("View Gdn's",$permissions))
                                                                                                                            <option >View GDN History</option>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                    @if(in_array('Add doc',$permissions))
                                                                                                                        <option >Attach Documents</option>
                                                                                                                    @endif
                                                                                                                    @if(in_array('View doc',$permissions))
                                                                                                                        <option >View Documents</option>
                                                                                                                    @endif
                                                                                                                </select>
                                                                                                            @else
                                                                                                                @if ($s->s_status == 'Return' && $s->return_status == 'Complete')
                                                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                                                        <option >Actions</option>
                                                                                                                        @if(in_array('show',$permissions))
                                                                                                                            <option >Sale Details</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('view invoices',$permissions))
                                                                                                                            <option >View All Sale Reports</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('report',$permissions))
                                                                                                                            <option >Sale Report</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('View Payment',$permissions))
                                                                                                                            <option >View Payment</option>
                                                                                                                        @endif
                                                                                                                        @if ($s->saletype->name !='Pos')
                                                                                                                            @if(in_array("View Gdn's",$permissions))
                                                                                                                                <option >View GDN History</option>
                                                                                                                            @endif
                                                                                                                        @endif
                                                                                                                    </select>
                                                                                                                @else
                                                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                                                        <option >Actions</option>
                                                                                                                        @if(in_array('edit',$permissions))
                                                                                                                            <option >Edit Sale</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('status',$permissions))
                                                                                                                            <option >Approve</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('show',$permissions))
                                                                                                                            <option >Sale Details</option>
                                                                                                                        @endif
                                                                                                                        {{-- @if(in_array('report',$permissions))
                                                                                                                            <option >Sale Report</option>
                                                                                                                        @endif --}}
                                                                                                                        @if(in_array('view invoices',$permissions))
                                                                                                                            <option >View All Sale Reports</option>
                                                                                                                        @endif
                                                                                                                    </select>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endif
                                                                                            @endif
                                                                                        @endif
                                                                                    @endif
                                                                                @endif
                                                                            @endif

                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        {{$s->customer->company}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->customer->name}}
                                                                    </td>
                                                                    @if($s->s_status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Approved')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Complete')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  green" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Return')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Delivered')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  green" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  red" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  blue" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Paid')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  green" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Return')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    <td>
                                                                        {{$s->saleperson == null ? '-' : $s->saleperson->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->total - ($s->return == null ? 0 : $s->return->total)}}
                                                                    </td>
                                                                    <td>
                                                                        {{($s->total_amount == 0 ? '0' : $s->total_amount) == $s->total ?  $s->total_amount - ($s->return == null ? 0 : $s->return->total) : $s->total_amount}}
                                                                    </td>
                                                                    <td>
                                                                        <input  width="10"  type="text" class="form-control" readonly id="amount" value="{{($s->total == $s->total_amount ? $s->total -  $s->total_amount : $s->total  - ($s->return == null ? 0 : $s->return->total) - ($s->total_amount == 0 ? '0' : $s->total_amount))  }}">
                                                                    </td>
                                                                    <td>
                                                                        {{$s->expected_date == null ? 'no' : $s->expected_date}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->biller->name}}
                                                                    </td>
                                                                    <td>
                                                                        @php
                                                                            $credit = 0;
                                                                            $debit = 0;
                                                                            $account=\App\AccountDetails::with('generalLedger')->where('name_of_account',$s->customer->company.' - '.$s->customer->name)
                                                                            ->where('Code','like','CA-01%')
                                                                            ->where('Code','not like','NCA-01%')
                                                                            ->first();
                                                                            if($account!=null)
                                                                            {
                                                                                for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                                                                                {
                                                                                    $credit += $account->generalLedger[$k]->credit;

                                                                                    $debit += $account->generalLedger[$k]->debit;
                                                                                }
                                                                                $balance =  $debit - $credit;
                                                                            }

                                                                        @endphp
                                                                        {{$balance}}
                                                                    </td>


                                                                    @php

                                                                        $totalAmount += $s->total - ($s->return == null ? 0 : $s->return->total);
                                                                        $totalPaid += ($s->total_amount == 0 ? '0' : $s->total_amount) == $s->total ?  $s->total_amount - ($s->return == null ? 0 : $s->return->total) : $s->total_amount;
                                                                        $totalBalance += ($s->total == $s->total_amount ? $s->total -  $s->total_amount : $s->total  - ($s->return == null ? 0 : $s->return->total) - ($s->total_amount == 0 ? '0' : $s->total_amount)) ;
                                                                    @endphp

                                                                    @if($s->return_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  blue" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='No Return')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  green" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Complete')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  red" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Requested')
                                                                        <td>
                                                                            <button type="button" class="btn_style btn btn-xs  yellow" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    <td>
                                                                        {{$s->saletype->name}}
                                                                    </td>


                                                                </tr>
                                                            {{-- @else
                                                                @php
                                                                    $uid = Auth::user()->id;
                                                                    $sale =  \App\SaleCounter::where('u_id',$uid)->where('co_id',$counter)->get();
                                                                @endphp
                                                                    @foreach ($sale as $ss)
                                                                        @if ($ss->s_id == $s->id)
                                                                        <tr>
                                                                            <td>
                                                                                {{$s->id}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->sale_date}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->expected_date == null ? 'no' : $s->expected_date}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->warehouse->w_name}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->biller->name}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->customer->name}}
                                                                            </td>
                                                                            <td>
                                                                                @php
                                                                                    $credit = 0;
                                                                                    $debit = 0;
                                                                                    $account=\App\AccountDetails::with('generalLedger')->where('name_of_account',$s->customer->company.' - '.$s->customer->name)
                                                                                    ->where('Code','like','CA-01%')
                                                                                    ->first();
                                                                                    // dd($account);
                                                                                    if($account!=null)
                                                                                    {
                                                                                        for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                                                                                        {
                                                                                            $credit += $account->generalLedger[$k]->credit;

                                                                                            $debit += $account->generalLedger[$k]->debit;
                                                                                        }
                                                                                        $balance =  $debit - $credit;
                                                                                    }

                                                                                @endphp
                                                                                {{$balance}}
                                                                            </td>
                                                                            <td>
                                                                                -
                                                                            </td>
                                                                            <td>
                                                                                {{$s->total}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->total_amount == 0 ? '0' : $s->total_amount}}
                                                                            </td>
                                                                            <td>
                                                                                <input  width="10"  type="text" class="form-control" readonly id="amount" value="{{$s->total - $s->total_amount}}">
                                                                            </td>
                                                                            @if($s->s_status=='Pending')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Approved')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Partial')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Complete')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Return')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Delivered')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Pending')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                        {{$s->p_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Partial')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  blue" id="{{$s->id}}">
                                                                                        {{$s->p_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Paid')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->p_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Return')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='Partial')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  blue" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='No Return')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='Complete')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='Requested')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  yellow" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            <td>
                                                                                {{$s->s_type}}
                                                                            </td>

                                                                            <td>

                                                                                    @if ($s->s_status == 'Delivered' && $s->p_status == 'Paid')


                                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                                            <option >Actions</option>
                                                                                            @if(in_array('show',$permissions))
                                                                                                <option >Sale Details</option>
                                                                                            @endif
                                                                                            @if(in_array('report',$permissions))
                                                                                                <option >Sale Report</option>
                                                                                            @endif
                                                                                            @if ($s->return_status !='Requested')
                                                                                                @if(in_array('Return',$permissions))
                                                                                                    <option >Sale Return</option>
                                                                                                @endif
                                                                                            @endif
                                                                                            @if(in_array('View Payment',$permissions))
                                                                                                <option >View Payment</option>
                                                                                            @endif
                                                                                            @if(in_array("View Gdn's",$permissions))
                                                                                                <option >View GDN History</option>
                                                                                            @endif
                                                                                            @if ($s->return_status !='Requested')
                                                                                                @if(in_array('Return',$permissions))
                                                                                                    <option >Sale Return</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        </select>
                                                                                    @else
                                                                                        @if ($s->s_status == 'Delivered' && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                            <select class="form-control action" id="{{$s->id}}" >
                                                                                                <option >Actions</option>
                                                                                                @if(in_array('Add Payment',$permissions))
                                                                                                    <option>Add Payment</option>
                                                                                                @endif
                                                                                                @if(in_array('show',$permissions))
                                                                                                    <option >Sale Details</option>
                                                                                                @endif
                                                                                                @if(in_array('report',$permissions))
                                                                                                    <option >Sale Report</option>
                                                                                                @endif
                                                                                                @if ($s->return_status !='Requested')
                                                                                                    @if(in_array('Return',$permissions))
                                                                                                        <option >Sale Return</option>
                                                                                                    @endif
                                                                                                @endif
                                                                                                @if(in_array('View Payment',$permissions))
                                                                                                    <option >View Payment</option>
                                                                                                @endif
                                                                                                @if(in_array("View Gdn's",$permissions))
                                                                                                    <option >View GDN History</option>
                                                                                                @endif
                                                                                            </select>
                                                                                        @else
                                                                                            @if($s->s_status == 'Complete' && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                                    <option >Actions</option>
                                                                                                    @if(in_array('status Delivered',$permissions))
                                                                                                        <option >Delivered</option>
                                                                                                    @endif
                                                                                                    @if(in_array('show',$permissions))
                                                                                                        <option >Sale Details</option>
                                                                                                    @endif
                                                                                                    @if(in_array('report',$permissions))
                                                                                                        <option >Sale Report</option>
                                                                                                    @endif
                                                                                                    @if(in_array('Add Payment',$permissions))
                                                                                                        <option>Add Payment</option>
                                                                                                    @endif
                                                                                                    @if(in_array('View Payment',$permissions))
                                                                                                        <option >View Payment</option>
                                                                                                    @endif
                                                                                                    @if(in_array("View Gdn's",$permissions))
                                                                                                        <option >View GDN History</option>
                                                                                                    @endif
                                                                                                </select>
                                                                                            @else
                                                                                                @if($s->s_status == 'Complete' && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                                        <option >Actions</option>
                                                                                                        @if(in_array('status Delivered',$permissions))
                                                                                                            <option >Delivered</option>
                                                                                                        @endif
                                                                                                        @if(in_array('show',$permissions))
                                                                                                            <option >Sale Details</option>
                                                                                                        @endif
                                                                                                        @if(in_array('report',$permissions))
                                                                                                            <option >Sale Report</option>
                                                                                                        @endif
                                                                                                        @if(in_array('Add Payment',$permissions))
                                                                                                            <option>Add Payment</option>
                                                                                                        @endif
                                                                                                        @if(in_array('View Payment',$permissions))
                                                                                                            <option >View Payment</option>
                                                                                                        @endif
                                                                                                        @if(in_array("View Gdn's",$permissions))
                                                                                                            <option >View GDN History</option>
                                                                                                        @endif
                                                                                                    </select>
                                                                                                @else
                                                                                                    @if(($s->s_status == 'Approved') && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                                                            <option >Actions</option>
                                                                                                            @if(in_array('Ready To Deliver',$permissions))
                                                                                                                <option >Ready To Deliver</option>
                                                                                                            @endif
                                                                                                            @if(in_array('show',$permissions))
                                                                                                                <option >Sale Details</option>
                                                                                                            @endif
                                                                                                            @if(in_array('report',$permissions))
                                                                                                                <option >Sale Report</option>
                                                                                                            @endif

                                                                                                            @if(in_array('View Payment',$permissions))
                                                                                                                <option >View Payment</option>
                                                                                                            @endif
                                                                                                        </select>
                                                                                                    @else
                                                                                                        @if(($s->s_status == 'Partial') && ($s->p_status == 'Partial' || $s->p_status == 'Pending'))
                                                                                                            <select class="form-control action" id="{{$s->id}}" >
                                                                                                                <option >Actions</option>
                                                                                                                @if(in_array('Ready To Deliver',$permissions))
                                                                                                                    <option >Ready To Deliver</option>
                                                                                                                @endif
                                                                                                                @if(in_array('show',$permissions))
                                                                                                                    <option >Sale Details</option>
                                                                                                                @endif
                                                                                                                @if ($s->return_status !='Requested')
                                                                                                                    @if(in_array('Return',$permissions))
                                                                                                                        <option >Sale Return</option>
                                                                                                                    @endif
                                                                                                                @endif
                                                                                                                @if(in_array('report',$permissions))
                                                                                                                    <option >Sale Report</option>
                                                                                                                @endif
                                                                                                                @if(in_array('Add Payment',$permissions))
                                                                                                                    <option>Add Payment</option>
                                                                                                                @endif
                                                                                                                @if(in_array('View Payment',$permissions))
                                                                                                                    <option >View Payment</option>
                                                                                                                @endif
                                                                                                                @if(in_array("View Gdn's",$permissions))
                                                                                                                    <option >View GDN History</option>
                                                                                                                @endif
                                                                                                            </select>
                                                                                                        @else
                                                                                                            @if(($s->s_status == 'Approved' || $s->s_status == 'Partial') && $s->p_status == 'Paid')
                                                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                                                    <option >Actions</option>
                                                                                                                    @if(in_array('Ready To Deliver',$permissions))
                                                                                                                        <option >Ready To Deliver</option>
                                                                                                                    @endif
                                                                                                                    @if(in_array('show',$permissions))
                                                                                                                        <option >Sale Details</option>
                                                                                                                    @endif
                                                                                                                    @if ($s->return_status !='Requested')
                                                                                                                        @if(in_array('Return',$permissions))
                                                                                                                            <option >Sale Return</option>
                                                                                                                        @endif
                                                                                                                    @endif
                                                                                                                    @if(in_array('report',$permissions))
                                                                                                                        <option >Sale Report</option>
                                                                                                                    @endif
                                                                                                                    @if(in_array('View Payment',$permissions))
                                                                                                                        <option >View Payment</option>
                                                                                                                    @endif
                                                                                                                    @if(in_array("View Gdn's",$permissions))
                                                                                                                        <option >View GDN History</option>
                                                                                                                    @endif
                                                                                                                </select>
                                                                                                            @else
                                                                                                                @if ($s->s_status == 'Return' && $s->p_status == 'Return')
                                                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                                                        <option >Actions</option>
                                                                                                                        @if(in_array('show',$permissions))
                                                                                                                            <option >Sale Details</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('report',$permissions))
                                                                                                                            <option >Sale Report</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('View Payment',$permissions))
                                                                                                                            <option >View Payment</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array("View Gdn's",$permissions))
                                                                                                                            <option >View GDN History</option>
                                                                                                                        @endif
                                                                                                                    </select>
                                                                                                                @else
                                                                                                                    <select class="form-control action" id="{{$s->id}}" >
                                                                                                                        <option >Actions</option>
                                                                                                                        @if(in_array('edit',$permissions))
                                                                                                                            <option >Edit Sale</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('status',$permissions))
                                                                                                                            <option >Approved</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('show',$permissions))
                                                                                                                            <option >Sale Details</option>
                                                                                                                        @endif
                                                                                                                        @if(in_array('report',$permissions))
                                                                                                                            <option >Sale Report</option>
                                                                                                                        @endif
                                                                                                                    </select>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endif
                                                                                            @endif
                                                                                        @endif
                                                                                    @endif
                                                                            </td>
                                                                        </tr>
                                                                        @endif
                                                                    @endforeach --}}
                                                            {{-- @endif --}}

                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>AMOUNT</td>
                                                            <td>PAID AMOUNT</td>
                                                            <td>BALANCE</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8" align="right">TOTAL</td>
                                                            <td>{{$totalAmount}}</td>
                                                            <td>{{$totalPaid}}</td>
                                                            <td>{{$totalBalance}}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        @if ($count == 0)
                                                            <tr>
                                                                <td colspan="8" align="right">AVERAGE</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>

                                                        @else
                                                            <tr>
                                                                <td colspan="8" align="right">AVERAGE</td>
                                                                <td>{{$totalAmount/$count}}</td>
                                                                <td>{{$totalPaid/$count}}</td>
                                                                <td>{{$totalBalance/$count}}</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        @endif
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        @include('modals.payment')
        @include('modals.transaction')
        @include('modals.delivery')
        @include('modals.salereturn')
        @include('modals.addDoc')
        @include('modals.viewDoc')
        @include('modals.allSaleInvoices')

        <div id="GDN" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">View GDN History</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Number</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="SaleNo" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="example7" class="table table-striped table-bordered" style="width:1400px">
                                    <thead>
                                        <tr>
                                            <th style="width:100px">S.No</th>
                                            <th style="width:120px">Delivery Date</th>
                                            <th style="width:100px">GDN No</th>
                                            <th style="width:100px">Delivered Product</th>
                                            <th style="width:100px">Price</th>
                                            <th style="width:100px">Quantity</th>
                                            <th style="width:100px">Sub Total</th>
                                            <th style="width:200px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Sales</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label>Invoice Format</label>
                                    <input type="text" id="Iformat" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label>Invoice Type</label>
                                    <input type="text" id="Itype" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="sale_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Invoice No</label>
                                    <input class="form-control" type="text" id="Ino" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reference No</label>
                                    <input class="form-control" type="text" id="ref_no" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="c_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Biller</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="b_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Person</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="s_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Advance payment</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="advance" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row docx" hidden>
                            <div class="col-sm-5">
                                <input type="hidden" name="doc1" id="doc1">
                                <label style="margin-top: 10px" ><b> Document1 </b></label>
                            </div>
                            <div class="col-sm-1">
                                <a href="" class='btn green docs' style="margin-top: 10px" ><i class='fa fa-download'></i></a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-1">
                                <a href="" id="edithref">
                                    <button type="button"  id="edit" class="btn btn-sm  btn-info" >Edit</button>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered" style="overflow-x:auto;">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @section('custom-script')
    <script>
        CKEDITOR.replace( 'editor2' );
   </script>
    @toastr_js
    @toastr_render
<script type="text/javascript">

    $(document).ready(function () {
        var table = $('#example').DataTable({
            scrollX: true,
            order: [[ 0, "desc" ]]
        });
        $('input:radio[name="optradio"]').change(function(){
            if ($(this).is(':checked')) {
                $('#search').prop('disabled',false);
                var val = $(this).val();
                if(val == 'Year')
                {
                    $('#year').prop('disabled',false);
                    $('#year').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'Month')
                {
                    $('#month').prop('disabled',false);
                    $('#month').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'Date')
                {
                    $('#from').prop('disabled',false);
                    $('#from').attr('required',true);
                    $('#to').prop('disabled',false);
                    $('#to').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'Status')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',true);
                    $('#status').attr('disabled',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

                if(val == 'PStatus')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'customer')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',true);
                    $('#sp_id').prop('disabled',true);
                    $('#sp_id').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'saleperson')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').attr('required',false);
                    $('#c_id').prop('disabled',true);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',true);
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'last24Hours')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'lastweek')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'last15Days')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'lastMonth')
                {
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
            }
        });


        $(document).on('change','#paid_by',function(){
            var p=$(this).val();
            if(p == 'Cheque')
            {
                $('#cheque').show();
                $("#cheque_no").prop('required',true);
                $('#gift').hide();
                $("#gift_no").prop('required',false);
                $('#credit').hide();
                $("#cc_no").prop('required',false);
                $("#cc_holder").prop('required',false);
            }
            if(p == 'Gift')
            {
                $('#gift').show();
                $("#gift_no").prop('required',true);
                $('#cheque').hide();
                $("#cheque_no").prop('required',false);
                $('#credit').hide();
                $("#cc_no").prop('required',false);
                $("#cc_holder").prop('required',false);
            }
            if(p == 'Cash')
            {
                $('#gift').hide();
                $("#gift_no").prop('required',false);
                $('#cheque').hide();
                $("#cheque_no").prop('required',false);
                $('#credit').hide();
                $("#cc_no").prop('required',false);
                $("#cc_holder").prop('required',false);
            }
            if(p == 'Credit')
            {
                $('#gift').hide();
                $("#gift_no").prop('required',false);
                $('#cheque').hide();
                $("#cheque_no").prop('required',false);
                $('#credit').show();
                $("#cc_no").prop('required',true);
                $("#cc_holder").prop('required',true);
            }
        });
        $(document).on('change','.action',function(){
            var val=$(this).val();
            if(val == 'Pending' || val == 'Approve' || val == 'Delivered' )//
            {
                var id = $(this).attr('id');
                var status = val;
                axios
                .post('{{route("sales.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                });
            }
            if(val == 'Ready To Deliver')//
            {
                var id=$(this).attr("id");
                $('#w_id1').val('');
                $('#delivery').find("select").val('').end();
                $('#p_total1').val('');
                $("#example3").empty();
                $.ajax({
                    url:"{{url('')}}/sales/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        if (data[0].Iformat == 'GST') {

                            $('#example3').append(` <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Code - Name</th>
                                        <th>Image</th>
                                        <th>Sale Quantity</th>
                                        <th>Delivered Quantity</th>
                                        <th>Delivery Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>`);
                        } else {
                            $('#example3').append(` <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Code - Name</th>
                                        <th>Image</th>
                                        <th>Sale Quantity</th>
                                        <th>Delivered Quantity</th>
                                        <th>Delivery Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>`);
                        }
                        var a =2;
                        var total = 0;
                        for (let i = 0; i < data[1].length; i++) {
                             max= data[1][i].quantity - data[1][i].delivered_quantity;

                            if(data[1][i].quantity == data[1][i].delivered_quantity)
                            {

                            }
                            else
                            {
                                total = +total + +(data[1][i].sub_total / data[1][i].quantity);
                                if(data[1][i].discounted_amount == null)
                                {
                                    discounted_amount = 0;
                                }
                                else
                                {
                                    discounted_amount = data[1][i].discounted_amount;
                                }
                                if(data[0].Iformat == 'GST'){

                                    if(data[1][i].type == 1)
                                    {
                                        var img = '';
                                        var APP_URL = {!! json_encode(url('/')) !!}
                                        if(data[1][i].variant.product.image == null || data[1][i].variant.product.image == "")
                                        {
                                            img = APP_URL+"/uploads/noimg.png";
                                        }
                                        else
                                        {
                                            img = APP_URL+"/uploads/"+data[1][i].variant.product.image;
                                        }
                                        $("#example3").append("<tr><td> <input name='subtotal[]' type='hidden' value='"+data[1][i].sub_total / data[1][i].quantity+"' class='subtotal"+a+"'> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].variant.id+"'></td><td>"+data[1][i].variant.name+"</td><td><img class='img-fluid' width='120px' height='80px' src='"+img+"' alt=''></td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'><input type='hidden' name='type[]' value='"+data[1][i].type+"'><input type='hidden' name='cost[]' value='"+data[1][i].cost+"'></td><td><input type='text' style='background-color: transparent;border: 0px solid;' readonly value='"+data[1][i].delivered_quantity+"' class='dq"+a+"'></td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='delivered_quantity[]'></td></tr>");
                                    }
                                    else
                                    {
                                        var APP_URL = {!! json_encode(url('/')) !!}
                                        var img = '';
                                        if(data[1][i].products.image == null || data[1][i].products.image == "")
                                        {
                                            img = APP_URL+"/uploads/noimg.png";
                                        }
                                        else
                                        {
                                            img = APP_URL+"/uploads/"+data[1][i].products.image;
                                        }
                                        $("#example3").append("<tr><td><input type='hidden' name='subtotal[]' value='"+data[1][i].sub_total / data[1][i].quantity+"' class='subtotal"+a+"'>  <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+"</td><td><img class='img-fluid' width='120px' height='80px' src='"+img+"' alt=''></td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'><input type='hidden' name='type[]' value='"+data[1][i].type+"'><input type='hidden' name='cost[]' value='"+data[1][i].cost+"'></td><td><input type='text' style='background-color: transparent;border: 0px solid;' readonly value='"+data[1][i].delivered_quantity+"' class='dq"+a+"'> </td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='delivered_quantity[]'></td></tr>");
                                    }
                                }
                                else
                                {
                                    if(data[1][i].type == 1)
                                    {
                                        var img = '';
                                        var APP_URL = {!! json_encode(url('/')) !!}
                                        if(data[1][i].variant.product.image == null || data[1][i].variant.product.image == "")
                                        {
                                            img = APP_URL+"/uploads/noimg.png";
                                        }
                                        else
                                        {
                                            img = APP_URL+"/uploads/"+data[1][i].variant.products.image;
                                        }
                                        $("#example3").append("<tr><td><input type='hidden' name='subtotal[]' value='"+data[1][i].sub_total / data[1][i].quantity+"' class='subtotal"+a+"'><input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].variant.id+"'></td><td>"+data[1][i].variant.name+"</td><td><img class='img-fluid' width='120px' height='80px' src='"+img+"' alt=''></td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'><input type='hidden' name='type[]' value='"+data[1][i].type+"'><input type='hidden' name='cost[]' value='"+data[1][i].cost+"'> </td><td><input type='text' style='background-color: transparent;border: 0px solid;' readonly value='"+data[1][i].delivered_quantity+"' class='dq"+a+"'> </td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='delivered_quantity[]'></td></tr>");
                                    }
                                    else
                                    {
                                        var img = '';
                                        var APP_URL = {!! json_encode(url('/')) !!}
                                        if(data[1][i].products.image == null || data[1][i].products.image == "")
                                        {
                                            img = APP_URL+"/uploads/noimg.png";
                                        }
                                        else
                                        {
                                            img = APP_URL+"/uploads/"+data[1][i].products.image;
                                        }
                                        $("#example3").append("<tr><td> <input name='subtotal[]' type='hidden' value='"+data[1][i].sub_total / data[1][i].quantity+"' class='subtotal"+a+"'> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+"</td><td><img class='img-fluid' width='120px' height='80px' src='"+img+"' alt=''></td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'><input type='hidden' name='type[]' value='"+data[1][i].type+"'><input type='hidden' name='cost[]' value='"+data[1][i].cost+"'></td><td><input type='text' style='background-color: transparent;border: 0px solid;' readonly value='"+data[1][i].delivered_quantity+"' class='dq"+a+"'> </td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='delivered_quantity[]'></td></tr>");
                                    }
                                }
                            }
                            a++;
                        }
                        $('#w_id1').val(data[0].w_id);
                        $('#s_id1').val(id);
                        $('#IF').val(data[0].Iformat);
                        $('#IT').val(data[0].Itype);
                        $('#example3').append('</tbody>');

                        $('#delivery').modal("show");

                        $('#p_total1').attr('max',total.toFixed(2));
                        $('#p_total1').val(total.toFixed(2));
                        $('.action').val('Actions');
                        $('#example3').DataTable();

                    }
                });
            }
            if(val == 'Edit Sale')//
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/sales/'+id+'/edit';
                // $('.action').val('Actions');
            }
            if(val == 'Edit Invoice')//
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/sales/editInvoice/'+id;
                // $('.action').val('Actions');
            }
            if(val == 'Sale Report')//
            {
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/sales/pdf/"+id,
                    method:"GET",
                    data:
                    {
                        id:id,
                    },
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        window.location.href= '{{url('')}}/sales/pdf/'+id;
                        // location.reload();
                        $('.action').val('Actions');
                    }
                });
            }
            if(val == 'Add Payment')
            {
                $('#p_total').val('');
                $('#actual').val('');
                $('#p_s_id').val('');
                $('#p_ref_no').val('');

                $('#payment').find("textarea,select").val('').end();
                $('#gift').hide();
                $('#cheque').hide();
                $('#credit').hide();
                var amount = 0;
                var id=$(this).attr('id');
                $.ajax({
                    url:"{{url('')}}/sales/"+id,
                    method:"GET",
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){

                        for (let i = 0; i < data[1].length; i++) {
                            amount += (data[1][i].sub_total / data[1][i].quantity) * (data[1][i].delivered_quantity - data[1][i].returnQty) - data[2] + data[3];
                        }

                        $('#p_s_id').val(id);
                        $('#p_type').val('Sales');
                        $('#t_type').val('Received');
                        $('#p_total').attr('max',amount);
                        $('#actual').val(amount.toFixed(2));
                        $('#payment').modal("show");
                        $('.action').val('Actions');
                    }
                });
            }
            if(val == 'Sale Details')//
            {

                var id=$(this).attr("id");
                $("#example1").empty();
                $.ajax({
                    url:"{{url('')}}/sales/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        $('#sale_date').val(data[0].sale_date);
                        $('#Iformat').val(data[0].Iformat);
                        $('#Itype').val(data[0].Itype);
                        $('#Ino').val(data[0].Ino);
                        $('#ref_no').val(data[0].ref_no);
                        $('#w_name').val(data[0].warehouse.w_name);
                        $('#c_name').val(data[0].customer.name);
                        $('#b_name').val(data[0].biller.name);
                        $('#s_name').val(data[0].saleperson.name);
                        $('#advance').val(data[0].advance);
                        if(data[0].doc != null)
                        {
                            $('.docx').show();
                            $('#doc1').val(data[0].doc);
                            $('.docs').attr('href','{{url('')}}/sales/document/'+data[0].id);
                        }
                        if(data[0].Iformat == 'GST')
                        {
                            $('#example1').append(`<thead><tr>
                                                <th >S.No</th>
                                                <th >Code - Name - Brand</th>
                                                @if (in_array('price',$permissions))
                                                    <th width="7%">Price</th>
                                                @endif
                                                <th >Quantity</th>
                                                <th >Tax</th>
                                                <th >Discount</th>
                                                @if (in_array('subtotal',$permissions))
                                                    <th >Sub total</th>
                                                @endif
                                                <th >Delivered Quantity</th>
                                                <th >Return Quantity</th>
                                            </tr>
                                        </thead> <tbody>`);
                        }
                        else
                        {
                            $('#example1').append(`<thead><tr>
                                                <th >S.No</th>
                                                <th >Code - Name - Brand</th>
                                                @if (in_array('price',$permissions))
                                                    <th width="7%">Price</th>
                                                @endif
                                                <th >Quantity</th>
                                                <th >Discount</th>
                                                @if (in_array('subtotal',$permissions))
                                                    <th >Sub total</th>
                                                @endif
                                                <th >Delivered Quantity</th>
                                                <th >Return Quantity</th>
                                            </tr>
                                        </thead> <tbody>`);
                        }
                        if(data[0].s_status == "Pending")
                        {
                            $('#edit').attr('disabled',false);
                        }
                        else
                        {
                            $('#edit').attr('disabled',true);
                        }
                        $('#edithref').attr('href','{{url('')}}/sales/'+id+'/edit');
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].returnQty == null)
                            {
                                returnqty=0;
                            }
                            else
                            {
                                returnqty = data[1][i].returnQty;
                            }
                            if (data[0].Iformat == 'GST') {

                                if(data[1][i].type == 1)
                                {

                                    $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].variant.name+ " - "+data[1][i].variant.product.brands.b_name+"</td> @if (in_array('price',$permissions))<td>"+data[1][i].price+"</td> @endif <td>"+data[1][i].quantity+"</td><td>"+data[1][i].taxA+"</td><td>"+data[1][i].discounted_amount+"</td> @if (in_array('subtotal',$permissions))<td>"+data[1][i].sub_total+"</td> @endif <td>"+data[1][i].delivered_quantity+"</td><td>"+returnqty+"</td>  </tr>");
                                }
                                else
                                {
                                    $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+" - "+data[1][i].products.brands.b_name+"</td> @if (in_array('price',$permissions))<td>"+data[1][i].price+"</td> @endif <td>"+data[1][i].quantity+"</td><td>"+data[1][i].taxA+"</td><td>"+data[1][i].discounted_amount+"</td> @if (in_array('subtotal',$permissions)) <td>"+data[1][i].sub_total+"</td> @endif<td>"+data[1][i].delivered_quantity+"</td><td>"+returnqty+"</td>   </tr>");
                                }
                            } else {
                                if(data[1][i].type == 1)
                                {

                                    $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].variant.name+ " - "+data[1][i].variant.product.brands.b_name+"</td> @if (in_array('price',$permissions))<td>"+data[1][i].price+"</td> @endif <td>"+data[1][i].quantity+"</td><td>"+data[1][i].discounted_amount+"</td> @if (in_array('subtotal',$permissions))<td>"+data[1][i].sub_total+"</td> @endif <td>"+data[1][i].delivered_quantity+"</td><td>"+returnqty+"</td>  </tr>");
                                }
                                else
                                {
                                    $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+" - "+data[1][i].products.brands.b_name+"</td> @if (in_array('price',$permissions))<td>"+data[1][i].price+"</td> @endif <td>"+data[1][i].quantity+"</td><td>"+data[1][i].discounted_amount+"</td> @if (in_array('subtotal',$permissions)) <td>"+data[1][i].sub_total+"</td> @endif<td>"+data[1][i].delivered_quantity+"</td><td>"+returnqty+"</td>   </tr>");
                                }
                            }
                        }
                        $('#example1').append(`</tbody>`);
                        $('#myModal').modal("show");
                        $('.action').val('Actions');
                        $('#example1').DataTable();

                    }
                });
            }
            if(val == 'View Payment')//
            {
                var id=$(this).attr("id");
                $("#example2 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/transaction/view",
                    method:"GET",
                    data:
                    {
                        p_s_id:id,
                        p_type:'Sales',
                    },
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        for (let i = 0; i < data.length; i++) {
                            date = moment(data[i].created_at).format('MM/DD/YYYY');
                            $("#example2").append("<tr><td>"+data[i].id+"</td><td>"+date+"</td><td>"+data[i].paid_by+"</td><<td>"+data[i].total+"</td> @if(in_array('Download Invoice',$permissions)) <td> <button type='button' class='btn green invoice' id="+data[i].id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button' disabled class='btn green invoice' id="+data[i].id+"><i class='fa fa-download'></i></button></td> @endif</tr>");
                        }
                        $('#transaction').modal("show");
                        $('.action').val('Actions');
                        $('#example2').DataTable();

                    }
                });
            }
            if(val == "View GDN History")//
            {
                var id=$(this).attr("id");
                $("#example7 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/sales/gdn/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        // console.log(data);
                        var a = 1;
                        var total = 0;
                        // console.log(data[0].stock.length);
                        $('#SaleNo').val(data[0].s_id);
                        for (let i = 0; i < data.length; i++) {
                            if(data[i].stock == null)
                            {

                            }
                            else
                            {
                                for (let j = 0; j < data[i].stock.length; j++) {
                                    total = data[i].price * data[i].stock[j].quantity;
                                    console.log(total);
                                    if(data[i].type == 1)
                                    {
                                        $("#example7").append("<tr><td>"+a+"</td><td>"+data[i].stock[j].stock_date+"</td><td>"+data[i].stock[j].gdn_no+"</td><td>"+data[i].variant.name+"</td><td>"+data[i].stock[j].price+"</td><td>"+data[i].stock[j].quantity+"</td><td>"+total+"</td> @if(in_array('Download Gdr',$permissions)) <td> <button type='button' class='btn green gdn' data-gdn="+data[i].stock[j].gdn_no+" id="+data[i].s_id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button' disabled class='btn green gdn' data-gdn="+data[i].stock[j].gdn_no+" id="+data[i].s_id+"><i class='fa fa-download'></i></button></td> @endif </tr>");
                                    }
                                    else
                                    {

                                        $("#example7").append("<tr><td>"+a+"</td><td>"+data[i].stock[j].stock_date+"</td><td>"+data[i].stock[j].gdn_no+"</td><td>"+data[i].products.pro_name+"</td><td>"+data[i].stock[j].price+"</td><td>"+data[i].stock[j].quantity+"</td><td>"+total+"</td> @if(in_array('Download Gdr',$permissions)) <td> <button type='button' class='btn green gdn' data-gdn="+data[i].stock[j].gdn_no+" id="+data[i].s_id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button' disabled class='btn green gdn' data-gdn="+data[i].stock[j].gdn_no+" id="+data[i].s_id+"><i class='fa fa-download'></i></button></td> @endif </tr>");
                                    }
                                    a= +a + 1;
                                    // total = 0;
                                }
                            }
                        }
                        $('#GDN').modal("show");
                        $('.action').val('Actions');
                        $('#example7').DataTable();
                    }
                });
            }
            if(val == 'Sale Return')
            {
                var id=$(this).attr("id");
                $('#w_id2').val('');
                $('#return').find("select").val('').end();
                $("#example8 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/sales/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        var a =2;
                        for (let i = 0; i < data[1].length; i++) {
                             max= data[1][i].delivered_quantity;

                            if(data[1][i].quantity == data[1][i].returnQty)
                            {
                            }
                            else
                            {


                                if(data[1][i].type == 1)
                                {
                                    $("#example8").append("<tr><td> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].variant.id+"'></td><td>"+data[1][i].variant.name+"</td><input type='hidden' name='type[]' value='"+data[1][i].type+"'><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td><input type='text' readonly class='form-control' value='"+data[1][i].delivered_quantity+"' id='dq"+a+"'></td><td><h5>Damage Qty</h5> <input type='number' max='"+max+"' value='0' min='0' required class='form-control dqty quantitys"+a+"' name='Dquantity[]' id='dqty"+a+"'> <br><h5>Usable Qty</h5><input type='number' max='"+max+"' value='0' min='0' required class='form-control nqty quantitys"+a+"' name='Nquantity[]' id='nqty"+a+"'></td></tr>");
                                }
                                else
                                {
                                    $("#example8").append("<tr><td> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].products.pro_name+" - "+data[1][i].products.pro_code+"</td><input type='hidden' name='type[]' value='"+data[1][i].type+"'><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td><input type='text' readonly class='form-control' value='"+data[1][i].delivered_quantity+"' id='dq"+a+"'></td><td><h5>Damage Qty</h5> <input type='number' max='"+max+"' value='0' min='0' required class='form-control dqty quantitys"+a+"' name='Dquantity[]' id='dqty"+a+"'> <br><h5>Usable Qty</h5><input type='number' max='"+max+"' value='0' min='0' required class='form-control nqty quantitys"+a+"' name='Nquantity[]' id='nqty"+a+"'></td></tr>");
                                }
                            }
                            a++;
                        }
                        // // console.log(a);
                        $('#w_id2').val(data[0].w_id);
                        $('#s_id2').val(id);

                        // console.log(amount);
                        $('#return').modal("show");
                        $('.action').val('Actions');
                        $('#example8').DataTable();

                    }
                });
            }
            if(val == 'Attach Documents')
            {
                var id = $(this).attr('id');
                $('#s_id').val(id);
                $('#addDoc').modal("show");
            }
            if(val == 'View Documents')
            {
                var id = $(this).attr('id');
                $("#documents tbody").empty();
                $.ajax({
                    url:"{{url('')}}/viewDoc/"+id,
                    method:"GET",
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        var a =1;
                        for (let i = 0; i < data.length; i++) {
                            $('#documents').append(`<tr><td>`+a+`</td><td>Document`+a+`</td><td><a class='download' href=`+document.location.origin+`/documents/`+data[i].document+` download><button type='button' class='btn green docs' data-grn=`+data[i].id+` id=`+data[i].id+`><i class='fa fa-download'></i></button></a></td></tr>`);
                            a= +a + 1;
                        }
                        $('#viewDoc').modal("show");
                        $('.action').val('Actions');
                        $('#documents').DataTable();
                    }
                });
            }
            if(val == 'View All Sale Reports')
            {
                var id = $(this).attr('id');
                $("#allReports tbody").empty();
                $.ajax({
                    url:"{{url('')}}/sales/allInvoices/"+id,
                    method:"GET",
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        var a =1;
                        var num = 1;
                        $('#allReports').append(`<tr><td>`+a+`</td><td>Original</td><td><a class='allIn' href={{url('')}}/sales/pdf/`+id+` download><button type='button' class='btn green docs' data-grn=`+data.id+` id=`+data.id+`><i class='fa fa-download'></i></button></a></td></tr>`);

                        for (let i = 0; i < data.length; i++) {
                            a= +a + 1;
                            if(data[i].return != null)
                            {
                                if(data[i].return.status == 'Approved')
                                {
                                    $('#allReports').append(`<tr><td>`+a+`</td><td>Copy `+num+`</td><td><a class='allIn' href={{url('')}}/sales/allInvoices/pdf/`+id+` download><button type='button' class='btn green docs' data-grn=`+data[i].id+` id=`+data[i].id+`><i class='fa fa-download'></i></button></a></td></tr>`);
                                    num= +num + 1;
                                }
                            }
                        }
                        $('#allInvoices').modal("show");
                        $('.action').val('Actions');
                        $('#allReports').DataTable();
                    }
                });
            }
        });
    });
    $(document).on('click','.invoice',function(){
        var id=$(this).attr("id");
        $.ajax({
            url:"{{url('')}}/sales/invoice/"+id,
            method:"GET",
            data:
            {
                id:id,
            },
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                window.location.href= '{{url('')}}/sales/invoice/'+id;
                // location.reload();
                $('.action').val('Actions');
            }
        });
    });


    $(document).on('change','.rcv',function(){
        var total = 0;
        var total1 = 0;
        var rowCount1 = $('#example3 tr').length;
        for (let i = 2; i <= rowCount1; i++) {
                var total1 = $('.subtotal'+i).val();
                total = +total +  +(total1 * $('.quantity'+i).val());
        }
        $('#p_total1').val(total.toFixed(2));
        $('#purchase').val(total.toFixed(2));
        $('#p_total1').attr('max',total.toFixed(2));
    });

    $(document).on('click','.gdn',function(){
        var id=$(this).attr("id");
        var gdn=$(this).attr("data-gdn");
        // console.log(id,grn);
        // debugger
        $.ajax({
            url:"{{url('')}}/sales/gdr/",
            method:"GET",
            data:
            {
                gdn:gdn,
                id:id
            },
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                window.location.href= '{{url('')}}/sales/gdr1/'+gdn+'-'+id;
                // location.reload();
                $('.action').val('Actions');
            }
        });
    });

    $(document).on('click','#delivered',function(){
        $('#delivery').modal("hide");
        $('.action').val('Actions');
    });

    $(document).on('click','.download',function(){
        $('#viewDoc').modal("hide");
        $('.action').val('Actions');
    });

    $(document).on('click','.allIn',function(){
        $('#allInvoices').modal("hide");
        $('.action').val('Actions');
    });

    $(document).on('change','.dqty',function(){
        var value = $(this).val();
        var id = $(this).attr('id');
        id = id.substring(4);
        var nvalue = $('#nqty'+id).val();
        var total = +nvalue + +value;
        var dq = $('#dq'+id).val();
        if(total > dq)
        {
            alert('You can not return quantity more than delivered');
            $(this).val(value - 1);
        }
    });

    $(document).on('change','.nqty',function(){
        var value = $(this).val();
        var id = $(this).attr('id');
        id = id.substring(4);
        var dvalue = $('#dqty'+id).val();
        var total = +dvalue + +value;
        var dq = $('#dq'+id).val();
        if(total > dq)
        {
            alert('You can not return quantity more than delivered');
            $(this).val(value - 1);
        }
    });
</script>

    @endsection
@endsection
