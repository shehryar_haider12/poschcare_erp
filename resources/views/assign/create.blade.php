@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/assigned">Assigned Counters</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit Assigned' : 'Assign'}}  Counter</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-gg font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit Assigned' : 'Assign'}} Counter</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="assignForm" action="{{$isEdit ? route('assigned.update',$assign->id) :  route('assigned.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Counters*</label>
                                        <select style="overflow-y: scroll;" id="c_id" size="1" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($counter as $s)
                                                <option {{$s->id == $assign->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->counterNo}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($counter as $s)
                                                <option value="{{$s->id}}">{{$s->counterNo}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Users*</label>
                                        <select style="overflow-y: scroll;" id="u_id" size="1" class="form-control selectpicker" multiple data-live-search="true" name="u_id[]" >
                                            <option value="" disabled >Select...</option>
                                            @if ($isEdit)
                                                @foreach ($user as $s)
                                                    @if ($s->role->name == 'Cash Counter' || $s->role->name == 'Admin')
                                                        <option {{$s->id == $assign->u_id ? 'selected' : null}} value="{{$s->id}}">{{$s->name}}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach ($user as $s)
                                                    @if ($s->role->name == 'Cash Counter' || $s->role->name == 'Admin')
                                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.category')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#assignForm').validate({
        rules: {
            c_id: {
                required: true,
            },
            u_id: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>


@endsection
