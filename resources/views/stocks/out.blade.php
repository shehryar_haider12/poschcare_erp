@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {

        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/stockout">Stock Out History</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-tasks font-white"></i>Stock Out History

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/stockout/stockOutSearch" method="POST" id="advanceSearch">
                                                @csrf

                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Product Name</label>
                                                                <select class="form-control " data-live-search="true" name="p_id" id="p_name" disabled>
                                                                    <option value=""  selected>Select...</option>
                                                                    @foreach ($product as $s)
                                                                        @if ($s->vstatus == 0)
                                                                            <option value="{{$s->id.'-'.'0'}}"> {{$s->pro_name}} - {{$s->brands->b_name}}</option>
                                                                        @else
                                                                            @foreach ($s->variants as $v)
                                                                                @php
                                                                                    $name = explode(' - ',$v->name);
                                                                                @endphp
                                                                                <option value="{{$v->id.'-'.'1'}}">{{$name[1]}} - {{$s->brands->b_name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From</label>
                                                                <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To</label>
                                                                <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="" style="visibility: hidden">.</label>
                                                                <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                <i class="fa fa-search pr-1"></i> Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Product Code</th>
                                                            <th>Product Name</th>
                                                            <th>Warehouse</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Price</th>
                                                            <th>Quantity</th>
                                                            <th>Total</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $totalqty = 0;
                                                            $totalprice = 0;
                                                            $total = 0;
                                                        @endphp
                                                          @foreach ($stock as $s)
                                                          <tr>
                                                              <td>
                                                                  {{$a}}
                                                              </td>
                                                              @if ($s->type == 1)
                                                                    @php
                                                                        $name = explode(' - ',$s->variant->name);
                                                                    @endphp
                                                                  <td>{{$name[0]}}</td>
                                                                  <td>{{$name[1]}}</td>

                                                                  <td>
                                                                      {{$s->warehouse->w_name}}
                                                                  </td>
                                                                  <td>
                                                                      {{$s->variant->product->brands->b_name}}
                                                                  </td>
                                                                  <td>
                                                                      {{$s->variant->product->category->cat_name}}
                                                                  </td>
                                                              @else
                                                                    <td>
                                                                        {{$s->products->pro_code}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->category->cat_name}}
                                                                    </td>
                                                              @endif
                                                              <td>
                                                                  {{$s->price ?? 1}}
                                                              </td>
                                                              <td>
                                                                  {{$s->quantity}}
                                                              </td>
                                                              <td>
                                                                  {{($s->price ?? 1) * $s->quantity}}
                                                              </td>
                                                              <td>
                                                                  {{$s->stock_date}}
                                                              </td>
                                                          </tr>
                                                          @php
                                                              $a++;
                                                              $totalqty += $s->quantity;
                                                              $totalprice += $s->price ?? 1;
                                                              $total += ($s->price ?? 1) * $s->quantity;
                                                          @endphp
                                                      @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$totalprice}}</td>
                                                            <td>{{$totalqty}}</td>
                                                            <td>{{$total}}</td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                scrollX: true,
            });
            $('input:radio[name="optradio"]').change(function(){
                if ($(this).is(':checked')) {
                    $('#search').prop('disabled',false);
                    var val = $(this).val();
                    if(val == 'Year')
                    {
                        $('#year').prop('disabled',false);
                        $('#year').attr('required',true);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',true);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Month')
                    {
                        $('#month').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Date')
                    {
                        $('#from').prop('disabled',false);
                        $('#from').attr('required',true);
                        $('#to').prop('disabled',false);
                        $('#to').attr('required',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#month').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'last24Hours')
                    {
                        $('#year').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',true);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'lastweek')
                    {
                        $('#year').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',true);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'last15Days')
                    {
                        $('#year').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',true);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'lastMonth')
                    {
                        $('#year').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',true);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#c_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });

        });

    </script>
    @endsection
@endsection
