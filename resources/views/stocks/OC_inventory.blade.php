@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {

        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="/stocks">Stock</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">

                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-tasks font-white"></i> &nbsp; Opening & Closing Inventory
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="float: right"  href="{{route('stocks.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/stocks/OC_inventory" method="POST" id="advanceSearch">
                                                @csrf

                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Product Name</label>
                                                                <select class="form-control " data-live-search="true" name="p_id" id="p_name" disabled>
                                                                    <option value="" disabled  selected>Select...</option>
                                                                    @foreach ($product as $s)
                                                                        @if ($s->vstatus == 0)
                                                                            <option value="{{$s->id.'-'.'0'}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @else
                                                                            @foreach ($s->variants as $v)
                                                                                <option value="{{$v->id.'-'.'1'}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Product Type</label>
                                                                <select name="ptype" id="ptype" disabled class="form-control">
                                                                <option selected="" disabled value="">No Filter</option>
                                                                <option>Raw</option>
                                                                <option>Material</option>
                                                                <option>Packaging</option>
                                                                <option>Finished</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Warehouse Name</label>
                                                                <select class="form-control" disabled data-live-search="true" id="w_name" name="w_name" >
                                                                    <option value="" disabled selected>Select...</option>
                                                                    @foreach ($warehouse as $u)
                                                                    <option  value="{{$u->id}}">{{$u->w_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From</label>
                                                                <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To</label>
                                                                <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="" style="visibility: hidden">.</label>
                                                                <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                <i class="fa fa-search pr-1"></i> Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1400px">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:40px">S.No</th>
                                                            <th style="width:120px">Warehouse Name</th>
                                                            <th style="width:120px">Warehouse Type</th>
                                                            <th style="width:300px">Product Code</th>
                                                            <th style="width:300px">Product Name</th>
                                                            <th style="width:120px">Product Type</th>
                                                            <th style="width:40px">Open Quantity</th>
                                                            <th style="width:40px">Close Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $count = 1;
                                                            $closing = 0;
                                                            $opening = 0;
                                                            $status = false;
                                                            $finalopening = 0;
                                                            $finalclosing = 0;
                                                            $yesterdays = \Carbon\Carbon::yesterday()->format('Y-m-d');
                                                        @endphp
                                                        @if ($index == 0)
                                                            @foreach ($stock_in as $o)
                                                                @if ($o->stock_date <= $yesterdays)
                                                                    @foreach ($stock_out as $si)
                                                                        @if ($si->type == $o->type && $si->p_id==$o->p_id && $si->w_id == $o->w_id)
                                                                            @php
                                                                                $status = true;
                                                                                $opening = $o->quantity - $si->quantity;
                                                                                $closing = $o->quantity - $si->quantity;
                                                                                $finalopening+=$opening;
                                                                                $finalclosing+=$closing;
                                                                            @endphp
                                                                        @break
                                                                        @else
                                                                            @php
                                                                                $status = false;
                                                                                $opening = $o->quantity ;
                                                                                $closing = $o->quantity ;
                                                                                $finalopening+=$opening;
                                                                                $finalclosing+=$closing;
                                                                            @endphp
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @php
                                                                        $opening = 0 ;
                                                                        $finalopening+=$opening;
                                                                    @endphp
                                                                    @foreach ($stock_out as $si)
                                                                        @if ($si->type == $o->type && $si->p_id==$o->p_id && $si->w_id == $o->w_id)
                                                                            @php
                                                                                $closing = $o->quantity - $si->quantity;
                                                                                $finalclosing+=$closing;
                                                                            @endphp
                                                                        @break
                                                                        @else
                                                                            @php
                                                                                $closing = $o->quantity;
                                                                                $finalclosing+=$closing;
                                                                            @endphp
                                                                        @endif
                                                                    @endforeach
                                                                @endif

                                                                <tr>
                                                                    <td>{{$count}}</td>
                                                                    <td>{{$o->w_name}}</td>
                                                                    <td>{{$o->w_type}}</td>
                                                                    @if ($o->type == 0)
                                                                        @php
                                                                            $product = \App\Products::find($o->p_id);
                                                                        @endphp
                                                                        <td>{{$product->pro_code}}</td>
                                                                        <td>{{$product->pro_name}}</td>
                                                                        <td>{{$product->p_type}}</td>
                                                                    @else
                                                                        @php
                                                                            $product = \App\ProductVariants::with('product')->where('id',$o->p_id)->first();
                                                                            $name= explode(' - ',$product->name);
                                                                        @endphp
                                                                        <td>{{$name[0]}}</td>
                                                                        <td>{{$name[1]}}</td>
                                                                        <td>{{$product->product->p_type}}</td>
                                                                    @endif
                                                                    <td>{{$opening }}</td>
                                                                    <td>{{$closing}}</td>
                                                                </tr>
                                                                @php
                                                                    $count++;
                                                                @endphp
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$finalopening}}</td>
                                                            <td>{{$finalclosing}}</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                $('input:radio[name="optradio"]').change(function(){
                if ($(this).is(':checked')) {
                    $('#search').prop('disabled',false);
                    var val = $(this).val();
                    if(val == 'Year')
                    {
                        $('#year').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#w_name').prop('disabled',false);
                        $('#ptype').prop('disabled',false);
                        $('#year').attr('required',true);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#w_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Month')
                    {
                        $('#month').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#w_name').prop('disabled',false);
                        $('#ptype').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#w_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Date')
                    {
                        $('#from').prop('disabled',false);
                        $('#from').attr('required',true);
                        $('#to').prop('disabled',false);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#w_name').prop('disabled',false);
                        $('#ptype').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#month').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                        $('#w_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
                });

                var table = $('#example').DataTable({
                    scrollX: true
                });
        });
    </script>
    @endsection
@endsection
