@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/stocks">Stock</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">

                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-tasks font-white"></i>Current Stock
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px"  href="{{route('stocks.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    <a style="margin-left:-50px;" href="{{route('stocks.pdf')}}">
                                                        <i class="fa fa-file-pdf-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('stocks.create')}}">
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button" class="btn btn-block btn-primary btn-md ">Add Stock</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" id="p_name" multiple>
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($product as $s)
                                                                    @if ($s->vstatus == 0)
                                                                        <option value="{{$s->pro_name}}"> {{$s->pro_name}}</option>
                                                                    @else
                                                                        @foreach ($s->variants as $v)
                                                                            @php
                                                                                $name = explode(' - ',$v->name);
                                                                            @endphp
                                                                            <option value="{{$name[1]}}">{{$name[1]}} </option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Warehouse Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" id="w_name" multiple>
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($warehouse as $u)
                                                                <option  value="{{$u->w_name}}">{{$u->w_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" id="code"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Brand Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" multiple id="b_name" >
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($brand as $u)
                                                                <option  value="{{$u->b_name}}">{{$u->b_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" id="c_name" multiple>
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($cat as $u)
                                                                <option  value="{{$u->cat_name}}">{{$u->cat_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Type</label>
                                                            <select id="ptype" class="form-control selectpicker" multiple>
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                <option>Raw</option>
                                                                <option>Material</option>
                                                                <option>Packaging</option>
                                                                <option>Finished</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1400px">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:50px">S.No</th>
                                                            <th style="width:120px">Warehouse Name</th>
                                                            <th style="width:140px">Product Code</th>
                                                            <th style="width:140px">Product Name</th>
                                                            <th style="width:120px">Product Type</th>
                                                            <th style="width:50px">Unit</th>
                                                            <th style="width:80px">Brand</th>
                                                            <th style="width:50px">Category</th>
                                                            <th style="width:40px">Quantity</th>
                                                            <th style="width:50px">Cost</th>
                                                            <th style="width:50px">Total Cost</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $cost = 0 ;
                                                            $count = 0 ;
                                                            $count1 = 0 ;
                                                            $finalcost = 0;
                                                            $totalcost = 0 ;
                                                            $totalqty = 0 ;
                                                            $totalfinalcost = 0 ;
                                                        @endphp
                                                        @foreach ($stock as $s)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_name}}
                                                                </td>
                                                                @if ($s->type == 1)
                                                                    @php
                                                                        $name = explode(' - ',$s->variant->name);
                                                                    @endphp
                                                                    <td>{{$name[0]}}</td>
                                                                    <td>{{$name[1]}}</td>
                                                                    <td>
                                                                        {{ $s->variant->product->p_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->weight == null ? $s->variant->product->unit->u_name : $s->variant->product->weight.$s->variant->product->unit->u_name }}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->category->cat_name}}
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        {{$s->products->pro_code}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->p_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->weight == null ? $s->products->unit->u_name : $s->products->weight.$s->products->unit->u_name }}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->category->cat_name}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$s->quantity == null  ? $s->unit_quantity  : $s->quantity}}
                                                                </td>

                                                                <td>
                                                                    @if ($s->stock->isEmpty())
                                                                        {{$s->type == 1 ? $s->variant->cost : $s->products->cost}}
                                                                        @php
                                                                            $finalcost += ($s->type == 1 ? $s->variant->cost : $s->products->cost) * ($s->quantity == null  ? $s->unit_quantity  : $s->quantity);
                                                                            $totalcost += $s->type == 1 ? $s->variant->cost : $s->products->cost;
                                                                            $totalfinalcost += $finalcost;
                                                                        @endphp
                                                                    @else
                                                                        @php
                                                                            $count = count($s->stock);
                                                                            $c = 0;
                                                                        @endphp
                                                                        @foreach ($s->stock as $p)
                                                                            @php
                                                                                $c = $p->cost == null ? 0 : $p->cost;
                                                                                $cost+=$c;
                                                                                $totalcost += $cost;
                                                                            @endphp
                                                                        @endforeach
                                                                        @if ($count > 0 || $cost > 0)
                                                                        {{round($cost/$count)}}
                                                                        @php
                                                                            $finalcost += ($cost/$count) * ($s->quantity == null  ? $s->unit_quantity  : $s->quantity);
                                                                            $totalfinalcost += $finalcost;
                                                                        @endphp
                                                                        @else
                                                                        {{0}}
                                                                        @php
                                                                            $finalcost += ($s->type == 1 ? $s->variant->cost : $s->products->cost) * ($s->quantity == null  ? $s->unit_quantity  : $s->quantity);
                                                                            $totalfinalcost += $finalcost;
                                                                        @endphp
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{$finalcost}}
                                                                </td>

                                                            </tr>
                                                            @php
                                                                $totalqty += $s->quantity == null  ? $s->unit_quantity  : $s->quantity;
                                                                $a++;
                                                                $count = 0 ;
                                                                $count1 = 0 ;
                                                                $price = 0 ;
                                                                $cost = 0 ;
                                                                $finalcost = 0;
                                                            @endphp
                                                        @endforeach
                                                        <tfoot>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>TOTAL</td>
                                                                <td>{{$totalqty}}</td>
                                                                <td>{{$totalcost}}</td>
                                                                <td>{{$totalfinalcost}}</td>
                                                            </tr>
                                                        </tfoot>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    scrollX: true
                });
                $('#w_name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#w_name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#w_name').selectpicker('deselectAll');
                        $('#w_name').selectpicker('refresh');
                    }
                    $.each($('#w_name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(1).search(search, true, false).draw();
                });
                $('#p_name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#p_name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#p_name').selectpicker('deselectAll');
                        $('#p_name').selectpicker('refresh');
                    }
                    $.each($('#p_name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(3).search(search, true, false).draw();
                });
                $('#ptype').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#ptype option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#ptype').selectpicker('deselectAll');
                        $('#ptype').selectpicker('refresh');
                    }
                    $.each($('#ptype option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(4).search(search, true, false).draw();
                });
                $('#b_name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#b_name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#b_name').selectpicker('deselectAll');
                        $('#b_name').selectpicker('refresh');
                    }
                    $.each($('#b_name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(6).search(search, true, false).draw();
                });
                $('#c_name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#c_name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#c_name').selectpicker('deselectAll');
                        $('#c_name').selectpicker('refresh');
                    }
                    $.each($('#c_name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(7).search(search, true, false).draw();
                });
                $('#code').on('keyup',function(){
                    value = $("#code").val();
                    table.columns(2).search(value, true, false).draw();
                });
            });
    </script>
    @endsection
@endsection
