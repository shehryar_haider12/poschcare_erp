@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/stocks">Stocks</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Stock</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-tasks font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Stock</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('stocks.update',$sub->id) :  route('stocks.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Name*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="p_id" id="p_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @foreach ($products as $s)
                                                @if ($s->vstatus == 0)
                                                    <option value="{{$s->id.'-'.'0'}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}} - {{$s->unit->u_name}}</option>
                                                @else
                                                    @foreach ($s->variants as $v)
                                                        <option value="{{$v->id.'-'.'1'}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}} - {{$s->unit->u_name}}</option>
                                                    @endforeach
                                                @endif
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Quantity*</label>
                                        <input min="1" value="{{$stock->quantity ?? old('quantity')}}" class="form-control" type="text" placeholder="Enter Quantity" name="quantity" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Unit</label>
                                        <input type="text" readonly class="form-control" id="unitName">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouses*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="w_id" id="w_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            @foreach ($warehouse as $u)
                                            <option {{$product->w_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->w_name}} - {{$u->w_type}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($warehouse as $u)
                                            <option  value="{{$u->id}}">{{$u->w_name}} - {{$u->w_type}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Supplier*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="s_id" id="s_id" required>
                                            <option value="" disabled selected>Select...</option>

                                            @foreach ($supplier as $u)
                                            <option  value="{{$u->id}}">{{$u->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Date*</label>
                                        <input value="{{$stock->stock_date ?? old('stock_date') ?? $date}}" class="form-control" type="date" placeholder="Enter Quantity" name="stock_date" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Cost*</label>
                                        <input min="1" value="{{$stock->cost ?? old('cost')}}" class="form-control" type="text" placeholder="Enter Cost" name="cost" id="cost" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Invoice Number <small>(optional)</small> </label>
                                        <input min="1" value="{{$stock->inv_no ?? old('inv_no')}}" class="form-control" type="text" placeholder="Enter Invoice Number" name="inv_no">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Challan Number <small>(optional)</small> </label>
                                        <input min="1" value="{{$stock->challan_no ?? old('challan_no')}}" class="form-control" type="text" placeholder="Enter Challan Number" name="challan_no">
                                    </div>
                                </div>

                                <div class="col-sm-6 finish"  hidden>
                                    <div class="form-outline">
                                        <label >Available Quantity*</label>
                                        <select class="form-control uid" multiple data-live-search="true" name="u_id[]" id="u_id" >
                                        </select>

                                    </div>
                                </div>
                                <div class="col-sm-6 image" hidden>
                                    <div class="form-outline">
                                        <label>Image</label>
                                        <img class="img-fluid"  width="120px" height="150px" id="img" alt="">
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@endsection
@section('custom-script')
@toastr_js
@toastr_render

<script>
    $(document).on('change','#p_id',function(){
        var id = $(this).val();
        $("#u_id").empty();
        $('#u_id').selectpicker('destroy');
        $.ajax({
            url:"{{url('')}}/product/stockShow/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data[2]);
                var APP_URL = {!! json_encode(url('/')) !!}
                if(data[1]==0)
                {
                    var img = '';
                    if(data[0].image == null || data[0].image == "")
                    {
                        img = APP_URL+"/uploads/noimg.png";
                    }
                    else
                    {
                        img = APP_URL+"/uploads/"+data[0].image;
                    }
                    $('#img').attr('src',img);
                    $('.image').show();
                    $('#cost').val(data[0].cost);
                    $('#unitName').val(data[0].unit.u_name);
                    if(data[2].length != 0)
                    {
                        $('.finish').show();
                        $('#u_id').append(`<option value="" disabled >Select...</option>`);
                        for(var i = 0; i < data[2].length ; i++)
                        {
                            if(data[2][i].type == 1)
                            {
                                if(data[2][i].length > 0)
                                {
                                    var qty =  data[2][i][0].total;
                                    var uname = data[2][i][0].unit.u_name;
                                    $('#u_id').append(`<option value = "`+data[2][i][0].id+`">`+data[2][i][0].variant.name+` In `+data[2][i][0].warehouse.w_name+` Size: `+data[2][i][0].size+uname+` Quantity: `+qty+`</option>`);
                                }
                            }
                            else
                            {
                                if(data[2][i].length > 0)
                                {
                                    var qty = data[2][i][0].total;
                                    var uname = data[2][i][0].unit.u_name;

                                    $('#u_id').append(`<option value = "`+data[2][i][0].id+`">`+data[2][i][0].products.pro_code+` - `+data[2][i][0].products.pro_name+` In `+data[2][i][0].warehouse.w_name+` Size: `+data[2][i][0].size+uname+` Quantity: `+qty+`</option>`);
                                }
                            }
                        }
                        $('.uid').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    else
                    {
                        $('.finish').hide();
                    }
                }
                else
                {
                    $('#cost').val(data[0].cost);
                    $('#unitName').val(data[0].product.unit.u_name);
                    $('.finish').hide();
                    // if(data[2].length != 0)
                    // {
                    //     $('.finish').show();
                    //     $('#u_id').append(`<option value="" disabled selected>Select...</option>`);
                    //     for(var i = 0; i < data[2].length ; i++)
                    //     {
                    //         if(data[2][i].type == 1)
                    //         {
                    //             var qty =  data[2][i].total;
                    //             var uname = data[2][i].unit.u_name;
                    //             $('#u_id').append(`<option value = "`+data[2][i].variant.id+` - `+data[2][i].w_id+` - `+qty+`">`+data[2][i].variant.name+` In `+data[2][i].warehouse.w_name+` quantity: `+qty+uname+`</option>`);
                    //         }
                    //         else
                    //         {
                    //             var qty = data[2][i].total;
                    //             var uname = data[2][i].unit.u_name;

                    //             $('#u_id').append(`<option value = "`+data[2][i].products.id+` - `+data[2][i].w_id+` - `+qty+`">`+data[2][i].products.pro_code+` - `+data[2][i].products.pro_name+` In `+data[2][i].warehouse.w_name+` quantity: `+qty+uname+`</option>`);
                    //         }
                    //     }
                    // }
                    // else
                    // {
                    //     $('.finish').hide();
                    // }
                }
            }
        });
    });
</script>
@endsection
