<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .hr {
        display: block;
        border-style: inset;
        border-width: 1px;
        margin-top: 30px;
        margin-right: 485px;
        width: 50%;
    }
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            font-size: 15px;
            text-align: left;
            /* color: #000; */
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            /* background: #F0FFFF; */
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A4;
        }
  </style>
    <body>
        <img src="{{url('')}}/uploads/posch.jpg" style="margin-top: 10px;  float: left;" width="110px" height="40px" alt="logo" class="logo-default"  />
        <header>
          GRR (GOOD RECEIVED RECEIPT)
        </header>
        <div class="label col-sm-12">
            {{-- <label><b>Reference#: </b>{{$purchase->ref_no}}</label>
            <br> --}}
            @php
                $date=Carbon\Carbon::parse($stock->stock_date)->format('d-m-y');
            @endphp
            <label><b>GRR#: </b>{{$stock->grn_no}}</label>
            <br>
            <label><b>Date: </b>{{$date}}</label>

        </div>

        <div class="form-body">

            <div class="row col">
                <div>
                    <div class="form-outline">
                        <label for=""><b>Supplier:</b></label>
                    </div>
                </div>
                {{-- <div style="margin-top: -18px; margin-left: 480px">
                    <div class="form-outline">
                        <label for=""><b>Customer:</b></label>
                    </div>
                </div> --}}
            </div>
            <div class="row">
                <div>
                    <div class="form-outline">
                        <p><b>Name: {{ucwords($stock->supplier->name)}}</b></p>
                        <p><b>Company: {{ucwords($stock->supplier->company)}}</b></p>
                        <p><b>Contact No: {{$stock->supplier->c_no}}</b></p>
                        <p><b>Address: {{ucwords($stock->supplier->address)}}</b></p>
                    </div>
                </div>
                {{-- <div style="margin-top: -150px; margin-left: 480px">
                    <div class="form-outline">
                        <p><b>Name: {{ $purchase->biller == null ? 'Posch Care' :  ucwords($purchase->biller->name)}}</b></p>
                        <p><b>Warehouse Name: {{ucwords($purchase->warehouse->w_name)}}</b></p>
                        <p><b>Address: {{ucwords($purchase->warehouse->w_address)}}</b></p>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="attendance-table" style="margin-top: 25px">
            <table class="table table-striped table-bordered">

                <thead>
                    <tr>
                        <th class="attendance-cell">S.No</th>
                        <th class="attendance-cell">Name</th>
                        <th class="attendance-cell">Brand Name</th>
                        <th class="attendance-cell">UOM</th>
                        {{-- <th class="attendance-cell">Ordered Quantity</th> --}}
                        <th class="attendance-cell">Received Quantity</th>
                        {{-- <th class="attendance-cell">Weight</th> --}}
                        {{-- <th class="attendance-cell">Brand</th> --}}
                        {{-- <th class="attendance-cell" >Cost</th> --}}
                        {{-- <th class="attendance-cell">Sub total</th> --}}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td  class="attendance-cell">
                            1
                        </td>
                        <td  class="attendance-cell">
                            {{$stock->products->pro_name}}
                        </td>
                        <td  class="attendance-cell">
                            {{$stock->products->brands->b_name}}
                        </td>
                        <td>
                            {{$stock->products->unit->u_name}}
                        </td>
                        <td  class="attendance-cell">
                            {{$stock->quantity}}
                        </td>
                    </tr>

                </tbody>

            </table>


        </div>
        <br>
        <br>
        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Recieved By:</b></label>
                    <hr color="black" class="hr">
                </div>
            </div>
        </div>
    </body>
</html>
