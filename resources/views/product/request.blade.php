@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/productUpdateRequest">Product Data Update Request</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-list-alt font-white"></i>View Product Data Update Request
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <input type="text" id="p_name"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" id="code"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Brand Name</label>
                                                        <select class="form-control selectpicker" data-live-search="true" multiple id="b_name">
                                                            <option value="Not All">Unselect All</option>
                                                            <option value="All">Select All</option>
                                                            @foreach ($brands as $u)
                                                            <option  value="{{$u->b_name}}">{{$u->b_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" multiple id="c_name">
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($cat as $u)
                                                                <option  value="{{$u->cat_name}}">{{$u->cat_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Date</label>
                                                            <input type="date" id="date"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Name</th>
                                                            <th>Code</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Date</th>
                                                            <th>Weight/Unit</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Product</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="pro_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Code</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="pro_code" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Weight</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Weight" id="weight" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Unit</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="unit" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Cost</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Cost" id="cost" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >TP</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Code" id="price" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Quantity</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Quantity" id="quantity" readonly>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Alert Quantity</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Alert Quantity" id="alert_quantity" readonly>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="row"> --}}
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Brand</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Brand" id="brand" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Category</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Category" id="cat" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sub Category</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Sub Category" id="sub" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >MRP</label>
                                    <input class="form-control" type="text" placeholder="Enter Product MRP" id="mrp" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row variant">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
@toastr_render
        {{-- view one product --}}
        <script>
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/productUpdateRequest/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#pro_name').val(data.pro_name);
                        $('#pro_code').val(data.pro_code);
                        $('#weight').val(data.weight);
                        $('#unit').val(data.unit.u_name);
                        $('#brand').val(data.brands.b_name);
                        $('#cat').val(data.category.cat_name);
                        $('#sub').val(data.subcategory.s_cat_name);
                        // $('#ware').val(data.warehouse.w_name);
                        $('#cost').val(data.cost);
                        $('#mrp').val(data.mrp);
                        $('#price').val(data.tp);
                        $('#alert_quantity').val(data.alert_quantity);

                    }
                });
            });
        </script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "desc" ]],
            scrollX: true,
            ajax: '{{route("productUpdateRequest.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "pro_name",
                    "defaultContent": ""
                },
                {
                    "data": "pro_code",
                    "defaultContent": ""
                },
                {
                    "data": "brands.b_name",
                    "defaultContent": ""
                },
                {
                    "data": "category.cat_name",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": "",
                    "render": function (value) {
                        if (value === null) return "";
                        return moment(value).format('MM/DD/YYYY');
                    }
                },
                {
                    "data": "",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        if(row.weight == 0 || row.weight ==null)
                        {
                            return row.unit.u_name;
                        }
                        else
                        {
                            return row.weight+row.unit.u_name;
                        }
                    }
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var sta = row.status;
                        var status = '{{route("productUpdateRequest.status",[":id"])}}';
                        status = status.replace(':id', row.id );
                        if (sta == 'Pending') {
                            return `
                            @if(in_array('show',$permissions))
                                <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                    <i class="icon-eye"></i>
                                </button>
                            @endif
                            @if(in_array('status',$permissions))
                                <a href="`+status+`">
                                    <button class="btn btn-sm btn-danger">Approve It</button>
                                </a>
                            @else
                                <button class="btn btn-sm btn-success" disabled>`+row.status +`</button>
                            @endif
                            `;
                        }
                        else {
                            return `
                            @if(in_array('show',$permissions))
                                <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                    <i class="icon-eye"></i>
                                </button>
                            @endif
                            <button class="btn btn-sm btn-success" disabled>`+row.status +`</button>
                            `;
                        }
                    },
                },
            ],
        });

        $('#b_name').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#b_name option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#b_name').selectpicker('deselectAll');
                $('#b_name').selectpicker('refresh');
            }
            $.each($('#b_name option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(3).search(search, true, false).draw();
        });
        $('#c_name').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#c_name option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#c_name').selectpicker('deselectAll');
                $('#c_name').selectpicker('refresh');
            }
            $.each($('#c_name option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(4).search(search, true, false).draw();
        });
        $('#p_name').on('keyup',function(){
            value = $("#p_name").val();
            table.columns(1).search(value, true, false).draw();
        });
        $('#code').on('keyup',function(){
            value = $("#code").val();
            table.columns(2).search(value, true, false).draw();
        });
        $('#date').on('change',function(){
            value = $("#date").val();
            table.columns(5).search(value, true, false).draw();
        });
    });


</script>
    @endsection
@endsection
