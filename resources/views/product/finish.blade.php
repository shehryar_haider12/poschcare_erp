@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
     thead {
	    background-color: #ADD8E6;
    }
</style>
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/product">Product</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Finish Product</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-list-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Finish Product</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('product.finishProducts.update',$product->id) : route('product.addFinishProduct')}} " class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="image" id="imageUpload1" placeholder="add image"
                                            accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Featured Image <small>(optional)</small></span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/'}}{{$isEdit ? $product->image : 'placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Name*</label>
                                        <input value="{{$product->pro_name ?? old('pro_name')}}" {{$isEdit ? 'readonly' : ''}}  class="form-control" type="text" placeholder="Enter Product Name" name="pro_name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Code*</label>
                                        <input readonly value="{{$product->pro_code ?? old('pro_code') ?? $code}}" class="form-control" type="text" placeholder="Enter Product Code" name="pro_code" required>
                                        <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Weight*</label>
                                        <input min="1" value="{{$product->weight ?? old('weight')}}" class="form-control" type="number" placeholder="Enter Product Weight" name="weight" >
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Unit',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Product Unit*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="unit_id" required id="unit_id">
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($unit as $u)
                                                    <option {{$product->unit_id == $u->id ? 'selected' : null}} value="{{$u->u_name}}">{{$u->u_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($unit as $u)
                                                    <option  value="{{$u->id}}">{{$u->u_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 24px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#unitModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                {{-- <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Quantity*</label>
                                        <input min="1" class="form-control" type="number" placeholder="Enter Product Quantity" name="stockquantity" required>
                                    </div>
                                </div> --}}
                            </div>

                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="form-outline">
                                        <label >Products*</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="p_id" required>
                                            <option disabled selected>Select..</option>
                                            @foreach ($pro as $s)
                                                <option value="{{$s->id}}">{{$s->pro_code}} -
                                                    {{$s->pro_name}} - {{$s->unit->u_name}}
                                                 - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-control" style="margin-top: 24px; width:50px">
                                        <a href="#"  data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-2x fa-list addIcon font-green" style="margin-top: 3px"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-outline">
                                        <label>Product Variants</label>
                                        <select class="form-control" data-live-search="true" id="v_id" >
                                            <option disabled selected>Select..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th width="35%">Code - Name - Weight - Brand - Type</th>
                                                <th width="5%">Quantity Usage</th>
                                                <th width="18%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $a = 1;
                                            @endphp
                                            @if ($isEdit)
                                                @foreach ($fp as $f)

                                                <tr>
                                                    <td>
                                                        <input type='hidden' class='cost{{$a}}' value='{{$f->rawproducts->cost}}'>
                                                        <input type='hidden' name='unit1[]' value='{{$f->rawproducts->unit->u_name}}'>
                                                        <input name='p_id[]' type='text' class='form-control' readonly value='{{$f->p_id}}'>
                                                    </td>
                                                    <td>
                                                        {{$f->rawproducts->pro_code.' - '.
                                                        $f->rawproducts->pro_name.' - '.
                                                        $f->rawproducts->unit->u_name.' - '.
                                                        $f->rawproducts->brands->b_name.' - '.
                                                        $f->rawproducts->p_type}}
                                                    </td>
                                                    <td>
                                                        <input type='number'  class='form-control quantity'  id='{{$f->p_id}}' name='quantity[]' value='{{$f->quantity}}'>
                                                    </td>
                                                    <td>
                                                        <button type='button' id='{{$f->p_id}}' class='btn red delete' ><i class='fa fa-trash'></i></button>
                                                    </td>
                                                </tr>
                                                    @php
                                                        $a++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-offset-5" >
                                    <button type="button" class="btn green" id="check">Check</button>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Average Cost*</label>
                                        <input min="1" value="{{$product->cost ?? old('cost')}}" class="form-control" type="number" placeholder="Enter Product Cost" name="cost" id="cost" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Price*</label>
                                        <input min="1" value="{{$product->price ?? old('price')}}" class="form-control" type="number" placeholder="Enter Product Price" name="price" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Description <small>(optional)</small></label>
                                        <input value="{{$product->description ?? old('description')}}" class="form-control" type="text" placeholder="Enter Product Description" name="description">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Alert Quantity*</label>
                                        <input  value="{{$product->alert_quantity ?? old('alert_quantity')}}" class="form-control" type="number" placeholder="Enter Product Alert Quantity" name="alert_quantity" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Brand',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Brand*</label>
                                        <select style="overflow-y: scroll; height: 20px;"  class="form-control selectpicker" data-live-search="true" name="brand_id" id="brand_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($brands as $u)
                                                    <option {{$product->brand_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->b_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($brands as $u)
                                                    <option  value="{{$u->id}}">{{$u->b_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 0px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#brandModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Category*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="cat_id" id="cat_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($cat as $u)
                                                    <option {{$product->cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cat as $u)
                                                    <option  value="{{$u->id}}">{{$u->cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 0px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#catModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Sub Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Sub Category*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="s_cat_id" id="s_cat_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($sub as $u)
                                                    <option {{$product->s_cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($sub as $u)
                                                    <option  value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top:0px;  width:50px">
                                            <a href="#" id="subcat" data-toggle="modal" data-target="#subModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Visible to POS*</label>
                                        <select class="form-control selectpicker" name="visibility" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->visibility == '1' ? 'selected' : null }} value="1">Yes</option>
                                            <option {{$product->visibility == '0' ? 'selected' : null }} value="0">No</option>
                                            @else
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>MRP <small>(optional)</small> </label>
                                        <input type="text" placeholder="Enter MRP" class="form-control" name="mrp">
                                    </div>
                                </div>
                            </div>

                            <br>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0" >
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
        @include('modals.unit')
        @include('modals.category')
        @include('modals.subcategory')
        @include('modals.brand')
        @include('modals.products')

@endsection


@section('custom-script')
@toastr_js
@toastr_render
    <script>
        $(document).ready(function(){ //Make script DOM ready
            $('#unit_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#unitModal').modal("show"); //Open Modal
            }
        });
        $('#cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#catModal').modal("show"); //Open Modal
            }
        });
        $('#s_cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#subModal').modal("show"); //Open Modal
                $('#cattt').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        });
        $('#brand_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#brandModal').modal("show"); //Open Modal
            }
        });


        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
            $("#imageUpload1").change(function () {
                readURL(this, 1);
        });

        var table = $('#example5').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: '{{route("product.add.datatable")}}',
            "columns": [{
                    "data": "",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        return row.id;
                    },
                },
                {
                    "data": "pro_name",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        return row.pro_code+ ` - ` +row.pro_name ;
                    },
                },
                {
                    "data": "weight",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        if(row.weight == null)
                        {
                            return row.u_name;
                        }
                        else
                        {
                            return row.weight+``+row.u_name ;
                        }
                    },
                },
                {
                    "data": "b_name",
                    "defaultContent": ""
                },
                {
                    "data": "p_type",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        return ` <button type="button"  class="btn green add" id="add`+row.p_id+`+`+row.w_id+`">
                            <i class="fa fa-plus"></i>
                        </button>`;
                    },
                },
            ],
        });

        $('#example5').on('click','.add',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).attr('id');
            id = id.substring(3, 6);
            console.log(id);
            $.ajax({
                url:"{{url('')}}/product/add/show/"+id,
                method:"GET",
                error: function (request, error) {
                        alert("Variants status is not approved ");
                        },
                success:function(data){
                    console.log(data);
                    if(data.weight == 0)
                    {
                        weight='';
                    }
                    else
                    {
                        weight=data.weight;
                    }
                    if(data.variants.length == 0)
                    {
                        if($('#'+data.id).length && data.vstatus == 0)
                        {

                        }
                        else
                        {
                            $("#example").append("<tr><td><input type='hidden' class='cost"+rowCount+"' value='"+data.cost+"'>  <input type='hidden' name='unit1[]' value='"+data.unit.u_name+"'><input name='p_id["+rowCount+"]' type='text' class='form-control pid' readonly value='"+data.id+"'></td><td>"+data.pro_code+" - "+data.pro_name+" - "+data.unit.u_name+" - "+data.brands.b_name+" - "+data.p_type+"</td></td><td><input type='number' class='form-control quantity'  id='"+data.id+"' name='quantity["+rowCount+"]' value='1'><input type='hidden' class='type' name='type["+rowCount+"]' value='0'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                        }
                        var total=0;
                        var rowCount1 = $('#example tr').length;
                        var a = rowCount1 - 1;
                        for (let i = 1; i < rowCount1; i++) {
                            total = +$('.cost'+i).val() + +total;
                        }
                            total = total / a;
                        $('#cost').val(Math.round(total));
                        $("#productModal").modal('hide');
                    }
                    else
                    {
                        $('#v_id').empty();
                        $('#v_id').selectpicker('destroy');
                        $('#v_id').append(`<option disabled selected>Select..</option>`);
                        for (let index = 0; index < data.variants.length; index++) {
                            if(data.variants[index].status == 0)
                            {

                            }
                            else
                            {
                                $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                            }
                        }
                        $('#v_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });
        });

        $(document).on('change','#p_id',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).val();
            // console.log(id);
            $.ajax({
                url:"{{url('')}}/product/add/show/"+id,
                method:"GET",
                error: function (request, error) {
                            alert("Variants status is not approved ");
                        },
                success:function(data){
                    // console.log(data);
                    if(data.weight == 0)
                    {
                        weight='';
                    }
                    else
                    {
                        weight=data.weight;
                    }
                    if(data.variants.length == 0)
                    {
                        if($('#'+data.id).length && data.vstatus == 0)
                        {

                        }
                        else
                        {
                            $("#example").append("<tr><td> <input type='hidden' class='cost"+rowCount+"' value='"+data.cost+"'><input type='hidden' name='unit1["+rowCount+"]' value='"+data.unit.u_name+"'><input name='p_id["+rowCount+"]' type='text' class='form-control pid' readonly value='"+data.id+"'></td><td>"+data.pro_code+" - "+data.pro_name+" - "+data.unit.u_name+" - "+data.brands.b_name+" - "+data.p_type+"</td></td><td><input type='number' class='form-control quantity'  id='"+data.id+"' name='quantity["+rowCount+"]' value='1'><input type='hidden' class='type' name='type["+rowCount+"]' value='0'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                        }
                        var total=0;
                        var rowCount1 = $('#example tr').length;
                        var a = rowCount1 - 1;
                        for (let i = 1; i < rowCount1; i++) {
                            total = +$('.cost'+i).val() + +total;
                        }
                            total = total / a;
                        $('#cost').val(Math.round(total));
                    }
                    else
                    {
                        $('#v_id').empty();
                        $('#v_id').selectpicker('destroy');
                        $('#v_id').append(`<option disabled selected>Select..</option>`);
                        for (let index = 0; index < data.variants.length; index++) {
                            if(data.variants[index].status == 0)
                            {

                            }
                            else
                            {
                                $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                            }
                        }
                        $('#v_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });
        });

    $('table').on('click', '.delete', function(e){
        var id=$(this).attr('id');
        $(this).closest('tr').remove();
        $(".pid").each(function (i){
            i=+i + +1;
            $(this).attr('name','p_id['+i+']');
        });
        $(".type").each(function (i){
            i=+i + +1;
            $(this).attr('name','type['+i+']');
        });
        $(".quantity").each(function (i){
            i=+i + +1;
            $(this).attr('name','quantity['+i+']');
        });
        $(".delete").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });
    });

    $(document).on('change','#v_id',function(){
        var rowCount = $('#example tr').length;
        var id=$(this).val();
        var w_id = $('#w_id').val();
        $.ajax({
            url:"{{url('')}}/product/Pvariantsforfinishproduct/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                // console.log(data);

                if($('#'+data.id).length && data.vstatus == 1)
                {

                }
                else
                {
                    $("#example").append(`<tr><td><input type='hidden' class='cost`+rowCount+`' value='`+data.cost+`'><input type='hidden' name='unit1[`+rowCount+`]' value='`+data.product.unit.u_name+`'><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'> </td><td>`+data.name+`</td><td><input type='text' class='form-control quantity' name='quantity[`+rowCount+`]' value='1'><input type="hidden" name="type[`+rowCount+`]" value="1" class="type"></td><td><button type='button' id='`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
                }
                var total=0;
                var rowCount1 = $('#example tr').length;
                var a = rowCount1 - 1;
                // console.log(a);
                for (let i = 1; i < rowCount1; i++) {
                    // console.log($('.cost'+i).val());
                    total = +$('.cost'+i).val() + +total;
                }
                    total = total / a;
                $('#cost').val(Math.round(total));
            }
        });
    });
});

$(document).on('click','#subcat',function(){
    $('#cattt').addClass('selectpicker');
    $('.selectpicker').selectpicker('render');
})
</script>


@endsection

