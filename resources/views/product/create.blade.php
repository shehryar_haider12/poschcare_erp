@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    thead {
       background-color: #ADD8E6;
   }
</style>
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/product">Product</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Product</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-list-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Product</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="productForm" action="{{$isEdit ? route('product.update',$product->id) :  route('product.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="image" id="imageUpload1" placeholder="add image"
                                            accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Featured Image <small>(optional)</small> </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/'}}{{$isEdit ? $product->image : 'placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Name*</label>
                                        <input value="{{$product->pro_name ?? old('pro_name')}}"  class="form-control" type="text" placeholder="Enter Product Name" name="pro_name" >
                                        <span class="text-danger">{{$errors->first('pro_name') ? 'Enter name' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Barcode <small>(optional)</small> </label>
                                        <select id="barcode"  class="form-control selectpicker" >
                                            <option selected value="No">Auto Generator</option>
                                            <option   value="Yes">Custom</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Code*</label>
                                        <input {{$isEdit ? '' : 'readonly'}} value="{{$product->pro_code ?? old('pro_code') ?? $code }}" autofocus class="form-control" type="text" placeholder="Enter Product Code" id="pro_code" name="pro_code" >
                                        {{-- <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span> --}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Type*</label>
                                        <select class="form-control selectpicker" name="p_type" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->p_type == 'Raw' ? 'selected' : null }}>Raw</option>
                                            <option {{$product->p_type == 'Material' ? 'selected' : null }}>Material</option>
                                            <option {{$product->p_type == 'Packaging' ? 'selected' : null }}>Packaging</option>
                                            <option {{$product->p_type == 'Finished' ? 'selected' : null }}>Finished</option>
                                            @else
                                            <option>Raw  {{old('p_type') == 'Raw' ? 'selected' : null}}</option>
                                            <option>Material  {{old('p_type') == 'Material' ? 'selected' : null}}</option>
                                            <option>Packaging {{old('p_type') == 'Packaging' ? 'selected' : null}}</option>
                                            <option>Finished {{old('p_type') == 'Finished' ? 'selected' : null}}</option>

                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('p_type') ? 'select type' : null}}</span>
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Unit',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Product Unit*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="unit_id" required id="unit_id">
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($unit as $u)
                                                    <option {{$product->unit_id == $u->id ? 'selected' : null}} value="{{$u->u_name}}">{{$u->u_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($unit as $u)
                                                    <option  value="{{$u->u_name}}" {{old('unit_id') == $u->u_name ? 'selected' : null}}>{{$u->u_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('unit_id') ? 'select unit' : null}}</span>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#unitModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Weight <small>(optionall)</small></label>
                                        <input min="0" value="{{$product->weight ?? old('weight')}}" class="form-control" type="text" placeholder="Enter Product Weight" name="weight" id="weight">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label>Manufactured*</label>
                                        <select class="form-control selectpicker" name="mstatus" id="mstatus" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->mstatus == 'Yes' ? 'selected' : null }}>Yes</option>
                                            <option {{$product->mstatus == 'No' ? 'selected' : null }}>No</option>
                                            @else
                                            <option>Yes {{old('mstatus') == 'Yes' ? 'selected' : null}}</option>
                                            <option>No {{old('mstatus') == 'No' ? 'selected' : null}}</option>

                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('mstatus') ? 'select anyone' : null}}</span>
                                    </div>
                                </div>

                            </div>
                            <div id="manufactured" hidden>
                                <div class="row">
                                    <div class="col-sm-11">
                                        <div class="form-outline">
                                            <label >Products*</label>
                                            <select class="form-control selectpicker" data-live-search="true" id="p_id" >
                                                <option disabled selected>Select..</option>
                                                @foreach ($pro as $s)
                                                    <option value="{{$s->id}}">{{$s->pro_code}} -
                                                        {{$s->pro_name}} - {{$s->unit->u_name}}
                                                     - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 24px; width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-2x fa-list addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-outline">
                                            <label>Product Variants</label>
                                            <select class="form-control" data-live-search="true" id="v_id" >
                                                <option disabled selected>Select..</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="5%">S.No</th>
                                                    <th width="35%">Code - Name - Weight - Brand - Type</th>
                                                    <th width="15%">Quantity Usage</th>
                                                    <th width="15%">Cost/Unit</th>
                                                    <th width="15%">Cost</th>
                                                    <th width="18%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $a = 1;
                                                    $raw =0;
                                                    $mat =0;
                                                    $pkg =0;
                                                    $fnsh =0;
                                                @endphp
                                                @if ($isEdit)
                                                    @foreach ($fp as $f)

                                                    <tr>
                                                        <td>
                                                            <input type='hidden' class='p_type ptype{{$a}}' value='{{$f->type == 0 ? $f->rawproducts->p_type : $f->rawproducts2->product->p_type}}'>
                                                            <input type='hidden' class="unitss" name='unitss[{{$a}}]' value='{{$f->type == 0 ? $f->rawproducts->unit->u_name : $f->rawproducts2->product->unit->u_name}}'>
                                                            <input name='p_id[{{$a}}]' type='text' class='form-control pid' readonly value='{{$f->p_id}}'>
                                                        </td>
                                                        @if ($f->type == 0)
                                                            <td>
                                                                {{$f->rawproducts->pro_code.' - '.
                                                                $f->rawproducts->pro_name.' - '.
                                                                $f->rawproducts->unit->u_name.' - '.
                                                                $f->rawproducts->brands->b_name.' - '.
                                                                $f->rawproducts->p_type}}
                                                            </td>
                                                            <td>
                                                                <input type='text'  class='form-control quantity qty{{$a}}'  id='{{$a}}' name='quantity[{{$a}}]' value='{{$f->quantity}}'>
                                                                <input type="hidden" class="type" name="type[{{$a}}]" value="{{$f->type}}">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control costperunit cpu{{$a}}" id="{{$f->p_id}}" readonly name="costperunit[{{$a}}]" value="{{$f->costperunit == null ? $f->rawproducts->cost : $f->costperunit}}">
                                                            </td>
                                                            <td>
                                                                <input type="text" readonly class="form-control costsf costf{{$a}}" id="{{$f->p_id}}" name="costf[{{$a}}]" value="{{$f->cost == null ? ($f->rawproducts->cost * $f->quantity) : $f->cost}}">
                                                            </td>
                                                            <td>
                                                                <button type='button' id='{{$f->p_id}}' class='btn red delete' ><i class='fa fa-trash'></i></button>
                                                            </td>
                                                        @else
                                                            <td>
                                                                {{$f->rawproducts2->name.' - '.
                                                                $f->rawproducts2->product->unit->u_name.' - '.
                                                                $f->rawproducts2->product->brands->b_name.' - '.
                                                                $f->rawproducts2->product->p_type
                                                            }}
                                                            </td>
                                                            <td>
                                                                <input type='text'  class='form-control quantity qty{{$a}}'  id='{{$a}}' name='quantity[{{$a}}]' value='{{$f->quantity}}'>
                                                                <input type="hidden" class="type" name="type[{{$a}}]" value="{{$f->type}}">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control costperunit cpu{{$a}}" id="{{$f->p_id}}" readonly name="costperunit[{{$a}}]" value="{{$f->costperunit == null ? $f->rawproducts2->product->cost : $f->costperunit}}">
                                                            </td>
                                                            <td>
                                                                <input type="text" readonly class="form-control costsf costf{{$a}}" id="{{$f->p_id}}" name="costf[{{$a}}]" value="{{$f->cost == null ? ($f->rawproducts2->product->cost * $f->quantity) : $f->cost}}">
                                                            </td>
                                                            <td>
                                                                <button type='button' id='{{$f->p_id}}' class='btn red delete' ><i class='fa fa-trash'></i></button>
                                                            </td>
                                                        @endif

                                                    </tr>
                                                        @php
                                                            if($f->type == 0)
                                                            {
                                                                if($f->rawproducts->p_type == 'Raw')
                                                                {
                                                                    $raw+=$f->cost == null ? ($f->rawproducts->cost * $f->quantity) : $f->cost;
                                                                }
                                                                if($f->rawproducts->p_type == 'Material')
                                                                {
                                                                    $mat+=$f->cost == null ? ($f->rawproducts->cost * $f->quantity) : $f->cost;
                                                                }
                                                                if($f->rawproducts->p_type == 'Packaging')
                                                                {
                                                                    $pkg+=$f->cost == null ? ($f->rawproducts->cost * $f->quantity) : $f->cost;
                                                                }
                                                                if($f->rawproducts->p_type == 'Finished')
                                                                {
                                                                    $fnsh+=$f->cost == null ? ($f->rawproducts->cost * $f->quantity) : $f->cost;
                                                                }
                                                            }
                                                            else {
                                                                if($f->rawproducts2->product->p_type == 'Raw')
                                                                {
                                                                    $raw+=$f->cost == null ? ($f->rawproducts2->product->cost * $f->quantity) : $f->cost;
                                                                }
                                                                if($f->rawproducts2->product->p_type == 'Material')
                                                                {
                                                                    $mat+=$f->cost == null ? ($f->rawproducts2->product->cost * $f->quantity) : $f->cost;
                                                                }
                                                                if($f->rawproducts2->product->p_type == 'Packaging')
                                                                {
                                                                    $pkg+=$f->cost == null ? ($f->rawproducts2->product->cost * $f->quantity) : $f->cost;
                                                                }
                                                                if($f->rawproducts2->product->p_type == 'Finished')
                                                                {
                                                                    $fnsh+=$f->cost == null ? ($f->rawproducts2->product->cost * $f->quantity) : $f->cost;
                                                                }
                                                            }
                                                            $a++;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-outline">
                                            <label>Raw</label>
                                            <input type="text" readonly class="form-control" id="rawCost" value="{{$isEdit ? $raw : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-outline">
                                            <label>Material</label>
                                            <input type="text" readonly class="form-control" id="matCost" value="{{$isEdit ? $mat : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-outline">
                                            <label>Packaging</label>
                                            <input type="text" readonly class="form-control" id="pkgCost" value="{{$isEdit ? $pkg : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-outline">
                                            <label>Finished</label>
                                            <input type="text" readonly class="form-control" id="fnshCost" value="{{$isEdit ? $fnsh : ''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>TP* </label>
                                        <input type="text" value="{{$product->tp ?? old('tp')}}" placeholder="Enter TP" class="form-control" name="tp">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >MRP*</label>
                                        <input value="{{$product->mrp ?? old('mrp')}}" class="form-control" type="text" placeholder="Enter Product MRP" autocomplete="off" name="mrp" >
                                        <span class="text-danger">{{$errors->first('mrp') ? 'enter MRP' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Cost*</label>
                                        <input value="{{$isEdit ? (trim($product->cost) - ($product->otherCost == null ? '0' : trim($product->otherCost))) : old('cost')}}" class="form-control" type="text" placeholder="Enter Product Cost" name="cost" {{$isEdit ? ($product->mstatus == 'Yes' ? "id=cost" : '') : ''}} >
                                        <span class="text-danger">{{$errors->first('cost') ? 'enter cost' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Cost Type <small>(optional)</small></label>
                                        <select style="overflow-y: scroll; height: 20px;"  class="form-control selectpicker" data-live-search="true" name="ct_id" id="ct_id" >
                                            <option value="" disabled selected>Select...</option>
                                            <option value="No Cost">No Cost</option>
                                            @if ($isEdit)
                                                @foreach ($ct as $u)
                                                    <option {{$product->ct_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->cost_type}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($ct as $u)
                                                    <option  value="{{$u->id}}" {{old('ct_id') == $u->id ? 'selected' : null}}>{{$u->cost_type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" {{$isEdit ? ($product->ct_id == null ? 'hidden' : '') : 'hidden'}} id="others">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Additional Cost</label>
                                        <input type="text" class="form-control" value="{{$product->otherCost ?? old('otherCost')}}" name="otherCost">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Description <small>(optional)</small></label>
                                        <input value="{{$product->description ?? old('description')}}" class="form-control" type="text" placeholder="Enter Product Description" name="description">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Alert Quantity*</label>
                                        <input min="1" value="{{$product->alert_quantity ?? old('alert_quantity')}}" class="form-control" type="text" placeholder="Enter Product Alert Quantity" name="alert_quantity" >
                                        <span class="text-danger">{{$errors->first('alert_quantity') ? 'enter alert quantity' : null}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Brand',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Brand*</label>
                                        <select style="overflow-y: scroll; height: 20px;"  class="form-control selectpicker" data-live-search="true" name="brand_id" id="brand_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($brands as $u)
                                                    <option {{$product->brand_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->b_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($brands as $u)
                                                    <option  value="{{$u->id}}" {{old('brand_id') == $u->id ? 'selected' : null}}>{{$u->b_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('brand_id') ? 'select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top:23px;  width:50px;">
                                            <a href="#"  data-toggle="modal" data-target="#brandModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Category <small>(optional)</small></label>
                                        <select class="form-control selectpicker" data-live-search="true" name="cat_id" id="cat_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($cat as $u)
                                                    <option {{$product->cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cat as $u)
                                                    <option  value="{{$u->id}}" {{old('cat_id') == $u->id ? 'selected' : null}}>{{$u->cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('cat_id') ? 'select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#catModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Sub Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Sub Category <small>(optional)</small></label>
                                        <select class="form-control selectpicker" data-live-search="true" name="s_cat_id" id="s_cat_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($sub as $u)
                                                    <option {{$product->s_cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($sub as $u)
                                                    <option  value="{{$u->id}}" {{old('s_cat_id') == $u->id ? 'selected' : null}}>{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('s_cat_id') ? 'select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#" id="subcat" data-toggle="modal" data-target="#subModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Visible to POS*</label>
                                        <select class="form-control selectpicker" name="visibility" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->visibility == '1' ? 'selected' : null }} value="1">Yes</option>
                                            <option {{$product->visibility == '0' ? 'selected' : null }} value="0">No</option>
                                            @else
                                            <option value="1" {{old('visibility') == '1' ? 'selected' : null}}>Yes</option>
                                            <option value="0" {{old('visibility') == '0' ? 'selected' : null}}>No</option>

                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('visibility') ? 'select anyone' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <button class="btn btn-info variant" type="button">Add Variant</button>
                                    </div>
                                </div>
                            </div>
                            <div class="variants" id="variants">
                                @if ($isEdit)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach ($p_variant as $va)
                                        @php
                                            $name = explode('-',$va->name);
                                            $status = false;
                                        @endphp
                                        <br>
                                        <div class="row addvariants" id="{{$count}}" >
                                            @foreach($variant->where('p_id',0) as $v)
                                                <div class="col-sm-3">
                                                    <div class="form-outline">
                                                        <label><b> {{$v->name}} </b>
                                                        </label>
                                                        <select name="variants[{{$count}}][]" data-live-search="true" class="form-control va selectpicker">
                                                            <option selected disabled> select</option>
                                                            @foreach($variant->where('p_id',$v->id) as $c)
                                                                @foreach ($name as $n)
                                                                    @if ($n == $c->name)
                                                                        @php
                                                                            $status = true;
                                                                        @endphp
                                                                    @break
                                                                    @else
                                                                        @php
                                                                            $status = false;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if ($status == true)
                                                                    <option selected>{{$c->name}}</option>
                                                                @else
                                                                    <option>{{$c->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            @endforeach
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> Cost </b>
                                                        </label>
                                                        <input type="text" class="form-control co" value="{{$va->cost}}" name="costv[{{$count}}]">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> MRP </b>
                                                        </label>
                                                        <input type="text" class="form-control pr" value="{{$va->price}}" name="pricev[{{$count}}]">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> TP </b>
                                                        </label>
                                                        <input type="text" class="form-control tpv" value="{{$va->tp}}" name="tpv[{{$count}}]">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="v_id[{{$count}}]" class="vid" value="{{$va->id}}">
                                                <div class="col-sm-2"> <button type="button" style="margin-top:23px" id="{{$count}}" class="btn red deletev d{{$count}}" ><i class="fa fa-trash"></i></button></div>
                                        </div>
                                        @php
                                            $count++;
                                        @endphp
                                    @endforeach
                                    {{-- {{dd($name)}} --}}
                                @endif
                            </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0" >
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
        @include('modals.unit')
        @include('modals.category')
        @include('modals.subcategory')
        @include('modals.brand')
        @include('modals.products')
@endsection


@section('custom-script')
@toastr_js
@toastr_render
    <script>
        $(document).ready(function(){ //Make script DOM ready
            $('#unit_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#unitModal').modal("show"); //Open Modal
            }
            if(opval == 'pieces')
            {
                $('#weight').attr('readonly',true);
                $('#weight').prop('required',false);
            }
            else
            {
                $('#weight').attr('readonly',false);
                $('#weight').prop('required',false);
            }
        });
            $('#cat_id').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                console.log(opval);
                if(opval=="other"){ //Compare it and if true
                    $('#catname').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#catModal').modal("show"); //Open Modal
                }
            });
            $('#s_cat_id').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                console.log(opval);
                if(opval=="other"){ //Compare it and if true
                    $('#subModal').modal("show"); //Open Modal
                }
            });
            $('#brand_id').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                console.log(opval);
                if(opval=="other"){ //Compare it and if true
                    $('#brandModal').modal("show"); //Open Modal
                }
            });

            $('#ct_id').change(function(){
                var value = $(this).val();
                if(value!= 'No Cost')
                {
                    $('#others').show();
                }
                else
                {
                    $('#others').hide();
                }
            });


            function readURL(input, number) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                        $('#imagePreview' + number).hide();
                        $('#imagePreview' + number).fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
                $("#imageUpload1").change(function () {
                    readURL(this, 1);
            });
        });

        $(document).on('click','#subcat',function(){
            $('#catname').addClass('selectpicker');
            $('#catname').selectpicker('render');
        })

        $(document).on('click','.variant',function(){
            var a = 0;
            var parentDiv = document.getElementById("variants");
            if (parentDiv.hasChildNodes())
            {
                var last = 0;
                $(".addvariants").each(function(){
                    last = ($(this).attr("id"));
                });
                a = +last + +1;
            }
            else
            {
                a = 1;
            }
            $('.variants').append(`<br><div class="row addvariants" id="`+a+`" >
                @foreach($variant->where('p_id',0) as $v)
                <div class="col-sm-2">
                    <div class="form-outline">
                        <label><b> {{$v->name}} </b>
                        </label>
                        <select name="variants[`+a+`][]" class="form-control selectpicker va v`+a+`" data-live-search="true">
                            <option selected disabled> select</option>
                            @foreach($variant->where('p_id',$v->id) as $c)
                            <option>{{$c->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @endforeach
                <div class="col-sm-2"><div class="form-outline">
                    <label><b> Cost </b>
                    </label>
                    <input type="text" class="form-control co c`+a+`" name="costv[`+a+`]">
                </div></div>
                <div class="col-sm-2"><div class="form-outline">
                    <label><b> MRP </b>
                    </label>
                    <input type="text" class="form-control pr p`+a+`" name="pricev[`+a+`]">
                </div></div>
                <div class="col-sm-2"><div class="form-outline">
                    <label><b> TP </b>
                    </label>
                    <input type="text" class="form-control tpv tp`+a+`" name="tpv[`+a+`]">
                </div></div>
                <div class="col-sm-2"> <button type="button" style="margin-top: 23px" id="`+a+`" class="btn red deletev d`+a+`" ><i class="fa fa-trash"></i></button></div>
                </div>

            `);
            $('.va').addClass('selectpicker');
            $('.va').selectpicker('render');
        });

        $(document).on('click','.deletev',function(){
            var id=$(this).attr('id');
            $(this).closest('.addvariants').remove();
            $('.addvariants').each(function(i){
                i=+i + +1;
                $(this).attr('id',i);
            });
            $(".co").each(function (i){
                i=+i + +1;
                $(this).attr('name','costv['+i+']');
            });
            $(".pr").each(function (i){
                i=+i + +1;
                $(this).attr('name','pricev['+i+']');
            });

            $(".tpv").each(function (i){
                i=+i + +1;
                $(this).attr('name','tpv['+i+']');
            });
            $(".vid").each(function (i){
                i=+i + +1;
                $(this).attr('name','v_id['+i+']');
            });
            $(".deletev").each(function (i){
                i=+i + +1;
                $(this).attr('id',i);
            });
            $('.addvariants').each(function(i){
                i=+i + +1;
                $('#'+i+' .va').attr('name','variants['+i+'][]');
            });
        });

        $(document).on('change','#barcode',function(){
            var val = $(this).val();
            var code =  Math.floor((Math.random() * 10000000) + 1);
            if(val == "Yes")
            {
                $('#pro_code').val('');
                $('#pro_code').attr('readonly',false);
            }
            else{
                $('#pro_code').val(code);
                $('#pro_code').attr('readonly',true);
            }
        });

    function Mstatus($this)
    {
        var id = $this;
        if(id == "Yes")
        {
            $('#manufactured').show();
            $('.variant').prop('disabled',true);
        }
        else
        {
            $('#manufactured').hide();
            $('.variant').prop('disabled',false);
            $('#cost').val('');
        }
    }
    var mstatus = $('#mstatus').val();
    if(mstatus != null){
        Mstatus(mstatus);
        }

    $(document).on('change','#mstatus',function(){
        var id = $(this).val();
        Mstatus(id);

    });
    var table = $('#example5').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{route("product.add.datatable")}}',
        "columns": [{
                "data": "",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    return row.id;
                },
            },
            {
                "data": "pro_code",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    return row.pro_code;
                },
            },
            {
                "data": "pro_name",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    return row.pro_name ;
                },
            },
            {
                "data": "weight",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if(row.weight == null)
                    {
                        return row.u_name;
                    }
                    else
                    {
                        return row.weight+``+row.u_name ;
                    }
                },
            },
            {
                "data": "b_name",
                "defaultContent": ""
            },
            {
                "data": "p_type",
                "defaultContent": ""
            },
            {
                "data": "id",
                "defaultContent": ""
            },
        ],
        "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            },
            {
                "targets": 0,
                "render": function (data, type, row, meta) {
                    return meta.row + 1;
                },
            },
            {
                "targets": -1,
                "render": function (data, type, row, meta) {
                    return ` <button type="button"  class="btn green add" id="add`+row.id+`">
                        <i class="fa fa-plus"></i>
                    </button>`;
                },
            },
        ],
    });



        $('#example5').on('click','.add',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).attr('id');
            id = id.substring(3);
            console.log(id);
            $.ajax({
                url:"{{url('')}}/product/add/show/"+id,
                method:"GET",
                error: function (request, error) {
                        alert("Variants status is not approved ");
                        },
                success:function(data){
                    console.log(data);
                    if(data.weight == 0)
                    {
                        weight='';
                    }
                    else
                    {
                        weight=data.weight;
                    }
                    if(data.vstatus == 0)
                    {
                        if($('#'+data.id).length && data.vstatus == 0)
                        {

                        }
                        else
                        {
                            $("#example").append("<tr><td><input type='hidden' class='p_type ptype"+rowCount+"' value='"+data.p_type+"'> <input type='hidden' class='unitss' name='unitss["+rowCount+"]' value='"+data.unit.u_name+"'><input name='p_id["+rowCount+"]' type='text' class='form-control pid' readonly value='"+data.id+"'></td><td>"+data.pro_code+" - "+data.pro_name+" - "+data.unit.u_name+" - "+data.brands.b_name+" - "+data.p_type+"</td></td><td><input type='text' class='form-control quantity qty"+rowCount+"' id='"+rowCount+"' name='quantity["+rowCount+"]' value='1'><input type='hidden' class='type' name='type["+rowCount+"]' value='0'></td><td><input type='text' class='form-control costperunit cpu"+rowCount+"' readonly id='"+data.id+"' name='costperunit["+rowCount+"]' value='"+data.cost+"'></td><td><input type='text' class='form-control costsf costf"+rowCount+"' id='"+data.id+"' readonly name='costf["+rowCount+"]' value='"+data.cost+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                            var total=0;
                            var rowCount1 = $('#example tr').length;
                            var a = rowCount1 - 1;
                            for (let i = 1; i < rowCount1; i++) {
                                total = +$('.costf'+i).val() + +total;
                            }
                                // total = total / a;
                            $('#cost').val(total.toFixed(2));
                            if(data.p_type == 'Raw')
                            {
                                var rawCost = $('#rawCost').val();
                                $('#rawCost').val((+rawCost + +data.cost).toFixed(2));
                            }
                            if(data.p_type == 'Material')
                            {
                                var matCost = $('#matCost').val();
                                $('#matCost').val((+matCost + +data.cost).toFixed(2));
                            }
                            if(data.p_type == 'Packaging')
                            {
                                var pkgCost = $('#pkgCost').val();
                                $('#pkgCost').val((+pkgCost + +data.cost).toFixed(2));
                            }
                            if(data.p_type == 'Finished')
                            {
                                var fnshCost = $('#fnshCost').val();
                                $('#fnshCost').val((+fnshCost + +data.cost).toFixed(2));
                            }
                            $("#productModal").modal('hide');
                        }
                    }
                    else
                    {
                        $('#v_id').empty();
                        $('#v_id').selectpicker('destroy');
                        $('#v_id').append(`<option disabled selected>Select..</option>`);
                        for (let index = 0; index < data.variants.length; index++) {
                            if(data.variants[index].status == 0)
                            {

                            }
                            else
                            {
                                $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                            }
                        }
                        $("#productModal").modal('hide');
                        $('#v_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });
        });

        $(document).on('change','#p_id',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).val();
            // console.log(id);
            $.ajax({
                url:"{{url('')}}/product/add/show/"+id,
                method:"GET",
                error: function (request, error) {
                            alert("Variants status is not approved ");
                        },
                success:function(data){
                    // console.log(data);
                    if(data.weight == 0)
                    {
                        weight='';
                    }
                    else
                    {
                        weight=data.weight;
                    }
                    if(data.vstatus == 0)
                    {
                        if($('#'+data.id).length && data.vstatus == 0)
                        {
                        }
                        else
                        {
                            $("#example").append("<tr><td><input type='hidden' class='p_type ptype"+rowCount+"' value='"+data.p_type+"'><input type='hidden' class='unitss' name='unitss["+rowCount+"]' value='"+data.unit.u_name+"'><input name='p_id["+rowCount+"]' type='text' class='form-control pid' readonly value='"+data.id+"'></td><td>"+data.pro_code+" - "+data.pro_name+" - "+data.unit.u_name+" - "+data.brands.b_name+" - "+data.p_type+"</td></td><td><input type='text' class='form-control quantity qty"+rowCount+"' id='"+rowCount+"' name='quantity["+rowCount+"]' value='1'><input type='hidden' class='type' name='type["+rowCount+"]' value='0'></td><td><input type='text' class='form-control costperunit cpu"+rowCount+"' readonly id='"+data.id+"' name='costperunit["+rowCount+"]' value='"+data.cost+"'></td><td><input type='text' class='form-control costsf costf"+rowCount+"' id='"+data.id+"' readonly name='costf["+rowCount+"]' value='"+data.cost+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                            var total=0;
                            var rowCount1 = $('#example tr').length;
                            var a = rowCount1 - 1;
                            for (let i = 1; i < rowCount1; i++) {
                                if($('.costf'+i).val() == null)
                                {
                                    cost = 0 ;
                                }
                                else
                                {
                                    cost = $('.costf'+i).val();
                                }
                                total = +cost + +total;
                            }
                                // total = total / a;
                            $('#cost').val((total.toFixed(2)));
                            if(data.p_type == 'Raw')
                            {
                                var rawCost = $('#rawCost').val();
                                $('#rawCost').val((+rawCost + +data.cost).toFixed(2));
                            }
                            if(data.p_type == 'Material')
                            {
                                var matCost = $('#matCost').val();
                                $('#matCost').val((+matCost + +data.cost).toFixed(2));
                            }
                            if(data.p_type == 'Packaging')
                            {
                                var pkgCost = $('#pkgCost').val();
                                $('#pkgCost').val((+pkgCost + +data.cost).toFixed(2));
                            }
                            if(data.p_type == 'Finished')
                            {
                                var fnshCost = $('#fnshCost').val();
                                $('#fnshCost').val((+fnshCost + +data.cost).toFixed(2));
                            }
                        }
                    }
                    else
                    {
                        $('#v_id').empty();
                        $('#v_id').selectpicker('destroy');
                        $('#v_id').append(`<option disabled selected>Select..</option>`);
                        for (let index = 0; index < data.variants.length; index++) {
                            if(data.variants[index].status == 0)
                            {

                            }
                            else
                            {
                                $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                            }
                        }
                        $('#v_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });
        });
        $(document).on('change','#v_id',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).val();
            var w_id = $('#w_id').val();
            $.ajax({
                url:"{{url('')}}/product/Pvariantsforfinishproduct/"+id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    // console.log(data);

                    if($('#'+data.id).length && data.vstatus == 1)
                    {

                    }
                    else
                    {
                        $("#example").append(`<tr><td><input type='hidden' class='p_type ptype`+rowCount+`' value='`+data.product.p_type+`'><input type='hidden' class='unitss' name='unitss[`+rowCount+`]' value='`+data.product.unit.u_name+`'><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'> </td><td>`+data.name+`</td><td><input type='text' class='form-control quantity qty`+rowCount+`' id='`+rowCount+`' name='quantity[`+rowCount+`]' value='1'><input type="hidden" name="type[`+rowCount+`]" value="1" class="type"></td><td><input type='text' readonly class='form-control costperunit cpu`+rowCount+`' id='`+data.id+`' name='costperunit[`+rowCount+`]' value='`+data.cost+`'></td><td><input type='text' readonly class='form-control costsf costf`+rowCount+`' id='`+data.id+`' name='costf[`+rowCount+`]' value='`+data.cost+`'></td><td><button type='button' id='`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
                        var total=0;
                        var rowCount1 = $('#example tr').length;
                        var a = rowCount1 - 1;
                        // console.log(a);
                        for (let i = 1; i < rowCount1; i++) {
                            // console.log($('.cost'+i).val());
                            total = +$('.costf'+i).val() + +total;
                        }
                            // total = total / a;
                        $('#cost').val(total.toFixed(2));
                        if(data.product.p_type == 'Raw')
                        {
                            var rawCost = $('#rawCost').val();
                            $('#rawCost').val((+rawCost + +data.cost).toFixed(2));
                        }
                        if(data.product.p_type == 'Material')
                        {
                            var matCost = $('#matCost').val();
                            $('#matCost').val((+matCost + +data.cost).toFixed(2));
                        }
                        if(data.product.p_type == 'Packaging')
                        {
                            var pkgCost = $('#pkgCost').val();
                            $('#pkgCost').val((+pkgCost + +data.cost).toFixed(2));
                        }
                        if(data.product.p_type == 'Finished')
                        {
                            var fnshCost = $('#fnshCost').val();
                            $('#fnshCost').val((+fnshCost + +data.cost).toFixed(2));
                        }
                    }
                }
            });
        });
        $('table').on('click', '.delete', function(e){
            var id=$(this).attr('id');
            // console.log(id);
            $(this).closest('tr').remove();
            $(".pid").each(function (i){
                i=+i + +1;
                $(this).attr('name','p_id['+i+']');
            });
            $(".p_type ").each(function (i){
                $(this).removeClass('ptype'+(i+2));
                $(this).addClass('ptype'+(i+1));
            });
            $(".costsf").each(function (i){
                $(this).removeClass('costf'+(i+2));
                $(this).addClass('costf'+(i+1));
                i=+i + +1;
                $(this).attr('name','costf['+i+']');
            });
            $(".costperunit").each(function (i){
                $(this).removeClass('cpu'+(i+2));
                $(this).addClass('cpu'+(i+1));
                i=+i + +1;
                $(this).attr('name','costperunit['+i+']');
            });
            $(".type").each(function (i){
                i=+i + +1;
                $(this).attr('name','type['+i+']');
            });
            $(".quantity").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
                $(this).removeClass('qty'+(i+2));
                $(this).addClass('qty'+(i+1));
                i=+i + +1;
                $(this).attr('name','quantity['+i+']');
            });

            $(".unitss").each(function (i){
                i=+i + +1;
                $(this).attr('name','unitss['+i+']');
            });
            $(".delete").each(function (i){
                i=+i + +1;
                $(this).attr('id',i);
            });
            var total=0;
            var rowCount1 = $('#example tr').length;
            var a = rowCount1 - 1;
            var rawCost = 0;
            var matCost = 0;
            var pkgCost = 0;
            var fnshCost = 0;
            for (let i = 1; i < rowCount1; i++) {
                if($('.costf'+i).val() == null)
                {
                    cost = 0 ;
                }
                else
                {
                    cost = $('.costf'+i).val();
                }
                var ptype = $('.ptype'+i).val();
                if(ptype == 'Raw')
                {
                    rawCost = +$('.costf'+i).val() + +rawCost;
                    $('#rawCost').val(rawCost.toFixed(2));
                }
                if(ptype == 'Material')
                {
                    matCost = +$('.costf'+i).val() + +matCost;
                    $('#matCost').val(matCost.toFixed(2));
                }
                if(ptype == 'Packaging')
                {
                    pkgCost = +$('.costf'+i).val() + +pkgCost;
                    $('#pkgCost').val(pkgCost.toFixed(2));
                }
                if(ptype == 'Finished')
                {
                    var fnshCost = +$('.costf'+i).val() + +fnshCost;
                    $('#fnshCost').val(fnshCost.toFixed(2));
                }
                total = +cost + +total;
            }
                // total = total / a;
                $('#cost').val((total.toFixed(2)));
        });

        $(document).on('keyup','.quantity',function(){
            var value = $(this).val();
            var id = $(this).attr('id');
            var cpu = $('.cpu'+id).val();
            var cost = cpu * value;
            $('.costf'+id).val(cost.toFixed(2));
            var total=0;
            var rowCount1 = $('#example tr').length;
            var a = rowCount1 - 1;
            var rawCost = 0;
            var matCost = 0;
            var pkgCost = 0;
            var fnshCost = 0;

            for (let i = 1; i < rowCount1; i++) {
                total = +$('.costf'+i).val() + +total;
                var ptype = $('.ptype'+i).val();
                if(ptype == 'Raw')
                {
                    rawCost = +$('.costf'+i).val() + +rawCost;
                    $('#rawCost').val(rawCost.toFixed(2));
                }
                if(ptype == 'Material')
                {
                    matCost = +$('.costf'+i).val() + +matCost;
                    $('#matCost').val(matCost.toFixed(2));
                }
                if(ptype == 'Packaging')
                {
                    pkgCost = +$('.costf'+i).val() + +pkgCost;
                    $('#pkgCost').val(pkgCost.toFixed(2));
                }
                if(ptype == 'Finished')
                {
                    var fnshCost = +$('.costf'+i).val() + +fnshCost;
                    $('#fnshCost').val(fnshCost.toFixed(2));
                }
            }
            console.log(total);

                // total = total / a;
            $('#cost').val(total.toFixed(2));
        });

    </script>

<script>
    $('#productForm').validate({
        rules: {
            pro_name: {
                required: true,
            },
            pro_code: {
                required: true,
            },
            p_type: {
                required: true,
            },
            unit_id: {
                required: true,
            },
            cost: {
                required: true,
            },
            mrp: {
                required: true,
            },
            tp: {
                required: true,
            },
            brand_id: {
                required: true,
            },
            alert_quantity: {
                required: true,
            },
            visibility: {
                required: true,
            },
            mstatus: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>

@endsection

