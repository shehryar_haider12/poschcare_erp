@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
    .cs {
    width: 1600px!important;
    padding: 0px;
}
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/product">Product</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-list-alt font-white"></i>View Products
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px"  href="{{route('product.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('product.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Product</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <input type="text" id="p_name"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" id="code"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Weight</label>
                                                            <input type="text" name="weight"  class="form-control" placeholder="Weight" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Brand Name</label>
                                                        <select class="form-control selectpicker" multiple data-live-search="true" id="b_name" name="b_name">
                                                            <option value="Not All">Unselect All</option>
                                                            <option value="All">Select All</option>
                                                            @foreach ($brands as $u)
                                                            <option  value="{{$u->b_name}}">{{$u->b_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category Name</label>
                                                            <select class="form-control selectpicker" multiple data-live-search="true" id="c_name" name="c_name">
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All"> All</option>
                                                                @foreach ($cat as $u)
                                                                <option  value="{{$u->cat_name}}">{{$u->cat_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Active Status</label>
                                                        <select name="status" multiple id="status" class="form-control selectpicker ">
                                                            <option value="Not All">Unselect All</option>
                                                            <option value="All"> All</option>
                                                            <option value="1">Active</option>
                                                            <option value="0">InActive</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Type</label>
                                                            <select name="ptype" id="ptype" multiple class="form-control selectpicker ">
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All"> All</option>
                                                                <option>Raw</option>
                                                                <option>Material</option>
                                                                <option>Packaging</option>
                                                                <option>Finished</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Date</label>
                                                            <input type="date" id="date"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1400px;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:50px;">S.No</th>
                                                            <th style="width:70px;">Code</th>
                                                            <th style="width:90px;">Name</th>
                                                            <th style="width:100px;">Type</th>
                                                            <th style="width:90px;">Brand</th>
                                                            <th style="width:50px;">Category</th>
                                                            <th style="width:50px;">Date</th>
                                                            <th style="width:70px;">Weight/Unit</th>
                                                            <th style="width:50px;">Cost</th>
                                                            <th style="width:50px;">TP</th>
                                                            <th style="width:50px;">MRP</th>
                                                            <th style="width:5px;">Quantity</th>
                                                            <th style="width:150px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th style="width:50px;"></th>
                                                            <th style="width:70px;"></th>
                                                            <th style="width:90px;"></th>
                                                            <th style="width:100px;"></th>
                                                            <th style="width:90px;"></th>
                                                            <th style="width:50px;"></th>
                                                            <th style="width:50px;"></th>
                                                            <th style="width:70px;">Total</th>
                                                            <th style="width:50px;"></th>
                                                            <th style="width:50px;"></th>
                                                            <th style="width:50px;"></th>
                                                            <th style="width:5px;"></th>
                                                            <th style="width:150px;"></th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="Purchase" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Purchase History</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Order No</th>
                                            <th>Order Date</th>
                                            <th>Received Quantity</th>
                                            <th>Cost</th>
                                            <th>Warehouse</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <div id="variants" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Product Variants</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example5" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Product Name</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="Sale" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Sales History</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Sale No</th>
                                            <th>Sale Date</th>
                                            <th>Quantity</th>
                                            <th>Cost</th>
                                            <th>Warehouse</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="Product" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Product Detail</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example3" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Product Name</th>
                                            <th>Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Product</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="pro_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Code</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="pro_code" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Weight</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Weight" id="weight" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Unit</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="unit" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Cost</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Cost" id="cost" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >TP</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Code" id="price" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Quantity</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Quantity" id="quantity" readonly>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Alert Quantity</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Alert Quantity" id="alert_quantity" readonly>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="row"> --}}
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Brand</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Brand" id="brand" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Category</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Category" id="cat" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sub Category</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Sub Category" id="sub" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >MRP</label>
                                    <input class="form-control" type="text" placeholder="Enter Product MRP" id="mrp" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row variant">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
@toastr_render
        {{-- view one product --}}
<script>
    $(document).on('click','.view',function(){
        var id=$(this).attr("id");
        $.ajax({
            url:"{{url('')}}/product/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#pro_name').val(data.pro_name);
                $('#pro_code').val(data.pro_code);
                $('#weight').val(data.weight);
                $('#unit').val(data.unit.u_name);
                $('#brand').val(data.brands.b_name);
                $('#cat').val(data.category.cat_name);
                $('#sub').val(data.subcategory.s_cat_name);
                // $('#ware').val(data.warehouse.w_name);
                $('#cost').val(data.cost);
                $('#mrp').val(data.mrp);
                $('#price').val(data.tp);
                $('#alert_quantity').val(data.alert_quantity);
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            order: [[ 0, "desc" ]],
            ajax: '{{route("product.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "pro_code",
                    "defaultContent": ""
                },
                {
                    "data": "pro_name",
                    "defaultContent": ""
                },
                {
                    "data": "p_type",
                    "defaultContent": ""
                },
                // {
                //     "data": "",
                //     "defaultContent": "",
                //     "render": function (data, type, row, meta) {
                //         if(row.variants.length==0)
                //         {
                //             return 'No Variants';
                //         }
                //         else
                //         {
                //             for (let i = 0; i < row.variants.length; i++) {
                //                 return row.variants[i].name;
                //             }
                //         }
                //     }
                // },
                {
                    "data": "brands.b_name",
                    "defaultContent": ""
                },
                {
                    "data": "category.cat_name",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": "",
                    "render": function (value) {
                        if (value === null) return "";
                        return moment(value).format('MM/DD/YYYY');
                    }
                },
                {
                    "data": "",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        if(row.weight == null)
                        {
                            return row.unit.u_name;
                        }
                        else
                        {
                            return row.weight+row.unit.u_name;
                        }
                    }
                },
                {
                    "data": "cost",
                    "defaultContent": ""
                },
                {
                    "data": "tp",
                    "defaultContent": ""
                },
                {
                    "data": "mrp",
                    "defaultContent": ""
                },
                {
                    "data": "total",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        if(row.total == null)
                        {
                            return `0`;
                        }
                        else
                        {
                            if(row.unit_total == null)
                            {
                                return row.total;
                            }
                            else
                            {
                                return row.total+`(`+row.unit_total+`)`;
                            }
                        }
                    }
                },
                {
                    "data": "status",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("product.edit",[":id"])}}';
                        edit = edit.replace(':id', row.id);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
                        @if(in_array('edit',$permissions))
                          <div class="row">
                            <a id="GFG" href="` + edit + `" class="text-info p-1">
                                <button type="button" class="btn blue edit" >
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>
                        @endif
                        @if(in_array('show',$permissions))
                            <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                <i class="icon-eye"></i>
                            </button>
                        @endif
                        @if(in_array('status',$permissions))
                            <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                            checked + ` value="` + row.id + `">
                        @endif
                            <select style="margin-top:5px; width: 89px;" class="form-control action" id="`+row.id +`" >
                                <option >Actions</option>
                                @if(in_array("product sale details",$permissions))
                                <option >Sale Details</option>
                                @endif
                                @if(in_array("product purchase details",$permissions))
                                <option >Purchase Details</option>
                                @endif
                                @if(in_array("product details",$permissions))
                                <option >Product Details</option>
                                @endif
                                <option >Product Variants</option>

                            </select>
                            </div>
                        `;
                    },
                },
            ],
            "footerCallback": function( tfoot, data, start, end, display ) {
                var totalcost = 0;
                var totalqty = 0;
                var totaltp = 0;
                var totalmrp = 0;
                for (var i = 0; i < data.length; i++) {
                    totalcost = totalcost + parseInt(data[i]['cost']);
                    totaltp = totaltp + parseInt(data[i]['tp']);
                    totalmrp = totalmrp + parseInt(data[i]['mrp']);
                    totalqty = totalqty + parseInt((data[i]['total'] == null ? '0' : parseInt(data[i]['total'])));
                }
                $( table.column( 8 ).footer() ).html(Number.isNaN(totalcost) ? '0' : totalcost);
                $( table.column( 9 ).footer() ).html(Number.isNaN(totaltp) ? '0' : totaltp);
                $( table.column( 10 ).footer() ).html(Number.isNaN(totalmrp) ? '0' : totalmrp);
                $( table.column( 11 ).footer() ).html(Number.isNaN(totalqty) ? '0' : totalqty);
            },
            "drawCallback": function (settings) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                if (elems) {
                elems.forEach(function (html) {
                    var switchery = new Switchery(html, {
                    color: '#007bff'
                    , secondaryColor: '#dfdfdf'
                    , jackColor: '#fff'
                    , jackSecondaryColor: null
                    , className: 'switchery'
                    , disabled: false
                    , disabledOpacity: 0.5
                    , speed: '0.1s'
                    , size: 'small'
                    });

                });
            }
            $('.status').change(function () {
                var $this = $(this);
                var id = $this.val();
                var status = this.checked;

                if (status) {
                    status = 1;
                } else {
                    status = 0;
                }
                // console.log(status);
                axios
                    .post('{{route("product.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    // location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                    });
                });

            },
        });
        $('#b_name').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#b_name option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#b_name').selectpicker('deselectAll');
                $('#b_name').selectpicker('refresh');
            }

            $.each($('#b_name option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(4).search(search, true, false).draw();

        });
        $('#c_name').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#c_name option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#c_name').selectpicker('deselectAll');
                $('#c_name').selectpicker('refresh');
            }
            $.each($('#c_name option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(5).search(search, true, false).draw();
        });
        $('#ptype').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#ptype option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#ptype').selectpicker('deselectAll');
                $('#ptype').selectpicker('refresh');
            }
            $.each($('#ptype option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(3).search(search, true, false).draw();
        });
        $('#date').on('change',function(){
            value = $("#date").val();
            table.columns(6).search(value, true, false).draw();
        });
        $('#status').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#status option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#status').selectpicker('deselectAll');
                $('#status').selectpicker('refresh');
            }
            $.each($('#status option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(12).search(search, true, false).draw();
        });
        $('#p_name').on('keyup',function(){
            value = $("#p_name").val();
            table.columns(2).search(value, true, false).draw();
        });
        $('#code').on('keyup',function(){
            value = $("#code").val();
            table.columns(1).search(value, true, false).draw();
        });
    });

    $(document).on('change','.action',function(){
        var val=$(this).val();
        if(val == 'Sale Details')
        {
            var id=$(this).attr("id");
            $("#example2 tbody").empty();
            $.ajax({
                url:"{{url('')}}/sales/product/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    for (let i = 0; i < data.length; i++) {
                        if(data[i].sale == null)
                        {

                        }
                        else
                        {
                            $("#example2").append("<tr><td>"+data[i].s_id+"</td><td>"+data[i].sale.sale_date+"</td><td>"+data[i].quantity+"</td><td>"+data[i].price+"</td><td>"+data[i].sale.warehouse.w_name+"</td></tr>");
                        }
                    }
                    $('#Sale').modal("show");
                    $('.action').val('Actions');
                    $('#example2').DataTable();

                }
            });
        }
        if(val == 'Purchase Details')
        {
            var id=$(this).attr("id");
            $("#example1 tbody").empty();
            $.ajax({
                url:"{{url('')}}/purchase/product/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    for (let i = 0; i < data.length; i++) {
                        if(data[i].purchase == null)
                        {
                            console.log('a'+i);
                        }
                        else
                        {
                            $("#example1").append("<tr><td>"+data[i].o_id+"</td><td>"+data[i].purchase.order_date+"</td><td>"+data[i].received_quantity+"</td><td>"+data[i].cost+"</td><td>"+data[i].purchase.warehouse.w_name+"</td></tr>");
                        }
                    }
                    $('#Purchase').modal("show");
                    $('.action').val('Actions');
                    $('#example1').DataTable();
                }
            });
        }
        if(val == 'Product Details')
        {
            var id=$(this).attr("id");
            $("#example3 tbody").empty();
            $.ajax({
                url:"{{url('')}}/product/detail/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    if(data.length == 0)
                    {
                        $("#example3 tbody").empty();
                        $('#Product').modal("show");
                        $('.action').val('Actions');
                    }
                    else
                    {
                        var a = 1;
                        for (let i = 0; i < data.length; i++) {
                            if(data[i].type == 0)
                            {
                                $("#example3").append("<tr><td>"+a+"</td><td>"+data[i].rawproducts.pro_name+"</td><td>"+data[i].quantity+"</td></tr>");
                            }

                            else
                            {
                                $("#example3").append("<tr><td>"+a+"</td><td>"+data[i].rawproducts2.name+"</td><td>"+data[i].quantity+"</td></tr>");
                            }
                            a +=1;
                        }
                        $('#Product').modal("show");
                        $('.action').val('Actions');
                    }
                }
            });
        }


        if(val == 'Product Variants')
        {
            var id=$(this).attr("id");
            $("#example5 tbody").empty();
            $.ajax({
                url:"{{url('')}}/product/variants/"+id,
                method:"GET",
                error: function (request, error) {
                    alert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    if(data == null)
                    {

                    }
                    else
                    {
                        var a = 1;
                        for (let i = 0; i < data.length; i++) {
                            var checked = data[i].status == 1 ? 'checked' : null;
                            $("#example5").append(`<tr><td>`+a+`</td>
                                <td>`+data[i].name+`</td>
                                <td><input class="variant_status" type="checkbox"  data-plugin="switchery" `+checked+` data-color="#005CA3" data-size="small" value="` +data[i].id+ `"></td>
                                </tr>`);
                            a = +a + +1;
                        }
                        $('#variants').modal("show");
                        $('.action').val('Actions');
                        $('#example5').DataTable();
                    }

                }
            });
        }
    });


$(document).on('change','.variant_status',function () {
    // alert('dfsd');
    var $this = $(this);
    var id = $this.val();
    var status = this.checked;
    if (status) {
        status = 1;
    } else {
        status = 0;
    }
    axios
        .post('{{route("product.variants.status")}}', {
        _token: '{{csrf_token()}}',
        _method: 'post',
        id: id,
        status: status,
        })
        .then(function (responsive) {
        console.log('responsive');
        // location.reload();
        })
        .catch(function (error) {
        console.log(error);
        });
});





</script>
    @endsection
@endsection
