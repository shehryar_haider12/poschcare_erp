

<div id="personModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Sale Person</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
                    @php
                        $user=App\User::max('id');
                    @endphp

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <input type="hidden" id="u_ids" name="u_id" value="{{ $user}}">
                                <label >Sale Person Name*</label>
                                <input value="{{old('name')}}" class="form-control nameSP" type="text" placeholder="Enter Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select id="citySP" class="form-control selectpicker " data-live-search="true" name="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="v_type" class="v_typeSP" value="Saleperson" id="">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address*</label>
                                <input value="{{ old('address')}}" class="form-control addressSP" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No*</label>
                                <input min="0" value="{{old('c_no')}}" class="form-control c_noSP" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email*</label>
                                <input value="{{old('email')}}" required class="form-control emailSP" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Password*</label>
                                <input value="" class="form-control passwordSP" required type="password" placeholder="Enter Password" name="password" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_typeSP" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitSaleperson" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitSaleperson').click(function(e){
        e.preventDefault();
        var name = $('.nameSP').val();
        var c_id = $('#citySP').val();
        var address = $('.addressSP').val();
        var c_no = $('.c_noSP').val();
        var email = $('.emailSP').val();
        var password = $('.passwordSP').val();
        var request_type = $('.request_typeSP').val();
        var v_type = $('.v_typeSP').val();
        var u_id = $('#u_ids').val();
        if(name == '' || p_type == '' || address == '' || c_no == '' || email == '' || password == '' || commission == '')
        {
            alert('Fill all the required fields');
        }
        else
        {
            axios
            .post('{{route("saleperson.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    address:address,
                    c_no:c_no,
                    email:email,
                    c_id:c_id,
                    u_id:u_id,
                    v_type:v_type,
                    commission:commission,
                    password:password,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    if(responsive.data.saleperson == null)
                    {
                        alert(responsive.data.message);
                    }
                    else
                    {
                        $('#personModal').modal('hide');
                        $('#sp_id').append(`<option selected value="`+responsive.data.saleperson.id+`">`+responsive.data.saleperson.name+`</option>`);
                    }
                })
                .catch(function (error) {
                    alert(error);
            });
        }

    });
</script>
