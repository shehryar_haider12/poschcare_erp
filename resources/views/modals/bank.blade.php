

<div id="bankModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Bank</h4>
            </div>
            <form action="{{url('')}}/bank" class="form-horizontal" method="POST" >
                @csrf
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Bank Name*</label>
                                <input value="{{old('name')}}" class="form-control" type="text" placeholder="Enter Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select id="c_id" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address*</label>
                                <input value="{{old('address')}}" class="form-control" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No <small>(optional)</small></label>
                                <input min="0" value="{{old('c_no')}}" class="form-control" type="number" placeholder="Enter Contact Number" name="c_no" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Branch*</label>
                                <input value="{{ old('branch')}}" required class="form-control" type="text" placeholder="Enter Branch" name="branch" >
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
