

<div id="customerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Customer</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name*</label>
                                <input autocomplete="off" class="form-control companyC" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select class="form-control selectpicker " data-live-search="true" name="c_id" id="cityC" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="v_type" class="v_typeC" value="Customer" id="">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Group*</label>
                                <select required class="form-control selectpicker" id="c_groupC" name="c_group" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($cgroup as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Price Group*</label>
                                <select required class="form-control selectpicker " id="p_groupC" name="p_group" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($pgroup as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address*</label>
                                <input class="form-control addressC" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control countryC" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Name*</label>
                                <input autocomplete="off" class="form-control nameC" type="text" placeholder="Enter Customer Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No*</label>
                                <input autocomplete="off" min="0" class="form-control c_noC" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Name 2<small>(optional)</small></label>
                                <input class="form-control name2" type="text" placeholder="Enter Customer Name" name="name2" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No 2<small>(optional)</small></label>
                                <input class="form-control c_no2" type="text" placeholder="Enter Contact Number" name="c_no2" >
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control VATC" type="text" placeholder="Enter NTN Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control GSTC" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State  <small>(optional)</small></label>
                                <input class="form-control stateC" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control emailC" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control postalCodeC" type="number" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Balance <small>(optional)</small></label>
                                <input  class="form-control balanceC" type="number" placeholder="Enter Balance" name="balance" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Opening Balance <small>(optional)</small></label>
                                <input autocomplete="off" class="form-control opening_balance" type="text" placeholder="Enter Opening Balance" name="opening_balance" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="request_typeC" class="request_typeC" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitCustomer" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitCustomer').click(function(e){
        e.preventDefault();
        var company = $('.companyC').val();
        var c_id = $('#cityC').val();
        var c_group = $('#c_groupC').val();
        var p_group = $('#p_groupC').val();
        var address = $('.addressC').val();
        var country = $('.countryC').val();
        var name = $('.nameC').val();
        var c_no = $('.c_noC').val();
        var name2 = $('.name2').val();
        var c_no2 = $('.c_no2').val();
        var VAT = $('.VATC').val();
        var GST = $('.GSTC').val();
        var state = $('.stateC').val();
        var email = $('.emailC').val();
        var postalCode = $('.postalCodeC').val();
        var v_type = $('.v_typeC').val();
        var balance = $('.balanceC').val();
        var opening_balance = $('.opening_balance').val();
        var request_type = $('.request_typeC').val();

        if(company == '' || c_group == '' || p_group == '' || address =='' || name == '' || c_no == '')
        {
            alert('Fill all the required fields');
        }
        else
        {

            axios
            .post('{{route("customer.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    c_id:c_id,
                    company:company,
                    address:address,
                    c_no:c_no,
                    country:country,
                    VAT:VAT,
                    GST:GST,
                    state:state,
                    email:email,
                    postalCode:postalCode,
                    v_type:v_type,
                    c_group:c_group,
                    p_group:p_group,
                    name2:name2,
                    c_no2:c_no2,
                    balance:balance,
                    opening_balance:opening_balance,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    if(responsive.data.ven == null)
                    {
                        alert(responsive.data.message);
                    }
                    else
                    {
                        $('#s_address').val(responsive.data.ven.address);
                        $('#customerModal').modal('hide');
                        $('#cm_id').selectpicker('destroy');
                        $('#cm_id').append(`<option selected value="`+responsive.data.ven.id+`">`+responsive.data.ven.name+`</option>`);
                        $('#cm_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                })
                .catch(function (error) {
                    alert(error);
            });
        }

    });
</script>
