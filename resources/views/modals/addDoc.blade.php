<div id="addDoc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  style="width: 70%;">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Attach Document</h4>
            </div>
            <form action="{{url('')}}/addDoc" class="form-horizontal" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label for="">Document</label>
                            <input type="hidden" name="s_id" id="s_id">
                            <input type="file" class="form-control" name='document'>
                        </div>
                    </div>

                </div>
            <button type="submit" class="btn green">Submit</button>
        </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
