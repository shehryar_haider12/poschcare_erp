

<div id="cityModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">City</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City Name*</label>
                                <input class="form-control c_name" type="text" placeholder="Enter City Name" name="c_name" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitCity" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $('#submitCity').click(function(e){
        e.preventDefault();
        var c_name = $('.c_name').val();
        var request_type = $('.request_type').val();
        if(c_name == '')
        {
            alert('Enter City Name');
        }
        else
        {

            axios
            .post('{{route("city.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    c_name:c_name,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#cityModal').modal('hide');
                    $('#c_id').selectpicker('destroy');
                    $('#c_id').append(`<option selected value="`+responsive.data.city.id+`">`+responsive.data.city.c_name+`</option>`);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
