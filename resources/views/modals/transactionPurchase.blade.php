

<div id="transactionPurchase" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">View Payment History</h4>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table id="example2" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="5%">Invoice No</th>
                                    {{-- <th width="10%">Payment Type</th> --}}
                                    <th>Date</th>
                                    {{-- <th width="10%">Transaction Type</th> --}}
                                    <th width="7%">Paid By</th>
                                    {{-- <th>Ref.No</th> --}}
                                    <th>Paid Amount</th>
                                    <th>Action</th>
                                    {{-- <th>Cheque No</th> --}}
                                    {{-- <th >Credit Card #</th> --}}
                                    {{-- <th>Gift Card #</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
