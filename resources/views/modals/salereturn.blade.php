<div id="return" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  style="width: 70%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Sale Order Return</h4>
            </div>
            <form action="{{url('')}}/sales/return" class="form-horizontal" method="POST" >
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label >Sale Numer</label>
                            <input readonly class="form-control" type="text" placeholder="Enter Product Name" name="s_id" id="s_id2">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="table-responsive">
                        <table id="example8" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width='5%'>S.No</th>
                                    <th width='30%'>Name - Code</th>
                                    <th width='5%'>Sale Quantity</th>
                                    <th width='5%'>Delivered Quantity</th>
                                    <th width='5%'>Return Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>


                        </table>
                    </div>
                        <input type="hidden" name="w_id" id="w_id2">
                    </div>
                </div>
                <br>
            <button type="submit" id="returned" class="btn green">Submit</button>
        </div>

            {{-- <div class="row">
                <div class="col-md-offset-0 col-md-12">
                </div>
            </div> --}}
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
