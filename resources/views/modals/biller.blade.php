

<div id="billerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Biller</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' name="document" id="imageUpload1" placeholder="add image"
                                    accept=".png, .jpg, .jpeg" class="document"/>
                                    <label for="imageUpload1"><span>Featured Image* </span></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview1"
                                        style="background-image : url({{url('').'/uploads/placeholder.jpg'}})">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Biller Name*</label>
                                <input class="form-control nameB" type="text" placeholder="Enter Biller Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City</label>
                                <select  id="cityB" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                    <option value="" selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="v_type" class="vtypeB" value="Biller" id="">

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name*</label>
                                <input class="form-control companyB" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address*</label>
                                <input  class="form-control addressB" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No*</label>
                                <input class="form-control c_noB" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control countryB" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control VATB" type="text" placeholder="Enter NTN Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control GSTB" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State <small>(optional)</small></label>
                                <input class="form-control stateB" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control emailB" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control postalCodeB" type="text" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="request_typeb" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitBiller" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
            $("#imageUpload1").change(function () {
                readURL(this, 1);
        });
    })

    $('#submitBiller').click(function(e){
        e.preventDefault();
        var name = $('.nameB').val();
        var c_id = $('#cityB').val();
        var company = $('.companyB').val();
        var address = $('.addressB').val();
        var c_no = $('.c_noB').val();
        var country = $('.countryB').val();
        var VAT = $('.VATB').val();
        var GST = $('.GSTB').val();
        var state = $('.stateB').val();
        var email = $('.emailB').val();
        var document = $('.document').val();
        var postalCode = $('.postalCodeB').val();
        var v_type = $('.vtypeB').val();
        var request_type = $('#request_typeb').val();

        if(name == '' || company == '' || address == '' || c_no == '' || document == '')
        {
            alert('Fill all the required fields');
        }
        else
        {
            var formData 	= new FormData();
            var attachment = '';

            formData.append("_token", "{{csrf_token()}}");
            attachment = $('.document');
            console.log(attachment.files[0]);
            if(attachment.files[0]){
                formData.append('document',attachment.files[0]);
            }
            axios
            .post('{{route("biller.store")}}', formData, {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    }
                })
                .then(function (responsive) {
                    $('#billerModal').modal('hide');
                    $('#b_id').selectpicker('destroy');
                    $('#b_id').append(`<option selected value="`+responsive.data.biller.id+`">`+responsive.data.biller.name+`</option>`);
                    $('#b_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
