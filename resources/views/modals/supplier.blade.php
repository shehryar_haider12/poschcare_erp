

<div id="supplierModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Supplier</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Supplier Name*</label>
                                <input class="form-control nameS" type="text" placeholder="Enter Supplier Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City</label>
                                <select  id="cityS" class="form-control selectpicker " data-live-search="true" name="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- <div class="col-sm-1">
                            <div class="form-control" style="margin-top: 24px; margin-left:-20px;  width:50px">
                                <a href="#"  data-toggle="modal" data-target="#cityModal">
                                    <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                </a>
                            </div>
                        </div> --}}
                        <input type="hidden" name="v_type" class="vtypeS" value="Supplier" id="">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name*</label>
                                <input class="form-control companyS" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address*</label>
                                <input class="form-control addressS" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No*</label>
                                <input class="form-control c_noS" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control countryS" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control VATS" type="text" placeholder="Enter NTN Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control GSTS" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State <small>(optional)</small></label>
                                <input class="form-control stateS" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control emailS" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control postalCodeS" type="text" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden"  name="request_typeS" class="request_typeS" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitSupplier" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $('#submitSupplier').click(function(e){
        e.preventDefault();
        var name = $('.nameS').val();
        var c_id = $('#cityS').val();
        var company = $('.companyS').val();
        var address = $('.addressS').val();
        var c_no = $('.c_noS').val();
        var country = $('.countryS').val();
        var VAT = $('.VATS').val();
        var GST = $('.GSTS').val();
        var state = $('.stateS').val();
        var email = $('.emailS').val();
        var postalCode = $('.postalCodeS').val();
        var v_type = $('.vtypeS').val();
        var request_type = $('.request_typeS').val();
         axios
        .post('{{route("supplier.store")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                name:name,
                c_id:c_id,
                company:company,
                address:address,
                c_no:c_no,
                country:country,
                VAT:VAT,
                GST:GST,
                state:state,
                email:email,
                postalCode:postalCode,
                v_type:v_type,
                request_type:request_type,
            })
            .then(function (responsive) {
                if(responsive.data.supplier == null)
                {
                    alert(responsive.data.message);
                }
                else
                {
                    $('#supplierModal').modal('hide');
                    $('#s_id').selectpicker('destroy');
                    $('#s_id').append(`<option selected value="`+responsive.data.supplier.id+`">`+responsive.data.supplier.name+`</option>`);
                    $('#s_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
            })
            .catch(function (error) {
                alert(error);
        });
    });
</script>
