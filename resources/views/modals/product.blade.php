

<div id="productModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Product</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Name*</label>
                                <input  class="form-control pro_name" type="text" placeholder="Enter Product Name" name="pro_name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Code*</label>
                                <input  class="form-control" id="pro_code" type="text" placeholder="Enter Product Code" name="pro_code" required>
                                <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label>Product Type*</label>
                                <select class="form-control selectpicker" name="p_type" id="p_type">
                                <option value="" disabled selected>Select...</option>
                                <option>Raw  {{old('p_type') == 'Raw' ? 'selected' : null}}</option>
                                <option>Material  {{old('p_type') == 'Material' ? 'selected' : null}}</option>
                                <option>Packaging {{old('p_type') == 'Packaging' ? 'selected' : null}}</option>
                                <option>Finished {{old('p_type') == 'Finished' ? 'selected' : null}}</option>

                            </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Weight <small>(optional)</small></label>
                                <input min="1" class="form-control weight" type="text" placeholder="Enter Product Weight" name="weight">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Unit*</label>
                                <select class="form-control selectpicker" data-live-search="true" name="unit_id" required id="unit_id">
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($unit as $u)
                                    <option  value="{{$u->u_name}}">{{$u->u_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" name="mstatus" value="No" class="mstatus">
                            <div class="form-outline">
                                <label >Description <small>(optional)</small></label>
                                <input class="form-control description" type="text" placeholder="Enter Product Description" name="description">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label>TP* </label>
                                <input type="text" value="{{old('tp')}}" placeholder="Enter TP" class="form-control tp" name="tp">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >MRP*</label>
                                <input min="1" value="{{old('mrp')}}" class="form-control mrp" type="text" placeholder="Enter Product MRP" autocomplete="off" name="mrp" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Cost*</label>
                                <input min="1" class="form-control cost" type="text" placeholder="Enter Product Cost" name="cost" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label>Cost Type <small>(optional)</small></label>
                                <select style="overflow-y: scroll; height: 20px;"  class="form-control selectpicker" data-live-search="true" name="ct_id" id="costtype" >
                                    <option value="" disabled selected>Select...</option>
                                    <option value="No Cost">No Cost</option>
                                    @foreach ($ct as $u)
                                        <option  value="{{$u->id}}" {{old('ct_id') == $u->id ? 'selected' : null}}>{{$u->cost_type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" hidden id="others">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label>Additional Cost</label>
                                <input type="text" class="form-control otherCost" value="{{ old('otherCost')}}" name="otherCost">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Alert Quantity*</label>
                                <input min="1" class="form-control alert_quantity" type="text" placeholder="Enter Product Alert Quantity" name="alert_quantity" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Brand*</label>
                                <select class="form-control selectpicker" data-live-search="true" name="brand_id" id="brand_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($brands as $u)
                                    <option  value="{{$u->id}}">{{$u->b_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Category</label>
                                <select class="form-control selectpicker" data-live-search="true" name="cat_id" id="cat_id" >
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($cat as $u)
                                    <option  value="{{$u->id}}">{{$u->cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Sub Category</label>
                                <select class="form-control selectpicker" data-live-search="true" name="s_cat_id" id="s_cat_id" >
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($sub as $u)
                                    <option  value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Visible to POS*</label>
                                <select class="form-control selectpicker" name="visibility" id="visibility" >
                                    <option value="" disabled selected>Select...</option>
                                    <option value="1" {{old('visibility') == '1' ? 'selected' : null}}>Yes</option>
                                    <option value="0" {{old('visibility') == '0' ? 'selected' : null}}>No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="request_typeP" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitProduct" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#costtype').change(function(){
        var value = $(this).val();
        if(value!= 'No Cost')
        {
            $('#others').show();
        }
        else
        {
            $('#others').hide();
        }
    });

    $('#submitProduct').click(function(e){
        e.preventDefault();
        var pro_code = $('#pro_code').val();
        var pro_name = $('.pro_name').val();
        var p_type = $('#p_type').val();
        var weight = $('.weight').val();
        var unit_id = $('#unit_id').val();
        var description = $('.description').val();
        var tp = $('.tp').val();
        var mrp = $('.mrp').val();
        var cost = $('.cost').val();
        var ct_id = $('#costtype').val();
        var otherCost = $('.otherCost').val();
        var mstatus = $('.mstatus').val();
        var alert_quantity = $('.alert_quantity').val();
        var brand_id = $('#brand_id').val();
        var cat_id = $('#cat_id').val();
        var s_cat_id = $('#s_cat_id').val();
        var visibility = $('#visibility').val();
        var request_type = $('#request_typeP').val();
        console.log(unit_id);

        if(pro_code == '' || pro_name == '' || p_type == '' || unit_id == '' || tp == '' || mrp == '' || cost == '' || alert_quantity == '' || brand_id == '' )
        {
            alert('Fill all the required fields');
        }
        else
        {

            axios
            .post('{{route("product.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    pro_code: pro_code,
                    pro_name:pro_name,
                    p_type:p_type,
                    weight:weight,
                    unit_id:unit_id,
                    description:description,
                    tp:tp,
                    mrp:mrp,
                    cost:cost,
                    ct_id:ct_id,
                    otherCost:otherCost,
                    mstatus:mstatus,
                    alert_quantity:alert_quantity,
                    brand_id:brand_id,
                    cat_id:cat_id,
                    s_cat_id:s_cat_id,
                    visibility:visibility,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#productModal').modal('hide');
                    $('#p_id').selectpicker('destroy');
                    $('#p_id').append(`<option value="`+responsive.data.p.id+`">`+responsive.data.p.pro_code+` - `+responsive.data.p.pro_name+`</option>`);
                    $('#p_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });

</script>
