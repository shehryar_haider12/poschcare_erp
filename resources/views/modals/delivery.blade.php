<div id="delivery" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  style="width: 70%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Sale Order Delivery</h4>
            </div>
            <form action="{{url('')}}/sales/delivery" class="form-horizontal" method="POST" >
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label for="">Invoice Format</label>
                            <input type="text" id="IF" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label for="">Invoice Type</label>
                            <input type="text" id="IT" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label >Sale Numer</label>
                            <input readonly class="form-control" type="text" placeholder="Enter Product Name" name="s_id" id="s_id1">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label >Delivery Status*</label>
                            <select class="form-control selectpicker" name="s_status" id="s_status" required>
                                <option value="" disabled selected>Select...</option>
                                <option value="Partial">Partial</option>
                                <option value="Complete">Complete</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="table-responsive">

                            <table id="example3" class="table table-striped table-bordered">



                            </table>
                        </div>
                        <input type="hidden" name="w_id" id="w_id1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-outline">
                            <label >Amount*</label>
                            <input  class="form-control" readonly type="text" min="0" id="p_total1" name="p_total1">
                        </div>
                    </div>
                </div>
                <br>
            <button type="submit" id="delivered" class="btn green">Submit</button>
        </div>

            {{-- <div class="row">
                <div class="col-md-offset-0 col-md-12">
                </div>
            </div> --}}
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).on('change','#s_status',function(){
        var val = $(this).val();
        if(val == 'Partial')
        {
            var total = 0;
            $('.rcv').val('1');
            $('.rcv').prop('readonly',false);
            var rowCount1 = $('#example3 tr').length;
            for (let i = 2; i <= rowCount1; i++) {
                total = +($('.subtotal'+i).val() * 1) + +total;
            }
            $('#p_total1').attr('max',total.toFixed(2));
            $('#p_total1').val(total.toFixed(2));
        }
        else
        {
            var total = 0;
            var rowCount1 = $('#example3 tr').length;
            for (let i = 2; i <= rowCount1; i++) {
                max= $('.q'+i).val() - $('.dq'+i).val();
                $('.quantity'+i).val(max);
                $('.quantity'+i).prop('readonly',true);
                total = +($('.subtotal'+i).val() * max) + +total;
                console.log(total);
            }
            $('#p_total1').attr('max',total.toFixed(2));
            $('#p_total1').val(total.toFixed(2));
        }
    })
</script>
