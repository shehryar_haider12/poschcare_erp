

<div id="brandModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Brand</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Brand Name*</label>
                                <input class="form-control b_name" type="text" placeholder="Enter Product Name" name="b_name" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Name</label>
                                <input value="{{old('c_p_name')}}" class="form-control c_p_name" type="text" placeholder="Enter Contact Person Name" name="c_p_name" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Number</label>
                                <input value="{{old('c_p_contactNo')}}" class="form-control c_p_contactNo" type="text" placeholder="Enter Contact Person Number" name="c_p_contactNo" >
                            </div>
                            <span class="text-danger">{{$errors->first('c_p_contactNo') ? 'Contact number not be greater than 11 digits' : null}}</span>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitBrand" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $('#submitBrand').click(function(e){
        e.preventDefault();
        var b_name = $('.b_name').val();
        var c_p_name = $('.c_p_name').val();
        var c_p_contactNo = $('.c_p_contactNo').val();
        var request_type = $('.request_type').val();
        if(b_name == '')
        {
            alert('Enter Brand Name');
        }
        else
        {

            axios
                .post('{{route("brands.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    b_name:b_name,
                    c_p_contactNo:c_p_contactNo,
                    c_p_name:c_p_name,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#brandModal').modal('hide');
                    $('#brand_id').selectpicker('destroy');
                    $('#brand_id').append(`<option selected value="`+responsive.data.brands.id+`">`+responsive.data.brands.b_name+`</option>`);
                    $('#brand_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
