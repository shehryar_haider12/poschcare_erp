

<div id="variantsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Product Variants</h4>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table id="example5" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th >S.No</th>
                                    <th>Code - Name</th>
                                    <th>Weight</th>
                                    <th>Brand</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
