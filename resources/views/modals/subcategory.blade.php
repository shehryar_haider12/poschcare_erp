

<div id="subModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Sub Category</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Sub Category Name*</label>
                                <input class="form-control s_cat_name" type="text" placeholder="Enter Sub Category Name" name="s_cat_name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Category Name*</label>
                                <select class="form-control selectpicker" data-live-search="true" id="cattt" name="cat_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($cat as $s)
                                        <option value="{{$s->id}}">{{$s->cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitSubCategory" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitSubCategory').click(function(e){
        e.preventDefault();
        var s_cat_name = $('.s_cat_name').val();
        var cat_id = $('#cattt').val();
        var request_type = $('.request_type').val();
         axios
        .post('{{route("subcategory.store")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                s_cat_name:s_cat_name,
                cat_id:cat_id,
                request_type:request_type,
            })
            .then(function (responsive) {
                $('#subModal').modal('hide');
                $('#s_cat_id').selectpicker('destroy');
                $('#s_cat_id').append(`<option selected value="`+responsive.data.sub.id+`">`+responsive.data.sub.s_cat_name+`</option>`);
                $('#s_cat_id').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            })
            .catch(function (error) {
                alert(error);
        });
    });
</script>
