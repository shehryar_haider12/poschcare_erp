

<div id="productModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">All Products</h4>
            </div>

            <div class="modal-body">
                <div class="custom_datatable" >
                    <form action="#" id="advanceSearch" >
                        <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product Type</label>
                                    <select name="producttype" class="form-control selectpicker ">
                                    <option selected="" value="">No Filter</option>
                                    <option>Raw</option>
                                    <option>Material</option>
                                    <option>Packaging</option>
                                    <option>Finished</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Product Name</label>
                                    <input type="text" name="productname"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Code</label>
                                    <input type="text" id="productcode"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                {{-- <label for="">Unit Name</label> --}}
                                <label for="" style="visibility: hidden">.</label>
                                <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                <i class="fa fa-search pr-1"></i> Search</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table id="example5" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th >S.No</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>UOM</th>
                                    <th>Brand</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('#advanceSearch').submit(function(e){
            e.preventDefault();
            var table = $('#example5').DataTable();
            table.columns(1).search($('#productcode').val());
            table.columns(2).search($('input[name="productname"]').val());
            table.columns(5).search($('select[name="producttype"]').val());
            table.draw();
        });
    });
</script>
