@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {

        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/transaction">Transactions</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-history font-white"></i>Transaction History
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                            @if(in_array('excel',$permissions))
                                                @if ($index == 0)
                                                    <a style="margin-left: 130px"  href="{{route('transaction.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 1)
                                                    <a  style="margin-left: 130px" href="{{url('')}}/transaction/search/{{$year}}/{{$tf}}/{{$pb}}/excel/year">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 2)
                                                    <a  style="margin-left: 130px" href="{{url('')}}/transaction/search/{{$month}}/{{$tf}}/{{$pb}}/excel/month">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 4)
                                                    <a style="margin-left: 130px"  href="{{url('')}}/transaction/search/{{$date}}/{{$tf}}/{{$pb}}/excel/date">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 3)
                                                    <a style="margin-left: 130px"  href="{{url('')}}/transaction/search/{{$from}}/{{$to}}/{{$tf}}/{{$pb}}/excel/date">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 5 || $index == 6 || $index == 7 || $index == 8)
                                                    <a style="margin-left: 130px"  href="{{url('')}}/transaction/search/{{$index}}/{{$tf}}/{{$pb}}/excel">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/transaction/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" name="menuid">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date Range
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                        </label>
                                                    </div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Transaction From</label>
                                                            <select disabled id="tf" name="tf" class="form-control ">
                                                            <option selected="" value="">No Filter</option>
                                                            <option >Sales</option>
                                                            <option>Purchase</option>
                                                            <option>Return</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Paid By</label>
                                                                <select name="pb" id="pb" disabled class="form-control ">
                                                                <option selected="" value="">No Filter</option>
                                                                <option value="Cash">Cash</option>
                                                                <option value="Gift">Gift Card</option>
                                                                <option value="Credit">Credit Card</option>
                                                                <option value="Cheque">Cheque</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8"></div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            {{-- <label for="">Unit Name</label> --}}
                                                            {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                            <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive ">
                                                <table id="example" class="table table-striped table-bordered table-striped table-dark" style="width:1400px;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:40px;">S.No</th>
                                                            <th style="width:50px;">Invoice#</th>
                                                            <th style="width:130px;">Name <br><small>(Customer & Supplier)</small> </th>
                                                            <th style="width:130px;">Transaction From</th>
                                                            <th style="width:140px;">Transaction Type</th>
                                                            <th style="width:140px;">Transaction Date</th>
                                                            <th style="width:100px;">Paid By</th>
                                                            <th >Amount</th>
                                                            <th >Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $total =0;
                                                        @endphp
                                                        @foreach ($trans as $t)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$t->id}}
                                                                </td>
                                                                @if ($t->p_type == 'Sales')
                                                                    <td>
                                                                        {{$t->sales->customer->name}}
                                                                    </td>
                                                                @endif
                                                                @if ($t->p_type == 'Purchase')
                                                                    <td>
                                                                        {{$t->purchase->supplier->name}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$t->p_type}}
                                                                </td>
                                                                <td>
                                                                    {{$t->t_type}}
                                                                </td>
                                                                @php
                                                                    $date=Carbon\Carbon::parse($t->created_at)->format('d-m-y');
                                                                @endphp
                                                                <td>
                                                                    {{$date}}
                                                                </td>
                                                                <td>
                                                                    {{$t->paid_by}}
                                                                </td>
                                                                <td>
                                                                    {{$t->total}}
                                                                </td>
                                                                <td>
                                                                    <button type="button" data-target="#transactionhistory" data-toggle="modal"  class="btn green view" id="{{$t->id}}">
                                                                        <i class="icon-eye"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $total+=$t->total;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$total}}</td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
            <!-- End: life time stats -->
    </div>
    @section('modal')
        @include('modals.transactionhistory')
    @endsection
@section('custom-script')
@toastr_js
@toastr_render
    <script>
        $(document).on('click','.view',function(){
            var id=$(this).attr("id");
            $("#example2").empty();
            $.ajax({
                url:"{{url('')}}/transaction/"+id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    console.log(data);
                    if(data.paid_by == 'Cash')
                    {
                        $("#example2").append("<thead><tr><th>Invoice No</th><th>Transaction From</th><th>Received Amount</th></tr></thead><tr><td>"+data.id+"</td><td>"+data.p_type+"</td><td>"+data.total+"</td></tr>");
                    }
                    if(data.paid_by == 'Cheque')
                    {
                        $("#example2").append("<thead><tr><th>Invoice No</th><th>Transaction From</th><th>Received Amount</th><th>Cheque No</th><th>Bank Name</th></tr></thead><tr><td>"+data.id+"</td><td>"+data.p_type+"</td><td>"+data.total+"</td><td>"+data.cheque_no+"</td><td>"+data.bank.name+"</td></tr>");
                    }
                    if(data.paid_by == 'Gift Card')
                    {
                        $("#example2").append("<thead><tr><th>Invoice No</th><th>Transaction From</th><th>Received Amount</th><th>Gift Card #</th></tr></thead><tr><td>"+data.id+"</td><td>"+data.p_type+"</td><td>"+data.total+"</td><td>"+data.gift_no+"</td></tr>");
                    }
                    if(data.paid_by == 'Credit Card')
                    {
                        $("#example2").append("<thead><tr><th>Invoice No</th><th>Transaction From</th><th>Received Amount</th><th>Credit Card #</th><th>Bank Name</th></tr></thead><tr><td>"+data.id+"</td><td>"+data.p_type+"</td><td>"+data.total+"</td><td>"+data.cc_no+"</td><td>"+data.bank.name+"</td></tr>");
                    }
                    $('#transaction').modal("show");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                scrollX: true,
            });
            $('input:radio[name="optradio"]').change(function(){
                if ($(this).is(':checked')) {
                    $('#search').prop('disabled',false);
                    var val = $(this).val();
                    if(val == 'Year')
                    {
                        $('#year').prop('disabled',false);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#year').attr('required',true);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                    }
                    if(val == 'Month')
                    {
                        $('#month').prop('disabled',false);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                    }
                    if(val == 'Date')
                    {
                        $('#from').prop('disabled',false);
                        $('#from').attr('required',true);
                        $('#to').prop('disabled',false);
                        $('#to').attr('required',true);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#month').prop('disabled',true);
                        $('#year').attr('required',false);
                    }
                    if(val == 'last24Hours')
                    {
                        $('#month').prop('disabled',true);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                    }
                    if(val == 'lastweek')
                    {
                        $('#month').prop('disabled',true);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                    }
                    if(val == 'last15Days')
                    {
                        $('#month').prop('disabled',true);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                    }
                    if(val == 'lastMonth')
                    {
                        $('#month').prop('disabled',true);
                        $('#tf').prop('disabled',false);
                        $('#pb').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                    }
                }
            });

        });

    </script>

@endsection
@endsection
