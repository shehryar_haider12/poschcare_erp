@extends('layouts.master')
@section('top-styles')
<style>
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
@section('sidebar-name1')
{{-- @if(Session::has('download'))
         <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif --}}
<li>
    <a href="{{url('')}}/quotation">Quotations</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Quotation</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-quote-left font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}}  Quotation</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('quotation.update',$quote->id) : route('quotation.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            @if ($isEdit)

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <input type='file' name="attachment" id="imageUpload1" placeholder="add image"
                                                accept=".png, .jpg, .jpeg" required />
                                                <label for="imageUpload1"><span>Featured Image <small>(optional)</small> </span></label>
                                            </div>
                                            <div class="avatar-preview">
                                                <div id="imagePreview1"
                                                    style="background-image : url({{url('').'/uploads/placeholder.jpg'}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Quotation Number</label>
                                        <input value="{{$ids ?? $quote->id}}" name="id" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Quotation Date*</label>
                                        <input value="{{$date ?? old('date') ?? $quote->date}}" class="form-control" type="date" placeholder="Enter Order Date" name="date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Request Number</label>
                                        <input value="{{$id ?? $quote->r_id}}" name="r_id" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Supplier Name*</label>
                                        @if ($isEdit)
                                        <input type="text" class="form-control"  value="{{$quote->supplier->name}}" readonly>
                                        <input type="hidden" name="s_id" value="{{$quote->s_id}}">
                                        @else
                                        <select multiple class="form-control selectpicker" data-live-search="true" id="s_id" name="s_id[]" required>
                                            <option disabled >Select...</option>
                                            <option>Other</option>
                                            @foreach ($supplier as $s)
                                                <option value="{{$s->id}}">{{$s->name}} </option>
                                            @endforeach
                                        </select>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th >Product Id</th>
                                                <th >Code - Name - Brand - Category</th>
                                                <th >Quantity</th>
                                                @if ($isEdit)
                                                    <th>Rates</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($prequest as $item)
                                                @if ($item->type == 0)
                                                    <tr>
                                                        <td>{{$item->products->id}}
                                                            <input type="hidden" name="p_id[]" value="{{$item->products->id}}">
                                                            <input type="hidden" name="quantity[]" value="{{$item->quantity}}">
                                                            <input type="hidden" name="type[]" value="{{$item->type}}">
                                                        </td>
                                                        <td>
                                                            {{$item->products->pro_code}} -
                                                            {{$item->products->pro_name}} -
                                                            {{$item->products->brands->b_name}} -
                                                            {{$item->products->category->cat_name}}
                                                        </td>
                                                        <td>{{$item->quantity}}</td>
                                                        @if ($isEdit)
                                                            <td>
                                                                <input required type="number" name="cost[]" class="form-control">
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{$item->variant->id}}
                                                            <input type="hidden" name="p_id[]" value="{{$item->variant->id}}">
                                                            <input type="hidden" name="quantity[]" value="{{$item->quantity}}">
                                                            <input type="hidden" name="type[]" value="{{$item->type}}">
                                                        </td>
                                                        <td>
                                                            {{$item->variant->name}}
                                                        </td>
                                                        <td>{{$item->quantity}}</td>
                                                        @if ($isEdit)
                                                            <td>
                                                                <input required type="number" name="cost[]" class="form-control">
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @section('modal')
        @include('modals.supplier')
    @endsection

@endsection
@section('custom-script')
@toastr_js
@toastr_render
    <script>
        $(document).ready(function()
        {
            function readURL(input, number)
            {
                if (input.files && input.files[0])
                {
                    var reader = new FileReader();
                    reader.onload = function (e)
                    {
                        $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                        $('#imagePreview' + number).hide();
                        $('#imagePreview' + number).fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
                $("#imageUpload1").change(function () {
                    readURL(this, 1);
            });

            $('#s_id').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                console.log(opval);
                if(opval=="Other"){ //Compare it and if true
                    $('#supplierModal').modal("show"); //Open Modal
                }
            });
        });
    </script>


@endsection
