@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/accountdetails">Account</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>Add Account</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-money font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Add Account</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{route('accountdetails.store')}} " class="form-horizontal" method="POST" id="accountForm" >
                        @csrf
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Head of Account*</label>
                                        <select style="overflow-y: scroll;" id="hoa" size="1" class="form-control selectpicker" data-live-search="true" name="hoa" >
                                            <option value="" disabled selected>Select...</option>
                                                @foreach ($hoa as $s)
                                                <option value="{{$s->id}}" >{{$s->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Head Category Name*</label>
                                        <select style="overflow-y: scroll;" id="c_id" size="1" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                                {{-- @foreach ($hcat as $s)
                                                <option value="{{$s->id}}" >{{$s->name}}</option>
                                                @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6" id="name_of_account">
                                    <div class="form-outline">
                                        <label >Account Name*</label>
                                        <input value="{{old('name_of_account')}}" class="form-control" type="text" placeholder="Enter Account Name" id="name_of_account" name="name_of_account" >
                                    </div>
                                </div>

                                <div class="col-sm-6" id="bank_id">
                                    <div class="form-outline">
                                        <label >Account Name*</label>
                                        <select  id="b_id" class="form-control" data-live-search="true" name="name_of_account" >
                                            <option value="" disabled selected>Select...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" id="product">
                                    <div class="form-outline">
                                        <label >Account Name*</label>
                                        <select  id="p_id" class="form-control" data-live-search="true" name="name_of_account" >
                                            <option value="" disabled selected>Select...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" id="supplier">
                                    <div class="form-outline">
                                        <label >Account Name*</label>
                                        <select  id="s_id" class="form-control" data-live-search="true" name="name_of_account" >
                                            <option value="" disabled selected>Select...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" id="customer">
                                    <div class="form-outline">
                                        <label >Account Name*</label>
                                        <select  id="ct_id" class="form-control" data-live-search="true" name="name_of_account" >
                                            <option value="" disabled selected>Select...</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Account Code*</label>
                                        <input readonly value="{{old('Code')}}" class="form-control" type="text" id="code" name="Code" >
                                    </div>
                                </div>
                                <div class="col-sm-6" id="opening">
                                    <div class="form-outline">
                                        <label>Opening Balance <small>(optional)</small></label>
                                        <input type="text" name="opening" value="{{old('opening')}}" class="form-control">
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $(document).ready(function () {
        $('#bank_id').hide();
        $('#product').hide();
        $('#supplier').hide();
        $('#customer').hide();
        $('#opening').hide();
    });
    var count = 0;
    function recursive(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#c_id').append(`<option value = `+children.id+`-c>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                if(count < 7)
                {
                    recursive(children.accounts,count);
                }
            }
        });
    }
    $(document).on('change','#hoa',function(){
        var id = $(this).val();
        $.ajax({
            url:"{{url('')}}/headofaccounts/find/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $("#c_id").empty();
                $('#c_id').selectpicker('destroy');
                $('#c_id').append('<option disabled selected>Select..</option>');
                for(var i = 0 ; i < data.length ; i++)
                {
                    $('#c_id').append(`<option value='`+data[i].id+`-p'>`+data[i].name+` (Parent)</option>`);
                    if(data[i].account_details.length > 0)
                    {
                        for (let j = 0; j < data[i].account_details.length; j++) {

                            $('#c_id').append(`<option value='`+data[i].account_details[j].id+`-c'>&emsp;`+data[i].account_details[j].name_of_account+` (Layer1)</option>`);
                            if(data[i].account_details[j].children.length > 0)
                            {
                                count = 3;
                                recursive(data[i].account_details[j].children,count);

                            }
                        }
                    }
                }
                $('#c_id').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        });
    })

    $(document).on('change','#c_id',function(){
        var value=$(this).val();
        var id = value.substr(0, value.indexOf('-'));
        var type = value.substring(value.indexOf('-') + 1);
        var name=$("#c_id option:selected").text();
        $.ajax({
            url:"{{url('')}}/accountdetails/view/"+id+"/"+type,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                length = data.length;
                if(length == 1)
                {
                    if(data[0].includes('EXP') || (data[0].includes('CA') && !data[0].includes('NCA')))
                    {
                        console.log('dsds');
                        $('#opening').show();
                    }
                    else
                    {
                        $('#opening').hide();
                    }
                    $('#code').val(data);
                    $('#name_of_account').show();
                    $('#bank_id').hide();
                    $('#product').hide();
                    $('#supplier').hide();
                    $('#customer').hide();
                }
                else
                {
                    $('#opening').hide();
                    if(name  == 'Bank')
                    {
                        $('#name_of_account').hide();
                        $('#code').val(data[0]);
                        $('#bank_id').show();
                        $('#customer').hide();
                        $('#supplier').hide();
                        $('#product').hide();
                        for(var i = 0 ; i < data[1].length ; i++)
                        {
                            $("#b_id").append('<option value="'+data[1][i].name+' - '+data[1][i].branch+'">'+data[1][i].name+' - '+data[1][i].branch+'</option>');
                        }

                        $('#b_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    else if(name  == 'Receivables')
                    {
                        $('#name_of_account').hide();
                        $('#code').val(data[0]);
                        $('#customer').show();
                        $('#bank_id').hide();
                        $('#supplier').hide();
                        $('#product').hide();
                        $("#ct_id").empty();
                        $('#ct_id').selectpicker('destroy');
                        $('#ct_id').append('<option value="" disabled selected>Select...</option>');
                        for(var i = 0 ; i < data[1].length ; i++)
                        {
                            $("#ct_id").append('<option value="'+data[1][i].company+' - '+data[1][i].name+'">'+data[1][i].name+' - '+data[1][i].company+'</option>');
                        }
                        $('#ct_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    else if(name  == 'Loan')
                    {
                        $('#name_of_account').hide();
                        $('#code').val(data[0]);
                        $('#customer').show();
                        $('#bank_id').hide();
                        $('#supplier').hide();
                        $('#product').hide();
                        $("#ct_id").empty();
                        $('#ct_id').selectpicker('destroy');
                        $('#ct_id').append('<option value="" disabled selected>Select...</option>');
                        for(var i = 0 ; i < data[1].length ; i++)
                        {
                            $("#ct_id").append('<option value="'+data[1][i].company+' - '+data[1][i].name+'">'+data[1][i].name+' - '+data[1][i].company+'</option>');
                        }
                        $('#ct_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    else if(name  == 'Short Loan')
                    {
                        $('#name_of_account').hide();
                        $('#code').val(data[0]);
                        $('#customer').show();
                        $('#bank_id').hide();
                        $('#supplier').hide();
                        $('#product').hide();
                        $("#ct_id").empty();
                        $('#ct_id').selectpicker('destroy');
                        $('#ct_id').append('<option value="" disabled selected>Select...</option>');
                        for(var i = 0 ; i < data[1].length ; i++)
                        {
                            $("#ct_id").append('<option value="'+data[1][i].company+' - '+data[1][i].name+'">'+data[1][i].name+' - '+data[1][i].company+'</option>');
                        }
                        $('#ct_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    else if(name  == 'Inventory')
                    {
                        $('#name_of_account').hide();
                        $('#code').val(data[0]);
                        $('#product').show();
                        $('#bank_id').hide();
                        $('#customer').hide();
                        $('#supplier').hide();
                        for(var i = 0 ; i < data[1].length ; i++)
                        {
                            if(data[1][i].vstatus == 0)
                            {
                                console.log(data[1][i].pro_code);
                                $("#p_id").append('<option value="'+data[1][i].pro_code+' - '+data[1][i].pro_name+'_0">'+data[1][i].pro_code+' - '+data[1][i].pro_name+' - '+data[1][i].p_type+'</option>');
                            }
                            else
                            {
                                for(var n = 0 ; n< data[1][i].variants.length ; n++)
                                {
                                    console.log(data[1][i].variants[n]);
                                    $("#p_id").append('<option value="'+data[1][i].variants[n].name+'_1">'+data[1][i].variants[n].name+' - '+data[1][i].p_type+'</option>');
                                }
                            }
                        }
                        $('#p_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    else if(name  == 'Payables')
                    {
                        $('#name_of_account').hide();
                        $('#code').val(data[0]);
                        $('#supplier').show();
                        $('#bank_id').hide();
                        $('#customer').hide();
                        $('#product').hide();
                        for(var i = 0 ; i < data[1].length ; i++)
                        {
                            $("#s_id").append('<option value="'+data[1][i].name+' - '+data[1][i].company+'">'+data[1][i].name+' - '+data[1][i].company+'</option>');
                        }
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    // console.log(data);
                }
            }
        });
    });
</script>

<script>
   $('#accountForm').validate({
       rules: {
        Code: {
               required: true,
           },
           name_of_account:{
               required: true,
           },
           c_id:{
               required: true,
           }
       },
       errorElement: 'span',
       errorPlacement: function (error, element) {
           error.addClass('invalid-feedback back_error');
           element.closest('.form-outline').append(error);
       },
       highlight: function (element, errorClass, validClass) {
           $(element).addClass('is-invalid');

       },
       unhighlight: function (element, errorClass, validClass) {
           $(element).removeClass('is-invalid');
       }
   });
</script>
@endsection
