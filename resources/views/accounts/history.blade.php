@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/accountdetails">All Accounts</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-money font-white"></i>General Ledger
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    @if ($index == 0)
                                                        <a style="margin-left:200px; "  href="{{url('')}}/accountdetails/{{$id}}/{{$ledger[0]->account_code}}/{{$ledger[0]->account_name}}/excel">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 1)
                                                        <a style="margin-left:200px; "  href="{{url('')}}/accountdetails/search/{{$year}}/{{$id}}/excel/year">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 2)
                                                        <a style="margin-left:200px; "  href="{{url('')}}/accountdetails/search/{{$month}}/{{$id}}/excel/month">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 3)
                                                        <a style="margin-left:200px; "  href="{{url('')}}/accountdetails/search/{{$from}}/{{$to}}/{{$id}}/excel/dates">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 4)
                                                        <a style="margin-left:200px; "  href="{{url('')}}/accountdetails/search/{{$date}}/{{$id}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 5 || $index == 6 || $index == 7)
                                                        <a style="margin-left:200px; "  href="{{url('')}}/accountdetails/search/{{$index}}/{{$id}}/excel">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    @if ($index == 0)
                                                        <a style="margin-left:-50px;" href="{{url('')}}/accountdetails/{{$ledger[0]->account_code}}/pdf">
                                                            <i class="fa fa-file-pdf-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 1)
                                                        <a style="margin-left:-50px; "  href="{{url('')}}/accountdetails/search/{{$year}}/{{$id}}/pdf/year">
                                                            <i class="fa fa-file-pdf-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 2)
                                                        <a style="margin-left:-50px; "  href="{{url('')}}/accountdetails/search/{{$month}}/{{$id}}/pdf/month">
                                                            <i class="fa fa-file-pdf-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 3)
                                                        <a style="margin-left:-50px; "  href="{{url('')}}/accountdetails/search/{{$from}}/{{$to}}/{{$id}}/pdf/dates">
                                                            <i class="fa fa-file-pdf-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 4)
                                                        <a style="margin-left:-50px; "  href="{{url('')}}/accountdetails/search/{{$date}}/{{$id}}/pdf/date">
                                                            <i class="fa fa-file-pdf-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 5 || $index == 6 || $index == 7)
                                                        <a style="margin-left:-50px; "  href="{{url('')}}/accountdetails/search/{{$index}}/{{$id}}/pdf">
                                                            <i class="fa fa-file-pdf-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/accountdetails/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$mid}}" name="menuid">
                                                <input type="hidden" value="{{$id}}" name="id">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Month</label>
                                                            <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From</label>
                                                                <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To</label>
                                                                <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="" style="visibility: hidden">.</label>
                                                                <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                <i class="fa fa-search pr-1"></i> Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive" style="overflow-x: hidden">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Source</th>
                                                            <th>Account Code</th>
                                                            <th>Account Name</th>
                                                            <th>Description</th>
                                                            <th>Created By</th>
                                                            <th>Accounting Date</th>
                                                            <th>Against Account</th>
                                                            <th>Posted Date</th>
                                                            <th>Period</th>
                                                            <th>Reference No</th>
                                                            @if ($id == 'CA-02')
                                                                <th>Debit</th>
                                                                <th>Credit</th>
                                                                <th>In Stock</th>
                                                                <th>Out Stock</th>
                                                                <th>Net Quantity</th>
                                                                <th>Balance</th>
                                                                <th>Amount</th>
                                                            @else
                                                            @if ($id == 'EXP-05')
                                                                <th>Debit</th>
                                                                <th>Credit</th>
                                                                <th>Net Value</th>
                                                                <th>Balance</th>
                                                                <th>Amount</th>
                                                            @else
                                                                <th>Debit</th>
                                                                <th>Credit</th>
                                                                <th>Net Value</th>
                                                                <th>Balance</th>
                                                            @endif
                                                            @endif
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $debit = 0;
                                                            $credit = 0;
                                                            $in = 0;
                                                            $out = 0;
                                                            $net = 0;
                                                            $amount = 0;
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($ledger as $a)
                                                        @if ($a->account_code == 'CL-02-001')
                                                            @php
                                                                $gl = App\GeneralLedger::where('link_id',$a->link_id)
                                                                ->where('account_code','!=',$a->account_code)
                                                                ->where('account_code','not like','CA-01%')
                                                                ->first();
                                                            @endphp
                                                        @else
                                                            @php
                                                                $gl = App\GeneralLedger::where('link_id',$a->link_id)
                                                                ->where('account_code','!=',$a->account_code)
                                                                ->first();
                                                            @endphp
                                                        @endif
                                                        @php
                                                            $debit += $a->debit;
                                                            $credit += $a->credit;
                                                            $in += $a->stock_in;
                                                            $out += $a->stock_out;
                                                            $net += $a->net_value;
                                                        @endphp
                                                            <tr>
                                                                <td>
                                                                    {{$a->id}}
                                                                </td>
                                                                <td>
                                                                    {{$a->source}}
                                                                </td>
                                                                <td>
                                                                    {{$a->account_code}}
                                                                </td>
                                                                <td>
                                                                    {{$a->account_name}}
                                                                </td>
                                                                <td>
                                                                    {{$a->description}}
                                                                </td>
                                                                <td>
                                                                    @if ($a->createUser == null)
                                                                        -
                                                                    @else
                                                                        {{$a->createUser->name}}
                                                                    @endif

                                                                </td>
                                                                @php
                                                                    $period = Carbon\Carbon::parse($a->period)
                                                                    ->format('Y-m');
                                                                @endphp
                                                                <td>
                                                                    {{$a->accounting_date}}
                                                                </td>
                                                                <td>
                                                                    {{$gl == null ? '-' :  $gl->account_name}}
                                                                </td>
                                                                <td>
                                                                    {{$a->posted_date}}
                                                                </td>
                                                                <td>
                                                                    {{$period}}
                                                                </td>
                                                                <td>
                                                                    {{$a->transaction_no}}
                                                                </td>
                                                                <td>
                                                                    {{$a->debit}}
                                                                </td>
                                                                <td>
                                                                    {{$a->credit}}
                                                                </td>
                                                                @if ($id == 'CA-02')
                                                                    <td>
                                                                        {{$a->stock_in}}
                                                                    </td>
                                                                    <td>
                                                                        {{$a->stock_out}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$a->net_value}}
                                                                </td>
                                                                <td>
                                                                    {{$a->balance}}
                                                                </td>
                                                                @if ($id == 'CA-02')
                                                                    @php
                                                                        $total += trim($a->amount);
                                                                    @endphp
                                                                    <td>{{$a->amount}}</td>
                                                                @else
                                                                    @if ($id == 'EXP-05')
                                                                    <td>{{$a->amount}}</td>
                                                                    @endif
                                                                @endif
                                                                @if ($a->account_code == 'RV-01-001')
                                                                    <td>
                                                                        <a id="GFG" href="{{url('')}}/sales" class="text-info p-1">
                                                                            <button type="button" class="btn blue edit" >
                                                                                <i class="fa fa-eye"></i>
                                                                            </button>
                                                                        </a>
                                                                    </td>
                                                                @else
                                                                    @if ($a->account_code == 'PUR-01-001')
                                                                        <td>
                                                                            <a id="GFG" href="{{url('')}}/purchase" class="text-info p-1">
                                                                                <button type="button" class="btn blue edit" >
                                                                                    <i class="fa fa-eye"></i>
                                                                                </button>
                                                                            </a>
                                                                        </td>
                                                                    @else
                                                                        <td>
                                                                            <button type="button" disabled class="btn blue view">
                                                                                <i class="fa fa-refresh"></i>
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                @endif


                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label  style="font-weight: bold;">

                                                            @if ($id == 'CA-02')
                                                                Total Debit: {{$debit}} &emsp;
                                                                Total Credit: {{$credit}} &emsp;
                                                                Total In Stock: {{$in}}&emsp;
                                                                Total Out Stock: {{$out}}&emsp;
                                                                Total Balance: {{$in - $out}} &emsp;
                                                                Total Amount: {{$total}}
                                                            @else
                                                                Total Debit: {{$debit}} &emsp;
                                                                Total Credit: {{$credit}} &emsp;
                                                                Total Balance: {{$debit - $credit}} &emsp;
                                                            @endif
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render

        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    scrollX: true
                });

                $('input:radio[name="optradio"]').change(function(){
                    if ($(this).is(':checked')) {
                        $('#search').prop('disabled',false);
                        var val = $(this).val();
                        if(val == 'Year')
                        {
                            $('#year').prop('disabled',false);
                            $('#year').attr('required',true);
                            $('#month').attr('required',false);
                            $('#month').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#to').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').attr('required',false);
                        }
                        if(val == 'Month')
                        {
                            $('#month').prop('disabled',false);
                            $('#month').attr('required',true);
                            $('#year').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#to').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').attr('required',false);
                            $('#year').attr('required',false);
                        }
                        if(val == 'Date')
                        {
                            $('#from').prop('disabled',false);
                            $('#from').attr('required',false);
                            $('#to').prop('disabled',false);
                            $('#to').attr('required',false);
                            $('#month').attr('required',false);
                            $('#year').prop('disabled',true);
                            $('#month').prop('disabled',true);
                            $('#year').attr('required',false);
                        }
                        if(val == 'lastweek')
                        {
                            $('#year').prop('disabled',true);
                            $('#year').attr('required',false);
                            $('#month').attr('required',false);
                            $('#month').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#to').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').attr('required',false);
                        }
                        if(val == 'last15Days')
                        {
                            $('#year').prop('disabled',true);
                            $('#year').attr('required',false);
                            $('#month').attr('required',false);
                            $('#month').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#to').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').attr('required',false);
                        }
                        if(val == 'lastMonth')
                        {
                            $('#year').prop('disabled',true);
                            $('#year').attr('required',false);
                            $('#month').attr('required',false);
                            $('#month').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#to').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').attr('required',false);
                        }
                    }
                });
            });

        </script>
    @endsection
@endsection
