@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/accountdetails">Account</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-money font-white"></i>View Accounts
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('accountdetails.create')}}" class="col-md-2" style="float: right">
                                                    <button style="background: #00CCFF" type="button"  class="btn btn-block btn-primary btn-md ">Add Account</button>
                                                </a>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Account Name</label>
                                                        <select class="form-control selectpicker" data-live-search="true" multiple id="name" >
                                                            <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                            @foreach ($account as $u)
                                                            <option  value="{{$u->name_of_account}}">{{$u->name_of_account}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Head Category Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" multiple id="a_id" >
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($hcat as $u)
                                                                <option  value="{{$u->name}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" id="code" id="autocomplete-ajax1" class="form-control" placeholder="Account Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                </div>
                                                </div>
                                            </form>
                                            <input type="hidden" id="menuid" value="{{$menu_id}}">
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Account Code</th>
                                                            <th>Account Name</th>
                                                            <th>Head Category</th>
                                                            <th>Balance</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $s = 1;
                                                        @endphp
                                                        @foreach ($account as $a)
                                                        @php
                                                        // if($a->Code == 'CA-01-003')
                                                        // {
                                                            $credit = $a->generalLedger->pluck('credit')->toArray();
                                                            $sum_credit = array_sum($credit);
                                                            $debit = $a->generalLedger->pluck('debit')->toArray();
                                                            $sum_debit = array_sum($debit);

                                                            $balance =  $sum_debit - $sum_credit;
                                                        // }

                                                        @endphp
                                                            <tr>
                                                                <td>
                                                                    {{$s}}
                                                                </td>
                                                                <td>
                                                                    {{$a->Code}}
                                                                </td>
                                                                <td>
                                                                    {{$a->name_of_account}}
                                                                </td>
                                                                <td>
                                                                    {{$a->account_type == 'p' ? $a->headCategory->name : $a->parent->name_of_account}}
                                                                </td>
                                                                <td>
                                                                    {{$balance}}
                                                                </td>
                                                                <td>
                                                                    @if(in_array('history',$permissions))
                                                                    <button type="button" class="btn blue view" id="{{$a->Code}}">
                                                                        <i class="fa fa-book"></i>
                                                                    </button>
                                                                    @else
                                                                        <button disabled type="button" class="btn blue view" id="{{$a->Code}}">
                                                                            <i class="fa fa-book"></i>
                                                                        </button>
                                                                    @endif
                                                                        {{-- <a id="GFG" href="/headcategory/{{$a->id}}/edit" class="text-info p-1">
                                                                            <button type="button" class="btn blue edit" >
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                        </a> --}}
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $s++;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render

        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable();
                $('#a_id').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#a_id option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#a_id').selectpicker('deselectAll');
                        $('#a_id').selectpicker('refresh');
                    }
                    $.each($('#a_id option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(3).search(search, true, false).draw();
                });
                $('#name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#name').selectpicker('deselectAll');
                        $('#name').selectpicker('refresh');
                    }
                    $.each($('#name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(2).search(search, true, false).draw();
                });

                $('#code').on('keyup',function(){
                    value = $("#code").val();
                    table.columns(1).search(value, true, false).draw();
                });
            });
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                var menuid = $('#menuid').val();
                $.ajax({
                    url:"{{url('')}}/accountdetails/chkHistory/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        if(data == 0)
                        {
                            alert('No Record Found');
                        }
                        else
                        {
                            window.location.href='{{url('')}}/accountdetails/history/'+id+'/'+menuid;
                        }
                    }
                });
            });


        </script>
    @endsection
@endsection
