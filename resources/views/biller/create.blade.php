@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/biller">Biller</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Biller</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Biller</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="billerForm" action="{{$isEdit ? route('biller.update',$vendor->id) :  route('biller.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="document" id="imageUpload1" placeholder="add image"
                                            accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Featured Image* </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{$isEdit ? ($vendor->attachment->isEmpty() ? url('').'/uploads/placeholder.jpg' : url('').'/documents/'.$vendor->attachment[0]->document) : url('').'/uploads/placeholder.jpg'}})">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Biller Name*</label>
                                        <input value="{{$vendor->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Biller Name" name="name" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Abbreviation</label>
                                        <input type="text" class="form-control" value="{{$vendor->abr ?? old('abr')}}" name="abr" placeholder="Enter abbreviation">
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : in_array('Add City',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >City <small>(optional)</small></label>
                                        <select id="c_id" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add City',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($city as $s)
                                                <option {{$s->id == $vendor->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->c_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($city as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('c_id') ? 'selected' : null}}>{{$s->c_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add City',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#cityModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <input type="hidden" name="v_type" value="Biller" id="">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Company Name*</label>
                                        <input value="{{$vendor->company ?? old('company')}}" class="form-control" type="text" placeholder="Enter Company Name" name="company" >
                                        <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Address*</label>
                                        <input value="{{$vendor->address ?? old('address')}}" class="form-control" type="text" placeholder="Enter Address" name="address" >
                                        <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact No*</label>
                                        <input autocomplete="off" value="{{$vendor->c_no ?? old('c_no')}}" class="form-control" type="number" placeholder="Enter Contact Number" name="c_no" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Country <small>(optional)</small></label>
                                        <input value="{{$vendor->country ?? old('country')}}" class="form-control" type="text" placeholder="Enter Country" name="country" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >NTN <small>(optional)</small></label>
                                        <input value="{{$vendor->VAT ?? old('VAT')}}" class="form-control" type="text" placeholder="Enter NTN Number" name="VAT" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >GST <small>(optional)</small></label>
                                        <input value="{{$vendor->GST ?? old('GST')}}" class="form-control" type="text" placeholder="Enter GST Number" name="GST" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >State <small>(optional)</small></label>
                                        <input value="{{$vendor->state ?? old('state')}}" class="form-control" type="text" placeholder="Enter State" name="state" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Email <small>(optional)</small></label>
                                        <input value="{{$vendor->email ?? old('email')}}" class="form-control" type="email" placeholder="Enter Email" name="email" >
                                        <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Postal Code <small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$vendor->postalCode ?? old('postalCode')}}" class="form-control" type="number" placeholder="Enter Postal Code" name="postalCode" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Website <small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$vendor->website ?? old('website')}}" class="form-control" type="text" placeholder="Enter Website" name="website" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.city')
    @endsection
    @section('custom-script')
    @toastr_js
    @toastr_render
    <script>
        $(document).ready(function(){
            $('#c_id').change(function() { //jQuery Change Function
                var opval = $(this).val(); //Get value from select element
                console.log(opval);
                if(opval=="other"){ //Compare it and if true
                    $('#cityModal').modal("show"); //Open Modal
                }
            });
            function readURL(input, number) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                        $('#imagePreview' + number).hide();
                        $('#imagePreview' + number).fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
                $("#imageUpload1").change(function () {
                    readURL(this, 1);
            });
        });
    </script>

<script>

   $('#billerForm').validate({
       rules: {
           name: {
               required: true,
           },
           abr: {
               required: true,
           },
           address:{
               required: true,
           },
           c_no:{
               required: true,
           },
           company:{
               required: true,
           },
           document:{
               required: true,
           }
       },
       errorElement: 'span',
       errorPlacement: function (error, element) {
           error.addClass('invalid-feedback back_error');
           element.closest('.form-outline').append(error);
       },
       highlight: function (element, errorClass, validClass) {
           $(element).addClass('is-invalid');

       },
       unhighlight: function (element, errorClass, validClass) {
           $(element).removeClass('is-invalid');
       }
   });
</script>
    @endsection
