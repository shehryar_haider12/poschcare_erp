<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
  </style>

  <body>
    <div class="attendance-table">
        <h3>Billers Information</h3>
        <table class="table-bordered">

            <tr>
                <th class="attendance-cell">S.NO</th>
                <th class="attendance-cell">Name</th>
                <th class="attendance-cell">Company</th>
                <th class="attendance-cell">Address</th>
                <th class="attendance-cell">Contact No</th>
            </tr>
            @php
                $count = 1;
            @endphp
            @foreach($vendor as $user)
                <tr>
                    <td class="attendance-cell">{{ucwords($count)}}</td>
                    <td class="attendance-cell">{{ucwords($user->name)}}</td>
                    <td class="attendance-cell">{{ucwords($user->company)}}</td>
                    <td class="attendance-cell">{{ucwords($user->address)}}</td>
                    <td class="attendance-cell">{{$user->c_no}}</td>
                </tr>
                @php
                    $count++;
                @endphp
            @endforeach

        </table>

    </div>
  </body>
</html>
