@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/history/cost">Average Cost History</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-houzz font-white"></i>Average Cost History
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Purchase Order No</th>
                                                            <th>Product Name</th>
                                                            <th>Cost</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($avg as $a)
                                                            <tr>
                                                                <td>
                                                                    {{$a->id}}
                                                                </td>
                                                                <td>
                                                                    {{$a->po_id}}
                                                                </td>
                                                                @if ($a->type == 0)
                                                                    <td>
                                                                        {{$a->products->pro_code.' - '.$a->products->pro_name}}
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        {{$a->variant->name}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$a->cost}}
                                                                </td>
                                                                <td>
                                                                    {{\Carbon\Carbon::parse($a->created_at)->format('d-m-Y')}}
                                                                </td>

                                                            </tr>
                                                            @php
                                                                $total += $a->cost;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$total}}</td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable();
    });
    </script>

@endsection
@endsection
