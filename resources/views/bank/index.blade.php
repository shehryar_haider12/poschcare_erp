@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {

        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/bank">Banks</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-bank font-white"></i>View Banks
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('bank.create')}}" class="col-md-2" style="float: right">
                                                    <button  style="background: #00CCFF" type="button"  class="btn btn-block btn-info btn-md ">Add Bank</button>
                                                </a>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="tableview">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Bank Name</label>
                                                        <input type="text" id="b_name" id="autocomplete-ajax1" class="form-control" placeholder="Bank Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">City</label>
                                                        <select id="c_name" class="form-control selectpicker" multiple data-live-search="true">
                                                            <option value="Not All">Unselect All</option>
                                                            <option value="All">Select All</option>
                                                            @foreach ($city as $u)
                                                                <option  value="{{$u->c_name}}">{{$u->c_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4"></div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>City</th>
                                                            <th>Branch</th>
                                                            <th>Opening Balance</th>
                                                            <th>Address</th>
                                                            <th>Contact No</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    processing: true,
                    serverSide: true,
                    order: [[ 0, "desc" ]],
                    scrollX: true,
                    ajax: '{{route("bank.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "name",
                            "defaultContent": ""
                        },
                        {
                            "data": "city.c_name",
                            "defaultContent": "",
                            "render": function (data, type, row, meta) {
                                if(row.city == null)
                                {
                                    return `-`;
                                }
                                else
                                {
                                    return row.city.c_name;
                                }
                            },
                        },
                        {
                            "data": "branch",
                            "defaultContent": ""
                        },
                        {
                            "data": "opening_balance",
                            "defaultContent": ""
                        },
                        {
                            "data": "address",
                            "defaultContent": ""
                        },
                        {
                            "data": "c_no",
                            "defaultContent": "",
                            "render": function (data, type, row, meta) {
                                if(row.c_no == null)
                                {
                                    return `-`;
                                }
                                else
                                {
                                    return row.c_no;
                                }
                            },
                        },
                        {
                            "data": "id",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                return meta.row + 1 ;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                var edit = '{{route("bank.edit",[":id"])}}';
                                edit = edit.replace(':id', row.id);
                                var checked = row.status == 1 ? 'checked' : null;
                                return `
                                @if(in_array('edit',$permissions))
                                    <a id="GFG" href="` + edit + `" class="text-info p-1">
                                        <button type="button" class="btn blue edit" >
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                @endif
                                @if(in_array('status',$permissions))
                                    <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                            checked + ` value="` + row.id + `">
                                @endif
                                `;
                            },
                        },
                    ],
                    "drawCallback": function (settings) {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                    if (elems)
                    {
                        elems.forEach(function (html)
                        {
                            var switchery = new Switchery(html,
                            {
                                color: '#007bff',
                                 secondaryColor: '#dfdfdf',
                                 jackColor: '#fff',
                                 jackSecondaryColor: null,
                                 className: 'switchery',
                                 disabled: false,
                                 disabledOpacity: 0.5,
                                 speed: '0.1s',
                                 size: 'small'
                            });

                        });
                    }
                    $('.status').change(function () {
                        var $this = $(this);
                        var id = $this.val();
                        var status = this.checked;

                        if (status) {
                            status = 1;
                        } else {
                            status = 0;
                        }
                        // console.log(status);
                        axios
                            .post('{{route("bank.status")}}', {
                            _token: '{{csrf_token()}}',
                            _method: 'post',
                            id: id,
                            status: status,
                            })
                            .then(function (responsive) {
                            console.log('responsive');
                            // location.reload();
                            })
                            .catch(function (error) {
                            console.log(error);
                            });
                        });

                    },
                });
                $('#c_name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#c_name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#c_name').selectpicker('deselectAll');
                        $('#c_name').selectpicker('refresh');
                    }
                    $.each($('#c_name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(2).search(search, true, false).draw();
                });
                $('#b_name').on('keyup',function(){
                    value = $("#b_name").val();
                    table.columns(1).search(value, true, false).draw();
                });

            });

        </script>
    @endsection
@endsection
