@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/bank">Banks</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Bank</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-bank font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Bank</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="bankForm" action="{{$isEdit ? route('bank.update',$bank->id) :  route('bank.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Bank Name* </label>
                                        <input value="{{$bank->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' :  in_array('Add City',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >City <small>(optional)</small></label>
                                        <select id="c_id" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add City',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($city as $s)
                                                <option {{$s->id == $bank->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->c_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($city as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('c_id') ? 'selected' : null}} >{{$s->c_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add City',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-outline" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#cityModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Address*</label>
                                        <input value="{{$bank->address ?? old('address')}}" class="form-control" type="text" placeholder="Enter Address" name="address" >
                                        <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact No <small>(optional)</small> </label>
                                        <input autocomplete="off" value="{{$bank->c_no ?? old('c_no')}}" class="form-control" type="text" placeholder="Enter Contact Number" name="c_no" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Branch*</label>
                                        <input value="{{$bank->branch ?? old('branch')}}"  class="form-control" type="text" placeholder="Enter Branch" name="branch" >
                                        <span class="text-danger">{{$errors->first('branch') ?? null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Opening Balance <small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$bank->opening_balance ?? old('opening_balance')}}" class="form-control" type="text" placeholder="Enter Opening Balance" name="opening_balance" >
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.city')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>

   $('#bankForm').validate({
       rules: {
           name: {
               required: true,
           },
           address:{
               required: true,
           },
           branch:{
               required: true,
           }
       },
       errorElement: 'span',
       errorPlacement: function (error, element) {
           error.addClass('invalid-feedback back_error');
           element.closest('.form-outline').append(error);
       },
       highlight: function (element, errorClass, validClass) {
           $(element).addClass('is-invalid');

       },
       unhighlight: function (element, errorClass, validClass) {
           $(element).removeClass('is-invalid');
       }
   });
</script>
<script>
    $(document).ready(function(){
        $('#c_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#cityModal').modal("show"); //Open Modal
            }
        });
    });
</script>
@endsection
