@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/city">City</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-building font-white"></i>View City
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('city.create')}}" class="col-md-2" style="float: right">
                                                    <button style="background: #00CCFF" type="button"  class="btn btn-block btn-primary btn-md">Add City</button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="tableview">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="">City Name</label>
                                                        <input type="text" id="c_name" id="autocomplete-ajax1" class="form-control" placeholder="City Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
@section('modal')

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">City</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City Name</label>
                                <input class="form-control" type="text" placeholder="Enter City Name" id="c_name" readonly>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
<!--End Modal-->
@endsection
@section('custom-script')
@toastr_js
@toastr_render
    {{-- view one product --}}
    <script>
        $(document).on('click','.view',function(){
            var id=$(this).attr("id");
            $.ajax({
                url:"{{url('')}}/city/"+id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    console.log(data);
                    $('#c_name').val(data.c_name);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                processing: true,
                order: [[ 0, "desc" ]],
                serverSide: true,
                ajax: '{{route("city.datatable")}}',
                "columns": [{
                        "data": "id",
                        "defaultContent": ""
                    },
                    {
                        "data": "c_name",
                        "defaultContent": ""
                    },
                    {
                        "data": "id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            var edit = '{{route("city.edit",[":id"])}}';
                            edit = edit.replace(':id', row.id );
                            var checked = row.status == 1 ? 'checked' : null;
                            return `
                            @if(in_array('edit',$permissions))
                                <a id="GFG" href="` + edit + `" class="text-info p-1">
                                    <button type="button" class="btn blue edit" >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            @endif
                            @if(in_array('show',$permissions))
                                <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                    <i class="icon-eye"></i>
                                </button>
                            @endif
                            @if(in_array('status',$permissions))
                                <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                checked + ` value="` + row.id + `">
                            @endif
                            `;
                        },
                    },
                ],
                "drawCallback": function (settings)
                {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                    if (elems)
                    {
                        elems.forEach(function (html)
                        {
                            var switchery = new Switchery(html,
                            {
                                color: '#007bff',
                                 secondaryColor: '#dfdfdf',
                                 jackColor: '#fff',
                                 jackSecondaryColor: null,
                                 className: 'switchery',
                                 disabled: false,
                                 disabledOpacity: 0.5,
                                 speed: '0.1s',
                                 size: 'small'
                            });

                        });
                    }
                    $('.status').change(function ()
                    {
                        var $this = $(this);
                        var id = $this.val();
                        var status = this.checked;

                        if (status) {
                            status = 1;
                        } else {
                            status = 0;
                        }
                        axios
                        .post('{{route("city.status")}}',
                        {
                            _token: '{{csrf_token()}}',
                            _method: 'post',
                            id: id,
                            status: status,
                        })
                        .then(function (responsive)
                        {
                            console.log('responsive');
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    });

                },
            });
            $('#c_name').on('keyup',function(){
                value = $("#c_name").val();
                table.columns(1).search(value, true, false).draw();
            });
        });

    </script>
@endsection
@endsection
