@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/supplierUpdateRequest">Supplier Data Update Requests</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-user font-white"></i>View Supplier Data Update Requests
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Supplier Name</label>
                                                        <input type="text" id="v_name" id="autocomplete-ajax1" class="form-control" placeholder="Supplier Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Company</label>
                                                        <input type="text" id="company" id="autocomplete-ajax1" class="form-control" placeholder="Company" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="examples" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Company</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Supplier</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Supplier Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Supplier Name" id="name" readonly >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Company</label>
                                    <input class="form-control" type="text" placeholder="Enter Vendor Name" id="company" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Address</label>
                                    <input  class="form-control" type="text" placeholder="Enter Vendor Name" id="address" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Contact Number</label>
                                    <input class="form-control" type="text" placeholder="Enter Vendor Name" id="c_no" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Country</label>
                                    <input  class="form-control" type="text" placeholder="Enter Vendor Name" id="country" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >NTN</label>
                                    <input class="form-control" type="text" placeholder="Enter Vendor Name" id="vat" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >GST</label>
                                    <input  class="form-control" type="text" placeholder="Enter Vendor Name" id="gst" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >State</label>
                                    <input class="form-control" type="text" placeholder="Enter Vendor Name" id="state" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Email</label>
                                    <input  class="form-control" type="text" placeholder="Enter Vendor Name" id="email" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Postal Code</label>
                                    <input class="form-control" type="text" placeholder="Enter Vendor Name" id="pc" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >City</label>
                                    <input  class="form-control" type="text" placeholder="Enter Vendor Name" id="city" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
            <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
    @toastr_render
        {{-- view one product --}}
        <script>
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/supplier/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#name').val(data.name);
                        $('#company').val(data.company);
                        $('#address').val(data.address);
                        $('#c_no').val(data.c_no);
                        $('#country').val(data.country);
                        $('#vat').val(data.NTN);
                        $('#gst').val(data.GST);
                        $('#email').val(data.email);
                        $('#state').val(data.state);
                        $('#pc').val(data.postalCode);
                        $('#city').val(data.city.c_name);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#examples').DataTable({
                    processing: true,
                    serverSide: true,
                    order: [[ 0, "desc" ]],
                    scrollX: true,
                    ajax: '{{route("supplierUpdateRequest.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "name",
                            "defaultContent": ""
                        },
                        {
                            "data": "company",
                            "defaultContent": ""
                        },
                        {
                            "data": "id",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                var sta = row.status;
                                var status = '{{route("customerUpdateRequest.status",[":id"])}}';
                                status = status.replace(':id', row.id );
                                if (sta == 'Pending') {
                                    return `
                                    @if(in_array('show',$permissions))
                                        <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                            <i class="icon-eye"></i>
                                        </button>
                                    @endif
                                    @if(in_array('status',$permissions))
                                        <a href="`+status+`">
                                            <button class="btn btn-sm btn-danger">Approve It</button>
                                        </a>
                                    @else
                                        <button class="btn btn-sm btn-success" disabled>`+row.status +`</button>
                                    @endif
                                    `;
                                }
                                else {
                                    return `
                                    @if(in_array('show',$permissions))
                                        <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                            <i class="icon-eye"></i>
                                        </button>
                                    @endif
                                    <button class="btn btn-sm btn-success" disabled>`+row.status +`</button>
                                    `;
                                }
                            },
                        },
                    ],

                });
                $('#v_name').on('keyup',function(){
                    value = $("#v_name").val();
                    table.columns(1).search(value, true, false).draw();
                }); $('#company').on('keyup',function(){
                    value = $("#company").val();
                    table.columns(2).search(value, true, false).draw();
                });
            });

        </script>
    @endsection
@endsection
