<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
        <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
        <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
            <a href="{{route('pos')}}"  data-close-others="true">
                <i class="fa fa-bars"></i>
                POS
            </a>
        </li>
        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-bell"></i>
                <span class="badge badge-default"> {{auth()->user()->unreadNotifications->count()}}  </span>
            </a>
            <ul class="dropdown-menu">
                <li class="external">
                    <h3>
                        @if (auth()->user()->unreadNotifications->count() == 0)
                            <span class="bold">No pending</span> notifications</h3>
                        @else

                            <span class="bold">{{auth()->user()->unreadNotifications->count()}}  pending</span> notifications</h3>
                        @endif
                    {{-- <a href="page_user_profile_1.html">view all</a> --}}
                </li>
                <li>
                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                        <li>
                            <a href="{{route('markAllRead')}}">
                                <strong>
                                    Mark All As Read
                                </strong>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('viewAllNotifications')}}">
                                <strong>
                                    View All Notifications
                                </strong>
                            </a>
                        </li>
                        @foreach (auth()->user()->unreadNotifications()->get() as $n)
                            @php
                                $timeago = \illuminate\Support\Carbon::createFromTimeStamp(strtotime($n->created_at))->diffForHumans();
                            @endphp
                            <li>
                                <a href="{{route('markReadSingle',["$n->id"])}}">
                                    <span class="time">{{$timeago}}</span>
                                    <span class="details">
                                        <span class="label label-sm label-icon label-success">
                                            <i class="fa fa-check"></i>
                                    </span> {{$n->data['notification']}} </span>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </li>
            </ul>
        </li>
        <!-- END NOTIFICATION DROPDOWN -->

        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" class="img-circle" src="{{url('')}}/style-lik/assets/layouts/layout/img/avatar3_small.jpg" />
                <span class="username username-hide-on-mobile"> {{Auth::user()->name}} </span>
                <i class="fa fa-angle-down"></i>
            </a>
            @php
                $permission = \App\Permissions::where('route', 'users.editProfile')->first();
                $perm = \App\RolePermission::where('r_id',Auth::user()->r_id)
                ->where('p_id', $permission->id)->first();
                $href = url('').'/users/editProfile/'.Auth::user()->id;
            @endphp
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a  {{$perm == null ? 'href=#' : 'href='.$href.''}}>
                        <i class="icon-user"></i> My Profile </a>
                </li>
                {{-- <li>
                    <a href="app_calendar.html">
                        <i class="icon-calendar"></i> My Calendar </a>
                </li> --}}
                {{-- <li>
                    <a href="app_inbox.html">
                        <i class="icon-envelope-open"></i> My Inbox
                        <span class="badge badge-danger"> 3 </span>
                    </a>
                </li> --}}
                {{-- <li>
                    <a href="app_todo.html">
                        <i class="icon-rocket"></i> My Tasks
                        <span class="badge badge-success"> 7 </span>
                    </a>
                </li> --}}
                <li class="divider"> </li>
                {{-- <li>
                    <a href="page_user_lock_1.html">
                        <i class="icon-lock"></i> Lock Screen </a>
                </li> --}}
                <li>
                    <a href="{{route('admin_logout')}}" class="dropdown-item notify-item">
                        <i class="icon-key"></i>
                        <span>Logout</span>
                        {{--v@csrf
                        </form> --}}
                    </a>
                </li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        {{-- <li class="dropdown dropdown-quick-sidebar-toggler">
            <a href="javascript:;" class="dropdown-toggle">
                <i class="icon-logout"></i>
            </a>
        </li> --}}
        <!-- END QUICK SIDEBAR TOGGLER -->
    </ul>
</div>
<!-- END TOP NAVIGATION MENU -->
