 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            @php
            // dd(Auth::user()->r_id);
                // $user = \App\User::where('id',Auth::user()->id)
                // ->first();
                $role_menu_id = \App\RoleMenu::with(['menu.children'])
                ->where('r_id',Auth::user()->r_id)
                ->get()->pluck('m_id')->toArray();
                $menus = \App\UserMenu::whereIn('id',$role_menu_id)->get();
                // dd($menus);
            @endphp
            @foreach ($menus as $kwy => $m)
            @php
                $child_menus[] =  $m->m_id;
            @endphp
            @if ($m->p_id == 0)
                <li class="nav-item  {{Route::currentRouteName() == $m->route ? 'active' : null}} ">
                    <a href="{{!empty($m->route) ? route($m->route) : 'javascript:;'}}" class="nav-link {{!empty($m->route) ? '' : 'nav-toggle'}}">
                        <i class="fa {{$m->icon}}"></i>
                        <span class="title">{{$m->name}}</span>
                        <span class="{{Route::currentRouteName() == $m->route ? 'selected' : 'arrow'}}"></span>
                        <!-- <span class="arrow open"></span> -->
                    </a>
                    @if ($m->name == 'Chart of Accounts')
                        @php
                            $coa = \App\HeadofAccounts::all();
                        @endphp
                        <ul class="sub-menu">
                            @foreach ($coa as $c)
                                @php
                                    $hc = App\HeadCategory::where('a_id',$c->id)
                                    ->get();
                                @endphp
                                <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">{{$c->name}}</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        @foreach ($hc as $h)

                                            <li class="nav-item  ">
                                                <a href="{{url('')}}/headcategory/history/{{$h->code}}/74" class="nav-link ">
                                                    <span class="title">{{$h->name}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    @else

                        <ul class="sub-menu">
                            {{-- {{dd($m->m_id)}} --}}
                            @foreach ($menus as $child_menu)
                            @if ($m->id == $child_menu->p_id)
                                <li class="nav-item ">
                                    <a href="{{route($child_menu->route)}}" class="nav-link">
                                        <span class="title">{{$child_menu->name}}</span>
                                    </a>
                                </li>

                            @endif
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endif

            @endforeach

            {{-- <li class="nav-item {{Route::currentRouteName() == 'headcategory.history' ? 'active' : null}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-500px"></i>
                    <span class="title">Chart of Accounts</span>
                    <span class="{{Route::currentRouteName() == 'headcategory.history' ? 'selected' : "arrow"}}"></span>
                </a>
                <ul class="sub-menu">
                    @foreach ($coa as $c)
                        @php
                            $hc = App\HeadCategory::where('a_id',$c->id)
                            ->get();
                        @endphp
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">{{$c->name}}</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                @foreach ($hc as $h)

                                    <li class="nav-item  ">
                                        <a href="{{url('')}}/headcategory/history/{{$h->code}}" class="nav-link ">
                                            <span class="title">{{$h->name}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </li> --}}

        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
    </div>
                    <!-- END SIDEBAR -->
</div>
                <!-- END SIDEBAR -->

