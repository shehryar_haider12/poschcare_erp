
    <head>
        <meta charset="utf-8" />

        <title> Admin Panel </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        {{-- Image-Upload --}}
        <link rel="stylesheet" href="{{url('')}}/assets/custom/css/image-upload.css">
        {{-- SweetAlert2 --}}
        <link rel="stylesheet" href="{{url('')}}/assets/plugins/sweet-alert2/sweetalert2.min.css">
        {{-- Switchery Css --}}
        <link href="{{url('')}}/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('')}}/style-lik/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{url('')}}/style-lik/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{url('')}}/style-lik/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <!-- <link href="{{url('')}}/style-lik/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="{{url('')}}/style-lik/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" /> -->
        <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        {{-- <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" > --}}
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    <link href="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js">
    <link href="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js">
    {{-- <link rel="stylesheet" href="https://code.jquery.com/jquery-3.5.1.js"> --}}

    {{-- <script src="{{url('')}}/assets/plugins/chart.js/Chart.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
