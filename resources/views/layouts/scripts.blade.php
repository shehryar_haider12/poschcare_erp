<!-- BEGIN CORE PLUGINS -->
<script src="{{url('')}}/style-lik/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        {{-- SweetAlert2 --}}
        <script src="{{url('')}}/assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
        <script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        {{-- Axios --}}
        <script src="{{url('')}}/assets/custom/js/axios.min.js"></script>
        {{-- Switchery --}}
        <script src="{{url('')}}/assets/plugins/switchery/js/switchery.min.js"></script>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{url('')}}/style-lik/assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('')}}/style-lik/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{url('')}}/style-lik/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
         {{-- <script src="{{url('')}}/style-lik/assets/pages/scripts/form-validation.min.js" type="text/javascript"></script> --}}
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <!-- <script src="{{url('')}}/style-lik/assets/global/scripts/datatable.js" type="text/javascript"></script> -->
        <!-- <script src="{{url('')}}/style-lik/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script> -->
        <!-- <script src="{{url('')}}/style-lik/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script> -->
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{url('')}}/style-lik/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/style-lik/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- <script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript"></script> -->

        <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
