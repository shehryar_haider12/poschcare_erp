@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/subcategory">Sub Category</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Sub Category</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-sitemap font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Sub Category</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="subCategoryForm" action="{{$isEdit ? route('subcategory.update',$sub->id) :  route('subcategory.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sub Category Name*</label>
                                        <input value="{{$sub->s_cat_name ?? old('s_cat_name')}}" class="form-control" type="text" placeholder="Enter Sub Category Name" name="s_cat_name" >
                                        <span class="text-danger">{{$errors->first('s_cat_name') ? 'Enter Sub category' : null}}</span>
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : in_array('Add Category',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Category Name*</label>
                                        <select style="overflow-y: scroll;" id="cat_id" size="1" class="form-control selectpicker" data-live-search="true" name="cat_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($cat as $s)
                                                <option {{$s->id == $sub->cat_id ? 'selected' : null}} value="{{$s->id}}">{{$s->cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cat as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('cat_id') ? 'selected' : null}}>{{$s->cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('cat_id') ? 'Select any one' : null}}</span>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Category',$permissions))

                                    <div class="col-sm-1">
                                        <div class="form-control" style=" width:50px; margin-top: 23px;">
                                            <a href="#"  data-toggle="modal" data-target="#catModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.category')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#subCategoryForm').validate({
        rules: {
            s_cat_name: {
                required: true,
            },
            cat_id: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
<script>
    $(document).ready(function(){
        $('#cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#catModal').modal("show"); //Open Modal
            }
        });
    });
</script>
@endsection
