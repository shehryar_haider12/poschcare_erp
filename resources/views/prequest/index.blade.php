@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css

<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="{{url('')}}/request">Purchase Request</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-shopping-cart font-white"></i>View Purchase Requests
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('request.create')}}" class="col-md-2" style="float: right">
                                                    <button  style="background: #00CCFF" type="button"  class="btn btn-block btn-info btn-md ">Add Request</button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Request Date</label>
                                                            <input type="date" id="date"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Request Status</label>
                                                            <select id="status" multiple class="form-control selectpicker">
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                <option value="Pending">Pending</option>
                                                                <option value="Approved">Approved</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th >Request No</th>
                                                            <th>Request Date</th>
                                                            <th>Warehouse</th>
                                                            <th >Request Status</th>
                                                            <th >Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($prequest as $p)
                                                            <tr>
                                                                <td>
                                                                    {{$p->id}}
                                                                </td>
                                                                <td>
                                                                    {{$p->req_date}}
                                                                </td>
                                                                <td>
                                                                    {{$p->warehouse->w_name}}
                                                                </td>
                                                                @if ($p->status == 'Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                    <td>
                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('status',$permissions))
                                                                                <option >Approved</option>
                                                                            @endif
                                                                            @if(in_array('edit',$permissions))
                                                                                <option >Edit Request</option>
                                                                            @endif
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Request Details</option>
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                                @if ($p->status == 'Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                    <td>
                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Request Details</option>
                                                                            @endif
                                                                            @if(in_array('add quotation',$permissions))
                                                                                @if ($p->quotation->isEmpty())
                                                                                <option>Add Quotation</option>

                                                                                @endif
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                @endif

                                                            </tr>

                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Purchase Request</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Request Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="order_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Purchase Request No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="o_no" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="35%">Product Code -  Name</th>
                                            <th>Product Description</th>
                                            <th width="5%">Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->


    @endsection
    @section('custom-script')
        @toastr_js
        @toastr_render

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#example').DataTable({
            scrollX: true
        });
        $('#status').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#status option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#status').selectpicker('deselectAll');
                $('#status').selectpicker('refresh');
            }
            $.each($('#status option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(3).search(search, true, false).draw();
        });
        $('#date').on('change',function(){
            value = $("#date").val();
            table.columns(1).search(value, true, false).draw();
        });

    });
    $(document).on('change','.action',function(){
            var val=$(this).val();
            if(val == 'Approved' )
            {
                var id = $(this).attr('id');
                var status = val;
                axios
                .post('{{route("request.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                });
            }

            if(val == 'Request Details')
            {
                var id=$(this).attr("id");
                $("#example1 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/request/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        $('#order_date').val(data[0].req_date);
                        $('#o_no').val(data[0].id);
                        $('#w_name').val(data[0].warehouse.w_name);
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].variant == null)
                            {
                                $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].products.pro_code+` - `+data[1][i].products.pro_name+`</td><td>`+data[1][i].products.description+`</td><td>`+data[1][i].quantity+`</td></tr>`);
                            }
                            else
                            {
                                $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].variant.name+`</td><td>`+data[1][i].variant.product.description+`</td><td>`+data[1][i].quantity+`</td></tr>`);
                            }
                        }

                        $('#myModal').modal("show");
                        $('.action').val('Actions');
                        $('#example1').DataTable();

                    }
                });
            }
            if(val == 'Edit Request')
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/request/'+id+'/edit';
                // $('.action').val('Actions');
            }

            if(val == 'Add Quotation')
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/quotation/newQuotation/'+id;
                // $('.action').val('Actions');
            }
        });


</script>

    @endsection
@endsection
