@extends('layouts.master')
@section('top-styles')
<style>
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
    .hr {
    display: block;
    border-style: inset;
    border-width: auto;
    }
</style>
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
@section('sidebar-name1')
{{-- @if(Session::has('download'))
         <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif --}}
<li>
    <a href="{{url('')}}/request">Purchase Request</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Purchase Request</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-shopping-cart font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}}  Purchase Request</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('request.update',$req->id) : route('request.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Request Number</label>
                                        <input value="{{$id}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Request Date*</label>
                                        <input value="{{$date ?? old('req_date') ?? $req->req_date}}" class="form-control" type="date" placeholder="Enter Order Date" name="req_date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse*</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="w_id" name="w_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($ware as $s)
                                                <option {{$s->id == $req->w_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->w_name}} - {{$s->w_type}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($ware as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('w_id') ? 'selected' : null}}>{{$s->w_name}} - {{$s->w_type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-11' : in_array('Add Product',$permissions) ? 'col-sm-10' : 'col-sm-11'}}">
                                    <div class="form-outline">
                                        <label >Products*</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="p_id" required>
                                            <option disabled selected>Select..</option>
                                            @foreach ($product as $s)
                                                <option value="{{$s->id}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->unit->u_name}}
                                                 - {{$s->brands->b_name}} - {{$s->category->cat_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Product',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 24px; width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-1">
                                    <div class="form-control" style="margin-top: 24px; width:50px">
                                        <a href="#"  data-toggle="modal" data-target="#allproduct">
                                            <i class="fa fa-2x fa-list addIcon font-green" style="margin-top: 3px"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-outline">
                                        <label>Product Variants</label>
                                        <select class="form-control" data-live-search="true" id="v_id" >
                                            <option disabled selected>Select..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Code - Name - Weight - Brand - Category</th>
                                                <th>In Stock </th>
                                                <th>Quantity</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $a=1;
                                            @endphp
                                            @if ($isEdit)
                                                @foreach ($rdetail as $p)
                                                    @php
                                                        $quantity = 0;
                                                    @endphp
                                                    @if ($p->variant == null)
                                                        @if ($p->products->currentstocks->isEmpty())
                                                            @php
                                                                $quantity = 0;
                                                            @endphp
                                                        @else
                                                            @foreach ($p->products->currentstocks as $d)
                                                                @php
                                                                    $quantity+=$d->quantity;
                                                                @endphp
                                                            @endforeach
                                                        @endif
                                                        <tr>
                                                            <td><input name="p_id[{{$a}}]" type="text" class="form-control pid" readonly="" value="{{$p->p_id}}"></td>
                                                            <td>{{$p->products->pro_code}} - {{$p->products->pro_name}} -
                                                                {{$p->products->unit->u_name}} - {{$p->products->brands->b_name}} -
                                                                {{$p->products->category->cat_name}}
                                                            </td>
                                                            <td><input type="text" class="form-control" readonly="" value="{{$quantity}}({{$p->products->unit->u_name}})">
                                                                <input type="hidden" class="type" name="type[{{$a}}]" value="{{$p->type}}">
                                                            </td>

                                                            <td><input type="number" class="form-control quantity" min="1" id="{{$p->p_id}}" name="quantity[{{$a}}]" value="{{$p->quantity}}"></td>
                                                            <td><button type="button" id="{{$p->p_id}}" class="btn red delete"><i class="fa fa-trash"></i></button></td>
                                                        </tr>
                                                    @else
                                                        @if ($p->variant->currentstocks->isEmpty())
                                                            @php
                                                                $quantity = 0;
                                                            @endphp
                                                        @else
                                                            @foreach ($p->variant->currentstocks as $d)
                                                                @php
                                                                    $quantity+=$d->quantity;
                                                                @endphp
                                                            @endforeach
                                                        @endif
                                                        <tr>
                                                            <td><input name="p_id[{{$a}}]" type="text" class="form-control pid" readonly="" value="{{$p->p_id}}"></td>
                                                            <td>{{$p->variant->name}}
                                                            </td>
                                                            <td><input type="text" class="form-control" readonly="" value="{{$quantity}}">
                                                                <input type="hidden" class="type" name="type[{{$a}}]" value="{{$p->type}}">
                                                            </td>

                                                            <td><input type="number" class="form-control quantity" min="1" id="{{$p->p_id}}" name="quantity[{{$a}}]" value="{{$p->quantity}}"></td>
                                                            <td><button type="button" id="{{$p->p_id}}" class="btn red delete"><i class="fa fa-trash"></i></button></td>
                                                        </tr>
                                                    @endif
                                                    @php
                                                      $a++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@section('modal')
    @include('modals.allproducts')
    @include('modals.product')
@endsection

@endsection
@section('custom-script')
@toastr_js
@toastr_render

<script>
    $(document).ready(function() {
    var table = $('#example5').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{route("product.datatable")}}',
        "columns": [{
                "data": "",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    return  ` P - ` +row.id;
                },
            },
            {
                "data": "pro_name",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    return row.pro_code+ ` - ` +row.pro_name ;
                },
            },
            {
                "data": "",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if(row.weight == null || row.weight == 0)
                    {
                        return row.unit.u_name ;
                    }
                    else
                    {
                        return row.weight+``+row.unit.u_name ;
                    }
                },
            },
            {
                "data": "brands.b_name",
                "defaultContent": ""
            },
            {
                "data": "category.cat_name",
                "defaultContent": ""
            },
            {
                "data": "id",
                "defaultContent": ""
            },
        ],
        "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            },
            {
                "targets": 0,
                "render": function (data, type, row, meta) {
                    return meta.row + 1;
                },
            },
            {
                "targets": -1,
                "render": function (data, type, row, meta) {
                    return ` <button type="button"  class="btn green add" id="add.`+row.id +`">
                        <i class="fa fa-plus"></i>
                    </button>`;
                },
            },
        ],
    });

    $('#example5').on('click','.add',function(){
        var rowCount = $('#example tr').length;
        var id=$(this).attr('id');
        id = id.split('.')
        var w_id = $('#w_id').val();
        $.ajax({
            url:"{{url('')}}/product/sales/"+id[1]+'.'+w_id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                if(data.total_quantity == null)
                {
                    total_quantity=0;
                }
                else
                {
                    total_quantity = data.total_quantity;
                }

                $("#allproduct").modal('hide');
                if(data.vstatus == 0)
                {
                    if($('#'+data.id).length && data.vstatus == 0)
                    {

                    }
                    else
                    {
                        $("#example").append(`<tr><td><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'></td><td>`+data.pro_code+` - `+data.pro_name+` - `+data.unit.u_name+` - `+data.brands.b_name+` - `+data.category.cat_name+`</td><td><input type='text' class='form-control' readonly  value='`+total_quantity+`(`+data.unit.u_name+`)'><input type="hidden" class="type" name="type[`+rowCount+`]" value="0"></td><td><input type='number' class='form-control quantity' min='1' required id='`+data.id+`' name='quantity[`+rowCount+`]' value='1'></td><td><button type='button' id='`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);

                    }
                }
                else
                {
                    $('#v_id').empty();
                    $('#v_id').selectpicker('destroy');
                    $('#v_id').append(`<option disabled selected>Select..</option>`);
                    for (let index = 0; index < data.variants.length; index++) {
                        $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                    }
                    $('#v_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
            }
        });
    });
});
$(document).on('change','#p_id',function(){
    var rowCount = $('#example tr').length;
    // console.log(rowCount);
    var id=$(this).val();
    var w_id = $('#w_id').val();
    $.ajax({
        url:"{{url('')}}/product/sales/"+id+'.'+w_id,
        method:"GET",
        error: function (request, error) {
                    alert(" Can't do because: " + error +request);
                },
        success:function(data){
            // console.log(data);
            if(data.total_quantity == null)
            {
                total_quantity=0;
            }
            else
            {
                total_quantity = data.total_quantity;
            }

            if(data.vstatus == 0)
            {
                if($('#'+data.id).length && data.vstatus == 0)
                {
                    console.log('fyyty');
                }
                else
                {
                    $("#example").append(`<tr><td><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'></td><td>`+data.pro_code+` - `+data.pro_name+` - `+data.unit.u_name+` - `+data.brands.b_name+` - `+data.category.cat_name+`</td><td><input type='text' class='form-control' readonly  value='`+total_quantity+`(`+data.unit.u_name+`)'><input type="hidden" class="type" name="type[`+rowCount+`]" value="0"></td><td><input type='number' class='form-control quantity' min='1' required id='`+data.id+`' name='quantity[`+rowCount+`]' value='1'></td><td><button type='button' id='`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
                }
            }
            else
            {
                $('#v_id').empty();
                $('#v_id').selectpicker('destroy');
                $('#v_id').append(`<option disabled selected>Select..</option>`);
                for (let index = 0; index < data.variants.length; index++) {
                    $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                }
                $('#v_id').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        }
    });
});

$('table').on('click', '.delete', function(e){
    var id=$(this).attr('id');
    $(this).closest('tr').remove();
    $(".pid").each(function (i){
        i=+i + +1;
        $(this).attr('name','p_id['+i+']');
    });
    $(".type").each(function (i){
        i=+i + +1;
        $(this).attr('name','type['+i+']');
    });
    $(".quantity").each(function (i){
        i=+i + +1;
        $(this).attr('name','quantity['+i+']');
    });
    $(".delete").each(function (i){
        i=+i + +1;
        $(this).attr('id',i);
    });
});


$(document).on('change','#v_id',function(){
    var rowCount = $('#example tr').length;
    var id=$(this).val();
    var w_id = $('#w_id').val();
    $.ajax({
        url:"{{url('')}}/product/Pvariants/"+id+'.'+w_id,
        method:"GET",
        error: function (request, error) {
                    alert(" Can't do because: " + error +request);
                },
        success:function(data){
            // console.log(data);
            if(data.total_quantity == null)
            {
                total_quantity=0;
            }
            else
            {
                total_quantity = data.total_quantity;
            }
            if($('#'+data.id).length && data.product.vstatus == 1)
            {

            }
            else
            {
                $("#example").append(`<tr><td><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'> </td><td>`+data.name+`</td><td><input type='text' class='form-control' readonly  value='`+total_quantity+`'><input type="hidden" class="type" name="type[`+rowCount+`]" value="1"></td><td><input type='number' class='form-control quantity' min='1' required id='`+data.id+`' name='quantity[`+rowCount+`]' value='1'></td><td><button type='button' id='`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
            }
        }
    });
});

</script>
@endsection
