<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    @include('layouts.head')
    {{-- @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"]) --}}
    @yield('top-styles')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        div#example_filter {
            float: right !important;
        }
        .pagination{
            float: right !important;
        }
        .box:hover {
            border-color: red;
        }

    </style>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white">
        <div class="w3-sidebar w3-bar-block w3-card w3-animate-right" style="display:none;right:0; margin-top: 50px; width:550px;" id="categorybar">
            <button onclick="cat_close()" class="w3-bar-item w3-button w3-large"> &times;</button>
            <div class="row">
                @foreach ($cat as $c)
                    <div class="col-sm-4 col-md-offset-1 hover_cate" style="border: 1px solid;" onclick="category({{$c->id}})">
                    <table class="table table-hover table-fixed">
                        <tr class="box" style="border-style: groove;">
                            {{$c->cat_name}}
                        </tr>
                    </table>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="w3-sidebar w3-bar-block w3-card w3-animate-right" style="display:none;right:0; margin-top: 50px; width:550px;" id="brandbar">
            <button onclick="brand_close()" class="w3-bar-item w3-button w3-large"> &times;</button>
            <div class="row">
                @foreach ($brand as $c)
                    <div class="col-sm-4 col-md-offset-1 hover_brand" style="border: 1px solid;" onclick="brand({{$c->id}})">
                    <table class="table table-hover table-fixed">
                    <tr class="box" style="border-style: groove;">
                            {{$c->b_name}}
    </tr>
    </table>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="{{url('')}}/dashboard">
                            <img src="{{url('')}}/uploads/posch.jpg"  style="margin-top: -6px;" width="110px" height="40px" alt="logo" class="logo-default" /> </a>
                        {{-- <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div> --}}
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                   <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="{{route('pos')}}"  data-close-others="true">
                                    <i class="fa fa-bars"></i>
                                    POS
                                </a>
                            </li>
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> {{auth()->user()->unreadNotifications->count()}}  </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            @if (auth()->user()->unreadNotifications->count() == 0)
                                                <span class="bold">No pending</span> notifications</h3>
                                            @else

                                                <span class="bold">{{auth()->user()->unreadNotifications->count()}}  pending</span> notifications</h3>
                                            @endif
                                        {{-- <a href="page_user_profile_1.html">view all</a> --}}
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <li>
                                                <a href="{{route('markAllRead')}}">
                                                    <strong>
                                                        Mark All As Read
                                                    </strong>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('viewAllNotifications')}}">
                                                    <strong>
                                                        View All Notifications
                                                    </strong>
                                                </a>
                                            </li>
                                            @foreach (auth()->user()->unreadNotifications()->get() as $n)
                                                @php
                                                    $timeago = \illuminate\Support\Carbon::createFromTimeStamp(strtotime($n->created_at))->diffForHumans();
                                                @endphp
                                                <li>
                                                    <a href="{{route('markReadSingle',["$n->id"])}}">
                                                        <span class="time">{{$timeago}}</span>
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success">
                                                                <i class="fa fa-check"></i>
                                                        </span> {{$n->data['notification']}} </span>
                                                    </a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->

                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="{{url('')}}/style-lik/assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"> {{Auth::user()->name}} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                @php
                                    $permission = \App\Permissions::where('route', 'users.editProfile')->first();
                                    $perm = \App\RolePermission::where('r_id',Auth::user()->r_id)
                                    ->where('p_id', $permission->id)->first();
                                    $href = url('').'/users/editProfile/'.Auth::user()->id;
                                @endphp
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a  {{$perm == null ? 'href=#' : 'href='.$href.''}}>
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    {{-- <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li> --}}
                                    {{-- <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li> --}}
                                    {{-- <li>
                                        <a href="app_todo.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li> --}}
                                    <li class="divider"> </li>
                                    {{-- <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li> --}}
                                    <li>
                                        <a href="{{route('logout')}}" class="dropdown-item notify-item"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i>
                                            <span>Logout</span>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            {{-- <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li> --}}
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->

                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <form action="{{route('stockout.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                {{-- <div class="page-content-wrapper"> --}}
                    <!-- BEGIN CONTENT BODY -->

                    <div class="page-content" >

                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="#">POS</a>
                                </li>
                            </ul>

                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <!-- Begin: life time stats -->
                                <div class="portlet light portlet-fit portlet-datatable bordered">
                                    <div class="portlet-title">

                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <select required class="form-control selectpicker" data-live-search="true" name="b_id" id="">
                                                    <option value=""  selected disabled>Select Biller</option>
                                                    @foreach ($biller as $b)
                                                        <option value="{{$b->id}}">{{$b->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <select required class="form-control selectpicker" data-live-search="true" name="c_id" id="">
                                                    <option value=""  selected disabled>Select Customer</option>
                                                    @foreach ($customer as $b)
                                                        <option value="{{$b->id}}">{{$b->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="form-control selectpicker"  data-live-search="true" id="p_id">
                                                    <option value=""  selected disabled>Select Product</option>
                                                    @foreach ($product as $b)
                                                        <option value="{{$b->id}}">{{$b->pro_code}} - {{$b->pro_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text" autofocus class="form-control" id="pid">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12 table-responsive text-nowrap">
                                                <table id="example" class="table table-striped table table-hover table-fixed"  style="width: 100%; ">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Code - Name</th>
                                                            <th>Price</th>
                                                            <th>Quantity</th>
                                                            <th>Discount In %</th>
                                                            <th>Discount In Amount</th>
                                                            <th>Sub total</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="height: 120px; overflow-y: scroll">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table id="example2" class="table table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Items</th>
                                                            <th>Total</th>
                                                            {{-- <th>Discount
                                                                <a data-toggle="modal" data-target="#discountAmount"><i class="fa fa-edit"></i>Amount</a>
                                                                 <a data-toggle="modal" data-target="#discountPercent"><i class="fa fa-edit"></i>Percent</a>
                                                            </th> --}}
                                                            <th>Tax <a data-toggle="modal" data-target="#Tax"><i class="fa fa-edit"></i></a></th>
                                                            <th>Grand Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            {{-- <td>0</td> --}}
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <button style="background: #0066cc; width: 70%;color:white;" type="button" class=" btn payment-btn btn-lg" data-toggle="modal" data-target="#credit" id="credit-card-btn"><i class="fa fa-credit-card"></i> Card</button>
                                            </div>
                                            <div class="col-md-4 ">
                                                <button style="background: #47d147;width: 70%;color:white;" type="button" class=" btn payment-btn btn-lg" data-toggle="modal" data-target="#cash" id="cash-btn"><i class="fa fa-money"></i> Cash</button>
                                            </div>
                                            <div class="col-md-4">
                                                <button style="background-color: #cc0000;width: 70%;color:white;" type="button" class="btn btn-lg" id="cancel-btn" onclick="return confirmCancel()"><i class="fa fa-close"></i> Cancel</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <!-- Begin: life time stats -->
                                <div class="portlet light portlet-fit portlet-datatable bordered">
                                    <div class="portlet-title">

                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button style="background: #34495E; width: 70%;color:white;" onclick="cat_open()" type="button"  class="btn btn-info">
                                                    <span>Category</span>
                                                </button>
                                                {{-- <button style="background: #34495E; width: 70%;color:white;" type="button" class=" btn payment-btn btn-md" data-toggle="modal" data-target="#credit" id="credit-card-btn">Category</button> --}}
                                            </div>
                                            <div class="col-md-6 ">
                                                <button style="background: #17A2B8;width: 70%;color:white;" type="button" class=" btn btn-md" onclick="brand_open()"> Brand</button>
                                            </div>
                                            {{-- <div class="col-md-4">
                                                <button style="background-color: #cc0000;width: 70%;color:white;" type="button" class="btn btn-md" id="cancel-btn" >Featured</button>
                                            </div> --}}
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table id="example3" class="table " style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="portlet light portlet-fit portlet-datatable bordered">
                                    <div class="portlet-title">
                                        Payment History
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table id="example4" class="table " style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Paid By</th>
                                                            <th>Received Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-0 " style="margin-top: 115px">
                                             <button type="submit" class="btn green">Submit</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <input class="form-control" type="hidden" min="1" name="actual" id="actual2">
                    <input class="form-control" type="hidden" min="1" name="discount" id="discount">
                    <input class="form-control" type="hidden" min="1" name="tax" id="taxFinal">

                {{-- </div> --}}
            </div>
            <div id="cash" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Add Payment</h4>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Total Amount</label>
                                        <input class="form-control" type="text" min="1"  readonly id="actual">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Received Amount<span class="text-danger">*</span></label>
                                        <input required class="form-control" type="text" min="1"  id="p_total">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label>Return Amount</label>
                                        <input name="return_amount" class="form-control" type="text" min="1" readonly id="ramount">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-offset-0 col-md-12">
                                    <button type="button" class="btn btn-info cashSubmit">Submit</button>
                                </div>
                            </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            <div id="credit" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Add Payment</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Total Amount</label>
                                        <input class="form-control" type="number" min="1" readonly id="actual1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Received Amount*</label>
                                        <input required class="form-control" type="number" min="1" id="p_total1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Credit Card Number*</label>
                                        <input class="form-control" type="text" placeholder="Enter Credit Card Number" id="cc_no" name="cc_no">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Holder Name*</label>
                                        <input class="form-control" type="text" placeholder="Enter Holder Name" id="cc_holder" name="cc_holder">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Bank*</label>
                                        <select class="form-control selectpicker" id="bank_id" data-live-search="true" name="bank_id">
                                            <option value="" disabled selected>Select...</option>
                                                @foreach ($bank as $s)
                                                <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Merchant Type</label>
                                        <select name="merchant_type" class="form-control selectpicker" id="">
                                            <option selected disabled>Select</option>
                                            <option value="Jazz Cash">Jazz Cash</option>
                                            <option value="Easy Paisa">Easy Paisa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Payment Note <small>(optional)</small> </label>
                                        <textarea id="note" class="form-control">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Sale Note <small>(optional)</small> </label>
                                        <textarea  id="salenote" class="form-control">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-0 col-md-12">
                                    <button type="button" class="btn green creditSubmit">Submit</button>
                                </div>
                            </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            <!-- END CONTENT BODY -->
        </div>
    </form>


<div id="discountAmount" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Discount In Amount</h4>
            </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Amount</label>
                                <input class="form-control" id="disAmount" type="text" placeholder="Enter Amount" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="button" class="btn green disAmount">Submit</button>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="discountPercent" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Discount In Percent</h4>
            </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Percentage</label>
                                <input class="form-control" id="disPercent" type="text" placeholder="Enter Percentage" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="button" class="btn green disPercent">Submit</button>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div id="Tax" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Tax</h4>
            </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Tax In %</label>
                                <input class="form-control" id="taxP" type="text" placeholder="Enter Amount">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="button" class="btn green Tax">Submit</button>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    {{-- @include('layouts.footer') --}}
</div>

<div class="quick-nav-overlay"></div>

@include('layouts.scripts')
<script type="text/javascript" src="{{url('')}}/style-lik/jQuery-Scanner-Detection-master/jquery.scannerdetection.js"></script>
<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>
<script>

    function category(id)
    {
        $('#example3 tbody').empty();
        $.ajax({
            url:"{{url('')}}/getproduct/0/"+id,
            method:"GET",
            dataType: 'JSON',
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                // var data = ['asdf', 'qwer', 'rtyu', 'tyui', 'yuio', 'dfgh', 'zxcv', 'xcvb', '1234', 'isdfgt', 'foo'];

                document.getElementById("categorybar").style.display = "none";
                var tbl = $('#example3 tbody');
                for (var i = 0; i <  data.product.length / 1; i++)
                {
                    var nextRow = $('<tr>');
                    $(tbl).append(nextRow);

                    var nextItem = 1 * i; // starts each row 1 elements more into the array
                    for (var j = 0; j < 1 && nextItem + j <  data.product.length; j++) // stops when the array ends
                    {
                        var nextRow = $('<tr>');
                        $(tbl).append(nextRow);
                        var nextCell = $('<td onclick="add('+data.product[j].id+')" style=" border-style: groove;cursor:pointer" class="box">');
                        console.log(data.product[j].pro_name);
                        $(nextCell).html(data.product[j].pro_name); // add the next array item
                        $(nextRow).append(nextCell);
                        console.log(j);
                    }
                }
            }
        });
    }

    function brand(id)
    {
        $('#example3 tbody').empty();
        $.ajax({
            url:"{{url('')}}/getproduct/"+id+"/0",
            method:"GET",
            dataType: 'JSON',
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                document.getElementById("brandbar").style.display = "none";
                var tbl = $('#example3 tbody');
                for (var i = 0; i < data.product.length / 1; i++)
                {
                    var nextRow = $('<tr>');
                    $(tbl).append(nextRow);

                    var nextItem = 1 * i; // starts each row 1 elements more into the array
                    for (var j = 0; j < 1 && nextItem + j < data.product.length; j++) // stops when the array ends
                    {
                        var nextCell = $('<td onclick="add('+data.product[nextItem + j].id+')" style=" border-style: groove;cursor: pointer;" class="box">');
                        console.log(data.product[nextItem + j].pro_name);
                        $(nextCell).html(data.product[nextItem + j].pro_name); // add the next array item
                        $(nextRow).append(nextCell);
                    }
                }
            }
        });
    }

    function add(id,e)
    {
        var rowCount = $('#example tr').length ;
        $.ajax({
            url:"{{url('')}}/pos-detail/product/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                if($('#pro'+data.id).length){
                    var row = $('#pro'+data.id);
                    var qty = row.find("td .quantity").val();
                    var price = row.find("td .price").val();
                    var rate = row.find("td .discountr").val();
                    var percent = row.find("td .discountp").val();
                    // con
                    if(data.total_quantity == qty)
                    {
                        alert('Not Enough Stock');
                    }
                    else
                    {

                        qty = +qty + +1;
                        if(rate == '')
                        {
                            var sub= qty*price;
                        }
                        if(rate != '')
                        {
                            var disc = (qty * price * percent) / 100;
                            $('.rate'+id).val(disc);
                            var sub = (qty * price) - disc;
                        }
                        row.find("td .quantity").val(qty.toFixed(2));
                        row.find("td .abc").val(sub.toFixed(2));

                        var amount=0;
                        var qnty=0;
                        var rowCount1 = $('#example tr').length ;
                        var rowCount2 = $('#example tr').length - 1;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            qnty = +qnty + +$('.quantity'+i).val();
                        }
                        $('#example2 tbody').empty();
                        $('#example2').append(`<tr><td>`+rowCount2+`(`+qnty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
                        $('#actual').val(amount.toFixed(2));
                        $('#actual2').val(amount.toFixed(2));
                        $('#p_total').val(amount.toFixed(2));
                        $('#actual1').val(amount.toFixed(2));
                        $('#p_total1').val(amount.toFixed(2));
                    }
                }
                else
                {
                    if(data.total_quantity == 0 || data.total_quantity == null)
                    {
                        alert('Not Enough Stock');
                    }
                    else
                    {

                        $("#example").append("<tr id='pro"+data.id+"'><td><input name='p_id[]' type='text' class='form-control' readonly value='"+data.id+"' style='width:60px'></td><td>"+data.pro_code+" - "+data.pro_name+"</td><td><input style='width:auto' type='number' class='form-control quantity quantity"+rowCount+"' required min='1' id='"+rowCount+"' max='"+data.total_quantity+"' name='quantity[]' value='1'></td><td><input style='width:auto' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data.price+"'></td><td><input style='width:auto' type='hidden' name='type[]' value='"+data.vstatus+"'><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:auto' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:auto' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data.price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");

                        var amount=0;
                        var qty=0;
                        var rowCount1 = $('#example tr').length ;
                        var rowCount2 = $('#example tr').length - 1;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            qty = +qty + +$('.quantity'+i).val();
                        }
                        $('#example2 tbody').empty();
                        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
                        $('#actual').val(amount.toFixed(2));
                        $('#p_total').val(amount.toFixed(2));
                        $('#actual1').val(amount.toFixed(2));
                        $('#p_total1').val(amount.toFixed(2));
                        $('#actual2').val(amount.toFixed(2));
                    }
                }
            }
        });
    }

    function cat_open() {
        document.getElementById("categorybar").style.display = "block";
    }

    function cat_close() {
        document.getElementById("categorybar").style.display = "none";
    }

    function brand_open() {
        document.getElementById("brandbar").style.display = "block";
    }

    function brand_close() {
        document.getElementById("brandbar").style.display = "none";
    }

    $(document).on('change','#p_id',function(){
        var id=$(this).val();
        var rowCount = $('#example tr').length ;
        $.ajax({
            url:"{{url('')}}/pos-detail/product/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                if($('#pro'+data.id).length){
                    var row = $('#pro'+data.id);
                    var qty = row.find("td .quantity").val();
                    var price = row.find("td .price").val();
                    var rate = row.find("td .discountr").val();
                    var percent = row.find("td .discountp").val();
                    if(data.total_quantity == qty)
                    {
                        alert('Not Enough Stock');
                    }
                    else{

                        qty = +qty + +1;
                        if(rate == '')
                        {
                            var sub= qty*price;
                        }
                        if(rate != '')
                        {
                            var disc = (qty * price * percent) / 100;
                            $('.rate'+id).val(disc);
                            var sub = (qty * price) - disc;
                        }
                        row.find("td .quantity").val(qty);
                        row.find("td .abc").val(sub.toFixed(2));

                        var amount=0;
                        var qnty=0;
                        var rowCount1 = $('#example tr').length ;
                        var rowCount2 = $('#example tr').length - 1;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            qnty = +qnty + +$('.quantity'+i).val();
                        }
                        $('#example2 tbody').empty();
                        $('#example2').append(`<tr><td>`+rowCount2+`(`+qnty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
                        $('#actual').val(amount.toFixed(2));
                        $('#p_total').val(amount.toFixed(2));
                        $('#actual1').val(amount.toFixed(2));
                        $('#p_total1').val(amount.toFixed(2));
                        $('#actual2').val(amount.toFixed(2));
                    }
                }
                else
                {
                    if(data.total_quantity == 0 || data.total_quantity == null)
                    {
                        alert('Not Enough Stock');
                    }
                    else
                    {

                        $("#example").append("<tr id='pro"+data.id+"'><td><input style='width:60px' name='p_id[]' type='text' class='form-control' readonly value='"+data.id+"'></td><td>"+data.pro_code+" - "+data.pro_name+"</td><td><input style='width:auto' type='number' class='form-control quantity quantity"+rowCount+"' required min='1' max='"+data.total_quantity+"' id='"+rowCount+"' name='quantity[]' value='1'></td><td><input required style='width:auto' type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data.price+"'></td><td><input style='width:auto' type='hidden' name='type[]' value='"+data.vstatus+"'><input style='width:auto' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width:auto' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width:auto' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data.price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");

                        var amount=0;
                        var qty=0;
                        var rowCount1 = $('#example tr').length ;
                        var rowCount2 = $('#example tr').length - 1;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            qty = +qty + +$('.quantity'+i).val();
                        }
                        $('#example2 tbody').empty();
                        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
                        $('#actual').val(amount.toFixed(2));
                        $('#p_total').val(amount.toFixed(2));
                        $('#actual1').val(amount.toFixed(2));
                        $('#p_total1').val(amount.toFixed(2));
                        $('#actual2').val(amount.toFixed(2));
                    }
                }
            }
        });
    });

    $('table').on('click', '.delete', function(e){
        var id=$(this).attr('id');
        var price=$('#price'+id).val();
        var sub=$('#sub'+id).val();
        var amount=0;
        var qty=0;
        var rowCount1 = $('#example tr').length;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
        }
        amount = -sub - -amount;
        $(this).closest('tr').remove();
        $(".abc").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'sub'+(i+1));
           $(this).removeClass('sub'+(i+2));
           $(this).addClass('sub'+(i+1));
        });
        $(".price").each(function (i){
            $(this).removeAttr('data-id');
            $(this).attr('data-id', (i+1));
            $(this).removeAttr('id');
            $(this).attr('id', 'price'+(i+1));
           $(this).removeClass('price'+(i+2));
           $(this).addClass('price'+(i+1));
        });
        $(".quantity").each(function (i){
           $(this).removeClass('quantity'+(i+2));
           $(this).addClass('quantity'+(i+1));
           $(this).removeAttr('id');
            $(this).attr('id', (i+1));
        });
        $(".delete").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });
        $(".discountp").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'percent'+(i+1));

            $(this).removeAttr('data-id');
            $(this).attr('data-id', (i+1));
           $(this).removeClass('percent'+(i+2));
           $(this).addClass('percent'+(i+1));
        });
        $(".discountr").each(function (i){
            $(this).removeAttr('id');
            $(this).attr('id', 'rate'+(i+1));
            $(this).removeAttr('data-id');
            $(this).attr('data-id', (i+1));
           $(this).removeClass('rate'+(i+2));
           $(this).addClass('rate'+(i+1));
        });
        var rowCount3 = $('#example tr').length ;
        for (let i = 1; i < rowCount3; i++) {
            qty = +qty + +$('.quantity'+i).val();
        }
        var rowCount2 = $('#example tr').length -1;
        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
        $('#actual').val(amount.toFixed(2));
        $('#p_total').val(amount.toFixed(2));
        $('#actual1').val(amount.toFixed(2));
        $('#p_total1').val(amount.toFixed(2));
        $('#actual2').val(amount.toFixed(2));
    });

    $('table').on('keyup','.quantity',function(){
        var a=$(this).val();
        var id=$(this).attr('id');
        var price=$('.price'+id).val();
        var rate = $('.rate'+id).val();
        var percent = $('.percent'+id).val();
        if(rate == '')
        {
            var sub= a*price;
        }
        if(rate != '')
        {
            var disc = (a * price * percent) / 100;
            $('.rate'+id).val(disc);
            var sub = (a * price) - disc;
        }

        $('.sub'+id).val(sub.toFixed(2));
        var amount=0;
        var qty=0;
        var rowCount = $('#example tr').length ;
        var rowCount2 = $('#example tr').length - 1;
        for (let i = 1; i < rowCount; i++) {
            amount = +amount + +$('.sub'+i).val();
            qty = +qty + +$('.quantity'+i).val();
        }
        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
        $('#actual').val(amount.toFixed(2));
        $('#p_total').val(amount.toFixed(2));
        $('#actual1').val(amount.toFixed(2));
        $('#p_total1').val(amount.toFixed(2));
        $('#actual2').val(amount.toFixed(2));
    });

    $('table').on('keyup','.price',function(){
        var id=$(this).attr('data-id');
        var price=$(this).val();
        var quan=$('.quantity'+id);
        quan=quan.val();

        var rate = $('.rate'+id).val();
        var percent = $('.percent'+id).val();
        if(rate == '')
        {
            var sub=quan * price;
        }
        if(rate != '')
        {
            var disc = (quan * price * percent) / 100;
            $('.rate'+id).val(disc);
            var sub = (quan * price) - disc;
        }
        $('.sub'+id).val(sub.toFixed(2));
        var amount=0;
        var qty=0;
        var rowCount1 = $('#example tr').length ;
        var rowCount2 = $('#example tr').length -1;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            qty = +qty + +$('.quantity'+i).val();
        }

        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
        $('#actual').val(amount.toFixed(2));
        $('#p_total').val(amount.toFixed(2));
        $('#actual1').val(amount.toFixed(2));
        $('#p_total1').val(amount.toFixed(2));
        $('#actual2').val(amount.toFixed(2));
    });


    $('table').on('keyup','.discountp',function(){
        var id=$(this).attr('data-id');
        var percent=$(this).val();
        var price=$('.price'+id).val();
        var quan=$('.quantity'+id);
        quan=quan.val();
        console.log(price,quan,percent);
        var sub=quan * price;
        var disc = (sub * percent)/100;
        sub = sub - disc;
        $('.sub'+id).val(sub.toFixed(2));
        $('.rate'+id).val(disc.toFixed(2));
        var amount=0;
        var qty=0;
        var rowCount1 = $('#example tr').length;
        var rowCount2 = $('#example tr').length -1;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            qty = +qty + +$('.quantity'+i).val();
        }

        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
        $('#actual').val(amount.toFixed(2));
        $('#p_total').val(amount.toFixed(2));
        $('#actual1').val(amount.toFixed(2));
        $('#p_total1').val(amount.toFixed(2));
        $('#actual2').val(amount.toFixed(2));
    });


    $('table').on('keyup','.discountr',function(){
        var id=$(this).attr('data-id');
        var disc=$(this).val();
        var price=$('.price'+id).val();
        var quan=$('.quantity'+id);
        quan=quan.val();
        var sub=quan * price;
        var percent = Math.round((disc * 100) /sub);
        sub = sub - disc;
        $('.sub'+id).val(sub.toFixed(2));
        $('.percent'+id).val(percent.toFixed(2));
        var amount=0;
        var qty=0;
        var rowCount1 = $('#example tr').length;
        var rowCount2 = $('#example tr').length -1;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            qty = +qty + +$('.quantity'+i).val();
        }

        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount.toFixed(2)+`</td><td>0</td><td>`+amount.toFixed(2)+`</td></tr>`);
        $('#actual').val(amount.toFixed(2));
        $('#p_total').val(amount.toFixed(2));
        $('#actual1').val(amount.toFixed(2));
        $('#p_total1').val(amount.toFixed(2));
        $('#actual2').val(amount.toFixed(2));
    });

    function confirmCancel()
    {
        $('#example tbody').empty();
        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>0</td><td>0</td><td>0</td><td>0</td></tr>`);
        $('#p_total').val('');
        $('#actual').val('');
        $('#note').val('');
        $('#salenote').val('');
    }

    $(document).on('click','.cashSubmit',function(){

        var amount = $('#p_total').val();

        var rowCount = $('#example4 tr').length ;
        $('#example4').append(`<tr><td><input type="hidden"  name="paid_by[]" value="Cash"> Cash</td><input type="hidden" name="total[]" class="cc`+rowCount+`" value="`+amount+`"> <td>`+amount+`</td></tr>`);
        $('#cash').modal("hide");

    });
    $(document).on('click','.creditSubmit',function(){
        var amount = $('#p_total1').val();
        var rowCount = $('#example4 tr').length ;
        $('#example4').append(`<tr><td><input type="hidden" name="paid_by[]" value="Credit Card"> Credit Card</td><input type="hidden" name="total[]" class="cc`+rowCount+`" value="`+amount+`"> <td>`+amount+`</td></tr>`);
        $('#credit').modal("hide");
    });

    $('#p_total').on('keyup',function(){
        var actual = $('#actual').val();
        var rcv = $(this).val();
        if(parseInt(rcv) > parseInt(actual) )
        {
            var total = rcv - actual;
            $('#ramount').val(total.toFixed(2));
        }
        else
        {
            $('#ramount').val('');
        }
    })

    // $(document).on('click','.disAmount',function(){
    //     var disamount = $('#disAmount').val();
    //     $('#discount').val(disamount);
    //     var total = 0;
    //     var qty=0;
    //     var rowCount1 = $('#example tr').length;
    //     var rowCount2 = $('#example tr').length -1;
    //     for (let i = 1; i < rowCount1; i++) {
    //         total = +total + +$('.sub'+i).val();
    //         qty = +qty + +$('.quantity'+i).val();
    //     }
    //     total = total - disamount;
    //     $('#example2 tbody').empty();
    //     $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+total+`</td><td>0</td><td>0</td><td>`+total+`</td></tr>`);
    //     $('#actual').val(total);
    //     $('#p_total').val(total);
    //     $('#actual1').val(total);
    //     $('#p_total1').val(total);
    //     $('#actual2').val(total);
    //     $('#discountAmount').modal('hide');
    // });

    // $(document).on('click','.disPercent',function(){
    //     var disPercent = $('#disPercent').val();
    //     $('#discount').val(disPercent);
    //     var total = 0;
    //     var qty=0;
    //     var rowCount1 = $('#example tr').length;
    //     var rowCount2 = $('#example tr').length -1;
    //     for (let i = 1; i < rowCount1; i++) {
    //         total = +total + +$('.sub'+i).val();
    //         qty = +qty + +$('.quantity'+i).val();
    //     }
    //     var amount = (total * disPercent);
    //     amount = amount / 100;
    //     total = total - amount;
    //     $('#example2 tbody').empty();
    //     $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+total+`</td><td>0</td><td>0</td><td>`+total+`</td></tr>`);
    //     $('#actual').val(total);
    //     $('#p_total').val(total);
    //     $('#actual1').val(total);
    //     $('#p_total1').val(total);
    //     $('#actual2').val(total);
    //     $('#discountPercent').modal('hide');
    // });


    $(document).on('click','.Tax',function(){
        var taxP = $('#taxP').val();
        $('#taxFinal').val(taxP);
        var total = 0;
        var qty=0;
        var rowCount1 = $('#example tr').length;
        var rowCount2 = $('#example tr').length -1;
        for (let i = 1; i < rowCount1; i++) {
            total = +total + +$('.sub'+i).val();
            qty = +qty + +$('.quantity'+i).val();
        }
        var amount = (total * taxP);
        amount = amount / 100;
        total = +total + +amount;
        $('#example2 tbody').empty();
        $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+total.toFixed(2)+`</td><td>0</td><td>`+total.toFixed(2)+`</td></tr>`);
        $('#actual').val(total.toFixed(2));
        $('#p_total').val(total.toFixed(2));
        $('#actual1').val(total.toFixed(2));
        $('#p_total1').val(total.toFixed(2));
        $('#actual2').val(total.toFixed(2));
        $('#Tax').modal('hide');
    });

    $('#cash').on('show.bs.modal', function (e) {
        var rowCount = $('#example4 tr').length ;
        var paid = 0;
        var amount = $('#actual').val();
        if(rowCount > 1)
        {
            for (let i = 1; i <= rowCount - 1; i++) {
                paid = +paid + +$('.cc'+i).val();
            }
            if(amount > paid)
            {
                amount = amount - paid;
                $('#p_total').val(amount.toFixed(2));
                $('.cashSubmit').prop('disabled',false);
            }
            else
            {
                alert('payment is complete');
                amount = amount - paid;
                $('#p_total').val(amount.toFixed(2));
                $('.cashSubmit').prop('disabled',true);
            }

        }
        else
        {
            $('#p_total').val(amount);
            $('.cashSubmit').prop('disabled',false);
        }
    });

    $('#credit').on('show.bs.modal', function (e) {
        var rowCount = $('#example4 tr').length ;
        var paid = 0;
        var amount = $('#actual1').val();
        if(rowCount > 1)
        {
            for (let i = 1; i <= rowCount - 1; i++) {
                paid = +paid + +$('.cc'+i).val();
            }
            if(amount > paid)
            {
                amount = amount - paid;
                $('#p_total1').val(amount.toFixed(2));
                $('.creditSubmit').prop('disabled',false);
            }
            else
            {
                console.log(amount,paid);
                alert('payment is complete');
                amount = amount - paid;
                $('#p_total1').val(amount.toFixed(2));
                $('.creditSubmit').prop('disabled',true);
            }
        }
        else
        {
            $('#p_total1').val(amount);
            $('.creditSubmit').prop('disabled',false);
        }
    });


    function code(c)
    {
        var rowCount = $('#example tr').length ;
        var length = c.length;
        if(length > 6)
        {
            $.ajax({
                url:"{{url('')}}/pos-detail/product/code/"+c,
                method:"GET",
                error: function (request, error) {
                            console.log(" Can't do because: " + error +request);
                        },
                success:function(data){
                    console.log(data);
                    if($('#'+data.id).length){
                        var row = $('#'+data.id);
                        var qty = row.find("td .quantity").val();
                        var price = row.find("td .price").val();
                        var rate = row.find("td .discountr").val();
                        var percent = row.find("td .discountp").val();
                        if(data.total_quantity == qty)
                        {
                            alert('Not Enough Stock');
                        }
                        else
                        {

                            qty = +qty + +1;
                            if(rate == '')
                            {
                                var sub= qty*price;
                            }
                            if(rate != '')
                            {
                                var disc = (qty * price * percent) / 100;
                                $('.rate'+id).val(disc);
                                var sub = (qty * price) - disc;
                            }
                            row.find("td .quantity").val(qty);
                            row.find("td .abc").val(sub);

                            var amount=0;
                            var qnty=0;
                            var rowCount1 = $('#example tr').length ;
                            var rowCount2 = $('#example tr').length - 1;
                            for (let i = 1; i < rowCount1; i++) {
                                amount = +amount + +$('.sub'+i).val();
                                qnty = +qnty + +$('.quantity'+i).val();
                            }
                            $('#example2 tbody').empty();
                            $('#example2').append(`<tr><td>`+rowCount2+`(`+qnty+`)`+`</td><td>`+amount+`</td><td>0</td><td>0</td><td>`+amount+`</td></tr>`);
                            $('#actual').val(amount);
                            $('#actual2').val(amount);
                            $('#p_total').val(amount);
                            $('#actual1').val(amount);
                            $('#p_total1').val(amount);
                        }
                    }
                    else
                    {
                        if(data.total_quantity == 0 || data.total_quantity == null)
                        {
                            alert('Not Enough Stock');
                        }
                        else
                        {

                            $("#example").append("<tr id='"+data.id+"'><td><input name='p_id[]' type='text' class='form-control' readonly value='"+data.id+"'></td><td>"+data.pro_code+" - "+data.pro_name+"</td><td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data.price+"'></td><td><input type='number' class='form-control quantity quantity"+rowCount+"' required min='1' id='"+rowCount+"' max='"+data.total_quantity+"' name='quantity[]' value='1'></td><td><input type='hidden' name='type[]' value='"+data.vstatus+"'><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data.price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");

                            var amount=0;
                            var qty=0;
                            var rowCount1 = $('#example tr').length ;
                            var rowCount2 = $('#example tr').length - 1;
                            for (let i = 1; i < rowCount1; i++) {
                                amount = +amount + +$('.sub'+i).val();
                                qty = +qty + +$('.quantity'+i).val();
                            }
                            $('#example2 tbody').empty();
                            $('#example2').append(`<tr><td>`+rowCount2+`(`+qty+`)`+`</td><td>`+amount+`</td><td>0</td><td>0</td><td>`+amount+`</td></tr>`);
                            $('#actual').val(amount);
                            $('#p_total').val(amount);
                            $('#actual1').val(amount);
                            $('#p_total1').val(amount);
                            $('#actual2').val(amount);
                        }
                    }
                }
            });
        }
    }

    $(window).scannerDetection();
    $(window).bind('scannerDetectionComplete',function(e,data){
        console.log('complete '+data.string);
        $("#pid").val(data.string);
        code(data.string);
        $('#pid').val('');
    })
    .bind('scannerDetectionError',function(e,data){
        console.log('detection error '+data.string);
    })
    .bind('scannerDetectionReceive',function(e,data){
        console.log('Recieve');
        console.log(data.evt.which);
    });



    // $(document).on('keyup','#pid',function(){
    //     var c = $(this).val();
    //     setInterval(code(c),60000);
    // });

</script>
</body>

</html>
