@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/permission">Permissions</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Permissions</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-lock font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Permissions</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('permission.update',$permission->id) :  route('permission.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-outline">
                                        <label for="">Menu*</label>
                                        <select class="form-control selectpicker" style="overflow-y: auto" data-live-search="true" required name="m_id" id="m_id" >
                                            <option value=""  selected disabled>Select...</option>
                                            @if ($isEdit)

                                                @foreach ($menus as $m)

                                                    @if ($m->p_id != 0)
                                                        <option value="{{$m->id}}" {{$permission->m_id == $m->id ? 'selected' : ''}}>&emsp; {{$m->name}}</option>
                                                    @else
                                                        <option value="{{$m->id}}" {{$permission->m_id == $m->id ? 'selected' : ''}}>{{$m->name}}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach ($menus as $m)
                                                    @if ($m->p_id != 0)
                                                        <option value="{{$m->id}}">&emsp; {{$m->name}}</option>
                                                    @else
                                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Name*</label>
                                        <input value="{{$permission->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Menu Name" name="name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Route*</label>
                                        <input value="{{$permission->route ?? old('route')}}" class="form-control" type="text" placeholder="Enter Route Name" name="route" required >
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
