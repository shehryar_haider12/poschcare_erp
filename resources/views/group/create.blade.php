@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/group">Group</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Group</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-users font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Group</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="groupForm" action="{{$isEdit ? route('group.update',$group->id) :  route('group.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Group Name*</label>
                                        <input value="{{$group->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Group Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Group already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Group Type*</label>
                                        <select class="form-control selectpicker" name="g_type" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$group->g_type == 'Customer' ? 'selected' : null }} value="Customer">Customer</option>
                                            <option {{$group->g_type == 'Price' ? 'selected' : null }} value="Price">Price</option>
                                            @else
                                            <option value="Customer" {{old('g_type') == 'Customer' ? 'selected' : null}}>Customer</option>
                                            <option value="Price" {{old('g_type')    == 'Price' ? 'selected' : null}}>Price</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#groupForm').validate({
        rules: {
            name: {
                required: true,
            },
            g_type: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>

@endsection
