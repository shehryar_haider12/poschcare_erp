@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/customer">Customer</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-user font-white"></i>View Customers
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px"  href="{{route('customer.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    <a id="GFG" style="margin-left:-50px" href="{{route('customer.pdf')}}">
                                                        <i class="fa fa-file-pdf-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('customer.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Customer</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        @if (in_array('importExcel',$permissions))
                                            <div class="tableview">
                                                <a href="{{route('customer.importStructure')}}" download>
                                                    <button style="background: #000000; margin-left:20px; margin-top:-2px" type="button"  class="btn btn-primary btn-sm ">
                                                    <i class='fa fa-download'></i>
                                                    Download Structure</button>
                                                </a>
                                                <form id="Import_product" action="{{route('import.receivables')}}" method="POST" enctype="multipart/form-data" id="advanceSearch">
                                                    <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="col-md-4">
                                                            <input type="file" name="csv_file" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <button style="background: #000000; margin-left:-90px; margin-top:-2px" type="submit"  class="btn btn-primary btn-sm ">Import Csv</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <hr>
                                        @endif
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Customer Name</label>
                                                        <input type="text" id="v_name" id="autocomplete-ajax1" class="form-control" placeholder="Customer Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="">Company</label>
                                                        <input type="text" id="company" id="autocomplete-ajax1" class="form-control" placeholder="Company" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="examples" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Company</th>
                                                            <th>Opening Balance</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Customer</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer Name</label>
                                    <input class="form-control" type="text"  id="name" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Balance</label>
                                    <input class="form-control" type="text" id="balance" readonly >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer Group</label>
                                    <input class="form-control" type="text"  id="cgroup" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Price Group</label>
                                    <input  class="form-control" type="text"  id="pgroup" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Company</label>
                                    <input class="form-control" type="text"  id="company" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Address</label>
                                    <input  class="form-control" type="text"  id="address" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Contact Number</label>
                                    <input class="form-control" type="text"  id="c_no" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Country</label>
                                    <input  class="form-control" type="text"  id="country" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >NTN</label>
                                    <input class="form-control" type="text"  id="vat" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >GST</label>
                                    <input  class="form-control" type="text"  id="gst" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >State</label>
                                    <input class="form-control" type="text"  id="state" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Email</label>
                                    <input  class="form-control" type="text"  id="email" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Postal Code</label>
                                    <input class="form-control" type="text"  id="pc" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >City</label>
                                    <input  class="form-control" type="text"  id="city" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="docs" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Documents List</h4>
                    </div>

                    <div class="modal-body">
                        <div class="table-responsive">
                            <table id="example5" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th>Document</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                            </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
            <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
    @toastr_render
        {{-- view one product --}}
        <script>
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/customer/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#name').val(data.name);
                        $('#company').val(data.company);
                        $('#address').val(data.address);
                        $('#c_no').val(data.c_no);
                        $('#country').val(data.country);
                        $('#vat').val(data.VAT);
                        $('#balance').val(data.balance);
                        $('#gst').val(data.GST);
                        $('#email').val(data.email);
                        $('#state').val(data.state);
                        $('#pc').val(data.postalCode);
                        $('#city').val(data.city.c_name);
                        $('#cgroup').val(data.cgroup.name);
                        $('#pgroup').val(data.pgroup.name);
                    }
                });
            });
            $(document).on('click','.doc',function(){
                var id=$(this).attr("id");
                $("#example5 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/customer/"+id+"/document",
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(document.location.origin);
                        var a = 1;
                        var total = 0;
                        if(data.length > 0)
                        {
                            for (let i = 0; i < data.length; i++) {
                                $("#example5").append("<tr><td>"+a+"</td><td>Document"+a+"</td><td><a href="+document.location.origin+"/documents/"+data[i].document+" download><button type='button' class='btn green docs' data-grn="+data[i].id+" id="+data[i].id+"><i class='fa fa-download'></i></button></a></td></tr>");
                                a= +a + 1;
                            }
                        }
                        $('#docs').modal("show");
                        $('#example5').DataTable();
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#examples').DataTable({
                    processing: true,
                    order: [[ 0, "desc" ]],
                    serverSide: true,
                    ajax: '{{route("customer.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "name",
                            "defaultContent": ""
                        },
                        {
                            "data": "company",
                            "defaultContent": ""
                        },
                        {
                            "data": "opening_balance",
                            "defaultContent": ""
                        },
                        {
                            "data": "id",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                var edit = '{{route("customer.edit",[":id"])}}';
                                edit = edit.replace(':id', row.id );
                                var checked = row.status == 1 ? 'checked' : null;
                                return `
                                @if(in_array('edit',$permissions))
                                    <a id="GFG" href="` + edit + `" class="text-info p-1">
                                        <button type="button" class="btn blue edit" >
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                @endif
                                @if(in_array('show',$permissions))
                                    <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                        <i class="icon-eye"></i>
                                    </button>
                                @endif
                                @if(in_array('status',$permissions))
                                    <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                    checked + ` value="` + row.id + `">
                                @endif
                                <br>
                                <button type="button" data-target="#docs" data-toggle="modal"  class="btn btn-info doc" id="`+row.id +`">View Documents
                                </button>
                                `;
                            },
                        },
                    ],
                    "drawCallback": function (settings) {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                    if (elems)
                    {
                        elems.forEach(function (html)
                        {
                            var switchery = new Switchery(html,
                            {
                                color: '#007bff',
                                 secondaryColor: '#dfdfdf',
                                 jackColor: '#fff',
                                 jackSecondaryColor: null,
                                 className: 'switchery',
                                 disabled: false,
                                 disabledOpacity: 0.5,
                                 speed: '0.1s',
                                 size: 'small'
                            });

                        });
                    }
                    $('.status').change(function () {
                        var $this = $(this);
                        var id = $this.val();
                        var status = this.checked;

                        if (status) {
                            status = 1;
                        } else {
                            status = 0;
                        }
                        // console.log(status);
                        axios
                            .post('{{route("customer.status")}}', {
                            _token: '{{csrf_token()}}',
                            _method: 'post',
                            id: id,
                            status: status,
                            })
                            .then(function (responsive) {
                            console.log('responsive');
                            // location.reload();
                            })
                            .catch(function (error) {
                            console.log(error);
                            });
                        });

                    },

                });
                $('#v_name').on('keyup',function(){
                    value = $("#v_name").val();
                    table.columns(1).search(value, true, false).draw();
                });
                $('#company').on('keyup',function(){
                    value = $("#company").val();
                    table.columns(2).search(value, true, false).draw();
                });
            });

        </script>
    @endsection
@endsection
