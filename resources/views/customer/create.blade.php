@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/customer">Customer</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Customer</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Customer</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="customerForm" enctype="multipart/form-data" action="{{$isEdit ? route('customer.update',$vendor->id) :  route('customer.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Company Name*</label>
                                        <input value="{{$vendor->company ?? old('company')}}" class="form-control" type="text" placeholder="Enter Company Name" name="company" >
                                        <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                                    </div>
                                </div>

                                <div class="{{$isEdit ? 'col-sm-6' : in_array('Add City',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >City <small>(optional)</small></label>
                                        <select id="c_id" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add City',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($city as $s)
                                                <option {{$s->id == $vendor->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->c_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($city as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('c_id') ? 'selected' : null}}>{{$s->c_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add City',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#cityModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <input type="hidden" name="v_type" value="Customer" id="">
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Customer Group*</label>
                                        <select  class="form-control selectpicker" name="c_group" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($cgroup as $s)
                                                <option {{$s->id == $vendor->c_group ? 'selected' : null}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cgroup as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('c_group') ? 'selected' : null}}>{{$s->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Price Group*</label>
                                        <select  class="form-control selectpicker" name="p_group" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($pgroup as $s)
                                                <option {{$s->id == $vendor->p_group ? 'selected' : null}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($pgroup as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('p_group') ? 'selected' : null}}>{{$s->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Address*</label>
                                        <input value="{{$vendor->address ?? old('address')}}" class="form-control" type="text" placeholder="Enter Address" name="address" >
                                        <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Country <small>(optional)</small></label>
                                        <input value="{{$vendor->country ?? old('country')}}" class="form-control" type="text" placeholder="Enter Country" name="country" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Customer Name1*</label>
                                        <input value="{{$vendor->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Customer Name" name="name" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact No1*</label>
                                        <input autocomplete="off" value="{{$vendor->c_no ?? old('c_no')}}" class="form-control" type="number" placeholder="Enter Contact Number" name="c_no" >
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Customer Name2<small>(optional)</small></label>
                                        <input value="{{$vendor->name2 ?? old('name2')}}" class="form-control" type="text" placeholder="Enter Customer Name" name="name2" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact No2<small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$vendor->c_no2 ?? old('c_no2')}}" class="form-control" type="number" placeholder="Enter Contact Number" name="c_no2" >
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >NTN <small>(optional)</small></label>
                                        <input value="{{$vendor->VAT ?? old('VAT')}}" class="form-control" type="text" placeholder="Enter NTN Number" name="VAT" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >GST <small>(optional)</small></label>
                                        <input value="{{$vendor->GST ?? old('GST')}}" class="form-control" type="text" placeholder="Enter GST Number" name="GST" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >State <small>(optional)</small></label>
                                        <input value="{{$vendor->state ?? old('state')}}" class="form-control" type="text" placeholder="Enter State" name="state" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Email <small>(optional)</small></label>
                                        <input value="{{$vendor->email ?? old('email')}}" class="form-control" type="email" placeholder="Enter Email" name="email" >
                                        <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Postal Code <small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$vendor->postalCode ?? old('postalCode')}}" class="form-control" type="number" placeholder="Enter Postal Code" name="postalCode" >
                                    </div>
                                </div>
                                @if(in_array('credit balance',$permissions))
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Balance <small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$vendor->balance ?? old('balance')}}" class="form-control" type="text" placeholder="Enter Balance" name="balance" >
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                @if(in_array('opening balance',$permissions))
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Opening Balance <small>(optional)</small></label>
                                        <input autocomplete="off" value="{{$vendor->opening_balance ?? old('opening_balance')}}" class="form-control" type="text" placeholder="Enter Opening Balance" name="opening_balance" >
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <button class="btn btn-info doc" type="button">Add Documents</button>
                                    </div>
                                </div>
                            </div>
                            <div class="document" id="document">
                                @if ($isEdit)
                                    <br>
                                    @if ($vendor->attachment->isNotEmpty())
                                        @php
                                            $c=1;
                                        @endphp
                                        @foreach ($vendor->attachment as $a)
                                            <div class='row docs'>
                                                <div class='col-sm-6'>
                                                    <div class='form-outline'>
                                                         <span>Document{{$c}} <input type='hidden' name="documents[]" value="{{$a->document}}"> </span>
                                                        {{-- <input type='file' class='form-control' value="{{url('')}}/documents/{{$a->document}}" name='document[]'> --}}
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class='form-outline'>
                                                        <button type="button" id="{{$c}}" class="btn red delete d{{$c}}" ><i class="fa fa-trash"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.city')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $(document).ready(function(){
        $('#c_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#cityModal').modal("show"); //Open Modal
            }
        });
    });
</script>

<script>
    $('#customerForm').validate({
        rules: {
            company: {
                required: true,
            },
            c_group: {
                required: true,
            },
            p_group: {
                required: true,
            },
            address: {
                required: true,
            },
            name: {
                required: true,
            },
            c_no: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>

<script>
    $(document).on('click','.doc',function(){
        var a = 0;
        var parentDiv = document.getElementById("document");
        if (parentDiv.hasChildNodes())
        {
            var last = 0;
            $(".docs").each(function(){
                last = ($(this).attr("id"));
            });
            a = +last + +1;
        }
        else
        {
            a = 1;
        }
        $('.document').append(`<br>
        <div class='row docs'>
            <div class='col-sm-6'>
                <div class='form-outline'>
                    <input type='file' class='form-control' name='document[]'>
                </div>
            </div>
            <div class="col-sm-2">
                <div class='form-outline'>
                    <button type="button" id="`+a+`" class="btn red delete d`+a+`" ><i class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>`);
    });

    $(document).on('click','.delete',function(){
        var id=$(this).attr('id');
        $(this).closest('.docs').remove();

    });
</script>

@endsection
