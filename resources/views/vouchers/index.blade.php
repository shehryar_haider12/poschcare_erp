@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/JournalVoucher">Vouchers</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-sticky-note font-white"></i>View Journal Vouchers
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    @if($index == 0)
                                                        <a style="margin-left:-20px; "  href="{{route('JournalVoucher.excel')}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 1)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$year}}/{{$acc}}/{{$check}}/excel/year">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 2)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$month}}/{{$acc}}/{{$check}}/excel/month">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 3)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$from}}/{{$to}}/{{$acc}}/{{$check}}/excel/dates">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 4)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$date}}/{{$acc}}/{{$check}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 5)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$acc}}/excel/account">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 6 || $index == 7 || $index == 8 || $index == 9)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$index}}/excel">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 10)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$amount}}/excel/amount">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 11)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$desc}}/excel/desc">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if($index == 12)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/JournalVoucher/search/{{$vno}}/excel/vno">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('JournalVoucher.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Vocuher</button>
                                                    </a>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        @if (in_array('importExcel',$permissions))
                                            <div class="tableview">
                                                <a href="{{route('JournalVoucher.importStructure')}}" download>
                                                    <button style="background: #000000; margin-left:20px; margin-top:-2px" type="button"  class="btn btn-primary btn-sm ">
                                                    <i class='fa fa-download'></i>
                                                    Download Structure</button>
                                                </a>
                                                <form id="Import_product" action="{{route('import.journalVoucher')}}" method="post" enctype="multipart/form-data" id="advanceSearch">
                                                    <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="col-md-4">
                                                            <input type="file" name="csv_file" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <button style="background: #000000; margin-left:-90px; margin-top:-2px" type="submit"  class="btn btn-primary btn-sm ">Import Csv</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/JournalVoucher/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" id="menuid" name="menuid">
                                                <div class="tableview">
                                                    {{-- @if ($roleName->name == 'Admin') --}}
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Voucher Date
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Account">By Account
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Vno">By Voucher No
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Desc">By Description
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Amount">By Amount
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                            </label>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Year</label>
                                                                    <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">From</label>
                                                                    <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">To</label>
                                                                    <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Debit Account</label>
                                                                    <select class="form-control" data-live-search="true" name="account1" id="account1" disabled >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($account as $u)
                                                                            <option  value="{{$u->Code}}">{{$u->name_of_account}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Voucher No</label>
                                                                    <input type="text" disabled name="vno" id="vno" class="form-control"  style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Amount</label>
                                                                    <input type="text" disabled name="amount" id="amount" class="form-control"  style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Description</label>
                                                                    <input type="text" disabled name="desc" id="desc" class="form-control"  style=" z-index: 2;" autocomplete="off">
                                                                </div>
                                                            </div>
                                                                {{-- <div class="col-md-8"></div> --}}
                                                                <div class="col-md-4">
                                                                <div class="form-group">
                                                                    {{-- <label for="">Unit Name</label> --}}
                                                                    <label for="" style="visibility: hidden">.</label>
                                                                    <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                                                </div>
                                                                </div>
                                                        </div>
                                                    {{-- @endif --}}
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Date</th>
                                                            <th>Voucher Number</th>
                                                            <th>Voucher Type</th>
                                                            <th>Amount</th>
                                                            <th>Description</th>
                                                            <th>Status</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($voucher as $l)
                                                            @if ($l->p_id == 0)
                                                                <tr>
                                                                    <td>
                                                                        {{$l->id}}
                                                                    </td>
                                                                    <td>
                                                                        {{$l->date}}
                                                                    </td>
                                                                    <td>
                                                                        {{$l->v_no}}
                                                                    </td>
                                                                    <td>
                                                                        {{$l->v_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$l->amount}}
                                                                    </td>
                                                                    <td>
                                                                        {{$l->description}}
                                                                    </td>
                                                                    @if($l->status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  red" id="{{$l->id}}">
                                                                                {{$l->status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($l->status=='Approved')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-primary" id="{{$l->id}}">
                                                                                {{$l->status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($l->status=='Cancel')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$l->id}}">
                                                                                {{$l->status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    <td>
                                                                        @if ($l->status == 'Pending')
                                                                            <select class="form-control action" id="{{$l->id}}" >
                                                                                <option >Actions</option>
                                                                                <option >Edit Voucher</option>
                                                                                <option >Voucher Details</option>
                                                                                @if(in_array('Generate voucher',$permissions))
                                                                                    <option >Generate Voucher</option>
                                                                                @endif
                                                                                <option >Approved</option>
                                                                                <option >Cancel</option>
                                                                            </select>
                                                                        @else
                                                                            <select class="form-control action" id="{{$l->id}}" >
                                                                                <option >Actions</option>
                                                                                <option >Voucher Details</option>
                                                                                @if(in_array('Generate voucher',$permissions))
                                                                                    <option >Generate Voucher</option>
                                                                                @endif
                                                                            </select>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('modal')
        <div id="voucherModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Voucher Details</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Voucher Number</label>
                                    <input class="form-control" type="text"  id="vnos" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Voucher Date</label>
                                    <input class="form-control" type="text"  id="vdate" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Biller</label>
                                    <input class="form-control" type="text"  id="bi" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Remarks</label>
                                    <input class="form-control" type="text"  id="re" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div id="table2" class="table-responsive">
                            <table id="example10" class="table table-striped table-bordered" style="width:100%">
                            </table>

                        </div>
                        <br>
                        <div id="table" class="table-responsive">
                            <table hidden id="example1" class="table table-striped table-bordered" style="width:100%">
                            </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection

@section('custom-script')
@toastr_js
@toastr_render

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            order: [[ 0, "desc" ]]
        });
        $('input:radio[name="optradio"]').change(function(){
            if ($(this).is(':checked')) {
                $('#search').prop('disabled',false);
                var val = $(this).val();
                if(val == 'Year')
                {
                    $('#year').prop('disabled',false);
                    $('#year').attr('required',true);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').prop('disabled',true);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',false);
                    $('#account1').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'Month')
                {
                    $('#month').prop('disabled',false);
                    $('#month').attr('required',true);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',false);
                    $('#account1').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'Date')
                {
                    $('#from').prop('disabled',false);
                    $('#from').attr('required',true);
                    $('#to').prop('disabled',false);
                    $('#to').attr('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',false);
                    $('#account1').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'Account')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',true);
                    $('#account1').attr('disabled',false);
                    $('#account1').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'last24Hours')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'lastweek')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'last15Days')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'lastMonth')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'Vno')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',false);
                    $('#vno').attr('required',true);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'Amount')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',false);
                    $('#amount').attr('required',true);
                    $('#desc').prop('disabled',true);
                    $('#desc').attr('required',false);
                }
                if(val == 'Desc')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#account1').attr('required',false);
                    $('#account1').attr('disabled',true);
                    $('#vno').prop('disabled',true);
                    $('#vno').attr('required',false);
                    $('#amount').prop('disabled',true);
                    $('#amount').attr('required',false);
                    $('#desc').prop('disabled',false);
                    $('#desc').attr('required',true);
                }
            }
        });
    });

    $(document).on('change','.action',function(){
        var val=$(this).val();
        if(val == 'Approved' || val == 'Cancel')//
        {
            var id = $(this).attr('id');
            var status = val;
            axios
            .post('{{route("JournalVoucher.status")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                id: id,
                status: status,
                })
                .then(function (responsive) {
                console.log('responsive');
                location.reload();
                })
                .catch(function (error) {
                console.log(error);
            });
        }
        if(val == 'Edit Voucher')
        {
            var id=$(this).attr("id");
            window.location.href='{{url('')}}/JournalVoucher/'+id+'/edit';
        }
        if(val == 'Generate Voucher')
        {
            var id=$(this).attr("id");
            $.ajax({
                url:"{{url('')}}/JournalVoucher/pdf/"+id,
                method:"GET",
                data:
                {
                    id:id,
                },
                error: function (request, error) {
                    alert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    window.location.href= '{{url('')}}/JournalVoucher/pdf/'+id;
                    // location.reload();
                    $('.action').val('Actions');
                }
            });
        }
        if(val == 'Voucher Details')
        {
            var id=$(this).attr("id");
            $("#example1").empty();
            $("#example10").empty();
            $.ajax({
                url:"{{url('')}}/JournalVoucher/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    $('#vnos').val(data[0].v_no);
                    $('#vdate').val(data[0].date);
                    $('#bi').val(data[0].biller.name);
                    $('#re').val(data[0].remarks);
                    $('#voucherModal').modal("show");

                    if(data[2].length == 0)
                    {
                        $('#example10').append(`<thead><tr>
                                <th width='20%' >S.No</th>
                                <th width='20%'>Account</th>
                                <th width='20%'>Description</th>
                                <th width='20%'>Debit</th>
                                <th width='20%'>Credit</th>
                            </tr>
                            </thead> <tbody> <tr><td>1</td><td>`+data[0].acode1.name_of_account+`</td><td>`+data[0].description+`</td><td>`+data[0].amount+`</td><td></td></tr>
                            <tr><td>2</td><td>`+data[0].acode2.name_of_account+`</td><td>`+data[0].description+`</td><td></td><td>`+data[0].amount+`</td></tr><tr><td></td><td></td><td></td><td align="right"><b>Total</b></td><td>`+data[0].amount+`</td></tr></tbody>`);
                        // $('#example10').DataTable();
                    }
                    else
                    {
                        $('#example10').append(`<thead><tr>
                                <th width='20%' >S.No</th>
                                <th width='20%'>Account</th>
                                <th width='20%'>Description</th>
                                <th width='20%'>Debit</th>
                                <th width='20%'>Credit</th>
                            </tr>
                            </thead> <tbody>`);
                            if(data[0].head1 == null)
                            {
                                $('#example10').append(`<tr><td>1</td><td>`+data[0].acode2.name_of_account+`</td><td>`+data[0].description+`</td><td></td><td>`+data[0].amount+`</td></tr>`);
                                var no=2;
                                for (let i = 0; i < data[2].length; i++) {
                                    $('#example10').append(`<tr><td>`+no+`</td><td>`+data[2][i].acode1.name_of_account+`</td><td>`+data[2][i].description+`</td><td>`+data[2][i].amount+`</td><td></td></tr>`);
                                    no++;
                                }
                                $('#example10').append(`<tr><td></td><td></td><td></td><td align="right"><b>Total</b></td><td>`+data[0].amount+`</td></tr></tbody>`);
                                // $('#example10').DataTable();
                            }
                            else
                            {
                                $('#example10').append(`<tr><td>1</td><td>`+data[0].acode1.name_of_account+`</td><td>`+data[0].description+`</td><td>`+data[0].amount+`</td><td></td></tr>`);
                                var no=2;
                                for (let i = 0; i < data[2].length; i++) {
                                    $('#example10').append(`<tr><td>`+no+`</td><td>`+data[2][i].acode2.name_of_account+`</td><td>`+data[2][i].description+`</td><td></td><td>`+data[2][i].amount+`</td></tr>`);
                                    no++;
                                }
                                $('#example10').append(`<tr><td></td><td></td><td></td><td align="right"><b>Total</b></td><td>`+data[0].amount+`</td></tr></tbody>`);
                            }


                    }


                    if(data[1].length > 0 && data[0].head2 == 'CA-01')
                    {
                        $('#example1').append(`<thead><tr>
                                <th width='20%' >S.No</th>
                                <th width='20%'>Sale Id</th>
                            </tr>
                            </thead> <tbody>`);
                                var spid= '';
                            for (let i = 0; i < data[1].length; i++) {
                                if(data[1][i].spid == 0)
                                {
                                    spid = 'Opening';
                                }
                                else
                                {
                                    spid = data[1][i].spid;
                                }
                                $('#example1').append(`<tr><td>`+data[0].id+`</td><td>`+spid+`</td></tr>`)
                            }
                        $('#example1').append(`</tbody>`);
                        $('#example1').show();
                        $('#example1').DataTable();
                    }
                    if(data[1].length > 0 && data[0].head1 == 'CL-01')
                    {
                        $('#example1').append(`<thead><tr>
                                <th width='20%'>S.No</th>
                                <th width='20%'>Sale Id</th>
                            </tr>
                            </thead> <tbody>`);
                        var spid= '';
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].spid == 0)
                            {
                                spid = 'Opening';
                            }
                            else
                            {
                                spid = data[1][i].spid;
                            }
                            $('#example1').append(`<tr><td>`+data[0].id+`</td><td>`+spid+`</td></tr>`)
                        }
                        $('#example1').append(`</tbody>`);
                        $('#example1').show();
                        $('#example1').DataTable();
                    }
                    $('.action').val('Actions');


                }
            });
        }
    });
</script>

@endsection
@endsection
