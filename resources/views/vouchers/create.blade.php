@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
        /* height: 400px; */
    }
</style>
<style>
    .hr {
    display: block;
    border-style: inset;
    border-width: 1px;
    margin-top: 10px;
    margin-bottom: 10px;
    }
    .h5
    {
        margin: 0 auto;
        position: relative;
        right: 1170px;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
    <li>
        <a href="{{url('')}}/JournalVoucher">Vouchers</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>{{$isEdit ? 'Edit' : 'Add'}} Voucher</span>
    </li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-sticky-note font-white"></i>{{$isEdit ? 'Edit' : 'Add'}} Voucher
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <form id="voucherForm" action="{{$isEdit ? route('JournalVoucher.update',$voucher->id) : route('JournalVoucher.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" >
                                            @csrf
                                            @if ($isEdit)
                                                @method('PUT')
                                            @endif
                                            <div class="form-body">
                                                <div class="custom_datatable">
                                                    <div class="tableview">
                                                        <h4><b> Select Voucher Type</b> </h4>
                                                        <br>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio1">
                                                            <input type="radio" class="form-check-input"  {{$isEdit ? ($voucher->v_type == 'JV' ? 'checked' : '') : ''}} id="radio" name="v_type"  value="JV"> Journal Voucher
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label" for="radio2">
                                                                <input type="radio" class="form-check-input"  {{$isEdit ? ($voucher->v_type == 'EV' ? 'checked' : '') : ''}} id="radio" name="v_type" value="EV">Expense Voucher
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input"  {{$isEdit ? ($voucher->v_type == 'RV' ? 'checked' : '') : ''}} id="radio" name="v_type" value="RV">Receiving Voucher
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input"  {{$isEdit ? ($voucher->v_type == 'VPV' ? 'checked' : '') : ''}} id="radio" name="v_type" value="VPV">Vendor Payment Voucher
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" {{$isEdit ? ($voucher->v_type == 'CBD' ? 'checked' : '') : ''}} id="radio" name="v_type" value="CBD">Cash Bank Deposit
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" {{$isEdit ? ($voucher->v_type == 'CWD' ? 'checked' : '') : ''}} id="radio" name="v_type" value="CWD">Cash WithDrawal
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" {{$isEdit ? ($voucher->v_type == 'DV' ? 'checked' : '') : ''}} id="radio" name="v_type" value="DV">Drawing Voucher
                                                            </label>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Biller</label>
                                                                    <select name="b_id" id="b_id" class="form-control bid selectpicker" data-live-search="true">
                                                                        <option selected >Select Anyone</option>
                                                                        @if ($isEdit)
                                                                            @foreach ($biller as $b)
                                                                                <option value="{{$b->id}}" {{$voucher->b_id == $b->id ? 'selected' : ''}}>{{$b->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            @foreach ($biller as $b)
                                                                                <option value="{{$b->id}}">{{$b->name}}</option>
                                                                            @endforeach

                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Voucher No</label>
                                                                    <input type="hidden" name="vcno" value="{{$isEdit ? $voucher->vcno : ''}}" name="vcno" id="vcno">
                                                                    <input type="text" value="{{$isEdit ? $voucher->v_no : ''}}" readonly name="v_no" id="v_no" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="">Voucher Date</label>
                                                                    <input  name="date"  class="form-control" type="date"  value="{{$isEdit ?  $voucher->date : ''}}"  >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Attachment</label>
                                                                    <input type="file" class="form-control" name="attachment">
                                                                </div>
                                                            </div>
                                                            @if ($isEdit)
                                                                @if ($voucher->attachment != null)
                                                                    <div class="row">
                                                                        <div class="col-sm-6"></div>
                                                                        <div class="col-sm-5">
                                                                            <input type="hidden" name="att1" value="{{$voucher->attachment}}">
                                                                            <label style="margin-top: 10px" ><b> Attachment </b></label>
                                                                        </div>
                                                                        <div class="col-sm-1">
                                                                            <a href="{{route('JournalVoucher.document',$voucher->id)}}" class='btn green invoice' style="margin-top: 10px" ><i class='fa fa-download'></i></a>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Remarks</label>
                                                                    <input autocomplete="off" value="{{$isEdit ? $voucher->remarks : old('remarks')}}" name="remarks"  class="form-control" type="text" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        {{-- <div id="JV" hidden>
                                                            <div class="row" >
                                                                <h5>Debit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1" id="head_d" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->code != 'CA-02')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->code != 'CA-02')
                                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"   name="account1" id="account_code_djv" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account1 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account1 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_djv" value="{{$isEdit ? $account1name->name_of_account : ''}}" class="form-control account_name_djv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>



                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker head_c" data-live-search="true"  id="head_c" name="head2" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->code != 'CA-02')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->code != 'CA-02')
                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"  name="account2" id="account_code_cjv" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($account2 as $a)
                                                                                    <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_cjv" class="form-control account_name_cjv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end -->
                                                            </div>
                                                                <!-- inner row end -->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Accounting Date</label>
                                                                        <input  name="date"  class="form-control" type="date"  value="{{$isEdit ?  $voucher->date : ''}}"  >
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amount"  class="form-control" type="text" >
                                                                    </div>
                                                                </div>
                                                            <!-- main row end -->
                                                            </div>
                                                            <!-- main row end -->


                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Description</label>
                                                                        <textarea  name="description" rows="8" cols="65"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div id="sales" {{($isEdit && $voucher->head2 == 'CA-01') ? '' : 'hidden' }}  class="row">

                                                                <select multiple class="form-control {{$isEdit ? 'selectpicker' : ''}} " data-live-search="true" id="sale_id" name="s_id[]" >
                                                                    @if ($isEdit && $voucher->head2 == 'CA-01')
                                                                        @if (in_array(0,$spid) && count($vdetails) == 1)
                                                                            @foreach ($vdetails as $v)
                                                                                @if ($v->spid == 0 )
                                                                                    <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                    @break
                                                                                @endif
                                                                            @endforeach
                                                                            @foreach ($sales as $s)
                                                                                @if ($s->total != 0)
                                                                                    <option value="{{$s->id}}" >Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if (in_array(0,$spid) && count($vdetails) > 1)
                                                                                @foreach ($vdetails as $v)
                                                                                    @if ($v->spid == 0 )
                                                                                        <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                        @break
                                                                                    @endif
                                                                                @endforeach
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($sales as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @else

                                                                                <option value="0" >Opening Account: {{$ledger}}</option>
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($sales as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endif

                                                                </select>
                                                            </div>

                                                            <div id="purchase" {{($isEdit && $voucher->head2 == 'CL-01') ? '' : 'hidden' }} class="row">

                                                                <select multiple class="form-control {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"   id="purchase_id" name="p_id[]" >
                                                                    @if ($isEdit && $voucher->head2 == 'CL-01')
                                                                        @if (in_array(0,$spid) && count($vdetails) == 1)
                                                                            @foreach ($vdetails as $v)
                                                                                @if ($v->spid == 0 )
                                                                                    <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                    @break
                                                                                @endif
                                                                            @endforeach
                                                                            @foreach ($purchase as $s)
                                                                                @if ($s->total != 0)
                                                                                    <option value="{{$s->id}}" >PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if (in_array(0,$spid) && count($vdetails) > 1)
                                                                                @foreach ($vdetails as $v)
                                                                                    @if ($v->spid == 0 )
                                                                                        <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                        @break
                                                                                    @endif
                                                                                @endforeach
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($purchase as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @else

                                                                                <option value="0" >Opening Account: {{$ledger}}</option>
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($purchase as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div> --}}


                                                        <div id="CBD" {{$isEdit ? ($voucher->v_type == 'CBD' ? '' : 'hidden') : 'hidden'}}>
                                                            <div class="row" >
                                                                <h5>Debit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1cbd" id="head_d" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true" name="account1cbd" id="account_code_dcbd" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account1 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account1 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_dcbd" value="{{$isEdit ? $account1name : ''}}" class="form-control account_name_dcbd" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker head_c" data-live-search="true"  id="head_c" name="head2cbd" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank' || $u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank' || $u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"  name="account2cbd" id="account_code_ccbd" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($account2 as $a)
                                                                                    <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_ccbd" class="form-control account_name_ccbd" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end -->
                                                            </div>
                                                            <hr>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amountcbd"  class="form-control" type="text" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Description</label>
                                                                        <textarea  name="descriptioncbd" rows="8" cols="58"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr>
                                                            @if ($isEdit && $voucher->head1 == 'CA-04' && $voucher->v_type == 'CWD')
                                                                @php
                                                                    $count = 1;
                                                                @endphp
                                                                <span class="btn btn-md btn-info" id="addchequecbd">Add More</span>

                                                                <div id="chqcbd">
                                                                    @foreach ($th as $t)
                                                                        <div id = '{{$count}}' class="cbdchq">
                                                                            <div class="row">
                                                                                <h5>Cheque Details</h5>

                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque number</label>
                                                                                        <input name="cbdchqno[]" value="{{$t->cheque_no}}" type="text" class="form-control cbdchqno" id="cbdchq{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Type</label>
                                                                                        <select id="cbdchqtype{{$count}}" name="cbdchqtype[]" class="form-control cbdchqtype selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            <option {{$t->merchant_type == 'Cash' ? 'selected' : ''}} >Cash</option>
                                                                                            <option {{$t->merchant_type == 'Cross' ? 'selected' : ''}} >Cross</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Payee Title</label>
                                                                                        <input type="text" value="{{$t->title}}" class="form-control cbdpt" name="cbdpt[]" id="cbdpt{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Date</label>
                                                                                        <input type="date" class="form-control cbdchqdate" value="{{$t->chq_date}}" name="cbdchqdate[]" id="cbdchqdate{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Amount</label>
                                                                                        <input type="text" class="form-control cbdchqam" value="{{$t->total}}" name="cbdchqam[]" id="cbdchqam{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Bank</label>
                                                                                        <select id="cbdbank{{$count}}" name="cbdbank[]" class="form-control cbdbank selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            @foreach ($bank as $b)
                                                                                                <option value="{{$b->id}}" {{$t->b_id == $b->id ? 'selected' : ''}} >{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <div id="chqcbd" hidden>
                                                                    <span class="btn btn-md btn-info" id="addchequecbd">Add More</span>

                                                                    <div id = '1' class="cbdchq">
                                                                        <div class="row">
                                                                            <h5>Cheque Details</h5>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque number</label>
                                                                                    <input name="cbdchqno[]" type="text" class="form-control cbdchqno" id="cbdchq1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Type</label>
                                                                                    <select id="cbdchqtype1" name="cbdchqtype[]" class="form-control cbdchqtype selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        <option>Cash</option>
                                                                                        <option>Cross</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Payee Title</label>
                                                                                    <input type="text" class="form-control cbdpt" name="cbdpt[]" id="cbdpt1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Date</label>
                                                                                    <input type="date" class="form-control cbdchqdate" name="cbdchqdate[]" id="cbdchqdate1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Amount</label>
                                                                                    <input type="text" class="form-control cbdchqam" name="cbdchqam[]" id="cbdchqam1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Bank</label>
                                                                                    <select id="cbdbank1" name="cbdbank[]" class="form-control cbdbank selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        @foreach ($bank as $b)
                                                                                            <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div id="CWD" {{$isEdit ? ($voucher->v_type == 'CWD' ? '' : 'hidden') : 'hidden'}}>
                                                            <div class="row" >
                                                                <h5>Debit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1cwd" id="head_d" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"   name="account1cwd" id="account_code_dcwd" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account1 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account1 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_dcwd" value="{{$isEdit ? $account1name : ''}}" class="form-control account_name_dcwd" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker head_c" data-live-search="true"  id="head_c" name="head2cwd" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"  name="account2cwd" id="account_code_ccwd" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($account2 as $a)
                                                                                    <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_ccwd" class="form-control account_name_ccwd" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end -->
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amountcwd"  class="form-control" type="text" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Description</label>
                                                                        <textarea  name="descriptioncwd" rows="8" cols="58"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <hr>
                                                            @if ($isEdit && $voucher->head2 == 'CA-04' && $voucher->v_type == 'CWD')
                                                                <span class="btn btn-md btn-info" id="addchequecwd">Add More</span>
                                                                @php
                                                                    $count = 1;
                                                                @endphp
                                                                <div id="chqcwd">
                                                                    @foreach ($th as $t)
                                                                        <div id = '{{$count}}' class="cwdchq">
                                                                            <div class="row">
                                                                                <h5>Cheque Details</h5>

                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque number</label>
                                                                                        <input name="cwdchqno[]" value="{{$t->cheque_no}}" type="text" class="form-control cwdchqno" id="cwdchq{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Type</label>
                                                                                        <select id="cwdchqtype{{$count}}" name="cwdchqtype[]" class="form-control cwdchqtype selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            <option {{$t->merchant_type == 'Cash' ? 'selected' : ''}} >Cash</option>
                                                                                            <option {{$t->merchant_type == 'Cross' ? 'selected' : ''}} >Cross</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Payee Title</label>
                                                                                        <input type="text" value="{{$t->title}}" class="form-control cwdpt" name="cwdpt[]" id="cwdpt{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Date</label>
                                                                                        <input type="date" class="form-control cwdchqdate" value="{{$t->chq_date}}" name="cwdchqdate[]" id="cwdchqdate{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Amount</label>
                                                                                        <input type="text" class="form-control cwdchqam" value="{{$t->total}}" name="cwdchqam[]" id="cwdchqam{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Bank</label>
                                                                                        <select id="cwdbank{{$count}}" name="cwdbank[]" class="form-control cwdbank selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            @foreach ($bank as $b)
                                                                                                <option value="{{$b->id}}" {{$t->b_id == $b->id ? 'selected' : ''}} >{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <div id="chqcwd">
                                                                    <div id = '1' class="cwdchq">
                                                                        <div class="row">
                                                                            <h5>Cheque Details</h5>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque number</label>
                                                                                    <input name="cwdchqno[]" type="text" class="form-control cwdchqno" id="cwdchq1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Type</label>
                                                                                    <select id="cwdchqtype1" name="cwdchqtype[]" class="form-control cwdchqtype selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        <option>Cash</option>
                                                                                        <option>Cross</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Payee Title</label>
                                                                                    <input type="text" class="form-control cwdpt" name="cwdpt[]" id="cwdpt1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Date</label>
                                                                                    <input type="date" class="form-control cwdchqdate" name="cwdchqdate[]" id="cwdchqdate1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Amount</label>
                                                                                    <input type="text" class="form-control cwdchqam" name="cwdchqam[]" id="cwdchqam1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Bank</label>
                                                                                    <select id="cwdbank1" name="cwdbank[]" class="form-control cwdbank selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        @foreach ($bank as $b)
                                                                                            <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif


                                                        </div>

                                                        <div id="DV" {{$isEdit ? ($voucher->v_type == 'DV' ? '' : 'hidden') : 'hidden'}}>

                                                            <span class="btn btn-md btn-info" id="adddv">Add More</span>
                                                            @if ($isEdit)
                                                                @php
                                                                    $counter=1;
                                                                @endphp
                                                                <div id="debitdv">
                                                                    @foreach ($others as $o)
                                                                        <div id = '{{$a}}' class="dvdebit">
                                                                            <div class="row" >
                                                                                <h5>Debit Account
                                                                                    @if ($counter > 1)
                                                                                        <button type="button" id="dv{{$counter}}" class="btn dltdv red delete" ><i class="fa fa-trash"></i></button>
                                                                                    @endif
                                                                                </h5>
                                                                                {{-- <br> --}}
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="">Head Category</label>
                                                                                        <select class="form-control selectpicker head_ddv" data-id='dv{{$counter}}' data-live-search="true"  id="head_d" name="head2dv[]" >
                                                                                            <option value=""  selected>Select...</option>
                                                                                            @if ($isEdit)
                                                                                                @foreach ($head as $u)
                                                                                                    @if ($u->name == 'Equity')
                                                                                                        <option  value="{{$u->code}}" {{$o->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @else
                                                                                                @foreach ($head as $u)
                                                                                                    @if ($u->name == 'Equity')
                                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                                    @endif
                                                                                                @endforeach

                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="">Account Code</label>
                                                                                        <select class="form-control addv account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true" name="account2dv[]" id="account_code_ddv-{{$counter}}" >
                                                                                            @if ($isEdit)
                                                                                                @php
                                                                                                    $account1 = \App\AccountDetails::where('Code','like',$o->head1.'%')->get();
                                                                                                @endphp
                                                                                            <option value=""  selected>Select...</option>
                                                                                                @foreach ($account1 as $a)
                                                                                                    <option value="{{$a->Code}}" {{$o->account1 == $a->Code ? 'selected' : null}}>{{$a->name_of_account}}</option>
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="">Account Name</label>
                                                                                        @if ($isEdit)
                                                                                            @php
                                                                                                $name = \App\AccountDetails::where('Code',$o->account1)->first()
                                                                                            @endphp
                                                                                            <input  value="{{$name->name_of_account}}"  id="account_name_ddv{{$counter}}" class="form-control account_name_ddv" type="text" readonly >
                                                                                        @else
                                                                                            <input id="account_name_ddv1" class="form-control account_name_ddv" type="text" readonly >
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <!-- inner row end -->
                                                                            </div>
                                                                            <div class="row">

                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="">Amount</label>
                                                                                        <input autocomplete="off" value="{{$o->amount ?? old('amount')}}" name="amountdv[]"  class="form-control amountdv amountdv1" type="text" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="">Description</label>
                                                                                        <textarea  name="descriptiondv[]" rows="8" cols="35"  >{{$o->description ??  old('description') }}</textarea>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                            $counter++;
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <div id="debitdv">
                                                                    <div id = '1' class="dvdebit">
                                                                        <div class="row" >
                                                                            <h5>Debit Account</h5>
                                                                            {{-- <br> --}}
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="">Head Category</label>
                                                                                    <select class="form-control selectpicker head_ddv" data-id='dv1' data-live-search="true"  id="head_d" name="head2dv[]" >
                                                                                        <option value=""  selected>Select...</option>
                                                                                        @if ($isEdit)
                                                                                            @foreach ($head as $u)
                                                                                                @if ($u->name == 'Equity')
                                                                                                    <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        @else
                                                                                            @foreach ($head as $u)
                                                                                                @if ($u->name == 'Equity')
                                                                                                    <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                                @endif
                                                                                            @endforeach

                                                                                        @endif
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="">Account Code</label>
                                                                                    <select class="form-control addv account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true" name="account2dv[]" id="account_code_ddv-1" >
                                                                                        @if ($isEdit)
                                                                                        <option value=""  selected>Select...</option>
                                                                                            @foreach ($account2 as $a)
                                                                                                <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="">Account Name</label>
                                                                                    <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_ddv1" class="form-control account_name_ddv" type="text" readonly >
                                                                                </div>
                                                                            </div>
                                                                            <!-- inner row end -->
                                                                        </div>
                                                                        <div class="row">
                                                                            {{-- <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="">Accounting Date</label>
                                                                                    <input  name="date[]"  class="form-control" type="date"  value="{{$isEdit ?  $voucher->date : ''}}"  >
                                                                                </div>
                                                                            </div> --}}

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="">Amount</label>
                                                                                    <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amountdv[]"  class="form-control amountdv amountdv1" type="text" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="">Description</label>
                                                                                    <textarea  name="descriptiondv[]" rows="8" cols="35"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            <hr>
                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1dv" id="head_c" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Cash' || $u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Cash' || $u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"   name="account1dv" id="account_code_cdv" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account2 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_cdv" value="{{$isEdit ? $account2name->name_of_account : ''}}" class="form-control account_name_cdv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form group">
                                                                        <label>Final Amount</label>
                                                                        <input type="text" value="{{$voucher->amount ?? old('amount')}}" name="Amountdv" id="Amountdv" readonly class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form group">
                                                                        <label>Description</label>
                                                                        <textarea name="Descriptiondv" cols="60" rows="30">{{$voucher->description ??  old('description') }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            @if ($isEdit && $voucher->head2 == 'CA-04' && $voucher->v_type == 'DV')
                                                                @php
                                                                    $count = 1;
                                                                @endphp
                                                                <span class="btn btn-md btn-info" id="addchequedv">Add More</span>
                                                                <div id="chqdv" >
                                                                    @foreach ($th as $t)
                                                                        <div id = '{{$count}}' class="dvchq">
                                                                            <div class="row">
                                                                                <h5>Cheque Details</h5>

                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque number</label>
                                                                                        <input type="text" name="dvchqno[]" value="{{$t->cheque_no}}" class="form-control dvchqno" id="dvchq{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Type</label>
                                                                                        <select id="dvchqtype{{$count}}" name="dvchqtype[]"  class="form-control dvchqtype selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            <option {{$t->merchant_type == 'Cash' ? 'selected' : ''}} >Cash</option>
                                                                                            <option {{$t->merchant_type == 'Cross' ? 'selected' : ''}} >Cross</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Payee Title</label>
                                                                                        <input type="text" name="dvpt[]" value="{{$t->title}}"  class="form-control dvpt" id="dvpt{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Date</label>
                                                                                        <input type="date" name="dvchqdate[]" value="{{$t->chq_date}}"  class="form-control dvchqdate" id="dvchqdate{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Amount</label>
                                                                                        <input type="text" name="dvchqam[]" value="{{$t->total}}"   class="form-control dvchqam" id="dvchqam{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Bank</label>
                                                                                        <select id="dvbank{{$count}}" name="dvbank[]" class="form-control dvbank selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            @foreach ($bank as $b)
                                                                                                <option value="{{$b->id}}" {{$t->b_id == $b->id ? 'selected' : ''}}>{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <div id="chqdv" hidden>
                                                                    <span class="btn btn-md btn-info" id="addchequedv">Add More</span>
                                                                    <div id = '1' class="dvchq">
                                                                        <div class="row">
                                                                            <h5>Cheque Details</h5>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque number</label>
                                                                                    <input type="text" name="dvchqno[]" class="form-control dvchqno" id="dvchq1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Type</label>
                                                                                    <select id="dvchqtype1" name="dvchqtype[]"  class="form-control dvchqtype selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        <option>Cash</option>
                                                                                        <option>Cross</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Payee Title</label>
                                                                                    <input type="text" name="dvpt[]" class="form-control dvpt" id="dvpt1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Date</label>
                                                                                    <input type="date" name="dvchqdate[]" class="form-control dvchqdate" id="dvchqdate1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Amount</label>
                                                                                    <input type="text" name="dvchqam[]"  class="form-control dvchqam" id="dvchqam1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Bank</label>
                                                                                    <select id="dvbank1" name="dvbank[]" class="form-control dvbank selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        @foreach ($bank as $b)
                                                                                            <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            @endif
                                                        </div>

                                                        <div id="EV" {{$isEdit ? ($voucher->v_type == 'EV' ? '' : 'hidden') : 'hidden'}}>

                                                            <span class="btn btn-md btn-info" id="addev">Add More</span>
                                                            <div id="debitev">
                                                                <div id = '1' class="evdebit">
                                                                    <div class="row" >
                                                                        <h5>Debit Account</h5>
                                                                        {{-- <br> --}}
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="">Head Category</label>
                                                                                <select class="form-control selectpicker head_dev" data-id='ev1' data-live-search="true"  id="head_d" name="head2[]" >
                                                                                    <option value=""  selected>Select...</option>
                                                                                    @if ($isEdit)
                                                                                        @foreach ($head as $u)
                                                                                            @if (str_contains($u->code,'EXP'))
                                                                                                <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @else
                                                                                        @foreach ($head as $u)
                                                                                            @if (str_contains($u->code,'EXP'))
                                                                                                <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                            @endif
                                                                                        @endforeach

                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="">Account Code</label>
                                                                                <select class="form-control adev account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true" name="account2[]" id="account_code_dev-1" >
                                                                                    @if ($isEdit)
                                                                                    <option value=""  selected>Select...</option>
                                                                                        @foreach ($account2 as $a)
                                                                                            <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="">Account Name</label>
                                                                                <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_dev1" class="form-control account_name_dev" type="text" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <!-- inner row end -->
                                                                    </div>
                                                                    <div class="row">
                                                                        {{-- <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="">Accounting Date</label>
                                                                                <input  name="date[]"  class="form-control" type="date"  value="{{$isEdit ?  $voucher->date : ''}}"  >
                                                                            </div>
                                                                        </div> --}}

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="">Amount</label>
                                                                                <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amount"  class="form-control amountev amountev1" type="text" >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="">Description</label>
                                                                                <textarea  name="description[]" rows="8" cols="35"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1[]" id="head_c" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Cash' || $u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Cash' || $u->name == 'Bank')
                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"   name="account1[]" id="account_code_cev" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account1 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account1 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_cev" value="{{$isEdit ? $account1name : ''}}" class="form-control account_name_cev" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form group">
                                                                        <label>Final Amount</label>
                                                                        <input type="text" name="Amount" id="Amountev" readonly class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form group">
                                                                        <label>Description</label>
                                                                        <textarea name="description" cols="60" rows="30"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div id="chqev" hidden>
                                                                <span class="btn btn-md btn-info" id="addchequeev">Add More</span>
                                                                <div id = '1' class="evchq">
                                                                    <div class="row">
                                                                        <h5>Cheque Details</h5>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Cheque number</label>
                                                                                <input type="text" class="form-control evchqno" id="evchq1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Cheque Type</label>
                                                                                <select id="evchqtype1" class="form-control evchqtype selectpicker" data-live-search="true">
                                                                                    <option disabled selected> Select Anyone</option>
                                                                                    <option>Cash</option>
                                                                                    <option>Cross</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Payee Title</label>
                                                                                <input type="text" class="form-control evpt" id="evpt1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Cheque Date</label>
                                                                                <input type="date" class="form-control evchqdate" id="evchqdate1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Amount</label>
                                                                                <input type="text" class="form-control evchqam" id="evchqam1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Bank</label>
                                                                                <select id="evbank1" class="form-control evbank selectpicker" data-live-search="true">
                                                                                    <option disabled selected> Select Anyone</option>
                                                                                    @foreach ($bank as $b)
                                                                                        <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="RV" {{$isEdit ? ($voucher->v_type == 'RV' ? '' : 'hidden') : 'hidden'}}>
                                                            <div class="row" >
                                                                <h5>Debit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1rv" id="head_d" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank' || $u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank' || $u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true" name="account1rv" id="account_code_drv" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account1 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account1 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_drv" value="{{$isEdit ? $account1name : ''}}" class="form-control account_name_drv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker head_c" data-live-search="true"  id="head_c" name="head2rv" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Receivables')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head2 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Receivables')
                                                                                        <option  value="{{$u->code}}" {{old('head_c') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"  name="account2rv" id="account_code_crv" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($account2 as $a)
                                                                                    <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_crv" class="form-control account_name_crv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end -->
                                                            </div>
                                                            <hr>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amountrv"  class="form-control" type="text" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Description</label>
                                                                        <textarea  name="descriptionrv" rows="8" cols="58"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr>
                                                            @if ($isEdit && $voucher->head1 == 'CA-04' && $voucher->v_type == 'RV')
                                                                @php
                                                                    $count = 1;
                                                                @endphp
                                                                <span class="btn btn-md btn-info" id="addchequerv">Add More</span>

                                                                <div id="chqrv">
                                                                    @foreach ($th as $t)
                                                                        <div id = '{{$count}}' class="rvchq">
                                                                            <div class="row">
                                                                                <h5>Cheque Details</h5>

                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque number</label>
                                                                                        <input name="rvchqno[]" value="{{$t->cheque_no}}" type="text" class="form-control rvchqno" id="rvchq{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Type</label>
                                                                                        <select id="rvchqtype{{$count}}" name="rvchqtype[]" class="form-control rvchqtype selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            <option {{$t->merchant_type == 'Cash' ? 'selected' : ''}} >Cash</option>
                                                                                            <option {{$t->merchant_type == 'Cross' ? 'selected' : ''}} >Cross</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Payee Title</label>
                                                                                        <input type="text" value="{{$t->title}}" class="form-control rvpt" name="rvpt[]" id="rvpt{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Date</label>
                                                                                        <input type="date" class="form-control rvchqdate" value="{{$t->chq_date}}" name="rvchqdate[]" id="rvchqdate{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Amount</label>
                                                                                        <input type="text" class="form-control rvchqam" value="{{$t->total}}" name="rvchqam[]" id="rvchqam{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Bank</label>
                                                                                        <select id="rvbank{{$count}}" name="rvbank[]" class="form-control rvbank selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            @foreach ($bank as $b)
                                                                                                <option value="{{$b->id}}" {{$t->b_id == $b->id ? 'selected' : ''}} >{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <div id="chqrv" hidden>
                                                                    <span class="btn btn-md btn-info" id="addchequerv">Add More</span>

                                                                    <div id = '1' class="rvchq">
                                                                        <div class="row">
                                                                            <h5>Cheque Details</h5>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque number</label>
                                                                                    <input name="rvchqno[]" type="text" class="form-control rvchqno" id="rvchq1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Type</label>
                                                                                    <select id="rvchqtype1" name="rvchqtype[]" class="form-control rvchqtype selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        <option>Cash</option>
                                                                                        <option>Cross</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Payee Title</label>
                                                                                    <input type="text" class="form-control rvpt" name="rvpt[]" id="rvpt1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Date</label>
                                                                                    <input type="date" class="form-control rvchqdate" name="rvchqdate[]" id="rvchqdate1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Amount</label>
                                                                                    <input type="text" class="form-control rvchqam" name="rvchqam[]" id="rvchqam1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Bank</label>
                                                                                    <select id="rvbank1" name="rvbank[]" class="form-control rvbank selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        @foreach ($bank as $b)
                                                                                            <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            <div id="salesrv" {{($isEdit && $voucher->head2 == 'CA-01' && $voucher->v_type == 'RV') ? '' : 'hidden' }}  class="row">

                                                                <select multiple class="form-control {{$isEdit ? 'selectpicker' : ''}} " data-live-search="true" id="sale_idrv" name="s_id[]" >
                                                                    @if ($isEdit && $voucher->head2 == 'CA-01')
                                                                        @if (in_array(0,$spid) && count($vdetails) == 1)
                                                                            @foreach ($vdetails as $v)
                                                                                @if ($v->spid == 0 )
                                                                                    <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                    @break
                                                                                @endif
                                                                            @endforeach
                                                                            @foreach ($sales as $s)
                                                                                @if ($s->total != 0)
                                                                                    <option value="{{$s->id}}" >Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if (in_array(0,$spid) && count($vdetails) > 1)
                                                                                @foreach ($vdetails as $v)
                                                                                    @if ($v->spid == 0 )
                                                                                        <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                        @break
                                                                                    @endif
                                                                                @endforeach
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($sales as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @else

                                                                                <option value="0" >Opening Account: {{$ledger}}</option>
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($sales as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >Sale No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="VPV" {{$isEdit ? ($voucher->v_type == 'VPV' ? '' : 'hidden') : 'hidden'}}>
                                                            <div class="row" >
                                                                <h5>Debit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker" data-live-search="true" name="head1vpv" id="head_d" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Payables')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Payables')
                                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_d {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true" name="account1vpv" id="account_code_dvpv" >
                                                                            <option value=""  selected>Select...</option>
                                                                                @if ($isEdit)
                                                                                    @foreach ($account1 as $a)
                                                                                        <option value="{{$a->Code}}" {{$voucher->account1 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                    @endforeach
                                                                                @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  id="account_name_dvpv" value="{{$isEdit ? $account1name : ''}}" class="form-control account_name_dvpv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end     -->
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <h5>Credit Account</h5>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Head Category</label>
                                                                        <select class="form-control selectpicker head_c" data-live-search="true"  id="head_c" name="head2vpv" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank' || $u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{$voucher->head1 == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                @foreach ($head as $u)
                                                                                    @if ($u->name == 'Bank' || $u->name == 'Cash')
                                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                                                                    @endif
                                                                                @endforeach

                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <select class="form-control account_code_c {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"  name="account2vpv" id="account_code_cvpv" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @if ($isEdit)
                                                                                @foreach ($account2 as $a)
                                                                                    <option value="{{$a->Code}}" {{$voucher->account2 == $a->Code ? 'selected' : null}}>{{$a->Code}} - {{$a->name_of_account}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Account Name</label>
                                                                        <input  value="{{$isEdit ? $account2name->name_of_account : ''}}"  id="account_name_cvpv" class="form-control account_name_cvpv" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <!-- inner row end -->
                                                            </div>
                                                            <hr>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input autocomplete="off" value="{{$voucher->amount ?? old('amount')}}" name="amountvpv"  class="form-control" type="text" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="">Description</label>
                                                                        <textarea  name="descriptionvpv" rows="8" cols="58"  >{{$voucher->description ??  old('description') }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr>
                                                            @if ($isEdit && $voucher->head2 == 'CA-04' && $voucher->v_type == 'VPV')
                                                                @php
                                                                    $count = 1;
                                                                @endphp
                                                                <span class="btn btn-md btn-info" id="addchequevpv">Add More</span>

                                                                <div id="chqvpv">
                                                                    @foreach ($th as $t)
                                                                        <div id = '{{$count}}' class="vpvchq">
                                                                            <div class="row">
                                                                                <h5>Cheque Details</h5>

                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque number</label>
                                                                                        <input name="vpvchqno[]" value="{{$t->cheque_no}}" type="text" class="form-control vpvchqno" id="vpvchq{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Type</label>
                                                                                        <select id="vpvchqtype{{$count}}" name="vpvchqtype[]" class="form-control vpvchqtype selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            <option {{$t->merchant_type == 'Cash' ? 'selected' : ''}} >Cash</option>
                                                                                            <option {{$t->merchant_type == 'Cross' ? 'selected' : ''}} >Cross</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Payee Title</label>
                                                                                        <input type="text" value="{{$t->title}}" class="form-control vpvpt" name="vpvpt[]" id="vpvpt{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Cheque Date</label>
                                                                                        <input type="date" class="form-control vpvchqdate" value="{{$t->chq_date}}" name="vpvchqdate[]" id="vpvchqdate{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Amount</label>
                                                                                        <input type="text" class="form-control vpvchqam" value="{{$t->total}}" name="vpvchqam[]" id="vpvchqam{{$count}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label>Bank</label>
                                                                                        <select id="vpvbank{{$count}}" name="vpvbank[]" class="form-control vpvbank selectpicker" data-live-search="true">
                                                                                            <option disabled selected> Select Anyone</option>
                                                                                            @foreach ($bank as $b)
                                                                                                <option value="{{$b->id}}" {{$t->b_id == $b->id ? 'selected' : ''}} >{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <div id="chqvpv" hidden>
                                                                    <span class="btn btn-md btn-info" id="addchequevpv">Add More</span>

                                                                    <div id = '1' class="vpvchq">
                                                                        <div class="row">
                                                                            <h5>Cheque Details</h5>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque number</label>
                                                                                    <input name="vpvchqno[]" type="text" class="form-control vpvchqno" id="vpvchq1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Type</label>
                                                                                    <select id="vpvchqtype1" name="vpvchqtype[]" class="form-control vpvchqtype selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        <option>Cash</option>
                                                                                        <option>Cross</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Payee Title</label>
                                                                                    <input type="text" class="form-control vpvpt" name="vpvpt[]" id="vpvpt1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Cheque Date</label>
                                                                                    <input type="date" class="form-control vpvchqdate" name="vpvchqdate[]" id="vpvchqdate1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Amount</label>
                                                                                    <input type="text" class="form-control vpvchqam" name="vpvchqam[]" id="vpvchqam1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label>Bank</label>
                                                                                    <select id="vpvbank1" name="vpvbank[]" class="form-control vpvbank selectpicker" data-live-search="true">
                                                                                        <option disabled selected> Select Anyone</option>
                                                                                        @foreach ($bank as $b)
                                                                                            <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div id="purchasevpv" {{($isEdit && $voucher->head1 == 'CL-01' && $voucher->v_type == 'VPV') ? '' : 'hidden' }} class="row">

                                                                <select multiple class="form-control {{$isEdit ? 'selectpicker' : ''}}" data-live-search="true"   id="purchase_id" name="p_id[]" >
                                                                    @if ($isEdit && $voucher->head1 == 'CL-01')
                                                                        @if (in_array(0,$spid) && count($vdetails) == 1)
                                                                            @foreach ($vdetails as $v)
                                                                                @if ($v->spid == 0 )
                                                                                    <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                    @break
                                                                                @endif
                                                                            @endforeach
                                                                            @foreach ($purchase as $s)
                                                                                @if ($s->total != 0)
                                                                                    <option value="{{$s->id}}" >PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if (in_array(0,$spid) && count($vdetails) > 1)
                                                                                @foreach ($vdetails as $v)
                                                                                    @if ($v->spid == 0 )
                                                                                        <option value="0" selected>Opening Account: {{$ledger}}</option>
                                                                                        @break
                                                                                    @endif
                                                                                @endforeach
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($purchase as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @else

                                                                                <option value="0" >Opening Account: {{$ledger}}</option>
                                                                                @foreach ($vdetails as $vd)
                                                                                    @if ($vd->spid != 0)
                                                                                        @foreach ($purchase as $s)
                                                                                            @if ($vd->spid == $s->id)
                                                                                                <option value="{{$s->id}}" selected>PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                            @else
                                                                                                @if ($s->total != 0)
                                                                                                    <option value="{{$s->id}}" >PO No: {{$s->id}} - Total: {{$s->total}}</option>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </select>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-1 ">
                                                        <button type="submit" class="btn green">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

<script>
    $('#voucherForm').validate({
        rules: {
            head1: {
                required: true,
            },
            head2: {
                required: true,
            },
            account1: {
                required: true,
            },
            account2: {
                required: true,
            },
            date: {
                required: true,
            },
            amount: {
                required: true,
            },
            description: {
                required: true,
            },
            b_id: {
                required: true,
            },
            v_type: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>

<script>
    var countd = 0;
    var countc = 0;
    var headc = 0;
    var headd = 0;
    var countDebitjv=0;
    var countCreditjv=0;
    var countCreditcbd =0;
    var countDebitcbd=0;
    var countCreditcwd =0;
    var countDebitcwd=0;
    var countCreditdv =0;
    var countDebitdv=0;
    var countCreditev =0;
    var countDebitev=0;
    var countDebitrv=0;
    var countCreditvpv=0;

    function recursiveDJV(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_djv').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveDJV(children.accounts,count);
            }
        });
    }

    function recursiveCJV(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_cjv').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveCJV(children.accounts,count);
            }
        });
    }

    function recursiveDCBD(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_dcbd').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveDCBD(children.accounts,count);
            }
        });
    }

    function recursiveCCBD(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_ccbd').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveCCBD(children.accounts,count);
            }
        });
    }

    function recursiveDCWD(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_dcwd').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveDCWD(children.accounts,count);
            }
        });
    }

    function recursiveCCWD(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_ccwd').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveCCWD(children.accounts,count);
            }
        });
    }

    function recursiveDDV(account,count,pid)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_ddv-'+pid).append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveDDV(children.accounts,count,pid);
            }
        });
    }

    function recursiveCDV(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_cdv').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveCDV(children.accounts,count);
            }
        });
    }

    function recursiveDEV(account,count,pid)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_dev-'+pid).append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveDEV(children.accounts,count,pid);
            }
        });
    }

    function recursiveCEV(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_cev').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveCEV(children.accounts,count);
            }
        });
    }

    function recursiveCVPV(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_cvpv').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveCVPV(children.paccounts,count);
            }
        });
    }
    function recursiveDRV(account,count)
    {
        var text = '&emsp;';

        account.forEach(children => {
            $('#account_code_drv').append(`<option value = `+children.Code+`>`+text.repeat(count)+children.name_of_account+`</option>`);
            if(children.accounts)
            {
                count++;
                recursiveDRV(children.accounts,count);
            }
        });
    }

    $(document).on('change','.account_code_d',function(){
        var id=$(this).val();
        var Regex='CL-01';
        var Regex1='NCL-01';
        var parentid = $(this).attr('id');
        if(id != '')
        {
            $.ajax({
                url:"{{url('')}}/accountdetails/"+id,
                method:"GET",
                error: function (request, error) {
                    alert(" Can't do because: " + error +request);
                },
                success:function(data){

                        if(countd == 0){
                            if(id.match(Regex) && !(id.match(Regex1)))
                            {
                                $.ajax({
                                    url:"{{url('')}}/generalLedger/find/supplier/"+data.name_of_account,
                                    method:"GET",
                                    error: function (request1, error1) {
                                                alert(" Can't do because: " + error1 +request1);
                                            },
                                    success:function(dataa){
                                        // console.log(dataa);
                                        if(dataa[0].length > 0 || dataa[1]!=0)
                                        {
                                            $('#purchase_id').empty();
                                            $('#purchase_id').selectpicker('destroy');
                                            $('#purchasevpv').show();
                                            $('#purchase_id').append(`<option disabled >Select...</option> `);
                                            if(dataa[1]!=0)
                                            {
                                                $('#purchase_id').append(`<option value = '0' >Opening Amount : `+dataa[1]+`</option> `);

                                            }
                                            for (let i = 0; i < dataa[0].length; i++) {
                                                if(dataa[0][i].total_amount == null)
                                                {
                                                    total = 0;
                                                }
                                                else
                                                {
                                                    total = dataa[0][i].total_amount;
                                                }
                                                total = dataa[0][i].total - total;
                                                // if(total != 0){

                                                    $('#purchase_id').append(`<option value='`+dataa[0][i].id+`'>PO No: `+dataa[0][i].id+` - Total: `+total+`</option>`);
                                                // }
                                            }
                                            $('#purchase_id').addClass('selectpicker');
                                            $('.selectpicker').selectpicker('render');
                                        }
                                        else
                                        {

                                            alert('All dues are clear!');
                                        }
                                    }
                                });
                            }
                            else
                            {
                                $('#purchasevpv').hide();
                                $('#purchase_id').empty();
                            }
                        }
                        if($('.form-check-input:checked').val() == 'JV')
                        {
                            if(countd == 0)
                            {
                                $('#account_name_djv').val(data.name_of_account);
                                countd ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'CBD')
                        {
                            if(countd == 0)
                            {
                                $('#account_name_dcbd').val(data.name_of_account);
                                countd ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'CWD')
                        {
                            if(countd == 0)
                            {
                                $('#account_name_dcwd').val(data.name_of_account);
                                countd ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'RV')
                        {
                            if(countd == 0)
                            {
                                $('#account_name_drv').val(data.name_of_account);
                                countd ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'VPV')
                        {
                            if(countd == 0)
                            {
                                $('#account_name_dvpv').val(data.name_of_account);
                                countd ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'DV')
                        {
                            parentid = parentid.substring(parentid.indexOf('-') + 1);
                            $('#account_name_ddv'+parentid).val(data.name_of_account);
                            countd ++;
                        }
                        if($('.form-check-input:checked').val() == 'EV')
                        {
                            parentid = parentid.substring(parentid.indexOf('-') + 1);
                            $('#account_name_dev'+parentid).val(data.name_of_account);
                            countd ++;
                        }


                    // }
                }
            });
        }
    });

    $(document).on('change','.account_code_c',function(){
        var id=$(this).val();
        var parentid = $(this).attr('id');
        var Regex='CA-01';
        var Regex1='NCA-01';
        if(id != '')
        {
            $.ajax({
                url:"{{url('')}}/accountdetails/"+id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                        if(countc == 0)
                        {
                            if(id.match(Regex) && !(id.match(Regex1)))
                            {
                                $.ajax({
                                    url:"{{url('')}}/generalLedger/find/customer/"+data.name_of_account,
                                    method:"GET",
                                    error: function (request1, error1) {
                                                alert(" Can't do because: " + error1 +request1);
                                            },
                                    success:function(dataa){
                                        if(dataa[0].length > 0 || dataa[1]!=0)
                                        {
                                            $('#sale_idrv').empty();
                                            $('#sale_idrv').selectpicker('destroy');
                                            $('#salesrv').show();
                                            $('#sale_idrv').append(`<option disabled >Select...</option> `);
                                            if(dataa[1]!=0)
                                            {
                                                $('#sale_idrv').append(`<option value = '0' >Opening Amount : `+dataa[1]+`</option> `);

                                            }
                                            for (let i = 0; i < dataa[0].length; i++) {
                                                if(dataa[0][i].total_amount == null)
                                                {
                                                    total = 0;
                                                }
                                                else
                                                {
                                                    total = dataa[0][i].total_amount;
                                                }
                                                var ramount = 0;
                                                if(dataa[0][i].return != null)
                                                {
                                                    ramount = dataa[0][i].return.total;
                                                }
                                                else
                                                {
                                                    ramount = 0;
                                                }
                                                total = dataa[0][i].total - total - ramount;
                                                // if(total != 0)
                                                // {

                                                    $('#sale_idrv').append(`<option value='`+dataa[0][i].id+`'>Sale No: `+dataa[0][i].id+` - Total: `+total+`</option>`);
                                                // }
                                            }
                                            $('#sale_idrv').addClass('selectpicker');
                                            $('.selectpicker').selectpicker('render');
                                        }
                                        else
                                        {
                                            alert('All dues are clear!');
                                        }
                                    }
                                });
                            }
                            else
                            {
                                $('#salesrv').hide();
                                $('#sale_idrv').empty();
                            }
                        }
                        if($('.form-check-input:checked').val() == 'JV')
                        {
                            if(countc == 0)
                            {
                                $('#account_name_cjv').val(data.name_of_account);
                                countc ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'DV')
                        {
                            if(countc == 0)
                            {
                                $('#account_name_cdv').val(data.name_of_account);
                                countc ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'EV')
                        {
                            if(countc == 0)
                            {
                                $('#account_name_cev').val(data.name_of_account);
                                countc ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'CBD')
                        {
                            if(countc == 0)
                            {
                                $('#account_name_ccbd').val(data.name_of_account);
                                countc ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'CWD')
                        {
                            if(countc == 0)
                            {
                                $('#account_name_ccwd').val(data.name_of_account);
                                countc ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'RV')
                        {
                            console.log(countc);
                            if(countc == 0)
                            {
                                $('#account_name_crv').val(data.name_of_account);
                                countc ++;
                            }
                        }
                        if($('.form-check-input:checked').val() == 'VPV')
                        {
                            console.log(countc);
                            if(countc == 0)
                            {
                                $('#account_name_cvpv').val(data.name_of_account);
                                countc ++;
                            }
                        }

                    // }
                }
            });
        }
    });

    $(document).on('change','#head_d',function(){
        var id=$(this).val();
        var parentid = $(this).attr('data-id');
        $.ajax({
            url:"{{url('')}}/headcategory/account/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                if($('.form-check-input:checked').val() == 'JV')
                {
                    $("#account_code_djv").empty();
                    $('#account_code_djv').selectpicker('destroy');
                    $("#account_code_djv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_djv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countDebitjv = 2;
                            recursiveDJV(data[i].accounts,countDebitjv);
                        }
                    }
                    $('#account_code_djv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

                if($('.form-check-input:checked').val() == 'CBD')
                {
                    $("#account_code_dcbd").empty();
                    $('#account_code_dcbd').selectpicker('destroy');
                    $("#account_code_dcbd").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_dcbd").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countDebitcbd = 2;
                            recursiveDCBD(data[i].accounts,countDebitcbd);
                        }
                    }
                    $('#account_code_dcbd').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'RV')
                {
                    if(id == 'CA-04')
                    {
                        $('#chqrv').show();
                    }
                    else
                    {
                        $('#chqrv').hide();
                    }
                    $("#account_code_drv").empty();
                    $('#account_code_drv').selectpicker('destroy');
                    $("#account_code_drv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_drv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countDebitrv = 2;
                            recursiveDRV(data[i].accounts,countDebitrv);
                        }
                    }
                    $('#account_code_drv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'VPV')
                {
                    // if(id == 'CA-04')
                    // {
                    //     $('#chqvpv').show();
                    // }
                    // else
                    // {
                    //     $('#chqvpv').hide();
                    // }
                    $("#account_code_dvpv").empty();
                    $('#account_code_dvpv').selectpicker('destroy');
                    $("#account_code_dvpv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_dvpv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        // if(data[i].accounts.length > 0)
                        // {
                        //     countDebitvpv = 2;
                        //     recursiveDvpv(data[i].accounts,countDebitvpv);
                        // }
                    }
                    $('#account_code_dvpv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'CWD')
                {
                    $("#account_code_dcwd").empty();
                    $('#account_code_dcwd').selectpicker('destroy');
                    $("#account_code_dcwd").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_dcwd").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countDebitcwd = 2;
                            recursiveDCWD(data[i].accounts,countDebitcwd);
                        }
                    }
                    $('#account_code_dcwd').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'DV')
                {
                    parentid = parentid.substring(parentid.indexOf('v')+1);
                    $("#account_code_ddv-"+parentid).empty();
                    $('#account_code_ddv-'+parentid).selectpicker('destroy');
                    $("#account_code_ddv-"+parentid).append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_ddv-"+parentid).append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countDebitdv = 2;
                            recursiveDDV(data[i].accounts,countDebitdv,parentid);
                        }
                    }
                    $('#account_code_ddv-'+parentid).addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

                if($('.form-check-input:checked').val() == 'EV')
                {
                    parentid = parentid.substring(parentid.indexOf('v')+1);
                    console.log(parentid);
                    $("#account_code_dev-"+parentid).empty();
                    $('#account_code_dev-'+parentid).selectpicker('destroy');
                    $("#account_code_dev-"+parentid).append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_dev-"+parentid).append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countDebitev = 2;
                            recursiveDEV(data[i].accounts,countDebitev,parentid);
                        }
                    }
                    $('#account_code_dev-'+parentid).addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
            }
        });
    });

    $(document).on('change','#head_c',function(){
        var id=$(this).val();
        var parentid = $(this).attr('data-id');

        $.ajax({
            url:"{{url('')}}/headcategory/account/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                // debugger
                if($('.form-check-input:checked').val() == 'JV')
                {
                    $("#account_code_cjv").empty();
                    $('#account_code_cjv').selectpicker('destroy');
                    $("#account_code_cjv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_cjv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countCreditjv = 2;
                            recursiveCJV(data[i].accounts,countCreditjv);
                        }
                    }
                    $('#account_code_cjv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'DV')
                {
                    if(id == 'CA-04')
                    {
                        $('#chqdv').show();
                    }
                    else
                    {
                        $('#chqdv').hide();
                    }
                    $("#account_code_cdv").empty();
                    $('#account_code_cdv').selectpicker('destroy');
                    $("#account_code_cdv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_cdv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countCreditdv = 2;
                            recursiveCDV(data[i].accounts,countCreditdv);
                        }
                    }
                    $('#account_code_cdv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'EV')
                {
                    if(id == 'CA-04')
                    {
                        $('#chqev').show();
                    }
                    else
                    {
                        $('#chqev').hide();
                    }
                    $("#account_code_cev").empty();
                    $('#account_code_cev').selectpicker('destroy');
                    $("#account_code_cev").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_cev").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countCreditev = 2;
                            recursiveCEV(data[i].accounts,countCreditev);
                        }
                    }
                    $('#account_code_cev').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

                if($('.form-check-input:checked').val() == 'CBD')
                {
                    if(id == 'CA-04')
                    {
                        $('#chqcbd').show();
                    }
                    else
                    {
                        $('#chqcbd').hide();
                    }
                    $("#account_code_ccbd").empty();
                    $('#account_code_ccbd').selectpicker('destroy');
                    $("#account_code_ccbd").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_ccbd").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countCreditcbd = 2;
                            recursiveCCBD(data[i].accounts,countCreditcbd);
                        }
                    }
                    $('#account_code_ccbd').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

                if($('.form-check-input:checked').val() == 'CWD')
                {
                    $("#account_code_ccwd").empty();
                    $('#account_code_ccwd').selectpicker('destroy');
                    $("#account_code_ccwd").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_ccwd").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countCreditcwd = 2;
                            recursiveCCWD(data[i].accounts,countCreditcwd);
                        }
                    }
                    $('#account_code_ccwd').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'VPV')
                {
                    if(id == 'CA-04')
                    {
                        $('#chqvpv').show();
                    }
                    else
                    {
                        $('#chqvpv').hide();
                    }
                    $("#account_code_cvpv").empty();
                    $('#account_code_cvpv').selectpicker('destroy');
                    $("#account_code_cvpv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_cvpv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                        if(data[i].accounts.length > 0)
                        {
                            countCreditvpv = 2;
                            recursiveCVPV(data[i].accounts,countCreditvpv);
                        }
                    }
                    $('#account_code_cvpv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if($('.form-check-input:checked').val() == 'RV')
                {
                    $("#account_code_crv").empty();
                    $('#account_code_crv').selectpicker('destroy');
                    $("#account_code_crv").append(' <option value=""  selected>Select...</option>');
                    for(var i = 0 ; i < data.length ; i++)
                    {
                        $("#account_code_crv").append('<option value='+data[i].Code+'>'+data[i].name_of_account+'</option>');
                    }
                    $('#account_code_crv').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

            }
        });
    });

    $(document).on('change','#b_id',function(){
        var option = $('input:radio[name="v_type"]');
        if(option.is(':checked'))
        {
            var selected = $("input[type='radio'][name='v_type']:checked").val();
            // console.log(selected);
            var id = $(this).val();
            $.ajax({
                url:"{{url('')}}/JournalVoucher/biller/"+id+'-'+selected,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    // console.log(data);
                    $('#v_no').val(data[1]+data[0]);
                    $('#vcno').val(data[0]);
                }
            });
        }
        else
        {
            $(".bid option:selected").removeAttr("selected");
            $('.bid').selectpicker('refresh');
            alert ('Select Voucher Type First');
        }
    });

    $('input:radio[name="v_type"]').change(function(){
        if ($(this).is(':checked')) {
            $('#search').prop('disabled',false);
            var val = $(this).val();
            // console.log(val);
            if(val == 'JV')
            {
                $("#JV").show();
                $("#CBD").hide();
                $("#CWD").hide();
                $("#DV").hide();
                $("#EV").hide();
                $("#RV").hide();
                $("#VPV").hide();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
            if(val == 'CBD')
            {
                $("#JV").hide();
                $("#CBD").show();
                $("#CWD").hide();
                $("#VPV").hide();
                $("#EV").hide();
                $("#DV").hide();
                $("#RV").hide();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
            if(val == 'CWD')
            {
                $("#JV").hide();
                $("#CBD").hide();
                $("#CWD").show();
                $("#VPV").hide();
                $("#DV").hide();
                $("#EV").hide();
                $("#RV").hide();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
            if(val == 'DV')
            {
                $("#JV").hide();
                $("#CBD").hide();
                $("#CWD").hide();
                $("#DV").show();
                $("#VPV").hide();
                $("#EV").hide();
                $("#RV").hide();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
            if(val == 'EV')
            {
                $("#JV").hide();
                $("#CBD").hide();
                $("#CWD").hide();
                $("#DV").hide();
                $("#VPV").hide();
                $("#RV").hide();
                $("#EV").show();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
            if(val == 'RV')
            {
                $("#JV").hide();
                $("#CBD").hide();
                $("#CWD").hide();
                $("#VPV").hide();
                $("#DV").hide();
                $("#RV").show();
                $("#EV").hide();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
            if(val == 'VPV')
            {
                $("#JV").hide();
                $("#CBD").hide();
                $("#CWD").hide();
                $("#VPV").show();
                $("#DV").hide();
                $("#RV").hide();
                $("#EV").hide();
                $('#v_no').val('');
                $(".bid option:selected").removeAttr("selected");
                $('.bid').selectpicker('refresh');
                countd = 0;
                countc = 0;
                headc = 0;
                headd = 0;
            }
        }
    });



    $('#addchequecbd').on('click',function(){
        var id = $('#cbdchq').children().last().attr('id');
        id ++;
        $('#chqcbd').append(`<div id = '`+id+`' class="cbdchq">
            <div class="row">
                <h5>Cheque Details <button type='button' id='cbd`+id+`' class='btn red dltcbd deletechq' ><i class='fa fa-trash'></i></button> </h5>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque number</label>
                        <input type="text" class="form-control cbdchqno" name='cbdchqno[]' id="cbdchq`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Type</label>
                        <select id="cbdchqtype`+id+`" class="form-control cbdchqtype" name='cbdchqtype[]' data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            <option>Cash</option>
                            <option>Cross</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Payee Title</label>
                        <input type="text" class="form-control cbdpt" name='cbdpt[]' id="cbdpt`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Date</label>
                        <input type="date" class="form-control cbdchqdate" name='cbdchqdate[]' id="cbdchqdate`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control cbdchqam" name='cbdchqam[]' id="cbdchqam`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bank</label>
                        <select id="cbdbank`+id+`" class="form-control cbdbank" name='cbdbank[]' data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            @foreach ($bank as $b)
                                <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>`);
        $('.cbdchqtype').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
        $('.cbdbank').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });

    $('#addchequecwd').on('click',function(){
        var id = $('#cwdchq').children().last().attr('id');
        id ++;
        $('#chqcwd').append(`<div id = '`+id+`' class="cwdchq">
            <div class="row">
                <h5>Cheque Details <button type='button' id='cwd`+id+`' class='btn red dltcwd deletechq' ><i class='fa fa-trash'></i></button> </h5>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque number</label>
                        <input type="text" class="form-control cwdchqno" name='cwdchqno[]' id="cwdchq`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Type</label>
                        <select id="cwdchqtype`+id+`" class="form-control cwdchqtype" name='cwdchqtype[]' data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            <option>Cash</option>
                            <option>Cross</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Payee Title</label>
                        <input type="text" class="form-control cwdpt" name='cwdpt[]' id="cwdpt`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Date</label>
                        <input type="date" class="form-control cwdchqdate" name='cwdchqdate[]' id="cwdchqdate`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control cwdchqam" name='cwdchqam[]' id="cwdchqam`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bank</label>
                        <select id="cwdbank`+id+`" class="form-control cwdbank" name='cwdbank[]' data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            @foreach ($bank as $b)
                                <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>`);
        $('.cwdchqtype').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
        $('.cwdbank').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });

    $('#addchequedv').on('click',function(){
        var id = $('#dvchq').children().last().attr('id');
        id ++;
        $('#chqdv').append(`<div id = '`+id+`' class="dvchq">
            <div class="row">
                <h5>Cheque Details <button type='button' id='dv`+id+`' class='btn red dltdv deletechq' ><i class='fa fa-trash'></i></button> </h5>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque number</label>
                        <input type="text" class="form-control dvchqno" id="dvchq`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Type</label>
                        <select id="dvchqtype`+id+`" class="form-control dvchqtype" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            <option>Cash</option>
                            <option>Cross</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Payee Title</label>
                        <input type="text" class="form-control dvpt" id="dvpt`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Date</label>
                        <input type="date" class="form-control dvchqdate" id="dvchqdate`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control dvchqam" id="dvchqam`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bank</label>
                        <select id="dvbank`+id+`" class="form-control dvbank" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            @foreach ($bank as $b)
                                <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>`);
        $('.dvchqtype').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
        $('.dvbank').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });
    $('#addchequeev').on('click',function(){
        var id = $('#evchq').children().last().attr('id');
        id ++;
        $('#chqev').append(`<div id = '`+id+`' class="evchq">
            <div class="row">
                <h5>Cheque Details <button type='button' id='ev`+id+`' class='btn red dltev deletechq' ><i class='fa fa-trash'></i></button> </h5>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque number</label>
                        <input type="text" class="form-control evchqno" id="evchq`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Type</label>
                        <select id="evchqtype`+id+`" class="form-control evchqtype" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            <option>Cash</option>
                            <option>Cross</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Payee Title</label>
                        <input type="text" class="form-control evpt" id="evpt`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Date</label>
                        <input type="date" class="form-control evchqdate" id="evchqdate`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control evchqam" id="evchqam`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bank</label>
                        <select id="evbank`+id+`" class="form-control evbank" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            @foreach ($bank as $b)
                                <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>`);
        $('.evchqtype').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
        $('.evbank').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });
    $('#addchequerv').on('click',function(){
        var id = $('#rvchq').children().last().attr('id');
        id ++;
        $('#chqrv').append(`<div id = '`+id+`' class="rvchq">
            <div class="row">
                <h5>Cheque Details <button type='button' id='rv`+id+`' class='btn red dltrv deletechq' ><i class='fa fa-trash'></i></button> </h5>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque number</label>
                        <input type="text" class="form-control rvchqno" id="rvchq`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Type</label>
                        <select id="rvchqtype`+id+`" class="form-control rvchqtype" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            <option>Cash</option>
                            <option>Cross</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Payee Title</label>
                        <input type="text" class="form-control rvpt" id="rvpt`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Date</label>
                        <input type="date" class="form-control rvchqdate" id="rvchqdate`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control rvchqam" id="rvchqam`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bank</label>
                        <select id="rvbank`+id+`" class="form-control rvbank" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            @foreach ($bank as $b)
                                <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>`);
        $('.rvchqtype').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
        $('.rvbank').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });
    $('#addchequevpv').on('click',function(){
        var id = $('#vpvchq').children().last().attr('id');
        id ++;
        $('#chqvpv').append(`<div id = '`+id+`' class="vpvchq">
            <div class="row">
                <h5>Cheque Details <button type='button' id='vpv`+id+`' class='btn red dltvpv deletechq' ><i class='fa fa-trash'></i></button> </h5>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque number</label>
                        <input type="text" class="form-control vpvchqno" id="vpvchq`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Type</label>
                        <select id="vpvchqtype`+id+`" class="form-control vpvchqtype" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            <option>Cash</option>
                            <option>Cross</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Payee Title</label>
                        <input type="text" class="form-control vpvpt" id="vpvpt`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cheque Date</label>
                        <input type="date" class="form-control vpvchqdate" id="vpvchqdate`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control vpvchqam" id="vpvchqam`+id+`">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bank</label>
                        <select id="vpvbank`+id+`" class="form-control vpvbank" data-live-search="true">
                            <option disabled selected> Select Anyone</option>
                            @foreach ($bank as $b)
                                <option value="{{$b->id}}">{{'Name: '.$b->name.' Branch: '.$b->branch}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>`);
        $('.vpvchqtype').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
        $('.vpvbank').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });

    $('#adddv').on('click',function(){
        var id = $('#debitdv').children().last().attr('id');
        id ++;
        $('#debitdv').append(`<div id = '`+id+`' class="dvdebit">
            <div class="row" >
                <h5>Debit Account <button type='button' id='dv`+id+`' class='btn dltdv red delete' ><i class='fa fa-trash'></i></button></h5>
                {{-- <br> --}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Head Category</label>
                        <select class="form-control head_ddv" data-id='dv`+id+`' data-live-search="true"  id="head_d" name="head2dv[]" >
                            <option value=""  selected>Select...</option>

                            @foreach ($head as $u)
                                @if ($u->name == 'Equity')
                                    <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Account Code</label>
                        <select class="form-control account_code_d addv" data-live-search="true"  name="account2dv[]" id="account_code_ddv-`+id+`" >

                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Account Name</label>
                        <input  id="account_name_ddv`+id+`" class="form-control account_name_ddv" type="text" readonly >
                    </div>
                </div>
                <!-- inner row end -->
            </div>
            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Amount</label>
                        <input autocomplete="off" name="amountdv[]"  class="form-control amountdv amountdv`+id+`" type="text" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea  name="descriptiondv[]" rows="8" cols="35"  ></textarea>
                    </div>
                </div>

            </div>
        </div>`);
        $('.head_ddv').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });
    $('#addev').on('click',function(){
        var id = $('#debitev').children().last().attr('id');
        id ++;
        $('#debitev').append(`<div id = '`+id+`' class="evdebit">
            <div class="row" >
                <h5>Debit Account <button type='button' id='ev`+id+`' class='btn dltev red delete' ><i class='fa fa-trash'></i></button></h5>
                {{-- <br> --}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Head Category</label>
                        <select class="form-control head_dev" data-id='ev`+id+`' data-live-search="true"  id="head_d" name="head2[]" >
                            <option value=""  selected>Select...</option>

                            @foreach ($head as $u)
                                @if (str_contains($u->code,'EXP'))
                                    <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->name}}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Account Code</label>
                        <select class="form-control account_code_d adev" data-live-search="true"  name="account2[]" id="account_code_dev-`+id+`" >

                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Account Name</label>
                        <input  id="account_name_dev`+id+`" class="form-control account_name_dev" type="text" readonly >
                    </div>
                </div>
                <!-- inner row end -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Accounting Date</label>
                        <input  name="date[]"  class="form-control" type="date"   >
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Amount</label>
                        <input autocomplete="off" name="amount"  class="form-control amountev amountev`+id+`" type="text" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea  name="description[]" rows="8" cols="35"  ></textarea>
                    </div>
                </div>

            </div>
        </div>`);
        $('.head_dev').addClass('selectpicker');
        $('.selectpicker').selectpicker('render');
    });


    $(document).on('keyup','.amountdv',function(){
        var amount=0;
        var id = $('#debitdv').children().last().attr('id');
        var count = +id + +1;
        for (let i = 1; i < count; i++) {
            if($('.amountdv'+i).val() != '')
            {
                amount = +amount + +$('.amountdv'+i).val();
            }
            $('#Amountdv').val(amount);
        }
    });
    $(document).on('keyup','.amountev',function(){
        var amount=0;
        var id = $('#debitev').children().last().attr('id');
        var count = +id + +1;
        for (let i = 1; i < count; i++) {
            if($('.amountev'+i).val() != '')
            {
                amount = +amount + +$('.amountev'+i).val();
            }
            $('#Amountev').val(amount);
        }
    });

    $(document).on('click','.deletechq',function(){
        if($('.form-check-input:checked').val() == 'CWD')
        {
            $(this).closest('.cwdchq').remove();
            $('.cwdchqtype').selectpicker('destroy');
            $('.cwdbank ').selectpicker('destroy');
            $(".cwdchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".cwdchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".cwdchqno").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cwdchq'+(i+1));
            });
            $(".cwdchqtype").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cwdchqtype'+(i+1));
                $('.cwdchqtype').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".cwdchqdate").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cwdchqdate'+(i+1));
            });
            $(".cwdchqam").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cwdchqam'+(i+1));
            });
            $(".cwdpt").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cwdpt'+(i+1));
            });
            $(".cwdbank").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cwdbank'+(i+1));
                $('.cwdbank').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
        }
        if($('.form-check-input:checked').val() == 'CBD')
        {
            $(this).closest('.cbdchq').remove();
            $('.cbdchqtype').selectpicker('destroy');
            $('.cbdbank ').selectpicker('destroy');
            $(".cbdchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".cbdchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".cbdchqno").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cbdchq'+(i+1));
            });
            $(".cbdchqtype").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cbdchqtype'+(i+1));
                $('.cbdchqtype').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".cbdchqdate").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cbdchqdate'+(i+1));
            });
            $(".cbdchqam").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cbdchqam'+(i+1));
            });
            $(".cbdpt").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cbdpt'+(i+1));
            });
            $(".cbdbank").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'cbdbank'+(i+1));
                $('.cbdbank').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
        }
        if($('.form-check-input:checked').val() == 'DV')
        {
            $(this).closest('.dvchq').remove();
            $('.dvchqtype').selectpicker('destroy');
            $('.dvbank ').selectpicker('destroy');
            $(".dvchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".dvchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".dvchqno").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dvchq'+(i+1));
            });
            $(".dvchqtype").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dvchqtype'+(i+1));
                $('.dvchqtype').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".dvchqdate").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dvchqdate'+(i+1));
            });
            $(".dvchqam").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dvchqam'+(i+1));
            });
            $(".dvpt").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dvpt'+(i+1));
            });
            $(".dvbank").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dvbank'+(i+1));
                $('.dvbank').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
        }
        if($('.form-check-input:checked').val() == 'EV')
        {
            $(this).closest('.evchq').remove();
            $('.evchqtype').selectpicker('destroy');
            $('.evbank ').selectpicker('destroy');
            $(".evchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".evchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".evchqno").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'evchq'+(i+1));
            });
            $(".evchqtype").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'evchqtype'+(i+1));
                $('.evchqtype').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".evchqdate").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'evchqdate'+(i+1));
            });
            $(".evchqam").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'evchqam'+(i+1));
            });
            $(".evpt").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'evpt'+(i+1));
            });
            $(".evbank").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'evbank'+(i+1));
                $('.evbank').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
        }
        if($('.form-check-input:checked').val() == 'RV')
        {
            $(this).closest('.rvchq').remove();
            $('.rvchqtype').selectpicker('destroy');
            $('.rvbank ').selectpicker('destroy');
            $(".rvchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".rvchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".rvchqno").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'rvchq'+(i+1));
            });
            $(".rvchqtype").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'rvchqtype'+(i+1));
                $('.rvchqtype').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".rvchqdate").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'rvchqdate'+(i+1));
            });
            $(".rvchqam").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'rvchqam'+(i+1));
            });
            $(".rvpt").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'rvpt'+(i+1));
            });
            $(".rvbank").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'rvbank'+(i+1));
                $('.rvbank').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
        }
        if($('.form-check-input:checked').val() == 'VPV')
        {
            $(this).closest('.vpvchq').remove();
            $('.vpvchqtype').selectpicker('destroy');
            $('.vpvbank ').selectpicker('destroy');
            $(".vpvchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".vpvchq").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".vpvchqno").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'vpvchq'+(i+1));
            });
            $(".vpvchqtype").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'vpvchqtype'+(i+1));
                $('.vpvchqtype').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".vpvchqdate").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'vpvchqdate'+(i+1));
            });
            $(".vpvchqam").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'vpvchqam'+(i+1));
            });
            $(".vpvpt").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'vpvpt'+(i+1));
            });
            $(".vpvbank").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'vpvbank'+(i+1));
                $('.vpvbank').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
        }


    });

    $(document).on('click','.delete',function(){

        if($('.form-check-input:checked').val() == 'DV')
        {
            $(this).closest('.dvdebit').remove();
            $('.head_ddv').selectpicker('destroy');
            $('.addv').selectpicker('destroy');

            $(".dvdebit").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".head_ddv").each(function (i){
                $(this).removeAttr('data-id');
                $(this).attr('data-id', 'dv'+(i+1));
                $('.head_ddv').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".addv").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'account_code_ddv-'+(i+1));
                $('.addv').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".account_name_ddv").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'account_name_ddv'+(i+1));
            });
            $(".amountdv").each(function (i){
                $(this).removeClass('amountdv'+(i+2));
                $(this).addClass('amountdv'+(i+1));
            });
            $(".dltdv").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'dv'+(i+2));
            });
            var amount=0;
            var id = $('#debitdv').children().last().attr('id');
            var count = +id + +1;
            for (let i = 1; i < count; i++) {
                if($('.amountdv'+i).val() != '')
                {
                    amount = +amount + +$('.amountdv'+i).val();
                }
                $('#Amountdv').val(amount);
            }
        }
        if($('.form-check-input:checked').val() == 'EV')
        {
            $(this).closest('.evdebit').remove();
            $('.head_dev').selectpicker('destroy');
            $('.adev').selectpicker('destroy');

            $(".evdebit").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', (i+1));
            });
            $(".head_dev").each(function (i){
                $(this).removeAttr('data-id');
                $(this).attr('data-id', 'ev'+(i+1));
                $('.head_dev').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".adev").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'account_code_dev-'+(i+1));
                $('.adev').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            });
            $(".account_name_dev").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'account_name_dev'+(i+1));
            });
            $(".amountev").each(function (i){
                $(this).removeClass('amountev'+(i+2));
                $(this).addClass('amountev'+(i+1));
            });
            $(".dltev").each(function (i){
                $(this).removeAttr('id');
                $(this).attr('id', 'ev'+(i+2));
            });
            var amount=0;
            var id = $('#debitev').children().last().attr('id');
            var count = +id + +1;
            for (let i = 1; i < count; i++) {
                if($('.amountev'+i).val() != '')
                {
                    amount = +amount + +$('.amountev'+i).val();
                }
                $('#Amountev').val(amount);
            }
        }
    });
</script>
@endsection
@endsection
