<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
            width: 88%;
            border-collapse: collapse;
            border: 1px solid #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            margin-left: 100px;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
            margin-top: 20px;
            border: 4px solid #000000 !important;
            display: block;
            margin-left: 100px;
            margin-right: 100px;
        }
        .label
        {
            font-size: 30px;
            color: #28AE01;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
            border: 4px solid #000000 !important;
            display: block;
            margin-top: 20px;
            margin-left: 100px;
            margin-right: 100px;
            text-align: center;
        }
        .row
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            /* padding-left: 30px; */
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A4;
        }
        body
        {
            /* margin-left: 500px; */
            /* margin-right: 500px; */
            /* text-align: center; */
        }
  </style>
  <body >
        <header>
            <img src="{{url('')}}/uploads/posch.png" height="100px"  width="500px" style="margin-top:10px; margin-left:50px" alt="logo" class="logo-default"  />
        </header>
        <br>
    <div class="label">Journal Voucher</div>


    <div class="form-body">
        <div class="row">
            <div>
                <div style="margin-left: 100px" class="form-outline">
                    <p><b>Date: {{\Carbon\Carbon::parse($exp->date)->format('d-m-Y')}}</b></p>
                    <p><b>Voucher No: {{$exp->v_no ?? $exp->id}}</b></p>
                    <p><b>Voucher Type: {{$exp->v_type ?? ''}}</b></p>
                    <p><b>Prepared By: {{$exp->preparedUser->name}} </b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="attendance-table" style="margin-top: 25px">
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th colspan="5" class="attendance-cell">Payee:</th>
                </tr>
                <tr>
                    <th style="text-align: center"  class="attendance-cell">S.no</th>
                    <th style="text-align: center"  class="attendance-cell">Account Name</th>
                    <th style="text-align: center"  class="attendance-cell">Debit</th>
                    <th style="text-align: center"  class="attendance-cell">Credit</th>
                    <th style="text-align: center"  class="attendance-cell">Description</th>
                </tr>
            </thead>
            <tbody>
                @if (count($others) == 0)
                    <tr>
                        <td class="attendance-cell" style="text-align: center">
                            1
                        </td>
                        <td class="attendance-cell" style="text-align: center">
                            {{$exp->acode1->name_of_account}}
                        </td>
                        <td class="attendance-cell" style="text-align: center">
                            {{$exp->description}}
                        </td>
                        <td class="attendance-cell"  style="text-align: center">
                            {{$exp->amount}}
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="attendance-cell" style="text-align: center">
                            2
                        </td>
                        <td class="attendance-cell" style="text-align: center">
                            {{$exp->acode2->name_of_account}}
                        </td>
                        <td class="attendance-cell" style="text-align: center">
                            {{$exp->description}}
                        </td>
                        <td></td>
                        <td class="attendance-cell"  style="text-align: center">
                            {{$exp->amount}}
                        </td>
                    </tr>
                    <tr>
                        <td class="attendance-cell"></td>
                        <td class="attendance-cell"></td>
                        <td class="attendance-cell"></td>
                        <td class="attendance-cell"  style="text-align: center">TOTAL</td>
                        <td class="attendance-cell"  style="text-align: center">{{$exp->amount}}</td>
                    </tr>
                @else
                    @if ($exp->account1 == null)

                    @else

                    @endif
                @endif

            </tbody>
        </table>


    </div>
    <br>
    <br>
    <br>
    <div class="row">
        <hr color="black" style="width:20%;display: block;border-style: inset;border-width: 1px; margin-right: 530px">
        <div  style="margin-left: 120px" class="form-outline">
            <p> Accountant Sign</p>
        </div>
    </div>
    <div class="row" style="margin-top: -80px; margin-left: 320px">
        <hr color="black" style="width:35%;display: block;border-style: inset;border-width: 1px; margin-right: 450px">
        <div class="form-outline" style="margin-left: 20px">
            <p> Approval Sign</p>
        </div>
    </div>
    <div class="row" style="margin-top: -80px; margin-left: 540px">
        <hr color="black" style="width:65%;display: block;border-style: inset;border-width: 1px; margin-right: 400px">
        <div class="form-outline" style="margin-left: 20px">
            <p> Receiver Sign</p>
        </div>
    </div>

  </body>
</html>
