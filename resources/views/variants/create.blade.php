@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/variants">Variants</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>Add Variants</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-list-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Add Variants</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{route('variants.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-outline">
                                        <label for="">Parent Variant*</label>
                                        <select class="form-control selectpicker" style="overflow-y: auto" data-live-search="true" required name="p_id" id="p_id" >
                                            <option value=""  selected disabled>Select...</option>
                                            <option value="0">No Parent</option>
                                            @foreach ($variant as $m)
                                                @if ($m->p_id == 0)
                                                    <option value="{{$m->id}}">{{$m->name}}</option>
                                                @else
                                                    <option value="{{$m->id}}">&emsp;{{$m->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Name*</label>
                                        <input value="{{ old('name')}}" class="form-control" type="text" placeholder="Enter Variant Name" name="name" required>
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
