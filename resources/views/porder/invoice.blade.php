<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 65px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A4;
        }
  </style>
  <body>
      <header>
          PURCHASE PAYMENT INVOICE
    </header>
    <div class="label col-sm-12">
        <label><b>Reference#: </b>{{$th->purchase->ref_no}}</label>
        <br>
        @php
            $date=Carbon\Carbon::parse($th->purchase->order_date)->format('d-m-y');
            $date1=Carbon\Carbon::parse($th->created_at)->format('d-m-y');
        @endphp
        <label><b>Order Date: </b>{{$date}}</label>
        <br>
        <label><b>Order#: </b>{{$th->purchase->id}}</label>
        {{-- <br>
        <label><b>Invoice#: </b>{{$th->id}}</label>
        <br>
        <label><b>Transaction Date: </b>{{$date1}}</label> --}}

    </div>
    <div class="form-body">

        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Vendor:</b></label>
                </div>
            </div>
            <div style="margin-top: -18px; margin-left: 480px">
                <div class="form-outline">
                    <label for=""><b>Ship To:</b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline">
                    <p><b>Name: {{ucwords($th->purchase->supplier->name)}}</b></p>
                    <p><b>Company: {{ucwords($th->purchase->supplier->company)}}</b></p>
                    <p><b>Contact No: {{$th->purchase->supplier->c_no}}</b></p>
                    <p><b>Address: {{ucwords($th->purchase->supplier->address)}}</b></p>
                </div>
            </div>
            <div style="margin-top: -150px; margin-left: 480px">
                <div class="form-outline">
                    @if ($th->purchase->biller == null)
                        <p><b>Name: Posch Care</b></p>
                        <br>
                        <br>
                        <br>
                        <br>
                    @else
                        <p><b>Name: {{ucwords($th->purchase->biller->name)}}</b></p>
                        <p><b>Warehouse Name: {{ucwords($th->purchase->warehouse->w_name)}}</b></p>
                        <p><b>Address: {{ucwords($th->purchase->warehouse->w_address)}}</b></p>

                    @endif
                </div>
            </div>
        </div>
    </div>
        <div class="attendance-table" style="margin-top: 20px">
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th class="attendance-cell"  width="5%">Invoice No</th>
                    <th class="attendance-cell"  width="30%">Transaction Date</th>
                    <th class="attendance-cell"  width="10%">Paid by</th>
                    <th class="attendance-cell"  width="15%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="attendance-cell" >{{$th->id}}</td>
                    <td class="attendance-cell" >{{$date1}}</td>
                    <td class="attendance-cell" >{{$th->paid_by}}</td>
                    <td class="attendance-cell" >{{$th->total}}</td>
                </tr>
            </tbody>

        </table>

    </div>
  </body>
</html>
