@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        /* background-color:#F0F0F0; */
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }

</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }

    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css

<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="{{url('')}}/purchase">Purchase Order</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-shopping-cart font-white"></i>View Purchase Orders
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                                @if(in_array('excel',$permissions))
                                                    @if ($index == 0)
                                                        <a style="margin-left:-20px; "  href="{{route('purchase.excel')}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 1)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/purchase/search/{{$year}}/{{$s_id}}/{{$status}}/{{$check}}/excel/year">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 2)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/purchase/search/{{$month}}/{{$s_id}}/{{$status}}/{{$check}}/excel/month">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 3)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/purchase/search/{{$from}}/{{$to}}/{{$s_id}}/{{$status}}/{{$check}}/excel/dates">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 4)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/purchase/search/{{$date}}/{{$s_id}}/{{$status}}/{{$check}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 5)
                                                        <a style="margin-left:-20px; "  href="{{url('')}}/purchase/search/{{$s_id}}/{{$status}}/{{$check}}/excel/ostatus">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 6 || $index == 7 || $index == 8 || $index == 9)
                                                    <a style="margin-left:-20px; "  href="{{url('')}}/purchase/search/{{$index}}/{{$s_id}}/{{$status}}/{{$check}}/excel">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif


                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('purchase.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px;  margin-top:-20px " type="button"  class="btn btn-block btn-primary btn-md ">Add Purchase Order</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        @if (in_array('importExcel',$permissions))
                                            <div class="tableview">
                                                <a href="{{route('purchase.importStructure')}}" >
                                                    <button style="background: #000000; margin-left:20px; margin-top:-2px" type="button"  class="btn btn-primary btn-sm ">
                                                    <i class='fa fa-download'></i>
                                                    Download Structure</button>
                                                </a>
                                                <form id="Import_product" action="{{route('import.purchaseOrder')}}" method="post" enctype="multipart/form-data" id="advanceSearch">
                                                    <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="col-md-4">
                                                            <input type="file" name="csv_file" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <button style="background: #000000; margin-left:-90px; margin-top:-2px" type="submit"  class="btn btn-primary btn-sm ">Import Csv</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/purchase/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" name="menuid">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Order Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Order Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="last24Hours">Last 24 Hours
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio5" name="optradio" value="lastweek">Last Week
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio6" name="optradio" value="last15Days">Last 15 Days
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio7" name="optradio" value="lastMonth">Last Month
                                                        </label>
                                                    </div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Supplier Name</label>
                                                            <select class="form-control" data-live-search="true" name="s_id" id="s_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($supplier as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Order Status</label>
                                                            <select name="status" id="status" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                                <option>Partial</option>
                                                                <option>Received</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-8"></div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            {{-- <label for="">Unit Name</label> --}}
                                                            {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                            <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered cs"  style="width:1700px;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:200px;">Purchase Order No</th>
                                                            <th style="width:150px;">Purchase Date</th>
                                                            <th style="width:120px;">Reference #</th>
                                                            <th style="width:150px;">Warehouse</th>
                                                            <th style="width:130px;">Supplier</th>
                                                            <th style="width:180px;">Total Purchase Amount</th>
                                                            <th style="width:120px;">Paid</th>
                                                            <th style="width:120px;">Balance</th>
                                                            <th style="width:200px;">Total Purchase Quantity</th>
                                                            <th style="width:110px;">Order Status</th>
                                                            <th style="width:120px;">Payment Status</th>
                                                            <th style="width:110px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $total = 0 ;
                                                            $totalPaid = 0;
                                                            $totalBalance = 0;
                                                            $totalAmount = 0;
                                                            $totalQty = 0;
                                                        @endphp
                                                        @foreach ($porder as $p)
                                                            <tr>
                                                                <td>
                                                                    {{$p->id}}
                                                                </td>
                                                                <td>
                                                                    {{$p->order_date}}
                                                                </td>
                                                                <td>
                                                                    {{$p->ref_no}}
                                                                </td>
                                                                <td>
                                                                    {{$p->warehouse->w_name}}
                                                                </td>
                                                                <td>
                                                                    {{$p->supplier->name}}
                                                                </td>
                                                                <td>
                                                                    {{$p->total}}
                                                                </td>
                                                                <td>
                                                                    {{$p->total_amount == 0 ? '0' : $p->total_amount}}
                                                                </td>
                                                                <td>
                                                                    <input  width="10"  type="text" class="form-control" readonly id="amount" value="{{$p->total - $p->total_amount}}">
                                                                </td>
                                                                <td>
                                                                    @foreach ($p->orderdetails as $d)
                                                                        @php
                                                                            $total += $d->quantity;
                                                                            $totalQty+= $total;
                                                                        @endphp
                                                                    @endforeach
                                                                    {{$total}}
                                                                </td>
                                                                    @php
                                                                        $total = 0;
                                                                    @endphp
                                                                @if($p->status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red btn_style" id="{{$p->id}}">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->status=='Cancel')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red btn_style" id="{{$p->id}}">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  yellow btn_style" id="{{$p->id}}">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  blue btn_style" id="{{$p->id}}">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->status=='Received')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green btn_style" id="{{$p->id}}">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->p_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red btn_style" id="{{$p->id}}">
                                                                            {{$p->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->p_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  blue btn_style" id="{{$p->id}}">
                                                                            {{$p->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($p->p_status=='Paid')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green btn_style" id="{{$p->id}}">
                                                                            {{$p->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    @if($p->status == 'Partial' && ($p->p_status == 'Paid' ))
                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('Receive Order',$permissions))
                                                                                <option>Receive Order</option>
                                                                            @endif
                                                                            @if ($adminName != 'Danish Messani')
                                                                                @if(in_array('Edit purchase ledger',$permissions))
                                                                                    <option >Edit Invoice</option>
                                                                                @endif
                                                                            @endif
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Order Details</option>
                                                                            @endif
                                                                            @if(in_array('report',$permissions))
                                                                                <option >Purchase Order</option>
                                                                            @endif
                                                                            @if(in_array('invoice',$permissions))
                                                                                <option >Purchase Invoice</option>
                                                                            @endif
                                                                            @if(in_array('View Payment',$permissions))
                                                                                <option >View Payment</option>
                                                                            @endif
                                                                            @if(in_array("View Grn's",$permissions))
                                                                                <option >View GRN's</option>
                                                                            @endif
                                                                        </select>
                                                                    @else
                                                                        @if($p->status == 'Received' && $p->p_status == 'Paid')
                                                                            <select class="form-control action" id="{{$p->id}}" >
                                                                                <option >Actions</option>
                                                                                @if(in_array('show',$permissions))
                                                                                    <option >Order Details</option>
                                                                                @endif
                                                                                @if ($adminName != 'Danish Messani')
                                                                                    @if(in_array('Edit purchase ledger',$permissions))
                                                                                        <option >Edit Invoice</option>
                                                                                    @endif
                                                                                @endif
                                                                                @if(in_array('report',$permissions))
                                                                                    <option >Purchase Order</option>
                                                                                @endif
                                                                                @if(in_array('View Payment',$permissions))
                                                                                    <option >View Payment</option>
                                                                                @endif
                                                                                @if(in_array('invoice',$permissions))
                                                                                    <option >Purchase Invoice</option>
                                                                                @endif
                                                                                @if(in_array("View Grn's",$permissions))
                                                                                    <option >View GRN's</option>
                                                                                @endif
                                                                            </select>
                                                                        @else
                                                                            @if($p->status == 'Partial' && ($p->p_status == 'Partial' || $p->p_status == 'Pending' ))
                                                                                <select class="form-control action" id="{{$p->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('Receive Order',$permissions))
                                                                                        <option>Receive Order</option>
                                                                                    @endif
                                                                                    @if(in_array('Add Payment',$permissions))
                                                                                        <option>Add Payment</option>
                                                                                    @endif
                                                                                    @if ($adminName != 'Danish Messani')
                                                                                        @if(in_array('Edit purchase ledger',$permissions))
                                                                                            <option >Edit Invoice</option>
                                                                                        @endif
                                                                                    @endif
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Order Details</option>
                                                                                    @endif
                                                                                    @if(in_array('report',$permissions))
                                                                                        <option >Purchase Order</option>
                                                                                    @endif
                                                                                    @if(in_array('View Payment',$permissions))
                                                                                        <option >View Payment</option>
                                                                                    @endif
                                                                                    @if(in_array('invoice',$permissions))
                                                                                        <option >Purchase Invoice</option>
                                                                                    @endif
                                                                                    @if(in_array("View Grn's",$permissions))
                                                                                        <option >View GRN's</option>
                                                                                    @endif
                                                                                </select>
                                                                            @else
                                                                                @if($p->status == 'Received' && ($p->p_status == 'Partial' || $p->p_status == 'Pending'))
                                                                                    <select class="form-control action" id="{{$p->id}}" >
                                                                                        <option >Actions</option>
                                                                                        @if(in_array('Add Payment',$permissions))
                                                                                            <option>Add Payment</option>
                                                                                        @endif
                                                                                        @if ($adminName != 'Danish Messani')
                                                                                            @if(in_array('Edit purchase ledger',$permissions))
                                                                                                <option >Edit Invoice</option>
                                                                                            @endif
                                                                                        @endif
                                                                                        @if(in_array('show',$permissions))
                                                                                            <option >Order Details</option>
                                                                                        @endif
                                                                                        @if(in_array('report',$permissions))
                                                                                            <option >Purchase Order</option>
                                                                                        @endif
                                                                                        @if(in_array('View Payment',$permissions))
                                                                                            <option >View Payment</option>
                                                                                        @endif
                                                                                        @if(in_array('invoice',$permissions))
                                                                                            <option >Purchase Invoice</option>
                                                                                        @endif
                                                                                        @if(in_array("View Grn's",$permissions))
                                                                                            <option >View GRN's</option>
                                                                                        @endif
                                                                                    </select>
                                                                                @else
                                                                                    @if ($p->status == 'Approved' && $p->p_status == 'Partial' )
                                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                                            <option >Actions</option>
                                                                                            @if(in_array('Receive Order',$permissions))
                                                                                                <option>Receive Order</option>
                                                                                            @endif
                                                                                            @if(in_array('show',$permissions))
                                                                                                <option >Order Details</option>
                                                                                            @endif
                                                                                            @if(in_array('report',$permissions))
                                                                                                <option >Purchase Order</option>
                                                                                            @endif
                                                                                            @if(in_array('invoice',$permissions))
                                                                                                <option >Purchase Invoice</option>
                                                                                            @endif

                                                                                        </select>

                                                                                    @else

                                                                                        @if ($p->status == 'Approved' && $p->p_status == 'Pending')
                                                                                            <select class="form-control action" id="{{$p->id}}" >
                                                                                                <option >Actions</option>
                                                                                                @if(in_array('Receive Order',$permissions))
                                                                                                    <option>Receive Order</option>
                                                                                                @endif
                                                                                                @if ($adminName != 'Danish Messani')
                                                                                                    @if(in_array('edit',$permissions))
                                                                                                        <option >Edit Order</option>
                                                                                                    @endif
                                                                                                @endif
                                                                                                @if(in_array('show',$permissions))
                                                                                                    <option >Order Details</option>
                                                                                                @endif
                                                                                                @if(in_array('report',$permissions))
                                                                                                    <option >Purchase Order</option>
                                                                                                @endif
                                                                                                @if(in_array('invoice',$permissions))
                                                                                                    <option >Purchase Invoice</option>
                                                                                                @endif
                                                                                                @if(in_array('status',$permissions))
                                                                                                    <option>Cancel</option>
                                                                                                @endif
                                                                                            </select>
                                                                                        @else
                                                                                            @if ($p->status == 'Cancel')
                                                                                                <select class="form-control action" id="{{$p->id}}" >
                                                                                                    <option >Actions</option>

                                                                                                    @if(in_array('show',$permissions))
                                                                                                        <option >Order Details</option>
                                                                                                    @endif
                                                                                                    @if(in_array('report',$permissions))
                                                                                                        <option >Purchase Order</option>
                                                                                                    @endif
                                                                                                </select>
                                                                                            @else
                                                                                                <select class="form-control action" id="{{$p->id}}" >
                                                                                                    <option >Actions</option>
                                                                                                    @if(in_array('edit',$permissions))
                                                                                                        <option >Edit Order</option>
                                                                                                    @endif
                                                                                                    @if(in_array('status',$permissions))
                                                                                                        <option>Approved</option>
                                                                                                        <option>Cancel</option>
                                                                                                    @endif
                                                                                                    @if(in_array('show',$permissions))
                                                                                                        <option >Order Details</option>
                                                                                                    @endif
                                                                                                    @if(in_array('report',$permissions))
                                                                                                        <option >Purchase Order</option>
                                                                                                    @endif
                                                                                                    @if(in_array('invoice',$permissions))
                                                                                                        <option >Purchase Invoice</option>
                                                                                                    @endif
                                                                                                </select>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $totalAmount += $p->total;
                                                                $totalPaid += $p->total_amount == 0 ? '0' : $p->total_amount;
                                                                $totalBalance += $p->total - $p->total_amount;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$totalAmount}}</td>
                                                            <td>{{$totalPaid}}</td>
                                                            <td>{{$totalBalance}}</td>
                                                            <td>{{$totalQty}}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        @include('modals.paymentPurchase')
        @include('modals.transactionPurchase')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Purchase Order</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Order Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="order_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Purchase Order No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="o_no" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reference No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="ref_no" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Supplier</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="s_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Biller</label>
                                    <input class="form-control" type="text" placeholder="Biller Name" id="b_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row docx" hidden>
                            <div class="col-sm-5">
                                <input type="hidden" name="doc1" id="doc1">
                                <label style="margin-top: 10px" ><b> Document1 </b></label>
                            </div>
                            <div class="col-sm-1">
                                <a href="" class='btn green docs' style="margin-top: 10px" ><i class='fa fa-download'></i></a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="35%">Code</th>
                                            <th width="35%">Name</th>
                                            <th width="35%">Brand Name</th>
                                            <th width="7%">Cost</th>
                                            <th width="5%">Quantity</th>
                                            <th width="5%">Received Quantity</th>
                                            <th width="10%">Sub total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->

        <div id="GRN" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">View GRN History</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Order Number</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="OrderNo" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="example5" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th>Receiving Date</th>
                                        <th>GRN No</th>
                                        <th>Received Product</th>
                                        <th>Cost</th>
                                        <th>Quantity</th>
                                        <th>Sub Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>

                            </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <div id="myModal1" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg" style="width: 70%;">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Goods Receiving Note</h4>
                    </div>
                    <form action="{{url('')}}/purchase/updateQuantity" class="form-horizontal" method="POST" >
                        @csrf
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                <table id="example3" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Name - Code</th>
                                            <th>Cost</th>
                                            <th>Unit</th>
                                            <th>Quantity</th>
                                            <th>Total Received</th>
                                            <th>Received Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                                <input type="hidden" name="s_id" id="s_id1">
                                <input type="hidden" name="pu_id" id="pu_id1">
                                <input type="hidden" name="p_s_id" id="p_s_id1">
                                <input type="hidden" name="p_type" id="p_type1">
                                <input type="hidden" name="t_type" id="t_type1">
                                <input type="hidden" name="w_id" id="w_id1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label >Total Amount</label>
                                <input class="form-control" type="text" min="0" name="actual" readonly id="actual1">
                            </div>
                        </div>
                        <br>
                        <button type="submit" id="received" class="btn green">Submit</button>
                    </div>
                </div>

                    {{-- <div class="row">
                        <div class="col-md-offset-0 col-md-12">
                        </div>
                    </div> --}}
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->

    @endsection
    @section('custom-script')
        @toastr_js
        @toastr_render
        <script>
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $("#example1 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/purchase/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#order_date').val(data[0].order_date);
                        $('#ref_no').val(data[0].ref_no);
                        $('#w_name').val(data[0].warehouse.w_name);
                        $('#s_name').val(data[0].supplier.name);
                        for (let i = 0; i < data[1].length; i++) {
                            $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].products.pro_name+" - "+data[1][i].products.pro_code+"</td><td>"+data[1][i].products.cost+"</td><td>"+data[1][i].quantity+"</td><td>"+data[1][i].sub_total+"</td></tr>");

                        }
                    }
                });
            });
        </script>
        <script>
            CKEDITOR.replace( 'editor2' );
            CKEDITOR.replace( 'editor3' );
            $(document).on('click','#add',function(){
                $('#addpayment').show();
                $('#paid_by2').prop('disabled',true);
                $('#add').html('Hide');
                $('#add').attr('id','hide');
            });

            $(document).on('click','#hide',function(){
                $('#addpayment').hide();
                $('#paid_by2').prop('disabled',false);
                $('#hide').html('Add Payment');
                $('#hide').attr('id','add');
            });
       </script>
<script type="text/javascript">
    $(document).ready(function () {

        $(document).ready(function () {
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                scrollX: true,
            });
            $('input:radio[name="optradio"]').change(function(){
                if ($(this).is(':checked')) {
                    $('#search').prop('disabled',false);
                    var val = $(this).val();
                    if(val == 'Year')
                    {
                        $('#year').prop('disabled',false);
                        $('#s_id').prop('disabled',false);
                        $('#year').attr('required',true);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Month')
                    {
                        $('#month').prop('disabled',false);
                        $('#s_id').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Date')
                    {
                        $('#from').prop('disabled',false);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',false);
                        $('#to').attr('required',false);
                        $('#s_id').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#month').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Status')
                    {
                        $('#month').prop('disabled',true);
                        $('#s_id').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                        $('#status').attr('required',true);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'last24Hours')
                    {
                        $('#year').prop('disabled',true);
                        $('#s_id').prop('disabled',false);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'lastweek')
                    {
                        $('#year').prop('disabled',true);
                        $('#s_id').prop('disabled',false);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'last15Days')
                    {
                        $('#year').prop('disabled',true);
                        $('#s_id').prop('disabled',false);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'lastMonth')
                    {
                        $('#year').prop('disabled',true);
                        $('#s_id').prop('disabled',false);
                        $('#year').attr('required',false);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#status').attr('required',false);
                        $('#status').attr('disabled',false);
                        $('#s_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });

        });

        $(document).on('change','#paid_by1',function(){
            var p=$(this).val();
            console.log(p);
            if(p == 'Cheque')
            {
                $('#cheque1').show();
                $("#cheque_no1").prop('required',true);
                $('#gift1').hide();
                $("#gift_no1").prop('required',false);
                $('#credit1').hide();
                $("#cc_no1").prop('required',false);
                $("#cc_holder1").prop('required',false);
            }
            if(p == 'Gift')
            {
                $('#gift1').show();
                $("#gift_no1").prop('required',true);
                $('#cheque1').hide();
                $("#cheque_no1").prop('required',false);
                $('#credit1').hide();
                $("#cc_no1").prop('required',false);
                $("#cc_holder1").prop('required',false);
            }
            if(p == 'Cash')
            {
                $('#gift1').hide();
                $("#gift_no1").prop('required',false);
                $('#cheque1').hide();
                $("#cheque_no1").prop('required',false);
                $('#credit1').hide();
                $("#cc_no1").prop('required',false);
                $("#cc_holder1").prop('required',false);
            }
            if(p == 'Credit')
            {
                $('#gift1').hide();
                $("#gift_no1").prop('required',false);
                $('#cheque1').hide();
                $("#cheque_no1").prop('required',false);
                $('#credit1').show();
                $("#cc_no1").prop('required',true);
                $("#cc_holder1").prop('required',true);
            }
        });
        $(document).on('change','#paid_by',function(){
            var p=$(this).val();
            if(p == 'Cheque')
            {
                $('#cheque').show();
                $("#cheque_no").prop('required',true);
                $('#gift').hide();
                $("#gift_no").prop('required',false);
                $('#credit').hide();
                $("#cc_no").prop('required',false);
                $("#cc_holder").prop('required',false);
            }
            if(p == 'Gift')
            {
                $('#gift').show();
                $("#gift_no").prop('required',true);
                $('#cheque').hide();
                $("#cheque_no").prop('required',false);
                $('#credit').hide();
                $("#cc_no").prop('required',false);
                $("#cc_holder").prop('required',false);
            }
            if(p == 'Cash')
            {
                $('#gift').hide();
                $("#gift_no").prop('required',false);
                $('#cheque').hide();
                $("#cheque_no").prop('required',false);
                $('#credit').hide();
                $("#cc_no").prop('required',false);
                $("#cc_holder").prop('required',false);
            }
            if(p == 'Credit')
            {
                $('#gift').hide();
                $("#gift_no").prop('required',false);
                $('#cheque').hide();
                $("#cheque_no").prop('required',false);
                $('#credit').show();
                $("#cc_no").prop('required',true);
                $("#cc_holder").prop('required',true);
            }
        });
        $(document).on('change','.action',function(){
            var val=$(this).val();
            if(val == 'Receive Order')
            {
                var id=$(this).attr("id");
                // var rowCount = $('#example3 tr').length;
                var amount = $(this).closest("tr")   // Finds the closest row <tr>
                .find("#amount").val();
                $('#p_total1').val('');
                $('#actual1').val('');
                $('#p_s_id').val('');
                $('#p_ref_no').val('');
                $('#w_id').val('');
                $('#myModal1').find("textarea,select").val('').end();
                $('#gift1').hide();
                $('#cheque1').hide();
                $('#credit1').hide();
                $("#example3 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/purchase/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        var a =2;
                        for (let i = 0; i < data[1].length; i++) {
                             max= data[1][i].quantity - data[1][i].received_quantity;
                            if(data[1][i].quantity == data[1][i].received_quantity)
                            {

                            }
                            else
                            {
                                if(data[1][i].type == 0)
                                {
                                    if(data[1][i].products.unit.u_name == 'Liter' || data[1][i].products.unit.u_name == 'Kilograms' || data[1][i].products.unit.u_name == 'Mililiter' || data[1][i].products.unit.u_name == 'Grams' || data[1][i].products.unit.u_name == 'Kilograms')
                                    {
                                        $("#example3").append(`<tr><td><input type='hidden' name='unit_quantity[]' value='`+data[1][i].products.weight+`'> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='`+data[1][i].id+`'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='`+data[1][i].p_id+`'></td><td>`+data[1][i].products.pro_name+` - `+data[1][i].products.pro_code+`</td><td><input type='text' style='background-color: transparent;border: 0px solid;' id='cost' class='cost`+a+`' readonly name='cost[]' value='`+data[1][i].cost+`'></td><td>`+data[1][i].products.unit.u_name+`<input type='hidden' name='type[]' value='`+data[1][i].type+`'></td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q`+a+`' readonly name='quantity[]' value='`+data[1][i].quantity+`'></td><td>`+data[1][i].received_quantity+`</td><td><input type='number' max='`+max+`' value='1' min='0' required class='form-control rcv quantity`+a+`' name='received_quantity[]'><input type='hidden' name='hasUnit'><button id='`+data[1][i].id+`' data-id="`+a+`" type='button' class='btn multiple'>Add Receiving</button></td></tr>`);
                                    }
                                    else
                                    {
                                        $("#example3").append("<tr><td><input type='hidden' name='unit_quantity[]' value='"+data[1][i].products.weight+"'> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].products.pro_name+" - "+data[1][i].products.pro_code+"</td><td><input type='text' style='background-color: transparent;border: 0px solid;' id='cost' class='cost"+a+"' readonly name='cost[]' value='"+data[1][i].cost+"'></td><td>"+data[1][i].products.unit.u_name+"<input type='hidden' name='type[]' value='"+data[1][i].type+"'></td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td>"+data[1][i].received_quantity+"</td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='received_quantity[]'></td></tr>");
                                    }
                                }
                                else
                                {
                                    if(data[1][i].variant.product.unit.u_name == 'Liter' || data[1][i].variant.product.unit.u_name == 'Kilograms' || data[1][i].variant.product.unit.u_name == 'Mililiter' || data[1][i].variant.product.unit.u_name == 'Grams' || data[1][i].variant.product.unit.u_name == 'Kilograms')
                                    {
                                        $("#example3").append("<tr><td><input type='hidden' name='unit_quantity[]' value='"+data[1][i].products.weight+"'> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].variant.name+"</td><td><input type='text' style='background-color: transparent;border: 0px solid;' id='cost' class='cost"+a+"' readonly name='cost[]' value='"+data[1][i].cost+"'></td><td><input type='hidden' name='type[]' value='"+data[1][i].type+"'>-</td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td>"+data[1][i].received_quantity+"</td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='received_quantity[]'><input type='hidden' name='hasUnit'><button id='"+data[1][i].id+"' data-id='"+a+"' type='button' class='btn multiple'>Add Receiving</button></td></tr>");
                                    }
                                    else
                                    {
                                        $("#example3").append("<tr><td><input type='hidden' name='unit_quantity[]' value='"+data[1][i].products.weight+"'> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].variant.name+"</td><td><input type='text' style='background-color: transparent;border: 0px solid;' id='cost' class='cost"+a+"' readonly name='cost[]' value='"+data[1][i].cost+"'></td><td><input type='hidden' name='type[]' value='"+data[1][i].type+"'>-</td><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td>"+data[1][i].received_quantity+"</td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantity"+a+"' name='received_quantity[]'></td></tr>");
                                    }
                                }
                            }
                            a++;
                        }
                        // console.log(a);
                        $('#s_id1').val(data[0].s_id);
                        $('#w_id1').val(data[0].w_id);
                        $('#pu_id1').val(id);

                        // console.log(amount);
                        $('#actual1').val(amount);
                        $('#p_s_id1').val(id);
                        $('#p_type1').val('Purchase');
                        $('#t_type1').val('Sent');
                        $('#myModal1').modal("show");
                        var total =0;
                        var total1 =0;
                        var rowCount1 = $('#example3 tr').length;
                        for (let i = 2; i <= rowCount1; i++) {
                            total =+total + +Number(parseFloat($('.cost'+i).val())) * Number(parseFloat($('.quantity'+i).val()));
                            // total1 =+total1 + +Number(parseFloat($('.cost'+i).val())) * Number(parseFloat($('.q'+i).val()));
                            total1 = +total1 + +Number(parseFloat($('.cost'+i).val())) * Number(parseFloat($('.quantity'+i).val()));
                        }
                        $('#p_total1').attr('max',total1);
                        $('#p_total1').val(total);
                        $('#purchase').val(total);
                        $('.action').val('Actions');
                        $('#example3').DataTable();

                    }
                });
            }
            if(val == 'Add Payment')
            {
                $('#p_total').val('');
                $('#actual').val('');
                $('#p_s_id').val('');
                $('#p_ref_no').val('');

                $('#paymentPurchase').find("textarea,select").val('').end();
                $('#gift').hide();
                $('#cheque').hide();
                $('#credit').hide();
                var amount = $(this).closest("tr")   // Finds the closest row <tr>
                .find("#amount").val();
                var id=$(this).attr('id');
                $('#actual').val(amount);
                $('#p_s_id').val(id);
                $('#p_type').val('Purchase');
                $('#t_type').val('Sent');
                $('#p_total').attr('max',amount);
                $('#paymentPurchase').modal("show");
                $('.action').val('Actions');
            }
            if(val == 'Edit Order')
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/purchase/'+id+'/edit';
                // $('.action').val('Actions');
            }
            if(val == 'Edit Invoice')
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/purchase/editInvoice/'+id;
                // $('.action').val('Actions');
            }
            if(val == 'Purchase Order')
            {
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/purchase/pdf/"+id,
                    method:"GET",
                    data:
                    {
                        id:id,
                    },
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        window.location.href= '{{url('')}}/purchase/pdf/'+id;
                        // location.reload();
                        $('.action').val('Actions');
                    }
                });
            }
            if(val == 'Order Details')
            {
                var id=$(this).attr("id");
                $("#example1 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/purchase/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        $('#order_date').val(data[0].order_date);
                        $('#ref_no').val(data[0].ref_no);
                        $('#o_no').val(data[0].id);
                        $('#w_name').val(data[0].warehouse.w_name);
                        $('#s_name').val(data[0].supplier.name);
                        if(data[0].doc != null)
                        {
                            $('.docx').show();
                            $('#doc1').val(data[0].doc);
                            $('.docs').attr('href','{{url('')}}/purchase/document/'+data[0].id);
                        }
                        if(data[0].biller == null)
                        {

                        }
                        else
                        {
                            $('#b_name').val(data[0].biller.name);
                        }
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].variant == null)
                            {
                                $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].products.pro_code+`</td><td> `+data[1][i].products.pro_name+` </td><td>`+data[1][i].products.brands.b_name+`</td><td>`+data[1][i].cost+`</td><td>`+data[1][i].quantity+`</td><td>`+data[1][i].received_quantity+`</td><td>`+data[1][i].sub_total+`</td></tr>`);
                            }
                            else
                            {
                                var name = data[1][i].variant.name.split('-');
                                $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+name[0]+`</td><td>`+name[1]+`</td><td>`+data[1][i].variant.product.brands.b_name+`</td><td>`+data[1][i].cost+`</td><td>`+data[1][i].quantity+`</td><td>`+data[1][i].received_quantity+`</td><td>`+data[1][i].sub_total+`</td></tr>`);
                            }
                        }
                        $('#myModal').modal("show");
                        $('.action').val('Actions');
                        $('#example1').DataTable();

                    }
                });
            }
            if(val == 'View Payment')
            {
                var id=$(this).attr("id");
                $("#example2 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/transaction/view",
                    method:"GET",
                    data:
                    {
                        p_s_id:id,
                        p_type:'Purchase',
                    },
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        for (let i = 0; i < data.length; i++) {
                             date = moment(data[i].created_at).format('MM/DD/YYYY');
                            $("#example2").append("<tr><td>"+data[i].id+"</td><td>"+date+"</td><td>"+data[i].paid_by+"</td><td>"+data[i].total+"</td> @if(in_array('Download Invoice',$permissions)) <td> <button type='button' class='btn green invoice' id="+data[i].id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button'  disabled class='btn green invoice' id="+data[i].id+"><i class='fa fa-download'></i></button></td> @endif</tr>");
                        }
                        $('#transactionPurchase').modal("show");
                        $('.action').val('Actions');
                        $('#example2').DataTable();

                    }
                });
            }
            if(val == "View GRN's")
            {
                var id=$(this).attr("id");
                $("#example5 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/purchase/grn/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        // console.log(data);
                        var a = 1;
                        var total = 0;
                        // console.log(data[0].stock.length);
                        $('#OrderNo').val(data[0].o_id);
                        for (let i = 0; i < data.length; i++) {
                            if(data[i].stock == null)
                            {

                            }
                            else
                            {
                                for (let j = 0; j < data[i].stock.length; j++) {
                                    total = data[i].cost * data[i].stock[j].quantity;
                                    console.log(total);
                                    if(data[i].variant == null)
                                    {
                                        $("#example5").append("<tr><td>"+a+"</td><td>"+data[i].stock[j].stock_date+"</td><td>"+data[i].stock[j].grn_no+"</td><td>"+data[i].products.pro_name+"</td><td>"+data[i].cost+"</td><td>"+data[i].stock[j].quantity+"</td><td>"+total+"</td> @if(in_array('Download Grn',$permissions)) <td> <button type='button' class='btn green grn' data-grn="+data[i].stock[j].grn_no+" id="+data[i].o_id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button' disabled class='btn green grn' data-grn="+data[i].stock[j].grn_no+" id="+data[i].o_id+"><i class='fa fa-download'></i></button></td> @endif</tr>");
                                        a= +a + 1;
                                    }
                                    else
                                    {
                                        $("#example5").append("<tr><td>"+a+"</td><td>"+data[i].stock[j].stock_date+"</td><td>"+data[i].stock[j].grn_no+"</td><td>"+data[i].variant.name+"</td><td>"+data[i].cost+"</td><td>"+data[i].stock[j].quantity+"</td><td>"+total+"</td> @if(in_array('Download Grn',$permissions)) <td> <button type='button' class='btn green grn' data-grn="+data[i].stock[j].grn_no+" id="+data[i].o_id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button' disabled class='btn green grn' data-grn="+data[i].stock[j].grn_no+" id="+data[i].o_id+"><i class='fa fa-download'></i></button></td> @endif</tr>");
                                        a= +a + 1;
                                    }

                                    // total = 0;
                                }
                            }
                        }
                        $('#GRN').modal("show");
                        $('.action').val('Actions');
                        $('#example5').DataTable();
                    }
                });
            }
            if(val == 'Approved' || val == 'Cancel')
            {
                var id = $(this).attr('id');
                var status = val;
                axios
                .post('{{route("purchase.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                });
            }
            if(val == 'Purchase Invoice')
            {
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/purchase/order/invoice/"+id,
                    method:"GET",
                    data:
                    {
                        id:id,
                    },
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        window.location.href= '{{url('')}}/purchase/order/invoice/'+id;
                        $('.action').val('Actions');
                    }
                });
            }
        });

    });

    $(document).on('change','.rcv',function(){
        var total =0;
        var rowCount1 = $('#example3 tr').length;
        for (let i = 2; i <= rowCount1; i++) {
            total =+total + +Number(parseFloat($('.cost'+i).val())) * Number(parseFloat($('.quantity'+i).val()));
        }
        console.log(total);
        $('#p_total1').val(total);
        $('#purchase').val(total);
        $('#p_total1').attr('max',total);
    });

    $(document).on('click','.invoice',function(){
        var id=$(this).attr("id");
        $.ajax({
            url:"{{url('')}}/purchase/invoice/"+id,
            method:"GET",
            data:
            {
                id:id,
            },
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                window.location.href= '{{url('')}}/purchase/invoice/'+id;
                // location.reload();
                $('.action').val('Actions');
            }
        });
    });

    $(document).on('click','.grn',function(){
        var id=$(this).attr("id");
        var grn=$(this).attr("data-grn");
        console.log(id,grn);
        // debugger
        $.ajax({
            url:"{{url('')}}/purchase/grr/",
            method:"GET",
            data:
            {
                grn:grn,
                id:id
            },
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                window.location.href= '{{url('')}}/purchase/grr1/'+grn+'-'+id;
                // location.reload();
                $('.action').val('Actions');
            }
        });
    });

    $(document).on('click','.multiple',function(){
        var id=$(this).attr("id");
        var currentRow=$(this).closest("tr");
        // console.log(id);
        $('.action').val('Actions');
        $.ajax({
            url:"{{url('')}}/purchaseDetail/"+id,
            method:"GET",
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                // console.log(data);
                type=data.type;
                if(data.type==1)
                {
                    unitName=data.variant.product.unit.u_name;
                    unitId=data.variant.product.unit_id;
                    pid=data.variant.id;
                }
                else
                {
                    unitName=data.products.unit.u_name;
                    unitId=data.products.unit_id;
                    pid=data.products.id;
                }
                var col1=currentRow.find("td:eq(6)").append(`<br>
                <input type="hidden" name="uid[]" value="`+unitId+`">
                <input type="hidden" name="pid[]" value="`+pid+`">
                <input type="hidden" name="type1[]" value="`+type+`">
                <input type="text" readonly value="`+unitName+`" class="form-control" >
                <input type="text" name="size[]" placeholder="wrrite size" class="form-control" >
                <input type="text" name="quantity2[]" placeholder="write quantity" class="form-control" >`);
            }
        });
    });

    // $(document).on('click','#received',function(){
    //     $('#myModal1').modal("hide");
    //     // location.reload();
    // });



    // $('#rcvfinal').on('submit',function(e){
    //     e.preventDefault();
    //     var size = $('input[name="size[]"]').map(function(){
    //         return this.value;
    //     }).get();
    //     var quantity = $('input[name="quantity2[]"]').map(function(){
    //         return this.value;
    //     }).get();
    //     var pid = $('input[name="pid"]').val();
    //     var uid = $('input[name="uid"]').val();
    //     $.ajax({
    //         url: "/purchase/Receiving",
    //         type: "POST",
    //         data: {
    //             "_token": "{{ csrf_token() }}",
    //             'pid': pid,
    //             'uid': uid,
    //             'size[]': size,
    //             'quantity[]': quantity,
    //         },
    //         success:function(response){
    //         console.log(response);
    //         if(response==200)
    //         {

    //         }
    //         else
    //         {
    //             location.reload();
    //         }
    //       },
    //     });
    // });

    $(document).on('click','#received',function(){
        $('#myModal1').modal("hide");
    });
</script>

    @endsection
@endsection
