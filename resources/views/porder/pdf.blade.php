<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .attendance-table1 table{
        width: 100%;
        /* border-collapse: collapse; */
        /* border: 1px solid #000; */
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
            /* padding-right: 120px; */
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 90px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .label1
        {
            font-size: 15px;
            text-align: left;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        .hr {
            display: block;
            border-style: solid;
            border-width: 1px;
            width: 350px;

        }
        @page
        {
            margin: 0;
            size: A4;
        }
  </style>
  <body>
      <header>
          <img src="{{url('')}}/uploads/posch.jpg" style="margin-top: 10px; float: left;" width="110px" height="40px" alt="logo" class="logo-default"  />
        PURCHASE ORDER
    </header>
    <div class="label col-sm-12">
        {{-- <label><b>Reference#: </b>{{$purchase->ref_no}}</label> --}}
        <br>
        @php
            $date=Carbon\Carbon::parse($purchase->order_date)->format('d-m-y');
        @endphp
        <label><b>Order Date: </b>{{$date}}</label>
        <br>
        <label><b>Order#: </b>{{$purchase->id}}</label>
        <br>
        <label><b>Terms of Payment: </b>{{$purchase->days == null  ? '' : $purchase->days}}{{$purchase->payment_mode == 'Credit' ? $purchase->payment_mode.' Days' : $purchase->payment_mode}} </label>
    </div>
    <div class="form-body">

        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Vendor:</b></label>
                </div>
            </div>
            <div style="margin-top: -18px; margin-left: 480px">
                <div class="form-outline">
                    <label for=""><b>Ship To:</b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline">
                    <p><b>Name: {{ucwords($purchase->supplier->name)}}</b></p>
                    <p><b>Company: {{ucwords($purchase->supplier->company)}}</b></p>
                    <p><b>Contact No: {{$purchase->supplier->c_no}}</b></p>
                    <p><b>NTN: {{ucwords($purchase->supplier->VAT)}}</b></p>
                </div>
            </div>
            <div style="margin-top: -250px; margin-left: 480px">
                <div class="form-outline">
                <p><b>Name: {{ $purchase->biller == null ? 'Posch Care' :  ucwords($purchase->biller->name)}}</b></p>
                    <p><b>Warehouse Name: {{ucwords($purchase->warehouse->w_name)}}</b></p>
                    <p><b>Address: {{ucwords($purchase->biller->address)}}</b></p>
                </div>
            </div>
        </div>
    </div>
        <div class="attendance-table" style="margin-top: 25px">
            <table class="table table-striped table-bordered">

                <thead>
                    <tr>
                        <th class="attendance-cell">S.No</th>
                        <th class="attendance-cell">Name</th>
                        <th class="attendance-cell">Brand Name</th>
                        <th class="attendance-cell">UOM</th>
                        {{-- <th class="attendance-cell">Brand</th> --}}
                        <th class="attendance-cell" >Per Unit Cost</th>
                        <th class="attendance-cell">Quantity</th>
                        {{-- <th class="attendance-cell">Received Quantity</th> --}}
                        <th class="attendance-cell">Sub total</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $amount=0;
                        $balance=0;
                        $a=1;
                    @endphp
                    @foreach($pdetail as $d)
                        <tr>
                            <td class="attendance-cell" >{{$a}}</td>
                            @if ($d->type == 1)
                                @php
                                    $name = explode('-',$d->variant->name);
                                @endphp
                                <td class="attendance-cell" >{{$name[1]}}</td>
                                <td class="attendance-cell" >{{$d->variant->product->brands->b_name}}</td>
                                <td class="attendance-cell" >{{$d->variant->product->unit->u_name}}</td>
                            @else
                                <td class="attendance-cell" >{{$d->products->pro_name}}</td>
                                <td class="attendance-cell" >{{$d->products->brands->b_name}}</td>
                                <td class="attendance-cell" >{{$d->products->unit->u_name}}</td>
                            @endif
                            <td class="attendance-cell" >{{$d->cost}}</td>
                            <td class="attendance-cell" >{{$d->quantity}}</td>
                            {{-- <td class="attendance-cell" >{{$d->received_quantity}}</td> --}}
                            <td class="attendance-cell" >{{$d->sub_total}}</td>
                        </tr>
                        @php
                        if($purchase->p_status == 'Pending')
                        {
                            $balance+=$d->sub_total;
                        }
                        $a++;
                        @endphp
                    @endforeach
                    <tr>
                        <td class="attendance-cell"  colspan="5"></td>
                        <td class="attendance-cell" ><b>SUBTOTAL</b></td>
                        <td class="attendance-cell" >{{$balance}}</td>
                    </tr>
                    @if ($purchase->tax_status == 'Yes')
                        @php
                            $tax = 100 + $purchase->tax;
                            $amount = ($purchase->total / $tax) * $purchase->tax;
                        @endphp
                        <tr>
                            <td class="attendance-cell"  colspan="5"></td>
                            <td class="attendance-cell" ><b>TAX</b>{{$purchase->tax.'%'}}</td>
                            <td class="attendance-cell" >{{$amount}}</td>
                        </tr>
                    @endif
                    <tr>
                        <td class="attendance-cell"  colspan="5"></td>
                        <td class="attendance-cell" ><b>TOTAL</b></td>
                        <td class="attendance-cell" >{{$purchase->total}}</td>
                    </tr>

                </tbody>

            </table>
        </div>
        <div class="label1">

            @if ($purchase->note != null)
                <br>
                <label>TERMS & CONDITIONS</label>
                <br>
                {!!$purchase->note!!}
            @endif
            <br>
            <br>
            <br>
            <br>
            <div class="attendance-table1">
                <table>
                    <tbody>
                        <tr>
                            <td><hr style="margin-left: -10px" class="hr" color="black"><br> <label style="margin-left: 50px"> Authorized Signature</label> <br> <label style="margin-left: 50px">M/S POSCH CARE</label> </td>
                            <td><hr style="margin-left: -50px" class="hr" color="black"><br> <label> Authorized Signature</label> <br> <label> {{ucwords($purchase->supplier->name)}} </label> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </body>
</html>

