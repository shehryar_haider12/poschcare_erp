@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/purchase">Purchase Order</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 row">
                                            <div class="col-md-4">
                                                <i class="fa fa-shopping-cart font-white"></i>Purchase Order Report
                                            </div>
                                            <div class="col-md-5"></div>
                                            <div class="col-md-3">
                                                {{-- <a style="margin-left:-20px"  href="{{route('purchase.excel')}}">
                                                    <i class="fa fa-file-excel-o  font-white"></i>
                                                </a> --}}
                                                {{-- <a id="GFG" href="{{route('purchase.create')}}" >
                                                    <button style="background: #00CCFF; margin-left:20px; " type="button"  class="btn btn-block btn-primary btn-md ">Add Purchase Order</button>
                                                </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Products</label>
                                                            <select  id="p_name" multiple class="form-control selectpicker" data-live-search="true">
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($product as $s)
                                                                    @if ($s->vstatus == 0)
                                                                        <option value="{{$s->pro_name}} ">{{$s->pro_name}}; Brand: {{$s->brands->b_name}}</option>
                                                                    @else
                                                                        @foreach ($s->variants as $v)
                                                                            @php
                                                                                $name = explode(' - ',$v->name);
                                                                            @endphp
                                                                            <option value="{{$name[1]}}">{{$name[1]}}; Brand: {{$s->brands->b_name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Order Date</label>
                                                            <input type="date"  dateFormat= 'yyyy-mm-dd' id="date"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Supplier Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" multiple id="s_name" >
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($supplier as $u)
                                                                <option  value="{{$u->name}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:1400px">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:20px">S.No</th>
                                                            <th style="width:20px">Invoice#</th>
                                                            <th style="width:40px">Reference #</th>
                                                            <th style="width:110px">Order Date</th>
                                                            <th style="width:120px">Warehouse</th>
                                                            <th style="width:140px">Supplier</th>
                                                            <th style="width:100px">Product Code</th>
                                                            <th style="width:100px">Product Name</th>
                                                            <th style="width:100px">Cost</th>
                                                            <th style="width:50px">Req Quantity</th>
                                                            <th style="width:50px">Total Cost</th>
                                                            <th style="width:70px">Received Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a=1;
                                                            $totalCost = 0;
                                                            $totalReqQty = 0 ;
                                                            $totalRcvQty = 0;
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($purchasedetail as $p)
                                                            <tr>
                                                                <td>{{$a}}</td>
                                                                <td>{{$p->id}}</td>
                                                                <td>{{$p->purchase->ref_no}}</td>
                                                                <td>{{$p->purchase->order_date}}</td>
                                                                <td>{{$p->purchase->warehouse->w_name}}</td>
                                                                <td>{{$p->purchase->supplier->name}}</td>
                                                                @if ($p->type == 1)
                                                                    @php
                                                                        $name = explode(' - ',$p->variant->name);
                                                                    @endphp
                                                                    <td>{{$name[0]}}</td>
                                                                    <td>{{$name[1]}}</td>
                                                                @else

                                                                    <td>{{$p->products->pro_code}}</td>
                                                                    <td>{{$p->products->pro_name}}</td>
                                                                @endif
                                                                <td>{{$p->cost}}</td>
                                                                <td>{{$p->quantity}}</td>
                                                                <td>{{$p->cost * $p->quantity}}</td>
                                                                <td>{{$p->received_quantity}}</td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $totalCost += $p->cost;
                                                                $totalReqQty += $p->quantity ;
                                                                $totalRcvQty += $p->received_quantity;
                                                                $total += $p->cost * $p->quantity;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$totalCost}}</td>
                                                            <td>{{$totalReqQty}}</td>
                                                            <td>{{$totalRcvQty}}</td>
                                                            <td>{{$total}}</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('custom-script')
        @toastr_js
        @toastr_render
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable();
        $('#p_name').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#p_name option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#p_name').selectpicker('deselectAll');
                $('#p_name').selectpicker('refresh');
            }
            $.each($('#p_name option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(7).search(search, true, false).draw();
        });
        $('#s_name').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#s_name option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#s_name').selectpicker('deselectAll');
                $('#s_name').selectpicker('refresh');
            }
            $.each($('#s_name option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(5).search(search, true, false).draw();
        });

        $('#date').on('change',function(){
            value = $("#date").val();
            table.columns(3).search(value, true, false).draw();
        });
    });

</script>

    @endsection
@endsection
