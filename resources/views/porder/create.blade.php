@extends('layouts.master')
@section('top-styles')
<style>
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
@section('sidebar-name1')
{{-- @if(Session::has('download'))
         <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif --}}
<li>
    <a href="{{url('')}}/purchase">Purchase Order</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Purchase Order</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-shopping-cart font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}}  Purchase Order</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ?  ($invoice ? route('purchase.updateInvoice',$purchase->id) : route('purchase.update',$purchase->id))  : route('purchase.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Order Number</label>
                                        <input value="{{$id}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Order Date*</label>
                                        <input value="{{$date ?? old('order_date') ?? $purchase->order_date}}" id="order_date" class="form-control" type="date" placeholder="Enter Order Date" name="order_date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Reference Number <small>(optional)</small></label>
                                        <input value="{{$purchase->ref_no ?? old('ref_no')}}" class="form-control" type="text" placeholder="Enter Contact Person Name" name="ref_no" >
                                        <span class="text-danger">{{$errors->first('ref_no') ? 'Reference Number already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse*</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="w_id" name="w_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($ware as $s)
                                                <option {{$s->id == $purchase->w_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->w_name}} - {{$s->w_type}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($ware as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('w_id') ? 'selected' : null}}>{{$s->w_name}} - {{$s->w_type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Status</label>
                                        <select class="form-control selectpicker" name="status" required>
                                            <option value="" disabled selected>Select...</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Received">Received</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Attach Document <small>(optional)</small></label>
                                        <input class="form-control" type="file" placeholder="Enter Product Name" name="doc" accept=".pdf, .docx, .txt">
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : in_array('Add Supplier',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Supplier*</label>
                                        <select class="form-control selectpicker" id="s_id" data-live-search="true" name="s_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Supplier',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($supplier as $s)
                                                <option {{$s->id == $purchase->s_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($supplier as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('s_id') ? 'selected' : null}}>{{$s->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Supplier',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px; width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#supplierModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if ($isEdit)
                            @if ($purchase->doc != null)
                                <div class="row">
                                    <div class="col-sm-5">
                                        <input type="hidden" name="doc1" value="{{$purchase->doc}}">
                                        <label style="margin-top: 10px" ><b> Document1 </b></label>
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="{{route('purchase.document',$purchase->id)}}" class='btn green invoice' style="margin-top: 10px" ><i class='fa fa-download'></i></a>
                                    </div>
                                </div>
                            @endif
                        @endif
                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Biller',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Biller*</label>
                                        <select class="form-control selectpicker" id="b_id" data-live-search="true" name="b_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Biller',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($biller as $s)
                                                <option {{$s->id == $purchase->b_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($biller as $s)
                                                <option value="{{$s->id}}" {{$s->id == old('b_id') ? 'selected' : null}}>{{$s->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Biller',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#billerModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Expected Recieving Date <small>(optional)</small> </label>
                                        <input class="form-control" id="exp_rcv_date" value="{{$purchase->exp_rcv_date ??  old('exp_rcv_date') }}"  type="date"  name="exp_rcv_date">
                                    </div>
                                </div>
                            </div>
                            @if ($isEdit)
                                @php
                                    if($purchase->exp_rcv_date != null)
                                    {
                                        $date1=date_create($purchase->order_date);
                                        $date2=date_create($purchase->exp_rcv_date);
                                        $diff = $diff=date_diff($date1,$date2);
                                        $diffDays = $diff->format("%a Days");
                                    }
                                    else {
                                        $diffDays = '';
                                    }
                                @endphp
                            @endif
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Expected Days <small>(optional)</small> </label>
                                        <input type="text" readonly class="form-control" id="exp_days" value="{{$isEdit ? $diffDays : ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-11' : in_array('Add Product',$permissions) ? 'col-sm-10' : 'col-sm-11'}}">
                                    <div class="form-outline">
                                        <label >Products*</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="p_id" required>
                                            <option disabled selected>Select..</option>
                                            @foreach ($product as $s)
                                                <option value="{{$s->id}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->unit->u_name}}
                                                 - {{$s->brands->b_name}} - {{$s->category->cat_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Product',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px; width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-1">
                                    <div class="form-control" style="margin-top: 23px; width:50px">
                                        <a href="#"  data-toggle="modal" data-target="#allproduct">
                                            <i class="fa fa-2x fa-list addIcon font-green" style="margin-top: 3px"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-outline">
                                        <label>Product Variants</label>
                                        <select class="form-control" data-live-search="true" id="v_id" >
                                            <option disabled selected>Select..</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Code - Name - Weight - Brand - Category</th>
                                                <th width="7%">Cost</th>
                                                <th width="10%">In Stock </th>
                                                <th width="5%">Quantity</th>
                                                <th width="10%">Sub total</th>
                                                <th width="18%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        @php
                                            $a=1;
                                            $quantity=0;
                                        @endphp
                                        @if ($isEdit)
                                            @foreach ($pdetail as $p)
                                                @if ($p->type == 0)
                                                    @if ($p->products->currentstocks->isEmpty())
                                                        @php
                                                            $quantity = 0;
                                                        @endphp
                                                    @else
                                                        @foreach ($p->products->currentstocks as $d)
                                                            @php
                                                                $quantity+=$d->quantity;
                                                            @endphp
                                                        @endforeach
                                                    @endif

                                                    <tr>
                                                        <td><input name="p_id[{{$a}}]" type="text" class="form-control pid" readonly="" value="{{$p->p_id}}"></td>
                                                        <td>{{$p->products->pro_code}} - {{$p->products->pro_name}} -
                                                            {{$p->products->unit->u_name}} - {{$p->products->brands->b_name}} -
                                                            {{$p->products->category->cat_name}}
                                                        </td>
                                                        <td><input type="text" class="form-control costs cost{{$a}}" min="1" id="cost{{$p->p_id}}" data-id="{{$p->p_id}}" name="cost[{{$a}}]" value="{{$p->cost}}"></td>
                                                        <td><input type="text" class="form-control" readonly="" value="{{$quantity}}({{$p->products->unit->u_name}})">
                                                            <input type="hidden" class="type" name="type[{{$a}}]" value="{{$p->type}}">
                                                            <input type='hidden' name='pdid[{{$a}}]' value='{{$p->id}}'>
                                                        </td>
                                                        <td><input type="text" class="form-control quantity" min="1" id="{{$p->p_id}}" name="quantity[{{$a}}]" value="{{$p->quantity}}"></td>
                                                        <td><input type="text" class="form-control abc sub{{$a}}" name="sub_total[{{$a}}]" id="sub{{$p->p_id}}" value="{{$p->sub_total}}"></td>
                                                        <td><button type="button" id="{{$p->p_id}}" class="btn red delete"><i class="fa fa-trash"></i></button></td>
                                                    </tr>
                                                @else
                                                    @if ($p->variant->currentstocks->isEmpty())
                                                        @php
                                                            $quantity = 0;
                                                        @endphp
                                                    @else
                                                        @foreach ($p->variant->currentstocks as $d)
                                                            @php
                                                                $quantity+=$d->quantity;
                                                            @endphp
                                                        @endforeach
                                                    @endif
                                                    <tr>
                                                        <td><input name="p_id[{{$a}}]" type="text" class="form-control pid" readonly="" value="{{$p->p_id}}"></td>
                                                        <td>{{$p->variant->name}}
                                                        </td>
                                                        <td><input type="text" class="form-control costs cost{{$a}}" min="1" id="cost{{$p->p_id}}" data-id="{{$p->p_id}}" name="cost[{{$a}}]" value="{{$p->cost}}"></td>
                                                        <td><input type="text" class="form-control" readonly="" value="{{$quantity}}({{$p->variant->product->unit->u_name}})">
                                                            <input type="hidden" class="type" name="type[{{$a}}]" value="{{$p->type}}">
                                                            <input type='hidden' name='pdid[{{$a}}]' value='{{$p->id}}'>
                                                        </td>
                                                        <td><input type="text" class="form-control quantity" min="1" id="{{$p->p_id}}" name="quantity[{{$a}}]" value="{{$p->quantity}}"></td>
                                                        <td><input type="text" class="form-control abc sub{{$a}}" name="sub_total[{{$a}}]" id="sub{{$p->p_id}}" value="{{$p->sub_total}}"></td>
                                                        <td><button type="button" id="{{$p->p_id}}" class="btn red delete"><i class="fa fa-trash"></i></button></td>
                                                    </tr>
                                                @endif
                                                @php
                                                  $a++;
                                                @endphp
                                            @endforeach
                                        @endif
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <div class="col-sm-6">
                                    <label for=""><b>TOTAL COST:</b> </label>
                                    <label id="total">0</label>
                                </div> --}}
                                <div class="col-sm-6">
                                    <label for=""><b>TOTAL AMOUNT:</b> </label>
                                    @if ($isEdit)
                                        <label hidden id="amount1">{{$purchase->total}}</label>
                                        <label id="amount">{{$purchase->total}}</label>
                                    @else
                                        <label id="amount">0</label>
                                        <label hidden id="amount1">0</label>
                                    @endif
                                    <input type="hidden" name="final" id="final" value="{{ $isEdit ? $purchase->total : 0}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Tax Status*</label>
                                        <select class="form-control selectpicker" required name="tax_status" id="tax_status">
                                            <option value="" disabled selected>Select...</option>

                                            @if ($isEdit)
                                            <option {{$purchase->tax_status == 'No' ? 'selected' : null}} >No</option>
                                            <option {{$purchase->tax_status == 'Yes' ? 'selected' : null}} >Yes</option>
                                            @else
                                            <option {{old('tax_status') == 'No' ? 'selected' : null}}>No</option>
                                            <option {{old('tax_status') == 'Yes' ? 'selected' : null}}>Yes</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Tax <small>In %</small></label>
                                        <input value="{{$isEdit ? $purchase->tax : old('tax')}}" readonly class="form-control" type="text" min="0" placeholder="Enter  Tax" name="tax" id="tax">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Payment Mode*</label>
                                        <select class="form-control" name="payment_mode" id="payment_mode" required>
                                            <option value=""  selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$purchase->payment_mode == 'Credit' ? 'selected' : null}} value="Credit">Credit</option>
                                            <option {{$purchase->payment_mode == 'Cash on Delivery' ? 'selected' : null}} value="Cash on Delivery">Cash on Delivery</option>
                                            @else
                                            <option value="Credit" {{old('payment_mode') == 'Credit' ? 'selected' : null}}>Credit</option>
                                            <option value="Cash on Delivery" {{old('payment_mode') == 'Cash on Delivery' ? 'selected' : null}} >Cash on Delivery</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Date</label>
                                        <input class="form-control" readonly type="date" id="pdate" name="pdate" value="{{$isEdit ?  $purchase->pdate : old('pdate')}}">

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Term</label>
                                        <input class="form-control" id="days" readonly type="text" name="days" value="{{$isEdit ?  $purchase->days : old('days')}}">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Note <small>(optional)</small></label>
                                        <input type="hidden" id="note" value="{{$isEdit ?  $purchase->note : old('note')}}">
                                        {{-- @if ($isEdit)
                                            <textarea style="display:none;" id="note1" value="{{$purchase->note}}"></textarea>
                                        @else --}}
                                        <textarea name="editor1" id="editor1" rows="10" cols="80">
                                        </textarea>
                                        {{-- @endif --}}
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@section('modal')
    @include('modals.product')
    @include('modals.biller')
    @include('modals.supplier')
    @include('modals.allproducts')
@endsection

@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>


    $(document).on('change','#payment_mode',function(){
        var p=$(this).val();
        console.log(p);
        if(p == 'Credit')
        {
            $('#pdate').prop('readonly',false);
            $('#pdate').prop('required',true);
        }
        else
        {
            $('#pdate').prop('readonly',true);
            $('#pdate').prop('required',false);
            $('#pdate').val('');
            $('#days').val('');
        }
    });

    $(document).on('change','#pdate',function(){
        var odate = $('#order_date').val();
        var today = new Date(odate);
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var pdate = $(this).val();
        var date1 = new Date(date);
        var date2 = new Date(pdate);
        var diff =  date2.getTime() - date1.getTime();
        var Difference_In_Days = diff / (1000 * 3600 * 24);
        $('#days').val(Difference_In_Days);
    });


    $(document).on('change','#exp_rcv_date',function(){
        var odate = $('#order_date').val();
        var today = new Date(odate);
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var pdate = $(this).val();
        var date1 = new Date(date);
        var date2 = new Date(pdate);
        var diff =  date2.getTime() - date1.getTime();
        var Difference_In_Days = diff / (1000 * 3600 * 24);
        $('#exp_days').val(Difference_In_Days+' Days');
    });

    $(document).on('change','#tax_status',function(){
        var val = $(this).val();
        if(val == 'Yes')
        {
            $('#tax').prop('readonly',false);
        }
        else
        {
            $('#tax').prop('readonly',true);
        }
    })

    var note=$('#note').val();
    if(note == '')
    {
        CKEDITOR.replace( 'editor1' );
    }
    else
    {
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.instances['editor1'].setData(note);
        // CKEDITOR.Text= note;
    }
</script>
<script>
    $(document).ready(function() {
        $('#s_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#supplierModal').modal("show"); //Open Modal
            }
        });
        $('#b_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#billerModal').modal("show"); //Open Modal
            }
        });
        var table = $('#example5').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: '{{route("purchase.product.datatable")}}',
            "columns": [{
                    "data": "",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        return  ` P - ` +row.id;
                    },
                },
                {
                    "data": "pro_name",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        return row.pro_code+ ` - ` +row.pro_name ;
                    },
                },
                {
                    "data": "",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                            return row.unit.u_name ;
                    },
                },
                {
                    "data": "brands.b_name",
                    "defaultContent": ""
                },
                {
                    "data": "category.cat_name",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        return ` <button type="button"  class="btn green add" id="add.`+row.id +`">
                            <i class="fa fa-plus"></i>
                        </button>`;
                    },
                },
            ],
        });

        $('#example5').on('click','.add',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).attr('id');
            id = id.split('.');
            var w_id = $('#w_id').val();
            console.log(id);
            $.ajax({
                url:"{{url('')}}/product/sales/"+id+'.'+w_id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    if(data.total_quantity == null)
                    {
                        total_quantity=0;
                    }
                    else
                    {
                        total_quantity = data.total_quantity;
                    }
                    if(data.vstatus == 0)
                    {
                        if($('#'+data.id).length && data.vstatus == 0)
                        {

                        }
                        else
                        {
                            $("#example").append(`<tr id='`+data.id+`'><td><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'><input type='hidden' name='type[`+rowCount+`]' class="type" value='0'></td>
                        <td>`+data.pro_code+` - `+data.pro_name+` - `+data.unit.u_name+` - `+data.brands.b_name+` - `+data.category.cat_name+`</td>
                        <td><input required type='text' class='form-control costs cost`+rowCount+`' min='1' id='cost`+rowCount+`' data-id='`+rowCount+`' name='cost[`+rowCount+`]' value='`+data.cost+`'></td>
                        <td><input type='text' class='form-control' readonly  value='`+total_quantity+`(`+data.unit.u_name+`)'></td>
                        <td><input type='number' class='form-control quantity' min='1' required id='`+rowCount+`' name='quantity[`+rowCount+`]' value='1'></td><td><input type='text' readonly class='form-control abc sub`+rowCount+`' name='sub_total[`+rowCount+`]' id='sub`+rowCount+`' value='`+data.cost+`'></td><td><button type='button' data-id='`+rowCount+`' id='dlt`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
                            var amount=0;
                            var total=0;
                            var getamount = $('#amount1').html();
                            console.log(getamount);
                            var rowCount1 = $('#example tr').length;
                            for (let i = 1; i < rowCount1; i++) {
                                amount = +amount + +$('.sub'+i).val();
                                total = +total + +$('.cost'+i).val();
                            }
                            $('#amount1').html(amount.toFixed(2));



                            if($('#tax').val()=='' )
                            {
                                if(getamount == 0)
                                {
                                    $('#amount').html(amount.toFixed(2));
                                }
                                else
                                {
                                    amount = +amount + +getamount;
                                    $('#amount').html(amount.toFixed(2));
                                }
                                $('#final').val(amount.toFixed(2));
                            }

                            if($('#tax').val()!='')
                            {
                                var tax = $('#tax').val();
                                var final = (amount * tax) / 100;
                                amount = +amount + +final;
                                $('#final').val(amount.toFixed(2));
                                $('#amount').html(amount.toFixed(2));
                            }
                        }
                    }
                    else
                    {
                        $('#v_id').empty();
                        $('#v_id').selectpicker('destroy');
                        $('#v_id').append(`<option disabled selected>Select..</option>`);
                        for (let index = 0; index < data.variants.length; index++) {
                            $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                        }
                        $('#v_id').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });
        });
    });
    $(document).on('change','#p_id',function(){
        var rowCount = $('#example tr').length;
        var id=$(this).val();
        var w_id = $('#w_id').val();
        $.ajax({
            url:"{{url('')}}/product/sales/"+id+'.'+w_id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                if(data.total_quantity == null)
                {
                    total_quantity=0;
                }
                else
                {
                    total_quantity = data.total_quantity;
                }
                if(data.vstatus == 0)
                {
                    if($('#'+data.id).length && data.vstatus == 0)
                    {
                        console.log('fyyty');
                    }
                    else
                    {
                        $("#example").append(`<tr id='`+data.id+`'><td><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'><input type='hidden' name='type[`+rowCount+`]' class="type" value='0'></td>
                        <td>`+data.pro_code+` - `+data.pro_name+` - `+data.unit.u_name+` - `+data.brands.b_name+` - `+data.category.cat_name+`</td>
                        <td><input required type='text' class='form-control costs cost`+rowCount+`' min='1' id='cost`+rowCount+`' data-id='`+rowCount+`' name='cost[`+rowCount+`]' value='`+data.cost+`'></td>
                        <td><input type='text' class='form-control' readonly  value='`+total_quantity+`(`+data.unit.u_name+`)'></td>
                        <td><input type='number' class='form-control quantity' min='1' required id='`+rowCount+`' name='quantity[`+rowCount+`]' value='1'></td><td><input type='text' readonly class='form-control abc sub`+rowCount+`' name='sub_total[`+rowCount+`]' id='sub`+rowCount+`' value='`+data.cost+`'></td><td><button type='button' data-id='`+rowCount+`' id='dlt`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
                        var amount=0;
                        var total=0;
                        var getamount = $('#amount1').html();
                        console.log(getamount);
                        var rowCount1 = $('#example tr').length;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            total = +total + +$('.cost'+i).val();
                        }
                        $('#amount1').html(amount.toFixed(2));



                        if($('#tax').val()=='' )
                        {
                            if(getamount == 0)
                            {
                                $('#amount').html(amount.toFixed(2));
                            }
                            else
                            {
                                amount = +amount + +getamount;
                                $('#amount').html(amount.toFixed(2));
                            }
                            $('#final').val(amount.toFixed(2));
                        }

                        if($('#tax').val()!='')
                        {
                            var tax = $('#tax').val();
                            var final = (amount * tax) / 100;
                            amount = +amount + +final;
                            $('#final').val(amount.toFixed(2));
                            $('#amount').html(amount.toFixed(2));
                        }
                    }
                }
                else
                {
                    $('#v_id').empty();
                    $('#v_id').selectpicker('destroy');
                    $('#v_id').append(`<option disabled selected>Select..</option>`);
                    for (let index = 0; index < data.variants.length; index++) {
                        $('#v_id').append(`<option value="`+data.variants[index].id+`">`+data.variants[index].name+`</option>`);
                    }
                    $('#v_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
            }
        });
    });

    $('table').on('click', '.delete', function(e){
        var id=$(this).attr('data-id');
        var cost=$('#cost'+id).val();
        var sub=$('#sub'+id).val();
        // var total=$('#total').html();
        var amount=$('#amount1').html();
        // total = -cost - -total;
        amount = -sub - -amount;
        // $('#total').text(total);
        $('#amount1').text(amount);

        $(this).closest('tr').remove();
        $(".abc").each(function (i){
           $(this).removeClass('sub'+(i+2));
           $(this).addClass('sub'+(i+1));
        });
        $(".costs").each(function (i){
           $(this).removeClass('cost'+(i+2));
           $(this).addClass('cost'+(i+1));
           $(this).removeAttr('data-id');
           $(this).attr('data-id', (i+1));
           $(this).removeAttr('id');
           $(this).attr('id', 'cost'+(i+1));
        });
        $(".pid").each(function (i){
            i=+i + +1;
            $(this).attr('name','p_id['+i+']');
        });
        $(".type").each(function (i){
            i=+i + +1;
            $(this).attr('name','type['+i+']');
        });
        $(".costs").each(function (i){
            i=+i + +1;
            $(this).attr('name','cost['+i+']');
        });
        $(".quantity").each(function (i){
            i=+i + +1;
            $(this).attr('name','quantity['+i+']');
            $(this).removeAttr('id');
            $(this).attr('id', (i));
        });
        $(".abc").each(function (i){
            i=+i + +1;
            $(this).attr('name','sub_total['+i+']');
            $(this).removeAttr('id');
            $(this).attr('id', 'sub'+(i));
        });
        $(".delete").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
            $(this).attr('data-id',i);
        });
        if($('#tax').val()=='' )
        {
            $('#final').val(amount);
            $('#amount').html(amount);
        }

        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount.toFixed(2));
            $('#amount').html(amount.toFixed(2));
        }

    });

    $('table').on('change','.quantity',function(){
        var a=$(this).val();
        var id=$(this).attr('id');
        var cost=$('#cost'+id).val();
        var sub=a * cost;
        $('#sub'+id).val(sub);
        var amount=0;
        var rowCount = $('#example tr').length;
        for (let i = 1; i < rowCount; i++) {
            amount = +amount + +$('.sub'+i).val();
        }
        $('#amount1').html(amount);
        if($('#tax').val()=='' )
        {
            $('#final').val(amount.toFixed(2));
            $('#amount').html(amount.toFixed(2));
        }

        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount.toFixed(2));
            $('#amount').html(amount.toFixed(2));
        }
    });

    $('table').on('keyup','.costs',function(){
        var id=$(this).attr('data-id');
        var cost=$(this).val();
        var quan=$('#'+id);
        quan=quan.val();
        var sub=quan * cost;
        $('#sub'+id).val(sub);
        var amount=0;
        var total=0;
        var rowCount1 = $('#example tr').length;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            total = +total + +$('.cost'+i).val();
        }
        $('#amount1').html(amount);
        if($('#tax').val()=='' )
        {
            $('#final').val(amount.toFixed(2));
            $('#amount').html(amount.toFixed(2));
        }

        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount.toFixed(2));
            $('#amount').html(amount.toFixed(2));
        }
    });



$(document).on('change','#v_id',function(){
    var rowCount = $('#example tr').length;
    var id=$(this).val();
    var w_id = $('#w_id').val();
    $.ajax({
        url:"{{url('')}}/product/Pvariants/"+id+'.'+w_id,
        method:"GET",
        error: function (request, error) {
                    alert(" Can't do because: " + error +request);
                },
        success:function(data){
            console.log(data);
            if(data.total_quantity == null)
            {
                total_quantity=0;
            }
            else
            {
                total_quantity = data.total_quantity;
            }
            if($('#'+data.id).length && data.product.vstatus == 1)
            {
                console.log('dsds');
            }
            //type 1 = variant, type 0 = no variant
            else
            {
                $("#example").append(`<tr id='`+data.id+`'><td><input name='p_id[`+rowCount+`]' type='text' class='form-control pid' readonly value='`+data.id+`'><input type='hidden' name='type[`+rowCount+`]' class="type" value='1'></td>
                <td>`+data.name+`</td>
                <td><input required type='text' class='form-control costs cost`+rowCount+`' min='1' id='cost`+rowCount+`' data-id='`+rowCount+`' name='cost[`+rowCount+`]' value='`+data.cost+`'></td>
                <td><input type='text' class='form-control' readonly  value='`+total_quantity+`'></td>
                <td><input type='number' class='form-control quantity' min='1' required id='`+rowCount+`' name='quantity[`+rowCount+`]' value='1'></td><td><input type='text' readonly class='form-control abc sub`+rowCount+`' name='sub_total[`+rowCount+`]' id='sub`+rowCount+`' value='`+data.cost+`'></td><td><button type='button' id='dlt`+rowCount+`' data-id='`+rowCount+`' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>`);
                var amount=0;
                var total=0;
                var getamount = $('#amount1').html();
                // console.log(getamount);
                var rowCount1 = $('#example tr').length;
                for (let i = 1; i < rowCount1; i++) {
                    amount = +amount + +$('.sub'+i).val();
                    total = +total + +$('.cost'+i).val();
                }
                $('#amount1').html(amount.toFixed(2));



                if($('#tax').val()=='' )
                {
                    if(getamount == 0)
                    {
                        $('#amount').html(amount.toFixed(2));
                    }
                    else
                    {
                        amount = +amount + +getamount;
                        $('#amount').html(amount.toFixed(2));
                    }
                    $('#final').val(amount.toFixed(2));
                }

                if($('#tax').val()!='')
                {
                    var tax = $('#tax').val();
                    var final = (amount * tax) / 100;
                    amount = +amount + +final;
                    $('#final').val(amount.toFixed(2));
                    $('#amount').html(amount.toFixed(2));
                }
            }
        }
    });
});


    $(document).on('keyup','#tax',function(){
        var amount =   $('#amount1').text();
        var tax = $(this).val();
        if(tax == '')
        {
            $('#amount').html(amount);
            $('#final').val(amount);
        }
        else
        {
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#amount').html(amount);
            $('#final').val(amount);
        }
    });


</script>
@endsection
