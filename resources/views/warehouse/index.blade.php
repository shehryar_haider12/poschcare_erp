@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/warehouse">Warehouse</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-map-marker font-white"></i>View Warehouse
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('warehouse.create')}}" class="col-md-2" style="float: right">
                                                    <button style="background: #00CCFF" type="button"  class="btn btn-block btn-primary btn-md ">Add Warehouse</button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Warehouse Name</label>
                                                        <input type="text" id="w_name" id="autocomplete-ajax1" class="form-control" placeholder="Warehouse Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">City Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" id="c_name" multiple>
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($city as $u)
                                                                <option  value="{{$u->c_name}}">{{$u->c_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Type</th>
                                                            <th>City</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Warehouse</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Warehouse Name" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >City Name</label>
                                    <input class="form-control" type="text" placeholder="Enter City Name" id="c_name" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse Address</label>
                                    <input class="form-control" type="text" placeholder="Enter Warehouse Address" id="w_address" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse Contact No</label>
                                    <input class="form-control" type="text" placeholder="Enter Warehouse Contact No" id="w_contactNo" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Contact Person Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Contact Person Name" id="c_p_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Contact Person Number</label>
                                    <input class="form-control" type="text" placeholder="Enter Contact Person Number" id="c_p_contactNo" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse Type</label>
                                    <input class="form-control" type="text" placeholder="Enter Contact Person Number" id="w_type" readonly>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
            <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
@toastr_render
        {{-- view one product --}}
        <script>
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/warehouse/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#w_name').val(data.w_name);
                        $('#c_name').val(data.city.c_name);
                        $('#w_address').val(data.w_address);
                        $('#w_contactNo').val(data.w_contactNo);
                        $('#c_p_name').val(data.c_p_name);
                        $('#w_type').val(data.w_type);
                        $('#c_p_contactNo').val(data.c_p_contactNo);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    processing: true,
                    serverSide: true,
                    order: [[ 0, "desc" ]],
                    scrollX: true,
                    ajax: '{{route("warehouse.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "w_name",
                            "defaultContent": ""
                        },
                        {
                            "data": "w_type",
                            "defaultContent": ""
                        },
                        {
                            "data": "city.c_name",
                            "defaultContent": ""
                        },
                        {
                            "data": "id",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                var edit = '{{route("warehouse.edit",[":id"])}}';
                                edit = edit.replace(':id', row.id);
                                var checked = row.status == 1 ? 'checked' : null;
                                return `
                                @if(in_array('edit',$permissions))
                                    <a id="GFG" href="` + edit + `" class="text-info p-1">
                                        <button type="button" class="btn blue edit" >
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                @endif
                                @if(in_array('show',$permissions))
                                    <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                        <i class="icon-eye"></i>
                                    </button>
                                @endif
                                `;
                            },
                        },
                    ],
                });
                $('#c_name').on('change', function(){
                    var search = [];
                    if($(this).get(0).value == 'All')
                    {
                        $('#c_name option:not(:eq(0))').prop('selected',true);

                    }
                    if($(this).get(0).value == 'Not All')
                    {
                        $('#c_name').selectpicker('deselectAll');
                        $('#c_name').selectpicker('refresh');
                    }
                    $.each($('#c_name option:selected'), function(){
                        search.push($(this).val());
                    });

                    search = search.join('|');
                    table.column(3).search(search, true, false).draw();
                });
                $('#w_name').on('keyup',function(){
                    value = $("#w_name").val();
                    table.columns(1).search(value, true, false).draw();
                });
            });



        </script>
    @endsection
@endsection
