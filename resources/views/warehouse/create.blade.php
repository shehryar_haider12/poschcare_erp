@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/warehouse">Warehouse</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Warehouse</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-map-marker font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Warehouse</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form id="warehouseForm" action="{{$isEdit ? route('warehouse.update',$ware->id) :  route('warehouse.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Warehouse Name*</label>
                                        <input value="{{$ware->w_name ?? old('w_name')}}" class="form-control" type="text" placeholder="Enter Warehouse Name" name="w_name" >
                                        <span class="text-danger">{{$errors->first('w_name') ? 'already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Warehouse Address*</label>
                                        <input value="{{$ware->w_address ?? old('w_address')}}" class="form-control" type="text" placeholder="Enter Warehouse Address" name="w_address" >
                                        <span class="text-danger">{{$errors->first('w_address') ? 'already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Warehouse Contact No <small>(optional)</small></label>
                                        <input min="0" value="{{$ware->w_contactNo ?? old('w_contactNo')}}" class="form-control" type="number" placeholder="Enter Warehouse Contact Number" name="w_contactNo" >
                                        <span class="text-danger">{{$errors->first('w_contactNo') ? 'already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : in_array('Add City',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >City <small>(optional)</small></label>
                                        <select id="c_id" class="form-control selectpicker" data-live-search="true" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add City',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($city as $s)
                                                <option {{$s->id == $ware->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->c_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($city as $s)
                                                <option value="{{$s->id}}" {{old('c_id') == $s->id ? 'selected' : null}}>{{$s->c_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add City',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#cityModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Contact Person Name*</label>
                                        <input value="{{$ware->c_p_name ?? old('c_p_name')}}" class="form-control" type="text" placeholder="Enter Contact Person Name" name="c_p_name" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Contact Person Number*</label>
                                        <input min="0" value="{{$ware->c_p_contactNo ?? old('c_p_contactNo')}}" class="form-control" type="number" placeholder="Enter Contact Person Number" name="c_p_contactNo" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Warehouse Type*</label>
                                        <select class="form-control selectpicker" name="w_type" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$ware->w_type == 'Standard' ? 'selected' : null }} value="Standard">Standard</option>
                                            <option {{$ware->w_type == 'Finished' ? 'selected' : null }} value="Finished">Finished</option>
                                            @else
                                            <option value="Standard" {{old('w_type') == 'Standard' ? 'selected' : null}} >Standard</option>
                                            <option value="Finished" {{old('w_type') == 'Finished' ? 'selected' : null}} >Finished</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.city')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#warehouseForm').validate({
        rules: {
            w_name: {
                required: true,
            },
            w_address: {
                required: true,
            },
            c_id: {
                required: true,
            },
            c_p_name: {
                required: true,
            },
            c_p_contactNo: {
                required: true,
            },
            w_type: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
<script>
    $(document).ready(function(){
        $('#c_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#cityModal').modal("show"); //Open Modal
            }
        });
    });
</script>
@endsection
