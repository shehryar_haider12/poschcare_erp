@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
        /* height: 1000px; */
    }
</style>
<style>
    .hr {
    display: block;
    border-style: inset;
    border-width: 1px;
    margin-top: 10px;
    margin-bottom: 10px;
    }
    .h5
    {
        margin: 0 auto;
        position: relative;
        /* right: 940px; */
    }
    .h51
    {
        margin: 0 auto;
        position: relative;
        right: 1170px;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/accountdetails">General Ledger</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>General Ledger Entry</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-money font-white"></i>Reverse Entry
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <form action="{{route('generalLedger.updateEntry')}} " class="form-horizontal" method="POST" >
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Source</label>
                                                            <input type="hidden" name="linkidold" value="{{$ledger[0]->link_id}}">
                                                            <input value="{{$ledger[0]->source}}" class="form-control" type="text" readonly name="source" >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Created By</label>
                                                            <input value="{{$u_name}}" class="form-control" type="text" readonly  >
                                                            <input value="{{$u_id}}" name="created_by" hidden >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Posted Date</label>
                                                            <input value="{{$date}}" name="posted_date" class="form-control" type="date" readonly >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Period</label>
                                                            <input value="{{$month}}" name="period" class="form-control" type="text" readonly >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Currency Code</label>
                                                            <input value="PKR" name="currency_code" class="form-control" type="text" readonly >
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="hr">
                                                <div class="custom_datatable">
                                                    <div class="tableview">
                                                        @foreach ($ledger as $l)
                                                            @if ($l->debit != 0 && ($l->account_name != 'Tax' && $l->account_name != 'Discount' && (!str_contains($l->account_code, 'CA-02'))))
                                                                <div class="row">
                                                                    <h5>Debit Entry</h5>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="">Account Code</label>
                                                                            <input type="text" readonly class="form-control" name="account_code_d[]" value="{{$l->account_code}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="">Account Name</label>
                                                                            <input value="{{$l->account_name}}" name="account_name_d[]" id="account_name_d" class="form-control" type="text" readonly >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="">Accounting Date</label>
                                                                            <input value="{{$l->accounting_date}}" name="accounting_date_d[]"  class="form-control" type="date"  >
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="">Reference No</label>
                                                                            <input readonly value="{{$l->transaction_no}}" name="transaction_no_d[]"  class="form-control" type="text"  >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="">Debit <small>(Add Credit Value)</small></label>
                                                                            <input name="debit_d[]"  class="form-control" type="text" required value="{{$l->debit}}">
                                                                            <input name="credit_d[]" value="0"  hidden >
                                                                            <input type="hidden" name="net_d[]" value="{{$l->debit}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="">Description</label>
                                                                            <textarea name="description_d[]" rows="2" cols="50" required >{{$l->description}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $id=1;
                                                            $count_p = 0;
                                                            $a = 1;
                                                            // dd($ledger[3]->account_code);
                                                        @endphp
                                                        @if ($count > 2 && ($ledger[0]->account_name == 'Purchase' || $ledger[1]->account_name == 'Purchase'))
                                                        @if (str_contains($ledger[2]->account_code, 'CA-02'))
                                                            @php
                                                                $count_p = 2;
                                                            @endphp
                                                        @else
                                                            @if (str_contains($ledger[3]->account_code, 'CA-02'))
                                                                @php
                                                                    $count_p = 3;
                                                                @endphp
                                                            @endif
                                                        @endif
                                                        <div id="prod_name_d1">
                                                            @for ($i = $count_p; $i < $count; $i++)
                                                            <div class="rowd1">
                                                                <h5 class="{{$a>1 ? 'h51' : 'h5'}}" >Products</h5>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Products Name</label>
                                                                        <input type="text" readonly class="form-control" name="prod_name_d[]" value="{{$ledger[$i]->account_name}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <input value="{{$ledger[$i]->account_code}}" name="prod_code_d[]" id="prod_code_d{{$id}}" class="form-control" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Reference No</label>
                                                                        <input readonly name="prod_transaction_no_d[]" value="{{$ledger[$i]->transaction_no}}"  class="form-control" type="text"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Quantity</label>
                                                                        <input name="prod_credit_d[]" value="{{$ledger[$i]->stock_in}}"  class="form-control" type="number" min="0" >
                                                                        <input name="prod_debit_d[]" value="0" type="hidden"  >
                                                                        <input name="prod_net_d[]"  value="{{$ledger[$i]->debit}}" type="hidden"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input readonly name="prod_amount_d[]" id="prod_amount_d1" value="{{$ledger[$i]->amount}}" class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button disabled type='button' id="{{$id}}" class='btn btn-lg red delete_d' style="margin-top: 20px; margin-left:30px" ><i class='fa fa-trash'></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button disabled type='button' id="{{$id}}" class='btn btn-lg blue add_d' style="margin-top: 20px; margin-right: 20px" ><i class='fa fa-plus'></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @php
                                                            $id++;
                                                            $a++;
                                                        @endphp
                                                        @endfor
                                                        @endif


                                                        @foreach ($ledger as $s)
                                                        @if ($s->credit != 0 && ($s->account_name != 'Tax' && $s->account_name != 'Discount' && (!str_contains($s->account_code, 'CA-02'))))
                                                        <div class="row">
                                                            <h5 class="{{$ledger[0]->account_name == 'Purchase' ? 'h51'  : null}}">Credit Entry</h5>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="">Account Code</label>
                                                                    <input type="text" readonly class="form-control" name="account_code_c[]" value="{{$s->account_code}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="">Account Name</label>
                                                                    <input value="{{$s->account_name }}" name="account_name_c[]" id="account_name_c" class="form-control" type="text" readonly >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="">Accounting Date</label>
                                                                    <input name="accounting_date_c[]" value="{{$s->accounting_date }}" class="form-control" type="date"  >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="">Reference No</label>
                                                                    <input readonly name="transaction_no_c[]" value="{{$s->transaction_no }}" class="form-control" type="text"  >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="">Credit<small>(Add Debit Value)</small></label>
                                                                    <input name="credit_c[]"  class="form-control" type="text" required value="{{$s->credit}}">
                                                                    <input name="debit_c[]" value="0"  hidden >
                                                                    <input type="hidden" name="net_c[]" value="{{$s->credit}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label for="">Description</label>
                                                                    <textarea name="description_c[]" rows="2" cols="50" required >{{$s->description}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endforeach
                                                        @if ($count > 2 && ($ledger[1]->account_name == 'Sales' || $ledger[0]->account_name == 'Sales' || $ledger[2]->account_name == 'Sales'))
                                                        <div id="prod_name_c1" >
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Tax <small>(In %)</small> </label>
                                                                        @php
                                                                            $tax = 0;
                                                                            $discount = 0;
                                                                            $counter = 0;
                                                                        @endphp
                                                                        @if (count($result1) == 2)
                                                                            @php
                                                                                $counter = 2;
                                                                            @endphp
                                                                        @endif
                                                                        @if (count($result1) == 3)
                                                                            @php
                                                                                $counter = 3;
                                                                            @endphp
                                                                        @endif

                                                                        {{-- @if (count($result1) == 4 || count($result1) == 2)

                                                                            @if (count($result) == 1)
                                                                                @php
                                                                                    $counter = 3;
                                                                                @endphp
                                                                            @endif
                                                                            @if (count($result) == 2)
                                                                                @php
                                                                                    $counter = 4;
                                                                                @endphp
                                                                            @endif
                                                                            @if (count($result) == 3)
                                                                                @php
                                                                                    $counter = 5;
                                                                                @endphp
                                                                            @endif
                                                                            @if (count($result) == null)
                                                                                @php
                                                                                    $counter = 2;
                                                                                @endphp
                                                                            @endif
                                                                        @endif
                                                                        @if (count($result1) == 3 || count($result1) == 1)

                                                                        @if (count($result) == 1)
                                                                            @php
                                                                                $counter = 2;
                                                                            @endphp
                                                                        @endif
                                                                        @if (count($result) == 2)
                                                                            @php
                                                                                $counter = 3;
                                                                            @endphp
                                                                        @endif
                                                                        @if (count($result) == 3)
                                                                            @php
                                                                                $counter = 4;
                                                                            @endphp
                                                                        @endif
                                                                        @if (count($result) == null)
                                                                            @php
                                                                                $counter = 3;
                                                                            @endphp
                                                                        @endif
                                                                    @endif --}}

                                                                        @foreach ($ledger as $l)
                                                                            @if ($l->account_name == 'Tax')
                                                                                @php
                                                                                    $tax = $l->debit;
                                                                                @endphp
                                                                            @endif
                                                                        @endforeach
                                                                        <input value="{{$tax}}" name="tax" id="tax" class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- {{dd($counter)}} --}}
                                                            @for ($i = $counter ; $i < $count; $i++)
                                                            <div class="rowc1">
                                                                <h4 class="{{$i == $counter ? null  : 'h51'}}">Products</h4>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Products Name</label>
                                                                        <input type="text" readonly class="form-control" name="prod_name_c[]" value="{{$ledger[$i]->account_name}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <input name="prod_code_c[]" value="{{$ledger[$i]->account_code}}" id="prod_code_c{{$id}}" class="form-control" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Reference No</label>
                                                                        <input readonly name="prod_transaction_no_c[]" value="{{$ledger[$i]->transaction_no}}" class="form-control" type="text"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Quantity</label>
                                                                        <input name="prod_credit[]" value="{{$ledger[$i]->stock_out}}" class="form-control" type="text"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input readonly name="prod_amount_c[]" value="{{$ledger[$i]->amount}}" id="prod_amount_c1" class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button disabled type='button' id="{{$id}}" class='btn btn-lg red delete_c' style="margin-top: 20px; margin-left:30px" ><i class='fa fa-trash'></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button disabled type='button' id="{{$id}}" class='btn btn-lg blue add_c' style="margin-top: 20px; margin-right: 20px" ><i class='fa fa-plus'></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @php
                                                            $id++;
                                                        @endphp
                                                        @endfor
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-1">
                                                        <button type="submit" class="btn green">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

<script>

    $(document).on('change','#account_code_d',function(){
        var id=$(this).val();
        $.ajax({
            url:"{{url('')}}/accountdetails/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                if(id == 'PUR-01-001')
                {
                    $('#prod_name_d1').show();
                    $('h5').last().addClass('h5');
                }
                else
                {
                    $('#prod_name_d1').hide();
                }

                $('#account_name_d').val(data.name_of_account);
            }
        });
    });

    $(document).on('change','#account_code_c',function(){
        var id=$(this).val();
        $.ajax({
            url:"{{url('')}}/accountdetails/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                if(id == 'RV-01-001')
                {
                    $('#prod_name_c1').show();
                }
                else
                {
                    $('#prod_name_c1').hide();
                }
                $('#account_name_c').val(data.name_of_account);
            }
        });
    });

    $(document).on('change','.prod_name_d',function(){
        var id=$(this).val();
        var ids = $(this).attr('id');
        $.ajax({
            url:"{{url('')}}/accountdetails/prodName/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $('#prod_code_d'+ids).val(data.Code);
            }
        });
    });

    $(document).on('change','.prod_name_c',function(){
        var id=$(this).val();
        var ids = $(this).attr('id');
        $.ajax({
            url:"{{url('')}}/accountdetails/prodName/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $('#prod_code_c'+ids).val(data.Code);
            }
        });
    });

    $(document).on('click','.add_d',function(){
        var id = $(this).attr('id');
        newid = +id + 1;
        $("#prod_name_d1").append('<div class="rowd'+newid+'"><h5 class="h5">Products</h5><div class="col-md-2"><div class="form-group"><label for="">Products Name</label><select class="form-control prod_name_d" data-live-search="true" required name="prod_name_d[]" id="'+newid+'" ><option value=""  selected>Select...</option> <?php foreach ($product as $key => $u) { ?> <option  value="<?php echo $u->name_of_account ?>" > <?php echo $u->name_of_account ?> </option> <?php } ?> </select></div></div><div class="col-md-2"><div class="form-group"><label for="">Account Code</label><input name="prod_code_d[]" id="prod_code_d'+newid+'" class="form-control" type="text" readonly ></div></div><div class="col-md-2"><div class="form-group"><label for="">Reference No</label><input name="prod_transaction_no_d[]"  class="form-control" type="text"  ></div></div><div class="col-md-2"><div class="form-group"><label for="">Quantity</label><input name="prod_debit[]"  class="form-control" type="text"  ></div></div><div class="col-md-2"><div class="form-group"><button type="button"  class="btn btn-lg red delete_d" id="'+newid+'" style="margin-top: 20px; margin-left:30px" ><i class="fa fa-trash"></i></button></div></div><div class="col-md-2"><div class="form-group"><button type="button" id="'+newid+'"  class="btn btn-lg blue add_d" style="margin-top: 20px; margin-right: 20px" ><i class="fa fa-plus"></i></button></div></div></div>');
    });

    $(document).on('click','.delete_d',function(){
        var id = $(this).attr('id');
        $(this).closest('.rowd'+id).remove();
        if(id == 1)
        {
            $('h5').last().removeClass('h5');
        }
    });

    $(document).on('click','.add_c',function(){
        var id = $(this).attr('id');
        newid = +id + 1;
        $("#prod_name_c1").append('<div class="rowc'+newid+'"><h5 class="h5">Products</h5><div class="col-md-2"><div class="form-group"><label for="">Products Name</label><select class="form-control prod_name_c" data-live-search="true" required name="prod_name_c[]" id="'+newid+'" ><option value=""  selected>Select...</option> <?php foreach ($product as $key => $u) { ?> <option  value="<?php echo $u->name_of_account ?>" > <?php echo $u->name_of_account ?> </option> <?php } ?> </select></div></div><div class="col-md-2"><div class="form-group"><label for="">Account Code</label><input name="prod_code_c[]" id="prod_code_c'+newid+'" class="form-control" type="text" readonly ></div></div><div class="col-md-2"><div class="form-group"><label for="">Reference No</label><input name="prod_transaction_no_c[]"  class="form-control" type="text"  ></div></div><div class="col-md-2"><div class="form-group"><label for="">Quantity</label><input name="prod_credit[]"  class="form-control" type="text"  ></div></div><div class="col-md-2"><div class="form-group"><button type="button"  class="btn btn-lg red delete_c" id="'+newid+'" style="margin-top: 20px; margin-left:30px" ><i class="fa fa-trash"></i></button></div></div><div class="col-md-2"><div class="form-group"><button type="button" id="'+newid+'"  class="btn btn-lg blue add_c" style="margin-top: 20px; margin-right: 20px" ><i class="fa fa-plus"></i></button></div></div></div>');
    });

    $(document).on('click','.delete_c',function(){
        var id = $(this).attr('id');
        $(this).closest('.rowc'+id).remove();
    });
</script>
@endsection
@endsection
