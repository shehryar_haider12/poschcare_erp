@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
        /* height: 400px; */
    }
</style>
<style>
    .hr {
    display: block;
    border-style: inset;
    border-width: 1px;
    margin-top: 10px;
    margin-bottom: 10px;
    }
    .h5
    {
        margin: 0 auto;
        position: relative;
        right: 1170px;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/accountdetails">General Ledger</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>General Ledger Entry</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-money font-white"></i>New Entry
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <form action="{{route('generalLedger.store')}} " class="form-horizontal" method="POST" >
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Source</label>
                                                            <input value="Manual" class="form-control" type="text" readonly name="source" >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Created By</label>
                                                            <input value="{{$u_name}}" class="form-control" type="text" readonly  >
                                                            <input value="{{$u_id}}" name="created_by" hidden >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Posted Date</label>
                                                            <input value="{{$date}}" name="posted_date" class="form-control" type="date" readonly >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Period</label>
                                                            <input value="{{old('period')}}" name="period" class="form-control" type="month" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-outline">
                                                            <label >Currency Code</label>
                                                            <input value="PKR" name="currency_code" class="form-control" type="text" readonly >
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="hr">
                                                <div class="custom_datatable">
                                                    <div class="tableview">
                                                        <div class="row">
                                                            <h5>Debit Entry</h5>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Head Category</label>
                                                                    <select class="form-control selectpicker" data-live-search="true" required id="head_d" >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($head as $u)
                                                                        <option  value="{{$u->code}}" {{old('head_d') == $u->code ? 'selected' : null}} >{{$u->code}} - {{$u->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Account Code</label>
                                                                    <select class="form-control " data-live-search="true" required name="account_code_d" id="account_code_d" >
                                                                        <option value=""  selected>Select...</option>
                                                                        {{-- @foreach ($account as $u)
                                                                        <option  value="{{$u->Code}}">{{$u->Code}}</option>
                                                                        @endforeach --}}
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Account Name</label>
                                                                    <input  name="account_name_d" id="account_name_d" class="form-control" type="text" readonly >
                                                                </div>
                                                            </div>
                                                               <!-- inner row end     -->
                                                          </div>
                                                           <!-- inner row end     -->
                                                            <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Accounting Date</label>
                                                                    <input name="accounting_date_d"  class="form-control" type="date"  >
                                                                    <span class="text-danger">{{$errors->first('accounting_date_d') ? 'Date must of same period' : null}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Reference No</label>
                                                                    <input value="{{ old('transaction_no_d')}}" name="transaction_no_d"  class="form-control" type="text"  >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Debit</label>
                                                                    <input autocomplete="off" name="debit_d" value="{{old('debit_d')}}" class="form-control" type="number" required >
                                                                    <input name="credit_d" value="0"  hidden >
                                                                </div>
                                                            </div>




                                                     <!-- mainrowend -->
                                                        </div>
                                                        <!-- mainrowend -->
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label for="">Description</label>
                                                                    <textarea name="description_d" rows="4" cols="80" required >{{ old('description_d')}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="prod_name_d1" hidden>
                                                            <div class="rowd1">
                                                                <h5>Products</h5>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Products Name</label>
                                                                        <select class="form-control selectpicker prod_name_d" data-live-search="true"  name="prod_name_d[]" id="1" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @foreach ($product as $u)
                                                                            <option  value="{{$u->name_of_account}}">{{$u->name_of_account}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <input name="prod_code_d[]" id="prod_code_d1" class="form-control" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Reference No</label>
                                                                        <input name="prod_transaction_no_d[]"   class="form-control" type="text"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Quantity</label>
                                                                        <input autocomplete="off" name="prod_debit[]"  class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input readonly name="prod_amount_d[]"  id="prod_amount_d1" class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button type='button' id="1" class='btn btn-lg red delete_d' style="margin-top: 20px; margin-left:30px" ><i class='fa fa-trash'></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button type='button' id="1" class='btn btn-lg blue add_d' style="margin-top: 20px; margin-right: 20px" ><i class='fa fa-plus'></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <h5>Credit Entry</h5>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Head Category</label>
                                                                    <select class="form-control selectpicker" data-live-search="true" required id="head_c" >
                                                                        <option value=""  selected>Select...</option>
                                                                        @foreach ($head as $u)
                                                                        <option  value="{{$u->code}}">{{$u->code}} - {{$u->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Account Code</label>
                                                                    <select class="form-control " data-live-search="true" required name="account_code_c" id="account_code_c" >
                                                                        <option value=""  selected>Select...</option>
                                                                        {{-- @foreach ($account as $u)
                                                                        <option  value="{{$u->Code}}">{{$u->Code}}</option>
                                                                        @endforeach --}}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Account Name</label>
                                                                    <input value="{{old('account_name_c')}}" name="account_name_c" id="account_name_c" class="form-control" type="text" readonly >
                                                                </div>
                                                            </div>
                                                            <!-- inner row end -->
                                                                        </div>
                                                            <!-- inner row end -->
                                                           <div class="row">
                                                                        <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Accounting Date</label>
                                                                    <input  name="accounting_date_c"  class="form-control" type="date"  >
                                                                    <span class="text-danger">{{$errors->first('accounting_date_c') ? 'Date must of same period' : null}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Reference No</label>
                                                                    <input value="{{old('transaction_no_c')}}" name="transaction_no_c"  class="form-control" type="text"  >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Credit</label>
                                                                    <input autocomplete="off" value="{{old('credit_c')}}" name="credit_c"  class="form-control" type="number" required >
                                                                    <input name="debit_c" value="0"  hidden >
                                                                </div>
                                                            </div>
                                                        <!-- main row end -->
                                                        </div>
                                                        <!-- main row end -->


                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label for="">Description</label>
                                                                    <textarea  name="description_c" rows="4" cols="80" required >{{old('description_c')}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="sales" hidden class="row">

                                                            <select multiple class="form-control "  id="sale_id" name="s_id[]" >

                                                            </select>
                                                        </div>

                                                        <div id="prod_name_c1" hidden>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="">Tax <small>(In %)</small> </label>
                                                                        <input value="{{old('tax')}}" name="tax" id="tax" autocomplete="off" class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="rowc1">
                                                                <h4>Products</h4>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Products Name</label>
                                                                        <select class="form-control selectpicker prod_name_c" data-live-search="true"  name="prod_name_c[]" id="1" >
                                                                            <option value=""  selected>Select...</option>
                                                                            @foreach ($product as $u)
                                                                            <option  value="{{$u->name_of_account}}">{{$u->name_of_account}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Account Code</label>
                                                                        <input name="prod_code_c[]" id="prod_code_c1" class="form-control" type="text" readonly >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Reference No</label>
                                                                        <input name="prod_transaction_no_c[]"  class="form-control" type="text"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Quantity</label>
                                                                        <input name="prod_credit[]"  class="form-control" type="number" autocomplete="off" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label for="">Amount</label>
                                                                        <input readonly  name="prod_amount_c[]" id="prod_amount_c1" class="form-control" type="number"  >
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button type='button' id="1" class='btn btn-lg red delete_c' style="margin-top: 20px; margin-left:30px" ><i class='fa fa-trash'></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <button type='button' id="1" class='btn btn-lg blue add_c' style="margin-top: 20px; margin-right: 20px" ><i class='fa fa-plus'></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-1 ">
                                                        <button type="submit" class="btn green">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

<script>
    $(document).on('change','#account_code_d',function(){
        var id=$(this).val();
        $.ajax({
            url:"{{url('')}}/accountdetails/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                if(id == 'PUR-01-001')
                {
                    $('#prod_name_d1').show();
                    $('h5').last().addClass('h5');
                }
                else
                {
                    $('#prod_name_d1').hide();
                }

                $('#account_name_d').val(data.name_of_account);
            }
        });
    });

    $(document).on('change','#account_code_c',function(){
        var id=$(this).val();
        var Regex='CA-01';
        var Regex1='NCA-01';
        $.ajax({
            url:"{{url('')}}/accountdetails/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                if(id.match(Regex) && !(id.match(Regex1)))
                {
                    $('#prod_name_c1').hide();
                    $.ajax({
                        url:"{{url('')}}/generalLedger/find/"+data.name_of_account,
                        method:"GET",
                        error: function (request1, error1) {
                                    alert(" Can't do because: " + error1 +request1);
                                },
                        success:function(dataa){
                            console.log(dataa);
                            if(dataa.length > 0)
                            {
                                $('#sale_id').empty();
                                $('#sale_id').selectpicker('destroy');
                                $('#sales').show();
                                $('#sale_id').append(`<option disabled >Select...</option> `);
                                for (let i = 0; i < dataa.length; i++) {
                                    if(dataa[i].total_amount == null)
                                    {
                                        total = 0;
                                    }
                                    else
                                    {
                                        total = dataa[i].total_amount;
                                    }
                                    total = dataa[i].total - total;
                                    $('#sale_id').append(`<option value='`+dataa[i].id+`'>Sale No: `+dataa[i].id+` - Total: `+total+`</option>`);
                                }
                                $('#sale_id').addClass('selectpicker');
                                $('.selectpicker').selectpicker('render');
                            }
                            else
                            {
                                alert('All dues are clear!');
                            }
                        }
                    });
                }
                else if(id == 'RV-01-001')
                {
                    $('#prod_name_c1').show();
                    $('#sales').hide();
                }
                else if(id.match(Regex1))
                {
                    $('#sales').hide();
                }
                else
                {
                    $('#sales').hide();
                    $('#prod_name_c1').hide();
                }

                $('#account_name_c').val(data.name_of_account);
            }
        });
    });

    $(document).on('change','#head_d',function(){
        var id=$(this).val();
        $("#account_code_d").empty();
        $('#account_code_d').selectpicker('destroy');
        $.ajax({
            url:"{{url('')}}/headcategory/account/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $("#account_code_d").append(' <option value=""  selected>Select...</option>');
                for(var i = 0 ; i < data.length ; i++)
                {
                    $("#account_code_d").append('<option value='+data[i].Code+'>'+data[i].Code+' - '+data[i].name_of_account+'</option>');
                }
                $('#account_code_d').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        });
    });

    $(document).on('change','#head_c',function(){
        var id=$(this).val();
        $("#account_code_c").empty();
        $('#account_code_c').selectpicker('destroy');

        $.ajax({
            url:"{{url('')}}/headcategory/account/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $("#account_code_c").append(' <option value=""  selected>Select...</option>');
                for(var i = 0 ; i < data.length ; i++)
                {
                    $("#account_code_c").append('<option value='+data[i].Code+'>'+data[i].Code+' - '+data[i].name_of_account+'</option>');
                }
                $('#account_code_c').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        });
    });

    $(document).on('change','.prod_name_d',function(){
        var id=$(this).val();
        var ids = $(this).attr('id');
        $.ajax({
            url:"{{url('')}}/accountdetails/prodName/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#prod_code_d'+ids).val(data[2].Code);
                $('#prod_amount_d'+ids).val(data[1].cost);
            }
        });
    });

    $(document).on('change','.prod_name_c',function(){
        var id=$(this).val();
        var ids = $(this).attr('id');
        $.ajax({
            url:"{{url('')}}/accountdetails/prodName/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#prod_code_c'+ids).val(data[0].Code);
                $('#prod_amount_c'+ids).val(data[1].cost);
            }
        });
    });

    $(document).on('click','.add_d',function(){
        var id = $(this).attr('id');
        var newid = +id + 1;
        // console.log(newid);
        $("#prod_name_d1").append(`<div class="rowd`+newid+`"><h5 class="h5">Products</h5><div class="col-md-2"><div class="form-group"><label for="">Products Name</label><select class="form-control prod_name_d" data-live-search="true" required name="prod_name_d[]" id="`+newid+`"><option value=""  selected>Select...</option> <?php foreach ($product as $key => $u) { ?> <option  value="<?php echo $u->name_of_account ?>" > <?php echo $u->name_of_account ?> </option> <?php } ?> </select></div></div><div class="col-md-2"><div class="form-group"><label for="">Account Code</label><input name="prod_code_d[]" id="prod_code_d`+newid+`" class="form-control" type="text" readonly ></div></div><div class="col-md-2"><div class="form-group"><label for="">Reference No</label><input name="prod_transaction_no_d[]"  class="form-control" type="text"  ></div></div><div class="col-md-2"><div class="form-group"><label for="">Quantity</label><input name="prod_debit[]"  class="form-control" type="number" autocomplete="off"  ></div></div><div class="col-md-2"><div class="form-group"><label for="">Amount</label><input name="prod_amount_d[]" id="prod_amount_d`+newid+`" readonly class="form-control" type="number"  ></div></div><div class="col-md-1"><div class="form-group"><button type="button"  class="btn btn-lg red delete_d" id="`+newid+`" style="margin-top: 20px; margin-left:30px" ><i class="fa fa-trash"></i></button></div></div><div class="col-md-1"><div class="form-group"><button type="button" id="`+newid+`"  class="btn btn-lg blue add_d" style="margin-top: 20px; margin-right: 20px" ><i class="fa fa-plus"></i></button></div></div></div>`);
    });

    $(document).on('click','.delete_d',function(){
        var id = $(this).attr('id');
        $(this).closest('.rowd'+id).remove();
        if(id == 1)
        {
            $('h5').last().removeClass('h5');
        }
    });

    $(document).on('click','.add_c',function(){
        var id = $(this).attr('id');
        newid = +id + 1;
        $("#prod_name_c1").append(`<div class="rowc`+newid+`"><h5 class="h5">Products</h5><div class="col-md-2"><div class="form-group"><label for="">Products Name</label><select class="form-control prod_name_c" data-live-search="true" required name="prod_name_c[]" id="`+newid+`" ><option value=""  selected>Select...</option> <?php foreach ($product as $key => $u) { ?> <option  value="<?php echo $u->name_of_account ?>" > <?php echo $u->name_of_account ?> </option> <?php } ?> </select></div></div><div class="col-md-2"><div class="form-group"><label for="">Account Code</label><input name="prod_code_c[]" id="prod_code_c`+newid+`" class="form-control" type="text" readonly ></div></div><div class="col-md-2"><div class="form-group"><label for="">Reference No</label><input name="prod_transaction_no_c[]"  class="form-control" type="text"  ></div></div><div class="col-md-2"><div class="form-group"><label for="">Quantity</label><input name="prod_credit[]"  class="form-control" type="number" autocomplete="off" ></div></div><div class="col-md-2"><div class="form-group"><label for="">Amount</label><input name="prod_amount_c[]" id="prod_amount_c`+newid+`" readonly class="form-control" type="number"  ></div></div><div class="col-md-1"><div class="form-group"><button type="button"  class="btn btn-lg red delete_c" id="`+newid+`" style="margin-top: 20px; margin-left:30px" ><i class="fa fa-trash"></i></button></div></div><div class="col-md-1"><div class="form-group"><button type="button" id="`+newid+`"  class="btn btn-lg blue add_c" style="margin-top: 20px; margin-right: 20px" ><i class="fa fa-plus"></i></button></div></div></div>`);
    });

    $(document).on('click','.delete_c',function(){
        var id = $(this).attr('id');
        $(this).closest('.rowc'+id).remove();
    });
</script>
@endsection
@endsection
