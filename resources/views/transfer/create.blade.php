@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/productTransfer">Product Transfer History</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>Product Transfer</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-map-marker font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Product Transfer</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{route('productTransfer.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Date*</label>
                                        <input value="{{$date}}" class="form-control" type="date" placeholder="Enter Date" name="transfer_date" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="from_w" id="from_w" required>
                                            <option value="" disabled selected>Select...</option>
                                            @foreach ($warehouse as $u)
                                            <option  value="{{$u->id}}">{{$u->w_name}} - {{$u->w_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Transfer To*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="to_w" id="to_w" required>
                                            <option value="" disabled selected>Select...</option>
                                            @foreach ($warehouse as $u)
                                            <option  value="{{$u->id}}">{{$u->w_name}} - {{$u->w_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Products*</label>
                                        <select class="form-control" data-live-search="true" name="p_id" id="p_id" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >In Warehouse*</label>
                                        <input readonly name="inhouse" class="form-control" type="number" id="in" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Transfer Quantity*</label>
                                        <input min="1"  class="form-control" type="number" placeholder="Enter Quantity" name="quantity" id="quantity" required>
                                    </div>
                                </div>
                                <input type="hidden" name="type" id="type">
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@endsection
@section('custom-script')
@toastr_js
@toastr_render

<script>
    $(document).on('change','#p_id',function(){
        var p_id = $(this).val();
        var w_id = $('#from_w').val();
        var id = p_id.substring(0,1);
        var type = p_id.substring(3);
        // console.log(type);
        $.ajax({
            url:"{{url('')}}/productTransfer/product",
            method:"GET",
            data:
            {
                p_id:id,
                w_id:w_id,
                type:type,
            },
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#in').val(data.quantity);
                $('#type').val(type);
                $('#quantity').attr('max',data.quantity);
            }
        });
    });

    $(document).on('change','#from_w',function(){
        var id = $(this).val();

        $("#p_id").empty();
        $('#p_id').selectpicker('destroy');
        $.ajax({
            url:"{{url('')}}/stocks/search/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $('#p_id').append('<option disabled selected>Select..</option>');
                for(var i = 0 ; i < data.length ; i++)
                {
                    if(data[i].type == 1)
                    {
                        $("#p_id").append('<option value='+data[i].p_id+'.'+data[i].type+'>'+data[i].variant.name+'</option>');
                    }
                    else
                    {
                        if(data[i].products.weight == null)
                        {
                            weight='';
                        }
                        else
                        {
                            weight = data[i].products.weight ;
                        }
                        $("#p_id").append('<option value='+data[i].p_id+'.'+data[i].type+'>'+data[i].products.pro_code+' - '+data[i].products.pro_name+' - '+weight+data[i].products.unit.u_name+'- '+data[i].products.brands.b_name+' - '+data[i].products.category.cat_name+'</option>');
                    }
                }
                $('#p_id').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            }
        });
    });
</script>
@endsection
