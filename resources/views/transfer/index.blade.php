@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
       
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/productTransfer">Product Transfer History</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">

                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-map-marker font-white"></i>Product Transfer History
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    @if ($index == 0)
                                                        <a style="margin-left:-10px"  href="{{route('productTransfer.excel')}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 1)
                                                        <a style="margin-left:-10px"  href="{{url('')}}/productTransfer/search/{{$year}}/{{$p_name}}/{{$types}}/{{$fw}}/{{$tw}}/excel/year">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 2)
                                                        <a style="margin-left:-10px"  href="{{url('')}}/productTransfer/search/{{$month}}/{{$p_name}}/{{$types}}/{{$fw}}/{{$tw}}/excel/month">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 4)
                                                        <a style="margin-left:-10px"  href="{{url('')}}/productTransfer/search/{{$date}}/{{$p_name}}/{{$types}}/{{$fw}}/{{$tw}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 3)
                                                        <a style="margin-left:-10px"  href="{{url('')}}/productTransfer/search/{{$from}}/{{$to}}/{{$p_name}}/{{$types}}/{{$fw}}/{{$tw}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                {{-- <a style="margin-left:-50px;" href="{{route('productTransfer.pdf')}}">
                                                    <i class="fa fa-file-pdf-o  font-white"></i>
                                                </a> --}}
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('productTransfer.create')}}">
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button" class="btn btn-block btn-primary btn-md ">Transfer Product</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/productTransfer/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <select class="form-control " disabled id="p_name" data-live-search="true" name="p_name" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($products as $s)
                                                                    @if ($s->vstatus == 0)
                                                                        <option value="{{$s->id.'-'.'0'}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                    @else
                                                                        @foreach ($s->variants as $v)
                                                                            <option value="{{$v->id.'-'.'1'}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From Warehouse</label>
                                                            <select class="form-control" id="fw" disabled data-live-search="true" name="fw" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($warehouse as $u)
                                                                <option  value="{{$u->id}}">{{$u->w_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To Warehouse</label>
                                                            <select class="form-control" id="tw" disabled data-live-search="true" name="tw" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($warehouse as $u)
                                                                <option  value="{{$u->id}}">{{$u->w_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Transfer Date</th>
                                                            <th>From Warehouse</th>
                                                            <th>To Warehouse</th>
                                                            <th>Code - Product Name</th>
                                                            <th>Requested Quantity</th>
                                                            {{-- <th>Weight</th>
                                                            <th>Brand</th>
                                                            <th>Category</th> --}}
                                                            <th>In House Quantity</th>
                                                            {{-- <th>Cost</th>
                                                            <th>Price</th> --}}
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a=1;
                                                        @endphp
                                                        @foreach ($product as $p)
                                                            <tr>
                                                                <td>{{$a}}</td>
                                                                <td>{{$p->transfer_date}}</td>
                                                                <td>{{$p->fromWarehouse->w_name}}</td>
                                                                <td>{{$p->toWarehouse->w_name}}</td>
                                                                @if ($p->type == 1)
                                                                    <td>{{$p->variant->name}} </td>
                                                                @else
                                                                    <td>{{$p->products->pro_code}} - {{$p->products->pro_name}}</td>
                                                                @endif
                                                                <td>{{$p->quantity}}</td>
                                                                <td>{{$p->currentStock->quantity}}</td>
                                                                <input type="hidden" id="inhouse{{$p->id}}" value="{{$p->currentStock->quantity}}">
                                                                <input type="hidden" id="quantity{{$p->id}}" value="{{$p->quantity}}">
                                                                <td>
                                                                    @if ($p->status == 1)
                                                                        <input class="status btn btn-success" disabled type="button" value="Approved" id="{{$p->id}}">
                                                                    @else
                                                                        <input class="status btn btn-danger" {{in_array('status',$permissions) ? '' : 'disabled'}} type="button"  value="Pending" id="{{$p->id}}">
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                scrollX: true,
            });
            $('input:radio[name="optradio"]').change(function(){
                if ($(this).is(':checked')) {
                    $('#search').prop('disabled',false);
                    var val = $(this).val();
                    if(val == 'Year')
                    {
                        $('#year').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#fw').prop('disabled',false);
                        $('#tw').prop('disabled',false);
                        $('#year').attr('required',true);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Month')
                    {
                        $('#month').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#fw').prop('disabled',false);
                        $('#tw').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                    if(val == 'Date')
                    {
                        $('#from').prop('disabled',false);
                        $('#from').attr('required',true);
                        $('#to').prop('disabled',false);
                        $('#to').attr('required',true);
                        $('#p_name').prop('disabled',false);
                        $('#fw').prop('disabled',false);
                        $('#tw').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#month').prop('disabled',true);
                        $('#year').attr('required',false);
                        $('#p_name').addClass('selectpicker');
                        $('.selectpicker').selectpicker('render');
                    }
                }
            });


            $('.status').click(function () {
            var $this = $(this);
            var id = $(this).attr("id");
            var status = $(this).val();
            var inhouse = $('#inhouse'+id).val();
            var quantity = $('#quantity'+id).val();

            // console.log(status);
            axios
                .post('{{route("productTransfer.status")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                id: id,
                status: status,
                inhouse: inhouse,
                quantity: quantity,
                })
                .then(function (responsive) {
                console.log('responsive');
                location.reload();
                })
                .catch(function (error) {
                    alert('Requested quantity is greater than in warehouse');
                    location.reload();
                });
            });
        });
</script>
    @endsection
@endsection
