@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="#">POS</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">

                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-exchange font-white"></i> &nbsp; POS Opening & Closing
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Date</th>
                                                            <th>Counter NO</th>
                                                            <th>Total Sale</th>
                                                            <th>Total Return</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $count = 1;
                                                            $status = false;
                                                        @endphp
                                                        @if (count($sales) >= count($return))
                                                            @foreach ($sales as $s)
                                                                <tr>
                                                                    @if (count($return) > 0)
                                                                        @foreach ($return as $r)
                                                                            @if ($s->date == $r->date && $s->co_id == $r->co_id)
                                                                                <td>{{$count}}</td>
                                                                                <td>{{$r->date}}</td>
                                                                                <td>{{$r->co_id}}</td>
                                                                                <td>{{$s->total}}</td>
                                                                                <td>{{$r->total}}</td>
                                                                                <td>

                                                                                    <button type="button"  class="btn green details" id="{{$s->co_id}}">
                                                                                        <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                            Generate Invoice
                                                                                        </a>
                                                                                    </button>
                                                                                </td>
                                                                            @break
                                                                            @else
                                                                                @if ($s->ret_id == null)

                                                                                    <td>{{$count}}</td>
                                                                                    <td>{{$s->date}}</td>
                                                                                    <td>{{$s->co_id}}</td>
                                                                                    <td>{{$s->total}}</td>
                                                                                    <td>No Return</td>
                                                                                    <td>

                                                                                        <button type="button"  class="btn green details" id="{{$s->co_id}}">
                                                                                            <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                                Generate Invoice
                                                                                            </a>
                                                                                        </button>
                                                                                    </td>
                                                                                @else
                                                                                    <td>{{$count}}</td>
                                                                                    <td>{{$r->date}}</td>
                                                                                    <td>{{$r->co_id}}</td>
                                                                                    <td>No Sale</td>
                                                                                    <td>{{$r->total}}</td>
                                                                                    <td>

                                                                                        <button type="button" class="btn green details" id="{{$r->co_id}}">
                                                                                            <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                                Generate Invoice
                                                                                            </a>
                                                                                        </button>
                                                                                    </td>
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        <td>{{$count}}</td>
                                                                        <td>{{$s->date}}</td>
                                                                        <td>{{$s->co_id}}</td>
                                                                        <td>{{$s->total}}</td>
                                                                        <td>No Return</td>
                                                                        <td>

                                                                            <button type="button" class="btn green details" id="{{$s->co_id}}">
                                                                                <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                    Generate Invoice
                                                                                </a>
                                                                            </button>
                                                                        </td>
                                                                    @endif

                                                                    @php
                                                                        $count ++;
                                                                    @endphp

                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            @foreach ($return as $s)
                                                            <tr>
                                                                @if (count($sales) > 0)
                                                                    @foreach ($sales as $r)
                                                                        @if ($s->date == $r->date && $s->co_id == $r->co_id)
                                                                            <td>{{$count}}</td>
                                                                            <td>{{$s->date}}</td>
                                                                            <td>{{$s->co_id}}</td>
                                                                            <td>{{$s->total}}</td>
                                                                            <td>{{$r->total}}</td>
                                                                            <td>

                                                                                <button type="button"  class="btn green details" id="{{$r->co_id}}">
                                                                                    <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                        Generate Invoice
                                                                                    </a>
                                                                                </button>
                                                                            </td>
                                                                        @break
                                                                        @else
                                                                            @if ($s->s_id == null)

                                                                                <td>{{$count}}</td>
                                                                                <td>{{$s->date}}</td>
                                                                                <td>{{$s->co_id}}</td>
                                                                                <td>No Sale</td>
                                                                                <td>{{$r->total}}</td>
                                                                                <td>

                                                                                    <button type="button" class="btn green details" id="{{$r->co_id}}">
                                                                                        <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                            Generate Invoice
                                                                                        </a>
                                                                                    </button>
                                                                                </td>
                                                                            @else
                                                                                <td>{{$count}}</td>
                                                                                <td>{{$s->date}}</td>
                                                                                <td>{{$s->co_id}}</td>
                                                                                <td>{{$s->total}}</td>
                                                                                <td>No Return</td>
                                                                                <td>

                                                                                    <button type="button"   class="btn green details" id="{{$s->co_id}}">
                                                                                        <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                                            Generate Invoice
                                                                                        </a>
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <td>{{$count}}</td>
                                                                <td>{{$r->date}}</td>
                                                                <td>{{$r->co_id}}</td>
                                                                <td>No Sale</td>
                                                                <td>{{$r->total}}</td>
                                                                <td>

                                                                    <button type="button"   class="btn green details" id="{{$r->co_id}}">
                                                                        <a   href="{{url('')}}/stockout/PosInvoice/{{$s->co_id}}" >
                                                                            Generate Invoice
                                                                        </a>
                                                                    </button>
                                                                </td>
                                                            @endif

                                                            @php
                                                                $count ++;
                                                            @endphp

                                                        </tr>
                                                    @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render

    @endsection
@endsection
