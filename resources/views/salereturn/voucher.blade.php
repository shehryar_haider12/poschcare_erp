<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
            width: 88%;
            border-collapse: collapse;
            border: 1px solid #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            margin-left: 100px;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
            margin-top: 20px;
            /* border: 4px solid #000000 !important; */
            display: block;
            margin-left: 200px;
            margin-right: 100px;
        }
        .label
        {
            font-size: 30px;
            color: #28AE01;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
            border: 4px solid #000000 !important;
            display: block;
            margin-top: 20px;
            margin-left: 100px;
            margin-right: 100px;
            text-align: center;
        }
        .row
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            /* padding-left: 30px; */
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A4;
        }
        body
        {
            /* margin-left: 500px; */
            /* margin-right: 500px; */
            /* text-align: center; */
        }
  </style>
  <body >
        <header>
            <img src="{{url('')}}/uploads/posch.jpg" height="80px" width="400px" style="margin-top:10px" alt="logo" class="logo-default"  />
        </header>
    <div class="label">Goods Return-Voucher</div>


    <div class="form-body">
        <div class="row">
            <div>
                <div style="margin-left: 100px" class="form-outline">
                    <p><b>Date: {{\Carbon\Carbon::parse($salereturn->return_date)->format('d-m-Y')}}</b></p>
                    <p><b>Voucher No: {{$salereturn->id}}</b></p>
                    <p><b>Sale No: {{$salereturn->sale_id}}</b></p>
                </div>
            </div>
            <div style="margin-top: -120px; margin-left: 400px">
                <div class="form-outline">
                    <p><b>Customer ID: {{$salereturn->c_id}} </b></p>
                    <p><b>Customer Name: {{$salereturn->customer->name}}</b></p>
                    <p><b>Customer Number: {{$salereturn->customer->c_no}} </b></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div style="margin-left: 100px" class="form-outline">
                    <p><b>Sale Person ID: {{$salereturn->sales->sp_id}}</b></p>
                    <p><b>Sale Person Name: {{$salereturn->sales->saleperson->name}}</b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="attendance-table" style="margin-top: 25px">
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align: center"  class="attendance-cell">S.NO</th>
                    <th class="attendance-cell" style="text-align: center">Product Code-Name</th>
                    <th class="attendance-cell" style="text-align: center">Damaged Qty</th>
                    <th class="attendance-cell" style="text-align: center">Usable Qty</th>
                    <th class="attendance-cell" style="text-align: center">Rate</th>
                    <th class="attendance-cell" style="text-align: center">Sub-Total</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                    $amount = 0;
                    $Dqty = 0;
                    $Nqty = 0;
                @endphp
                @foreach ($salereturnd as $d)
                    @php
                        $amount = $d->saledetail->sub_total / $d->saledetail->quantity;
                        if($d->Dquantity != 0)
                        {
                            $Dqty+= $d->Dquantity * $amount;
                        }
                        else
                        {
                            $Dqty=0;
                        }
                        if($d->Nquantity != 0)
                        {
                            $Nqty+= $d->Nquantity * $amount;
                        }
                        else
                        {
                            $Nqty=0;
                        }
                        $total += $Nqty + $Dqty;

                    @endphp
                    <tr>
                        <td class="attendance-cell" style="text-align: center">{{$d->id}}</td>
                        @if ($d->type == 1)
                            <td class="attendance-cell" style="text-align: center">{{$d->saledetail->variant->name}}</td>
                        @else
                        <td class="attendance-cell" style="text-align: center">{{$d->saledetail->products->pro_code}} - {{$d->saledetail->products->pro_name}}</td>
                        @endif
                        <td class="attendance-cell" style="text-align: center">{{$d->Dquantity == 0 ? '-' : $d->Dquantity}}</td>
                        <td class="attendance-cell" style="text-align: center">{{$d->Nquantity == 0 ? '-' : $d->Nquantity}}</td>
                        <td class="attendance-cell" style="text-align: center">{{$d->saledetail->sub_total / $d->saledetail->quantity}}</td>
                        <td class="attendance-cell" style="text-align: center">{{$Nqty + $Dqty}}</td>
                    </tr>
                    @php
                        $amount = 0;
                        $Dqty = 0;
                        $DNqty = 0;
                    @endphp

                @endforeach
                <tr>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell"></td>
                    <td  class="attendance-cell" style="text-align: center">TOTAL</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell" style="text-align: center">{{$total}}</td>
                </tr>
            </tbody>
        </table>


    </div>
    <br>
    <br>
    <br>
    <div class="row">
        <hr color="black" style="width:20%;display: block;border-style: inset;border-width: 1px; margin-right: 530px">
        <div  style="margin-left: 120px" class="form-outline">
            <p> Accountant Sign</p>
        </div>
    </div>
    <div class="row" style="margin-top: -80px; margin-left: 320px">
        <hr color="black" style="width:35%;display: block;border-style: inset;border-width: 1px; margin-right: 450px">
        <div class="form-outline" style="margin-left: 20px">
            <p> Warehouse Stamp</p>
        </div>
    </div>
    <div class="row" style="margin-top: -80px; margin-left: 540px">
        <hr color="black" style="width:65%;display: block;border-style: inset;border-width: 1px; margin-right: 400px">
        <div class="form-outline" style="margin-left: 20px">
            <p> Delivery Agent Sign</p>
        </div>
    </div>

  </body>
</html>
