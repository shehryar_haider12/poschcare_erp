@extends('layouts.master')
@section('top-styles')
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="{{url('')}}/sales/return/view">Sale Returns</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-credit-card font-white"></i>View Sale Returns
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Customer Name</label>
                                                            <select class="form-control selectpicker" multiple data-live-search="true" id="c_id"  >
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                @foreach ($customer as $u)
                                                                <option  value="{{$u->name}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Return Status</label>
                                                            <select id="status" multiple class="form-control selectpicker" >
                                                                <option value="Not All">Unselect All</option>
                                                                <option value="All">Select All</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        @php
                                                            $total = 0;
                                                        @endphp
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Sale No</th>
                                                            <th>Return Date</th>
                                                            <th>Warehouse</th>
                                                            <th>Customer</th>
                                                            <th>Total</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($return as $s)
                                                        {{-- @if ($counter == null) --}}
                                                            <tr>
                                                                <td>
                                                                    {{$s->id}}
                                                                </td>
                                                                <td>
                                                                    {{$s->sale_id}}
                                                                </td>
                                                                <td>
                                                                    {{$s->return_date}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->customer->name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->total}}
                                                                </td>
                                                                @if($s->status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                            {{$s->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($s->status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                            {{$s->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    @if ($s->status == 'Approved')
                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Sale Return Details</option>
                                                                            @endif
                                                                            <option >Generate Voucher</option>
                                                                        </select>
                                                                    @else
                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('status',$permissions))
                                                                                <option >Approved</option>
                                                                            @endif
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Sale Return Details</option>
                                                                            @endif
                                                                        </select>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        {{-- @else
                                                            @php
                                                                $uid = Auth::user()->id;
                                                                $sale =  \App\SaleCounter::where('u_id',$uid)->where('co_id',$counter)->get();
                                                            @endphp
                                                            @foreach ($sale as $ss)
                                                                @if ($ss->ret_id == $s->id)
                                                                    <tr>
                                                                        <td>
                                                                            {{$s->id}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->return_date}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->warehouse->w_name}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->customer->name}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->total}}
                                                                        </td>
                                                                        @if($s->status=='Pending')
                                                                            <td>
                                                                                <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                    {{$s->status}}
                                                                                </button>
                                                                            </td>
                                                                        @endif
                                                                        @if($s->status=='Approved')
                                                                            <td>
                                                                                <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                    {{$s->status}}
                                                                                </button>
                                                                            </td>
                                                                        @endif
                                                                        <td>
                                                                            @if ($s->status == 'Approved')
                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Sale Return Details</option>
                                                                                    @endif
                                                                                    <option >Generate Voucher</option>
                                                                                </select>
                                                                            @else
                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('status',$permissions))
                                                                                        <option >Approved</option>
                                                                                    @endif
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Sale Return Details</option>
                                                                                    @endif
                                                                                </select>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif --}}
                                                        @php
                                                            $total+=$s->total;
                                                        @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td>{{$total}}</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')


        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Sale Return Details</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Return Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="return_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="c_name" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Code - Name</th>
                                            <th>Brand</th>
                                            <th>Damaged Qty</th>
                                            <th>Usable Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @section('custom-script')

    @toastr_js
    @toastr_render
<script type="text/javascript">

    $(document).ready(function () {
        var table = $('#example').DataTable({
            scrollX: true,
            order: [[ 0, "desc" ]]
        });
        $('#c_id').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#c_id option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#c_id').selectpicker('deselectAll');
                $('#c_id').selectpicker('refresh');
            }
            $.each($('#c_id option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(4).search(search, true, false).draw();
        });
        $('#status').on('change', function(){
            var search = [];
            if($(this).get(0).value == 'All')
            {
                $('#status option:not(:eq(0))').prop('selected',true);

            }
            if($(this).get(0).value == 'Not All')
            {
                $('#status').selectpicker('deselectAll');
                $('#status').selectpicker('refresh');
            }
            $.each($('#status option:selected'), function(){
                search.push($(this).val());
            });

            search = search.join('|');
            table.column(6).search(search, true, false).draw();
        });

        $(document).on('change','.action',function(){
            var val=$(this).val();
            if( val == 'Approved')
            {
                var id = $(this).attr('id');
                var status = val;
                axios
                .post('{{route("sales.returnstatus")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                });
            }
            if(val == 'Sale Return Details')//
            {

                var id=$(this).attr("id");
                $("#example1 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/sales/return/show/"+id,
                    method:"GET",
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        $('#return_date').val(data[0].return_date);
                        $('#w_name').val(data[0].warehouse.w_name);
                        $('#c_name').val(data[0].customer.name);
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].type == 1)
                            {
                                $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].saledetail.variant.name+"</td><td>"+data[1][i].saledetail.variant.product.brands.b_name+"</td> <td>"+data[1][i].Dquantity+"</td><td>"+data[1][i].Nquantity+"</td></tr>");
                            }
                            else
                            {
                                $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].saledetail.products.pro_code+" - "+data[1][i].saledetail.products.pro_name+"</td><td>"+data[1][i].saledetail.products.brands.b_name+"</td>  <td>"+data[1][i].Dquantity+"</td><td>"+data[1][i].Nquantity+"</td></tr>");
                            }
                        }
                        $('#myModal').modal("show");
                        $('.action').val('Actions');
                        $('#example1').DataTable();

                    }
                });
            }

            if( val == 'Generate Voucher')
            {
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/sales/return/voucher/"+id,
                    method:"GET",
                    data:
                    {
                        id:id,
                    },
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        window.location.href= '{{url('')}}/sales/return/voucher/'+id;
                        $('.action').val('Actions');
                    }
                });
            }
        });
    });

</script>

    @endsection
@endsection
