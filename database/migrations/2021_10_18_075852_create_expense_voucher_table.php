<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_voucher', function (Blueprint $table) {
            $table->id();
            $table->date('v_date');
            $table->integer('c_id');
            $table->string('sc_id');
            $table->string('description', 100);
            $table->string('status', 15)->default("Pending");
            $table->double('amount');
            $table->unsignedBigInteger('prepared_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_voucher');
    }
}
