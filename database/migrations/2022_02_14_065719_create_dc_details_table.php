<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDcDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dc_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('s_id')->nullable();
            $table->bigInteger('p_id')->nullable();
            $table->string('quantity')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('price')->nullable();
            $table->timestamps();
            $table->string('delivered_quantity')->nullable();
            $table->string('discount_percent')->nullable();
            $table->string('discounted_amount')->nullable();
            $table->integer('type')->nullable();
            $table->string('cost')->nullable();
            $table->string('vet')->nullable();
            $table->string('afterDiscount')->nullable();
            $table->string('taxA')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dc_details');
    }
}
