<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_ledger', function (Blueprint $table) {
            $table->id();
            $table->string('source');
            $table->string('description')->nullable();
            $table->string('account_name');
            $table->string('link_id');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->date('accounting_date')->nullable();
            $table->date('posted_date');
            $table->string('period');
            $table->string('account_code');
            $table->string('transaction_no')->nullable();
            $table->string('currency_code')->default('PKR');
            $table->double('debit')->nullable();
            $table->double('credit')->nullable();
            $table->double('net_value')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_ledger');
    }
}
