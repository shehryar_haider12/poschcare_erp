<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dc', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->string('ref_no')->nullable();
            $table->bigInteger('b_id')->nullable();
            $table->bigInteger('c_id')->nullable();
            $table->bigInteger('w_id')->nullable();
            $table->string('s_address')->nullable();
            $table->string('d_status')->nullable()->default('Pending');
            $table->string('p_status')->nullable()->default('Pending');
            $table->string('doc')->nullable();
            $table->string('total')->nullable();
            $table->longText('note')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->bigInteger('sp_id')->nullable();
            $table->date('expected_date')->nullable();
            $table->string('advance')->nullable();
            $table->string('pay_type')->nullable();
            $table->string('Iformat')->nullable();
            $table->string('Itype')->nullable();
            $table->string('Ino')->nullable();
            $table->date('pdate')->nullable();
            $table->longText('remarks')->nullable();
            $table->bigInteger('st_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dc');
    }
}
