<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrentStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_stock', function (Blueprint $table) {
            $table->unsignedBigInteger('p_id')->nullable();
            $table->foreign('p_id')->references('id')->on('products');
            $table->unsignedBigInteger('w_id')->nullable();
            $table->foreign('w_id')->references('id')->on('warehouse');
            $table->double('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_stock');
    }
}
