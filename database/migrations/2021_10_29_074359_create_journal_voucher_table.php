<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_voucher', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->string('status', 100)->nullable()->default('Pending');
            $table->string('account1', 255)->nullable();
            $table->string('account2', 255)->nullable();
            $table->string('amount', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('month', 255)->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_voucher');
    }
}
