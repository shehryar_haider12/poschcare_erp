<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOutRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_out_request_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('r_id');
            // $table->foreign('o_id')->references('id')->on('purchase_request');
            $table->bigInteger('p_id');
            // $table->foreign('p_id')->references('id')->on('products');
            $table->bigInteger('quantity');
            $table->bigInteger('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_out_request_details');
    }
}
