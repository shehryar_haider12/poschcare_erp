<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsUpdateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_update_request', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->unsignedBigInteger('c_group')->nullable();
            $table->unsignedBigInteger('p_group')->nullable();
            $table->string('v_type');
            $table->string('company')->nullable();
            $table->string('address');
            $table->string('name');
            $table->bigInteger('c_no');
            $table->string('country')->nullable();
            $table->string('NTN')->nullable();
            $table->string('GST')->nullable();
            $table->string('state')->nullable();
            $table->string('email')->nullable();
            $table->bigInteger('postalCode')->nullable();
            $table->unsignedBigInteger('c_id')->nullable();
            $table->foreign('c_id')->references('id')->on('city');
            $table->foreign('id')->references('id')->on('vendors');
            $table->foreign('c_group')->references('id')->on('groups');
            $table->foreign('p_group')->references('id')->on('groups');
            $table->string('status')->nullable()->default('Pending');
            $table->string('name2')->nullable();
            $table->string('password')->nullable();
            $table->bigInteger('c_no2')->nullable();
            $table->unsignedBigInteger('u_id')->nullable();
            $table->foreign('u_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors_update_request');
    }
}
