<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('c_group')->nullable();
            $table->unsignedBigInteger('p_group')->nullable();
            $table->string('v_type');
            $table->string('company')->nullable();
            $table->string('name');
            $table->string('address');
            $table->bigInteger('c_no');
            $table->string('country')->nullable();
            $table->string('VAT')->nullable();
            $table->string('GST')->nullable();
            $table->string('state')->nullable();
            $table->string('email')->nullable();
            $table->bigInteger('postalCode')->nullable();
            $table->unsignedBigInteger('c_id')->nullable();
            $table->foreign('c_id')->references('id')->on('city');
            $table->foreign('c_group')->references('id')->on('groups');
            $table->foreign('p_group')->references('id')->on('groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
