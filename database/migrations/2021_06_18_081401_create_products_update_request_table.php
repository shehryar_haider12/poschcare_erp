<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsUpdateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_update_request', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('pro_name');
            $table->string('pro_code');
            $table->double('weight')->nullable();
            $table->double('cost')->nullable();
            $table->double('price')->nullable();
            $table->bigInteger('alert_quantity');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('cat_id')->nullable();
            $table->unsignedBigInteger('s_cat_id')->nullable();
            $table->foreign('cat_id')->references('id')->on('category');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('id')->references('id')->on('products');
            $table->foreign('s_cat_id')->references('id')->on('sub_category');
            $table->foreign('unit_id')->references('id')->on('units');
            $table->string('status')->nullable()->default('Pending');
            $table->string('image')->nullable();
            $table->string('description')->nullable();
            $table->Integer('visibility')->default(1);
            $table->string('p_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_update_request');
    }
}
