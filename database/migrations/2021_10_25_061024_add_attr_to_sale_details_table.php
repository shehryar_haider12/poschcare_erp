<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttrToSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            $table->string('vet', 255)->nullable()->comment('value excluding tax');
            $table->string('afterDiscount', 255)->nullable()->comment('afterDiscount');
            $table->string('taxA', 255)->nullable()->comment('tax in amount');
            $table->string('Itype', 255)->nullable()->comment('invoice type mrp/tp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            //
        });
    }
}
