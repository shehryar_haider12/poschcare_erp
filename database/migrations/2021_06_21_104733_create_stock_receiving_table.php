<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockReceivingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_receiving', function (Blueprint $table) {
            $table->id();
            $table->string('size')->nullable();
            $table->string('quantity')->nullable();
            $table->unsignedBigInteger('s_id')->nullable();
            $table->foreign('s_id')->references('id')->on('stocks');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_receiving');
    }
}
