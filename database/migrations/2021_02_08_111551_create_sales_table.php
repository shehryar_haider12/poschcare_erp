<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('sale_date');
            $table->string('ref_no');
            $table->unsignedBigInteger('b_id');
            $table->foreign('b_id')->references('id')->on('vendors');
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('vendors');
            $table->unsignedBigInteger('w_id');
            $table->foreign('w_id')->references('id')->on('warehouse');
            $table->string('s_address')->nullable();
            $table->string('s_status')->default('Pending');
            $table->string('p_status');
            $table->string('doc')->nullable();
            $table->double('tax')->nullable();
            $table->double('discount')->nullable();
            $table->double('total');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
