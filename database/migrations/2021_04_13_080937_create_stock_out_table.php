<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_out', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('p_id');
            $table->foreign('p_id')->references('id')->on('products');
            $table->double('quantity');
            $table->date('stock_date');
            $table->string('s_type');
            $table->string('price');
            $table->unsignedBigInteger('w_id');
            $table->foreign('w_id')->references('id')->on('warehouse');
            $table->unsignedBigInteger('sale_d_id')->nullable();
            $table->foreign('sale_d_id')->references('id')->on('sale_details');
            $table->string('gdn_no')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_out');
    }
}
