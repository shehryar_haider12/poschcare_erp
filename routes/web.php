<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cacheclear',function(){
    \Artisan::call('config:clear');
    \Artisan::call('config:cache');
    \Artisan::call('route:clear');
    \Artisan::call('view:clear');
    \Artisan::call('cache:clear');
    return 'Clear  Cache';
});


Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});
Route::get('admin-logout', 'UserController@adminLogout')->name('admin_logout');

//trialBalance
////////////////// dashboard and report//////////////////////
Route::get('/pos-opening-closing','HomeController@POS_OC')->name('pos.oc');
Route::get('/dashboard','HomeController@index')->name('dashboard');
Route::get('/pos-detail','HomeController@pos')->name('pos');
Route::get('/pos-detail/product/{id}','HomeController@posProduct')->name('pos.product');
Route::get('/pos-detail/product/code/{code}','HomeController@posProductCode')->name('pos.product.code');
Route::get('/price-checker','HomeController@priceChecker')->name('price-checker');
Route::get('/trialBalance','HomeController@trialBalance')->name('trialBalance');
Route::get('/trialBalance/excel','HomeController@trialBalanceExcel')->name('trialBalance.excel');
Route::get('/trialBalance/search/{lm}/{trial}/{period}','HomeController@trialBalanceSearchExcel')->name('trialBalance.search.excel');
Route::post('/trialBalance/search','HomeController@trialBalanceSearch')->name('trialBalance.search');

///////////////// Home controller incomeStatement Reports Route //////////////////////////////

Route::post('/reports/incomeStatementsearch','HomeController@incomeStatementSearch')->name('reports.incomeStatementsearch');
Route::get('/reports/incomeStatement','HomeController@incomeStatement')->name('reports.incomeStatement');
Route::get('/reports/incomeStatementsearch/{M}/excel/month','HomeController@incomeStatementSearchMExcel')->name('reports.incomeStatementsearchMExcel');//excel for month
Route::get('/reports/incomeStatementsearch/{M}/pdf/month','HomeController@incomeStatementSearchMPDF')->name('reports.incomeStatementsearchMpdf');//pdf for month
Route::get('/reports/incomeStatementsearch/{Y}/excel/year','HomeController@incomeStatementSearchYExcel')->name('reports.incomeStatementsearchYExcel');//excel for year
Route::get('/reports/incomeStatementsearch/{Y}/pdf/year','HomeController@incomeStatementSearchYPDF')->name('reports.incomeStatementsearchYpdf');//pdf for year
Route::get('/reports/incomeStatement/excel','HomeController@incomeStatementExcel')->name('reports.incomeStatement.excel');
Route::get('/reports/incomeStatement/pdf','HomeController@incomeStatementPDF')->name('reports.incomeStatement.pdf');
Route::get('/reports/incomeStatementsearch/{from}/{to}/excel/dates','HomeController@incomeStatementSearchDExcel')->name('reports.incomeStatementsearchDExcel');//excel for year
Route::get('/reports/incomeStatementsearch/{from}/{to}/pdf/dates','HomeController@incomeStatementSearchDPDF')->name('reports.incomeStatementsearchDpdf');//pdf for year

///////////////// Home controller sale costing Reports Route //////////////////////////////

Route::post('/reports/saleCostingsearch','HomeController@saleCostingSearch')->name('reports.saleCostingsearch');
Route::get('/reports/salesCosting','HomeController@salesCosting')->name('reports.salesCosting');
Route::get('/reports/saleCosting/excel','HomeController@saleCostingExcel')->name('reports.saleCosting.excel');
Route::get('/reports/saleCosting/pdf','HomeController@saleCostingPDF')->name('reports.saleCosting.pdf');
Route::get('/reports/saleCostingsearch/{M}/excel/month','HomeController@saleCostingSearchMExcel')->name('reports.saleCostingsearchMExcel');//excel for month
Route::get('/reports/saleCostingsearch/{M}/pdf/month','HomeController@saleCostingSearchMPDF')->name('reports.saleCostingsearchMpdf');//pdf for month
Route::get('/reports/saleCostingsearch/{Y}/excel/year','HomeController@saleCostingSearchYExcel')->name('reports.saleCostingsearchYExcel');//excel for year
Route::get('/reports/saleCostingsearch/{Y}/pdf/year','HomeController@saleCostingSearchYPDF')->name('reports.saleCostingsearchYpdf');//pdf for year
Route::get('/reports/saleCostingsearch/{D}/excel/date','HomeController@saleCostingSearchDExcel')->name('reports.saleCostingsearchDExcel');//excel for one date
Route::get('/reports/saleCostingsearch/{D}/pdf/date','HomeController@saleCostingSearchDPDF')->name('reports.saleCostingsearchDpdf');//pdf for one date
Route::get('/reports/saleCostingsearch/{from}/{to}/excel/date','HomeController@saleCostingSearchDatesExcel')->name('reports.saleCostingsearchDatesExcel');//excel for date diff

////////////////////// Home Controller Customer & Supplier Report ///////////////////
Route::get('/customerReport','HomeController@customerReport')->name('report.customerReport');
Route::get('/customerReport/{id}','HomeController@customerReportDetail')->name('report.customerReportDetail');
Route::get('/customerReport/{id}/excel','HomeController@customerReportexcel')->name('customerReport.excel');

Route::post('/customerReport/search','HomeController@customerReportsearch')->name('customerReport.search');

Route::get('/customerReport/search/{Y}/{id}/excel/year','HomeController@customerReportsearchYExcel')->name('customerReport.searchYExcel');//excel for year
Route::get('/customerReport/search/{M}/{id}/excel/month','HomeController@customerReportsearchMExcel')->name('customerReport.searchMExcel');//excel for month
Route::get('/customerReport/search/{D}/{id}/excel/date','HomeController@customerReportsearchDExcel')->name('customerReport.searchDExcel');//excel for year
Route::get('/customerReport/search/{from}/{to}/{id}/excel/dates','HomeController@customerReportsearchDDExcel')->name('customerReport.searchDDExcel');//excel for year
Route::get('/customerReport/search/{index}/{id}/excel','HomeController@customerReportsearchExcel')->name('customerReport.searchExcel');//excel for year


Route::get('/supplierReport','HomeController@supplierReport')->name('report.supplierReport');
Route::get('/supplierReport/{id}','HomeController@supplierReportDetail')->name('report.supplierReportDetail');
Route::get('/supplierReport/{id}/excel','HomeController@supplierReportexcel')->name('supplierReport.excel');

Route::post('/supplierReport/search','HomeController@supplierReportsearch')->name('supplierReport.search');

Route::get('/supplierReport/search/{Y}/{id}/excel/year','HomeController@supplierReportsearchYExcel')->name('supplierReport.searchYExcel');//excel for year
Route::get('/supplierReport/search/{M}/{id}/excel/month','HomeController@supplierReportsearchMExcel')->name('supplierReport.searchMExcel');//excel for month
Route::get('/supplierReport/search/{D}/{id}/excel/date','HomeController@supplierReportsearchDExcel')->name('supplierReport.searchDExcel');//excel for year
Route::get('/supplierReport/search/{from}/{to}/{id}/excel/dates','HomeController@supplierReportsearchDDExcel')->name('supplierReport.searchDDExcel');//excel for year
Route::get('/supplierReport/search/{index}/{id}/excel','HomeController@supplierReportsearchExcel')->name('supplierReport.searchExcel');//excel for year


//////////////// product controller /////////////////////////

Route::get('/product/datatable','ProductController@datatable')->name('product.datatable');
Route::get('/product/modal/{id}','ProductController@modal')->name('product.modal');
Route::get('/product/variants/{id}','ProductController@variants')->name('product.variants');
Route::get('/product/Pvariants/{id}','ProductController@Pvariants')->name('product.Pvariants');
Route::get('/product/Pvariantsforfinishproduct/{id}','ProductController@Pvariantsforfinishproduct')->name('product.Pvariantsforfinishproduct');
Route::get('/product/sales/{id}','ProductController@sales')->name('product.sales');
Route::get('/product/salesProduct/{id}','ProductController@salesProduct')->name('product.salesProduct');
Route::get('/product/detail/{id}','ProductController@detail')->name('product.detail');
Route::get('/product/excel','ProductController@excel')->name('product.excel');
Route::get('/product/finishProducts','ProductController@finishProducts')->name('product.finishProducts');
Route::get('/product/finishProducts/datatable','ProductController@finishProductsDatatable')->name('product.finishProducts.datatable');
Route::get('/product/finishProducts/edit/{id}','ProductController@finishProductEdit')->name('product.finishProducts.edit');
Route::put('/product/finishProducts/update/{id}','ProductController@finishProductUpdate')->name('product.finishProducts.update');
Route::get('/product/finish/excel','ProductController@Finishexcel')->name('product.finish.excel');
Route::get('/product/add','ProductController@add')->name('product.add');
Route::get('/product/stockShow/{id}','ProductController@stockShow')->name('product.stockShow');
Route::get('/product/add/show/{id}','ProductController@addSingleProduct')->name('product.add.show');
Route::get('/product/add/datatable','ProductController@addDT')->name('product.add.datatable');

Route::get('/product/productsearch/{id}','ProductController@productsearch')->name('product.productsearch');
Route::post('/product/addFinishProduct','ProductController@addFinishProduct')->name('product.addFinishProduct');
Route::resource('/product','ProductController');
Route::post('/product/status','ProductController@status')->name('product.status');
Route::post('/product/variants/status','ProductController@Vstatus')->name('product.variants.status');

/////////////////////// brands controller //////////////////////////

Route::get('/brands/datatable','BrandsController@datatable')->name('brands.datatable');
Route::get('/brands/excel','BrandsController@excel')->name('brands.excel');
Route::resource('/brands','BrandsController');
Route::post('/brands/status','BrandsController@status')->name('brands.status');

//////////////////////// unit controller ///////////////////////////

Route::get('/unit/datatable','UnitController@unitDatatable')->name('unit.datatable');
Route::resource('/unit','UnitController');
Route::post('/unit/status','UnitController@status')->name('unit.status');

///////////////////// category controller //////////////////////

Route::get('/category/datatable','CategoryController@datatable')->name('category.datatable');
Route::get('/category/excel','CategoryController@excel')->name('category.excel');
Route::resource('/category','CategoryController');
Route::post('/category/status','CategoryController@status')->name('category.status');

//////////////////// sub category controller ///////////////////////

Route::get('/subcategory/datatable','SubcategoryController@datatable')->name('subcategory.datatable');
Route::resource('/subcategory','SubcategoryController');
Route::post('/subcategory/status','SubcategoryController@status')->name('subcategory.status');

////////////// city controller ///////////////////////

Route::post('/city/status','CityController@status')->name('city.status');
Route::get('/city/datatable','CityController@datatable')->name('city.datatable');
Route::resource('/city','CityController');

//////////////////// warehouse controller /////////////////////

Route::get('/warehouse/datatable','WarehouseController@datatable')->name('warehouse.datatable');
Route::resource('/warehouse','WarehouseController');

/////////////////// product transfer controller /////////////////////////

Route::post('/productTransfer/status','ProductTransaferHistoryController@status')->name('productTransfer.status');
Route::get('/productTransfer/product','ProductTransaferHistoryController@product')->name('productTransfer.product');
Route::get('/productTransfer/excel','ProductTransaferHistoryController@excel')->name('productTransfer.excel');
Route::get('/productTransfer/pdf','ProductTransaferHistoryController@pdf')->name('productTransfer.pdf');
Route::post('/productTransfer/search','ProductTransaferHistoryController@search')->name('productTransfer.search');
Route::get('/productTransfer/search/{M}/{pname}/type/{fw}/{tw}/excel/month','ProductTransaferHistoryController@searchMExcel')->name('productTransfer.searchMExcel');//excel for month
Route::get('/productTransfer/search/{Y}/{pname}/type/{fw}/{tw}/excel/year','ProductTransaferHistoryController@searchYExcel')->name('productTransfer.searchYExcel');//excel for year
Route::get('/productTransfer/search/{D}/{pname}/type/{fw}/{tw}/excel/date','ProductTransaferHistoryController@searchDExcel')->name('productTransfer.searchDExcel');//excel for one date
Route::get('/productTransfer/search/{from}/{to}/{pname}/type/{fw}/{tw}/excel/date','ProductTransaferHistoryController@searchDatesExcel')->name('productTransfer.searchDatesExcel');//excel for date diff
Route::resource('/productTransfer','ProductTransaferHistoryController');

///////////////// company controller ////////////////////////

Route::get('/company/datatable','CompanyController@datatable')->name('company.datatable');
Route::get('/company/excel','CompanyController@excel')->name('company.excel');
Route::get('/company/pdf','CompanyController@pdf')->name('company.pdf');
Route::resource('/company','CompanyController');

/////////////////// group controller //////////////////////////

Route::get('/group/datatable','GroupController@datatable')->name('group.datatable');
Route::resource('/group','GroupController');

///////////////counter controller///////////////////////////
Route::get('/counter/datatable','CounterController@datatable')->name('counter.datatable');
Route::resource('/counter','CounterController');


///////////////CounterAssignedController ///////////////////////////
Route::get('/assigned/datatable','CounterAssignedController@datatable')->name('assigned.datatable');
Route::resource('/assigned','CounterAssignedController');

/////////////// customer controller ///////////////////////
Route::get('/customer/importStructure','CustomerController@importStructure')->name('customer.importStructure');

Route::post('/customer/status','CustomerController@status')->name('customer.status');
Route::get('/customer/datatable','CustomerController@datatable')->name('customer.datatable');
Route::get('/customer/excel','CustomerController@excel')->name('customer.excel');
Route::get('/customer/{id}/document','CustomerController@document')->name('customer.document');
Route::get('/customer/document/{id}','CustomerController@singleDocument')->name('customer.singleDocument');
Route::get('/customer/pdf','CustomerController@pdf')->name('customer.pdf');
Route::get('/customer/balance/{id}','CustomerController@balance')->name('customer.balance');
Route::resource('/customer','CustomerController');

///////////////// supplier controller /////////////////

Route::post('/supplier/status','SupplierController@status')->name('supplier.status');
Route::get('/supplier/datatable','SupplierController@datatable')->name('supplier.datatable');
Route::get('/supplier/excel','SupplierController@excel')->name('supplier.excel');
Route::get('/supplier/pdf','SupplierController@pdf')->name('supplier.pdf');
Route::resource('/supplier','SupplierController');

////////////////// sale person controller ////////////////////////////////

Route::post('/saleperson/status','SalePersonController@status')->name('saleperson.status');
Route::get('/saleperson/datatable','SalePersonController@datatable')->name('saleperson.datatable');
Route::get('/saleperson/excel','SalePersonController@excel')->name('saleperson.excel');
Route::get('/saleperson/pdf','SalePersonController@pdf')->name('saleperson.pdf');
Route::resource('/saleperson','SalePersonController');

//////////////////// purchase controller  ///////////////////////////

Route::get('/purchase/search/{date}/{s_id}/{status}/{check}/excel/date','PurchaseOrderController@purchaseDateExcel')->name('purchase.purchaseDateExcel');//excel for month
Route::get('/purchase/search/{from}/{to}/{s_id}/{status}/{check}/excel/dates','PurchaseOrderController@purchaseDatesExcel')->name('purchase.purchaseDatesExcel');//excel for month
Route::get('/purchase/importStructure','PurchaseOrderController@importStructure')->name('purchase.importStructure');
Route::post('/purchase/search','PurchaseOrderController@search')->name('purchase.search');

Route::get('/purchase/search/{s_id}/{status}/{check}/excel/ostatus','PurchaseOrderController@purchaseOSExcel')->name('purchase.purchaseOSExcel');//excel for status
Route::get('/purchase/search/{year}/{s_id}/{status}/{check}/excel/year','PurchaseOrderController@purchaseYearExcel')->name('purchase.purchaseYearExcel');//excel for year
Route::get('/purchase/search/{month}/{s_id}/{status}/{check}/excel/month','PurchaseOrderController@purchaseMonthExcel')->name('purchase.purchaseMonthExcel');//excel for month
Route::get('/purchase/search/{index}/{s_id}/{status}/{check}/excel','PurchaseOrderController@purchaseExcel')->name('purchase.purchaseExcel');//excel for year
Route::get('/purchase/excel','PurchaseOrderController@excel')->name('purchase.excel');

Route::get('/purchase/editInvoice/{id}','PurchaseOrderController@editInvoice')->name('purchase.editInvoice');
Route::put('/purchase/updateInvoice/{id}','PurchaseOrderController@updateInvoice')->name('purchase.updateInvoice');

Route::get('/purchase/pdf/{id}','PurchaseOrderController@pdf')->name('purchase.pdf');
Route::get('/purchase/product/datatable','PurchaseOrderController@prodDatatable')->name('purchase.product.datatable');
Route::get('/purchase/order/invoice/{id}','PurchaseOrderController@purchaseInvoice')->name('purchase.order.invoice');
Route::get('/purchaseDetail/{id}','PurchaseOrderController@purchaseDetail')->name('purchaseDetail');
Route::get('/purchase/pdf','PurchaseOrderController@pdf1');
Route::get('/purchase/grr/{no,id}','PurchaseOrderController@GRR')->name('purchase.GRR');
Route::get('/purchase/grr1/{no}','PurchaseOrderController@GRR1');
Route::get('/purchase/document/{id}','PurchaseOrderController@document')->name('purchase.document');
Route::get('/purchase/invoice/{id}','PurchaseOrderController@invoice')->name('purchase.invoice');
Route::get('/purchase/product/{id}','PurchaseOrderController@product')->name('purchase.product');
Route::get('/purchase/grn/{id}','PurchaseOrderController@GRN')->name('purchase.grn');
Route::get('/purchase/report','PurchaseOrderController@report')->name('purchase.report');
Route::resource('/purchase','PurchaseOrderController');
Route::post('/purchase/status','PurchaseOrderController@status')->name('purchase.status');
// Route::post('/purchase/Receiving','PurchaseOrderController@Receiving')->name('purchase.Receiving');
Route::post('/purchase/updateQuantity','PurchaseOrderController@updateQuantity')->name('purchase.updateQuantity');

//////////////////// biller controller ////////////////////////////////

Route::post('/biller/status','BillerController@status')->name('biller.status');
Route::get('/biller/datatable','BillerController@datatable')->name('biller.datatable');
Route::get('/biller/excel','BillerController@excel')->name('biller.excel');
Route::get('/biller/pdf','BillerController@pdf')->name('biller.pdf');
Route::resource('/biller','BillerController');

//////////////////// sales  controller ///////////////////////////

Route::post('/sales/search','SalesController@search')->name('sales.search');
Route::get('/sales/sync','SalesController@sync')->name('sales.sync');
Route::get('/sales/return/voucher/{id}','SalesController@voucher')->name('sales.return.voucher');
Route::get('/sales/search/{index}/{c}/{s}/{ss}/{ps}/{check}/excel','SalesController@salesExcel')->name('sales.salesExcel');//excel for year

Route::get('/sales/excel','SalesController@excel')->name('sales.excel');
Route::get('/sales/search/{Y}/{c}/{s}/{ss}/{ps}/{check}/excel/year','SalesController@saleYExcel')->name('sales.saleYExcel');//excel for year
Route::get('/sales/search/{M}/{c}/{s}/{ss}/{ps}/{check}/excel/month','SalesController@saleMExcel')->name('sales.saleMExcel');//excel for month
Route::get('/sales/search/{D}/{c}/{s}/{ss}/{ps}/{check}/excel/date','SalesController@saleDExcel')->name('sales.saleDExcel');//excel for one date
Route::get('/sales/search/{from}/{to}/{c}/{s}/{ss}/{ps}/{check}/excel/date','SalesController@saleDatesExcel')->name('sales.saleDatesExcel');//excel for date diff
Route::get('/sales/search/{c}/{s}/{ss}/{check}/excel/ss','SalesController@saleSSExcel')->name('sales.saleSSExcel');//excel for year
Route::get('/sales/search/{c}/{s}/{ps}/{check}/excel/ps','SalesController@salePSExcel')->name('sales.salePSExcel');//excel for year
Route::get('/sales/search/{c}/excel/c','SalesController@saleCExcel')->name('sales.saleCExcel');//excel for year
Route::get('/sales/search/{sp}/excel/sp','SalesController@saleSPExcel')->name('sales.saleSPExcel');//excel for year

Route::get('/sales/report','SalesController@report')->name('sales.report');
Route::get('/sales/report/excel','SalesController@reportExcel')->name('sales.report.excel');
Route::post('/sales/report/search','SalesController@reportSearch')->name('sales.report.search');
Route::get('/sales/report/search/{Y}/{c}/{s}/{p}/{type}/{w}/{check}/excel/year','SalesController@reportExcelYExcel')->name('sales.report.saleYExcel');//excel for year
Route::get('/sales/report/search/{M}/{c}/{s}/{p/{type}}/{w}/{check}/excel/month','SalesController@reportExcelMExcel')->name('sales.report.saleMExcel');//excel for month
Route::get('/sales/report/search/{D}/{c}/{s}/{p}/{type}/{w}/{check}/excel/date','SalesController@reportExcelDExcel')->name('sales.report.saleDExcel');//excel for one date
Route::get('/sales/report/search/{from}/{to}/{c}/{s}/{p}/{type}/{w}/{check}/excel/date','SalesController@reportExcelDatesExcel')->name('sales.report.saleDatesExcel');//excel for date diff
Route::get('/sales/report/search/{c}/excel/c','SalesController@reportCExcel')->name('sales.report.reportExcelCExcel');//excel for year
Route::get('/sales/report/search/{p}/{type}/excel/p','SalesController@reportPExcel')->name('sales.report.reportExcelPExcel');//excel for year
Route::get('/sales/report/search/{w}/excel/w','SalesController@reportWExcel')->name('sales.report.reportExcelWExcel');//excel for year
Route::get('/sales/report/search/{sp}/excel/sp','SalesController@reportSPExcel')->name('sales.report.reportExcelSPExcel');

Route::get('/sales/editInvoice/{id}','SalesController@editInvoice')->name('sales.editInvoice');
Route::put('/sales/updateInvoice/{id}','SalesController@updateInvoice')->name('sales.updateInvoice');

Route::get('/sales/allInvoices/{id}','SalesController@allInvoices')->name('sales.allInvoices');
Route::get('/sales/allInvoices/pdf/{id}','SalesController@allInvoicesPDF')->name('sales.allInvoices.pdf');

Route::get('/sales/importStructure','SalesController@importStructure')->name('sales.importStructure');

Route::get('/sales/pdf/{id}','SalesController@pdf')->name('sales.pdf');
Route::get('/sales/pdf','SalesController@pdf1');
Route::get('/sales/gdr/{no,id}','SalesController@GDR')->name('sales.GDR');
Route::get('/sales/gdr1/{no}','SalesController@GDR1');
Route::get('/sales/invoice/{id}','SalesController@invoice')->name('sales.invoice');
Route::get('/sales/product/{id}','SalesController@product')->name('sales.product');
Route::get('/sales/document/{id}','SalesController@document')->name('sales.document');
Route::get('/sales/gdn/{id}','SalesController@GDN')->name('sales.gdn');
Route::get('/sales/return/view','SalesController@returnView')->name('sales.return.view');
Route::get('/sales/return/show/{id}','SalesController@returnShow')->name('sales.return.show');
Route::resource('/sales','SalesController');
Route::post('/sales/status','SalesController@status')->name('sales.status');
Route::post('/sales/returnstatus','SalesController@returnStatus')->name('sales.returnstatus');
Route::post('/sales/delivery','SalesController@delivery')->name('sales.delivery');
Route::post('/sales/return','SalesController@return')->name('sales.return');


Route::get('/sales/format/{format}/{biller}','SalesController@saleFormat')->name('sales.format');

///////////////// transaction controller ////////////////////////////
Route::get('/transaction/search/{index}/{tf}/{pb}/excel','TransactionHistoryController@searchExcel')->name('transaction.searchExcel');//excel for year

Route::get('/transaction/view','TransactionHistoryController@view')->name('transaction.view');
Route::get('/transaction/excel','TransactionHistoryController@excel')->name('transaction.excel');
Route::post('/transaction/search','TransactionHistoryController@search')->name('transaction.search');
Route::get('/transaction/search/{M}/{tf}/{pb}/excel/month','TransactionHistoryController@searchMExcel')->name('transaction.searchMExcel');//excel for month
Route::get('/transaction/search/{Y}/{tf}/{pb}/excel/year','TransactionHistoryController@searchYExcel')->name('transaction.searchYExcel');//excel for year
Route::get('/transaction/search/{D}/{tf}/{pb}/excel/date','TransactionHistoryController@searchDExcel')->name('transaction.searchDExcel');//excel for one date
Route::get('/transaction/search/{from}/{to}/{tf}/{pb}/excel/date','TransactionHistoryController@searchDatesExcel')->name('transaction.searchDatesExcel');//excel for date diff
// Route::get('/transaction/datatable','TransactionHistoryController@datatable')->name('transaction.datatable');
Route::resource('/transaction','TransactionHistoryController');

////////////////// stock controller ///////////////////////////

Route::post('/stocks/stockInSearch','StockController@stockInSearch')->name('stocks.stockInSearch');
Route::get('/stocks/search/{id}','StockController@search')->name('stocks.search');
Route::get('/stocks/current','StockController@current')->name('stocks.current');
Route::get('/stocks/OC_inventory','StockController@OC_inventory')->name('stocks.OC_inventory');
Route::post('/stocks/OC_inventory','StockController@OC_inventory')->name('stocks.OC_inventory');
Route::get('/stocks/pdf','StockController@pdf')->name('stocks.pdf');
Route::get('/stocks/grr','StockController@GRR')->name('stocks.grr');
Route::get('/stocks/grr1/{id}','StockController@GRR1')->name('stocks.grr1');
Route::get('/stocks/excel','StockController@excel')->name('stocks.excel');
Route::resource('/stocks','StockController');

///////////////////// bank controller /////////////////////////////

Route::post('/bank/status','BankController@status')->name('bank.status');
Route::get('/bank/datatable','BankController@datatable')->name('bank.datatable');
Route::resource('/bank','BankController');

/////////////////////// accounts controller ///////////////////////

Route::post('/accountdetails/search','AccountDetailsController@search')->name('accountdetails.search');

Route::get('/accountdetails/search/{Y}/{id}/excel/year','AccountDetailsController@searchYExcel')->name('accountdetails.searchYExcel');//excel for year
Route::get('/accountdetails/search/{M}/{id}/excel/month','AccountDetailsController@searchMExcel')->name('accountdetails.searchMExcel');//excel for month
Route::get('/accountdetails/search/{D}/{id}/excel/date','AccountDetailsController@searchDExcel')->name('accountdetails.searchDExcel');//excel for year
Route::get('/accountdetails/search/{from}/{to}/{id}/excel/dates','AccountDetailsController@searchDDExcel')->name('accountdetails.searchDDExcel');//excel for year
Route::get('/accountdetails/search/{index}/{id}/excel','AccountDetailsController@searchExcel')->name('accountdetails.searchExcel');//excel for year


Route::get('/accountdetails/search/{Y}/{id}/pdf/year','AccountDetailsController@searchYPDF')->name('accountdetails.searchYPDF');//excel for year
Route::get('/accountdetails/search/{M}/{id}/pdf/month','AccountDetailsController@searchMPDF')->name('accountdetails.searchMPDF');//excel for month
Route::get('/accountdetails/search/{D}/{id}/pdf/date','AccountDetailsController@searchDPDF')->name('accountdetails.searchDPDF');//excel for year
Route::get('/accountdetails/search/{from}/{to}/{id}/pdf/dates','AccountDetailsController@searchDDPDF')->name('accountdetails.searchDDPDF');//excel for year
Route::get('/accountdetails/search/{index}/{id}/pdf','AccountDetailsController@searchPDF')->name('accountdetails.searchPDF');//excel for year


Route::get('/accountdetails/view/{id}/{status}','AccountDetailsController@view')->name('accountdetails.view');
Route::get('/accountdetails/prodName/{id}','AccountDetailsController@prodName')->name('accountdetails.prodName');
Route::get('/accountdetails/chkHistory/{id}','AccountDetailsController@chkHistory')->name('accountdetails.chkHistory');
Route::get('/accountdetails/history/{id}/{mid}','AccountDetailsController@history')->name('accountdetails.history');
Route::get('/accountdetails/{id}/{code}/{name}/excel','AccountDetailsController@excel')->name('accountdetails.excel');
Route::get('/accountdetails/{id}/pdf','AccountDetailsController@pdf')->name('accountdetails.pdf');
Route::resource('/accountdetails','AccountDetailsController');

///////////////////// general ledger controller ////////////////////////////

Route::post('/generalLedger/updateEntry','GeneralLedgerController@updateEntry')->name('generalLedger.updateEntry');
Route::get('/generalLedger/find/customer/{id}','GeneralLedgerController@findCustomer')->name('generalLedger.customer.find');
Route::get('/generalLedger/find/supplier/{id}','GeneralLedgerController@findSupplier')->name('generalLedger.supplier.find');
Route::resource('/generalLedger','GeneralLedgerController');

////////////////// head category controller ////////////////////
Route::post('/headcategory/search','HeadCategoryController@search')->name('headcategory.search');

Route::get('/headcategory/search/{Y}/{id}/excel/year','HeadCategoryController@searchYExcel')->name('headcategory.searchYExcel');//excel for year
Route::get('/headcategory/search/{M}/{id}/excel/month','HeadCategoryController@searchMExcel')->name('headcategory.searchMExcel');//excel for month
Route::get('/headcategory/search/{D}/{id}/excel/date','HeadCategoryController@searchDExcel')->name('headcategory.searchDExcel');//excel for year
Route::get('/headcategory/search/{from}/{to}/{id}/excel/dates','HeadCategoryController@searchDDExcel')->name('headcategory.searchDDExcel');//excel for year
Route::get('/headcategory/search/{index}/{id}/excel','HeadCategoryController@searchExcel')->name('headcategory.searchExcel');//excel for year


Route::get('/headcategory/search/{Y}/{id}/pdf/year','HeadCategoryController@searchYPDF')->name('headcategory.searchYPDF');//excel for year
Route::get('/headcategory/search/{M}/{id}/pdf/month','HeadCategoryController@searchMPDF')->name('headcategory.searchMPDF');//excel for month
Route::get('/headcategory/search/{D}/{id}/pdf/date','HeadCategoryController@searchDPDF')->name('headcategory.searchDPDF');//excel for year
Route::get('/headcategory/search/{from}/{to}/{id}/pdf/dates','HeadCategoryController@searchDDPDF')->name('headcategory.searchDDPDF');//excel for year
Route::get('/headcategory/search/{index}/{id}/pdf','HeadCategoryController@searchPDF')->name('headcategory.searchPDF');//excel for year

Route::get('/headcategory/history/{id}/{mid}','HeadCategoryController@history')->name('headcategory.history');
Route::get('/headcategory/{id}/{code}/{name}/excel','HeadCategoryController@excel')->name('headcategory.excel');
Route::get('/headcategory/{id}/pdf','HeadCategoryController@pdf')->name('headcategory.pdf');
Route::get('/headcategory/account/{id}','HeadCategoryController@accountsName')->name('headcategory.account');
Route::resource('/headcategory','HeadCategoryController');

/////////////////////// head of accounts controller///////////////////////////////
Route::get('/headofaccounts/find/{id}','HeadofAccountsController@find')->name('headofaccounts.find');
Route::resource('/headofaccounts','HeadofAccountsController');

//////////////////// stock out controller ///////////////////////////

Route::get('/stockout/PosInvoice/{id}','StockOutController@PosInvoice')->name('stockout.PosInvoice');
Route::get('/stockout/pdf/{id}','StockOutController@pdf')->name('stockout.pdf');
Route::post('/stockout/stockOutSearch','StockOutController@stockOutSearch')->name('stockout.stockOutSearch');
Route::resource('/stockout','StockOutController');

/////////////////////// Stock Out Request Controller ////////////////////////////
Route::post('/stockoutRequest/status','StockOutRequestController@status')->name('stockoutRequest.status');
Route::post('/stockoutRequest/delivery','StockOutRequestController@delivery')->name('stockoutRequest.delivery');
Route::resource('/stockoutRequest','StockOutRequestController');

////////////////////// Purchase Request Controller /////////////////////////
Route::post('/request/status','PurchaseRequestController@status')->name('request.status');
// Route::get('/request/datatable','PurchaseRequestController@datatable')->name('request.datatable');
Route::resource('/request','PurchaseRequestController');

//////////////////////// Quotation Controller/////////////////////////////////
Route::get('/quotation/newQuotation/{id}','QuotationController@newQuotation')->name('quotation.newQuotation');
Route::post('/quotation/status','QuotationController@status')->name('quotation.status');
Route::resource('/quotation','QuotationController');


////////////////////// Roles Controller ==////////////////////////
Route::get('/roles/datatable','RolesController@datatable')->name('roles.datatable');
Route::get('/roles/menu/{id}','RolesController@menu')->name('roles.menu');
Route::get('/roles/permission/{id}','RolesController@permission')->name('roles.permission');
Route::post('/roles/status','RolesController@status')->name('roles.status');
Route::resource('/roles','RolesController');


////////////////////// User Menu Controller ==////////////////////////
Route::get('/menu/datatable','UserMenuController@datatable')->name('menu.datatable');
Route::resource('/menu','UserMenuController');

///////////////// permission controller////////////////////////
Route::get('/permission/datatable','PermissionsController@datatable')->name('permission.datatable');
Route::resource('/permission','PermissionsController');


///////////////// user controller////////////////////////
Route::get('/users/datatable','UserController@datatable')->name('users.datatable');
Route::post('/users/editProfile/update','UserController@editProfileUpdate')->name('users.editProfile.update');
Route::get('/users/editProfile/{id}','UserController@editProfile')->name('users.editProfile');
Route::resource('/users','UserController');

Auth::routes();
////////////////////// login customize ============================

Route::post('/login/user','Auth\LoginController@loginUser')->name('login.user');

//////////////// mark all notifications as read /////////////////////////////
Route::get('markAsRead',function(){
    auth()->user()->unreadNotifications->markAsRead();
    return redirect()->back();
})->name('markAllRead');

////////////////////// mark signle notification as read /////////////////////
Route::get('markRead/{id}',function($id){
    $userUnreadNotification = auth()->user()->unreadNotifications->where('id', $id)->first();
    if($userUnreadNotification) {
        $userUnreadNotification->markAsRead();
    }
    return redirect()->back();
})->name('markReadSingle');

//////////////////////// VIEW ALL NOTIFICATIONS /////////////////////////////////
Route::get('/notifications', function () {
    return view('notifications');
})->name('viewAllNotifications');


/////////////////////// biller request update ////////////////////////
Route::get('/billerUpdateRequest/status/{id}','BillerUpdateRequestController@status')->name('billerUpdateRequest.status');
Route::get('/billerUpdateRequest/datatable','BillerUpdateRequestController@datatable')->name('billerUpdateRequest.datatable');
Route::resource('/billerUpdateRequest','BillerUpdateRequestController');


/////////////////////// customer request update ////////////////////////
Route::get('/customerUpdateRequest/status/{id}','CustomerUpdateRequestController@status')->name('customerUpdateRequest.status');
Route::get('/customerUpdateRequest/datatable','CustomerUpdateRequestController@datatable')->name('customerUpdateRequest.datatable');
Route::resource('/customerUpdateRequest','CustomerUpdateRequestController');

/////////////////////// saleperson request update ////////////////////////
Route::get('/salepersonUpdateRequest/status/{id}','SalePersonUpdateRequestController@status')->name('salepersonUpdateRequest.status');
Route::get('/salepersonUpdateRequest/datatable','SalePersonUpdateRequestController@datatable')->name('salepersonUpdateRequest.datatable');
Route::resource('/salepersonUpdateRequest','SalePersonUpdateRequestController');

/////////////////////// supplier request update ////////////////////////
Route::get('/supplierUpdateRequest/status/{id}','SupplierUpdateRequestController@status')->name('supplierUpdateRequest.status');
Route::get('/supplierUpdateRequest/datatable','SupplierUpdateRequestController@datatable')->name('supplierUpdateRequest.datatable');
Route::resource('/supplierUpdateRequest','SupplierUpdateRequestController');


/////////////////////// product request update ////////////////////////
Route::get('/productUpdateRequest/status/{id}','ProductUpdateRequestController@status')->name('productUpdateRequest.status');
Route::get('/productUpdateRequest/datatable','ProductUpdateRequestController@datatable')->name('productUpdateRequest.datatable');
Route::resource('/productUpdateRequest','ProductUpdateRequestController');

//////////////////////// variants controller ///////////////////////////
Route::resource('/variants','VariantsController');

/////////////////////// GST controller /////////////////////////
Route::get('/gst/datatable','GSTController@datatable')->name('gst.datatable');
Route::resource('/gst','GSTController');

/////////////////////// CostType controller /////////////////////////
Route::get('/costtype/datatable','CostTypeController@datatable')->name('costtype.datatable');
Route::resource('/costtype','CostTypeController');


///////////// opening inventory and history/ ///////////////////////////
Route::get('/openingInventory','OpeningInventoryController@viewOpening')->name('openingInventory.viewOpening');
Route::get('/openingInventory/{id}/edit','OpeningInventoryController@edit')->name('openingInventory.edit');
Route::get('/openingInventory/update','OpeningInventoryController@update')->name('openingInventory.update');
Route::get('/history/cost','OpeningInventoryController@avgHistory')->name('history.avgHistory');
Route::get('/history/damages','OpeningInventoryController@damagesHistory')->name('history.damages');

/////////////// Journal Voucher ////////////////////////////
Route::get('/JournalVoucher/search/{amount}/excel/amount','JournalVoucherController@JournalVoucherAmountExcel')->name('JournalVoucher.JournalVoucherAmountExcel');//excel for status
Route::get('/JournalVoucher/search/{desc}/excel/desc','JournalVoucherController@JournalVoucherDescExcel')->name('JournalVoucher.JournalVoucherDescExcel');//excel for status
Route::get('/JournalVoucher/search/{vno}/excel/vno','JournalVoucherController@JournalVoucherVnoExcel')->name('JournalVoucher.JournalVoucherVnoExcel');//excel for status


Route::get('/JournalVoucher/search/{date}/{account}/{check}/excel/date','JournalVoucherController@JournalVoucherDateExcel')->name('JournalVoucher.JournalVoucherDateExcel');//excel for month
Route::get('/JournalVoucher/search/{from}/{to}/{accoount}/{check}/excel/dates','JournalVoucherController@JournalVoucherDatesExcel')->name('JournalVoucher.JournalVoucherDatesExcel');//excel for month
Route::get('/JournalVoucher/search/{index}/excel','JournalVoucherController@JournalVoucherExcel')->name('JournalVoucher.JournalVoucherExcel');//excel for year

Route::get('/JournalVoucher/search/{account}/excel/account','JournalVoucherController@JournalVoucherAccountExcel')->name('JournalVoucher.JournalVoucherAccountExcel');//excel for status
Route::get('/JournalVoucher/search/{year}/{account}/{check}/excel/year','JournalVoucherController@JournalVoucherYearExcel')->name('JournalVoucher.JournalVoucherYearExcel');//excel for year
Route::get('/JournalVoucher/search/{month}/{account}/{check}/excel/month','JournalVoucherController@JournalVoucherMonthExcel')->name('JournalVoucher.JournalVoucherMonthExcel');//excel for month
Route::post('/JournalVoucher/search','JournalVoucherController@search')->name('JournalVoucher.search');
Route::get('/JournalVoucher/excel','JournalVoucherController@excel')->name('JournalVoucher.excel');

Route::get('/JournalVoucher/document/{id}','JournalVoucherController@document')->name('JournalVoucher.document');

Route::post('/JournalVoucher/status','JournalVoucherController@status')->name('JournalVoucher.status');
Route::get('/JournalVoucher/biller/{id}','JournalVoucherController@biller')->name('JournalVoucher.biller');
Route::get('/JournalVoucher/pdf/{id}','JournalVoucherController@pdf')->name('JournalVoucher.pdf');
Route::get('/JournalVoucher/importStructure','JournalVoucherController@importStructure')->name('JournalVoucher.importStructure');
Route::resource('/JournalVoucher','JournalVoucherController');

/////////////////// SALE DOCUMENT CONTROLLER //////////////////////////
Route::post('/addDoc','SaleDocumentController@addDoc')->name('addDoc');
Route::get('/viewDoc/{id}','SaleDocumentController@viewDoc')->name('viewDoc');


/////////////////////// SaleType controller /////////////////////////
Route::get('/saletype/datatable','SaleTypeController@datatable')->name('saletype.datatable');
Route::resource('/saletype','SaleTypeController');


/////////////// import controller ////////////


Route::prefix('import')->group(function () {
    Route::post('journalVoucher','ImportController@journalVoucher')->name('import.journalVoucher');
    Route::post('purchaseOrder','ImportController@purchaseOrder')->name('import.purchaseOrder');
    Route::post('saleOrder','ImportController@saleOrder')->name('import.saleOrder');
    Route::post('receivables','ImportController@receivables')->name('import.receivables');
});
Route::prefix('getproduct')->group(function () {
    Route::get('/{brand?}/{category?}','API\SiteController@getProduct');
});

/////////////////////////// DC ///////////////////////////
Route::post('/dc/delivery','DCController@delivery')->name('saldces.delivery');
Route::post('/dc/status','DCController@status')->name('dc.status');
Route::get('/dc/format/{format}/{biller}','DCController@saleFormat')->name('dc.format');
Route::get('/dc/pdf/{id}','DCController@pdf')->name('dc.pdf');
Route::get('/dc/pdf','DCController@pdf1');
Route::get('/dc/document/{id}','DCController@document')->name('dc.document');
Route::get('/dc/gdn/{id}','DCController@GDN')->name('dc.gdn');
Route::get('/dc/gdr/{no,id}','DCController@GDR')->name('dc.GDR');
Route::get('/dc/gdr1/{no}','DCController@GDR1');
Route::resource('/dc','DCController');
